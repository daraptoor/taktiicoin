// Defining datatable   
function dt(data){
    $('#'+data).DataTable({
        stateSave: true
    });
}

//Starting loader 
function loaderStart(msg){
    swal({
        imageUrl: 'https://taktii.sgp1.digitaloceanspaces.com/static/loaderCross.gif',
        imageHeight: 200,
        showConfirmButton: false,
        title: msg
      })
}

//Closing swal loader
function loaderEnd(){
    swal.close();
}

//Send SMS
function sendSMS(num,msg){
    $.ajax({
        type: 'GET',
        crossDomain: true,
        dataType: 'jsonp',
        url:'https://www.selfenabler.com/sendSMSAPI?send_to=91'+num+'&msg='+msg+'&userid=2000178430&password=taktii123',
        success:function(data){
            return data;
        }
    })
}

// success notification of left bottom
function success(msg, time=1000){
    var success = '<span style="font-size:16px;">'+msg+'</span>';
    $.notify({
        message: success 
    },{
        type: 'success',
        timer: time,
        placement: {
            from: "bottom",
            align: "left"
        },
        animate: {
            enter: 'animated pulse',
            exit: 'animated fadeOutUp'
        }
    });
}

// error notification of left bottom
function error(msg, time=1000){
    var error = '<span style="font-size:16px;">'+msg+'</span>';
    $.notify({
        message: error 
    },{
        type: 'danger',
        timer: time,
        placement: {
            from: "bottom",
            align: "left"
        },
        animate: {
            enter: 'animated pulse',
            exit: 'animated fadeOutUp'
        }
    });
}
// tooltip for days

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});






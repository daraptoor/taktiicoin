<?php
//
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueryController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Query List";
        //geting data from table
        $input = $request->all();
        $query_list = \App\Querystudentteacher::where([
            'inst_id'=>$user_id,
           // 'query_id'=> $input['query_id'],
            //['status','!=','2']
            ])->with(['querystudentdata','students'])->get();       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.query.list',compact('query_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }
}

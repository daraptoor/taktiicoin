<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function student(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
       // print_r($user_id);die;
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Student's List";
        // Getting data from database
        $student_list = \App\InstStudent::where([
            'inst_id'=>$user_id,
            ['status','!=','2']

            ])->with(['batchdetails'])->get();
//])->get(); 
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.student.list',compact('student_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_student(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Student";

        // call view templates
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.student.addStudent',compact('title','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    
    public function assign(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Assign Student in to a Batch";
        // Getting data from database
        $student_list = \App\InstStudent::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
 

        // call view templates
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.student.assignStudent',compact('batch_list','student_list','title','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    //for functions without pages only
    public function add_new_student(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $user_id = $request->session()->get('user_id');
        $input = $request->all();
        // Checking student exist with same institute
        $chk_student = \App\InstStudent::where([
            'mobile'=> $input['mobile'],
            'inst_id'=>$user_id,
        ])->count();
        if($chk_student > 0){
            $request->session()->flash('error', 'Unable to add Student, Same number already exist in your student list!!');
            return redirect('/student/add');
        }
        //dd($chk_student);
        //image uploading code
        $image = $request->file('image');
        if($image){
            $image_name = $input['mobile'].'_'.time().'.'.$image->getClientOriginalExtension();
            $image_path = 'public/uploads/student_img/'.$image_name;
            $destinationPath = public_path('/uploads/student_img');
            $image->move($destinationPath, $image_name);
        }else{
            $image_path = $input['image_exist'];
        }      
       
        // data collection
        $client = new \App\InstStudent;
        $client->std_id = $input['std_id'];
        $client->mobile = $input['mobile'];
        // $client->password = md5($input['password']);
        $client->first_name =  $input['first_name'];
        $client->last_name =  $input['last_name'];
        $client->email =  $input['email'];
        $client->gender =  $input['gender'];
        $client->address =  $input['address'];
        $client->address2 =  $input['address2'];
        $client->city =  $input['city'];
        $client->state =  $input['state'];
        $client->zip =  $input['zip'];
        $client->mother_name =  $input['mother_name'];
        $client->father_name =  $input['father_name'];
        $client->parent_mob =  $input['parent_mob'];
        $client->language =  $input['language'];
        $client->date_join =  $input['date_join'];        
        $client->image =  $image_path;
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $client->status =  '1';
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'New Student Added Successfully!');
            return redirect('/student/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Student Please try again!!');
            return redirect('/student/add');
        }
        print_r($data);
    }

    public function assign_student(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $user_id = $request->session()->get('user_id');
        $input = $request->all();
        
        // $client = \App\StudentRel::get();

        if($request->session()->get('user_role') == 0){
            $assign = 'owner';
        }elseif($request->session()->get('user_role') == 1){
            $assign = 'admin';
        }else{
            $assign = 'teacher';
        }

        $chk_student = \App\StudentRel::where([
            'student_id'=> $input['std_id'],
            'batch_id'=> $input['batch_id']
        ])->count();

      //  dd($chk_student);
//
        if($chk_student > 0 ){
            $request->session()->flash('error', 'Student already assigned to same batch please update it!');
            return redirect('/student/assign');
        }
        $user = new \App\StudentRel;
        $user->student_id = $input['std_id'];
        $user->batch_id = $input['batch_id'];
        $user->batch_name = $input['batch_name'];
        $user->assigned_by = $assign;
        $user->assign_id = $request->session()->get('user_id');
        $user->date_added = date("d-m-Y");
        $user->status = '1';
        $client = $user->save();
        if($client){
            $request->session()->flash('success', 'Student assigned Successfully to batch '.$input['batch_name'].'!');
            return redirect('/student/assign');
        }else{
            $request->session()->flash('error', 'Unable to assigned Student Please try again!!');
            return redirect('/student/assign');
        }

    }

    //Edit Student Code
    public function edit_student(Request $request, $slug){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Edit Student";
        $sid = base64_decode($slug);
        //getting data 
        $std_data = \App\InstStudent::find($sid);
        // call view templates
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.student.editStudent',compact('title','success','error','std_data'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    // Update Student Code
    public function update_student(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $user_id = $request->session()->has('user_id');
        $input = $request->all();

          // Checking student exist with same institute
          $chk_student = \App\InstStudent::where([
            'mobile'=> $input['mobile'],
            'inst_id'=>$inst_id,
        ])->count();
        //  if($chk_student > 0){
        //     $request->session()->flash('error', 'Unable to add Student, Same number already exist in your student list!!');
        //     return redirect('/student/add');
        // }
        
        //image uploading code
        $image = $request->file('image');
        if($image){
            $image_name = $input['mobile'].'_'.time().'.'.$image->getClientOriginalExtension();
            $image_path = 'public/uploads/student_img/'.$image_name;
            $destinationPath = public_path('/uploads/student_img');
            $image->move($destinationPath, $image_name);
        }else{
            $image_path = $input['image_exist'];
        }      
       
        // data collection
        $id = $input['s_id'];
        $client = \App\InstStudent::find($id);
        $client->std_id = $input['std_id'];
        $client->mobile = $input['mobile'];
        // if(isset($input['password'])){
        //     $client->password = md5($input['password']);
        // }
       // $client->password = md5($input['password']);
        $client->first_name =  $input['first_name'];
        $client->last_name =  $input['last_name'];
        $client->email =  $input['email'];
        $client->gender =  $input['gender'];
        $client->address =  $input['address'];
        $client->address2 =  $input['address2'];
        $client->city =  $input['city'];
        $client->state =  $input['state'];
        $client->zip =  $input['zip'];
        $client->mother_name =  $input['mother_name'];
        $client->father_name =  $input['father_name'];
        $client->parent_mob =  $input['parent_mob'];
        $client->language =  $input['language'];
        $client->date_join =  $input['date_join'];        
        $client->image =  $image_path;
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $inst_id : $request->session()->get('owner_id');
        $client->status =  '1';
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'Update Successfully!');
            return redirect('/student/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Please try again!!');
            return redirect('/student/editStudent'.base64_decode($id));
        }
        print_r($data);
    }
}

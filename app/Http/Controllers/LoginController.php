<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    public function index(Request $request){
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        if ($request->session()->has('user_id')) {
            $request->session()->forget('error');
            return redirect('/dashboard');
        }else{
            return view('login',compact('error'));
        }
    }

    public function teacher(Request $request){
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        if ($request->session()->has('user_id')) {
            $request->session()->forget('error');
            return redirect('/dashboard');
        }else{
            return view('loginTeacher',compact('error'));
        }
    }

    public function student(Request $request){
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        if ($request->session()->has('user_id')) {
            $request->session()->forget('error');
            return redirect('/dashboard');
        }else{
            return view('loginStudent',compact('error'));
        }
    }

    public function chkLogin(Request $request){

        $input = $request->all();
        $username =  $input['username'];
        $password =  md5($input['password']);
        
        $users = DB::table('inst_admins')->where([
            ['username', $username],
            ['password', $password]
        ])->get();
       
        //$users = DB::table('inst_admins')->get();
        if(count($users) == 1){
            session(['user_id' => $users[0]->id]);
            session(['user_name' => $users[0]->username]);
            session(['inst_email' => $users[0]->inst_email]);
            session(['inst_name' => $users[0]->inst_name]);
            session(['inst_mobile' => $users[0]->inst_mobile]);
            session(['user_role' => $users[0]->user_role]);
            session(['user_type' => "A"]);
            session(['logo' => $users[0]->logo]);
            session(['owner_id' => $users[0]->parrent]);
            return redirect('/dashboard');
            // $data = $request->session()->all();
            // return $data;
        }else{
            $request->session()->flush();
            $request->session()->flash('error', 'Error: Wrong Credential! Please check your Detail!!');
            return redirect('/');
        }
       return $users;
    }

    public function chkLoginTeacher(Request $request){

        $input = $request->all();
        
        $type =  $input['type'];
        if($type == 'setpass'){
            $mobile =  $input['mobile'];
            $otp =  $input['otp'];
            $password =  md5($input['password']);
            $otpValid = \App\OTP::where(
                ['otp'=>$otp],
                ['mobile'=>$mobile]
            )->count();
            //echo $otpValid;
            if($otpValid == '0'){
                //redirect
                $request->session()->flash('error', 'Error: OTP is not valid!!');
                return redirect('/login/teacher');
            }else{
                $up = \App\TeacherMain::where('mobile', $mobile)->update(['password' => $password]);
                if($up == '1'){
                    $users = DB::table('teachers')->where([
                        ['mobile', $mobile]
                    ])->get();
                    session(['user_id' => $users[0]->id]);
                    session(['user_name' => $users[0]->first_name]);
                    session(['inst_email' => $users[0]->email]);
                    session(['inst_name' => $users[0]->first_name]);
                    session(['inst_mobile' => $users[0]->mobile]);
                    session(['user_role' => $users[0]->user_role]);
                    session(['user_type' => "T"]);
                    session(['logo' => $users[0]->profile_img]);
                    session(['owner_id' => "-"]);
                    return redirect('/dashboard');
                }else{
                    $request->session()->flash('error', 'Error: Something went wrong Please Login again!!');
                    return redirect('/login/teacher');
                }
            }
        }elseif($type == 'login'){
            $mobile =  $input['mobile'];
            $password =  md5($input['password']);
            $users = DB::table('teachers')->where([
                ['mobile', $mobile],
                ['password', $password]
            ])->get();
            if(count($users) == '1'){
                session(['user_id' => $users[0]->id]);
                session(['user_name' => $users[0]->first_name]);
                session(['inst_email' => $users[0]->email]);
                session(['inst_name' => $users[0]->first_name]);
                session(['inst_mobile' => $users[0]->mobile]);
                session(['user_role' => $users[0]->user_role]);
                session(['user_type' => "T"]);
                session(['logo' => $users[0]->profile_img]);
                session(['owner_id' => "-"]);

                return redirect('/dashboard');
            }else{
                $request->session()->flash('error', 'Error: Please check your number and password!!');
                return redirect('/login/teacher');
            }
        }elseif($type == 'reg'){
            $mobile =  $input['mobile'];
            $password =  md5($input['password']);
            $email =  $input['email'];
            $otp =  $input['otp'];
            $first_name = $input['first_name'];
            $otpValid = \App\OTP::where(
                ['otp'=>$otp],
                ['mobile'=>$mobile]
            )->count();
            //echo $otpValid;

            if($otpValid == '0'){
                $request->session()->flash('error', 'Error: OTP is not valid!!');
                return redirect('/login/teacher');
            }else{
                //creating user here
                $user = new \App\TeacherMain;
                $user->mobile = $mobile;
                $user->first_name = $first_name;
                $user->email = $email;
                $user->password = $password;
                $user->user_type = 'teacher';
                $user->user_role = '1';
                $user->status = '1';
                $user->mobile_verify = '1';
                $user->added_date = date('d-m-Y H:i:s');
                $data = $user->save();
                if($data == 1){
                    $users = DB::table('teachers')->where([
                        ['mobile', $mobile],
                        ['password', $password]
                    ])->get();
                    session(['user_id' => $users[0]->id]);
                    session(['user_name' => $users[0]->first_name]);
                    session(['inst_email' => $users[0]->email]);
                    session(['inst_name' => $users[0]->first_name]);
                    session(['inst_mobile' => $users[0]->mobile]);
                    session(['user_role' => $users[0]->user_role]);
                    session(['user_type' => "T"]);
                    session(['logo' => '']);
                    session(['owner_id' => "-"]);

                    return redirect('/dashboard');
                }else{
                    $request->session()->flash('error', 'Unable to Register your profile Please check your detail!!');
                    return redirect('/login/teacher');
                }
            }
            
        }
    }


    public function chkLoginStudent(Request $request){

        $input = $request->all();
        
        $type =  $input['type'];
        if($type == 'setpass'){
            $mobile =  $input['mobile'];
            $otp =  $input['otp'];
            $password =  md5($input['password']);
            $otpValid = \App\OTP::where(
                ['otp'=>$otp],
                ['mobile'=>$mobile]
            )->count();
            //echo $otpValid;
            if($otpValid == '0'){
                //redirect
                $request->session()->flash('error', 'Error: OTP is not valid!!');
                return redirect('/login/student');
            }else{
                $up = \App\Student::where('mobile', $mobile)->update(['password' => $password]);
                if($up == '1'){
                    $users = DB::table('students')->where([
                        ['mobile', $mobile]
                    ])->get();
                    session(['user_id' => $users[0]->id]);
                    session(['user_name' => $users[0]->first_name]);
                    session(['inst_email' => $users[0]->email]);
                    session(['inst_name' => $users[0]->first_name]);
                    session(['inst_mobile' => $users[0]->mobile]);
                    session(['user_role' => "-"]);
                    session(['user_type' => "S"]);
                    session(['logo' => $users[0]->profile_pic]);
                    session(['owner_id' => "-"]);
                    return redirect('/dashboard');
                }else{
                    $request->session()->flash('error', 'Error: Something went wrong Please Login again!!');
                    return redirect('/login/student');
                }
            }
        }elseif($type == 'login'){
            $mobile =  $input['mobile'];
            $password =  md5($input['password']);
            $users = DB::table('students')->where([
                ['mobile', $mobile],
                ['password', $password]
            ])->get();
            if(count($users) == '1'){
                session(['user_id' => $users[0]->id]);
                session(['user_name' => $users[0]->first_name]);
                session(['inst_email' => $users[0]->email]);
                session(['inst_name' => $users[0]->first_name]);
                session(['inst_mobile' => $users[0]->mobile]);
                session(['user_role' => "-"]);
                session(['user_type' => "S"]);
                session(['logo' => $users[0]->profile_pic]);
                session(['owner_id' => "-"]);

                return redirect('/dashboard');
            }else{
                $request->session()->flash('error', 'Error: Please check your number and password!!');
                return redirect('/login/student');
            }
        }elseif($type == 'reg'){
            $mobile =  $input['mobile'];
            $password =  md5($input['password']);
            $email =  $input['email'];
            $otp =  $input['otp'];
            $first_name = $input['first_name'];
            $otpValid = \App\OTP::where(
                ['otp'=>$otp],
                ['mobile'=>$mobile]
            )->count();
            //echo $otpValid;

            if($otpValid == '0'){
                $request->session()->flash('error', 'Error: OTP is not valid!!');
                return redirect('/login/student');
            }else{
                //creating user here
                $user = new \App\Student;
                $user->mobile = $mobile;
                $user->first_name = $first_name;
                $user->email = $email;
                $user->password = $password;
                $user->status = '1';
                $user->mobile_verify = '1';
                $data = $user->save();
                if($data == 1){
                    $users = DB::table('students')->where([
                        ['mobile', $mobile],
                        ['password', $password]
                    ])->get();
                    session(['user_id' => $users[0]->id]);
                    session(['user_name' => $users[0]->first_name]);
                    session(['inst_email' => $users[0]->email]);
                    session(['inst_name' => $users[0]->first_name]);
                    session(['inst_mobile' => $users[0]->mobile]);
                    session(['user_role' => "-"]);
                    session(['user_type' => "S"]);
                    session(['logo' => '']);
                    session(['owner_id' => "-"]);

                    return redirect('/dashboard');
                }else{
                    $request->session()->flash('error', 'Unable to Register your profile Please check your detail!!');
                    return redirect('/login/student');
                }
            }
            
        }
    }

    public function teacherForget(Request $request){
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        return view('forgetTeacher',compact('error'));
    }

    public function chkforgetTeacher(Request $request){
        
        $input = $request->all();
        $mobile =  $input['mobile'];
        $otp =  $input['otp'];
        $password =  md5($input['password']);
        $otpValid = \App\OTP::where(
            ['otp'=>$otp],
            ['mobile'=>$mobile]
        )->count();
        //echo $otpValid;
        if($otpValid == '0'){
            //redirect
            $request->session()->flash('error', 'Error: OTP is not valid!!');
            return redirect('/login/teacher/forget');
        }else{
            $up = \App\TeacherMain::where('mobile', $mobile)->update(['password' => $password]);
            if($up == '1'){
                $request->session()->flash('error', 'Password Reset Successfully! Please Login Now!');
                return redirect('/login/teacher');
            }else{
                $request->session()->flash('error', 'Error: Something went wrong Please Try again!!');
                return redirect('/login/teacher/forget');
            }
        }
    }

    public function studentForget(Request $request){
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }
        return view('forgetStudent',compact('error'));
    }

    public function chkforgetStudent(Request $request){
        
        $input = $request->all();
        $mobile =  $input['mobile'];
        $otp =  $input['otp'];
        $password =  md5($input['password']);
        $otpValid = \App\OTP::where(
            ['otp'=>$otp],
            ['mobile'=>$mobile]
        )->count();
        //echo $otpValid;
        if($otpValid == '0'){
            //redirect
            $request->session()->flash('error', 'Error: OTP is not valid!!');
            return redirect('/login/student/forget');
        }else{
            $up = \App\Student::where('mobile', $mobile)->update(['password' => $password]);
            if($up == '1'){
                $request->session()->flash('error', 'Password Reset Successfully! Please Login Now!');
                return redirect('/login/student');
            }else{
                $request->session()->flash('error', 'Error: Something went wrong Please Try again!!');
                return redirect('/login/student/forget');
            }
        }
    }


    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/');
    }

}

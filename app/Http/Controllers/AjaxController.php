<?php
//
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Http\Controllers\ActionController;

class AjaxController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->session()->has('user_id') || $request->session()->get('user_role') != 1) {
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function chk_exist_username($slug)
    {
        $admin_list = \App\Admin::where([
            'username' => $slug,
        ])->count();
        return $admin_list;
    }

    public function chk_exist_teacher(Request $request, $slug)
    {
        $user_id = $request->session()->has('user_id');
        $teacher = \App\Teacher::where([
            'mobile' => $slug,
            'inst_id' => $user_id,
        ])->count();
        return $teacher;
    }

    public function chk_exist_teacher_main(Request $request, $slug)
    {
        $teacher = \App\TeacherMain::where([
            'mobile' => $slug,
        ])->get(array('password'));
        $count = count($teacher);
        $is_pass = (empty($teacher[0]->password)) ? '0' : '1';
        if ($is_pass == '0') {
            $otp = rand(1000, 9999);
            $flight = \App\OTP::updateOrCreate(
                ['mobile' => $slug],
                ['otp' => $otp]
            );
            ActionController::sendSms($slug, 'Your TAKTii OTP is ' . $otp . ' , Please use it for further process.');
        }
        return compact('count', 'is_pass');
    }

    public function chk_forget_teacher_main(Request $request, $slug){
        $teacher = \App\TeacherMain::where([
            'mobile' => $slug,
        ])->get();
        $count = count($teacher);
        if ($count != '0') {
            $otp = rand(1000, 9999);
            $flight = \App\OTP::updateOrCreate(
                ['mobile' => $slug],
                ['otp' => $otp]
            );
            ActionController::sendSms($slug, 'Your TAKTii OTP is ' . $otp . ' , Please use it for further process.');
        }
        return compact('count');
    }

    public function chk_forget_student_main(Request $request, $slug){
        $student = \App\Student::where([
            'mobile' => $slug,
        ])->get();
        $count = count($student);
        if ($count != '0') {
            $otp = rand(1000, 9999);
            $flight = \App\OTP::updateOrCreate(
                ['mobile' => $slug],
                ['otp' => $otp]
            );
            ActionController::sendSms($slug, 'Your TAKTii OTP is ' . $otp . ' , Please use it for further process.');
        }
        return compact('count');
    }



    public function chk_exist_student_main(Request $request, $slug)
    {
        $student = \App\Student::where([
            'mobile' => $slug,
        ])->get(array('password'));
        $count = count($student);
        $is_pass = (empty($student[0]->password)) ? '0' : '1';
        if ($is_pass == '0') {
            $otp = rand(1000, 9999);
            $flight = \App\OTP::updateOrCreate(
                ['mobile' => $slug],
                ['otp' => $otp]
            );
            ActionController::sendSms($slug, 'Your TAKTii OTP is ' . $otp . ' , Please use it for further process.');
        }
        return compact('count', 'is_pass');
    }

    public function update_user_status($slug, $status)
    {
        $user = \App\Admin::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_program_status($slug, $status)
    {
        $user = \App\Program::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_program_demo($slug, $demo)
    {
        $user = \App\Program::find(base64_decode($slug));
        $user->demo = $demo;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }


    public function update_book_institute_demo($slug, $demo)
    {
        $user = \App\Instdemo::find(base64_decode($slug));
        $user->demo = $demo;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 2;
        }
    }

    public function update_institute_demo($slug, $status)
    {
        $user = \App\Instdemo::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_institute_query($slug, $status)
    {
        $user = \App\Querystudentteacher::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }



    public function update_teacher_status($slug, $status)
    {
        $user = \App\Teacher::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_student_status($slug, $status)
    {
        $user = \App\InstStudent::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_batch_status($slug, $status)
    {
        $user = \App\InstBatch::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_treasure_status($slug, $status)
    {
        $user = \App\InstTreasure::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_promotion_status($slug, $status)
    {
        $user = \App\InstPromotion::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }
    public function update_achiever_status($slug, $status)
    {
        $user = \App\StarAchiever::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }


    public function update_rel_status($slug, $status)
    {
        $user = \App\StudentRel::find(base64_decode($slug));
        $user->status = $status;
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_pay_teacher($slug)
    {
        $user = \App\PayTeacher::find(base64_decode($slug));
        $data = $user->save();
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function get_classes_from_course($slug)
    {
        $data = \App\Classes::where(['courses_id' => base64_decode($slug), 'status' => 1])->get();
        echo "<option value='-' disabled selected>Select Course First</option>";
        foreach ($data as $dt) {
            echo "<option id='cls_" . $dt->id . "' value='" . base64_encode($dt->id) . "' data-name='" . $dt->name . "'>" . $dt->name . "</option>";
        }
    }

    public function get_subject_from_class($slug)
    {
        $data = \App\Subjects::where(['class_id' => base64_decode($slug), 'status' => 1])->get();
        echo "<option value='-' disabled >Select Class First</option>";
        foreach ($data as $dt) {
            echo "<option id='sub_" . $dt->id . "' value='" . base64_encode($dt->id) . "' data-name='" . $dt->name . "'>" . $dt->name . "</option>";
        }
    }

    public function get_selected_classes_from_course($slug, $selected)
    {
        $data = \App\Classes::where(['courses_id' => base64_decode($slug), 'status' => 1])->get();
        echo "<option value='-' disabled selected>Select Course First</option>";
        foreach ($data as $dt) {
            if (base64_encode($dt->id) == $selected) {
                $selected1 = "selected";
            } else {
                $selected1 = "";
            }
            echo "<option id='cls_" . $dt->id . "' value='" . base64_encode($dt->id) . "' data-name='" . $dt->name . "' " . $selected1 . ">" . $dt->name . "</option>";
        }
    }

    public function get_selected_subject_from_class($slug, $selected)
    {
        $data = \App\Subjects::where(['class_id' => base64_decode($slug), 'status' => 1])->get();
        echo "<option value='-' disabled selected>Select Class First</option>";
        foreach ($data as $dt) {
            if (base64_encode($dt->id) == $selected) {
                $selected1 = "selected";
            } else {
                $selected1 = "";
            }
            echo "<option id='sub_" . $dt->id . "' value='" . base64_encode($dt->id) . "' data-name='" . $dt->name . "' " . $selected1 . " >" . $dt->name . "</option>";
        }
    }

    public function chk_exist_number($slug)
    {
        $chk_student = \App\Student::where([
            'mobile' => $slug,
        ])->first();
        return $chk_student;
    }

    //delete functions
    public function del_rel_status(Request $request, $slug)
    {
        $user_id = $request->session()->has('user_id');
        if ($user_id) {
            $flight = \App\StudentRel::where([
                'id' => $slug,
            ])->delete();
            return 1;
        } else {
            return 0;
        }

    }

    public function get_batches_rel(Request $request,$slug, $name)
    {
        $assign_id = $request->session()->get('user_id');
        //echo $assign_id;
        $data = \App\StudentRel::where([
            'student_id' => $slug,
            'assign_id' => $assign_id,
            ['status', '!=', '2'],
            ])->with(['batchdetails'])->get();


        echo '<h2>Student Assigned batches</h2>';
        echo '<table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">';
        echo '<thead>
            <tr>
                <th>Student Name</th>
                <th>Batch Name</th>
                <th>Program Name </th>
                <th>Assigned By</th>
                <th>Assigned Date</th>
                <th>Remove</th>
            </tr>
        </thead><tbody>';

        foreach ($data as $dt) {
            echo '
            <tr id="row_' . $dt->id . '">
                <td>' . $name . '</td>
                <td>' . $dt->batch_name . '</td>
                <td>' . $dt->batchdetails['program_name'] . '</td>
                <td>' . $dt->assigned_by . '</td>
                <td>' . $dt->date_added . '</td>
                <td>
                <a class="btn btn-link btn-danger btn-sm btn-just-icon remove" style="font-size: 11px;padding: 3px 2px;" onclick="delete_rel(' . $dt->id . ')">
                <i class="material-icons">close</i>
                <div class="ripple-container"></div>
                </a>
            </td>
                
            
            </tr>';
        }
    
        echo "</tbody></table>";
        echo '<script>
        $(function(){
            setTimeout(function() { dt("tbleTeacherList"); }, 200   );

        })
        function change_status(id,data){
            if($("#status_"+id).is(":checked")){
                 $.ajax({
                     type:"get",
                     url:"' . url('/') . '/ajax/update_rel_status/"+data+"/1",
                     success:function(data){
                         if(data == 1){
                             success("Student batch enabled Successfully", 500);
                         }else{
                             error("Unable to update Student batch status", 500);
                         }
                    }
                 })
            }else{
                 $.ajax({
                    type:"get",
                    url:"' . url('/') . '/ajax/update_rel_status/"+data+"/0",
                     success:function(data){
                         if(data == 1){
                             success("Student batch status disabled Successfully", 500);
                         }else{
                             error("Unable to update Student batch status", 500);
                         }
                     }
                 })
            }
        }

        function delete_rel(id){

            swal({
                title: "Wait...",
                text: "Are you sure you want to delete Student assigned batch?<br><strong>This can\'t be Undone</strong>",
                type: "question",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, Delete it"
                }).then((result) => {
                    if(result){
                        loaderStart("Removing Student Batch..");
                        $.ajax({
                            type:"get",
                            url:"' . url('/') . '/ajax/del_rel_status/"+id,
                            success:function(data){

                                if(data == 1){
                                    success("Student batch removed Successfully", 500);
                                    var table = $("#tbleTeacherList").DataTable();
                                    table.row( $("#row_"+id) ).remove().draw();
                                }else{
                                    error("Unable to removed Student batch", 500);
                                }
                                loaderEnd();
                           }
                        })
                }
            })


        }
        </script>';
    }

    public function get_batches_students(Request $request, $slug, $date)
    {
        $batch_id = $slug;
        $user_id = $request->session()->get('user_id');
        if ($request->session()->get('user_role') == 0) {
            $assign = 'owner';
        } elseif ($request->session()->get('user_role') == 1) {
            $assign = 'admin';
        } else {
            $assign = 'teacher';
        }

        $data = \App\StudentRel::where([
            'batch_id' => $slug,
            'status' => '1',
        ])->with(['students:std_id,first_name,last_name,mobile'])->get();

        return view('dashboard.ajax.studentMarkAttend', compact('data', 'date', 'batch_id', 'user_id', 'assign'));
    }

    public function get_batches_students_list(Request $request, $slug, $date)
    {
        $batch_id = $slug;
        $user_id = $request->session()->get('user_id');
        if ($request->session()->get('user_role') == 0) {
            $assign = 'owner';
        } elseif ($request->session()->get('user_role') == 1) {
            $assign = 'admin';
        } else {
            $assign = 'teacher';
        }

        $data = \App\StudentRel::where([
            'batch_id' => $slug,
            'status' => '1',
        ])->with(['students:std_id,first_name,last_name,mobile','Attendence'])->get();

        return view('dashboard.ajax.studentlist', compact('data', 'date', 'batch_id', 'user_id', 'assign'));
    }


    public function get_batches_teachers(Request $request, $slug, $date)
    {
        $batch_id = $slug;
        $user_id = $request->session()->has('user_id');
        if ($request->session()->get('user_role') == 0) {
            $assign = 'owner';
        } elseif ($request->session()->get('user_role') == 1) {
            $assign = 'admin';
        } else {
            $assign = 'teacher';
        }

        $data = \App\StudentRel::where([
            'batch_id' => $slug,
            'status' => '1',
        ])->with(['students:id,first_name,last_name,mobile'])->get();

        return view('dashboard.ajax.teacherMarkAttend', compact('data', 'date', 'batch_id', 'user_id', 'assign'));
    }

    
    public function get_personal_batches_teachers(Request $request, $slug, $date)
    {
        
        $batch_id = $slug;
        $user_id = $request->session()->has('user_id');
        
        if ($request->session()->get('user_type') == "A" && $request->session()->get('user_role') == 0) {
            $assign = 'owner';
        } elseif ($request->session()->get('user_type') == "A" && $request->session()->get('user_role') == 1) {
            $assign = 'admin';
        } else {
            $assign = 'teacher';
        }
        
        $data = \App\TeacherStdRel::where([
            'batch_id' => $slug,
            'approve' => '1',
        ])->with(['students:id,first_name,last_name,mobile'])->get();

        return view('dashboard.ajax.singleteacherMarkAttend', compact('data', 'date', 'batch_id', 'user_id', 'assign'));
    }

    
    public function get_student_selected_by_batches(Request $request, $slug)
    {
        
        $batch_id = $slug;
        $data = \App\TeacherStdRel::where([
            'batch_id' => $slug,
            'approve' => '1',
        ])->with(['students:id,first_name,last_name,mobile'])->get(array('id','student_id'));
        foreach($data as $slist){
            //print_r($slist->students);
  //echo "<option " . $slist->students['id'] . "' value='" . $slist->students['id'] . "' data-name='" . $dt->name . "' " . $selected1 . ">" . $dt->name . "</option>";
            echo  "<option value= ". $slist->students['id']. ">". $slist->students['first_name']. "</option>";
        };
    }


    


    //live session

    public function notify(Request $request)
    {
        $input = $request->all();

        $teacher_id = $input['teacher_id'];
        $batch_code = $input['batch_code'];
        $message = $input['message'];
        // print_r($_POST);
        // die();

        $post = [
            'teacher_id' => $teacher_id,
            'batch_code' => $batch_code,
            'message' => $message,

        ];
        $curl_handle = curl_init('https://taktii.com/etc/api/index.php/mobile/teacher/send_push_notification_batch');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        // print_r($buffer);
        // die();
        if (empty($buffer)) {
            echo 0;
        } else {
            echo $buffer;
            // $json = json_decode($buffer, true);
            // echo json_encode($json);
        }

    }

    public function get_batch_live_session(Request $request, $slug, $date)
    {
        //$batch_id = $slug;
        $user_id = $request->session()->has('user_id');
        if ($request->session()->get('user_role') == 0) {
            $assign = 'owner';
        } elseif ($request->session()->get('user_role') == 1) {
            $assign = 'admin';
        } else {
            $assign = 'teacher';
        }

        $live_list = \App\LiveTeacher::where([
            'teacher_id'=> $user_id
        ])->with(['teacherName:id,first_name'])->get();

        return view('dashboard.ajax.stdlivesession', compact('data', 'live_list', 'batch_id', 'user_id', 'assign'));
    }

    public function setFreeLiveForStudent($student, $liveid){
        $count = \App\bookedlivestd::where([
            'student_id'=> $student,
            'live_id'=> $liveid,
        ])->count();
        if($count == 0){
            $add = new \App\bookedlivestd;
            $add->student_id = $student;
            $add->live_id = $liveid;
            $add->is_paid = 'no';
            $add->amount = 0;
            $add->type = 'class';
            $add->save();
            return redirect('/students/batches/livebatchlist?success=1');
        }else{
            return redirect('/students/batches/livebatchlist?success=0');
        }

    }

    public function setPaidLiveForStudent($data){
        $decoded = base64_decode($data);
        $datta = explode('/',$decoded);

        $liveDetail = \App\LiveTeacher::find($datta['1']);
        //dd($liveDetail->teacher_id);
        //adding in payment database
        $addFee = new \App\FeeTeacher;
        $addFee->teacher_id = $liveDetail->teacher_id;
        $addFee->student_id = $datta['0'];
        $addFee->batch_type = 'LIVE';
        $addFee->batch_id = $datta['1'];
        $addFee->amount = $datta['4'];
        $addFee->pending = '0';
        $addFee->txn_id = $datta['3'];
        $addFee->order_id = $datta['2'];
        $addFee->plan_name = '';
        $addFee->activity = '';
        $addFee->payment_mode = '1';
        $addFee->paid_on = $datta['5'];
        $addFee->save();

        $add = new \App\bookedlivestd;
        $add->student_id = $datta['0'];
        $add->live_id = $datta['1'];
        $add->is_paid = 'yes';
        $add->amount = $datta['4'];
        $add->type = 'class';
        $add->save();

        return redirect('/students/batches/livebatchlist?success=1');
    }

    public function add_event_calender(Request $request, $text, $date){
        $user_id = $request->session()->get('user_id');
        //return $text;
        $add = new \App\InstCalendar;
        $add->user_id = $user_id;
        $add->user_type = 'admin';
        $add->content = $text;
        $add->date_alloted = $date;
        $rs = $add->save();
        if ($rs) {
            return '1';
        } else {
            return '0';
        }
    }

    public function get_event_calender(Request $request){
        $user_id = $request->session()->get('user_id');
        $elist = \App\InstCalendar::where([
            'user_id'=> $user_id
        ])->get();
        //return $count;
        $data = array();
        $count = 0;
        foreach($elist as $list){
            //print_r($list->user_id);
            $data[$count]['id'] = $list->id;
            $data[$count]['title'] = 'ad';
            $data[$count]['start'] = $list->date_alloted;
            $data[$count]['className'] = 'event-default';
            $count++;
        }
        return $data;
    }



    public function del_event_calender(Request $request,$slug){
        $user_id = $request->session()->has('user_id');
        if ($user_id) {
            $flight = \App\InstCalendar::where([
                'id' => $slug,
            ])->delete();
            return 1;
        } else {
            return 0;
        }
    }

    public function del_batch(Request $request,$slug){
        $user_id = $request->session()->get('user_id');
        if ($user_id) {
            $flight = \App\BatchTeacher::where([
                'id' => $slug,
            ])->delete();
            return 1;
        } else {
            return 0;
        }
    }





    // public function get_program_from_batch($id)
    // {
    //     $data = \App\InstBatch::where(['id' => $id, 'status' => 1])->get();
    //     echo "<option value='-' disabled selected>Select First</option>";
    //     foreach ($data as $dt) {
    //         echo "<option id='cls_" . $dt->id . "' value='" . $dt->id . "' data-name='" . $dt->name . "'>" . $dt->name . "</option>";
    //     }
    // }

    
    // // public function chk_prog_name($slug)
    // // {
    // //     $chk_prog = \App\InstBatch::where([
    // //         'title' => $slug,
    // //     ])->first();
    // //     return $chk_prog;
    // // }


}

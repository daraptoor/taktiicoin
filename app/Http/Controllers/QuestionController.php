<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function listques(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Assignment List";
        // Getting data from database
        // if(){

        //}
        $ques_list = \App\QuestionTeacher::where([
            'type' => 'inst',
            'teacher_id' => $user_id
            ])->with(['batchinfo:id,title'])->get();
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.ques.listques',compact('title','ques_list','ques_data','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function viewmultiques(Request $request, $slug){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $inst_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "MUltiple Question List";
        $id = base64_decode($slug);
       // dd($q_id);
        // Getting data from database
        $viewques_list = \App\QuestionTeacher::where([
           
            ])->with(['batchinfo:id,title'])->get();
       // dd($ques_data);
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.ques.viewques',compact('title','viewques_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function addques(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
    
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Assignment Add";
    
        $batch_list = \App\BatchTeacher::where([
    
            ])->get(); 
        $courses_list = \App\Courses::where([
                
            ])->get();    
        
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.ques.addques',compact('title','batch_list','courses_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }
    //for functions without pages only
    public function add_new_ques(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $inst_id = $request->session()->get('user_id');
        $input = $request->all();
        // echo $input['ques_1'][0];
        // dd($input);
       
        // data collection
        
        $teacher_id = $inst_id;
        $batch_id = $input['batch_id'];
        $ans_type = $input['ans_type'];
        //$time_stamp = time().str_random(3);
        $date = date('Y-m-d H:i:s');
        $total_question = $input['total_question'][0];
      
        if($ans_type == 'multiple'){
            for ($i=1; $i <= $total_question; $i++){
    
                $insertData = new \App\QuestionTeacher;
                $insertData->teacher_id = $teacher_id;
                $insertData->batch_id = $batch_id;
                $insertData->type =  $input['type'];
                $insertData->question = $input['ques_'.$i][0];
                $insertData->ans_type = $ans_type;
                $insertData->option1 = $input['option_a_'.$i][0];
                $insertData->option2 = $input['option_b_'.$i][0];
                $insertData->option3 = $input['option_c_'.$i][0];
                $insertData->option4 = $input['option_d_'.$i][0];
                $insertData->answer = $input['ans_'.$i][0];
                $insertData->class_id =  base64_decode($input['class_id']);
                $insertData->class_name =  $input['class_name'];
                $insertData->subject_id = base64_decode($input['subject_id']);
                $insertData->subject_name = $input['subject_name'];
                $insertData->topic = $input['topic'];
                $insertData->time_stamp = time().str_random(3);
                $insertData->created = $date;
                $insertData->save(); 
                           
            }
            //die();
            $request->session()->flash('success', 'New Assignment Added Successfully!');
            return redirect('/ques/listques');
    
        }elseif($ans_type == 'single'){
            for ($i=1; $i <= $total_question; $i++){
    
                $insertData = new \App\QuestionTeacher;
                $insertData->teacher_id = $teacher_id;
                $insertData->batch_id = $batch_id;
                $insertData->question = $input['sin_ques'][$i-1];
                $insertData->ans_type = $ans_type;
                $insertData->class_id =  base64_decode($input['class_id']);
                $insertData->class_name =  $input['class_name'];
                $insertData->subject_id = base64_decode($input['subject_id']);
                $insertData->subject_name = $input['subject_name'];
                $insertData->topic = $input['topic'];
                $insertData->time_stamp = time().str_random(3);
                $insertData->created = $date;
                $insertData->save();
            
            }
    
                $request->session()->flash('success', 'New Assignment Added Successfully!');
                return redirect('/ques/listques');
    
        
        }else{
                $request->session()->flash('error', 'Unable to add New Assignment Please try again!!');
                return redirect('/ques/addques');
        }
    
    }
    

}

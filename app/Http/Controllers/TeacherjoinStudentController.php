<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherjoinStudentController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function join_student(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Join Student";

        // Getting data from database
        $live_list = \App\BatchTeacher::where([
            'teacher_id'=> $user_id
        ])->with(['studentList'])->get();
        $std_list = array();
        //    echo "<pre>";
        foreach($live_list as $list){
            $inList = $list['studentList'];
            foreach( $inList as $inList2){
                array_push($std_list,$inList2['student_id']);
            }
        }
        $std_list = array_unique($std_list);

        //echo "</pre>";
        //die();
        //dd($live_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.student.join_student',compact('title','std_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class TeacherBatchController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T' ){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Batch's List";
        // Getting data from database
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
        ])->get(); 
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.batches.list',compact('title','batch_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }
    // For Add Batch

    public function add_tbatch(Request $request){
        if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T' ){
            return redirect('/');
        }  
        $teacher_id = $request->session()->has('user_id');
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Batch";
        // geting data from database
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
            ])->get(); 
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get();

        
        // call view templates
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.batches.add',compact('title','success','error','batch_list','courses_list','teachers_list'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

     //for functions without pages only
     public function add_new_tbatch(Request $request){
        if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T'){
            return redirect('/');
        }
        $user_id = $request->session()->get('user_id');
        $input = $request->all();

        $name1 = preg_replace('/\s+/', '',$input['title']);
		$rand = rand(1000, 9999);
		$batch_code = substr($name1, 0, 4) . $rand;		
        $batch_code = strtoupper($batch_code);
        
      
        // data collectioTn
        $client = new \App\BatchTeacher;
        $client->teacher_id= $user_id;
        $client->title = $input['title'];
        $client->batch_start_date =  $input['batch_start_date'];
        $client->batch_end_date =  $input['batch_end_date'];
        $client->day_name =  implode(',',$input['day_name']);
        $client->batch_start_time =  $input['batch_start_time'];
        $client->batch_end_time =  $input['batch_end_time'];
        $client->duration =  $input['duration'];
        $client->batch_strength =  $input['batch_strength'];
        $client->batch_fee =  $input['batch_fee']; 
        $client->batch_demo =  $input['batch_demo'];   
        $client->course_id = base64_decode($input['course_id']);
        $client->course_name = $input['course_name'];
        $client->class_id =  base64_decode($input['class_id']);
        $client->class_name =  $input['class_name'];
        $client->subject_id = base64_decode($input['subject_id']);
        $client->subject_name = $input['subject_name'];
        $client->topic = $input['topic'];
        $client->batch_type = 'open';
        $client->batch_status =  $input['batch_status'];
        $client->batch_code =  $batch_code;
        $client->added_date =date('Y-m-d H:i:s');
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'New Batch Added Successfully!');
            return redirect('/teachers/batches/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Batch Please try again!!');
            return redirect('/teachers/batches/add');
        }
        print_r($data);
    }

    //For edit Batch
    public function edit_tbatch(Request $request, $slug){
    if(!$request->session()->has('user_id')  ||  $request->session()->get('user_type') != 'T'){
        return redirect('/');
    } 
    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Edit Batch";
    $batchid = base64_decode($slug);
    // geting data from database
    $batch_data = \App\BatchTeacher::find($batchid);
    $batch_list = \App\BatchTeacher::where([
        'teacher_id'=>$user_id
        ])->get(); 
    $courses_list = \App\Courses::where([
        'status'=>'1'
    ])->get();

    // call view templates
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashTeacher.batches.edit',compact('title','success','error','batch_list','courses_list','batch_data'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

    public function update_tbatch(Request $request){
        if(!$request->session()->has('user_id')  ||  $request->session()->get('user_type') != 'T'){
            return redirect('/');
        }
        $user_id = $request->session()->get('user_id');
        $input = $request->all();

        $name1 = preg_replace('/\s+/', '',$input['title']);
		$rand = rand(1000, 9999);
		$batch_code = substr($name1, 0, 4) . $rand;		
        $batch_code = strtoupper($batch_code);
        
      
        // data collectioTn
        $id = $input['b_id'];
        $client = \App\BatchTeacher::find($id);
        $client->teacher_id= $user_id;
        $client->title = $input['title'];
        $client->batch_start_date =  $input['batch_start_date'];
        $client->batch_end_date =  $input['batch_end_date'];
        $client->day_name =  implode(',',$input['day_name']);
        $client->batch_start_time =  $input['batch_start_time'];
        $client->batch_end_time =  $input['batch_end_time'];
        $client->duration =  $input['duration'];
        $client->batch_strength =  $input['batch_strength'];
        $client->batch_fee =  $input['batch_fee']; 
        $client->batch_demo =  $input['batch_demo'];   
        $client->course_id = base64_decode($input['course_id']);
        $client->course_name = $input['course_name'];
        $client->class_id =  base64_decode($input['class_id']);
        $client->class_name =  $input['class_name'];
        $client->subject_id = base64_decode($input['subject_id']);
        $client->subject_name = $input['subject_name'];
        $client->topic = $input['topic'];
        $client->batch_type = 'open';
        $client->batch_status =  $input['batch_status'];
        $client->batch_code =  $batch_code;
        $client->added_date =date('Y-m-d H:i:s');
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', ' Update Successfully!');
            return redirect('/teachers/batches/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Batch Please try again!!');
            return redirect('/teachers/batches/edit_tbatch');
        }
        print_r($data);
    }

    public function attendence(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Mark Student Attendence";
        $teacher_id_get = \App\TeacherMain::where(
            ['id'=> $user_id])->get(array('mobile'));
        $mobile = $teacher_id_get[0]->mobile;
        //getting data 
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
        ])->get();

        //dd($batch_list);
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.batches.attendence',compact('batch_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public static function chk_attendence($student_id, $batch_id, $date){
        $chk_count = \App\AttendenceTeacher::where([
            'student_id' => $student_id,
            'batch_id' => $batch_id,
            'date' => $date
        ])->get();
        return $chk_count;
    }

    public function mark_attendence(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $input = $request->all();
        $user_id =$request->session()->get('user_id');
        foreach($input['attend'] as $key => $list){

            $chk_count = \App\AttendenceTeacher::where([
                'student_id' => $key,
                'batch_id' => $input['batch_id'],
                'date' => $input['date']
            ])->count();

            if($chk_count > 0){
                $client = \App\AttendenceTeacher::where([
                    'student_id' => $key,
                    'teacher_id' => $user_id,
                    'batch_id' => $input['batch_id'],
                    'date' => $input['date']
                ])->update([
                    'student_id' => $key,
                    'teacher_id' => $user_id,
                    'date' => $input['date'],
                    'batch_id' => $input['batch_id'],
                    'status' => $list
                   
                ]);
            }else{
                $client = new \App\AttendenceTeacher;            
                $client->student_id = $key;
                $client->teacher_id = $user_id;
                $client->date = $input['date'];
                $client->batch_id = $input['batch_id'];
                $client->status = $list;
                $client->save();
            }
            
        }
        $request->session()->flash('success', 'Attendence Marked Successfully!');
        return redirect('/teachers/batches/attendence');
    }

    public function joinstudent(Request $request, $batch_id){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Joined student List";
        // Getting data from database
        $std_list =\App\ActiveBatchlistStudent::where([
           'batch_id'=>$batch_id
            ])->with(['students:id,first_name,email,mobile,gender,profile_pic'])->get();
      // dd($std_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.batches.joinstudent',compact('title','std_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

}

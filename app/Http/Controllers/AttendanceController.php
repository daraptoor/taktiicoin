<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    //
    public function index(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }
    
    public function student_mark(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Mark Student Attendance";
        //getting data 
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.attendence.studentMark',compact('batch_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public static function chk_attendence($student_id, $batch_id, $date){
        $chk_count = \App\Attendence::where([
            'student_id' => $student_id,
            'batch_id' => $batch_id,
            'attend_date' => $date
        ])->get();
        return $chk_count;
    }

    public function mark_attendence(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
        foreach($input['attend'] as $key => $list){

            $chk_count = \App\Attendence::where([
                'student_id' => $key,
                'batch_id' => $input['batch_id'],
                'attend_date' => $input['attend_date']
            ])->count();

            if($chk_count > 0){
                $client = \App\Attendence::where([
                    'student_id' => $key,
                    'batch_id' => $input['batch_id'],
                    'attend_date' => $input['attend_date']
                ])->update([
                    'student_id' => $key,
                    'attend_date' => $input['attend_date'],
                    'batch_id' => $input['batch_id'],
                    'assistant' => $input['assistant'],
                    'attendence_by' => $input['attendence_by'],
                    'status' => $list,
                    'inst_id' => $request->session()->get('user_id'),
                    'owner_id' => ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id')
                ]);
            }else{
                $client = new \App\Attendence;            
                $client->student_id = $key;
                $client->attend_date = $input['attend_date'];
                $client->batch_id = $input['batch_id'];
                $client->assistant = $input['assistant'];
                $client->attendence_by = $input['attendence_by'];
                $client->status = $list;
                $client->inst_id = $request->session()->get('user_id');
                $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
                $client->save();
            }
            
        }
        $request->session()->flash('success', 'Attendence Marked Successfully!');
        return redirect('/attendance/student_mark');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Student Attendance";
        //getting data 
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.attendence.list',compact('batch_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;


class AdminController extends Controller
{
    
    public function index(Request $request){
        if(!$request->session()->has('user_id')|| $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }
    public function admin(Request $request){
        if(!$request->session()->has('user_id')|| $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Institute's List";
        //geting data from table
        $admin_list = \App\Admin::where([
            'user_role'=>'2',
           ['status','!=','2'],
            'parrent'=>$user_id
        ])->get();        
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.admin.list',compact('admin_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_admin(Request $request){
        if(!$request->session()->has('user_id')|| $request->session()->get('user_role') != 1){
            return redirect('/');
        }
         //checking session data
         if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Institute";
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.admin.addAdmin',compact('title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }



    //for functions without pages only
    public function add_new_admin(Request $request){
        if(!$request->session()->get('user_id')|| $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
        
        //image uploading code
        $image = $request->file('image');
        $image_name = $input['username'].'_'.time().'.'.$image->getClientOriginalExtension();
        $image_path = 'public/uploads/branch_img/'.$image_name;
        $destinationPath = public_path('/uploads/branch_img');
        $image->move($destinationPath, $image_name);
        // data collection
        $client = new \App\Admin;
        $client->username = $input['username'];
        $client->password = md5($input['password']);
        $client->inst_name =  $input['inst_name'];
        $client->inst_email =  $input['inst_email'];
        $client->inst_mobile =  $input['inst_mobile'];
        $client->parrent = $request->session()->get('user_id');
        $client->address =  $input['address'];
        $client->lat =  $input['lat'];
        $client->lng =  $input['lng'];
        $client->ba_fname =  $input['ba_fname'];
        $client->ba_lname =  $input['ba_lname'];
        $client->logo =  $image_path;
        $client->inst_type =  'admin';
        $client->user_role = 2;
        $data = $client->save();
      //  dd($data);
        if($data == 1){
            $request->session()->flash('success', 'New Admin Added Successfully!');
            return redirect('/admin/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Admin Please try again!!');
            return redirect('/admin/add_admin');
        }
        print_r($data);
    }
    //for edit code
    public function edit_admin(Request $request, $slug){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }

        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Edit Institute";
        $insid = base64_decode($slug);
        //getting data 
        $inst_data = \App\Admin::find($insid);

        //dd($inst_data);
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.admin.editAdmin',compact('inst_data','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }


    //for update code
    public function update_admin(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
        //dd($input);       

        // //image uploading code
        $image = $request->file('image');
        if($image){
            $image_name = $input['username'].'_'.time().'.'.$image->getClientOriginalExtension();
            $image_path = 'public/uploads/branch_img/'.$image_name;
            $destinationPath = public_path('/uploads/branch_img');
            $image->move($destinationPath, $image_name);
        }else{
            $image_path = $input['image_exist'];
        }

        // data collection
        $id = $input['user_id'];
        $client = \App\Admin::find($id);
        $client->username = $input['username'];
        // if(isset($input['password'])){
        //     $client->password = md5($input['password']);
        ////}
      
        $client->inst_name =  $input['inst_name'];
        $client->inst_email =  $input['inst_email'];
        $client->inst_mobile =  $input['inst_mobile'];
        //$client->parrent = $request->session()->get('user_id');
        $client->parrent = 0;
        $client->address =  $input['address'];
        $client->lat =  $input['lat'];
        $client->lng =  $input['lng'];
        $client->ba_fname =  $input['ba_fname'];
        $client->ba_lname =  $input['ba_lname'];
        $client->logo =  $image_path;
        $client->inst_type =  'admin';
       // $client->user_role = 2;
        $client->user_role = 1;
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'Update Admin Successfully!');
            return redirect('/admin/details/');
        }else{
            $request->session()->flash('error', 'Unable to Update Admin Please try again!!');
            return redirect('/admin/edit_admin/'.base64_decode($id));
        }
        print_r($data);
    }

        public function single_institute(Request $request){
            if(!$request->session()->has('user_id')|| $request->session()->get('user_role') != 1){
                return redirect('/');
            }
            //checking session data
            if ($request->session()->has('error')) {
                $error = $request->session()->get('error');
                $request->session()->forget('error');
            }else{
                $error = '';
            }
            if ($request->session()->has('success')) {
                $success = $request->session()->get('success');
                $request->session()->forget('success');
            }else{
                $success = '';
            }
            //Defining variables
            $user_id = $request->session()->get('user_id');
            $username = $request->session()->get('user_name');
            $inst_name = $request->session()->get('inst_name');
            $user_role = $request->session()->get('user_role');
            $inst_logo = $request->session()->get('logo');
            $title = "Institute Details";
            //geting data from table
            $inst_details = \App\Admin::where([
                'id'=>$user_id,
                ['status','!=','2']
            ])->get();
            
           // dd($inst_details);
            //passing data to pages
            $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
            $v2 = view('dashboard.admin.details',compact('inst_details','title','error','success'));
            $v3 = view('common.footer');
            return $v1.$v2.$v3;
        }
    


        
}

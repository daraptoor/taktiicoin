<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StarAchieverController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "StarAchiever";
        //geting data from table
        $achiever = \App\StarAchiever::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.starachiever.list',compact('achiever','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }


    public function add(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add new StarAchiever box";
        //getting data 
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.starachiever.add',compact('title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }
    public function add_achiever(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all(); 
         //image uploading code
         $image = $request->file('image');
         $image_name = $input['achiever_name'].'_'.time().'.'.$image->getClientOriginalExtension();
         $image_path = 'public/uploads/branch_img/'.$image_name;
         $destinationPath = public_path('/uploads/branch_img');
         $image->move($destinationPath, $image_name);

        //print_r($image_path);
        $client = new \App\StarAchiever;
      //  $client->achiever_pics = $input['achiever_pics'];
        $client->achiever_name = $input['achiever_name'];
        $client->achievement =  $input['achievement'];
        $client->achiever_pics =  $image_path;
        $client->status = '1';
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $data = $client->save();
    
    
    if($data == 1){
        $request->session()->flash('success', 'New StarAchiever Added Successfully!');
        return redirect('/starachiever/list');
    }else{
        $request->session()->flash('error', 'Unable to add New StarAchiever Please try again!!');
        return redirect('/starachiever/add');
    }
    print_r($data);


    }
}

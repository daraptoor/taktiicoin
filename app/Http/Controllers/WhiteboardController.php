<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WhiteboardController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function datesheetlist(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Datesheet List";
        //geting data from table
        $datesheet_list = \App\Datesheet::where([
            'inst_id'=>$user_id
            ])->get();
      
       // dd($rating_list);       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.whiteboard.datesheetlist',compact('datesheet_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    
    public function assignmentlist(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Assignmet List";
        //geting data from table
        $assignment_list = \App\Assignment::where([
            'inst_id'=>$user_id
            ])->get();
      
       // dd($rating_list);       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.whiteboard.assignmentlist',compact('assignment_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function videoslist(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "videos List";
        //geting data from table
        $videos_list = \App\videos::where([
            'inst_id'=>$user_id
            ])->get();
      
       // dd($rating_list);       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.whiteboard.videoslist',compact('videos_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function notificationslist(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Notification List";
        //geting data from table
        $notification_list = \App\Notification::where([
            'inst_id'=>$user_id
            ])->get();
      
       // dd($rating_list);       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.whiteboard.notificationslist',compact('notification_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }
}


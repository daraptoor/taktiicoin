<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Promotion Box";
        //geting data from table
        $promotion_box = \App\InstPromotion::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.promotion.list',compact('promotion_box','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }


    public function add(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add new Promotion box";
        //getting data 
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.promotion.add',compact('title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }
    public function add_promotion(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all(); 
        //image uploading code
        $img = $request->file('image');
        
        foreach($img as $image){
            if($image){
                $image_name = $input['title'].'_'.time().'.'.$image->getClientOriginalExtension();
                $image_path = 'public/uploads/treasure_box/'.$image_name;
                $destinationPath = public_path('/uploads/treasure_box');
                $image->move($destinationPath, $image_name);
            }else{
                $image_path = $input['image_exist'];
            }
            //print_r($image_path);
            $client = new \App\InstPromotion;
            $client->title = $input['title'];
            $client->file_desc = $input['file_desc'];
            $client->file_type =  $input['file_type'];
            $client->file_url =  $image_path;
            $client->status = '1';
            $client->inst_id = $request->session()->get('user_id');
            $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
            $data = $client->save();
        }
        
        if($data == 1){
            $request->session()->flash('success', 'New Promotion Added Successfully!');
            return redirect('/promotion/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Promotion Please try again!!');
            return redirect('/promotion/add');
        }
        print_r($data);


    }
}

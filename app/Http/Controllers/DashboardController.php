<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
       // dd($user_role);
        $inst_logo = $request->session()->get('logo');
        $title = "Dashboard";
        
        if($request->session()->get('user_type') == 'A'){
            $student_count = \App\InstStudent::where([
                'inst_id'=>$user_id,
                ['status','!=','2']
            ])->get()->count();
    
            $teacher_count = \App\Teacher::where([
                'inst_id'=>$user_id,
                ['status','!=','2']
            ])->get()->count();
    
            $batch_count = \App\InstBatch::where([
                'inst_id'=>$user_id,
                ['status','!=','2']
            ])->get()->count();
    
            $program_count = \App\Program::where([
                'inst_id'=>$user_id,
                ['status','!=','2']
            ])->get()->count();
    
            $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    
            $v2 = view('dashboard.reports.detail',compact('student_count','teacher_count','batch_count','program_count','title','error','success'));
    
            $v3 = view('common.footer');
            return $v1.$v2.$v3;
        }elseif($request->session()->get('user_type') == 'T'){
            if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T' ){
                return redirect('/');
            }  
            $teacher_id = $request->session()->has('user_id');
            //checking session data
            if ($request->session()->has('error')) {
                $error = $request->session()->get('error');
                $request->session()->forget('error');
            }else{
                $error = '';
            }
            if ($request->session()->has('success')) {
                $success = $request->session()->get('success');
                $request->session()->forget('success');
            }else{
                $success = '';
            }
            //Defining variables
            $user_id = $request->session()->get('user_id');
            $username = $request->session()->get('user_name');
            $inst_name = $request->session()->get('inst_name');
            $user_role = $request->session()->get('user_role');
            $inst_logo = $request->session()->get('logo');
            $title = "DashBoard";

            $student_count = \App\BatchTeacher::where([
                'teacher_id'=> $user_id
            ])->with(['studentList'])->get()->count();
          
            $treasure_count = \App\TreasureTeacher::where([
                'teacher_id'=>$user_id      
            ])->get()->count();
    
            $batch_count = \App\BatchTeacher::where([
                'teacher_id'=>$user_id
            ])->get()->count();
    

            $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));

            $v2 = view('dashTeacher.dashboard',compact('student_count','treasure_count','batch_count','title','error','success'));
    
            $v3 = view('common.footer');
            return $v1.$v2.$v3;
        }elseif($request->session()->get('user_type') == 'S'){
            $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));

            $v2 = view('dashboard.index');
    
            $v3 = view('common.footer');
            return $v1.$v2.$v3;
        }

    }

}

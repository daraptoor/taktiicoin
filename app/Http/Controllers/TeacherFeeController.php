<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherFeeController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
}

public function list(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    }
    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Fee's Detail";
    //geting data from table
    $fee_list = \App\FeeTeacher::where([
        'teacher_id'=>$user_id
    ])->with(['batchinfo:id,title'])->with(['studentinfo:id,first_name,mobile'])->get(); 
    //dd($fee_list); 
    //passing data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashTeacher.fee.list',compact('fee_list','title','error','success'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;

}
public function add(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    }
    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }

    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Add Fee's";
    //getting data 

    $batch_list = \App\BatchTeacher::where([
        'teacher_id'=>$user_id,
    ])->get();

    //passing data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashTeacher.fee.add',compact('title','batch_list','error','success'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;

}
public function add_fee(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    }

    $teacher_id = $request->session()->get('user_id');
    $input = $request->all(); 
    //dd($input);       
    $date = date('Y-m-d H:i:s');
    // data collection
    $client = new \App\FeeTeacher;
    $client->teacher_id = $teacher_id;
    $client->student_id = $input['student_id'];
    $client->batch_type = $input['batch_type'];
    $client->batch_id =  $input['batch_id'];
    $client->amount =  $input['amount'];
    $client->pending =  $input['pending'];
    $client->payment_mode =  $input['payment_mode'];
    $client->paid_on =  $input['paid_on'];
    $client->created_at =  $date;

    $data = $client->save();
    if($data == 1){
        $request->session()->flash('success', 'New Fee Added Successfully!');
        return redirect('/teachers/fee/list');
    }else{
        $request->session()->flash('error', 'Unable to add New fee Please try again!!');
        return redirect('/teachers/fee/add');
    }
    print_r($data);
}

public function editfee(Request $request,$slug){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    }
    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }

    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Edit Fee's";
    //getting data 
    $fee_data = \App\FeeTeacher::find($slug);

    $batch_list = \App\BatchTeacher::where([
        'teacher_id'=>$user_id,
    ])->get();

    
    //passing data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashTeacher.fee.edit',compact('title','fee_data','batch_list','error','success'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;

}

public function update_fees(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    }

    $teacher_id = $request->session()->get('user_id');
    $input = $request->all(); 
    //dd($input);       
    $date = date('Y-m-d H:i:s');
    // data collection
    $id = $input['id'];
    $client = \App\FeeTeacher::find($id);
    $client->teacher_id = $teacher_id;
    $client->student_id = $input['student_id'];
    $client->batch_type = $input['batch_type'];
    $client->batch_id =  $input['batch_id'];
    $client->amount =  $input['amount'];
    $client->pending =  $input['pending'];
    $client->payment_mode =  $input['payment_mode'];
    $client->paid_on =  $input['paid_on'];
    $client->created_at =  $date;

    $data = $client->save();
    if($data == 1){
        $request->session()->flash('success', 'New Fee Update Successfully!');
        return redirect('/teachers/fee/list');
    }else{
        $request->session()->flash('error', 'Unable to Update fee Please try again!!');
        return redirect('/teachers/fee/edit');
    }
    print_r($data);
}


}

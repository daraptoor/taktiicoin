<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherInstituteController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T' ){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Institute List";
        $mobile_get = \App\TeacherMain::where(
            ['id'=> $user_id])->get(array('mobile'));
        $mobile = $mobile_get[0]->mobile;
        // Getting data from database
        $institute_list = \App\Teacher::where([
            'mobile'=> $mobile,
            ['status','!=','2']
            ])->get();   
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.institute.list',compact('title','institute_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function view(Request $request, $slug){
        if(!$request->session()->has('user_id') ||  $request->session()->get('user_type') != 'T' ){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Institute view";
        $insid = base64_decode($slug);
        $mobile_get = \App\TeacherMain::where(
            ['id'=> $user_id])->get(array('mobile'));
        $mobile = $mobile_get[0]->mobile;
        // Getting data from database
        $institute_list = \App\Teacher::where([
            'mobile'=> $mobile
            ])->get();   
        $inst_data = \App\Teacher::find($insid);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.institute.view',compact('title','institute_list','inst_data','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function attendence(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Mark Student Attendence";
        $teacher_id_get = \App\TeacherMain::where(
            ['id'=> $user_id])->get(array('mobile'));
        $mobile = $teacher_id_get[0]->mobile;
        //getting data 
        $user_inst_id = \App\Teacher::where([
            'mobile'=> $mobile
            ])->get(array('id'));
        $user_inst_ids = $user_inst_id[0]->id;

        $batch_list = \App\InstBatch::where([
            'teacher_id'=>$user_inst_ids
        ])->get(); 

        //dd($batch_list);
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.institute.attendence',compact('batch_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public static function chk_attendence($student_id, $batch_id, $date){
        $chk_count = \App\Attendence::where([
            'student_id' => $student_id,
            'batch_id' => $batch_id,
            'attend_date' => $date
        ])->get();
        return $chk_count;
    }

    public function mark_attendence(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $input = $request->all();
        foreach($input['attend'] as $key => $list){

            $chk_count = \App\Attendence::where([
                'student_id' => $key,
                'batch_id' => $input['batch_id'],
                'attend_date' => $input['attend_date']
            ])->count();

            if($chk_count > 0){
                $client = \App\Attendence::where([
                    'student_id' => $key,
                    'batch_id' => $input['batch_id'],
                    'attend_date' => $input['attend_date']
                ])->update([
                    'student_id' => $key,
                    'attend_date' => $input['attend_date'],
                    'batch_id' => $input['batch_id'],
                    'assistant' => $input['assistant'],
                    'attendence_by' => $input['attendence_by'],
                    'status' => $list,
                    'inst_id' => $request->session()->has('user_id'),
                    'owner_id' => ($request->session()->has('owner_id') == 0)? $request->session()->has('user_id') : $request->session()->has('owner_id')
                ]);
            }else{
                $client = new \App\Attendence;            
                $client->student_id = $key;
                $client->attend_date = $input['attend_date'];
                $client->batch_id = $input['batch_id'];
                $client->assistant = $input['assistant'];
                $client->attendence_by = $input['attendence_by'];
                $client->status = $list;
                $client->inst_id = $request->session()->get('user_id');
                $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
                $client->save();
            }
            
        }
        $request->session()->flash('success', 'Attendence Marked Successfully!');
        return redirect('/teachers/institute/attendence');
    }


}

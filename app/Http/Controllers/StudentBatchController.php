<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StudentBatchController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
}

public function batchlist(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    } 
   // print_r($request->session());

    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
  //  dd($user_id);
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = " All Live Batches";
    // geting data from database
    $activestd_list = \App\LiveTeacher::where([
        ['liveDate','>=',date('Y-n-d')],
        ])->with('teacherName:id,first_name')->get();
    //dd($activestd_list);
    // Set data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashStudent.batches.batchlist',compact('title','activestd_list','user_id','success','error'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

public function livebatchlist(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    } 

    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
   // dd($user_id);
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "My Live Class";
    // geting data from database
    $activestd_list = \App\bookedlivestd::where([
     'student_id'=>$user_id
   
    ])->with(['livedetails'])->get();

   //dd($activestd_list);
    // Set data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashStudent.batches.livebatchlist',compact('title','username','activestd_list','success','error'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

public function join_student(Request $request){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    } 

    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
   // dd($user_id);
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Join Student";
    // geting data from database
 

   //dd($activestd_list);
    // Set data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashStudent.batches.join',compact('title','success','error'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherTreasureController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Treasure Box";
        //geting data from table
        $treasure_box = \App\TreasureTeacher::where([
            'teacher_id'=>$user_id     
        ])->get();
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.treasure.list',compact('treasure_box','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }


    public function add(Request $request){
        if(!$request->session()->get('user_id')){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add new Treasure";
        //getting data 
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
        ])->get();
        $courses_list = \App\Courses::where([
        ])->get(); 
       // dd($courses_list);

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.treasure.add',compact('title','batch_list','courses_list','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }
    public function add_treasure(Request $request){
        if(!$request->session()->get('user_id')){
            return redirect('/');
        }
        $teacher_id = $request->session()->get('user_id');
        $input = $request->all(); 
      // dd($input);
        //image uploading code
        $img = $request->file('image');
        $date = date('Y-m-d H:i:s');
        $type =  $input['type'];
        foreach($img as $image){
            if($image){
                $image_name = $input['topic'].'_'.time().'.'.$image->getClientOriginalExtension();
                $image_path = url('/').'/public/uploads/treasure_box/'.$image_name;
                $destinationPath = public_path('/uploads/treasure_box');
                $image->move($destinationPath, $image_name);
            }else{
                $image_path = $input['image_exist'];
            }
        
        if($type == 'doc'){
            //print_r($image_path);
                $client = new \App\TreasureTeacher;
                $client->teacher_id = $teacher_id;
                $client->class_id =  base64_decode($input['class_id']);
                $client->class_name =  $input['class_name'];
                $client->subject_id = base64_decode($input['subject_id']);
                $client->subject_name = $input['subject_name'];
                $client->chapter_name = $input['chapter_name'];
                $client->topic = $input['topic'];
                $client->type =  $input['type'];
                $client->pdf_heading =  $input['pdf_heading'];
                $client->pdf_desc =  $input['pdf_desc'];
                $client->created = $date;
                $client->pdf_img =  $image_path;
                $client->save();

        $request->session()->flash('success', 'New Treasure Added Successfully!');
        return redirect('/teachers/treasure/list');
        
        }elseif($type == 'pdf'){
                $client = new \App\TreasureTeacher;
                $client->teacher_id = $teacher_id;
                $client->class_id =  base64_decode($input['class_id']);
                $client->class_name =  $input['class_name'];
                $client->subject_id = base64_decode($input['subject_id']);
                $client->subject_name = $input['subject_name'];
                $client->chapter_name = $input['chapter_name'];
                $client->topic = $input['topic'];
                $client->type =  $input['type'];
                $client->pdf_heading =  $input['pdf_heading'];
                $client->pdf_desc =  $input['pdf_desc'];
                $client->created = $date;
                $client->pdf_img =  $image_path;
                $client->save();
    
            $request->session()->flash('success', 'New Treasure Added Successfully!');
            return redirect('/teachers/treasure/list');

        }elseif($type == 'image'){
                $client = new \App\TreasureTeacher;
                $client->teacher_id = $teacher_id;
                $client->class_id =  base64_decode($input['class_id']);
                $client->class_name =  $input['class_name'];
                $client->subject_id = base64_decode($input['subject_id']);
                $client->subject_name = $input['subject_name'];
                $client->chapter_name = $input['chapter_name'];
                $client->topic = $input['topic'];
                $client->type =  $input['type'];
                $client->img_heading =  $input['img_heading'];
                $client->img_desc =  $input['img_desc'];
                $client->created = $date;
                $client->pdf_img =  $image_path;
                $client->save();
            
            $request->session()->flash('success', 'New Treasure Added Successfully!');
            return redirect('/teachers/treasure/list');

        }elseif($type == 'Video'){
                $client = new \App\TreasureTeacher;
                $client->teacher_id = $teacher_id;
                $client->class_id =  base64_decode($input['class_id']);
                $client->class_name =  $input['class_name'];
                $client->subject_id = base64_decode($input['subject_id']);
                $client->subject_name = $input['subject_name'];
                $client->chapter_name = $input['chapter_name'];
                $client->topic = $input['topic'];
                $client->type =  $input['type'];
                $client->img_heading =  $input['img_heading'];
                $client->img_desc =  $input['img_desc'];
                $client->created = $date;
                $client->video_url =  $image_path;
                $client->save();
            
            $request->session()->flash('success', 'New Treasure Added Successfully!');
            return redirect('/teachers/treasure/list');

        }else{
            $request->session()->flash('error', 'Unable to add New Treasure Please try again!!');
                    return redirect('/teachers/treasure/add');
            }

    }
}

    //For Edit code
    // public function edit_treasure(Request $request, $slug){
    //     if(!$request->session()->has('user_id') ){
    //         return redirect('/');
    //     }
    //     //checking session data
    //     if ($request->session()->has('error')) {
    //         $error = $request->session()->get('error');
    //         $request->session()->forget('error');
    //     }else{
    //         $error = '';
    //     }
    //     if ($request->session()->has('success')) {
    //         $success = $request->session()->get('success');
    //         $request->session()->forget('success');
    //     }else{
    //         $success = '';
    //     }
    //     //Defining variables
    //     $user_id = $request->session()->get('user_id');
    //     $username = $request->session()->get('user_name');
    //     $inst_name = $request->session()->get('inst_name');
    //     $user_role = $request->session()->get('user_role');
    //     $inst_logo = $request->session()->get('logo');
    //     $title = " Edit Treasure Box";
    //     $treid = base64_decode($slug);
    //     // Getting data from database
    //     $treasure_data = \App\TreasureTeacher::find($treid);
       
    //     $batch_list = \App\InstBatch::where([
    //         'inst_id'=>$user_id,
    //         'status'=>'1'
    //     ])->get(); 

    //     //passing data to pages
    //     $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    //     $v2 = view('dashTeacher.treasure.edit',compact('title','error','success','batch_list','treasure_data','treid'));
    //     $v3 = view('common.footer');
    //     return $v1.$v2.$v3;

    // }

    // //for update
    // public function update_treasure(Request $request){
    //     if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
    //         return redirect('/');
    //     }
    //     $input = $request->all(); 

    //     //image uploading code
    //     $image = $request->file('image');
    //         if($image){
    //             $image_name = $input['batch_id'].'_'.time().'.'.$image->getClientOriginalExtension();
    //             $image_path = 'public/uploads/treasure_box/'.$image_name;
    //             $destinationPath = public_path('/uploads/treasure_box');
    //             $image->move($destinationPath, $image_name);
    //         }else{
    //             $image_path = $input['image_exist'];
    //         }

    //         //print_r($image_path);
    //         $id = $input['main_id'];
    //         $client = \App\TreasureTeacher::find($id);
    //         $client->title = $input['title'];
    //         $client->file_desc = $input['file_desc'];
    //         $client->file_type =  $input['file_type'];
    //         $client->file_url =  $image_path;
    //         $client->batch_id =  $input['batch_id'];
    //         $client->batch_name =  $input['batch_name'];
    //         $client->amount =  $input['amount'];
    //         $client->is_paid =  $input['is_paid'];
    //         $client->status = '1';
    //         $client->inst_id = $request->session()->has('user_id');
    //         $client->owner_id = ($request->session()->has('owner_id') == 0)? $request->session()->has('user_id') : $request->session()->has('owner_id');
    //         $data = $client->save();
    
        
    //     if($data == 1){
    //         $request->session()->flash('success', 'Update Successfully!');
    //         return redirect('/teachers/treasure/list');
    //     }else{
    //         $request->session()->flash('error', 'Unable to Update Please try again!!');
    //         return redirect('/teachers/treasure/edit');
    //     }
    //     print_r($data);


    // }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Teacher;
use \App\Http\Controllers\ActionController;

class TeacherController extends Controller
{   
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }
    public function teachers(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Teacher's List";
        //geting data from table
        $teachers_list = \App\Teacher::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.teacher.list',compact('teachers_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_teacher(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Teacher";
        // Getting data from database
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get();
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.teacher.addTeacher',compact('title','courses_list'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }


    //for functions without pages only
    public function add_new_teacher(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        $input = $request->all();

        // $subjectss = $input['prog_subject'];
        // $sub = array();
        // foreach($subjectss as $subz){
        //     $sub[] = base64_decode($subz);
        // }
        // $subs = implode(',',$sub);
        $inst_name = $request->session()->get('inst_name');
        //dd($input);
        //image uploading code
       
        $image = $request->file('image');
        
        if($image){
        $image_name = $input['number'].'_'.time().'.'.$image->getClientOriginalExtension();
        $image_path = 'public/uploads/teacher_img/'.$image_name;
        $destinationPath = public_path('/uploads/teacher_img');
        $image->move($destinationPath, $image_name);
    }else{
        $image_path = $input['image_exist'];
        }      
    
      
        // data collection
        $client = new \App\Teacher;
        $client->mobile = $input['number'];
       // $client->password = md5($input['password']);
        $client->first_name =  $input['first_name'];
        $client->last_name =  $input['last_name'];
        $client->email =  $input['email'];
        $client->teacher_type =  $input['teacher_type'];
        $client->gender =  $input['gender'];
        $client->teacher_salary =  $input['teacher_sal'];
        // $client->course_id = base64_decode($input['prog_courses']);
        // $client->course_name = $input['prog_course_name'];
        // $client->class_id =  base64_decode($input['prog_class']);
        // $client->class_name =  $input['prog_class_name'];
        // $client->subject_id =  $subs;
        // $client->subject_name =  $input['prog_subject_name'];
        $client->experience =  $input['experience'];
       // $client->video_url =  '0';
        $client->address =  $input['address'];
        $client->join_date =  $input['join_date'];
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $client->image =  $image_path;
        $client->status = '1';
        $data = $client->save();
        if($data == 1){
            ActionController::sendSms($input['number'],'Dear '.$input["first_name"].' '.$input["last_name"].', You are added as a Teacher in Institute ('.$inst_name.'). Please login '.url('/'));
            $request->session()->flash('success', 'New Admin Added Successfully!');
            return redirect('/teacher/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Admin Please try again!!');
            return redirect('/teacher/add');
        }
        
    }

    //Edit Teacher  Code
    public function edit_teacher(Request $request, $slug){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }   
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
       //Defining variables
       $user_id = $request->session()->has('user_id');
       $username = $request->session()->get('user_name');
       $inst_name = $request->session()->get('inst_name');
       $user_role = $request->session()->get('user_role');
       $inst_logo = $request->session()->get('logo');
       $title = "Edit Teacher";
       $tid = base64_decode($slug);
        // Getting data from database  
        $teacher_data = \App\Teacher::find($tid);
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get();
        $teachers_list = \App\Teacher::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();    
       //passing data to pages
       $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
       $v2 = view('dashboard.teacher.editTeacher',compact('teacher_data','teachers_list','courses_list','title'));
       $v3 = view('common.footer');
       return $v1.$v2.$v3;
   }

   //For Update Teacher Code

   public function update_teacher(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
        //dd($input);
        //image uploading code
         $image = $request->file('image');
        if($image){
            $image_name = $input['number'].'_'.time().'.'.$image->getClientOriginalExtension();
            $image_path = 'public/uploads/teacher_img/'.$image_name;
            $destinationPath = public_path('/uploads/teacher_img');
            $image->move($destinationPath, $image_name);
        }else{
             $image_path = $input['image_exist'];
        }
        // data collection
        $id = $input['tea_id'];
        $client = \App\Teacher::find($id);
      //  $client = new \App\Teacher;
        $client->mobile = $input['number'];
        // if(isset($input['password'])){
        //     $client->password = md5($input['password']);
        // }
        $client->first_name =  $input['first_name'];
        $client->last_name =  $input['last_name'];
        $client->email =  $input['email'];

        $client->teacher_type =  $input['teacher_type'];
        $client->teacher_salary =  $input['teacher_sal'];
        // $client->course_id = base64_decode($input['prog_courses']);
        // $client->course_name = $input['prog_courses_name'];
        // $client->class_id =  base64_decode($input['prog_class']);
        // $client->class_name =  $input['prog_class_name'];
        // $client->subject_id =  base64_decode($input['prog_subject']);
        // $client->subject_name = $input['prog_subject_name'];
        $client->address =  $input['address'];
        $client->join_date =  $input['join_date'];
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $client->image =  $image_path;
        $client->status = '1';
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'Update Teacher Successfully!');
            return redirect('/teacher/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Teacher Please try again!!');
            return redirect('/teacher/edit_teacher'.base64_decode($id));
        }
        print_r($data);
    }

    public function pay(Request $request,$slug){
        if(!$request->session()->get('user_id')){
            return redirect('/');
        }
        //Defining variables'
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Pay Teacher";
        $tid = base64_decode($slug);
        // Getting data from database
        $teacher_data = \App\Teacher::find($tid);

        $teacher = \App\TeacherMain::where([
            'mobile' => $teacher_data->mobile
        ])->get(array('id'));
        $teacher_id = $teacher[0]->id;

        $pay_info = \App\PayTeacher::where([
            'mobile' => $teacher_data->mobile
        ])->get();

        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.teacher.payTeacher',compact('title','teacher_data','teacher_id','pay_info'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    
    //for functions without pages only
    public function add_pay_teacher(Request $request){
        if(!$request->session()->get('user_id') ){
            return redirect('/');
        }
        $input = $request->all();
        $date = date('Y-m-d H:i:s');
        //
        
        // data collection
        $client = new \App\PayTeacher;
        $client->mobile = $input['number'];
        $client->teacher_id = $input['teacher_id'];
        $client->amount =  $input['amount'];
        $client->paid_date =  $input['paid_date'];
        $client->created_at = $date;
        $client->inst_id = $request->session()->get('user_id');
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'Update TeacherPay Successfully!');
           // return redirect('/teacher/pay/{{base64_encode($tlist->id)}}');
            return redirect('/teacher/list');
        }else{
            $request->session()->flash('error', 'Unable to Update TeacherPay Please try again!!');
            return redirect('/teacher/pay');
        }
        print_r($data);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BatchController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function batch(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Batch's List";
        // Getting data from database
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get(); 
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.batch.list',compact('title','batch_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_batch(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $user_id = $request->session()->has('user_id');
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Batch";
        // geting data from database
        $program_list = \App\Program::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        $teachers_list = \App\Teacher::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
        // call view templates
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.batch.addBatch',compact('title','program_list','teachers_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    //for functions without pages only
    public function add_new_batch(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $inst_id = $request->session()->get('user_id');
        $input = $request->all();
      
        // data collection
        $client = new \App\InstBatch;
        $client->title = $input['title'];
        $client->program_id = base64_decode($input['program_id']);
        $client->program_name = $input['prog_name'];
        $client->batch_start =  $input['batch_start'];
        $client->batch_end =  $input['batch_end'];
        $client->batch_days =  implode(',',$input['batch_days']);
        $client->batch_stime =  $input['batch_stime'];
        $client->batch_etime =  $input['batch_etime'];
        $client->teacher_id =  base64_decode($input['teacher_id']);
        $client->teacher_name =  $input['teacher_name'];
        $client->inst_id =  $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $inst_id : $request->session()->get('owner_id');
        $client->status =  '1';
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'New Batch Added Successfully!');
            return redirect('/batch/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Batch Please try again!!');
            return redirect('/batch/add');
        }
        print_r($data);
    }

    //For edit Batch
    public function edit_batch(Request $request, $slug){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    } 
    
    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Batch's Edit";
    $batid = base64_decode($slug);
    // Getting data from database
    $batch_data = \App\InstBatch::find($batid);
    $batch_list = \App\InstBatch::where([
        'inst_id'=>$user_id,
        ['status','!=','2']
    ])->get(); 

    $program_list = \App\Program::where([
        'inst_id'=>$user_id,
        'status'=>'1'
    ])->get(); 

    $teachers_list = \App\Teacher::where([
        'inst_id'=>$user_id,
        'status'=>'1'
    ])->get(); 

    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashboard.batch.editBatch',compact('title','batch_list','success','error','program_list','teachers_list','batch_data'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

    public function update_batch(Request $request){
    if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
        return redirect('/');
    }
    $inst_id  = $request->session()->get('user_id');
    $input = $request->all();
  
    // data collection
    $id = $input['b_id'];
    $client = \App\InstBatch::find($id);
    $client->title = $input['title'];
    $client->program_id = base64_decode($input['program_id']);
    $client->program_name = $input['prog_name'];
    $client->batch_start =  $input['batch_start'];
    $client->batch_end =  $input['batch_end'];
    $client->batch_days =  implode(',',$input['batch_days']);
    $client->batch_stime =  $input['batch_stime'];
    $client->batch_etime =  $input['batch_etime'];
    $client->teacher_id =  base64_decode($input['teacher_id']);
    $client->teacher_name =  $input['teacher_name'];
    $client->inst_id =  $request->session()->get('user_id');
    $client->owner_id = ($request->session()->get('owner_id') == 0)? $inst_id : $request->session()->get('owner_id');
    $client->status =  '1';
    $data = $client->save();
    if($data == 1){
        $request->session()->flash('success', 'Update Successfully!');
        return redirect('/batch/list');
    }else{
        $request->session()->flash('error', 'Unable to Update Batch Please try again!!');
        return redirect('/batch/edit_batch');
    }
    print_r($data);
}

public function joinstudent(Request $request, $batch_id){
    if(!$request->session()->has('user_id')){
        return redirect('/');
    } 

    //checking session data
    if ($request->session()->has('error')) {
        $error = $request->session()->get('error');
        $request->session()->forget('error');
    }else{
        $error = '';
    }
    if ($request->session()->has('success')) {
        $success = $request->session()->get('success');
        $request->session()->forget('success');
    }else{
        $success = '';
    }
    
    //Defining variables
    $user_id = $request->session()->get('user_id');
    $username = $request->session()->get('user_name');
    $inst_name = $request->session()->get('inst_name');
    $user_role = $request->session()->get('user_role');
    $inst_logo = $request->session()->get('logo');
    $title = "Joined student List";
  
    // Getting data from database
    
    $std_list = \App\StudentRel::where([
       // 'batch_id' => $batch_id,
        'assign_id'=> $user_id
        ])->with(['students','batchinfo'])->get();

   //dd($std_list);
    // Set data to pages
    $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
    $v2 = view('dashboard.batch.joinstudent',compact('title','std_list','success','error'));
    $v3 = view('common.footer');
    return $v1.$v2.$v3;
}

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParentController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function parent(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $inst_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Parent's List";
        // Getting data from database
        $parent_list = \App\InstStudent::where([
            'inst_id'=>$inst_id,
            ['status','!=','2']
        ])->get(); 
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.parent.list',compact('parent_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function update_parent_detail(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 
        $input = $request->all();
        //dd($input);

        $parent = \App\InstStudent::find($input['std_id']);
        $parent->mother_name = $input['mother_name'];
        $parent->father_name = $input['father_name'];
        $parent->parent_mob = $input['parent_mob'];
        $data = $parent->save();    
        if($data == 1){
            $request->session()->flash('success', 'Parent Detail Updated Successfully!');
            return redirect('/parent/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Parent Detail, Please check inserted data!!');
            return redirect('/parent/list');
        }


    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ActionController extends Controller
{
    // Sending SMS
    public static function sendSms($num,$msg){
        $curl = curl_init();
        $url = "https://www.selfenabler.com/sendSMSAPI?send_to=91".$num."&msg=".rawurlencode($msg)."&userid=2000178430&password=taktii123";
        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            return '0';
        } else {
            //print_r(json_decode($response));
            return '1';
        }
    }

    public static function teacherdetails($id, $data){
        $get_info = \App\TeacherMain::where([
            'id' => $id,
        ])->get($data);
        return $get_info;

    }

    public static function studentdetails($id, $dataa){
        $get_sinfo = \App\Student::where([
            'id' => $id,
        ])->get($dataa);
        return $get_sinfo;

    }

    

   


}

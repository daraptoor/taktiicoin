<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherLiveController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Live Session List";

        // Getting data from database
        $live_list = \App\LiveTeacher::where([
            'teacher_id'=> $user_id
            ])->with(['teacherName:id,first_name'])->get();
        //dd($live_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.live.list',compact('title','live_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Live Batch";

        // Getting data from database
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
            ])->get(); 
           //dd($batch_list); 
        $live_list = \App\LiveTeacher::where([
            'teacher_id'=> $user_id
            ])->with(['teacherName:id,first_name'])->get();
        //dd($live_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.live.add',compact('title','live_list','batch_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

     //for functions without pages only
     public function add_live_session(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        $user_id = $request->session()->get('user_id');
        $user_name = $request->session()->get('user_name');
        $input = $request->all();

        $url = "https://www.selfenabler.com/createMeetingApp?meetingName=".urlencode($input['title'])."&meetingID=".urlencode($input['batch_code'])."&attendeePW=12345678&moderatorPW=123456&presenter=".urlencode($user_name);
        //echo $url; die();
        $curl_handle=curl_init();
        curl_setopt($curl_handle,CURLOPT_URL,$url);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        //print_r($buffer);
        

        if (empty($buffer)){
            echo 0;
        }
        else{
            //echo json_encode($buffer);
            $json = json_decode($buffer, true);
            // echo $string = json_encode()."<br>";

            $client = new \App\LiveTeacher;
            $client->teacher_id= $user_id;
            $client->meetingID = $input['batch_code'];
            $client->title = $input['title'];
            $client->startTime =  $input['startTime'];
            $client->endTime =  $input['endTime'];
            $client->liveDate =  $input['liveDate'];
            $client->description =  $input['description'];
            $client->teacher_url =  $json[0]['joinModeratorUrl'];
            $client->student_login_url =  $json[0]['joinAttendeeUrl'];
            $data = $client->save();

        }
        //
        if($data == 1){
            $request->session()->flash('success', 'New Live session Added Successfully!');
            return redirect('/teachers/live/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Live session Please try again!!');
            return redirect('/teachers/live/add');
        }
      //  print_r($data);
    }

    public function addclass(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Live Class";

        // Getting data from database
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get();
        $batch_list = \App\BatchTeacher::where([
            'teacher_id'=>$user_id
            ])->get(); 
           //dd($batch_list); 
        $live_list = \App\LiveTeacher::where([
            'teacher_id'=> $user_id
            ])->with(['teacherName:id,first_name'])->get();
        //dd($live_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.live.addclass',compact('title','live_list','batch_list','courses_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    //for functions without pages only
    public function add_live_class(Request $request){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        }
        $datazz = 0;
        $user_id = $request->session()->get('user_id');
        $user_name = $request->session()->get('user_name');
        $user_name2 = str_replace(' ', '',$user_name);
        $input = $request->all();
        
        $name1 = str_replace(' ', '', $input['title']);
        
		$rand = rand(1000, 9999);
		$meetingID = substr($name1, 0, 4) . $rand;		
        $meetingID = strtoupper($meetingID);
        
        $url = "https://www.selfenabler.com/createMeetingApp?meetingName=".urlencode($name1)."&meetingID=".urlencode($meetingID)."&attendeePW=12345678&moderatorPW=123456&presenter=".urlencode($user_name2);
        //dd($url);
        $curl_handle=curl_init();
        curl_setopt($curl_handle,CURLOPT_URL,$url);
        curl_setopt($curl_handle,CURLOPT_CONNECTTIMEOUT,2);
        curl_setopt($curl_handle,CURLOPT_RETURNTRANSFER,1);
        $buffer = curl_exec($curl_handle);
        curl_close($curl_handle);
        //print_r($buffer);die;
        

        if (empty($buffer)){  
            echo 0;
        }
        else{
            //echo json_encode($buffer);
            $json = json_decode($buffer, true);
            // echo $string = json_encode()."<br>";

            $client = new \App\LiveTeacher;
            $client->teacher_id= $user_id;
            $client->meetingID = $meetingID;
            $client->title = $input['title'];
            $client->type ='class';
            $client->startTime =  $input['startTime'];
            $client->endTime =  $input['endTime'];
            $client->liveDate =  $input['liveDate'];
            $client->description =  $input['description'];
            $client->class_id =  base64_decode($input['class_id']);
            $client->class_name =  $input['class_name'];
            $client->subject_id = base64_decode($input['subject_id']);
            $client->subject_name = $input['subject_name'];
            $client->topic = $input['topic'];
            $client->is_paid = $input['is_paid'];
            $client->amount = $input['amount'];
            $client->teacher_url =  $json[0]['joinModeratorUrl'];
            $client->student_login_url =  $json[0]['joinAttendeeUrl'];
            $datazz = $client->save();

        }
        
        if($datazz == 1){
            $request->session()->flash('success', 'New Live session Added Successfully!');
            return redirect('/teachers/live/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Live session Please try again!!');
            return redirect('/teachers/live/add_live_class');
        }
        //print_r($datazz);
    }

    public function studentview(Request $request, $liveId){
        if(!$request->session()->has('user_id')){
            return redirect('/');
        } 

        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Booked student List";
        // Getting data from database
        $live_list =\App\bookedlivestd::where([
            'live_id'=>$liveId
            ])->with(['students:id,first_name'])->get();
       // dd($live_list);
        // Set data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashTeacher.live.studentview',compact('title','live_list','success','error'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function detail(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        //dd($user_id);
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Reports Detail";
        //geting data from table
        $student_count = \App\InstStudent::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get()->count();

        $teacher_count = \App\Teacher::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get()->count();

        $batch_count = \App\InstBatch::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get()->count();

        $program_count = \App\Program::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get()->count();
        

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.reports.detail',compact('student_count','teacher_count','batch_count','program_count','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TreasureController extends Controller
{
    //
    public function index(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Treasure Box";
        //geting data from table
        $treasure_box = \App\InstTreasure::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.treasure.list',compact('treasure_box','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }


    public function add(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add new Treasure";
        //getting data 
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.treasure.add',compact('title','batch_list','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }
    public function add_treasure(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all(); 
        //image uploading code
        $img = $request->file('image');
        
        foreach($img as $image){
            if($image){
                $image_name = $input['batch_id'].'_'.time().'.'.$image->getClientOriginalExtension();
                $image_path = 'public/uploads/treasure_box/'.$image_name;
                $destinationPath = public_path('/uploads/treasure_box');
                $image->move($destinationPath, $image_name);
            }else{
                $image_path = $input['image_exist'];
            }
            //print_r($image_path);
            $client = new \App\InstTreasure;
            $client->title = $input['title'];
            $client->file_desc = $input['file_desc'];
            $client->file_type =  $input['file_type'];
            $client->file_url =  $image_path;
            $client->batch_id =  $input['batch_id'];
            $client->batch_name =  $input['batch_name'];
            $client->amount =  $input['amount'];
            $client->is_paid =  $input['is_paid'];
            $client->status = '1';
            $client->inst_id = $request->session()->get('user_id');
            $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->has('owner_id');
            $data = $client->save();
        }
        
        if($data == 1){
            $request->session()->flash('success', 'New Treasure Added Successfully!');
            return redirect('/treasure/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Treasure Please try again!!');
            return redirect('/treasure/add');
        }
        print_r($data);


    }
    //For Edit code
    public function edit_treasure(Request $request, $slug){
        if(!$request->session()->has('user_id') ){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = " Edit Treasure Box";
        $treid = base64_decode($slug);
        // Getting data from database
        $treasure_data = \App\InstTreasure::find($treid);
       
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.treasure.edittreasure',compact('title','error','success','batch_list','treasure_data','treid'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }

    //for update
    public function update_treasure(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all(); 

        //image uploading code
        $image = $request->file('image');
            if($image){
                $image_name = $input['batch_id'].'_'.time().'.'.$image->getClientOriginalExtension();
                $image_path = 'public/uploads/treasure_box/'.$image_name;
                $destinationPath = public_path('/uploads/treasure_box');
                $image->move($destinationPath, $image_name);
            }else{
                $image_path = $input['image_exist'];
            }

            //print_r($image_path);
            $id = $input['main_id'];
            $client = \App\InstTreasure::find($id);
            $client->title = $input['title'];
            $client->file_desc = $input['file_desc'];
            $client->file_type =  $input['file_type'];
            $client->file_url =  $image_path;
            $client->batch_id =  $input['batch_id'];
            $client->batch_name =  $input['batch_name'];
            $client->amount =  $input['amount'];
            $client->is_paid =  $input['is_paid'];
            $client->status = '1';
            $client->inst_id = $request->session()->has('user_id');
            $client->owner_id = ($request->session()->has('owner_id') == 0)? $request->session()->has('user_id') : $request->session()->has('owner_id');
            $data = $client->save();
    
        
        if($data == 1){
            $request->session()->flash('success', 'Update Successfully!');
            return redirect('/treasure/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Please try again!!');
            return redirect('/treasure/edit_treasure');
        }
        print_r($data);


    }

}

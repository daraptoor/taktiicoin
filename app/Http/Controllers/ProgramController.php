<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function index(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function list(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Program's List";
        //geting data from table
        $program_list = \App\Program::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
            // orderBy('id', 'DESC')
        ])->get();
      //  dd($program_list);       
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.program.list',compact('program_list','title','error','success'))->with('no', 1);
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_program(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
         //checking session data
         if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Program";
        // Getting data from database
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get(); 
        
    //
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.program.addProgram',compact('title','error','success','courses_list'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function add_new_program(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
        // $sub = explode(',',$input['prog_subject_name']);
        // foreach($sub as $sublist){
        //      $sub_list = explode('|', $sublist);

           	


            $inst_name = $request->session()->get('inst_name');      
            $program = new \App\Program;
            
            $program->prog_name = $input['prog_name'];
            $program->course_id = base64_decode($input['prog_courses']);
            $program->course_name = $input['prog_course_name'];
            $program->class_id =  base64_decode($input['prog_class']);
            $program->class_name =  $input['prog_class_name'];
            $program->subject_id =  base64_decode($input['prog_subject']);
            $program->subject_name =  $input['prog_subject_name'];
            
            // $program->subject_id =  base64_decode($sub_list[1]);
            // $program->subject_name =  $sub_list[0];
            $program->duration =  $input['prog_duration']." ".$input['prog_duration_2'];
            $program->fees =  $input['prog_fees']." ".$input['prog_fees_2'];
            $program->inst_id =  $request->session()->get('user_id');
            $program->owner_id =  ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
            $program->status =  '1';
            $program->demo =  '1';
            $data = $program->save();
        //}
        
        if($data == 1){
            $request->session()->flash('success', 'New Program Added Successfully!');
            return redirect('/program/list');
        }else{
            $request->session()->flash('error', 'Unable to add New Program Please try again!!');
            return redirect('/program/add');
        }
        print_r($data);
    }

    //for edit code
    public function edit_program(Request $request, $slug){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }

    //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Edit Program";
        $proid = base64_decode($slug);
        // Getting data from database  
        $prog_data = \App\Program::find($proid);
        $courses_list = \App\Courses::where([
            'status'=>'1'
        ])->get();         
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.program.editProgram',compact('title','error','success','prog_data','courses_list'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    //for update code
    public function update_program(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all();
       // dd($input);
    //    $sub = explode(',',$input['prog_subject_name']);
    //    foreach($sub as $sublist){
    //        $sub_list = explode('|', $sublist);
               
       // $program = new \App\Program;
        $id = $input['pro_id'];
        $program = \App\Program::find($id);
        $program->prog_name = $input['prog_name'];
        $program->course_id = base64_decode($input['prog_courses']);
        $program->course_name = $input['prog_course_name'];
        $program->class_id =  base64_decode($input['prog_class']);
        $program->class_name =  $input['prog_class_name'];
       // $program->subject_id =  base64_decode($sub_list[1]);
       // $program->subject_name =  $sub_list[0];
        $program->subject_id =  base64_decode($input['prog_subject']);
        $program->subject_name =  $input['prog_subject_name'];
        $program->duration =  $input['prog_duration'];
        $program->fees =  $input['prog_fees']." ".$input['prog_fees_2'];
        $program->inst_id =  $request->session()->get('user_id');
        $program->owner_id =  ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $program->status =  '1';
        $data = $program->save();
    //   }
        if($data == 1){
            $request->session()->flash('success', 'Update Program Successfully!');
            return redirect('/program/list');
        }else{
            $request->session()->flash('error', 'Unable to Update Program Please try again!!');
            return redirect('/program/edit_program/'.base64_decode($id));
        }
        print_r($data);
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FeeController extends Controller
{
    // Fee controler
    public function index(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        return redirect('/dashboard');
    }

    public function batch(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Fee's Detail";
        //geting data from table
        $fee_list = \App\InstFee::where([
            'inst_id'=>$user_id
        ])->get();        
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.fees.list',compact('fee_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }

    public function student(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Fee's Detail";
        //geting data from table
        $program_list = \App\Program::where([
            'inst_id'=>$user_id,
            ['status','!=','2']
        ])->get();        
        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.fees.list',compact('program_list','title','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;
    }

    public function addfee(Request $request){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }

        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "Add Fee's";
        //getting data 
        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
        $program_list = \App\Program::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.fees.addfee',compact('title','batch_list','program_list','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }

    public function editfee(Request $request, $slug){
        if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        //checking session data
        if ($request->session()->has('error')) {
            $error = $request->session()->get('error');
            $request->session()->forget('error');
        }else{
            $error = '';
        }
        if ($request->session()->has('success')) {
            $success = $request->session()->get('success');
            $request->session()->forget('success');
        }else{
            $success = '';
        }
        
        //return $slug;
        //Defining variables
        $user_id = $request->session()->get('user_id');
        $username = $request->session()->get('user_name');
        $inst_name = $request->session()->get('inst_name');
        $user_role = $request->session()->get('user_role');
        $inst_logo = $request->session()->get('logo');
        $title = "edit Fee's";
        //getting data 

        $fee_data = \App\InstFee::find($slug);

        $batch_list = \App\InstBatch::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 
        $program_list = \App\Program::where([
            'inst_id'=>$user_id,
            'status'=>'1'
        ])->get(); 

        //passing data to pages
        $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
        $v2 = view('dashboard.fees.editfee',compact('title','fee_data','batch_list','program_list','error','success'));
        $v3 = view('common.footer');
        return $v1.$v2.$v3;

    }

    public function add_new_fees(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }

        $user_id = $request->session()->has('user_id');
        $input = $request->all(); 
        //dd($input);   
         // Checking student exist with same institute
         $chk_student = \App\InstStudent::where([
            'mobile'=> $input['mobile'],
            'inst_id'=>$user_id
        ])->count();

        if($chk_student > 0){
            $request->session()->flash('error', 'Unable to add Student, Same number already exist in your student list!!');
            return redirect('/student/add');
        }    

        // data collection
        $client = new \App\InstFee;
        $client->mobile = $input['mobile'];
        $client->student_name = $input['student_name'];
        // $client->batch_id =  $input['batch_id'];
        // $client->batch_name =  $input['batch_name'];
        $client->prog_id =  $input['prog_id'];
        $client->prog_name =  $input['prog_name'];
        $client->amount =  $input['amount'];
        $client->paid =  $input['paid'];
        $client->paid_status =  $input['paid_status'];
        $client->paid_date =  $input['paid_date'];
        $client->pending =  $input['pending'];
        $client->pending_status = $input['pending_status'];
        $client->pending_date =  $input['pending_date'];
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'New Fee Added Successfully!');
            return redirect('/fee/add');
        }else{
            $request->session()->flash('error', 'Unable to add New fee Please try again!!');
            return redirect('/fee/add');
        }
        print_r($data);
    }

    public function update_fees(Request $request){
        if(!$request->session()->has('user_id')  || $request->session()->get('user_role') != 1){
            return redirect('/');
        }
        $input = $request->all(); 
        //dd($input);       

        // data collection

        $id = $input['id'];
        $client = \App\InstFee::find($id);
        $client->student_name = $input['student_name'];
        $client->student_mobile = $input['student_mobile'];
        // $client->batch_id =  $input['batch_id'];
        // $client->batch_name =  $input['batch_name'];
        $client->prog_id =  $input['prog_id'];
        $client->prog_name =  $input['prog_name'];
        $client->amount =  $input['amount'];
        $client->paid =  $input['paid'];
        $client->paid_status =  $input['paid_status'];
        $client->paid_date =  $input['paid_date'];
        $client->pending =  $input['pending'];
        $client->pending_status = $input['pending_status'];
        $client->pending_date =  $input['pending_date'];
        $client->inst_id = $request->session()->get('user_id');
        $client->owner_id = ($request->session()->get('owner_id') == 0)? $request->session()->get('user_id') : $request->session()->get('owner_id');
        $data = $client->save();
        if($data == 1){
            $request->session()->flash('success', 'Fee Updated Successfully!');
            return redirect('/fee/edit/'.$id);
        }else{
            $request->session()->flash('error', 'Unable to Updated fee Please try again!!');
            return redirect('/fee/edit/'.$id);
        }
        print_r($data);
    }

    public function payments(Request $request){
            if(!$request->session()->has('user_id') || $request->session()->get('user_role') != 1){
                return redirect('/');
            }
            //checking session data
            if ($request->session()->has('error')) {
                $error = $request->session()->get('error');
                $request->session()->forget('error');
            }else{
                $error = '';
            }
            if ($request->session()->has('success')) {
                $success = $request->session()->get('success');
                $request->session()->forget('success');
            }else{
                $success = '';
            }
            //Defining variables
            $user_id = $request->session()->get('user_id');
         //   print_r($user_id);die;
            $username = $request->session()->get('user_name');
            $inst_name = $request->session()->get('inst_name');
            $user_role = $request->session()->get('user_role');
            $inst_logo = $request->session()->get('logo');
            $title = "Online Payments Detail";
            //geting data from table

            $fee_list = \App\Payments::where([
                'teacher_id'=>$user_id           
                ])->with(['students','programdetails'])->get();
                
                //dd($fee_list);
            //passing data to pages
            $v1 = view('common.header',compact('username','title','inst_name','user_role','inst_logo'));
            $v2 = view('dashboard.fees.onlinepayments',compact('fee_list','title','error','success'));
            $v3 = view('common.footer');
            return $v1.$v2.$v3;
    
        }


}

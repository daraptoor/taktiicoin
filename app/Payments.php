<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payments';
    public $timestamps = false;


    public function students(){
        return $this->hasOne('\App\InstStudent' ,'std_id', 'student_id');
    }
    

    public function programdetails(){
        return $this->hasOne('\App\Program', 'id' , 'batch_id');
    }


}


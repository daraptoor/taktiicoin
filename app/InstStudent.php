<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstStudent extends Model
{
    protected $table = 'inst_students';

    public function payments()
    {
        return $this->hasMany('App\Payments');
    }

    public function batchdetails(){
        return $this->hasOne('\App\InstBatch', 'id' , 'batch_id');
    }

}

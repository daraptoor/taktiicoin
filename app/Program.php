<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'inst_program';

    public function payments()
    {
        return $this->hasMany('App\Payments');
    }

}

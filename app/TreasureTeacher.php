<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreasureTeacher extends Model
{
    protected $table = "teasurebox";
    public $timestamps = false;
}

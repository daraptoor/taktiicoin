<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstBatch extends Model
{
    protected $table = 'inst_batches';

    public function programdetails(){
        return $this->hasOne('\App\Program', 'id' , 'program_id');
    }

}

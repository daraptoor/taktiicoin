<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionmodel extends Model
{
    protected $table = 'question_quiz';
    public $timestamps = false;
    
}

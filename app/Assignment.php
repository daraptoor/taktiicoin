<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $table = "inst_assignment";
        
    public function instteachers(){
        return $this->hasOne('\App\Teacher' ,'id', 'teacher_id');
    }

    public function batchinfo(){
        return $this->hasOne('\App\BatchTeacher', 'id', 'batch_id');
    }
}

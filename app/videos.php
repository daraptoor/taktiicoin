<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class videos extends Model
{
    protected $table = "inst_videos";
        
    public function instteachers(){
        return $this->hasOne('\App\Teacher' ,'id', 'teacher_id');
    }

    public function batchinfo(){
        return $this->hasOne('\App\BatchTeacher', 'id', 'batch_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayTeacher extends Model
{
    protected $table = 'inst_pay_teacher';  

    public function getteacherid(){
        return $this->belongsTo('\App\TeacherMain', 'mobile');
    }

}

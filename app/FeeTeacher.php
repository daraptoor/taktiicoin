<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeeTeacher extends Model
{
    protected $table = 'payments';
    public $timestamps = false;

    public function batchinfo(){
        return $this->hasOne('\App\BatchTeacher', 'id', 'batch_id');
    }
    public function studentinfo(){
        return $this->hasOne('\App\Student', 'id', 'student_id');
    }
}

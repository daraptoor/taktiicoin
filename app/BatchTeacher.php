<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchTeacher extends Model
{
    protected $table = "batches";
    public $timestamps = false;

    public function studentList(){
        return $this->hasMany('\App\TeacherStdRel', 'batch_id');
    }
}

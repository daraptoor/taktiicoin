<?php

namespace App;
//
use Illuminate\Database\Eloquent\Model;

class Instdemo extends Model
{
    protected $table = 'inst_demo';

    public function progdetails(){
        return $this->hasOne('\App\Program', 'id' , 'prog_id');
    }
}

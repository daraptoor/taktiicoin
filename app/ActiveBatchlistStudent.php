<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActiveBatchlistStudent extends Model
{
    protected $table = 'teacher_student_rel';
    public $timestamps = false;

     public function batchinfo(){
         return $this->belongsTo('\App\BatchTeacher', 'batch_id');
     }
    public function teacherinfo(){
        return $this->belongsTo('\App\TeacherMain', 'teacher_id');
    }

    public function students(){
        return $this->hasOne('\App\Student' ,'id', 'student_id');
    }

    // public function bookwebnair(){
    //     return $this->hasOne('\App\LiveTeacher', 'teacher_id', 'teacher_id');
    //     }
   
}

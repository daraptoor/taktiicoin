<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleQues extends Model
{
    protected $table = 'multiple_question';
    public $timestamps = false;
}

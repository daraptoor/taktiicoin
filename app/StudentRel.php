<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRel extends Model
{
    //
    protected $table = 'ins_rel';

    
    public function batchinfo(){
        return $this->belongsTo('\App\InstBatch', 'batch_id');
    }
    
    public function batchdetails(){
        return $this->hasOne('\App\InstBatch', 'id' , 'batch_id');
    }

    public function students(){
        return $this->hasOne('\App\InstStudent' ,'std_id', 'student_id');
    }

    public function Attendence(){
        return $this->hasOne('\App\Attendence' ,'batch_id', 'batch_id');
    }
    
    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentBookedTeacherList extends Model
{
    protected $table = 'book_webinar_teacher';
    public $timestamps = false;



    public function paymentdetails(){
        return $this->belongsTo('\App\bookedlivestd', 'id');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $table = "inst_bookmark";
        
    public function students(){
        return $this->hasOne('\App\InstStudent' ,'std_id', 'student_id');
    }
}

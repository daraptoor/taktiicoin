<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherMain extends Model
{
    //
    protected $table = "teachers";
    public $timestamps = false;
}

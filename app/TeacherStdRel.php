<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherStdRel extends Model
{
    protected $table = 'teacher_student_rel';
    public $timestamps = false;

    public function students()
    {
        return $this->hasOne('\App\Student', 'id', 'student_id');
    }

    // public function studentinfo(){
    //     return $this->hasOne('\App\Student', 'id', 'student_id');
    // }
}

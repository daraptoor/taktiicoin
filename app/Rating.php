<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = "inst_rating";
        
    public function students(){
        return $this->hasOne('\App\InstStudent' ,'std_id', 'std_id');
    }
}

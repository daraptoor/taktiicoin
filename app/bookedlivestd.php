<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookedlivestd extends Model
{
    protected $table = 'book_webinar_std_join';
    public $timestamps = false;

    public function students(){
        return $this->hasOne('\App\Student' ,'id', 'student_id');
    }

    public function livedetails(){
        return $this->belongsTo('\App\LiveTeacher', 'live_id');
    }

}

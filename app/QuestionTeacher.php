<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionTeacher extends Model
{
    protected $table = "teacher_question";
    public $timestamps = false;
    
    public function batchinfo(){
        return $this->belongsTo('\App\BatchTeacher', 'batch_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Querystudentteacher extends Model
{
    protected $table = 'student_query_ims';

    public function querystudentdata(){
        return $this->hasOne('\App\Querystudent' ,'id', 'query_id');
    }

    public function students(){
        return $this->hasOne('\App\Student' ,'id', 'student_id');
    }
    
}

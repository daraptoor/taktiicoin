<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveTeacher extends Model
{
    protected $table = 'book_webinar_teacher';
    public $timestamps = false;

    public function teacherName(){
        return $this->belongsTo('\App\TeacherMain', 'teacher_id');
    }
    
    public function batchDetail(){
         return $this->hasOne('\App\BatchTeacher', 'batch_code', 'meetingID');
     }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');
Route::get('login/teacher', 'LoginController@teacher');
Route::get('login/teacher/forget', 'LoginController@teacherForget');
Route::get('login/student', 'LoginController@student');
Route::get('login/student/forget', 'LoginController@studentForget');
Route::get('login', 'DashboardController@index');
Route::post('login', 'LoginController@chkLogin');
Route::post('loginTeacher', 'LoginController@chkLoginTeacher');
Route::post('forgetTeacher', 'LoginController@chkforgetTeacher');
Route::post('loginStudent', 'LoginController@chkLoginStudent');
Route::post('forgetStudent', 'LoginController@chkforgetStudent');
Route::get('logout','LoginController@logout');
Route::get('dashboard','DashboardController@index');

//Ajax requests  
Route::get('ajax','AjaxController@index');
Route::get('ajax/chk_exist_username/{slug}','AjaxController@chk_exist_username');
Route::get('ajax/chk_exist_teacher/{slug}','AjaxController@chk_exist_teacher');
Route::get('ajax/chk_exist_teacher_main/{slug}','AjaxController@chk_exist_teacher_main');
Route::get('ajax/chk_forget_teacher_main/{slug}','AjaxController@chk_forget_teacher_main');
Route::get('ajax/chk_forget_student_main/{slug}','AjaxController@chk_forget_student_main');
Route::get('ajax/chk_exist_student_main/{slug}','AjaxController@chk_exist_student_main');
Route::get('ajax/update_user_status/{slug}/{status}','AjaxController@update_user_status');
Route::get('ajax/get_batches_rel/{slug}/{name}','AjaxController@get_batches_rel');
Route::get('ajax/update_program_status/{slug}/{status}','AjaxController@update_program_status');
Route::get('ajax/update_program_demo/{slug}/{status}','AjaxController@update_program_demo');
Route::get('ajax/update_institute_demo/{slug}/{status}','AjaxController@update_institute_demo');
Route::get('ajax/update_institute_query/{slug}/{status}','AjaxController@update_institute_query');
Route::get('ajax/update_book_institute_demo/{slug}/{status}','AjaxController@update_book_institute_demo');
Route::get('ajax/update_teacher_status/{slug}/{status}','AjaxController@update_teacher_status');
Route::get('ajax/update_student_status/{slug}/{status}','AjaxController@update_student_status');
Route::get('ajax/update_treasure_status/{slug}/{status}','AjaxController@update_treasure_status');
Route::get('ajax/update_promotion_status/{slug}/{status}','AjaxController@update_promotion_status');
Route::get('ajax/update_achiever_status/{slug}/{status}','AjaxController@update_achiever_status');
Route::get('ajax/update_rel_status/{slug}/{status}','AjaxController@update_rel_status');
Route::get('ajax/update_batch_status/{slug}/{status}','AjaxController@update_batch_status');
Route::get('ajax/get_classes_from_course/{slug}','AjaxController@get_classes_from_course');
Route::get('ajax/get_subject_from_class/{slug}','AjaxController@get_subject_from_class');
Route::get('ajax/get_selected_classes_from_course/{slug}/{selected}','AjaxController@get_selected_classes_from_course');
Route::get('ajax/get_selected_subject_from_class/{slug}/{selected}','AjaxController@get_selected_subject_from_class');
Route::get('ajax/get_batches_teachers/{slug}/{date}','AjaxController@get_batches_teachers');
Route::get('ajax/get_personal_batches_teachers/{slug}/{date}','AjaxController@get_personal_batches_teachers');
Route::get('ajax/get_student_selected_by_batches/{slug}','AjaxController@get_student_selected_by_batches');

Route::get('ajax/get_batches_students/{slug}/{date}','AjaxController@get_batches_students');
Route::get('ajax/get_batches_students_list/{slug}/{date}','AjaxController@get_batches_students_list');
Route::get('ajax/chk_exist_number/{slug}','AjaxController@chk_exist_number');
Route::get('ajax/del_rel_status/{slug}','AjaxController@del_rel_status');
Route::get('ajax/get_batch_live_session/{slug}/{date}','AjaxController@get_batch_live_session');

Route::get('ajax/setFreeLiveForStudent/{studentid}/{liveid}','AjaxController@setFreeLiveForStudent');
Route::get('ajax/setPaidLiveForStudent/{data}','AjaxController@setPaidLiveForStudent');

Route::get('ajax/add_event_calendar/{text}/{date}','AjaxController@add_event_calender');
Route::get('ajax/get_event_calendar','AjaxController@get_event_calender');
Route::get('ajax/del_event_calender/{slug}','AjaxController@del_event_calender');
Route::get('ajax/del_batch/{slug}','AjaxController@del_batch');


//Admin in dashboard
Route::get('admin','AdminController@index');
Route::get('admin/list','AdminController@admin');
Route::get('admin/add_admin','AdminController@add_admin');
Route::get('admin/edit/{slug}','AdminController@edit_admin');
Route::post('admin/add_new_admin','AdminController@add_new_admin');
Route::post('admin/update_admin','AdminController@update_admin');
Route::get('admin/details','AdminController@single_institute');

//Program in dashboard
Route::get('program','ProgramController@index');
Route::get('program/list','ProgramController@list');
Route::get('program/add','ProgramController@add_program');
Route::get('program/edit/{slug}','ProgramController@edit_program');
Route::post('program/add_new_program','ProgramController@add_new_program');
Route::post('program/update_program','ProgramController@update_program');

//Teacher in dashboard
Route::get('teacher','TeacherController@index');
Route::get('teacher/list','TeacherController@teachers');
Route::get('teacher/add','TeacherController@add_teacher');
Route::get('teacher/edit/{slug}','TeacherController@edit_teacher');
Route::post('teacher/add_new_teacher','TeacherController@add_new_teacher');
Route::post('teacher/update_teacher','TeacherController@update_teacher');
Route::get('teacher/pay/{slug}','TeacherController@pay');
Route::post('teacher/add_pay_teacher','TeacherController@add_pay_teacher');


//Student in Dashboard
Route::get('student','StudentController@index');
Route::get('student/list','StudentController@student');
Route::get('student/add','StudentController@add_student');
Route::get('student/edit/{slug}','StudentController@edit_student');
Route::post('student/add_new_student','StudentController@add_new_student');
Route::post('student/update_student','StudentController@update_student');
Route::get('student/assign','StudentController@assign');
Route::post('student/assign_student','StudentController@assign_student');


//Batches in Dashboard
Route::get('batch','BatchController@index');
Route::get('batch/list','BatchController@batch');
Route::get('batch/add','BatchController@add_batch');
Route::get('batch/edit/{slug}','BatchController@edit_batch');
Route::post('batch/add_new_batch','BatchController@add_new_batch');
Route::post('batch/update_batch','BatchController@update_batch');
Route::get('batch/joinstudent/{batch_id}','BatchController@joinstudent');//join batch student


//Demo in Dashboard
Route::get('demo','DemoController@index');
Route::get('demo/list','DemoController@list');
Route::get('demo/report','DemoController@report');
Route::get('demo/penddingreport','DemoController@penddingreport');

//Bookmark in Dashboard
Route::get('bookmark','BookmarkController@index');
Route::get('bookmark/list','BookmarkController@list');


//Rating in Dashboard
Route::get('rating','RatingController@index');
Route::get('rating/list','RatingController@list');


//Query in Dashboard
Route::get('query','QueryController@index');
Route::get('query/list','QueryController@list');

//Whiteboard in Dashboard
Route::get('whiteboard','WhiteboardController@index');
Route::get('whiteboard/datesheetlist','WhiteboardController@datesheetlist');
Route::get('whiteboard/assignmentlist','WhiteboardController@assignmentlist');
Route::get('whiteboard/videoslist','WhiteboardController@videoslist');
Route::get('whiteboard/notificationslist','WhiteboardController@notificationslist');

//Parents in Dashboard
Route::get('parent','ParentController@index');
Route::get('parent/list','ParentController@parent');
Route::post('parent/update3','ParentController@update_parent_detail');

//Fees in Dashboard
Route::get('fee','FeeController@index');
Route::get('fee/batch','FeeController@batch');
Route::get('fee/student','FeeController@student');
Route::get('fee/add','FeeController@addfee');
Route::get('fee/edit/{slug}','FeeController@editfee');
Route::post('fee/add_new_fees','FeeController@add_new_fees');
Route::post('fee/update_fees','FeeController@update_fees');
Route::get('fee/payments','FeeController@payments');

//Attendence in Dashboard
Route::get('attendance','AttendanceController@index');
Route::get('attendance/student_mark','AttendanceController@student_mark');
Route::get('attendance/list','AttendanceController@list');
Route::post('attendance/mark_attendence','AttendanceController@mark_attendence');
Route::post('attendance/attendence_details','AttendanceController@attendence_details');


//Calendar Teacher  in Dashteacher
Route::get('calendar','CalendarController@index');
Route::get('calendar/view','CalendarController@view');


//Reports Class in Dashboard
Route::get('reports','ReportController@index');
Route::get('reports/detail','ReportController@detail');

//Treasure box in Dashboard
Route::get('treasure','TreasureController@index');
Route::get('treasure/list','TreasureController@list');
Route::get('treasure/add','TreasureController@add');
Route::get('treasure/edit/{slug}','TreasureController@edit_treasure');
Route::post('/treasure/add_treasure','TreasureController@add_treasure');
Route::post('treasure/update_treasure','TreasureController@update_treasure');

//Promotion box in Dashboard
Route::get('promotion','PromotionController@index');
Route::get('promotion/list','PromotionController@list');
Route::get('promotion/add','PromotionController@add');
Route::post('/promotion/add_promotion','PromotionController@add_promotion');

//StarAchiever in Dashboard
Route::get('starachiever','StarAchieverController@index');
Route::get('starachiever/list','StarAchieverController@list');
Route::get('starachiever/add','StarAchieverController@add');
Route::post('/starachiever/add_achiever','StarAchieverController@add_achiever');

//Live Class in Dashboard
Route::get('live/list','CsoonController@index');

//Quiz Class in Dashboard
Route::get('ques','QuestionController@index');
Route::get('ques/listques','QuestionController@listques');
Route::get('ques/addques','QuestionController@addques');
Route::post('ques/add_new_ques','QuestionController@add_new_ques');
Route::get('ques/viewmultiques/{slug}','QuestionController@viewmultiques');


/*--------------------------------------------------DashTeacher(Single Teacher)----------------------------------------------------  */

//Teacher Dashboard in Dashteacher
Route::get('teachers','DashboardController@index');
Route::get('teachers/dashboard','DashboardController@dashboard');


//Teacher Batches in Dashteacher
Route::get('teachers','TeacherBatchController@index');
Route::get('teachers/batches/list','TeacherBatchController@list');
Route::get('teachers/batches/add','TeacherBatchController@add_tbatch');
Route::post('teachers/batches/add_new_tbatch','TeacherBatchController@add_new_tbatch');
Route::get('teachers/batches/edit/{slug}','TeacherBatchController@edit_tbatch');
Route::post('teachers/batches/update_tbatch','TeacherBatchController@update_tbatch');
Route::get('teachers/batches/attendence','TeacherBatchController@attendence');
Route::post('teachers/batches/mark_attendence','TeacherBatchController@mark_attendence');
Route::get('teachers/batches/joinstudent/{batch_code}','TeacherBatchController@joinstudent');//join batch student


//Institute Teacher  in Dashteacher
Route::get('teachers','TeacherInstituteController@index');
Route::get('teachers/institute/list','TeacherInstituteController@list');
Route::get('teachers/institute/view/{slug}','TeacherInstituteController@view');
Route::get('teachers/institute/attendence','TeacherInstituteController@attendence');
Route::post('teachers/institute/mark_attendence','TeacherInstituteController@mark_attendence');

//BatchStudentList Teacher  in Dashteacher
Route::get('teachers','TeacherjoinStudentController@index');
Route::get('teachers/student/join_student','TeacherjoinStudentController@join_student');


//LIve Teacher  in Dashteacher
Route::get('teachers','TeacherLiveController@index');
Route::get('teachers/live/list','TeacherLiveController@list');
Route::get('teachers/live/add','TeacherLiveController@add');
Route::get('teachers/live/addclass','TeacherLiveController@addclass');
Route::post('teachers/live/add_live_session','TeacherLiveController@add_live_session');
Route::post('teachers/live/add_live_class','TeacherLiveController@add_live_class');
Route::get('teachers/live/studentview/{liveId}','TeacherLiveController@studentview');//Live Batch Booked StudentList

//Calendar Teacher  in Dashteacher
Route::get('teachers','TeacherCalendarController@index');
Route::get('teachers/calendar/view','TeacherCalendarController@view');

// Question/Assignment in dashTeacher
Route::get('teachers','TeacherQuestionController@index');
Route::get('teachers/question/list','TeacherQuestionController@list');
Route::get('teachers/question/add','TeacherQuestionController@add');
Route::post('teachers/question/add_new_ques','TeacherQuestionController@add_new_ques');
Route::get('teachers/question/viewmultiques/{slug}','TeacherQuestionController@viewmultiques');

//Treasure box in Dashteacher
Route::get('teachers','TeacherTreasureController@index');
Route::get('teachers/treasure/list','TeacherTreasureController@list');
Route::get('teachers/treasure/add','TeacherTreasureController@add');
//Route::get('teachers/treasure/edit/{slug}','TeacherTreasureController@edit_treasure');
Route::post('/teachers/treasure/add_treasure','TeacherTreasureController@add_treasure');
//Route::post('teachers/treasure/update_treasure','TeacherTreasureController@update_treasure');

//Fee in Dashteacher
Route::get('teachers','TeacherFeeController@index');
Route::get('teachers/fee/list','TeacherFeeController@list');
Route::get('teachers/fee/add','TeacherFeeController@add');
Route::post('/teachers/fee/add_fee','TeacherFeeController@add_fee');
Route::get('teachers/fee/edit/{slug}','TeacherFeeController@editfee');
Route::post('teachers/fee/update_fees','TeacherFeeController@update_fees');


/*-----------------------------------------------------DashStudent(only student)---------------------------------------------------------------------*/

//Students Batches in Dashstudent<!-- Modal -->
Route::get('students','StudentBatchController@index');
Route::get('students/batches/batchlist','StudentBatchController@batchlist');
Route::get('students/batches/livebatchlist','StudentBatchController@livebatchlist');
Route::get('students/batches/join_student','StudentBatchController@join_student');



<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");

// following files need to be included
require_once("./lib/config_paytm.php");
require_once("./lib/encdec_paytm.php");
$url = "http://localhost/taktii/taktiicoin/";
$paytmChecksum = "";
$paramList = array();
$isValidChecksum = "FALSE";

$paramList = $_POST;
$paytmChecksum = isset($_POST["CHECKSUMHASH"]) ? $_POST["CHECKSUMHASH"] : ""; //Sent by Paytm pg

//Verify all parameters received from Paytm pg to your application. Like MID received from paytm pg is same as your application�s MID, TXN_AMOUNT and ORDER_ID are same as what was sent by you to Paytm PG for initiating transaction etc.
$isValidChecksum = verifychecksum_e($paramList, PAYTM_MERCHANT_KEY, $paytmChecksum); //will return TRUE or FALSE string.
//

if($isValidChecksum == "TRUE") {

	if ($_POST["STATUS"] == "TXN_SUCCESS") {
		//Transaction status is success
		$orderid = $_POST['ORDERID'];
		$datttts = explode('-',$orderid);
		$user_id = $datttts['1'];
		$live_id = $datttts['2'];
		$ORDERID = $orderid;
		$TXNID = $_POST['TXNID'];
		$TXNAMOUNT = $_POST['TXNAMOUNT'];
		$TXNDATE = $_POST['TXNDATE'];
		$string = $user_id."/".$live_id."/".$ORDERID."/".$TXNID."/".$TXNAMOUNT."/".$TXNDATE;
		$encoded = base64_encode($string);
		// $decoded = base64_decode($encoded);
		// print_r($encoded);
		// print_r($decoded);
		header("Location: ".$url."ajax/setPaidLiveForStudent/".$encoded);
	}
	else {
		//Transaction status is failure
		return redirect($url.'/students/batches/livebatchlist?success=0');
	}

	// if (isset($_POST) && count($_POST)>0 )
	// { 
	// 	foreach($_POST as $paramName => $paramValue) {
	// 			echo "<br/>" . $paramName . " = " . $paramValue;
	// 	}
	// }
	

}
else {
	//Checksum mismatched
	//Process transaction as suspicious.
	return redirect($url.'/students/batches/livebatchlist?success=0');
}

?>
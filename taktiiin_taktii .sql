-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2018 at 02:56 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taktiiin_taktii`
--

-- --------------------------------------------------------

--
-- Table structure for table `acadmic`
--

CREATE TABLE `acadmic` (
  `id` int(11) NOT NULL,
  `acadmic_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acadmic`
--

INSERT INTO `acadmic` (`id`, `acadmic_name`) VALUES
(1, 'I'),
(2, 'II'),
(3, 'III'),
(4, 'IV'),
(5, 'V'),
(6, 'VI'),
(7, 'VII'),
(8, 'VIII'),
(9, 'IX'),
(10, 'X'),
(11, 'XI'),
(12, 'XII'),
(13, ' JEE'),
(14, 'AIEEE'),
(15, 'AIPMT'),
(16, 'MAT'),
(17, 'CAT'),
(18, 'XAT'),
(19, 'XIMB'),
(20, 'AIIMS'),
(21, 'BANK PO'),
(22, 'UGC NET'),
(23, 'IAS/IPS'),
(24, 'B.Ed '),
(25, 'CLAT'),
(26, 'BITSAT'),
(27, 'GATE'),
(28, 'GRE'),
(29, 'IELTS'),
(30, 'TOEFL'),
(31, 'GMAT'),
(32, 'SAT'),
(33, 'LSAT'),
(34, 'SSC-CGL'),
(35, 'SSC-CHSL'),
(36, 'SSC-JE'),
(37, 'JEE-MAINS'),
(38, 'JEE-ADVANCED'),
(39, 'CMAT'),
(40, 'IBPS-PO'),
(41, 'IBPS-CLERK'),
(42, 'SBI-PO'),
(43, 'SBI-CLERK'),
(44, 'RBI-OFFICER'),
(45, 'RBI-ASSISTANT'),
(46, 'CTET'),
(47, 'SSC-CPO'),
(48, 'SSC-MTS'),
(49, 'RRB'),
(50, 'NDA'),
(51, 'AILET(All INDIA Law Entrance test)'),
(52, 'NIFT'),
(53, 'CPT(Common Proficiency Test)'),
(54, 'NEET'),
(55, 'CDS'),
(56, 'NEET PG'),
(57, 'NEET UG'),
(58, 'UGC NET'),
(59, 'NET'),
(60, 'AFCAT'),
(61, 'C.A'),
(62, 'C.S'),
(63, 'TYPING '),
(64, 'NSD'),
(65, 'NAT(National Aptitude test)'),
(66, 'AICEE(All India Common Entrance Examination)'),
(67, 'AFMC(Armed Forces Medical core Entrance)'),
(68, 'J.B.T'),
(69, 'K.V.S'),
(70, 'STET'),
(71, 'HTET'),
(72, 'TET'),
(73, 'DSSSB');

-- --------------------------------------------------------

--
-- Table structure for table `aim`
--

CREATE TABLE `aim` (
  `id` int(11) NOT NULL,
  `aim` varchar(255) NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aim`
--

INSERT INTO `aim` (`id`, `aim`, `created`) VALUES
(1, 'Become a Doctor', 0),
(2, 'CA', 0),
(3, 'An Engineer', 0),
(4, 'politician', 0);

-- --------------------------------------------------------

--
-- Table structure for table `all_like_dislike`
--

CREATE TABLE `all_like_dislike` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL COMMENT '0:dislike, 1:liked',
  `added_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `all_like_dislike`
--

INSERT INTO `all_like_dislike` (`id`, `teacher_id`, `student_id`, `status`, `added_date`) VALUES
(1, '2', '1', '1', '20-12-2017 14:55:26'),
(2, '1', '3', '1', '20-12-2017 15:40:04'),
(3, '1', '1', '1', '21-12-2017 14:12:43'),
(4, '9', '22', '1', '21-12-2017 14:27:44'),
(5, '2', '27', '0', '21-12-2017 19:32:11'),
(6, '18', '1', '1', '22-12-2017 16:38:29'),
(7, '26', '11', '1', '24-12-2017 09:09:08'),
(8, '2', '30', '1', '24-12-2017 09:17:59'),
(9, '9', '1', '1', '11-01-2018 11:31:08'),
(10, '9', '3', '0', '11-01-2018 15:14:54'),
(11, '39', '1', '1', '15-01-2018 11:08:22'),
(12, '79', '1', '1', '15-01-2018 14:32:29'),
(13, '39', '3', '1', '15-01-2018 16:51:34'),
(14, '9', '4', '1', '16-01-2018 11:39:28'),
(15, '80', '1', '1', '16-01-2018 18:21:28'),
(16, '79', '5', '1', '18-01-2018 12:20:11'),
(17, '9', '5', '1', '18-01-2018 12:20:33'),
(18, '2', '6', '1', '20-01-2018 10:22:22'),
(19, '15', '6', '1', '20-01-2018 10:22:58'),
(20, '15', '25', '1', '20-01-2018 14:47:08'),
(21, '99', '25', '1', '21-01-2018 13:23:43'),
(22, '15', '29', '1', '22-01-2018 20:41:02'),
(23, '80', '23', '1', '23-01-2018 13:11:57'),
(24, '80', '17', '0', '23-01-2018 14:39:16'),
(25, '99', '17', '1', '23-01-2018 14:40:54'),
(26, '49', '20', '1', '24-01-2018 15:53:55'),
(27, '49', '23', '1', '24-01-2018 15:59:55'),
(28, '15', '23', '1', '24-01-2018 16:34:20'),
(29, '74', '23', '1', '24-01-2018 18:00:19'),
(30, '9', '000', '1', '24-01-2018 19:37:06'),
(31, '99', '000', '1', '24-01-2018 19:37:07'),
(32, '49', '000', '0', '24-01-2018 19:39:37'),
(33, '30', '000', '1', '24-01-2018 19:41:28'),
(34, '83', '000', '0', '25-01-2018 17:42:56'),
(35, '107', '000', '1', '25-01-2018 18:07:22'),
(36, '99', '30', '1', '25-01-2018 18:08:10'),
(37, '26', '30', '1', '25-01-2018 18:09:14'),
(38, '15', '000', '1', '25-01-2018 18:10:41'),
(39, '74', '000', '1', '25-01-2018 18:10:49'),
(40, '107', '5', '1', '29-01-2018 17:32:54'),
(41, '26', '000', '1', '02-02-2018 15:55:39'),
(42, '24', '000', '1', '02-02-2018 15:57:21'),
(43, '136', '30', '1', '02-02-2018 16:52:57'),
(44, '74', '30', '1', '02-02-2018 18:25:18'),
(45, '87', '000', '1', '05-02-2018 17:52:20'),
(46, '46', '000', '1', '05-02-2018 17:53:22'),
(47, '164', '30', '1', '06-02-2018 14:51:57'),
(48, '75', '5', '1', '06-02-2018 15:27:50'),
(49, '171', '2', '1', '07-02-2018 18:06:08'),
(50, '105', '2', '1', '07-02-2018 18:06:18'),
(51, '24', '2', '1', '07-02-2018 18:07:13'),
(52, '171', '35', '1', '07-02-2018 18:16:55'),
(53, '105', '35', '1', '07-02-2018 18:17:15'),
(54, '167', '35', '1', '07-02-2018 18:17:27'),
(55, '171', '20', '0', '08-02-2018 17:34:30'),
(56, '171', '20', '0', '08-02-2018 17:34:30'),
(57, '155', '2', '1', '08-02-2018 18:45:34'),
(58, '80', '2', '1', '08-02-2018 18:45:55'),
(59, '66', '2', '1', '08-02-2018 18:46:49'),
(60, '136', '000', '1', '09-02-2018 12:09:20'),
(61, '105', '42', '1', '09-02-2018 17:44:38'),
(62, '144', '47', '1', '10-02-2018 23:34:52'),
(63, '184', '2', '1', '11-02-2018 14:42:40'),
(64, '183', '6', '1', '11-02-2018 14:56:23'),
(65, '213', '23', '1', '12-02-2018 17:15:02'),
(66, '198', '23', '1', '13-02-2018 13:37:43'),
(67, '176', '42', '1', '14-02-2018 16:25:35'),
(68, '237', '42', '1', '28-02-2018 11:24:42'),
(69, '105', '124', '0', '15-03-2018 14:21:38'),
(70, '219', '124', '1', '15-03-2018 14:22:49'),
(71, '241', '124', '1', '16-03-2018 11:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `all_notifications`
--

CREATE TABLE `all_notifications` (
  `id` int(11) NOT NULL,
  `notif_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notif_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_read` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '0: from teacher to student, 1 from student to teacher',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `all_notifications`
--

INSERT INTO `all_notifications` (`id`, `notif_to`, `notif_from`, `title`, `description`, `link`, `is_read`, `added_date`, `read_date`, `type`, `status`) VALUES
(1, '1', '1', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Cat).', 'batch_id=1', '1', '20-12-2017 15:04:10', '21-12-2017 17:30:04', 0, 0),
(2, '2', '1', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Cat).', 'batch_id=1', '1', '20-12-2017 15:38:44', '21-12-2017 12:55:45', 0, 1),
(3, '1', '1', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(ENglish Batch).', 'batch_id=2', '1', '21-12-2017 14:00:38', '21-12-2017 17:30:03', 0, 0),
(4, '1', '9', 'Session Request', 'Congrats,  Tutor (Smrati Priya) has accepted you in his Session.', 'session_id=1', '1', '21-12-2017 14:24:55', '21-12-2017 17:30:01', 0, 0),
(5, '2', '9', 'Session Request', 'Congrats,  Tutor (Smrati Priya) has accepted you in his Session.', 'session_id=2', '1', '21-12-2017 14:29:24', '21-12-2017 14:41:38', 0, 0),
(6, '18', '1', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(ENglish Batch).', 'batch_id=2', '0', '21-12-2017 18:10:41', NULL, 0, 1),
(7, '22', '1', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(ENglish Batch).', 'batch_id=2', '1', '21-12-2017 18:23:09', '21-12-2017 18:25:24', 0, 1),
(8, '1', '18', 'Batch Request', 'Congrats, Teacher (Swetak) has accepted you in his batch(Engish).', 'batch_id=3', '0', '22-12-2017 16:21:56', NULL, 0, 0),
(9, '6', '26', 'Batch Request', 'Congrats, Teacher (S K SINGH) has accepted you in his batch(11 Class Accounts).', 'batch_id=6', '1', '23-12-2017 15:02:56', '23-12-2017 15:11:11', 0, 0),
(10, '6', '28', 'Batch Request', 'Congrats, Teacher (Rajeev Kaushik) has accepted you in his batch(Class 10 ).', 'batch_id=7', '1', '23-12-2017 17:41:38', '23-12-2017 17:55:09', 0, 0),
(11, '8', '28', 'Batch Request', 'Congrats, Teacher (Rajeev Kaushik) has accepted you in his batch(Class 10 ).', 'batch_id=7', '0', '23-12-2017 17:57:06', NULL, 0, 1),
(12, '6', '30', 'Batch Request', 'Congrats, Teacher (Nishesh Sharma) has accepted you in his batch(Class 11 Biology ).', 'batch_id=10', '1', '24-12-2017 14:45:26', '24-12-2017 14:50:22', 0, 0),
(13, '1', '38', 'Session Request', 'Congrats,  Tutor (Swetak) has accepted you in his Session.', 'session_id=3', '0', '26-12-2017 11:46:17', NULL, 0, 0),
(14, '1', '44', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Hellooo).', 'batch_id=13', '0', '26-12-2017 14:18:01', NULL, 0, 0),
(15, '22', '58', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Sciece).', 'batch_id=16', '0', '28-12-2017 11:40:13', NULL, 0, 0),
(16, '22', '57', 'Batch Request', 'Congrats, Teacher (j.g) has accepted you in his batch(Tuy).', 'batch_id=17', '1', '28-12-2017 14:42:37', '29-12-2017 11:13:57', 0, 1),
(17, '1', '60', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Bat H).', 'batch_id=19', '0', '29-12-2017 15:33:08', NULL, 0, 0),
(18, '22', '60', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Bat H).', 'batch_id=19', '0', '29-12-2017 15:34:04', NULL, 0, 1),
(19, '22', '60', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Hhd).', 'batch_id=20', '0', '29-12-2017 18:22:25', NULL, 0, 1),
(20, '1', '60', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Bat H).', 'batch_id=19', '1', '01-01-2018 14:28:10', '17-01-2018 16:46:09', 0, 0),
(21, '22', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '1', '04-01-2018 16:21:09', '09-01-2018 11:22:26', 0, 1),
(22, '22', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '0', '08-01-2018 10:56:48', NULL, 0, 1),
(23, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '0', '08-01-2018 14:11:53', NULL, 0, 0),
(24, '22', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Science).', 'batch_id=5', '0', '08-01-2018 14:11:56', NULL, 0, 1),
(25, '20', '9', 'Session Request', 'Congrats,  Tutor (Smrati Priya) has accepted you in his Session.', 'session_id=8', '1', '09-01-2018 14:48:42', '24-01-2018 14:55:35', 0, 1),
(26, '1', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '0', '09-01-2018 16:06:24', NULL, 0, 0),
(27, '6', '9', 'Session Request', 'Congrats,  Tutor (smrati priya) has accepted you in his Session.', 'session_id=9', '1', '09-01-2018 18:13:49', '09-01-2018 18:13:55', 0, 0),
(28, '22', '9', 'Session Request', 'Sorry, Tutor (smrati priya) has declined you in Session.', 'session_id=4', '0', '10-01-2018 10:29:55', NULL, 0, 1),
(29, '1', '9', 'Session Request', 'Congrats,  Tutor (smrati priya) has accepted you in his Session.', 'session_id=10', '0', '10-01-2018 10:36:26', NULL, 0, 0),
(30, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Class 9).', 'batch_id=8', '0', '11-01-2018 12:49:08', NULL, 0, 1),
(31, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Class 9).', 'batch_id=8', '0', '11-01-2018 12:52:31', NULL, 0, 1),
(32, '17', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Class8).', 'batch_id=9', '1', '12-01-2018 12:17:58', '12-01-2018 18:25:07', 0, 1),
(33, '17', '49', 'Batch Request', 'Congrats, Teacher (Saurabh Singh Grover) has accepted your demo request for batch(Class8).', 'batch_id=9', '1', '12-01-2018 14:43:14', '12-01-2018 18:25:07', 0, 1),
(34, '17', '49', 'Batch Request', 'Congrats, Teacher (Saurabh Singh Grover) has accepted your demo request for batch(Class8).', 'batch_id=9', '1', '12-01-2018 15:04:00', '12-01-2018 18:25:08', 0, 1),
(35, '6', '61', 'Batch Request', 'Congrats, Teacher (Ashish chikkara) has accepted you in his batch(class 11).', 'batch_id=18', '1', '12-01-2018 18:10:18', '16-01-2018 18:37:54', 0, 1),
(36, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'demo_id=2', '1', '13-01-2018 16:42:45', '13-01-2018 16:46:29', 0, 0),
(37, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'demo_id=3', '1', '13-01-2018 16:45:28', '13-01-2018 16:46:30', 0, 0),
(38, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'demo_id=4', '1', '13-01-2018 16:46:43', '23-01-2018 11:58:34', 0, 0),
(39, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'demo_id=5', '1', '13-01-2018 16:52:12', '23-01-2018 11:58:35', 0, 0),
(40, '1', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Science).', 'batch_id=5', '1', '13-01-2018 20:42:50', '17-01-2018 16:45:48', 0, 1),
(41, '22', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '0', '15-01-2018 11:40:12', NULL, 0, 1),
(42, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=6', '1', '15-01-2018 16:25:06', '23-01-2018 11:58:36', 0, 0),
(43, '22', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=7', '0', '15-01-2018 16:44:56', NULL, 0, 1),
(44, '22', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=8', '1', '15-01-2018 18:20:19', '16-01-2018 13:01:56', 0, 1),
(45, '22', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=9', '0', '16-01-2018 13:19:07', NULL, 0, 1),
(46, '22', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted you in his batch(Science).', 'batch_id=5', '0', '16-01-2018 14:01:42', NULL, 0, 1),
(47, '22', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=10', '0', '16-01-2018 15:36:09', NULL, 0, 1),
(48, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Science).', 'batch_id=5', '0', '16-01-2018 16:35:08', NULL, 0, 0),
(49, '17', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted your demo request for batch(Class8).', 'batch_id=9', '1', '16-01-2018 18:39:10', '16-01-2018 18:40:41', 0, 1),
(50, '8', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(Class 10 ).', 'batch_id=7', '0', '16-01-2018 18:39:15', NULL, 0, 1),
(51, '2', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(Class8).', 'batch_id=9', '0', '16-01-2018 18:39:21', NULL, 0, 0),
(52, '17', '61', 'Batch Request', 'Congrats, Teacher (Ashish chikkara) has accepted your demo request for batch(class 11).', 'batch_id=18', '1', '16-01-2018 19:46:58', '16-01-2018 19:48:58', 0, 1),
(53, '2', '61', 'Batch Request', 'Sorry, Teacher (Ashish chikkara) has rejected your demo request for batch(class 11).', 'batch_id=18', '0', '16-01-2018 19:48:29', NULL, 0, 0),
(54, '2', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(10th Class).', 'batch_id=36', '0', '16-01-2018 19:51:15', NULL, 0, 0),
(55, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=13', '1', '17-01-2018 12:38:20', '17-01-2018 12:38:55', 0, 1),
(56, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=14', '1', '17-01-2018 12:41:48', '17-01-2018 12:42:14', 0, 1),
(57, '5', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=15', '0', '17-01-2018 12:44:56', NULL, 0, 1),
(58, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=16', '1', '17-01-2018 14:33:40', '17-01-2018 14:48:58', 0, 1),
(59, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=17', '1', '17-01-2018 14:39:18', '21-01-2018 13:48:57', 0, 1),
(60, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=18', '1', '17-01-2018 15:10:13', '17-01-2018 16:45:39', 0, 1),
(61, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=21', '1', '17-01-2018 16:22:15', '21-01-2018 13:48:57', 0, 1),
(62, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=22', '1', '17-01-2018 16:24:29', '21-01-2018 13:48:56', 0, 1),
(63, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=19', '0', '18-01-2018 16:27:19', NULL, 0, 1),
(64, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=29', '1', '18-01-2018 16:28:53', '21-01-2018 13:48:56', 0, 1),
(65, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=13', '1', '18-01-2018 16:31:28', '18-01-2018 16:32:53', 0, 1),
(66, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=30', '1', '18-01-2018 16:44:50', '21-01-2018 13:48:55', 0, 1),
(67, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=32', '1', '18-01-2018 16:52:35', '21-01-2018 13:48:54', 0, 1),
(68, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=13', '0', '18-01-2018 16:52:52', NULL, 0, 1),
(69, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=33', '1', '18-01-2018 16:56:57', '21-01-2018 13:48:54', 0, 1),
(70, '17', '61', 'Batch Request', 'Congrats, Teacher (Ashish chikkara) has accepted your demo request for batch(class 11).', 'batch_id=18', '1', '18-01-2018 16:57:36', '21-01-2018 18:20:47', 0, 1),
(71, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=34', '1', '18-01-2018 16:58:32', '21-01-2018 13:48:53', 0, 1),
(72, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=13', '0', '18-01-2018 16:58:45', NULL, 0, 1),
(73, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=13', '0', '18-01-2018 16:58:59', NULL, 0, 1),
(74, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Science).', 'batch_id=5', '1', '18-01-2018 21:06:55', '05-02-2018 16:44:51', 0, 0),
(75, '6', '15', 'Session Request', 'Sorry, Tutor (Rahul) has accepted your request for demo Session.', 'demo_id=35', '1', '18-01-2018 21:10:56', '20-01-2018 00:06:18', 0, 1),
(76, '6', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Fflfldkxkzkxkxkxkxk Kxkxkxkxxkxkxkxkxjzj).', 'batch_id=15', '1', '18-01-2018 22:28:34', '20-01-2018 00:06:09', 0, 1),
(77, '1', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=13', '0', '19-01-2018 10:40:48', NULL, 0, 1),
(78, '6', '15', 'Session Request', 'Sorry, Tutor (Rahul) has accepted your request for demo Session.', 'demo_id=16', '0', '20-01-2018 00:21:03', NULL, 0, 1),
(79, '25', '97', 'Batch Request', 'Congrats, Teacher (s.mohan) has accepted you in his batch(Jee).', 'batch_id=53', '1', '20-01-2018 13:55:39', '21-01-2018 13:48:52', 0, 1),
(80, '25', '97', 'Batch Request', 'Congrats, Teacher (s.mohan) has accepted your demo request for batch(Kilo).', 'batch_id=55', '1', '20-01-2018 14:38:34', '21-01-2018 13:48:52', 0, 1),
(81, '25', '15', 'Session Request', 'Sorry, Tutor (Rahul) has accepted your request for demo Session.', 'demo_id=18', '1', '20-01-2018 14:54:43', '21-01-2018 13:48:52', 0, 1),
(82, '25', '15', 'Session Request', 'Sorry, Tutor (Rahul) has accepted your request for demo Session.', 'demo_id=37', '1', '20-01-2018 15:08:09', '21-01-2018 13:48:45', 0, 1),
(83, '6', '15', 'Session Request', 'Sorry, Tutor (Rahul) has declined your request for demo Session.', 'demo_id=16', '0', '20-01-2018 15:10:09', NULL, 0, 1),
(84, '1', '15', 'Session Request', 'Sorry, Tutor (Rahul) has declined your request for demo Session.', 'demo_id=17', '0', '20-01-2018 15:10:13', NULL, 0, 1),
(85, '25', '15', 'Session Request', 'Sorry, Tutor (Rahul) has declined your request for demo Session.', 'demo_id=38', '1', '20-01-2018 15:12:23', '21-01-2018 13:48:44', 0, 1),
(86, '2', '39', 'Batch Request', 'Congrats, Teacher (Naved) has accepted your demo request for batch(Photoshop).', 'batch_id=42', '0', '20-01-2018 16:02:51', NULL, 0, 0),
(87, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted you in his batch(A1).', 'batch_id=56', '1', '20-01-2018 16:16:37', '21-01-2018 13:48:44', 0, 1),
(88, '6', '15', 'Session Request', 'Sorry, Tutor (Rahul) has accepted your request for demo Session.', 'demo_id=16', '0', '20-01-2018 22:00:04', NULL, 0, 1),
(89, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted your demo request for batch(2).', 'batch_id=57', '1', '21-01-2018 13:25:58', '21-01-2018 13:48:43', 0, 1),
(90, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted your demo request for batch(2).', 'batch_id=57', '1', '21-01-2018 13:30:37', '21-01-2018 13:48:38', 0, 1),
(91, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted your demo request for batch(2).', 'batch_id=57', '1', '21-01-2018 13:38:26', '21-01-2018 18:44:14', 0, 1),
(92, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted you in his batch(2).', 'batch_id=57', '1', '21-01-2018 13:38:28', '21-01-2018 13:48:36', 0, 1),
(93, '25', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(10th Class).', 'batch_id=36', '1', '21-01-2018 14:09:39', '21-01-2018 18:44:11', 0, 1),
(94, '25', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(10th Class).', 'batch_id=36', '1', '21-01-2018 14:09:40', '21-01-2018 18:44:09', 0, 1),
(95, '2', '103', 'Batch Request', 'Congrats, Teacher (Rishi Dua) has accepted your demo request for batch(9 Th Batch).', 'batch_id=58', '0', '21-01-2018 14:37:09', NULL, 0, 0),
(96, '2', '103', 'Batch Request', 'Congrats, Teacher (Rishi Dua) has accepted your demo request for batch(9 Th Batch).', 'batch_id=58', '0', '21-01-2018 14:38:07', NULL, 0, 1),
(97, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Science).', 'batch_id=5', '0', '22-01-2018 13:09:37', NULL, 0, 0),
(98, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Science).', 'batch_id=5', '0', '22-01-2018 13:09:40', NULL, 0, 0),
(99, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=19', '1', '22-01-2018 15:41:53', '22-01-2018 17:03:30', 0, 1),
(100, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=19', '1', '22-01-2018 15:42:17', '28-01-2018 18:19:19', 0, 1),
(101, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=19', '1', '22-01-2018 15:43:39', '28-01-2018 18:19:19', 0, 1),
(102, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '22-01-2018 16:38:03', NULL, 0, 1),
(103, '1', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Ie8its).', 'batch_id=52', '0', '22-01-2018 16:38:07', NULL, 0, 1),
(104, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=41', '1', '22-01-2018 17:31:31', '28-01-2018 18:19:19', 0, 1),
(105, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=42', '1', '22-01-2018 17:31:40', '28-01-2018 18:19:18', 0, 1),
(106, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=19', '1', '22-01-2018 17:31:56', '28-01-2018 18:19:17', 0, 1),
(107, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '23-01-2018 10:57:08', NULL, 0, 1),
(108, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '23-01-2018 10:57:37', NULL, 0, 1),
(109, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '23-01-2018 10:57:49', NULL, 0, 1),
(110, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Gdhd).', 'batch_id=31', '0', '23-01-2018 10:57:56', NULL, 0, 1),
(111, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '23-01-2018 11:31:19', NULL, 0, 1),
(112, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Gdhd).', 'batch_id=31', '0', '23-01-2018 11:31:30', NULL, 0, 1),
(113, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=43', '1', '23-01-2018 11:37:40', '28-01-2018 18:19:17', 0, 1),
(114, '25', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=19', '1', '23-01-2018 11:38:29', '23-01-2018 13:43:51', 0, 1),
(115, '22', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Batch 1).', 'batch_id=29', '0', '23-01-2018 13:40:28', NULL, 0, 1),
(116, '22', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Gdhd).', 'batch_id=31', '0', '23-01-2018 13:40:32', NULL, 0, 1),
(117, '1', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch(Hdhd).', 'batch_id=30', '0', '23-01-2018 13:40:34', NULL, 0, 1),
(118, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 15:48:56', '23-01-2018 17:06:45', 0, 1),
(119, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 15:49:59', '23-01-2018 17:06:45', 0, 1),
(120, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:03:41', '23-01-2018 17:06:45', 0, 1),
(121, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:04:07', '23-01-2018 17:06:46', 0, 1),
(122, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:07:22', '23-01-2018 17:06:46', 0, 1),
(123, '4', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:10:49', '23-01-2018 17:06:47', 0, 1),
(124, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:15:01', '23-01-2018 17:06:48', 0, 1),
(125, '4', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:17:00', '23-01-2018 17:06:48', 0, 1),
(126, '4', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:17:11', '23-01-2018 17:06:48', 0, 1),
(127, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:21:06', '23-01-2018 17:06:50', 0, 1),
(128, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:22:47', '23-01-2018 17:06:50', 0, 1),
(129, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:24:48', '23-01-2018 17:06:50', 0, 1),
(130, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:25:46', '23-01-2018 17:06:52', 0, 1),
(131, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:27:10', '23-01-2018 17:06:52', 0, 1),
(132, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:28:30', '23-01-2018 17:06:52', 0, 1),
(133, '25', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Math101).', 'batch_id=59', '1', '23-01-2018 16:28:51', '28-01-2018 18:19:15', 0, 1),
(134, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 16:58:12', '23-01-2018 17:06:52', 0, 1),
(135, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 17:02:08', '23-01-2018 17:06:53', 0, 1),
(136, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 17:03:18', '23-01-2018 17:06:54', 0, 1),
(137, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 17:03:55', '23-01-2018 17:06:54', 0, 1),
(138, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 17:04:03', '23-01-2018 17:06:54', 0, 1),
(139, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 17:04:48', '23-01-2018 17:06:55', 0, 1),
(140, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '1', '23-01-2018 18:27:51', '24-01-2018 11:13:52', 0, 1),
(141, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=44', '1', '23-01-2018 18:33:32', '28-01-2018 18:19:06', 0, 1),
(142, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=45', '1', '23-01-2018 18:33:34', '28-01-2018 18:19:06', 0, 1),
(143, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Batch 1).', 'batch_id=29', '0', '24-01-2018 10:16:10', NULL, 0, 1),
(144, '22', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Gdhd).', 'batch_id=31', '0', '24-01-2018 10:16:18', NULL, 0, 1),
(145, '1', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '0', '24-01-2018 11:08:41', NULL, 0, 1),
(146, '1', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '0', '24-01-2018 11:11:40', NULL, 0, 1),
(147, '1', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '0', '24-01-2018 11:13:33', NULL, 0, 1),
(148, '4', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '1', '24-01-2018 11:13:34', '24-01-2018 11:13:53', 0, 1),
(149, '1', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch(Gdhd).', 'batch_id=31', '0', '24-01-2018 11:15:08', NULL, 0, 1),
(150, '25', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '1', '24-01-2018 11:16:24', '28-01-2018 18:19:06', 0, 1),
(151, '25', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '1', '24-01-2018 11:18:02', '28-01-2018 18:19:04', 0, 1),
(152, '1', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch(Gdhd).', 'batch_id=31', '0', '24-01-2018 11:22:23', NULL, 0, 1),
(153, '4', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your request in his batch(Math101).', 'batch_id=59', '1', '24-01-2018 11:23:51', '25-01-2018 11:09:16', 0, 1),
(154, '4', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch(Ie8its).', 'batch_id=52', '1', '24-01-2018 11:25:38', '25-01-2018 11:09:16', 0, 1),
(155, '4', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Ie8its).', 'batch_id=52', '1', '24-01-2018 11:26:05', '24-01-2018 19:10:27', 0, 1),
(156, '1', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Gdhd).', 'batch_id=31', '0', '24-01-2018 11:28:45', NULL, 0, 1),
(157, '1', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Ixyug).', 'batch_id=51', '0', '24-01-2018 11:36:47', NULL, 0, 1),
(158, '30', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Math101).', 'batch_id=59', '1', '24-01-2018 11:38:44', '30-01-2018 17:03:22', 0, 1),
(159, '30', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Ie8its).', 'batch_id=52', '1', '24-01-2018 11:40:24', '30-01-2018 17:03:21', 0, 1),
(160, '30', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Gdhd).', 'batch_id=31', '1', '24-01-2018 11:42:55', '30-01-2018 17:03:20', 0, 0),
(161, '30', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Gdhd).', 'batch_id=31', '1', '24-01-2018 11:53:32', '30-01-2018 17:03:20', 0, 0),
(162, '30', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Ixyug).', 'batch_id=51', '1', '24-01-2018 11:59:14', '02-02-2018 18:17:56', 0, 0),
(163, '30', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Gdhd).', 'batch_id=31', '1', '24-01-2018 12:56:57', '30-01-2018 17:03:18', 0, 0),
(164, '30', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Jdjd).', 'batch_id=62', '1', '24-01-2018 13:02:25', '30-01-2018 17:03:18', 0, 0),
(165, '30', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch().', 'batch_id=63', '0', '24-01-2018 13:06:37', NULL, 0, 0),
(166, '30', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch().', 'batch_id=63', '0', '24-01-2018 13:12:49', NULL, 0, 0),
(167, '30', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Helloo).', 'batch_id=64', '0', '24-01-2018 14:19:37', NULL, 0, 0),
(168, '30', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Helloo).', 'batch_id=64', '0', '24-01-2018 14:33:00', NULL, 0, 0),
(169, '20', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Helloo).', 'batch_id=64', '1', '24-01-2018 14:55:22', '24-01-2018 14:55:41', 0, 1),
(170, '20', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Ie8its).', 'batch_id=52', '1', '24-01-2018 15:03:51', '24-01-2018 15:04:15', 0, 1),
(171, '25', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Ie8its).', 'batch_id=52', '1', '24-01-2018 15:04:17', '28-01-2018 18:19:04', 0, 1),
(172, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=22', '0', '24-01-2018 15:25:50', NULL, 0, 0),
(173, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=23', '0', '24-01-2018 15:41:54', NULL, 0, 0),
(174, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=25', '0', '24-01-2018 15:42:28', NULL, 0, 0),
(175, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=26', '0', '24-01-2018 15:42:50', NULL, 0, 0),
(176, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 15:57:20', NULL, 0, 0),
(177, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 15:59:09', NULL, 0, 0),
(178, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=23', '0', '24-01-2018 15:59:21', NULL, 0, 0),
(179, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 15:59:26', NULL, 0, 0),
(180, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 15:59:28', NULL, 0, 0),
(181, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has accepted your request for demo Session.', 'demo_id=23', '0', '24-01-2018 16:01:51', NULL, 0, 0),
(182, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=23', '0', '24-01-2018 16:15:38', NULL, 0, 0),
(183, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=23', '0', '24-01-2018 16:15:41', NULL, 0, 0),
(184, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=23', '0', '24-01-2018 16:15:54', NULL, 0, 0),
(185, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=27', '0', '24-01-2018 16:17:16', NULL, 0, 0),
(186, '4', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=28', '0', '24-01-2018 16:20:54', NULL, 0, 0),
(187, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=54', '1', '24-01-2018 16:25:03', '28-01-2018 18:19:04', 0, 1),
(188, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=57', '1', '24-01-2018 16:25:06', '28-01-2018 18:19:03', 0, 1),
(189, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=56', '1', '24-01-2018 16:25:19', '28-01-2018 18:19:02', 0, 1),
(190, '20', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=59', '0', '24-01-2018 16:25:22', NULL, 0, 1),
(191, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=22', '0', '24-01-2018 16:25:30', NULL, 0, 0),
(192, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=22', '0', '24-01-2018 16:25:39', NULL, 0, 0),
(193, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=60', '0', '24-01-2018 16:28:24', NULL, 0, 0),
(194, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 16:28:48', NULL, 0, 0),
(195, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 16:30:23', NULL, 0, 0),
(196, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 16:30:23', NULL, 0, 0),
(197, '30', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined you in Session.', 'session_id=', '0', '24-01-2018 16:30:52', NULL, 0, 0),
(198, '4', '49', 'Session Request', 'Sorry, Tutor (Saurabh Singh Grover) has declined your request for demo Session.', 'demo_id=60', '0', '24-01-2018 16:32:39', NULL, 0, 0),
(199, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=29', '0', '24-01-2018 16:33:59', NULL, 0, 0),
(200, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=22', '0', '24-01-2018 16:35:18', NULL, 0, 0),
(201, '30', '49', 'Session Request', 'Congrats,  Tutor (Saurabh Singh Grover) has accepted you in his Session.', 'session_id=30', '0', '24-01-2018 16:36:12', NULL, 0, 0),
(202, '4', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined you in Session.', 'session_id=', '0', '24-01-2018 16:39:46', NULL, 0, 0),
(203, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=61', '0', '24-01-2018 16:45:29', NULL, 0, 0),
(204, '25', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=62', '1', '24-01-2018 16:59:31', '28-01-2018 18:19:02', 0, 1),
(205, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined your request for demo Session.', 'demo_id=63', '0', '24-01-2018 17:07:16', NULL, 0, 0),
(206, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=64', '0', '24-01-2018 17:12:09', NULL, 0, 0),
(207, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=65', '0', '24-01-2018 17:16:02', NULL, 0, 0),
(208, '30', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=31', '0', '24-01-2018 17:24:23', NULL, 0, 0),
(209, '4', '87', 'Session Request', 'Congrats Wow,  Tutor (Amit) has accepted you in his Session.', 'session_id=22', '0', '24-01-2018 17:28:54', NULL, 0, 0),
(210, '30', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=32', '0', '24-01-2018 17:37:47', NULL, 0, 0),
(211, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=66', '0', '24-01-2018 17:51:39', NULL, 0, 0),
(212, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has declined you in Session.', 'session_id=', '0', '24-01-2018 17:52:14', NULL, 0, 0),
(213, '30', '87', 'Session Request', 'Sorry, Tutor (Amit) has accepted your request for demo Session.', 'demo_id=67', '0', '24-01-2018 17:59:03', NULL, 0, 0),
(214, '30', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=34', '0', '24-01-2018 17:59:32', NULL, 0, 0),
(215, '20', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=35', '0', '24-01-2018 18:23:20', NULL, 0, 1),
(216, '2', '39', 'Batch Request', 'Congrats, Teacher (Naved) has accepted your demo request for batch(Photoshop).', 'batch_id=42', '0', '24-01-2018 18:32:49', NULL, 0, 1),
(217, '20', '39', 'Batch Request', 'Congrats, Teacher (Naved) has accepted you in his batch(Photoshop).', 'batch_id=42', '0', '24-01-2018 18:32:56', NULL, 0, 1),
(218, '30', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=36', '0', '24-01-2018 18:38:15', NULL, 0, 0),
(219, '25', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=37', '1', '24-01-2018 19:13:27', '28-01-2018 18:19:01', 0, 0),
(220, '30', '9', 'Session Request', 'Congrats,  Tutor (smrati priya) has accepted you in his Session.', 'session_id=38', '0', '25-01-2018 11:03:10', NULL, 0, 0),
(221, '20', '9', 'Session Request', 'Sorry, Tutor (smrati priya) has accepted your request for demo Session.', 'demo_id=68', '1', '25-01-2018 11:20:33', '25-01-2018 11:21:44', 0, 1),
(222, '20', '9', 'Session Request', 'Sorry, Tutor (smrati priya) has accepted your request for demo Session.', 'demo_id=69', '0', '25-01-2018 11:22:47', NULL, 0, 1),
(223, '20', '9', 'Session Request', 'Congrats,  Tutor (smrati priya) has accepted you in his Session.', 'session_id=39', '1', '25-01-2018 11:29:22', '25-01-2018 16:12:41', 0, 1),
(224, '30', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted you in his batch(Hhdhhd).', 'batch_id=66', '0', '25-01-2018 11:52:46', NULL, 0, 0),
(225, '30', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted you in his batch().', 'batch_id=68', '1', '25-01-2018 16:55:10', '30-01-2018 17:03:16', 0, 0),
(226, '3', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your demo request for batch(Jdjd).', 'batch_id=73', '1', '25-01-2018 18:35:15', '25-01-2018 19:18:29', 0, 0),
(227, '25', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted your demo request for batch(2).', 'batch_id=57', '1', '25-01-2018 21:35:41', '28-01-2018 18:18:59', 0, 1),
(228, '29', '99', 'Batch Request', 'Congrats, Teacher (Rohit Khatri) has accepted your demo request for batch(2).', 'batch_id=57', '1', '25-01-2018 21:35:46', '13-02-2018 00:57:11', 0, 1),
(229, '20', '39', 'Batch Request', 'Congrats, Teacher (Naved) has accepted your demo request for batch(gwd).', 'batch_id=75', '1', '26-01-2018 11:56:15', '08-02-2018 17:37:01', 0, 1),
(230, '6', '15', 'Session Request', 'Congrats,  Tutor (Rahul) has accepted you in his Session.', 'session_id=16', '0', '27-01-2018 23:17:26', NULL, 0, 1),
(231, '5', '107', 'Batch Request', 'Sorry, Teacher (Swetank It) has rejected your demo request for batch(Ndndbdhd).', 'batch_id=85', '0', '29-01-2018 17:07:18', NULL, 0, 1),
(232, '3', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted your demo request for batch(Ghhhhd).', 'batch_id=81', '1', '29-01-2018 17:07:32', '30-01-2018 11:39:39', 0, 0),
(233, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted you in his batch(Ndndbdhd).', 'batch_id=85', '0', '29-01-2018 17:40:41', NULL, 0, 1),
(234, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted you in his batch(Ghhhhd).', 'batch_id=81', '0', '29-01-2018 17:40:44', NULL, 0, 1),
(235, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted you in his batch(Hhdh).', 'batch_id=78', '0', '29-01-2018 17:40:46', NULL, 0, 1),
(236, '5', '107', 'Batch Request', 'Sorry, Teacher (Swetank It) has rejected your request in his batch(Hdhhd).', 'batch_id=83', '0', '29-01-2018 17:53:33', NULL, 0, 1),
(237, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted your demo request for batch(Hdhdh).', 'batch_id=79', '0', '29-01-2018 18:01:26', NULL, 0, 1),
(238, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted your demo request for batch(Hhdhhd).', 'batch_id=66', '0', '29-01-2018 18:21:07', NULL, 0, 1),
(239, '5', '107', 'Batch Request', 'Congrats, Teacher (Swetank It) has accepted your demo request for batch(Hhdhhd).', 'batch_id=66', '0', '29-01-2018 18:27:16', NULL, 0, 1),
(240, '25', '74', 'Batch Request', 'Sorry, Teacher (Amit K) has rejected your demo request for batch(Jdjd).', 'batch_id=73', '0', '02-02-2018 11:10:50', NULL, 0, 1),
(241, '5', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your request in his batch(Ie8its).', 'batch_id=52', '0', '02-02-2018 11:23:57', NULL, 0, 1),
(242, '6', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted your demo request for batch(10th Class).', 'batch_id=36', '0', '02-02-2018 12:33:36', NULL, 0, 1),
(243, '2', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted your demo request for batch(10th Class).', 'batch_id=36', '1', '02-02-2018 12:33:40', '05-02-2018 16:44:43', 0, 1),
(244, '5', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=42', '0', '02-02-2018 15:44:04', NULL, 0, 1),
(245, '4', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=40', '1', '02-02-2018 15:49:29', '02-02-2018 16:18:44', 0, 1),
(246, '25', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=45', '0', '02-02-2018 15:49:32', NULL, 0, 1),
(247, '25', '87', 'Session Request', 'Congrats,  Tutor (Amit) has accepted you in his Session.', 'session_id=46', '0', '02-02-2018 15:49:33', NULL, 0, 1),
(248, '30', '9', 'Session Request', 'Sorry, Tutor (smrati priya) has declined you in Session.', 'session_id=', '0', '02-02-2018 17:29:35', NULL, 0, 0),
(249, '30', '136', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted your demo request for batch(Ola To Uber -uber To Ola).', 'batch_id=91', '0', '02-02-2018 18:08:19', NULL, 0, 0),
(250, '30', '136', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Dhdhsaowoeiejejjejndjdjjdkskksjjdjdjddksssskksjs).', 'batch_id=87', '0', '02-02-2018 18:12:08', NULL, 0, 0),
(251, '30', '136', 'Batch Request', 'Congrats, Teacher (Swetank) has accepted you in his batch(Hlooo).', 'batch_id=86', '0', '02-02-2018 18:12:11', NULL, 0, 0),
(252, '30', '9', 'Session Request', 'Congrats, Tutor (smrati priya) has accepted your request for demo Session.', 'demo_id=71', '0', '03-02-2018 12:35:36', NULL, 0, 0),
(253, '30', '9', 'Session Request', 'Congrats,  Tutor (smrati priya) has accepted you in his Session.', 'session_id=47', '0', '03-02-2018 12:35:39', NULL, 0, 0),
(254, '3', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted your demo request for batch(Jdjd).', 'batch_id=73', '1', '05-02-2018 16:15:16', '06-02-2018 13:35:16', 0, 1),
(255, '30', '164', 'Batch Request', 'Congrats, Teacher (Aman RajpuT) has accepted your demo request for batch(Maths Classes).', 'batch_id=96', '0', '06-02-2018 14:56:51', NULL, 0, 1),
(256, '30', '164', 'Batch Request', 'Congrats, Teacher (Aman RajpuT) has accepted you in his batch(Maths Classes).', 'batch_id=95', '0', '06-02-2018 14:57:51', NULL, 0, 1),
(257, '5', '164', 'Batch Request', 'Congrats, Teacher (Aman RajpuT) has accepted you in his batch(Maths Classes).', 'batch_id=95', '0', '06-02-2018 15:03:15', NULL, 0, 1),
(258, '30', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Gdhd).', 'batch_id=31', '0', '06-02-2018 15:27:39', NULL, 0, 1),
(259, '5', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted your demo request for batch(Ie8its).', 'batch_id=52', '0', '06-02-2018 15:30:32', NULL, 0, 1),
(260, '30', '136', 'Batch Request', 'Congrats, Teacher (Swetank ) has accepted your demo request for batch(Olooo).', 'batch_id=100', '0', '07-02-2018 11:15:46', NULL, 0, 1),
(261, '17', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(Maths Special).', 'batch_id=101', '1', '07-02-2018 15:29:27', '07-02-2018 15:31:16', 0, 1),
(262, '17', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(10th Class).', 'batch_id=36', '1', '07-02-2018 15:29:54', '08-02-2018 15:34:09', 0, 1),
(263, '17', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted your demo request for batch(1st).', 'batch_id=33', '1', '07-02-2018 15:31:02', '07-02-2018 15:31:13', 0, 1),
(264, '30', '24', 'Session Request', 'Congrats, Tutor (Jyoti Behwal Reniwal) has accepted your request for demo Session.', 'demo_id=70', '0', '08-02-2018 13:17:34', NULL, 0, 1),
(265, '30', '24', 'Session Request', 'Congrats,  Tutor (Jyoti Behwal Reniwal) has accepted you in his Session.', 'session_id=49', '0', '08-02-2018 13:17:41', NULL, 0, 1),
(266, '30', '24', 'Session Request', 'Congrats,  Tutor (Jyoti Behwal Reniwal) has accepted you in his Session.', 'session_id=50', '0', '08-02-2018 13:17:48', NULL, 0, 1),
(267, '20', '136', 'Batch Request', 'Congrats, Teacher (Swetank ) has accepted your demo request for batch(What To Do).', 'batch_id=112', '0', '09-02-2018 14:22:00', NULL, 0, 1),
(268, '30', '9', 'Session Request', 'Congrats, Tutor (smrati priya) has accepted your request for demo Session.', 'demo_id=72', '0', '09-02-2018 14:26:51', NULL, 0, 1),
(269, '2', '2', 'Batch Request', 'Congrats, Teacher (Rahul) has accepted your demo request for batch(Science).', 'batch_id=5', '0', '10-02-2018 18:31:22', NULL, 0, 1),
(270, '6', '24', 'Session Request', 'Congrats, Tutor (Jyoti Behwal Reniwal) has accepted your request for demo Session.', 'demo_id=75', '0', '11-02-2018 13:06:07', NULL, 0, 1),
(271, '42', '39', 'Batch Request', 'Congrats, Teacher (Naved) has accepted you in his batch(Fee Pay).', 'batch_id=124', '1', '12-02-2018 14:19:40', '16-02-2018 12:55:55', 0, 0),
(272, '2', '183', 'Batch Request', 'Congrats, Teacher (Ashish) has accepted your demo request for batch(class 9).', 'batch_id=107', '0', '12-02-2018 15:46:25', NULL, 0, 1),
(273, '42', '80', 'Batch Request', 'Congrats, Teacher (Satyender Pal) has accepted you in his batch(Maths Special).', 'batch_id=101', '1', '12-02-2018 16:50:59', '12-02-2018 16:57:00', 0, 1),
(274, '42', '80', 'Batch Request', 'Sorry, Teacher (Satyender Pal) has rejected your request in his batch(10th Class).', 'batch_id=36', '1', '12-02-2018 16:56:19', '12-02-2018 16:56:52', 0, 1),
(275, '23', '138', 'Batch Request', 'Sorry, Teacher (RACHNA JAIN) has rejected your demo request for batch(CLASS-9).', 'batch_id=128', '0', '13-02-2018 15:37:02', NULL, 0, 1),
(276, '1', '15', 'Session Request', 'Congrats,  Tutor (Rahul) has accepted you in his Session.', 'session_id=17', '0', '13-02-2018 19:00:56', NULL, 0, 1),
(277, '25', '15', 'Session Request', 'Congrats,  Tutor (Rahul) has accepted you in his Session.', 'session_id=18', '0', '13-02-2018 19:01:03', NULL, 0, 1),
(278, '6', '15', 'Session Request', 'Congrats,  Tutor (Rahul) has accepted you in his Session.', 'session_id=52', '0', '13-02-2018 19:01:04', NULL, 0, 1),
(279, '42', '176', 'Session Request', 'Congrats,  Tutor (Deeksha Gupta) has accepted you in his Session.', 'session_id=53', '1', '15-02-2018 00:01:54', '16-02-2018 12:56:05', 0, 1),
(280, '42', '176', 'Session Request', 'Congrats,  Tutor (Deeksha Gupta) has accepted you in his Session.', 'session_id=54', '1', '15-02-2018 00:02:00', '16-02-2018 12:56:06', 0, 1),
(281, '42', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=77', '1', '15-02-2018 14:54:21', '16-02-2018 12:56:07', 0, 1),
(282, '25', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=78', '0', '15-02-2018 15:07:58', NULL, 0, 1),
(283, '25', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=79', '0', '15-02-2018 15:25:25', NULL, 0, 1),
(284, '42', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=80', '1', '20-02-2018 14:03:45', '20-02-2018 14:09:07', 0, 1),
(285, '30', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Feb).', 'batch_id=88', '0', '21-02-2018 15:42:22', NULL, 0, 1),
(286, '5', '75', 'Batch Request', 'Congrats, Teacher (Saurabh Singh) has accepted you in his batch(Feb).', 'batch_id=88', '0', '21-02-2018 15:42:24', NULL, 0, 1),
(287, '42', '176', 'Session Request', 'Congrats,  Tutor (Deeksha Gupta) has accepted you in his Session.', 'session_id=56', '0', '28-02-2018 11:39:59', NULL, 0, 1);
INSERT INTO `all_notifications` (`id`, `notif_to`, `notif_from`, `title`, `description`, `link`, `is_read`, `added_date`, `read_date`, `type`, `status`) VALUES
(288, '42', '176', 'Session Request', 'Congrats,  Tutor (Deeksha Gupta) has accepted you in his Session.', 'session_id=55', '0', '28-02-2018 11:40:04', NULL, 0, 1),
(289, '42', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=82', '0', '28-02-2018 11:40:07', NULL, 0, 1),
(290, '42', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=81', '0', '28-02-2018 11:41:16', NULL, 0, 1),
(291, '42', '75', 'Batch Request', 'Sorry, Teacher (Saurabh Singh) has rejected your demo request for batch(Testing).', 'batch_id=155', '0', '05-03-2018 15:41:54', NULL, 0, 1),
(292, '42', '176', 'Session Request', 'Congrats,  Tutor (Deeksha Gupta) has accepted you in his Session.', 'session_id=57', '0', '06-03-2018 12:02:36', NULL, 0, 1),
(293, '20', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Chat Test).', 'batch_id=163', '0', '10-03-2018 13:23:38', NULL, 0, 1),
(294, '25', '248', 'Session Request', 'Congrats,  Tutor (Vikash Dholakiya) has accepted you in his Session.', 'session_id=58', '0', '10-03-2018 15:01:20', NULL, 0, 1),
(295, '4', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Chat Test).', 'batch_id=163', '0', '10-03-2018 17:32:11', NULL, 0, 1),
(296, '25', '74', 'Batch Request', 'Congrats, Teacher (Amit K) has accepted you in his batch(Chat Test).', 'batch_id=163', '0', '12-03-2018 10:53:04', NULL, 0, 1),
(297, '42', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted your demo request for batch(Bske).', 'batch_id=162', '0', '12-03-2018 12:03:12', NULL, 0, 1),
(298, '42', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Bske).', 'batch_id=162', '0', '12-03-2018 12:17:04', NULL, 0, 1),
(299, '121', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Bske).', 'batch_id=162', '0', '12-03-2018 12:34:23', NULL, 0, 1),
(300, '6', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Bske).', 'batch_id=162', '0', '12-03-2018 12:35:42', NULL, 0, 1),
(301, '85', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Yee).', 'batch_id=165', '0', '14-03-2018 16:41:09', NULL, 0, 1),
(302, '124', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Yee).', 'batch_id=165', '1', '15-03-2018 15:21:21', '15-03-2018 15:24:37', 0, 1),
(303, '124', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted your demo request for batch(Bske).', 'batch_id=162', '1', '15-03-2018 15:39:28', '15-03-2018 15:39:41', 0, 1),
(304, '124', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted your demo request for batch(Bnk).', 'batch_id=164', '1', '15-03-2018 15:46:58', '15-03-2018 15:54:01', 0, 1),
(305, '124', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted your demo request for batch(Bnk).', 'batch_id=164', '1', '15-03-2018 15:49:07', '15-03-2018 15:54:02', 0, 1),
(306, '124', '241', 'Batch Request', 'Congrats, Teacher (deeksha gupta) has accepted you in his batch(Bske).', 'batch_id=162', '1', '15-03-2018 15:52:31', '15-03-2018 15:54:02', 0, 1),
(307, '124', '241', 'Batch Request', 'Congrats, Teacher (pinky) has accepted you in his batch(Bnk).', 'batch_id=164', '1', '15-03-2018 18:13:51', '15-03-2018 18:15:03', 0, 1),
(308, '124', '241', 'Batch Request', 'Congrats, Teacher (pinky) has accepted your demo request for batch(Hinku).', 'batch_id=166', '1', '16-03-2018 11:36:55', '16-03-2018 11:37:04', 0, 1),
(309, '124', '176', 'Session Request', 'Congrats, Tutor (Deeksha Gupta) has accepted your request for demo Session.', 'demo_id=83', '1', '16-03-2018 11:50:06', '16-03-2018 11:50:15', 0, 1),
(310, '124', '241', 'Batch Request', 'Congrats, Teacher (pinky) has accepted you in his batch(Hinku).', 'batch_id=166', '1', '16-03-2018 12:26:24', '16-03-2018 14:28:22', 0, 1),
(311, '124', '241', 'Batch Request', 'Congrats, Teacher (pinky) has accepted you in his batch(Hinku).', 'batch_id=166', '1', '16-03-2018 14:27:30', '16-03-2018 14:28:26', 0, 1),
(312, '124', '249', 'Batch Request', 'Congrats, Teacher (Neha) has accepted your demo request for batch(Bwuw).', 'batch_id=169', '0', '16-03-2018 15:31:16', NULL, 0, 1),
(313, '124', '249', 'Batch Request', 'Congrats, Teacher (Neha) has accepted you in his batch(Mm).', 'batch_id=168', '1', '16-03-2018 15:31:19', '16-03-2018 15:54:48', 0, 1),
(314, '124', '249', 'Batch Request', 'Congrats, Teacher (Neha) has accepted you in his batch(Bwuw).', 'batch_id=169', '1', '16-03-2018 16:12:07', '16-03-2018 16:16:23', 0, 1),
(315, '124', '250', 'Session Request', 'Congrats,  Tutor (Megha) has accepted you in his Session.', 'session_id=61', '0', '16-03-2018 17:04:25', NULL, 0, 1),
(316, '125', '253', 'Batch Request', 'Congrats, Teacher (Deeksha) has accepted your demo request for batch(Gh).', 'batch_id=170', '0', '04-04-2018 18:56:00', NULL, 0, 1),
(317, '125', '253', 'Batch Request', 'Congrats, Teacher (Deeksha) has accepted you in his batch(Gh).', 'batch_id=170', '0', '04-04-2018 18:56:54', NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `answerofquestion`
--

CREATE TABLE `answerofquestion` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `answer_option` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_class`
--

CREATE TABLE `app_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL COMMENT '0:inactive,1:active',
  `courses_id` varchar(11) NOT NULL,
  `trending` int(10) NOT NULL DEFAULT '0' COMMENT '0:no, 1:yes'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_class`
--

INSERT INTO `app_class` (`id`, `name`, `status`, `courses_id`, `trending`) VALUES
(1, 'CLASSES I-V', '1', '1', 0),
(261, 'English', '1', '40', 0),
(6, 'CLASS VI', '1', '1', 0),
(7, 'CLASS VII', '1', '1', 0),
(8, 'CLASS VIII', '1', '1', 0),
(9, 'CLASS IX', '1', '36', 0),
(10, 'CLASS X', '1', '36', 1),
(11, 'CLASS XI', '1', '36', 1),
(12, 'CLASS XII', '1', '36', 1),
(13, 'PROGRAMMING LANGUAGE', '1', '5', 0),
(14, 'APPLICATION SOFTWARE', '1', '5', 0),
(15, 'GRAPHIC/ANIMATION', '1', '5', 0),
(16, 'NETWORKING', '1', '5', 0),
(23, 'PAYROLL TRAINING', '1', '6', 0),
(24, 'PERSONALITY DEVELOPMENT', '1', '6', 0),
(25, 'COMPENSATION OF BENEFITS', '1', '6', 0),
(26, 'LINEAR PIRATE TRAINING', '1', '6', 0),
(27, 'SOFT SKILLS', '1', '6', 0),
(28, 'NEGOTIATION', '1', '6', 0),
(29, 'BODY LANGUAGE', '1', '6', 0),
(30, 'TEAM BUILDING', '1', '6', 0),
(31, 'LEADERSHIP', '1', '6', 0),
(32, 'PUBLIC SPEAKING', '1', '6', 0),
(33, 'WRITTEN COMMUNICATION', '1', '6', 0),
(34, 'INTERVIEW TECHNIQUE', '1', '6', 0),
(35, 'EMPLOYEE RELATION', '1', '6', 0),
(36, 'ENGINEERING', '0', '2', 0),
(37, 'MEDICAL', '0', '2', 0),
(38, 'MARINE,NAVY(DEFENCE)', '0', '2', 0),
(39, 'FASHION AND DESIGN', '0', '2', 0),
(40, 'HUMANITY & SOCIAL SCIENCE', '0', '2', 0),
(41, 'AGRICULTURE & HOTEL MANAGEMENT', '0', '2', 0),
(93, 'Mathematical Hons.', '1', '12', 0),
(92, 'JEE Mains', '1', '2', 0),
(46, 'INDOOR', '0', '8', 0),
(47, 'OUTDOOR', '0', '8', 0),
(169, 'Gate CS', '1', '13', 0),
(49, 'DEFENCE', '0', '3', 0),
(106, 'B.Sc. Nursing', '1', '12', 0),
(109, 'AIPMT', '1', '3', 0),
(53, 'M.TECH & ME', '0', '3', 0),
(54, 'GOVERNMENT JOBS', '0', '3', 0),
(55, 'ACADEMICS', '0', '3', 0),
(108, 'AIIMS', '1', '3', 0),
(107, 'B.Sc. Agriculture', '1', '12', 0),
(58, 'OTHERS', '0', '3', 0),
(192, 'SBI CLERK exam', '1', '15', 0),
(191, 'SBI PO', '1', '15', 0),
(61, 'RRB', '0', '3', 0),
(62, 'AFCAT', '0', '3', 0),
(63, 'C.A', '0', '3', 0),
(64, 'C.S', '0', '3', 0),
(65, 'TYPING ', '0', '3', 0),
(66, 'BACHELOR OF HOME SCIENCE', '0', '7', 0),
(173, 'Gate CIVIL', '1', '13', 0),
(68, 'BACHELOR OF EDUCATION(B.ED)', '0', '7', 0),
(69, 'DANCE', '1', '9', 0),
(70, 'MUSIC', '0', '9', 0),
(71, 'WRITING/READING', '0', '9', 0),
(72, 'Media', '0', '9', 0),
(73, 'INSTRUMENTS', '1', '9', 0),
(74, 'PHOTOGRAPHY', '1', '9', 0),
(75, 'COOKING', '1', '9', 0),
(76, 'ARCHITECTURE/INTERIOR DESIGINING', '0', '9', 0),
(77, 'FASHION DESIGINING', '0', '9', 0),
(78, 'ART & CRAFT', '1', '9', 0),
(172, 'Gate ME', '1', '13', 0),
(80, 'HUMANITIES', '0', '7', 0),
(81, 'DEFENCE', '0', '1', 0),
(171, 'Gate ECE', '1', '13', 0),
(170, 'Gate EE', '1', '13', 0),
(264, 'German', '1', '40', 0),
(262, 'Sanskrit', '1', '40', 0),
(263, 'French', '1', '40', 0),
(260, 'B.COM', '1', '7', 0),
(90, 'Chemistry Hons.', '1', '12', 0),
(89, 'Physics hons.', '1', '12', 0),
(91, 'Computer Science Hons.', '1', '12', 0),
(94, 'JEE Advanced', '1', '2', 0),
(95, 'BITSAT', '1', '2', 0),
(96, 'IIT-JAM', '1', '2', 0),
(97, 'SRMJEEE', '1', '2', 0),
(116, 'AIEEE', '1', '2', 0),
(99, 'Biotechnology', '1', '12', 0),
(120, 'COMEDK ', '1', '3', 0),
(101, 'Biochemistry', '1', '12', 0),
(102, 'Physical Science', '1', '12', 0),
(104, 'Zoology', '1', '12', 0),
(105, 'Horticulture', '1', '12', 0),
(111, 'NEET', '1', '3', 0),
(112, 'GUJCET', '1', '3', 0),
(118, 'BHU PMT ', '1', '3', 0),
(119, 'AIPVT', '1', '3', 0),
(115, 'AFMC', '1', '3', 0),
(117, 'VITEEE', '1', '2', 0),
(121, 'DU PGMET', '1', '3', 0),
(124, 'COMEDK PGET', '1', '3', 0),
(125, 'EAMCET Medical', '1', '3', 0),
(126, 'CMC Vellore ', '1', '3', 0),
(127, 'CLAT ', '1', '16', 0),
(128, 'AILET', '1', '16', 0),
(129, 'LSAT', '1', '16', 0),
(130, 'AIL ', '1', '16', 0),
(131, 'AIBE', '1', '16', 0),
(132, 'SET', '1', '16', 0),
(133, 'BVP CET LAW', '1', '16', 0),
(134, 'DU LLB', '1', '16', 0),
(135, 'AMU LAW', '1', '16', 0),
(136, 'BHU-UET', '1', '16', 0),
(137, 'IMU-CET', '1', '17', 0),
(138, 'Indian Navy B.tech Entry Scheme', '1', '17', 0),
(139, 'Indian Navy Sailors', '1', '17', 0),
(140, 'Indian Army Technical Entry Scheme', '1', '17', 0),
(141, 'National Defence Academy And Naval Academy Examination', '1', '17', 0),
(142, 'AFCAT', '1', '17', 0),
(143, 'Indian Navy Dockyard Apprentices Exam', '1', '17', 0),
(144, 'CAT', '1', '18', 0),
(145, 'XAT', '1', '18', 0),
(146, 'DU JAT', '1', '18', 0),
(147, 'IPMAT', '1', '18', 0),
(148, 'MAT ', '1', '18', 0),
(149, 'CMAT', '1', '18', 0),
(150, 'IIFT ', '1', '18', 0),
(151, 'NMAT by GMAC', '1', '18', 0),
(152, 'MAH-CET', '1', '18', 0),
(153, 'AIMA-UGAT', '1', '18', 0),
(154, 'SET', '1', '18', 0),
(155, 'XIMB', '1', '18', 0),
(156, 'NIFT', '1', '19', 0),
(157, 'National Institute Of Design', '1', '19', 0),
(158, 'AIEED', '1', '19', 0),
(159, 'AIFD', '1', '19', 0),
(160, 'CEED', '1', '19', 0),
(161, 'FDDI AIST', '1', '19', 0),
(162, 'GD Goenka DAT.', '1', '19', 0),
(163, 'IIAD', '1', '19', 0),
(164, 'IIHM ', '1', '20', 0),
(165, 'MAH HM CET', '1', '20', 0),
(166, 'NCHMCT JEE', '1', '20', 0),
(167, 'WBJEE JEHOM', '1', '20', 0),
(168, 'PUTHAT', '1', '20', 0),
(174, 'Gate IN', '1', '13', 0),
(175, 'SSC CPO(S.I) exam', '1', '14', 0),
(176, 'SSC CGL exam', '1', '14', 0),
(177, 'SSC CHSL exam', '1', '14', 0),
(180, 'JT(Junior Translator) CSOL exam', '1', '14', 0),
(179, 'SSC JE(Junior engineer) exam', '1', '14', 0),
(181, 'RTCG (Railway Technical Cadre exam)', '1', '14', 0),
(182, 'RRB NTPC exam', '1', '14', 0),
(183, 'RPF (Sub-Inspector/Constable) exam', '1', '14', 0),
(184, 'Gate BT', '1', '13', 0),
(185, 'sports', '0', '8', 0),
(186, 'co-curricular', '0', '8', 0),
(187, 'Overseas/Foreign education exams Tutions', '1', '22', 0),
(189, 'Teaching Exams Tuitions', '1', '22', 0),
(190, 'SPSC(State Public Service Commision)', '1', '22', 0),
(193, 'SBI Specialist Officer exam', '1', '15', 0),
(194, 'RBI exam', '1', '15', 0),
(195, 'RBI Assistant Exam', '1', '15', 0),
(198, 'IBPS Clerk exam', '1', '15', 0),
(197, 'IBPS PO/MT exam', '1', '15', 0),
(199, 'IBPS Specialist Officer exam', '1', '15', 0),
(200, 'NABARD AM exam', '1', '15', 0),
(201, 'L.I.C Development Officer (D.O) exam', '1', '15', 0),
(202, 'L.I.C AAO exam', '1', '15', 0),
(203, 'IRDA exam', '1', '15', 0),
(204, 'NIACL Assistant exam', '1', '15', 0),
(259, 'Civil Services Examination (CSE)', '1', '21', 0),
(208, 'NDA exam', '1', '21', 0),
(209, 'CMS(Combined Medical Services) exam', '1', '21', 0),
(210, 'CDS(Combined Defence Services) exam', '1', '21', 0),
(211, 'CAPF(Combined Armed Police Forces) exam', '1', '21', 0),
(212, 'CESE(Combined Engineering Services) exam', '0', '1', 0),
(213, 'SCRA(Special Class Railway Apprentice) exam', '1', '21', 0),
(215, 'BBA Tuition', '1', '24', 0),
(216, 'B.Com. Hons. Tuition', '1', '24', 0),
(217, 'B.A. Hons. (Economics)', '1', '24', 0),
(218, 'Computer Science  Engineering', '1', '23', 0),
(219, 'Civil Engineering ', '1', '23', 0),
(220, 'Mechanical Engineering', '1', '23', 0),
(221, 'Electrical & Electronics Engineering', '1', '23', 0),
(222, 'Information Technology Engineering', '1', '23', 0),
(223, 'Electronics & Communication Engineering', '1', '23', 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_courses`
--

CREATE TABLE `app_courses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL COMMENT '1:academic,2:nonacademic',
  `status` varchar(10) NOT NULL COMMENT '0:deactive,1:active',
  `order` int(11) NOT NULL,
  `is_trend` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_courses`
--

INSERT INTO `app_courses` (`id`, `name`, `image`, `type`, `status`, `order`, `is_trend`) VALUES
(1, 'Primary Classes', 'courses_1515670814Primary_Classes.png', '1', '1', 1, '1'),
(2, 'Engineering Entrance exams Tuition', 'courses_1515670916Engineering_Entrance_exams_Tutions.png', '1', '1', 4, '0'),
(3, 'Medical Entrance Exams Tuition', 'courses_1515670988Medical_Entrance_Exams_Tutions.png', '1', '1', 5, '0'),
(4, 'Skill Development Tuition', 'courses_1515672006Skill_Development_Tutions.png', '1', '0', 21, '0'),
(5, 'IT Courses', 'courses_1515672018IT_courses_Tutions.png', '1', '1', 9, '0'),
(6, 'Corporate Training', 'courses_1515672036Corporate_Training.png', '1', '1', 8, '0'),
(7, 'College Tuition', 'courses_1515671061College_Tuition.png', '1', '0', 17, '0'),
(8, 'NON-Academic', 'courses_1515671085NON-Academic.png', '2', '0', 3, '0'),
(9, 'CO-CURRICULAR ACTIVITIES', 'courses_1515671002CO-CURRICULAR_ACTIVITIES.png', '2', '0', 18, '0'),
(10, 'Software Engineering', 'courses_1515670971Software_Engineering.png', '1', '0', 25, '0'),
(11, 'Overseas/Foreign education exams Tuition', 'courses_1515671051Overseas_Foreign_education_exams_Tutions.png', '1', '0', 19, '0'),
(12, 'B.sc Tuition', 'courses_1515670840B_sc_Tutions.png', '1', '1', 15, '1'),
(13, 'Gate Tuition', 'courses_1515671015Gate_Tutions.png', '1', '1', 14, '0'),
(14, 'SSC and Railways exams Tuition', 'courses_1515670929SSC_and_Railways_exams_Tutions.png', '1', '1', 9, '0'),
(15, 'Banking and Insurance exams Tuition', 'courses_1515671035Banking_and_Insurance_exams_Tutions.png', '1', '1', 11, '0'),
(16, 'Law Entrance Exams ', 'courses_1515671992Law_Entrance_exams_Tutions.png', '1', '1', 6, '0'),
(17, 'Marine,Navy & Defence Entrance Exams', 'courses_1515670938Marine_Navy_Defence_exams_Tutions.png', '1', '1', 8, '0'),
(18, 'Management Entrance Exams ', 'courses_1515672050Management_Entrance_exams_Tutions.png', '1', '1', 7, '0'),
(19, 'Fashion and Design Entrance exams Tuition', 'courses_1515672029Fashion_and_Design_Entrance_exams_Tutions.png', '1', '1', 8, '0'),
(20, 'Hotel Management Entrance exams ', 'courses_1515670909H_M.png', '1', '1', 8, '0'),
(21, 'UPSC exams Tuition', 'courses_1515670862UPSC_exams_Tuition.png', '1', '1', 10, '0'),
(22, 'Other Important exams Tuitions', 'courses_1515671980Other_Important_exams_Tuitions.png', '1', '1', 26, '0'),
(23, 'B.Tech. &  B.E. Tuition', 'courses_1515670961B_Tech__B_E__Tuition.png', '1', '1', 13, '0'),
(24, 'College Tuition', 'courses_1515671072College_Tuition.png', '1', '1', 12, '0'),
(36, 'Secondary Classes Tuition', 'courses_1515670829Secondary_Classes_Tuition.png', '1', '1', 2, '0'),
(40, 'Language Courses', 'courses_1518584801aec-1782427_960_720.jpg', '1', '1', 3, '1');

-- --------------------------------------------------------

--
-- Table structure for table `app_subjects`
--

CREATE TABLE `app_subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `non_acc_img` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL COMMENT '0:inactive,1:active',
  `trending` int(10) NOT NULL DEFAULT '0' COMMENT '0: no, 1: yes'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_subjects`
--

INSERT INTO `app_subjects` (`id`, `name`, `class_id`, `non_acc_img`, `status`, `trending`) VALUES
(319, 'MATHEMATICS', '5', '', '1', 0),
(43, 'ACCOUNTANCY', '11', '', '1', 0),
(318, 'ENGLISH', '5', '', '1', 0),
(317, 'E.V.S', '5', '', '1', 0),
(11, 'PARALLEL COMPUTING', '13', '', '1', 0),
(12, 'CORE JAVA', '13', '', '1', 0),
(13, 'C++', '13', '', '1', 0),
(14, 'ASP.NET', '13', '', '1', 0),
(15, 'COMPUTER BASICS', '14', '', '1', 0),
(16, 'LINUX', '14', '', '1', 0),
(1880, 'CLASS V', '1', '', '1', 0),
(1879, 'CLASS IV', '1', '', '1', 0),
(20, 'GENERAL KNOWLEDGE', '2', '', '1', 0),
(21, ' HINDI ', '2', '', '1', 0),
(22, 'ENGLISH ', '3', '', '1', 0),
(23, 'HINDI', '3', '', '1', 0),
(24, 'MATHEMATICS', '3', '', '1', 0),
(312, 'GENERAL KNOWLEDGE', '3', '', '1', 0),
(26, 'ENGLISH', '4', '', '1', 0),
(27, 'HINDI', '4', '', '1', 0),
(322, 'MATHEMATICS', '6', '', '1', 0),
(323, 'ENGLISH', '6', '', '1', 0),
(31, 'MATHEMATICS', '7', '', '1', 0),
(350, 'SCIENCE', '8', '', '1', 0),
(349, 'ENGLISH', '8', '', '1', 0),
(348, 'MATHEMATICS', '8', '', '1', 0),
(361, 'MATHEMATICS', '9', '', '1', 0),
(374, 'MATHEMATICS', '10', '', '1', 0),
(375, 'ENGLISH', '10', '', '1', 0),
(44, 'BUSINESS STUDIES', '11', '', '1', 0),
(46, 'ENGLISH', '11', '', '1', 0),
(47, 'MATHEMATICS', '11', '', '1', 0),
(48, 'PHYSICAL EDUCATION', '11', '', '1', 0),
(49, 'INFORMATION TECHNOLOGY', '11', '', '1', 0),
(50, 'PHYSICS', '17', '', '1', 0),
(51, 'CHEMISTRY', '17', '', '1', 0),
(52, 'MATHEMATICS', '17', '', '1', 0),
(53, 'ENGLISH', '17', '', '1', 0),
(54, 'COMPUTER SCIENCE', '17', '', '1', 0),
(55, 'BIOLOGY', '17', '', '1', 0),
(56, 'PHYSICAL EDUCATION', '17', '', '1', 0),
(57, 'PHYSICS', '18', '', '1', 0),
(58, 'CHEMISTRY', '18', '', '1', 0),
(59, 'BIOLOGY', '18', '', '1', 0),
(60, 'ENGLISH', '18', '', '1', 0),
(61, 'GEOGRAPHY', '19', '', '1', 0),
(62, 'HISTORY', '19', '', '1', 0),
(63, 'HISTORY', '7', '', '1', 0),
(64, 'ACCOUNTANCY', '12', '', '1', 1),
(65, 'BUSINESS STUDIES', '12', '', '1', 0),
(1873, 'ECONOMICS', '12', '', '1', 1),
(67, 'PHYSICS', '20', '', '1', 0),
(68, 'CHEMISTRY', '20', '', '1', 0),
(69, 'COMPUTER SCIENCE', '20', '', '1', 0),
(70, 'COMPUTER GRAPHICS', '15', '', '1', 0),
(71, 'ANIMATION', '15', '', '1', 0),
(72, 'GRAPHIC DESIGINING', '15', '', '1', 0),
(73, 'MULTIMEDIA & WEB TECHNOLOGY', '15', '', '1', 0),
(74, 'WEB DESIGINING', '15', '', '1', 0),
(75, 'LOOKBOOK', '15', '', '1', 0),
(76, 'COMPUTER APPLICATION DIPLOMA', '16', '', '1', 0),
(77, 'DIPLOMA IN HARDWARE AND NETWORKING', '16', '', '1', 0),
(78, 'PHYSICS', '21', '', '1', 0),
(79, 'BIOLOGY', '21', '', '1', 0),
(80, 'PHYSICAL EDUCATION', '21', '', '1', 0),
(412, 'ENGLISH', '22', '', '1', 0),
(413, 'HINDI', '22', '', '1', 0),
(414, 'HISTORY', '22', '', '1', 0),
(415, 'GEOGRAPHY', '22', '', '1', 0),
(416, 'POLITICAL SCIENCE', '22', '', '1', 0),
(505, 'NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-PHYSICS', '37', '', '1', 0),
(508, 'AIIMS-CHEMISTRY', '37', '', '1', 0),
(511, 'CMC(LUDHIANA)-PHYSICS', '37', '', '1', 0),
(514, 'CMC(VELLORE)-PHYSICS', '37', '', '1', 0),
(95, 'INDIAN MARITIME UNIVERSITY COMMON ENTRANCE TEST', '38', '', '1', 0),
(98, 'NATIONAL DEFENCE ACADEMY & NAVAL ACADEMY EXAMINATION', '38', '', '1', 0),
(97, 'INDIAN ARMY TECHNICAL ENTRY SCHEME', '38', '', '1', 0),
(99, 'NATIONAL INSTITUTE OF FASHION TECHNOLOGY(NIFT)', '39', '', '1', 0),
(100, 'NATIONAL INSTITUTE OF DESIGN ADMISSIONS', '39', '', '1', 0),
(101, 'ALL INDIA ENTRANCE EXAMINATION FOR DESIGN(AIEED)', '39', '', '1', 0),
(102, 'BANARAS HINDU UNIVERSITY(B.H.U)', '40', '', '1', 0),
(103, 'IIT MADRAS HUMANITIES & SOCIAL SCIENCE ENTRANCE EXAMINATION(HSEE)', '40', '', '1', 0),
(104, 'TISS BACHELORS ADMISSION TEST(TISS-BAT)', '40', '', '1', 0),
(105, 'INDIAN COUNCIL OF AGRICULTURE RESEARCH(ICAR)', '41', '', '1', 0),
(106, 'AIEEA-UG-PG', '41', '', '1', 0),
(107, 'ALL INDIA HOTEL MANAGEMENT ENTRANCE EXAM ', '41', '', '1', 0),
(1098, 'Plant Biotechnology', '99', '', '1', 0),
(1097, 'Industrial Biotechnology\'', '99', '', '1', 0),
(1096, 'Molecular Biology', '99', '', '1', 0),
(1095, 'Bioenergetics And Biomembranes', '99', '', '1', 0),
(1094, 'Genetics', '99', '', '1', 0),
(1093, 'Cell Biology', '99', '', '1', 0),
(1092, 'Vector Analysis', '93', '', '1', 0),
(1091, 'Differntial Geometry', '93', '', '1', 0),
(1090, 'Numerical Analysis ', '93', '', '1', 0),
(1089, 'Abstract Algebra', '93', '', '1', 0),
(1088, 'Differntial Equations ', '93', '', '1', 0),
(1087, 'Integral Calculus And Trigonometry', '93', '', '1', 0),
(121, 'Cricket', '47', '', '1', 0),
(122, 'FOOTBALL', '47', '', '1', 0),
(123, 'HOCKEY', '47', '', '1', 0),
(124, 'CHESS', '46', '', '1', 0),
(125, 'TABLE TENNIS', '46', '', '1', 0),
(129, 'FOOD PRESERVATION', '66', '', '1', 0),
(128, 'INTRODUCTION TO HUMAN DEVELOPEMNT', '66', '', '1', 0),
(130, 'FUNDAMENTAL OF NUTRITION', '66', '', '1', 0),
(131, 'INSTITUTE OF MANAGEMENT', '66', '', '1', 0),
(132, 'TEXTILES', '66', '', '1', 0),
(135, 'HOLISTIC EDUCATION', '68', '', '1', 0),
(136, 'PHILOSOPHY  OF EDUCATION', '68', '', '1', 0),
(137, 'GUIDANCE & COUNSELLING', '68', '', '1', 0),
(192, 'Dandiya (Folk dance of Gujarat, West India)', '69', '', '1', 0),
(191, 'Chholiya ', '69', '', '1', 0),
(188, 'Bihu dance ', '69', '', '1', 0),
(189, 'Bathukamma ', '69', '', '1', 0),
(190, 'Vaikuntha ', '69', '', '1', 0),
(1347, 'Digital Logic', '169', '', '1', 0),
(1346, 'Engineering Mathematics', '169', '', '1', 0),
(150, 'BASKETBALL', '46', '', '1', 0),
(151, 'SWIMMING', '46', '', '1', 0),
(152, 'BOXING', '46', '', '1', 0),
(153, 'VOLLEYBALL', '46', '', '1', 0),
(154, 'BADMINTON', '46', '', '1', 0),
(155, 'BOWLING', '46', '', '1', 0),
(156, 'SQUASH', '46', '', '1', 0),
(157, 'SNOOKER', '46', '', '1', 0),
(158, 'CARD GAMES', '46', '', '1', 0),
(159, 'BOARD GAMES', '46', '', '1', 0),
(160, 'GYMNASTIC', '46', '', '1', 0),
(161, 'WRESTLING', '46', '', '1', 0),
(162, 'KARATE', '46', '', '1', 0),
(163, 'TAEKWANDO', '46', '', '1', 0),
(164, 'DANCING', '46', '', '1', 0),
(165, 'KABADDI', '47', '', '1', 0),
(166, 'CLIMBING', '47', '', '1', 0),
(167, 'RAFTING', '47', '', '1', 0),
(168, 'SKATING', '47', '', '1', 0),
(169, 'ROCK CLIMBING', '47', '', '1', 0),
(170, 'ARCHERY', '47', '', '1', 0),
(171, 'TENNIS', '47', '', '1', 0),
(172, 'CYCLING', '47', '', '1', 0),
(173, 'RUGBY', '47', '', '1', 0),
(174, 'POLO', '47', '', '1', 0),
(186, 'Bhangra ', '69', '', '1', 0),
(187, 'Bharatnatyam ', '69', '', '1', 0),
(183, 'Andhra natyam ', '69', '', '1', 0),
(184, 'Bagurumba ', '69', '', '1', 0),
(185, 'Bhortal ', '69', '', '1', 0),
(193, 'Deodhani (Folk dance of Assam, North East India)', '69', '', '1', 0),
(194, 'Dhangari (Folk dance of Maharastra, West India)', '69', '', '1', 0),
(195, 'Dhemsa (Tribal dance of Koraput, Odisha)', '69', '', '1', 0),
(196, 'Dollu Kunitha (Folk Dance Of Karnataka, India)', '69', '', '1', 0),
(197, 'Garba (Folk dance of Gujarat, West India)', '69', '', '1', 0),
(198, 'Giddha (folk dance of Punjab North India)', '69', '', '1', 0),
(199, 'Ghoomar (folk dance of Rajasthan West India)', '69', '', '1', 0),
(200, 'Gaudiya Nritya (Classical dance of West Bengal)', '69', '', '1', 0),
(201, 'Geraiya Nritya (folk tribe dance of Gujarat, India', '69', '', '1', 0),
(202, 'Ghumura (Odisha)', '69', '', '1', 0),
(203, 'Jhumur ', '69', '', '1', 0),
(204, 'Kathak (Uttar Pradesh, Classical Indian Dance)', '69', '', '1', 0),
(205, 'Kathakali (Kerala, India, Incorporates dance)', '69', '', '1', 0),
(206, 'Krishnanattam', '69', '', '1', 0),
(207, 'Kuchipudi (Classical Indian Dance, Andhra Pradesh)', '69', '', '1', 0),
(208, 'Kolattam (folk Tamil Nadu)', '69', '', '1', 0),
(209, 'Koli Dance (Folk Maharastra)', '69', '', '1', 0),
(210, 'Karakattam (folk Tamil Nadu)', '69', '', '1', 0),
(211, 'Kanyarkali (Folk Dance, Kerala', '69', '', '1', 0),
(212, 'Lavani (Folk Dance, Maharastra)', '69', '', '1', 0),
(213, 'Mohiniattam (classical dance from Kerala)', '69', '', '1', 0),
(214, 'Manipuri (Indian classical dance from Manipur)', '69', '', '1', 0),
(215, 'Nati dance (Folk dance of Himachal Pradesh, India.', '69', '', '1', 0),
(216, 'Panthi (folk dance of Chhattisgarh, India)', '69', '', '1', 0),
(217, 'Parai Attam', '69', '', '1', 0),
(218, 'Puliyattam (folk dance of Tamil Nadu, India)', '69', '', '1', 0),
(219, 'Pulikali – Kerala', '69', '', '1', 0),
(220, 'Raas Leela (Indian Classical Dance)', '69', '', '1', 0),
(221, 'HIP-HOP', '69', '', '1', 0),
(222, 'JAZZ', '69', '', '1', 0),
(223, 'BALLET ', '69', '', '1', 0),
(224, 'BALLROOM DANCE', '69', '', '1', 0),
(225, 'BBOYING', '69', '', '1', 0),
(226, 'BOLLYWOOD', '69', '', '1', 0),
(227, 'CONTEMPORARY', '69', '', '1', 0),
(228, 'FREESTYLE', '69', '', '1', 0),
(229, 'SALSA', '69', '', '1', 0),
(230, 'Rock music', '70', '', '1', 0),
(231, 'JAZZ', '70', '', '1', 0),
(232, 'Blues', '70', '', '1', 0),
(233, 'Folk music', '70', '', '1', 0),
(234, 'Pop music', '70', '', '1', 0),
(235, 'Hip hop music', '70', '', '1', 0),
(236, 'Country music', '70', '', '1', 0),
(237, 'Punk rock', '70', '', '1', 0),
(238, 'Rhythm and blues', '70', '', '1', 0),
(239, 'Reggae', '70', '', '1', 0),
(240, 'Rapping', '70', '', '1', 0),
(241, 'Classical period', '70', '', '1', 0),
(242, 'Techno', '70', '', '1', 0),
(243, 'Classical music', '70', '', '1', 0),
(244, 'Opera', '70', '', '1', 0),
(245, 'Alternative rock', '70', '', '1', 0),
(246, 'Singing', '70', '', '1', 0),
(247, 'Soul music', '70', '', '1', 0),
(248, 'Electronic dance music', '70', '', '1', 0),
(249, 'Popular music', '70', '', '1', 0),
(250, 'Funk', '70', '', '1', 0),
(251, 'House music', '70', '', '1', 0),
(252, 'Electro', '70', '', '1', 0),
(253, 'Trance music', '70', '', '1', 0),
(254, 'Heavy metal', '70', '', '1', 0),
(255, 'Orchestra', '70', '', '1', 0),
(256, 'Breakbeat', '70', '', '1', 0),
(257, 'Dubstep', '70', '', '1', 0),
(258, 'Disco', '70', '', '1', 0),
(259, 'Electronics in rock music', '70', '', '1', 0),
(260, 'Ska', '70', '', '1', 0),
(261, 'Gospel music', '70', '', '1', 0),
(262, 'Dub', '70', '', '1', 0),
(263, 'Hardstyle', '70', '', '1', 0),
(264, 'Ambient music', '70', '', '1', 0),
(265, 'Jazz fusion', '70', '', '1', 0),
(266, 'Grunge', '70', '', '1', 0),
(267, 'Bluegrass', '70', '', '1', 0),
(268, 'Drum and bass', '70', '', '1', 0),
(269, 'Hardcore punk', '70', '', '1', 0),
(270, 'GUITAR', '73', '', '1', 0),
(271, 'SAXOPHONE', '73', '', '1', 0),
(272, 'VIOLIN', '73', '', '1', 0),
(273, 'DRUMSET', '73', '', '1', 0),
(274, 'PIANO', '73', '', '1', 0),
(275, 'CASIO', '73', '', '1', 0),
(276, 'TABLA', '73', '', '1', 0),
(277, 'HARMONIUM', '73', '', '1', 0),
(278, 'KEYBOARD', '73', '', '1', 0),
(279, 'ORCHESTRA', '73', '', '1', 0),
(280, 'FLUTE', '73', '', '1', 0),
(281, 'TRUMPET', '73', '', '1', 0),
(282, 'CONGA', '73', '', '1', 0),
(283, 'XYLOPHONE', '73', '', '1', 0),
(284, 'ARCHITECTURAL ', '74', '', '1', 0),
(285, 'CANDID', '74', '', '1', 0),
(286, 'LANDSCAPE', '74', '', '1', 0),
(287, 'DOCUMENTARY', '74', '', '1', 0),
(288, 'FASHION', '74', '', '1', 0),
(289, 'CONCEPTUAL', '74', '', '1', 0),
(290, 'NIGHT LONG EXPOSURE PHOTOGRAPHY', '74', '', '1', 0),
(291, 'PORTRAITURE', '74', '', '1', 0),
(292, 'SPORT', '74', '', '1', 0),
(293, 'STREET', '74', '', '1', 0),
(294, 'WILDLIFE PHOTOGRAPHY', '74', '', '1', 0),
(295, 'FINE ART', '78', '', '1', 0),
(296, 'VISUAL ART', '78', '', '1', 0),
(297, 'APPLIED ART', '78', '', '1', 0),
(298, 'DECORATIVE ART', '78', '', '1', 0),
(299, 'CRAFTS', '78', '', '1', 0),
(300, 'Stock preparation', '75', '', '1', 0),
(301, 'Knife skills', '75', '', '1', 0),
(302, 'Protein fabrication', '75', '', '1', 0),
(303, 'Consommé', '75', '', '1', 0),
(304, 'Sautéing', '75', '', '1', 0),
(305, 'Braising', '75', '', '1', 0),
(306, 'Confections', '75', '', '1', 0),
(1878, 'CLASS III', '1', '', '1', 0),
(1877, 'CLASS II', '1', '', '1', 0),
(309, 'E.V.S', '2', '', '1', 0),
(310, 'ENGLISH', '2', '', '1', 0),
(311, 'MATHEMATICS', '2', '', '1', 0),
(313, 'E.V.S', '3', '', '1', 0),
(314, 'E.V.S', '4', '', '1', 0),
(315, 'MATHEMATICS', '4', '', '1', 0),
(316, 'GENERAL KNOWLEDGE', '4', '', '1', 0),
(320, 'HINDI', '5', '', '1', 0),
(321, 'GENERAL KNOWLEDGE', '5', '', '1', 0),
(324, 'SCIENCE', '6', '', '1', 0),
(325, 'HISTORY', '6', '', '1', 0),
(326, 'GEOGRAPHY', '6', '', '1', 0),
(327, 'CIVICS', '6', '', '1', 0),
(328, 'FRENCH', '6', '', '1', 0),
(329, 'GERMAN', '6', '', '1', 0),
(330, 'SANSKRIT', '6', '', '1', 0),
(331, 'JAPANESE', '6', '', '1', 0),
(332, 'HINDI', '6', '', '1', 0),
(333, 'COMPUTER', '6', '', '1', 0),
(334, 'GENERAL KNOWLEDGE', '6', '', '1', 0),
(336, 'ENGLISH', '7', '', '1', 0),
(337, 'SCIENCE', '7', '', '1', 0),
(1875, 'INFORMATION TECHNOLOGY', '12', '', '1', 0),
(339, 'GEOGRAPHY', '7', '', '1', 0),
(340, 'CIVICS', '7', '', '1', 0),
(341, 'FRENCH', '7', '', '1', 0),
(342, 'GERMAN', '7', '', '1', 0),
(343, 'SANSKRIT', '7', '', '1', 0),
(344, 'JAPANESE', '7', '', '1', 0),
(345, 'HINDI', '7', '', '1', 0),
(346, 'COMPUTER', '7', '', '1', 0),
(347, 'GENERAL KNOWLEDGE', '7', '', '1', 0),
(351, 'HISTORY', '8', '', '1', 0),
(352, 'GEOGRAPHY', '8', '', '1', 0),
(353, 'CIVICS', '8', '', '1', 0),
(354, 'FRENCH', '8', '', '1', 0),
(355, 'GERMAN', '8', '', '1', 0),
(356, 'SANSKRIT', '8', '', '1', 0),
(357, 'JAPANESE', '8', '', '1', 0),
(358, 'HINDI', '8', '', '1', 0),
(359, 'COMPUTER', '8', '', '1', 0),
(360, 'GENERAL KNOWLEDGE', '8', '', '1', 0),
(362, 'ENGLISH', '9', '', '1', 0),
(363, 'SCIENCE', '9', '', '1', 0),
(364, 'HISTORY', '9', '', '1', 0),
(365, 'GEOGRAPHY', '9', '', '1', 0),
(366, 'CIVICS', '9', '', '1', 0),
(367, 'FRENCH', '9', '', '1', 0),
(368, 'GERMAN', '9', '', '1', 0),
(369, 'SANSKRIT', '9', '', '1', 0),
(370, 'JAPANESE', '9', '', '1', 0),
(371, 'HINDI', '9', '', '1', 0),
(372, 'COMPUTER', '9', '', '1', 0),
(373, 'GENERAL KNOWLEDGE', '9', '', '1', 0),
(376, 'SCIENCE', '10', '', '1', 0),
(377, 'HISTORY', '10', '', '1', 0),
(378, 'GEOGRAPHY', '10', '', '1', 0),
(379, 'CIVICS', '10', '', '1', 0),
(380, 'FRENCH', '10', '', '1', 0),
(381, 'GERMAN', '10', '', '1', 0),
(382, 'SANSKRIT', '10', '', '1', 0),
(383, 'JAPANESE', '10', '', '1', 0),
(384, 'HINDI', '10', '', '1', 0),
(385, 'COMPUTER', '10', '', '1', 0),
(386, 'GENERAL KNOWLEDGE', '10', '', '1', 0),
(387, 'MATHEMATICS', '20', '', '1', 0),
(388, 'ENGLISH', '20', '', '1', 0),
(389, 'BIOLOGY', '20', '', '1', 0),
(390, 'PHYSICAL EDUCATION', '20', '', '1', 0),
(391, 'MATHEMATICS', '18', '', '1', 0),
(392, 'PHYSICAL EDUCATION', '18', '', '1', 0),
(393, 'ENGLISH', '19', '', '1', 0),
(394, 'HINDI', '19', '', '1', 0),
(395, 'POLITICAL SCIENCE', '19', '', '1', 0),
(396, 'MATHEMATICS', '19', '', '1', 0),
(397, 'PHYSICAL EDUCATION', '19', '', '1', 0),
(398, 'MUSIC', '19', '', '1', 0),
(399, 'PSYCHOLOGY', '19', '', '1', 0),
(400, 'SOCIOLOGY', '19', '', '1', 0),
(401, 'FASHION STUDIES', '19', '', '1', 0),
(402, 'SANSKRIT', '19', '', '1', 0),
(403, 'ANTHROPOLOGY', '19', '', '1', 0),
(404, 'ARCHAEOLOGY', '19', '', '1', 0),
(405, 'ECONOMICS', '19', '', '1', 0),
(406, 'ENGLISH', '12', '', '1', 0),
(407, 'MATHEMATICS', '12', '', '1', 1),
(408, 'PHYSICAL EDUCATION', '12', '', '1', 0),
(409, 'CHEMISTRY', '21', '', '1', 0),
(410, 'ENGLISH', '21', '', '1', 0),
(411, 'MATHEMATICS', '21', '', '1', 0),
(417, 'MATHEMATICS', '22', '', '1', 0),
(418, 'PHYSICAL EDUCATION', '22', '', '1', 0),
(419, 'MUSIC', '22', '', '1', 0),
(420, 'PSYCHOLOGY', '22', '', '1', 0),
(421, 'SOCIOLOGY', '22', '', '1', 0),
(422, 'FASHION STUDIES', '22', '', '1', 0),
(423, 'SANSKRIT', '22', '', '1', 0),
(424, 'ANTHROPOLOGY', '22', '', '1', 0),
(425, 'ARCHAEOLOGY', '22', '', '1', 0),
(426, 'ECONOMICS', '22', '', '1', 0),
(427, 'CSS', '13', '', '1', 0),
(428, 'FORTAN', '13', '', '1', 0),
(429, 'HTML', '13', '', '1', 0),
(430, 'DBMS', '13', '', '1', 0),
(431, 'IOS APP DEVELOPMENT', '13', '', '1', 0),
(432, 'INFORMATION TECHNOLOGY', '13', '', '1', 0),
(1072, 'E -COMMERCE', '13', '', '1', 0),
(434, 'RUBY ON RAIL', '13', '', '1', 0),
(435, 'JAVA SCRIPT', '13', '', '1', 0),
(436, 'SWIFT PROGRAMMING LANGUAGE', '13', '', '1', 0),
(437, 'DESIGN TECHNOLOGY IB', '13', '', '1', 0),
(438, 'MICROPROCESSOR', '13', '', '1', 0),
(439, 'ADVANCE JAVA', '13', '', '1', 0),
(440, 'MY SQL', '13', '', '1', 0),
(441, 'ADO.NET', '13', '', '1', 0),
(442, 'ORACLE', '13', '', '1', 0),
(443, 'PL SQL', '13', '', '1', 0),
(444, 'PHP', '13', '', '1', 0),
(445, 'DATA STRUCTURE', '13', '', '1', 0),
(446, 'ANDROID DEVELOPMENT', '13', '', '1', 0),
(447, 'DATA MINING', '13', '', '1', 0),
(448, 'OOPS', '13', '', '1', 0),
(449, 'AUTOMATA THEORY', '13', '', '1', 0),
(450, 'COMPILER DESIGN', '13', '', '1', 0),
(451, 'THEORY OF COMPUTATION', '13', '', '1', 0),
(452, 'C PROGRAMMING LANGUAGE', '13', '', '1', 0),
(453, 'PYTHON', '13', '', '1', 0),
(455, 'INFORMATION PRACTICE FOR CLASS XII', '14', '', '1', 0),
(456, 'CORAL DRAW', '14', '', '1', 0),
(457, 'PHOTOSHOP', '14', '', '1', 0),
(458, 'OPERATING SYSTEM', '14', '', '1', 0),
(459, 'DATA ENTRY', '14', '', '1', 0),
(460, 'VISUAL BASIC', '14', '', '1', 0),
(461, 'UNIX', '14', '', '1', 0),
(462, 'FLASH', '14', '', '1', 0),
(463, 'PAGE MAKER', '14', '', '1', 0),
(464, 'GRAPHICAL USER INTERFACE', '14', '', '1', 0),
(465, 'WORD ART', '14', '', '1', 0),
(466, '2D MAX', '14', '', '1', 0),
(467, 'AUTODESK 3DS MAX', '14', '', '1', 0),
(468, 'Q BASICS', '14', '', '1', 0),
(469, 'DIGITAL MARKETING', '14', '', '1', 0),
(470, 'CYBER SECURITY', '14', '', '1', 0),
(472, 'INFORMATION PRACTICE FOR CLASS XI', '14', '', '1', 0),
(473, 'INTERNET', '14', '', '1', 0),
(474, 'UBUNTU', '14', '', '1', 0),
(475, 'TALLY', '14', '', '1', 0),
(476, 'MS OFFICE', '14', '', '1', 0),
(477, 'MS EXCEL', '14', '', '1', 0),
(478, 'MAC', '14', '', '1', 0),
(479, 'GOOGLE SKETCHUP', '14', '', '1', 0),
(480, 'ALGORITHM DESIGN', '14', '', '1', 0),
(481, 'SYSTEM MANAGEMENT', '14', '', '1', 0),
(482, 'NEURAL NETWORK', '14', '', '1', 0),
(483, 'SCRATCH', '14', '', '1', 0),
(484, 'TUX PAINT', '14', '', '1', 0),
(485, 'WORDPRESS', '14', '', '1', 0),
(486, 'MS POWER POINT', '14', '', '1', 0),
(487, 'CCNA CCNP CCIE MICROSOFT MCSE COURSES', '16', '', '1', 0),
(488, 'CCNA CISCO CERTIFIED NETWORK ASSOCIATE ROUTING & SWITCHING', '16', '', '1', 0),
(489, 'COMPUTER HARDWARE NETWORKING COURSES', '16', '', '1', 0),
(490, 'CCNA IT COURSES-NIELIT O-LEVEL COURSES', '16', '', '1', 0),
(491, 'INTERNET COURSES-CCNA CISCO CERTIFIED NETWORK ASSOCIATE', '16', '', '1', 0),
(492, 'CCNA WIRELESS-IU WNE', '16', '', '1', 0),
(493, 'JEE(MAIN)-PHYSICS', '36', '', '1', 0),
(494, 'JEE(MAIN)-CHEMISTRY', '36', '', '1', 0),
(495, 'JEE(MAIN)-MATHEMATICS', '36', '', '1', 0),
(496, 'JEE(ADVANCED)-PHYSICS', '36', '', '1', 0),
(497, 'JEE(ADVANCED)-CHEMISTRY', '36', '', '1', 0),
(498, 'JEE(ADVANCED)-MATHEMATICS', '36', '', '1', 0),
(499, 'BITSAT-CHEMISTRY', '36', '', '1', 0),
(500, 'BITSAT-PHYSICS', '36', '', '1', 0),
(501, 'BITSAT-MATHEMATICS', '36', '', '1', 0),
(502, 'VIT-PHYSICS', '36', '', '1', 0),
(503, 'VIT-MATHEMATICS', '36', '', '1', 0),
(504, 'VIT-CHEMISTRY', '36', '', '1', 0),
(506, 'NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-BOILOGY', '37', '', '1', 0),
(507, 'NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-CHEMISTRY', '37', '', '1', 0),
(509, 'AIIMS-PHYSICS', '37', '', '1', 0),
(510, 'AIIMS-BIOLOGY', '37', '', '1', 0),
(512, 'CMC(LUDHIANA)-CHEMISTRY', '37', '', '1', 0),
(513, 'CMC(LUDHIANA)-BIOLOGY', '37', '', '1', 0),
(515, 'CMC(VELLORE)-BIOLOGY', '37', '', '1', 0),
(516, 'CMC(VELLORE)-CHEMISTRY', '37', '', '1', 0),
(863, 'AFCAT', '49', '', '1', 0),
(864, 'UPSC CDS', '49', '', '1', 0),
(1360, 'Signals And Systems', '170', '', '1', 0),
(1359, 'Electromagnetic Fields', '170', '', '1', 0),
(1358, 'Electric Circuits', '170', '', '1', 0),
(1357, 'Engineering Mathematics', '170', '', '1', 0),
(1356, 'Computer Networks', '169', '', '1', 0),
(1355, 'Databases', '169', '', '1', 0),
(1354, 'Operating System', '169', '', '1', 0),
(1353, 'Compiler Design', '169', '', '1', 0),
(1352, 'Theory of Computation', '169', '', '1', 0),
(1351, 'Algorithms', '169', '', '1', 0),
(1349, 'Computer Organisation and Architecture', '169', '', '1', 0),
(1430, 'Art', '186', 'http://taktii.com/etc/api/non-academy/art.jpg', '1', 0),
(1429, 'Fashion Designing', '186', 'http://taktii.com/etc/api/non-academy/Fashion-Designing.jpg', '1', 0),
(1427, 'Cooking', '186', 'http://taktii.com/etc/api/non-academy/Cooking.jpg', '1', 0),
(1428, 'Architecture/Interior Designing', '186', 'http://taktii.com/etc/api/non-academy/Interior-Designing.jpg', '1', 0),
(1426, 'Photography', '186', 'http://taktii.com/etc/api/non-academy/Photography.jpg', '1', 0),
(1425, 'Media', '186', 'http://taktii.com/etc/api/non-academy/Media.jpg', '1', 0),
(1424, 'Computer', '186', 'http://taktii.com/etc/api/non-academy/Computer.jpg', '1', 0),
(1423, 'Instruments', '186', 'http://taktii.com/etc/api/non-academy/Instruments.jpg', '1', 0),
(1422, 'Writing/ Reading', '186', 'http://taktii.com/etc/api/non-academy/Writing-Reading.jpg', '1', 0),
(1421, 'Singing', '186', 'http://taktii.com/etc/api/non-academy/Singing.jpg', '1', 0),
(1420, 'Dance', '186', 'http://taktii.com/etc/api/non-academy/Dance.jpg', '1', 0),
(1419, 'Table Tennis', '185', 'http://taktii.com/etc/api/non-academy/Table-Tennis.jpg', '1', 0),
(1418, 'Chess', '185', 'http://taktii.com/etc/api/non-academy/Chess.jpg', '1', 0),
(1417, 'Wrestling', '185', 'http://taktii.com/etc/api/non-academy/Wrestling.jpg', '1', 0),
(1416, 'Archery', '185', 'http://taktii.com/etc/api/non-academy/Archery.jpg', '1', 0),
(1415, 'Horse Riding', '185', 'http://taktii.com/etc/api/non-academy/Horse-Riding.jpg', '1', 0),
(1414, 'Golf', '185', 'http://taktii.com/etc/api/non-academy/Golf.jpg', '1', 0),
(1413, 'Martial Arts', '185', 'http://taktii.com/etc/api/non-academy/Martial-Arts.jpg', '1', 0),
(1412, 'Kickboxing', '185', 'http://taktii.com/etc/api/non-academy/Kickboxing.jpg', '1', 0),
(1411, 'Karrate', '185', 'http://taktii.com/etc/api/non-academy/Karrate.jpg', '1', 0),
(1410, 'Hockey', '185', 'http://taktii.com/etc/api/non-academy/Hockey.jpg', '1', 0),
(1409, 'Kabaddi', '185', 'http://taktii.com/etc/api/non-academy/Kabaddi.jpg', '1', 0),
(1408, 'Kho-kho', '185', 'http://taktii.com/etc/api/non-academy/Kho-kho.jpg', '1', 0),
(1407, 'Shooting', '185', 'http://taktii.com/etc/api/non-academy/Shooting.jpg', '1', 0),
(1406, 'Swimming', '185', 'http://taktii.com/etc/api/non-academy/Swimming.jpg', '1', 0),
(1405, 'Billiards', '185', 'http://taktii.com/etc/api/non-academy/Billiards.jpg', '1', 0),
(1404, 'Squash', '185', 'http://taktii.com/etc/api/non-academy/Squash.jpg', '1', 0),
(1403, 'Volleyball', '185', 'http://taktii.com/etc/api/non-academy/Volleyball.jpg', '1', 0),
(1402, 'Badminton', '185', 'http://taktii.com/etc/api/non-academy/Badminton.jpg', '1', 0),
(1401, 'Lawn Tennis', '185', 'http://taktii.com/etc/api/non-academy/Lawn-Tennis.jpg', '1', 0),
(1400, 'Boxing', '185', 'http://taktii.com/etc/api/non-academy/Boxing.jpg', '1', 0),
(1398, 'Judo', '185', 'http://taktii.com/etc/api/non-academy/Judo.jpg', '1', 0),
(1399, 'Cycling', '185', 'http://taktii.com/etc/api/non-academy/Cycling.jpg', '1', 0),
(1397, 'Weight Lifting', '185', 'http://taktii.com/etc/api/non-academy/Weight-Lifting.jpg', '1', 0),
(1396, 'Rugby', '185', 'http://taktii.com/etc/api/non-academy/Rugby.jpg', '1', 0),
(1395, 'Base Ball', '185', 'http://taktii.com/etc/api/non-academy/Base-Ball.jpg', '1', 0),
(1394, 'Gymnastics', '185', 'http://taktii.com/etc/api/non-academy/Gymnastics.jpg', '1', 0),
(1393, 'Basket ball', '185', 'http://taktii.com/etc/api/non-academy/Basket-ball.jpg', '1', 0),
(1391, 'Cricket', '185', 'http://taktii.com/etc/api/non-academy/Cricket.jpg', '1', 0),
(1392, 'Football', '185', 'http://taktii.com/etc/api/non-academy/Football.jpg', '1', 0),
(1388, 'Communication and Optical Instrumentation', '174', '', '1', 0),
(1387, 'Sensors and Industrial Instrumentation', '174', '', '1', 0),
(1386, ' Measurements', '174', '', '1', 0),
(1385, ' Digital Electronics', '174', '', '1', 0),
(1383, 'Control Systems', '174', '', '1', 0),
(1384, 'Analog Electronics', '174', '', '1', 0),
(1381, 'Signals and Systems', '174', '', '1', 0),
(1380, 'Electrical Circuits', '174', '', '1', 0),
(1379, ' Engineering Mathematics', '174', '', '1', 0),
(1378, 'Biotechnology Engineering', '184', '', '1', 0),
(1361, 'Electrical Machines', '170', '', '1', 0),
(1362, 'Power Systems', '170', '', '1', 0),
(1363, 'Control Systems', '170', '', '1', 0),
(1364, 'Electrical And Electronic Measurements', '170', '', '1', 0),
(1365, 'Analog And Digital Electronics', '170', '', '1', 0),
(1366, 'Power Electronics', '170', '', '1', 0),
(1367, 'Engineering Mathematics', '171', '', '1', 0),
(1368, 'General Aptitude', '171', '', '1', 0),
(1369, 'Electronics And Communication Engineering', '171', '', '1', 0),
(1370, 'Engineering Mathematics', '172', '', '1', 0),
(1371, 'General Aptitude', '172', '', '1', 0),
(1372, 'Applied Mechanics And Design', '172', '', '1', 0),
(1373, 'Engineering Mathematics', '173', '', '1', 0),
(1374, 'General Aptitude', '173', '', '1', 0),
(1375, 'Civil Engineering', '173', '', '1', 0),
(1376, 'Engineering Mathematics', '184', '', '1', 0),
(1377, 'General Aptitude', '184', '', '1', 0),
(865, 'I.A.F EXAM (TECH)', '49', '', '1', 0),
(866, 'I.A.F EXAM (NON TECH)', '49', '', '1', 0),
(867, 'INDIAN ARMY SOLDIERS M.E.R EXAM', '49', '', '1', 0),
(868, 'INDIAN ARMY SOLDIERS CLERK EXAM', '49', '', '1', 0),
(869, 'INDIAN ARMY SOLDIERS GENERAL DUTY EXAM', '49', '', '1', 0),
(870, 'I.N.A EXAM', '49', '', '1', 0),
(871, 'INDIAN NAVY DOCKYARD APPRENTICES EXAM', '49', '', '1', 0),
(1149, 'Water Management', '105', '', '1', 0),
(1165, 'English including Comprehension', '127', '', '1', 0),
(1164, 'Horticulture', '107', '', '1', 0),
(1163, 'Home Science', '107', '', '1', 0),
(1162, 'Fisheries', '107', '', '1', 0),
(1161, 'Veterinary Science', '107', '', '1', 0),
(1158, 'Agriculture (Agronomy)', '107', '', '1', 0),
(1150, 'Nutrition', '106', '', '1', 0),
(1154, 'Microbiology', '106', '', '1', 0),
(1153, 'Pharmacology', '106', '', '1', 0),
(1152, 'Biochemistry', '106', '', '1', 0),
(1151, 'Psychology', '106', '', '1', 0),
(1159, 'Forestry', '107', '', '1', 0),
(1157, 'Anatomy', '106', '', '1', 0),
(912, 'PAYROLL TRAINING', '23', '', '1', 0),
(913, 'PERSONALITY DEVELOPMENT', '24', '', '1', 0),
(914, 'COMPENSATION OF BENEFITS', '25', '', '1', 0),
(915, 'LINEAR PIRATE TRAINING', '26', '', '1', 0),
(916, 'SOFT SKILLS', '27', '', '1', 0),
(917, 'NEGOTIATION', '28', '', '1', 0),
(918, 'BODY LANGUAGE', '29', '', '1', 0),
(919, 'TEAM BUILDING', '30', '', '1', 0),
(920, 'LEADERSHIP', '31', '', '1', 0),
(921, 'PUBLIC SPEAKING', '32', '', '1', 0),
(922, 'WRITTEN COMMUNICATION', '33', '', '1', 0),
(923, 'INTERVIEW TECHNIQUE', '34', '', '1', 0),
(924, 'EMPLOYEE RELATION', '35', '', '1', 0),
(1185, 'Elementary Mathematics (Numerical Ability)', '131', '', '1', 0),
(1184, 'Logical Reasoning', '130', '', '1', 0),
(1183, 'Legal Aptitude', '130', '', '1', 0),
(1182, 'Elementary Mathematics (Numerical Ability)', '130', '', '1', 0),
(1181, 'General Knowledge and current Affairs', '130', '', '1', 0),
(1180, 'English including Comprehension', '130', '', '1', 0),
(1179, 'Logical Reasoning', '129', '', '1', 0),
(1178, 'Legal Aptitude', '129', '', '1', 0),
(1177, 'Elementary Mathematics (Numerical Ability)', '129', '', '1', 0),
(1176, 'General Knowledge and current Affairs', '129', '', '1', 0),
(1175, 'English including Comprehension', '129', '', '1', 0),
(1174, 'Logical Reasoning', '128', '', '1', 0),
(1173, 'Legal Aptitude', '128', '', '1', 0),
(1172, 'Elementary Mathematics (Numerical Ability)', '128', '', '1', 0),
(1171, 'General Knowledge and current Affairs', '128', '', '1', 0),
(1170, 'English including Comprehension', '128', '', '1', 0),
(1169, 'Logical Reasoning', '127', '', '1', 0),
(1168, 'Legal Aptitude', '127', '', '1', 0),
(1167, 'Elementary Mathematics (Numerical Ability)', '127', '', '1', 0),
(1166, 'General Knowledge and current Affairs', '127', '', '1', 0),
(1148, 'Renewable Energy', '105', '', '1', 0),
(1147, 'Farm Machinery', '105', '', '1', 0),
(1146, 'Agro Informatics', '105', '', '1', 0),
(1143, 'Crop Physiology', '105', '', '1', 0),
(1145, 'Pest Management', '105', '', '1', 0),
(1144, 'Soil Conservation', '105', '', '1', 0),
(1065, 'B.SC IN FASHION DESIGINING', '39', '', '1', 0),
(970, 'MASTER OF TECHNOLOGY [M.TECH] (CIVIL ENGINEERING)', '53', '', '1', 0),
(971, 'MASTER OF ENGINEERING [ME] (DIGITAL COMMUNICATION & NETWORKING)', '53', '', '1', 0),
(972, 'MASTER OF TECHNOLOGY [M.TECH] (AERONAUTICAL ENGINEERING)', '53', '', '1', 0),
(973, 'MASTER OF TECHNOLOGY [M.TECH] (AEROSPACE ENGINEERING)', '53', '', '1', 0),
(974, 'MASTER OF TECHNOLOGY [M.TECH] (AGRICULTURAL ENGINEERING)', '53', '', '1', 0),
(975, 'MASTER OF TECHNOLOGY [M.TECH] (AIR ARMAMENT)', '53', '', '1', 0),
(976, 'MASTER OF TECHNOLOGY [M.TECH] (ALLOY TECHNOLOGY)', '53', '', '1', 0),
(977, 'MASTER OF TECHNOLOGY [M.TECH] (APPLIED ELECTRONICS)', '53', '', '1', 0),
(978, 'MASTER OF TECHNOLOGY [M.TECH] (APPLIED GEOLOGY)', '53', '', '1', 0),
(979, 'MASTER OF TECHNOLOGY [M.TECH] (APPLIED GEOPHYSICS)', '53', '', '1', 0),
(980, 'MASTER OF TECHNOLOGY [M.TECH] (APPLIED MECHANICS)', '53', '', '1', 0),
(981, 'MASTER OF TECHNOLOGY [M.TECH] (AQUA CULTURAL ENGINEERING)', '53', '', '1', 0),
(982, 'MASTER OF TECHNOLOGY [M.TECH] (AUTOMOBILE ENGINEERING)', '53', '', '1', 0),
(983, 'MASTER OF TECHNOLOGY [M.TECH] (AUTOMOTIVE ENGINEERING)', '53', '', '1', 0),
(984, 'MASTER OF TECHNOLOGY [M.TECH] (BIO MINERAL PROCESSING)', '53', '', '1', 0),
(985, 'MASTER OF TECHNOLOGY [M.TECH] (BIO PHARMACEUTICAL TECHNOLOGY)', '53', '', '1', 0),
(986, 'MASTER OF TECHNOLOGY [M.TECH] (BIOCHEMICAL ENGINEERING)', '53', '', '1', 0),
(987, 'MASTER OF TECHNOLOGY [M.TECH] (BIOMEDICAL ENGINEERING)', '53', '', '1', 0),
(988, 'MASTER OF TECHNOLOGY [M.TECH] (BIOTECHNOLOGY)', '53', '', '1', 0),
(989, 'MASTER OF TECHNOLOGY [M.TECH] (CAD/ CAM)', '53', '', '1', 0),
(990, 'MASTER OF TECHNOLOGY [M.TECH] (CHEMICAL ENGINEERING)', '53', '', '1', 0),
(991, 'MASTER OF TECHNOLOGY [M.TECH] (COMMUNICATION SYSTEMS)', '53', '', '1', 0),
(992, 'MASTER OF TECHNOLOGY [M.TECH] (COMPUTER NETWORK ENGINEERING)', '53', '', '1', 0),
(993, 'MASTER OF TECHNOLOGY [M.TECH] (COMPUTER SCIENCE AND ENGINEERING)', '53', '', '1', 0),
(994, 'MASTER OF TECHNOLOGY [M.TECH] (CONSTRUCTION ENGINEERING AND MANAGEMENT)', '53', '', '1', 0),
(995, 'MASTER OF TECHNOLOGY [M.TECH] (CONSTRUCTION TECHNOLOGY)', '53', '', '1', 0),
(996, 'MASTER OF TECHNOLOGY [M.TECH] (CONTROL SYSTEMS)', '53', '', '1', 0),
(997, 'MASTER OF TECHNOLOGY [M.TECH] (DESIGN ENGINEERING)', '53', '', '1', 0),
(998, 'MASTER OF TECHNOLOGY [M.TECH] (DIGITAL COMMUNICATION)', '53', '', '1', 0),
(999, 'MASTER OF TECHNOLOGY [M.TECH] (DIGITAL ELECTRONICS & COMMUNICATION SYSTEMS)', '53', '', '1', 0),
(1000, 'MASTER OF TECHNOLOGY [M.TECH] (DIGITAL SYSTEM & SIGNAL PROCESSING)', '53', '', '1', 0),
(1001, 'MASTER OF TECHNOLOGY [M.TECH] (EARTHQUAKE ENGINEERING)', '53', '', '1', 0),
(1002, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRICAL AND ELECTRONICS ENGINEERING)', '53', '', '1', 0),
(1003, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRICAL ENGINEERING)', '53', '', '1', 0),
(1004, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRONICS & TELECOMMUNICATIONS ENGINEERING)', '53', '', '1', 0),
(1005, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRONICS AND COMMUNICATION ENGINEERING)', '53', '', '1', 0),
(1006, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRONICS AND INSTRUMENTATION ENGINEERING)', '53', '', '1', 0),
(1007, 'MASTER OF TECHNOLOGY [M.TECH] (ELECTRONICS ENGINEERING)', '53', '', '1', 0),
(1008, 'MASTER OF TECHNOLOGY [M.TECH] (ENERGY ENGINEERING)', '53', '', '1', 0),
(1009, 'MASTER OF TECHNOLOGY [M.TECH] (ENGINEERING DESIGN)', '53', '', '1', 0),
(1010, 'MASTER OF TECHNOLOGY [M.TECH] (ENGINEERING PHYSICS)', '53', '', '1', 0),
(1012, 'MASTER OF TECHNOLOGY [M.TECH] (ENVIRONMENTAL ENGINEERING)', '53', '', '1', 0),
(1013, 'MASTER OF TECHNOLOGY [M.TECH] (FLUIDS ENGINEERING)', '53', '', '1', 0),
(1014, 'ASTER OF TECHNOLOGY [M.TECH] (FOOD & NUTRITION)', '53', '', '1', 0),
(1015, 'STER OF TECHNOLOGY [M.TECH] (FOOD BIOTECHNOLOGY)', '53', '', '1', 0),
(1016, 'MASTER OF TECHNOLOGY [M.TECH] (FOOD CHAIN MANAGEMENT)', '53', '', '1', 0),
(1017, 'MASTER OF TECHNOLOGY [M.TECH] (FOOD PROCESS ENGINEERING)', '53', '', '1', 0),
(1018, 'MASTER OF TECHNOLOGY [M.TECH] (FOOD TECHNOLOGY)', '53', '', '1', 0),
(1019, 'MASTER OF TECHNOLOGY [M.TECH] (GEOTECHNICAL ENGINEERING)', '53', '', '1', 0),
(1020, 'MASTER OF TECHNOLOGY [M.TECH] (INDUSTRIAL ENGINEERING)', '53', '', '1', 0),
(1021, 'MASTER OF TECHNOLOGY [M.TECH] (INFORMATION TECHNOLOGY)', '53', '', '1', 0),
(1022, 'MASTER OF TECHNOLOGY [M.TECH] (INSTRUMENTATION AND CONTROL ENGINEERING)', '53', '', '1', 0),
(1023, 'MASTER OF TECHNOLOGY [M.TECH] (LEATHER TECHNOLOGY)', '53', '', '1', 0),
(1024, 'MASTER OF TECHNOLOGY [M.TECH] (MACHINE DESIGN)', '53', '', '1', 0),
(1025, 'MASTER OF TECHNOLOGY [M.TECH] (MANUFACTURING SCIENCE & ENGINEERING)', '53', '', '1', 0),
(1026, 'MASTER OF TECHNOLOGY [M.TECH] (MANUFACTURING TECHNOLOGY)', '53', '', '1', 0),
(1027, 'MASTER OF TECHNOLOGY [M.TECH] (MARINE ENGINEERING)', '53', '', '1', 0),
(1028, 'MASTER OF TECHNOLOGY [M.TECH] (MATERIAL SCIENCE AND ENGINEERING)', '53', '', '1', 0),
(1029, 'MASTER OF TECHNOLOGY [M.TECH] (MECHANICAL ENGINEERING)', '53', '', '1', 0),
(1030, 'MASTER OF TECHNOLOGY [M.TECH] (MINERAL ENGINEERING)', '53', '', '1', 0),
(1031, 'MASTER OF TECHNOLOGY [M.TECH] (MINING ENGINEERING)', '53', '', '1', 0),
(1032, 'MASTER OF TECHNOLOGY [M.TECH] (NANOTECHNOLOGY)', '53', '', '1', 0),
(1033, 'MASTER OF TECHNOLOGY [M.TECH] (NUCLEAR ENGINEERING)', '53', '', '1', 0),
(1034, 'MASTER OF TECHNOLOGY [M.TECH] (OPTICAL ENGINEERING)', '53', '', '1', 0),
(1035, 'MASTER OF TECHNOLOGY [M.TECH] (PETROLEUM ENGINEERING)', '53', '', '1', 0),
(1036, 'MASTER OF TECHNOLOGY [M.TECH] (PHARMACEUTICAL TECHNOLOGY)', '53', '', '1', 0),
(1037, 'MASTER OF TECHNOLOGY [M.TECH] (PIPELINE ENGINEERING)', '53', '', '1', 0),
(1038, 'MASTER OF TECHNOLOGY [M.TECH] (POLYMER TECHNOLOGY)', '53', '', '1', 0),
(1039, 'MASTER OF TECHNOLOGY [M.TECH] (POWER ELECTRONICS AND DRIVES)', '53', '', '1', 0),
(1040, 'MASTER OF TECHNOLOGY [M.TECH] (POWER ELECTRONICS)', '53', '', '1', 0),
(1041, 'TER OF TECHNOLOGY [M.TECH] (POWER SYSTEM ENGINEERING)', '53', '', '1', 0),
(1042, 'MASTER OF TECHNOLOGY [M.TECH] (PROCESS METALLURGY)', '53', '', '1', 0),
(1043, 'MASTER OF TECHNOLOGY [M.TECH] (PRODUCTION ENGINEERING)', '53', '', '1', 0),
(1044, 'MASTER OF TECHNOLOGY [M.TECH] (PRODUCTION TECHNOLOGY)', '53', '', '1', 0),
(1045, 'ASTER OF TECHNOLOGY [M.TECH] (RELIABILITY ENGINEERING)', '53', '', '1', 0),
(1046, 'MASTER OF TECHNOLOGY [M.TECH] (ROBOTICS ENGINEERING)', '53', '', '1', 0),
(1047, 'MASTER OF TECHNOLOGY [M.TECH] (RUBBER TECHNOLOGY)', '53', '', '1', 0),
(1048, 'MASTER OF TECHNOLOGY [M.TECH] (SIGNAL PROCESSING)', '53', '', '1', 0),
(1049, 'MASTER OF TECHNOLOGY [M.TECH] (SOFTWARE ENGINEERING)', '53', '', '1', 0),
(1050, 'ASTER OF TECHNOLOGY [M.TECH] (SOIL WATER CONSERVATION ENGINEERING)', '53', '', '1', 0),
(1051, 'MASTER OF TECHNOLOGY [M.TECH] (STRUCTURAL ENGINEERING)', '53', '', '1', 0),
(1052, 'MASTER OF TECHNOLOGY [M.TECH] (TELECOMMUNICATION ENGINEERING)', '53', '', '1', 0),
(1053, 'MASTER OF TECHNOLOGY [M.TECH] (TEXTILE TECHNOLOGY)', '53', '', '1', 0),
(1054, 'MASTER OF TECHNOLOGY [M.TECH] (THERMAL ENGINEERING)', '53', '', '1', 0),
(1055, 'MASTER OF TECHNOLOGY [M.TECH] (THERMAL POWER ENGINEERING)', '53', '', '1', 0),
(1056, 'MASTER OF TECHNOLOGY [M.TECH] (TOOL ENGINEERING)', '53', '', '1', 0),
(1057, 'MASTER OF TECHNOLOGY [M.TECH] (TRANSPORTATION ENGINEERING)', '53', '', '1', 0),
(1058, 'MASTER OF TECHNOLOGY [M.TECH] (VLSI DESIGN & EMBEDDED SYSTEM)', '53', '', '1', 0),
(1059, 'MASTER OF TECHNOLOGY [M.TECH] (VLSI DESIGN)', '53', '', '1', 0),
(1060, 'MASTER OF TECHNOLOGY [M.TECH] (WATER RESOURCES ENGINEERING)', '53', '', '1', 0),
(1061, 'MASTER OF TECHNOLOGY [M.TECH] (WELDING TECHNOLOGY)', '53', '', '1', 0),
(1062, 'ASTER OF TECHNOLOGY [M.TECH] (COMPUTER SCIENCE)', '53', '', '1', 0),
(1063, 'MASTER OF TECHNOLOGY [M.TECH.] (EMBEDDED SYSTEMS)', '53', '', '1', 0),
(1064, 'MASTER OF TECHNOLOGY [M.TECH.] (INTELLIGENT SYSTEM)', '53', '', '1', 0),
(1156, 'Child Health Nursing', '106', '', '1', 0),
(1350, 'Programming and Data Structures', '169', '', '1', 0),
(1074, 'Mathematical Physics', '89', '', '1', 0),
(1075, 'Mechanics', '89', '', '1', 0),
(1076, ' Chemistry', '89', '', '1', 0),
(1077, 'Electricity and Magnetism', '89', '', '1', 0),
(1078, 'Inorganic chemistry', '90', '', '1', 0),
(1079, 'Organic chemistry', '90', '', '1', 0),
(1080, 'Physical chemistry', '90', '', '1', 0),
(1081, 'Analytical chemistry', '90', '', '1', 0),
(1082, 'Website Designing', '91', '', '1', 0),
(1083, 'Object Oriented Programming', '91', '', '1', 0),
(1084, 'Software Engineering', '91', '', '1', 0),
(1085, 'Algorithms ', '91', '', '1', 0),
(1086, 'Computer Organisation', '91', '', '1', 0),
(1099, 'Biomolecules', '101', '', '1', 0),
(1100, 'Enymology', '101', '', '1', 0),
(1101, 'Intermediary Metabolism', '101', '', '1', 0),
(1102, 'Molecular Biology', '101', '', '1', 0),
(1103, 'Clinical Biochemistry', '101', '', '1', 0),
(1104, 'Immunology', '101', '', '1', 0),
(1105, 'Thermal Physics', '89', '', '1', 0),
(1106, 'Vibrations & Wave Optics', '89', '', '1', 0),
(1107, 'Quantum Mechanics And Nuclear Physics ', '89', '', '1', 0),
(1108, 'Computer Fundamentals And Programing', '89', '', '1', 0),
(1109, 'Electromagnetic Theory', '89', '', '1', 0),
(1110, 'Physics', '92', '', '1', 0),
(1111, 'Statistical Physics', '89', '', '1', 0),
(1112, 'Physics Of Materials', '89', '', '1', 0),
(1113, 'Chemistry', '92', '', '1', 0),
(1114, 'Electornic Devices: Physics And Applications ', '89', '', '1', 0),
(1115, 'Mathematics', '92', '', '1', 0),
(1116, 'Modern Chemistry ', '89', '', '1', 0),
(1117, 'Biophysics ', '89', '', '1', 0),
(1118, 'Economics', '89', '', '1', 0),
(1119, 'Physics', '94', '', '1', 0),
(1120, 'Chemistry', '94', '', '1', 0),
(1121, 'Mathematics', '94', '', '1', 0),
(1122, 'Physics', '95', '', '1', 0),
(1123, 'Chemistry', '95', '', '1', 0),
(1124, 'Mathematics', '95', '', '1', 0),
(1125, 'Biological Science', '96', '', '1', 0),
(1126, 'Biotechnology', '96', '', '1', 0),
(1127, 'Chemistry', '96', '', '1', 0),
(1128, 'Geology', '96', '', '1', 0),
(1129, 'Mathematics', '96', '', '1', 0),
(1130, 'Mathematical Statistics', '96', '', '1', 0),
(1131, 'Physics', '96', '', '1', 0),
(1132, 'Physics', '97', '', '1', 0),
(1133, 'Chemistry', '97', '', '1', 0),
(1134, 'Mathematics', '97', '', '1', 0),
(1135, 'Chemistry', '102', '', '1', 0),
(1136, 'Basic Principles Of Earth Science', '102', '', '1', 0),
(1137, 'Basic Principles Of Physics', '102', '', '1', 0),
(1138, 'Animal Biotechnology', '104', '', '1', 0),
(1139, 'Enviromental Management', '104', '', '1', 0),
(1140, 'Animal Bioinformatics', '104', '', '1', 0),
(1141, 'Biodiversity Studies', '104', '', '1', 0),
(1142, ' Ecosystem Monitoring', '104', '', '1', 0),
(1186, 'General Knowledge and current Affairs', '131', '', '1', 0),
(1187, 'Elementary Mathematics (Numerical Ability)', '131', '', '1', 0),
(1188, 'Legal Aptitude', '131', '', '1', 0),
(1189, 'Logical Reasoning', '131', '', '1', 0),
(1190, 'English including Comprehension', '132', '', '1', 0),
(1191, 'General Knowledge and current Affairs', '132', '', '1', 0),
(1192, 'Elementary Mathematics (Numerical Ability)', '132', '', '1', 0),
(1193, 'Legal Aptitude', '132', '', '1', 0),
(1194, 'Logical Reasoning', '132', '', '1', 0),
(1195, 'English including Comprehension', '133', '', '1', 0),
(1196, 'General Knowledge and current Affairs', '133', '', '1', 0),
(1197, 'Elementary Mathematics (Numerical Ability)', '133', '', '1', 0),
(1198, 'Legal Aptitude', '133', '', '1', 0),
(1199, 'Logical Reasoning', '133', '', '1', 0),
(1200, 'English including Comprehension', '134', '', '1', 0),
(1201, 'General Knowledge and current Affairs', '134', '', '1', 0),
(1202, 'Elementary Mathematics (Numerical Ability)', '134', '', '1', 0),
(1203, 'Legal Aptitude', '134', '', '1', 0),
(1204, 'Logical Reasoning', '134', '', '1', 0),
(1205, 'English including Comprehension', '135', '', '1', 0),
(1206, 'General Knowledge and current Affairs', '135', '', '1', 0),
(1207, 'Elementary Mathematics (Numerical Ability)', '135', '', '1', 0),
(1208, 'Legal Aptitude', '135', '', '1', 0),
(1209, 'Logical Reasoning', '135', '', '1', 0),
(1210, 'English including Comprehension', '136', '', '1', 0),
(1211, 'General Knowledge and current Affairs', '136', '', '1', 0),
(1212, 'Elementary Mathematics (Numerical Ability)', '136', '', '1', 0),
(1213, 'Legal Aptitude', '136', '', '1', 0),
(1214, 'Logical Reasoning', '136', '', '1', 0),
(1215, 'Physics', '137', '', '1', 0),
(1216, 'Chemistry', '137', '', '1', 0),
(1217, 'Maths', '137', '', '1', 0),
(1218, 'English', '137', '', '1', 0),
(1219, 'General Knowledge', '137', '', '1', 0),
(1220, 'Aptitude ', '137', '', '1', 0),
(1221, 'Physics', '138', '', '1', 0),
(1222, 'Chemistry', '138', '', '1', 0),
(1223, 'Maths', '138', '', '1', 0),
(1224, 'English ', '138', '', '1', 0),
(1225, 'General Knowledge', '138', '', '1', 0),
(1226, 'English ', '139', '', '1', 0),
(1227, 'Physics', '139', '', '1', 0),
(1228, 'Chemistry', '139', '', '1', 0),
(1229, 'Maths', '139', '', '1', 0),
(1230, 'General Knowledge', '139', '', '1', 0),
(1231, 'English ', '140', '', '1', 0),
(1232, 'Reasoning', '140', '', '1', 0),
(1233, 'Aptitude ', '140', '', '1', 0),
(1234, 'General Knowledge', '140', '', '1', 0),
(1235, 'Mathematics', '141', '', '1', 0),
(1236, 'General Ability', '141', '', '1', 0),
(1238, 'Verbal Ability', '142', '', '1', 0),
(1239, 'Numerical Ability', '142', '', '1', 0),
(1240, 'Reasoning', '142', '', '1', 0),
(1241, 'General Awareness', '142', '', '1', 0),
(1242, 'Military Aptitude', '142', '', '1', 0),
(1243, 'Mathematics', '143', '', '1', 0),
(1244, 'General Awareness', '143', '', '1', 0),
(1245, 'Verbal And Reading Comprehension', '144', '', '1', 0),
(1246, 'Data Interpretation And Logical Reasoning', '144', '', '1', 0),
(1247, 'Quantitative Ability', '144', '', '1', 0),
(1248, 'Verbal And Logical Ability', '145', '', '1', 0),
(1249, 'Decision Making', '145', '', '1', 0),
(1250, 'Quantitative Ability And Data Interpretation', '145', '', '1', 0),
(1251, 'General Knowledge', '145', '', '1', 0),
(1252, 'Essay Writing', '145', '', '1', 0),
(1253, 'General Awareness', '146', '', '1', 0),
(1254, 'General English', '146', '', '1', 0),
(1255, 'Analytical & Logical Reasoning', '146', '', '1', 0),
(1256, 'Quantitative Aptitude', '146', '', '1', 0),
(1257, 'Verbal And Logical Ability', '147', '', '1', 0),
(1258, 'Decision Making', '147', '', '1', 0),
(1259, 'Quantitative Ability And Data Interpretation', '147', '', '1', 0),
(1260, 'General Knowledge', '147', '', '1', 0),
(1261, 'Essay Writing', '147', '', '1', 0),
(1262, 'Language Comprehension', '148', '', '1', 0),
(1263, 'Mathematical Skills', '148', '', '1', 0),
(1264, 'Data Analysis And Sufficiency', '148', '', '1', 0),
(1265, 'Intelligence And Critical Reasoning', '148', '', '1', 0),
(1266, 'Verbal And Logical Ability', '149', '', '1', 0),
(1267, 'Decision Making', '149', '', '1', 0),
(1268, 'Quantitative Ability And Data Interpretation', '149', '', '1', 0),
(1269, 'General Knowledge', '149', '', '1', 0),
(1270, 'Essay Writing', '149', '', '1', 0),
(1271, 'Verbal And Logical Ability', '150', '', '1', 0),
(1272, 'Decision Making', '150', '', '1', 0),
(1273, 'Quantitative Ability And Data Interpretation', '150', '', '1', 0),
(1274, 'General Knowledge', '150', '', '1', 0),
(1275, 'Essay Writing', '150', '', '1', 0),
(1276, 'Language Skills', '151', '', '1', 0),
(1277, 'Quantitative Skills', '151', '', '1', 0),
(1278, 'Logical Reasoning', '151', '', '1', 0),
(1279, 'Verbal Ability And Reading Comprehension', '152', '', '1', 0),
(1280, 'Quantitative Aptitude', '152', '', '1', 0),
(1281, 'English Language', '153', '', '1', 0),
(1282, 'Numerical And Data Analysis', '153', '', '1', 0),
(1283, 'General Knowledge', '153', '', '1', 0),
(1284, 'Reasoning And Intelligence', '153', '', '1', 0),
(1285, 'English Language', '154', '', '1', 0),
(1286, 'Numerical And Data Analysis', '154', '', '1', 0),
(1287, 'General Knowledge', '154', '', '1', 0),
(1288, 'Reasoning And Intelligence', '154', '', '1', 0),
(1289, 'Verbal And Logical Ability', '155', '', '1', 0),
(1290, 'Decision Making', '155', '', '1', 0),
(1291, 'Quantitative Ability And Data Interpretation', '155', '', '1', 0),
(1292, 'General Knowledge', '155', '', '1', 0),
(1293, 'Essay Writing', '155', '', '1', 0),
(1294, 'Quantative Aptitude', '156', '', '1', 0),
(1295, 'Reasoning', '156', '', '1', 0),
(1296, 'English Comprehensive', '156', '', '1', 0),
(1297, 'General Knowledge', '156', '', '1', 0),
(1298, 'Knowledge', '157', '', '1', 0),
(1299, 'Comprehension', '157', '', '1', 0),
(1300, 'Analysis', '157', '', '1', 0),
(1301, 'Creativity', '157', '', '1', 0),
(1302, 'Visualisation', '157', '', '1', 0),
(1303, 'Awareness Of Design', '158', '', '1', 0),
(1304, 'General Awareness', '158', '', '1', 0),
(1305, 'Comprehension', '158', '', '1', 0),
(1306, 'Reasoning', '158', '', '1', 0),
(1307, 'Visualisation', '158', '', '1', 0),
(1308, 'Lateral Thinking', '158', '', '1', 0),
(1309, 'Intelligence And Reasoning', '159', '', '1', 0),
(1310, 'General Awareness', '159', '', '1', 0),
(1311, 'English Language Competency & Comprehension', '159', '', '1', 0),
(1312, 'Drawing', '160', '', '1', 0),
(1313, 'Creativity', '160', '', '1', 0),
(1314, 'Communication Skills', '160', '', '1', 0),
(1315, 'Analytical And Problem Solving Skills', '160', '', '1', 0),
(1316, 'Quantitative Aptitude', '161', '', '1', 0),
(1317, 'Verbal Ability', '161', '', '1', 0),
(1318, 'General Awareness', '161', '', '1', 0),
(1319, 'Business Aptitude Test (Bat) For Bdes-Rfm', '161', '', '1', 0),
(1320, 'Quantitative Aptitude', '162', '', '1', 0),
(1321, 'Verbal Ability', '162', '', '1', 0),
(1322, 'General Awareness', '162', '', '1', 0),
(1323, 'Business Aptitude Test (Bat) For Bdes-Rfm', '162', '', '1', 0),
(1324, 'Logical Reasoning', '163', '', '1', 0),
(1325, 'Critical Thinking', '163', '', '1', 0),
(1326, 'Reasoning And Logical Deduction', '164', '', '1', 0),
(1327, 'Mathematics', '164', '', '1', 0),
(1328, 'General Knowledge & Current Affairs', '164', '', '1', 0),
(1329, 'English', '164', '', '1', 0),
(1330, 'Reasoning ', '165', '', '1', 0),
(1331, 'English', '165', '', '1', 0),
(1332, 'General Awareness', '165', '', '1', 0),
(1333, 'Numerical Ability And Scientific Aptitude', '166', '', '1', 0),
(1334, 'Reasoning And Logical Deduction', '166', '', '1', 0),
(1335, 'General Knowledge & Current Affairs', '166', '', '1', 0),
(1336, 'English', '166', '', '1', 0),
(1337, 'Aptitude', '166', '', '1', 0),
(1338, 'English', '167', '', '1', 0),
(1339, 'General Knowledge & Current Affairs', '167', '', '1', 0),
(1340, 'Logical Reasoning', '167', '', '1', 0),
(1341, 'Elementary Mathematics', '167', '', '1', 0),
(1342, 'English Aptitude', '168', '', '1', 0),
(1343, 'General Awareness', '168', '', '1', 0),
(1344, 'Reasoning ', '168', '', '1', 0),
(1345, 'Hospitality Industry Awareness', '168', '', '1', 0),
(1431, 'TOEFL(Test of English as a Foreign language)', '187', '', '1', 0),
(1433, 'SAT(Scholastic Aptitude Test)', '187', '', '1', 0),
(1434, 'GRE(Graduate Record Examination)', '187', '', '1', 0),
(1435, 'IELTS(International English Language Test)', '187', '', '1', 0),
(1436, 'PET(Preliminary English Test)', '187', '', '1', 0),
(1437, 'PTE(Pearson Test of English)', '187', '', '1', 0),
(1439, 'B.Ed. Entrance', '189', '', '1', 0),
(1440, 'CBSE UGC NET', '189', '', '1', 0),
(1441, 'CTET', '189', '', '1', 0),
(1442, 'HTET', '189', '', '1', 0),
(1443, 'UP B.Ed. JEE', '189', '', '1', 0),
(1444, 'Ed.CET', '189', '', '1', 0),
(1445, 'HCS(Haryana Civil services) Exam', '190', '', '1', 0),
(1446, 'PCS(Punjab Civil Service) Exam', '190', '', '1', 0),
(1447, 'Physics', '116', '', '1', 0),
(1448, 'Chemistry', '116', '', '1', 0),
(1449, 'Mathematics', '116', '', '1', 0),
(1450, 'Physics', '117', '', '1', 0),
(1451, 'Chemistry', '117', '', '1', 0),
(1452, 'Mathematics', '117', '', '1', 0),
(1453, 'Biology', '109', '', '1', 0),
(1454, 'Physics', '109', '', '1', 0),
(1456, 'Chemistry', '109', '', '1', 0),
(1457, 'Biology', '108', '', '1', 0),
(1458, 'Physics', '108', '', '1', 0),
(1459, 'Chemistry', '108', '', '1', 0),
(1460, 'Physics', '120', '', '1', 0),
(1461, 'Chemistry', '120', '', '1', 0),
(1462, 'Biology', '120', '', '1', 0),
(1463, 'Mathematics', '120', '', '1', 0),
(1464, 'Physics', '111', '', '1', 0),
(1465, 'Chemistry', '111', '', '1', 0),
(1466, 'Biology', '111', '', '1', 0),
(1467, 'Physics', '112', '', '1', 0),
(1468, 'Chemistry', '112', '', '1', 0),
(1469, 'Biology', '112', '', '1', 0),
(1470, 'Physics', '118', '', '1', 0),
(1471, 'Chemistry', '118', '', '1', 0),
(1472, 'Biology', '118', '', '1', 0),
(1473, 'Physics', '119', '', '1', 0),
(1474, 'Chemistry', '119', '', '1', 0),
(1475, 'Biology', '119', '', '1', 0),
(1476, 'Physics', '115', '', '1', 0),
(1477, 'Chemistry', '115', '', '1', 0),
(1478, 'Biology', '115', '', '1', 0),
(1479, 'English and Local Reasoning', '115', '', '1', 0),
(1480, 'Pre and Para Clinical subjects', '121', '', '1', 0),
(1481, 'Clinical subjects', '121', '', '1', 0),
(1482, 'COMEDK PGET', '124', '', '1', 0),
(1483, 'Botany', '125', '', '1', 0),
(1484, 'Zoology', '125', '', '1', 0),
(1485, 'Physics', '125', '', '1', 0),
(1486, 'Physics', '126', '', '1', 0),
(1487, 'Chemistry', '126', '', '1', 0),
(1488, 'Biology', '126', '', '1', 0),
(1489, 'General Intelligence & Reasoning', '175', '', '1', 0),
(1490, 'General Knowledge', '175', '', '1', 0),
(1491, 'Quantitative Aptitude', '175', '', '1', 0),
(1492, 'English Comprehension', '175', '', '1', 0),
(1493, 'General Intelligence & Reasoning', '176', '', '1', 0),
(1494, 'General Awareness', '176', '', '1', 0),
(1495, 'Quantitative Aptitude', '176', '', '1', 0),
(1496, 'English Language', '176', '', '1', 0),
(1497, 'General Intelligence & Reasoning', '177', '', '1', 0),
(1498, 'Quantitative Aptitude', '177', '', '1', 0),
(1499, 'Reasoning And Intelligence', '177', '', '1', 0),
(1500, 'English Language', '177', '', '1', 0),
(1501, 'General Hindi', '180', '', '1', 0),
(1502, 'General English', '180', '', '1', 0),
(1503, 'Translation and Essay', '180', '', '1', 0),
(1504, 'Personality Test Tuition ', '180', '', '1', 0),
(1505, ' General Intelligence & Reasoning', '179', '', '1', 0),
(1506, 'General Awareness', '179', '', '1', 0),
(1507, 'Civil & Structural', '179', '', '1', 0),
(1508, 'Electrical', '179', '', '1', 0),
(1509, 'Mechanical', '179', '', '1', 0),
(1510, 'General English', '181', '', '1', 0),
(1513, 'Analytical and Quantitative Skills', '181', '', '1', 0),
(1512, 'General Arithmetic', '181', '', '1', 0),
(1514, 'General Knowledge', '181', '', '1', 0),
(1515, 'Reasoning And Intelligence', '182', '', '1', 0),
(1516, 'General Awareness', '182', '', '1', 0),
(1517, 'General Science', '182', '', '1', 0),
(1518, 'Arithmetic Ability', '182', '', '1', 0),
(1519, 'General Intelligence & Reasoning', '183', '', '1', 0),
(1520, 'General Awareness', '183', '', '1', 0),
(1521, 'Arithmetic Ability', '183', '', '1', 0),
(1522, 'Reasoning', '203', '', '1', 0),
(1523, 'english', '203', '', '1', 0),
(1524, 'General Awareness', '203', '', '1', 0),
(1525, 'Quantitative Aptitude', '203', '', '1', 0),
(1526, 'Insurance Management', '203', '', '1', 0),
(1527, 'Economic and Social issues impacting Insurance', '203', '', '1', 0),
(1528, 'Reasoning', '201', '', '1', 0),
(1529, 'English', '201', '', '1', 0),
(1530, 'Numerical Aptitude', '201', '', '1', 0),
(1531, 'General Awareness', '201', '', '1', 0),
(1532, 'Reasoning', '202', '', '1', 0),
(1533, 'English', '202', '', '1', 0),
(1534, 'Numerical Aptitude', '202', '', '1', 0),
(1535, 'General Awareness', '202', '', '1', 0),
(1536, 'Quantitative Aptitude', '204', '', '1', 0),
(1537, 'English including Comprehension', '204', '', '1', 0),
(1538, 'General Awareness', '204', '', '1', 0),
(1539, 'General Intelligence & Reasoning', '204', '', '1', 0),
(1540, 'Computer', '204', '', '1', 0);
INSERT INTO `app_subjects` (`id`, `name`, `class_id`, `non_acc_img`, `status`, `trending`) VALUES
(1541, 'Quantitative Aptitude', '191', '', '1', 0),
(1542, 'Reasoning', '191', '', '1', 0),
(1543, 'English', '191', '', '1', 0),
(1544, 'General Awareness', '191', '', '1', 0),
(1545, 'Computer', '191', '', '1', 0),
(1546, 'Quantitative Aptitude', '197', '', '1', 0),
(1547, 'Reasoning', '197', '', '1', 0),
(1548, 'English', '197', '', '1', 0),
(1549, 'General Awareness', '197', '', '1', 0),
(1550, 'Computer', '197', '', '1', 0),
(1551, 'Quantitative Aptitude', '198', '', '1', 0),
(1552, 'Reasoning', '198', '', '1', 0),
(1553, 'English', '198', '', '1', 0),
(1554, 'General Awareness', '198', '', '1', 0),
(1555, 'Computer', '198', '', '1', 0),
(1556, 'Quantitative Aptitude', '192', '', '1', 0),
(1557, 'Reasoning', '192', '', '1', 0),
(1558, 'English', '192', '', '1', 0),
(1559, 'General Awareness', '192', '', '1', 0),
(1560, 'Computer', '192', '', '1', 0),
(1561, 'Quantitative Aptitude', '194', '', '1', 0),
(1562, 'Reasoning', '194', '', '1', 0),
(1563, 'English', '194', '', '1', 0),
(1564, 'General Awareness', '194', '', '1', 0),
(1565, 'Elementary Mathematics (Numerical Ability)', '194', '', '1', 0),
(1566, 'Elementary Mathematics (Numerical Ability)', '195', '', '1', 0),
(1567, 'English', '195', '', '1', 0),
(1568, 'General Awareness', '195', '', '1', 0),
(1569, 'Reasoning', '195', '', '1', 0),
(1570, 'Quantitative Aptitude', '195', '', '1', 0),
(1571, 'Reasoning', '193', '', '1', 0),
(1572, 'Quantitative Aptitude', '193', '', '1', 0),
(1573, 'English', '193', '', '1', 0),
(1574, 'Professional Knowledge', '193', '', '1', 0),
(1575, 'Reasoning', '199', '', '1', 0),
(1576, 'Quantitative Aptitude', '199', '', '1', 0),
(1577, 'English', '199', '', '1', 0),
(1578, 'Professional Knowledge', '199', '', '1', 0),
(1579, 'Mathematics', '208', '', '1', 0),
(1580, 'General Aptitude', '208', '', '1', 0),
(1581, 'Mathematics', '220', '', '1', 0),
(1582, 'Basic Physical Sciences', '220', '', '1', 0),
(1583, 'Statics and Dynamics', '220', '', '1', 0),
(1584, 'Strength of materials and solid mechanics', '220', '', '1', 0),
(1585, 'Materials Engineering', '220', '', '1', 0),
(1586, 'Thermodynamics,', '220', '', '1', 0),
(1587, 'Fluid mechanics', '220', '', '1', 0),
(1588, 'Mechanism and Machine design ', '220', '', '1', 0),
(1589, 'Instrumentation and measurement', '220', '', '1', 0),
(1590, 'Vibration, control theory and control engineering', '220', '', '1', 0),
(1591, 'Hydraulics, and pneumatics', '220', '', '1', 0),
(1592, 'Mechatronics, and robotics', '220', '', '1', 0),
(1593, 'Engineering design and product design', '220', '', '1', 0),
(1594, 'Drafting', '220', '', '1', 0),
(1595, 'Chemistry', '220', '', '1', 0),
(1596, '	Engineering Graphics', '220', '', '1', 0),
(1597, '	Kinematics', '220', '', '1', 0),
(1598, '	Economics For Engineers', '220', '', '1', 0),
(1599, '	Production Technology', '220', '', '1', 0),
(1600, 'Organizational Behavior', '220', '', '1', 0),
(1601, 'Mathematics', '219', '', '1', 0),
(1602, '	Chemistry', '219', '', '1', 0),
(1603, '	Elements of Electrical Engineering', '219', '', '1', 0),
(1604, '	Art of Programming', '219', '', '1', 0),
(1605, 'Engineering Graphics', '219', '', '1', 0),
(1606, '	Physics', '219', '', '1', 0),
(1607, 'Building Construction Materials', '219', '', '1', 0),
(1608, 'Concrete Technology', '219', '', '1', 0),
(1609, 'Economics for Engineers', '219', '', '1', 0),
(1610, 'Geology and Geotechnical Engineering', '219', '', '1', 0),
(1611, 'Fluid Mechanics', '219', '', '1', 0),
(1612, '	Foundation Engineering', '219', '', '1', 0),
(1613, 'Design of Structures', '219', '', '1', 0),
(1614, '	Law for Engineers', '219', '', '1', 0),
(1615, 'Construction and Project Management', '219', '', '1', 0),
(1616, '	Organizational Behavior', '219', '', '1', 0),
(1617, 'Mathematics', '221', '', '1', 0),
(1618, 'Chemistry', '221', '', '1', 0),
(1619, 'Physics', '221', '', '1', 0),
(1620, 'Engineering Graphics', '221', '', '1', 0),
(1621, 'Environment and Energy Studies', '221', '', '1', 0),
(1622, '	Elements of Electrical Engineering', '221', '', '1', 0),
(1623, '	Thermal and Hydraulics Prime Movers', '221', '', '1', 0),
(1624, 'Analog Electronic Circuits', '221', '', '1', 0),
(1625, 'Network Analysis and Synthesis', '221', '', '1', 0),
(1626, 'Electrical Transducers and Measurements', '221', '', '1', 0),
(1627, '	Electrical Engineering Materials', '221', '', '1', 0),
(1628, 'Fundamentals of Electrical Power System', '221', '', '1', 0),
(1629, 'DC Machines and Transformers', '221', '', '1', 0),
(1630, 'Engineering Electromagnetics', '221', '', '1', 0),
(1631, 'Fundamentals of Power Electronics', '221', '', '1', 0),
(1632, 'Power Electronic Converters', '221', '', '1', 0),
(1633, 'Power System Operation and Control', '221', '', '1', 0),
(1634, 'Electrical Drives and Traction Systems', '221', '', '1', 0),
(1635, 'Power System Protection and Switchgear', '221', '', '1', 0),
(1636, 'Organizational Behavior', '221', '', '1', 0),
(1637, 'Mathematics', '218', '', '1', 0),
(1638, '	Physics', '218', '', '1', 0),
(1639, '	Engineering Graphics', '218', '', '1', 0),
(1640, '	Chemistry', '218', '', '1', 0),
(1641, '	Computer Programming', '218', '', '1', 0),
(1642, '	Elements of Electrical Engineering', '218', '', '1', 0),
(1643, '	Basic Electronics', '218', '', '1', 0),
(1644, '	Digital Systems', '218', '', '1', 0),
(1645, '	Object Oriented Programming', '218', '', '1', 0),
(1646, 'Applied Mathematics', '218', '', '1', 0),
(1647, '	Data Communication', '218', '', '1', 0),
(1648, 'Computer Organization', '218', '', '1', 0),
(1649, 'Data Structures', '218', '', '1', 0),
(1650, 'Theory of Computation', '218', '', '1', 0),
(1651, 'Database Management Systems', '218', '', '1', 0),
(1652, '	Computer Networks', '218', '', '1', 0),
(1653, 'Operating Systems', '218', '', '1', 0),
(1654, 'Web designing', '218', '', '1', 0),
(1655, 'Software Engineering', '218', '', '1', 0),
(1656, '.net Technologies', '218', '', '1', 0),
(1657, 'Java Technologies', '218', '', '1', 0),
(1658, 'Objective C Programming ', '218', '', '1', 0),
(1659, 'System Software', '218', '', '1', 0),
(1660, 'Mobile Applications Development Technologies', '218', '', '1', 0),
(1661, 'Advanced Computer Networks', '218', '', '1', 0),
(1662, 'Main Frame Systems', '218', '', '1', 0),
(1663, 'Computer Graphics and Visualization ', '218', '', '1', 0),
(1664, 'Software Testing ', '218', '', '1', 0),
(1665, 'Cloud Computing', '218', '', '1', 0),
(1666, 'Data Mining', '218', '', '1', 0),
(1667, 'Artificial Intelligence', '218', '', '1', 0),
(1668, 'Compiler Construction', '218', '', '1', 0),
(1669, 'Interfacing with Microprocessor', '218', '', '1', 0),
(1670, 'Digital Image Processing', '218', '', '1', 0),
(1671, 'Web security', '218', '', '1', 0),
(1672, 'Agile Software Development ', '218', '', '1', 0),
(1673, 'Secure software engineering', '218', '', '1', 0),
(1674, 'Network programming', '218', '', '1', 0),
(1675, 'Mathematics', '222', '', '1', 0),
(1676, 'Physics', '222', '', '1', 0),
(1677, 'Mechanics of Solids', '222', '', '1', 0),
(1678, 'Engineering Graphics', '222', '', '1', 0),
(1679, 'Chemistry', '222', '', '1', 0),
(1680, 'Art of Programming', '222', '', '1', 0),
(1681, 'Elements of Electrical Engineering', '222', '', '1', 0),
(1682, 'Digital Systems', '222', '', '1', 0),
(1683, 'Object Oriented Programming', '222', '', '1', 0),
(1684, 'Mathematical foundation of Computer Science Mathematical foundation of Computer Science', '222', '', '1', 0),
(1685, 'Communication Engineering', '222', '', '1', 0),
(1686, 'Computer Organization', '222', '', '1', 0),
(1687, 'Probability Statistics and Numerical Analysis', '222', '', '1', 0),
(1688, '	Theory of Computation', '222', '', '1', 0),
(1689, 'Database Management Systems', '222', '', '1', 0),
(1690, '	Data Communication Networks', '222', '', '1', 0),
(1691, 'Operating Systems', '222', '', '1', 0),
(1692, 'Web designing', '222', '', '1', 0),
(1693, 'Software Engineering', '222', '', '1', 0),
(1694, 'Design and Analysis of Algorithms', '222', '', '1', 0),
(1695, '.net Technologies', '222', '', '1', 0),
(1696, '	Java Technologies', '222', '', '1', 0),
(1697, 'Objective C Programming ', '222', '', '1', 0),
(1698, 'Mobile Applications Development Technologies', '222', '', '1', 0),
(1699, 'Advanced Computer Networks', '222', '', '1', 0),
(1700, 'Computer Graphics and Visualization', '222', '', '1', 0),
(1701, '	Main Frame Systems', '222', '', '1', 0),
(1702, 'Network Security and Encryption', '222', '', '1', 0),
(1703, 'Software Testing', '222', '', '1', 0),
(1704, 'Business Analysis and Optimization', '222', '', '1', 0),
(1705, 'IT Industry Management', '222', '', '1', 0),
(1706, 'Advanced Data Structures', '222', '', '1', 0),
(1707, 'Artificial Intelligence', '222', '', '1', 0),
(1708, 'Network programming', '222', '', '1', 0),
(1709, 'Compiler Construction', '222', '', '1', 0),
(1710, 'System Software ', '222', '', '1', 0),
(1711, 'Mathematics', '223', '', '1', 0),
(1712, 'Physics', '223', '', '1', 0),
(1713, 'Mechanics of Solids', '223', '', '1', 0),
(1714, 'Engineering Graphics', '223', '', '1', 0),
(1715, '	Chemistry', '223', '', '1', 0),
(1716, 'Art of Programming', '223', '', '1', 0),
(1717, '	Elements of Electrical Engineering', '223', '', '1', 0),
(1718, 'Electronics Devices and Circuits', '223', '', '1', 0),
(1719, 'Digital Circuits', '223', '', '1', 0),
(1720, 'Network Analysis', '223', '', '1', 0),
(1721, 'Linear Control System', '223', '', '1', 0),
(1722, 'Signals and Systems', '223', '', '1', 0),
(1723, 'Electrical Machines and Drives', '223', '', '1', 0),
(1724, 'Electronics Design, Tools and Packages', '223', '', '1', 0),
(1725, 'Electromagnetics Engineering', '223', '', '1', 0),
(1726, 'Integrated Circuits and Applications', '223', '', '1', 0),
(1727, 'Microprocessor and Computer Architecture', '223', '', '1', 0),
(1728, 'Digital Communication', '223', '', '1', 0),
(1729, 'Digital Signal Processing', '223', '', '1', 0),
(1730, 'Digital System Design', '223', '', '1', 0),
(1731, 'Antenna and Wave Propagation', '223', '', '1', 0),
(1732, 'Fiber Optic Communication', '223', '', '1', 0),
(1733, 'Digital Integrated Circuit Design', '223', '', '1', 0),
(1734, 'System Modeling and Design', '223', '', '1', 0),
(1735, 'Modern Processor Architecture', '223', '', '1', 0),
(1736, 'Satellite Communication', '223', '', '1', 0),
(1737, 'Data Communication and Networking', '223', '', '1', 0),
(1738, 'Embedded Systems', '223', '', '1', 0),
(1739, 'Microwave Engineering', '223', '', '1', 0),
(1740, 'Multimedia Systems', '223', '', '1', 0),
(1741, 'High Performance VLSI Design', '223', '', '1', 0),
(1742, '	Wireless Sensor Networks', '223', '', '1', 0),
(1743, 'Broadband Network', '223', '', '1', 0),
(1744, 'RF Communication Circuits', '223', '', '1', 0),
(1745, 'Introductory Microeconomics', '217', '', '1', 0),
(1751, 'Economic Theory', '217', '', '1', 0),
(1748, 'Introductory Macroeconomics', '217', '', '1', 0),
(1750, 'Mathematical Methods for Economics', '217', '', '1', 0),
(1752, 'Macroeconomics and International Trade', '217', '', '1', 0),
(1753, 'Money and Financial Systems', '217', '', '1', 0),
(1754, 'Indian Economy Since Independence', '217', '', '1', 0),
(1755, 'Economic Systems', '217', '', '1', 0),
(1756, 'Public Finance', '217', '', '1', 0),
(1757, 'Comparative Economic Development', '217', '', '1', 0),
(1758, 'Financial Accounting', '216', '', '1', 0),
(1759, 'Elements of Commerce', '216', '', '1', 0),
(1760, 'Business Mathematics', '216', '', '1', 0),
(1761, 'Principles of Micro Economics', '216', '', '1', 0),
(1762, 'Computer Applications', '216', '', '1', 0),
(1763, 'Costing', '216', '', '1', 0),
(1764, 'Business Statistics', '216', '', '1', 0),
(1765, 'Principles of Macro Economics', '216', '', '1', 0),
(1766, 'Business Communication', '216', '', '1', 0),
(1767, 'Corporate Accounting', '216', '', '1', 0),
(1768, 'Cost and Management Accounting', '216', '', '1', 0),
(1769, 'Company Law', '216', '', '1', 0),
(1770, 'Indian Economy', '216', '', '1', 0),
(1771, 'Financial Management', '216', '', '1', 0),
(1772, 'Direct Tax Laws', '216', '', '1', 0),
(1773, 'General and Commercial Laws', '216', '', '1', 0),
(1774, 'Business Mathematics', '215', '', '1', 0),
(1775, 'Principles of Micro Economics', '215', '', '1', 0),
(1776, 'Principles of Financial Accounting', '215', '', '1', 0),
(1777, 'Fundamentals of Information Technology', '215', '', '1', 0),
(1778, 'Elements of Management', '215', '', '1', 0),
(1779, 'Principles of Macro Economics', '215', '', '1', 0),
(1780, 'Company Accounts', '215', '', '1', 0),
(1781, 'Introduction to Indian Society', '215', '', '1', 0),
(1782, 'Introduction to Indian Business Environment', '215', '', '1', 0),
(1783, 'Introduction to Business Statistics', '215', '', '1', 0),
(1784, 'Cost & Management Accounting', '215', '', '1', 0),
(1785, 'Taxation', '215', '', '1', 0),
(1786, 'Introduction to Operations Research', '215', '', '1', 0),
(1787, 'Indian Business History', '215', '', '1', 0),
(1788, 'Business Law', '215', '', '1', 0),
(1789, 'Human Resource Management', '215', '', '1', 0),
(1790, 'Introduction to Operations Management', '215', '', '1', 0),
(1791, 'Introduction to Strategic Management', '215', '', '1', 0),
(1792, 'Principles of Research Methodology', '215', '', '1', 0),
(1893, 'German', '264', '', '1', 0),
(1795, 'Database Management Systems', '225', '', '1', 0),
(1892, 'Writing skills', '261', '', '1', 0),
(1891, 'Spoken English', '261', '', '1', 0),
(1890, 'Skating', '185', '', '1', 0),
(1889, 'Ice Hockey', '185', '', '1', 0),
(1888, 'Yoga', '185', '', '1', 0),
(1887, 'Aerobics', '185', '', '1', 0),
(1886, 'Zumba', '185', '', '1', 0),
(1884, 'GS PAPER I', '259', '', '1', 0),
(1883, 'CSAT PAPER II', '259', '', '1', 0),
(1885, 'Running', '185', '', '1', 0),
(1842, 'PHYSICS', '11', '', '1', 0),
(1843, 'CHEMISTRY', '11', '', '1', 0),
(1844, 'COMPUTER SCIENCE', '11', '', '1', 0),
(1845, 'BIOLOGY', '11', '', '1', 0),
(1846, 'HINDI', '11', '', '1', 0),
(1847, 'HISTORY', '11', '', '1', 0),
(1848, 'GEOGRAPHY', '11', '', '1', 0),
(1849, 'POLITICAL SCIENCE', '11', '', '1', 0),
(1850, 'MUSIC', '11', '', '1', 0),
(1851, 'PSYCHOLOGY', '11', '', '1', 0),
(1852, 'SOCIOLOGY', '11', '', '1', 0),
(1853, 'FASHION STUDIES', '11', '', '1', 0),
(1854, 'SANSKRIT', '11', '', '1', 0),
(1855, 'ANTHROPOLOGY', '11', '', '1', 0),
(1856, 'ARCHAEOLOGY', '11', '', '1', 0),
(1857, 'ECONOMICS', '11', '', '1', 0),
(1858, 'PHYSICS', '12', '', '1', 0),
(1859, 'CHEMISTRY', '12', '', '1', 0),
(1860, 'COMPUTER SCIENCE', '12', '', '1', 0),
(1861, 'BIOLOGY', '12', '', '1', 0),
(1862, 'HINDI', '12', '', '1', 0),
(1863, 'HISTORY', '12', '', '1', 0),
(1864, 'GEOGRAPHY', '12', '', '1', 0),
(1865, 'POLITICAL SCIENCE', '12', '', '1', 0),
(1866, 'MUSIC', '12', '', '1', 0),
(1867, 'PSYCHOLOGY', '12', '', '1', 0),
(1868, 'SOCIOLOGY', '12', '', '1', 0),
(1869, 'FASHION STUDIES', '12', '', '1', 0),
(1870, 'SANSKRIT', '12', '', '1', 0),
(1871, 'ANTHROPOLOGY', '12', '', '1', 0),
(1872, 'ARCHAEOLOGY', '12', '', '1', 0),
(1876, 'CLASS I', '1', '', '1', 0),
(1881, 'ALL CLASSES', '1', '', '1', 0),
(1894, 'French', '263', '', '1', 0),
(1895, 'Sanskrit', '262', '', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_topic`
--

CREATE TABLE `app_topic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject_id` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `assignment_mark`
--

CREATE TABLE `assignment_mark` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attendence`
--

CREATE TABLE `attendence` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `batch_id` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(1) NOT NULL COMMENT 'P: present, A: absent'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendence`
--

INSERT INTO `attendence` (`id`, `teacher_id`, `student_id`, `batch_id`, `date`, `status`) VALUES
(109, 241, '103', '156', '2018-02-26', ''),
(2, 9, '1', '1', '2017-12-21', 'P'),
(3, 18, '1', '3', '2017-12-22', 'P'),
(4, 28, '6', '7', '2017-12-23', 'P'),
(5, 2, '', '15', '2018-01-04', 'P'),
(6, 9, '1', '1', '2018-01-10', 'P'),
(7, 9, '2', '1', '2018-01-10', 'A'),
(8, 9, '20', '1', '2018-01-10', 'P'),
(9, 9, '6', '1', '2018-01-10', 'P'),
(10, 9, '1', '1', '2018-01-11', 'P'),
(11, 9, '2', '1', '2018-01-11', 'P'),
(12, 9, '20', '1', '2018-01-11', 'P'),
(13, 9, '6', '1', '2018-01-11', 'P'),
(14, 2, '1', '5', '2018-01-19', 'P'),
(15, 2, '22', '5', '2018-01-19', 'P'),
(16, 97, '25', '53', '2018-01-20', 'P'),
(17, 99, '25', '56', '2018-01-20', 'P'),
(18, 99, '25', '56', '2018-01-21', 'P'),
(19, 2, '22', '15', '2018-01-21', 'P'),
(20, 2, '1', '15', '2018-01-21', 'P'),
(21, 2, '2', '15', '2018-01-21', 'P'),
(22, 75, '1', '52', '2018-01-23', 'P'),
(95, 75, '63', '155', '2018-02-24', 'L'),
(24, 87, '30', '114', '2018-01-24', 'P'),
(25, 49, '4', '104', '2018-01-24', 'P'),
(26, 49, '30', '104', '2018-01-24', 'P'),
(27, 9, '20', '119', '2018-01-25', 'P'),
(28, 87, '30', '114', '2018-01-25', 'P'),
(29, 164, '30', '95', '2018-02-06', 'P'),
(30, 164, '5', '95', '2018-02-06', 'A'),
(31, 99, '25', '57', '2018-02-12', 'P'),
(32, 2, '22', '15', '2018-02-13', 'P'),
(110, 241, '104', '158', '2018-02-26', 'A'),
(111, 241, '105', '158', '2018-02-26', 'L'),
(112, 241, '106', '158', '2018-02-26', 'P'),
(96, 75, '64', '155', '2018-02-24', ''),
(97, 75, '65', '155', '2018-02-24', ''),
(98, 75, '66', '155', '2018-02-24', ''),
(99, 75, '67', '155', '2018-02-24', ''),
(100, 75, '68', '155', '2018-02-24', ''),
(101, 75, '69', '155', '2018-02-24', ''),
(102, 75, '73', '155', '2018-02-24', ''),
(103, 75, '82', '155', '2018-02-24', 'L'),
(104, 75, '101', '155', '2018-02-24', ''),
(105, 241, '92', '156', '2018-02-26', ''),
(106, 241, '94', '156', '2018-02-26', ''),
(107, 241, '97', '156', '2018-02-26', ''),
(108, 241, '102', '156', '2018-02-26', ''),
(90, 75, '1', '155', '2018-02-24', 'P'),
(91, 75, '58', '155', '2018-02-24', 'L'),
(92, 75, '36', '155', '2018-02-24', 'L'),
(93, 75, '61', '155', '2018-02-24', ''),
(94, 75, '62', '155', '2018-02-24', 'A'),
(113, 241, '107', '158', '2018-02-26', ''),
(114, 75, '1', '155', '2018-02-26', 'P'),
(115, 75, '58', '155', '2018-02-26', ''),
(116, 75, '36', '155', '2018-02-26', ''),
(117, 75, '61', '155', '2018-02-26', ''),
(118, 75, '62', '155', '2018-02-26', ''),
(119, 75, '63', '155', '2018-02-26', ''),
(120, 75, '64', '155', '2018-02-26', ''),
(121, 75, '65', '155', '2018-02-26', ''),
(122, 75, '66', '155', '2018-02-26', ''),
(123, 75, '67', '155', '2018-02-26', ''),
(124, 75, '68', '155', '2018-02-26', ''),
(125, 75, '69', '155', '2018-02-26', ''),
(126, 75, '73', '155', '2018-02-26', 'A'),
(127, 75, '82', '155', '2018-02-26', ''),
(128, 75, '101', '155', '2018-02-26', ''),
(129, 241, '108', '158', '2018-02-26', 'P'),
(130, 241, '109', '158', '2018-02-26', ''),
(131, 241, '104', '158', '2018-02-27', ''),
(132, 241, '105', '158', '2018-02-27', ''),
(133, 241, '106', '158', '2018-02-27', ''),
(134, 241, '107', '158', '2018-02-27', ''),
(135, 241, '108', '158', '2018-02-27', 'A'),
(136, 241, '109', '158', '2018-02-27', 'P'),
(137, 75, '1', '51', '2018-02-28', 'P'),
(138, 75, '110', '51', '2018-02-28', 'L'),
(139, 241, '104', '158', '2018-02-28', 'A'),
(140, 241, '105', '158', '2018-02-28', 'P'),
(141, 241, '106', '158', '2018-02-28', ''),
(142, 241, '107', '158', '2018-02-28', 'P'),
(143, 241, '108', '158', '2018-02-28', 'A'),
(144, 241, '109', '158', '2018-02-28', 'A'),
(145, 241, '111', '158', '2018-02-28', 'A'),
(146, 241, '112', '158', '2018-02-28', 'L'),
(147, 241, '113', '158', '2018-02-28', 'A'),
(148, 241, '114', '158', '2018-02-28', 'L'),
(149, 241, '77', '144', '2018-02-28', 'P'),
(150, 241, '78', '144', '2018-02-28', 'P'),
(151, 241, '79', '144', '2018-02-28', 'P'),
(152, 241, '80', '144', '2018-02-28', 'P'),
(153, 241, '81', '144', '2018-02-28', 'P'),
(154, 241, '83', '144', '2018-02-28', ''),
(155, 241, '84', '144', '2018-02-28', ''),
(156, 241, '85', '144', '2018-02-28', ''),
(157, 241, '86', '144', '2018-02-28', 'A'),
(158, 241, '93', '144', '2018-02-28', 'A'),
(159, 241, '115', '144', '2018-02-28', ''),
(160, 241, '104', '158', '2018-03-06', 'A'),
(161, 241, '105', '158', '2018-03-06', ''),
(162, 241, '106', '158', '2018-03-06', ''),
(163, 241, '107', '158', '2018-03-06', ''),
(164, 241, '108', '158', '2018-03-06', ''),
(165, 241, '109', '158', '2018-03-06', ''),
(166, 241, '111', '158', '2018-03-06', ''),
(167, 241, '112', '158', '2018-03-06', 'L'),
(168, 241, '113', '158', '2018-03-06', ''),
(169, 241, '114', '158', '2018-03-06', ''),
(170, 241, '116', '162', '2018-03-09', 'P'),
(171, 241, '117', '162', '2018-03-09', 'A'),
(172, 241, '118', '162', '2018-03-09', 'P'),
(173, 241, '116', '162', '2018-03-14', ''),
(174, 241, '117', '162', '2018-03-14', ''),
(175, 241, '118', '162', '2018-03-14', ''),
(176, 241, '120', '162', '2018-03-14', ''),
(177, 241, '42', '162', '2018-03-14', ''),
(178, 241, '121', '162', '2018-03-14', ''),
(179, 241, '6', '162', '2018-03-14', 'A'),
(180, 241, '122', '162', '2018-03-14', 'A'),
(181, 249, '130', '168', '2018-03-16', 'A'),
(182, 249, '131', '168', '2018-03-16', 'A'),
(183, 249, '132', '168', '2018-03-16', 'P'),
(184, 249, '133', '168', '2018-03-16', 'L'),
(185, 249, '124', '168', '2018-03-16', 'P'),
(186, 249, '134', '169', '2018-03-16', 'A'),
(187, 249, '135', '169', '2018-03-16', ''),
(188, 75, '119', '160', '2018-04-04', 'L'),
(189, 253, '137', '170', '2018-04-04', 'P'),
(190, 253, '138', '170', '2018-04-04', 'L'),
(191, 253, '139', '170', '2018-04-04', 'P'),
(192, 253, '137', '170', '2018-04-05', ''),
(193, 253, '138', '170', '2018-04-05', ''),
(194, 253, '139', '170', '2018-04-05', ''),
(195, 253, '125', '170', '2018-04-05', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `availability`
--

CREATE TABLE `availability` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `avail_date` date NOT NULL,
  `avail_time` varchar(255) NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `area` varchar(200) DEFAULT NULL,
  `available` enum('YES','NO') DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active, 0 for inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `teacher_id` bigint(22) DEFAULT NULL,
  `payee_name` varchar(255) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `bank_account` varchar(35) DEFAULT NULL,
  `bank_ifsc` varchar(22) NOT NULL,
  `pan_number` varchar(20) NOT NULL,
  `bank_address` text NOT NULL,
  `created_by` bigint(22) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barcode`
--

CREATE TABLE `barcode` (
  `bid` int(12) NOT NULL,
  `barcode_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `user_type` int(1) NOT NULL COMMENT '0:teacher, 1:student',
  `issue_date` varchar(64) NOT NULL,
  `image` varchar(512) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` int(11) NOT NULL,
  `day_name` text NOT NULL,
  `batch_demo` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `batch_code` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `batch_type` varchar(255) NOT NULL,
  `batch_start_time` varchar(255) NOT NULL,
  `batch_end_time` varchar(255) NOT NULL,
  `batch_start_date` varchar(255) NOT NULL,
  `batch_end_date` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `batch_strength` int(11) NOT NULL,
  `batch_fee` varchar(255) NOT NULL,
  `batch_status` tinyint(4) NOT NULL COMMENT '0 for past batch, 1 for active batch, 2 for future batch',
  `added_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `day_name`, `batch_demo`, `title`, `batch_code`, `topic`, `teacher_id`, `class_id`, `class_name`, `batch_type`, `batch_start_time`, `batch_end_time`, `batch_start_date`, `batch_end_date`, `duration`, `subject_id`, `subject_name`, `batch_strength`, `batch_fee`, `batch_status`, `added_date`) VALUES
(1, 'Friday,Thursday,Saturday,Wednesday,Monday,Sunday,Tuesday', '1', 'Cat', 'CAT7073', 'Algos', 1, 144, 'CAT', '', '15:00:00', '18:00:00', '2019-01-10', '2029-12-20', '', '1247', 'Quantitative Ability', 1, '100.00', 2, ''),
(2, 'Saturday,Sunday', '2', 'ENglish Batch', 'ENGL3890', 'How To Learn Enflish', 1, 184, 'Gate BT', 'close', '15:00:00', '21:00:00', '2017-12-03', '2018-05-17', '', '1376', 'Engineering Mathematics', 5, '100.00', 1, ''),
(3, 'Wednesday,Sunday,Monday,Tuesday,Thursday,Friday,Saturday', '1', 'Engish', 'ENGI6273', 'Railway Exams', 18, 181, 'RTCG (Railway Technical Cadre exam)', '', '15:00:00', '22:00:00', '2017-12-30', '2018-02-22', '', '1513', 'Analytical and Quantitative Skills', 10, '50.00', 0, ''),
(5, 'Saturday,Sunday', '2', 'Science', 'SCIE8284', 'Physics', 2, 92, 'JEE Mains', 'close', '05:00:00', '06:00:00', '2017-12-30', '2018-02-28', '2 months', '1115', 'Mathematics', 25, '750.00', 0, ''),
(6, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', '11 Class Accounts', '11CL2058', 'Rectification Of Error', 26, 11, 'CLASS XI(COMMERCE)', 'close', '03:30:00', '03:30:00', '2017-07-10', '2018-01-10', '', '43', 'ACCOUNTANCY', 16, '500.00', 0, ''),
(7, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '7', 'Class 10 ', 'CLAS9948', 'Surface Area And Volume', 28, 10, 'CLASS X', '', '04:30:00', '06:30:00', '2017-04-01', '2018-03-31', '', '374', 'MATHEMATICS', 25, '1000.00', 1, ''),
(8, 'Monday,Wednesday,Thursday,Friday,Saturday,Tuesday', '7', 'Class 9', 'CLAS7174', 'Circle', 28, 9, 'CLASS IX', '', '04:28:00', '06:29:00', '2017-04-01', '2018-03-31', '', '361', 'MATHEMATICS', 30, '1500.00', 1, ''),
(9, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '7', 'Class8', 'CLAS3848', 'Congruence', 28, 8, 'CLASS VIII', '', '04:30:00', '00:00:00', '2017-04-01', '2018-03-31', '', '348', 'MATHEMATICS', 20, '1800.00', 1, ''),
(10, 'Monday,Saturday,Tuesday,Wednesday,Thursday,Friday', '2', 'Class 11 Biology ', 'CLAS8212', 'Genetics ', 30, 18, 'CLASS XI(MEDICAL)', 'open', '05:00:00', '06:00:00', '2018-01-01', '2018-01-31', '', '59', 'BIOLOGY', 25, '800.00', 0, ''),
(11, 'Monday,Wednesday,Friday', '2', 'Programming And Language', 'PROG2462', 'C++, C, Java', 29, 218, 'Computer Science  Engineering', '', '07:30:00', '07:00:00', '2018-02-05', '2018-03-31', '', '1658', 'Objective C Programming ', 20, '2000.00', 1, ''),
(12, 'Friday,Thursday,Monday,Sunday,Tuesday,Wednesday,Saturday', '1', 'Hauz', 'HAUZ3088', 'Hau', 36, 183, 'RPF (Sub-Inspector/Constable) exam', '', '11:40:00', '14:00:00', '2017-12-26', '2017-12-26', '', '1520', 'General Awareness', 8, '100.00', 0, ''),
(13, 'Saturday,Friday,Tuesday,Monday,Sunday,Wednesday,Thursday', '1', 'Hellooo', 'HELL9513', 'Hind', 44, 16, 'NETWORKING', '', '00:00:00', '10:00:00', '2018-01-24', '2018-01-25', '', '487', 'CCNA CCNP CCIE MICROSOFT MCSE COURSES', 5, '50.00', 0, ''),
(14, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1', 'Class 11', 'CLAS9469', 'Classification Of Organisms', 46, 18, 'CLASS XI(MEDICAL)', 'open', '15:00:00', '16:00:00', '2017-12-26', '2019-02-28', '', '59', 'BIOLOGY', 25, '600.00', 1, ''),
(15, 'Thursday,Friday,Saturday', '2', 'Fflfl dkxkzkxkxkxk xk Kxkxkx kxxkxkx kxkxjzj', 'FFLF1959', 'Cell', 2, 18, 'CLASS XI(MEDICAL)', 'close', '05:00:00', '06:00:00', '2017-12-28', '2017-12-31', '2 months', '59', 'BIOLOGY', 25, '2500.00', 0, ''),
(16, 'Saturday,Friday,Thursday', '2', 'Sciece', 'SCIE4337', 'Alkene', 58, 94, 'JEE Advanced', '', '07:00:00', '00:00:00', '2017-12-28', '2018-02-23', '', '1120', 'Chemistry', 5, '150.00', 0, ''),
(17, 'Saturday,Friday,Thursday', '886', 'Tuy', 'TUY3722', 'Hdg', 57, 92, 'JEE Mains', '', '05:00:00', '00:00:00', '2017-12-28', '2017-12-30', '', '1113', 'Chemistry', 38, '3.00', 0, ''),
(18, 'Friday,Saturday', '2', 'class 11', 'CLAS2381', 'nutrition', 61, 21, 'CLASS XII(MEDICAL)', 'open', '00:00:00', '02:00:00', '2018-01-01', '2018-01-31', '', '409', 'CHEMISTRY', 55, '5000.00', 0, ''),
(19, 'Saturday,Friday', '1', 'Bat H', 'BATH8705', 'olo', 60, 1, 'CLASS I', '', '00:00:00', '16:00:00', '2017-12-29', '2018-02-22', '', '18', 'ENGLISH ', 2, '100.00', 0, ''),
(20, 'Saturday,Friday,Thursday', '878', 'Hhd', 'HHD2035', 'Hhd', 60, 1, 'CLASS I', '', '09:00:00', '15:00:00', '2017-12-30', '2018-02-22', '', '19', ' MATHEMATICS', 55, '100.00', 0, ''),
(21, 'Saturday,Friday,Wednesday,Thursday', '10', 'Hdhd', 'HDHD3056', 'Bdhd', 60, 1, 'CLASS I', '', '15:00:00', '21:00:00', '2017-12-29', '2018-01-26', '', '1810', 'EVS', 10, '100.00', 0, ''),
(22, 'Monday,Tuesday,Wednesday,Friday,Thursday,Saturday', '2', 'Alokttt ', 'ALOK3450', 'Algebra', 7, 169, 'Gate CS', '', '08:00:00', '10:00:00', '2017-12-25', '2018-02-01', '', '1346', 'Engineering Mathematics', 50, '2500.00', 0, ''),
(24, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1', '6th Class', '6THC1614', 'Maths', 69, 6, 'CLASS VI', '', '17:00:00', '19:00:00', '2018-01-05', '2018-01-10', '', '322', 'MATHEMATICS', 20, '1000.00', 0, ''),
(25, 'Tuesday,Thursday,Saturday', '1', '10th', '10TH1900', 'Gk', 69, 10, 'CLASS X', '', '17:00:00', '19:00:00', '2017-12-28', '2018-01-15', '', '386', 'GENERAL KNOWLEDGE', 20, '1000.00', 0, ''),
(26, 'Saturday,Thursday', '997', 'Gdh', 'GDH1894', 'Bdbdb', 57, 16, 'NETWORKING', '', '04:00:00', '07:00:00', '2018-01-07', '2018-01-22', '', '490', 'CCNA IT COURSES-NIELIT O-LEVEL COURSES', 95956, '68.00', 0, ''),
(27, 'Saturday,Friday,Thursday', '5', 'Hdhd', 'HDHD4700', 'Dhg', 68, 1, 'CLASSES I-V', '', '05:00:00', '05:00:00', '2018-01-25', '2018-02-23', '', '1880', 'CLASS V', 884, '87.00', 0, ''),
(31, 'Friday,Thursday', '6', 'Gdhd', 'GDHD3864', 'Jrej', 75, 7, 'CLASS VII', 'open', '04:00:00', '00:00:00', '2018-01-05', '0000-00-00', '6 months', '340', 'CIVICS', 5665, '65Complete Course', 0, ''),
(33, 'Monday,Wednesday,Friday', '2', '1st', '1ST2880', 'genetix', 80, 1, 'CLASSES I-V', 'open', '17:00:00', '18:00:00', '2018-01-03', '0000-00-00', '2 months', '1881', 'ALL CLASSES', 50, '800 per Month', 0, ''),
(35, 'Monday,Sunday,Tuesday,Wednesday,Thursday', '4', 'Jee', 'JEE4514', '', 61, 92, 'JEE Mains', 'open', '06:30:00', '06:55:00', '2018-01-12', '0000-00-00', '4 months', '1115', 'Mathematics', 25, '25000 Complete Course', 0, ''),
(36, 'Tuesday,Thursday,Saturday', '1', '10th Class', '10TH2004', 'Biology', 80, 10, 'CLASS X', 'open', '19:00:00', '20:00:00', '2018-01-02', '0000-00-00', '2 months', '376', 'SCIENCE', 20, '1000 per Month', 0, ''),
(37, 'Monday,Sunday,Tuesday', '4', 'English', 'ENGL6820', '', 61, 8, 'CLASS VIII', 'close', '04:00:00', '06:00:00', '2018-01-15', '0000-00-00', '4 months', '349', 'ENGLISH', 25, '2000 per Month', 0, ''),
(42, 'Sunday,Saturday,Friday,Thursday,Wednesday,Tuesday,Monday', '1', 'Photoshop', 'PHOT2118', 'Photoshop', 39, 15, 'GRAPHIC/ANIMATION', 'open', '0:0:00', '1:0:00', '2018-01-15', '', '1 months', '72', 'GRAPHIC DESIGINING', 5, '500 per Month', 0, ''),
(43, 'Saturday,Friday,Thursday', '7', 'Vitteee', 'VITT9774', '', 79, 117, 'VITEEE', 'open', '9:0:00', '16:0:00', '2012-08-09', '', '9 months', '1450,1451,1452', 'Physics,Chemistry,Mathematics', 10, '500 per Month', 0, ''),
(44, 'Saturday,Friday,Thursday', '6', '', '6298', '', 79, 117, 'VITEEE', 'open', '15:0:00', '9:0:00', '2018-01-18', '', '8 months', '1450', 'Physics', 5, '55454454 per Month', 0, ''),
(45, 'Saturday,Friday,Thursday,Wednesday,Sunday,Monday,Tuesday', '6', 'Hshs', 'HSHS8773', 'Greeko', 79, 117, 'VITEEE', 'open', '10:0:00', '15:0:00', '2011-08-08', '', '24 months', '1450,1451,1452', 'Physics,Chemistry,Mathematics', 10, '64644664 Complete Course', 0, ''),
(46, 'Saturday,Sunday', '7', 'Gate', 'GATE3717', '', 90, 8, 'CLASS VIII', 'open', '14:49 ', ' 10:49', '2011-2-7', '', '8 months', '353,354,355', 'CIVICS,FRENCH,GERMAN', 5, '500 per Month', 0, ''),
(47, 'Saturday,Friday,Thursday', '7', '', '5249', 'parralel lines', 90, 8, 'CLASS VIII', 'close', '5:0:00', '21:0:00', '2018-01-19', '', '9 months', '360,359,358,355,354,348,350,351,352,353,349,356,357', 'GENERAL KNOWLEDGE,COMPUTER,HINDI,GERMAN,FRENCH,MATHEMATICS,SCIENCE,HISTORY,GEOGRAPHY,CIVICS,ENGLISH,SANSKRIT,JAPANESE', 5, '100 Complete Course', 0, ''),
(49, 'Saturday,Friday', '7', '', '4956', 'Jdhd', 90, 24, 'PERSONALITY DEVELOPMENT', 'open', '6:0:00', '9:0:00', '2018-01-19', '', '7 months', '913', 'PERSONALITY DEVELOPMENT', 545, '100 Complete Course', 0, ''),
(51, 'Saturday,Friday,Thursday', '6', 'Ixyug', 'IXYU8602', '', 75, 7, 'CLASS VII', 'open', '1:06 PM ', ' 1:09 PM', '2018-1-24', '', '6 months', '343,341', 'SANSKRIT,FRENCH', 566, '698 Complete Course', 2, ''),
(52, 'Saturday,Friday', '6', 'Ie8its', 'IE8I4392', 'Hfcjjc', 75, 7, 'CLASS VII', 'open', '5:00 AM', '6:00 AM', '2018-01-19', '', '6 months', '344,343,342,341', 'JAPANESE,SANSKRIT,GERMAN,FRENCH', 69890, '6868 Complete Course', 0, ''),
(53, 'Saturday,Sunday', '7', 'Jee', 'JEE2135', '', 97, 94, 'JEE Advanced', 'open', '3:00 PM', '5:00 PM', '2018-05-25', '', '18 months', '1121,1120,1119', 'Mathematics,Chemistry,Physics', 5, '1500 per Month', 2, ''),
(54, 'Saturday,Friday', '7', 'Hddh', 'HDDH1102', '', 97, 94, 'JEE Advanced', 'open', '4:00 AM', '10:00 PM', '2018-01-20', '', '9 months', '1121,1120', 'Mathematics,Chemistry', 5, '100 Complete Course', 0, ''),
(55, 'Monday,Tuesday,Wednesday,Thursday,Friday', '7', 'Kilo', 'KILO8945', 'Maths', 97, 94, 'JEE Advanced', 'open', '9:00 AM', '9:00 PM', '2017-09-15', '', '24 months', '1121,1120,1119', 'Mathematics,Chemistry,Physics', 100, '12000 Complete Course', 0, ''),
(56, 'Monday,Wednesday,Friday', '3', 'A1', 'A17637', '', 99, 12, 'CLASS XII', 'open', '0:00 PM', '5:30 PM', '2018-01-22', '', '6 months', '407', 'MATHEMATICS', 30, '1000 per Month', 0, ''),
(57, 'Tuesday,Thursday,Saturday', '3', '2', '28628', '', 99, 8, 'CLASS VIII', 'open', '6:30 AM', '7:30 AM', '2018-01-22', '', '3 months', '350,349,348', 'SCIENCE,ENGLISH,MATHEMATICS', 20, '1000 per Month', 0, ''),
(58, 'Monday,Wednesday,Friday', '2', '9 Th Batch', '9THB2807', 'Revision', 103, 9, 'CLASS IX', 'open', '4:00 pm', '5:00 pm', '2017-09-14', '', '8 months', '361', 'MATHEMATICS', 20, '500 per Month', 0, ''),
(59, 'Monday,Wednesday,Friday', '1', 'Math101', 'MATH3907', 'Algebra', 74, 8, 'CLASS VIII', 'open', '4:00 AM', '6:00 AM', '2018-01-20', '', '6 months', '348', 'MATHEMATICS', 50, '500 per Month', 0, ''),
(61, 'Saturday,Friday', '7', 'Hehd', 'HEHD8637', '', 74, 8, 'CLASS VIII', 'open', '9:00 AM', '3:00 PM', '2018-01-24', '', '10 months', '350', 'SCIENCE', 5, '44 Complete Course', 0, ''),
(62, 'Saturday,Friday,Thursday', '7', 'Jdjd', 'JDJD1124', 'Hdheh', 74, 8, 'CLASS VIII', 'open', '9:00 AM', '11:00 PM', '2013-01-19', '', '10 months', '348,350', 'MATHEMATICS,SCIENCE', 65, '55 Complete Course', 0, ''),
(64, 'Saturday,Wednesday,Friday,Thursday', '7', 'Helloo', 'HELL2668', 'Bhio', 74, 8, 'CLASS VIII', 'open', '10:00 AM', '1:00 PM', '2018-01-24', '', '24 months', '348,350', 'MATHEMATICS,SCIENCE', 4, '100 per Month', 0, ''),
(65, 'Sunday,Monday,Tuesday', '2', 'Robotics', 'ROBO7534', '', 117, 218, 'Computer Science  Engineering', 'open', '6:00 a.m.', '7:00 a.m.', '2018-01-28', '', '2 months', '1639', '	Engineering Graphics', 25, '500 per Month', 0, ''),
(66, 'Saturday,Friday,Thursday,Wednesday,Monday,Sunday,Tuesday', '7', 'Hhdhhd', 'HHDH2314', 'Jndj', 107, 93, 'Mathematical Hons.', 'open', '9:00 AM', '3:00 PM', '2018-01-25', '', '9 months', '1092,1091,1090,1087,1088,1089', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Integral Calculus And Trigonometry,Differntial Equations ,Abstract Algebra', 10, '10000 Complete Course', 0, ''),
(68, 'Saturday,Thursday,Tuesday', '7', '', '4277', 'Hhddd', 107, 93, 'Mathematical Hons.', 'open', '10:00 AM', '2:00 PM', '2018-01-25', '', '8 months', '1092,1091', 'Vector Analysis,Differntial Geometry', 10, '963 per Month', 0, ''),
(69, 'Saturday,Friday,Thursday,Monday,Sunday,Tuesday,Wednesday', '7', 'Jedj', 'JEDJ3891', 'Hdhd', 107, 93, 'Mathematical Hons.', 'open', '9:00 AM', '2:00 PM', '2018-01-25', '', '9 months', '1091', 'Differntial Geometry', 5, '100 Complete Course', 0, ''),
(72, 'Saturday', '4', 'Mathince2', 'MATH4254', 'Hshs ', 74, 8, 'CLASS VIII', 'open', '0:00 AM', '4:00 AM', '2018-01-25', '', '8 months', '350,348', 'SCIENCE,MATHEMATICS', 2, '52 per Month', 0, ''),
(73, 'Saturday,Tuesday,Monday,Sunday,Wednesday,Thursday,Friday', '7', 'Jdjd', 'JDJD4278', 'Hehhd', 74, 8, 'CLASS VIII', 'open', '10:00 AM', '2:00 PM', '2018-01-25', '', '9 months', '350,348', 'SCIENCE,MATHEMATICS', 5, '555 Complete Course', 0, ''),
(74, 'Thursday,Saturday', '7', 'Hhdhd', 'HHDH2235', 'Hhd', 107, 93, 'Mathematical Hons.', 'close', '10:00 AM', '3:00 PM', '2018-01-25', '', '9 months', '1092,1091,1089,1087', 'Vector Analysis,Differntial Geometry,Abstract Algebra,Integral Calculus And Trigonometry', 50, '1000 Complete Course', 0, ''),
(76, 'Thursday', '7', 'Bhd', 'BHD4280', 'Vencroe', 107, 93, 'Mathematical Hons.', 'open', '0:00 AM', '3:00 PM', '2018-01-29', '', '6 months', '1092,1091,1089,1090', 'Vector Analysis,Differntial Geometry,Abstract Algebra,Numerical Analysis ', 10, '100 Complete Course', 0, ''),
(77, 'Saturday,Friday', '7', 'Hhd', 'HHD5638', 'Hhd', 107, 93, 'Mathematical Hons.', 'open', '0:00 AM', '2:00 PM', '2018-01-29', '', '7 months', '1092,1091', 'Vector Analysis,Differntial Geometry', 39, '100 per Month', 0, ''),
(78, 'Saturday,Friday,Thursday,Wednesday,Monday,Sunday,Tuesday', '7', 'Hhdh', 'HHDH2960', 'Hdhdhd', 107, 93, 'Mathematical Hons.', 'open', '10:00 AM', '11:00 AM', '2018-01-29', '', '9 months', '1092,1091,1089,1087,1088,1090', 'Vector Analysis,Differntial Geometry,Abstract Algebra,Integral Calculus And Trigonometry,Differntial Equations ,Numerical Analysis ', 100, '100 per Month', 0, ''),
(79, 'Saturday,Friday', '7', 'Hdhdh', 'HDHD2396', 'Hhd', 107, 93, 'Mathematical Hons.', 'open', '3:00 PM', '9:00 PM', '2018-01-29', '', '8 months', '1092,1091,1090,1089,1087', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Abstract Algebra,Integral Calculus And Trigonometry', 10, '500 per Month', 0, ''),
(80, 'Saturday,Friday,Thursday,Wednesday,Tuesday,Monday,Sunday', '7', 'Alom Kusheaha', 'ALOM6131', '', 107, 93, 'Mathematical Hons.', 'open', '5:00 AM', '9:00 PM', '2018-01-29', '', '9 months', '1092,1091,1090,1089', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Abstract Algebra', 60, '500 per Month', 0, ''),
(81, 'Saturday,Monday,Sunday', '7', 'Ghhhhd', 'GHHH1984', 'Hddh', 107, 93, 'Mathematical Hons.', 'open', '9:00 AM', '11:00 AM', '2018-01-29', '', '9 months', '1092,1091,1090,1087,1089,1088', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Integral Calculus And Trigonometry,Abstract Algebra,Differntial Equations ', 10, '100 Complete Course', 0, ''),
(82, 'Saturday,Friday,Thursday', '7', 'Hdhhd', 'HDHH3765', '', 107, 93, 'Mathematical Hons.', 'open', '5:00 AM', '9:00 AM', '2018-01-29', '', '9 months', '1092,1091,1087', 'Vector Analysis,Differntial Geometry,Integral Calculus And Trigonometry', 100, '100 Complete Course', 0, ''),
(83, 'Thursday,Tuesday,Saturday,Friday,Wednesday,Monday,Sunday', '7', 'Hdhhd', 'HDHH2192', '', 107, 93, 'Mathematical Hons.', 'open', '3:00 PM', '4:00 PM', '2018-01-29', '', '9 months', '1092,1091,1090,1089,1087,1088', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Abstract Algebra,Integral Calculus And Trigonometry,Differntial Equations ', 2147483647, '10000000 Complete Course', 0, ''),
(84, 'Saturday,Friday,Tuesday,Monday,Sunday,Wednesday,Thursday', '7', 'Hhh', 'HHH8066', '', 107, 93, 'Mathematical Hons.', 'open', '3:00 PM', '5:00 PM', '2018-01-29', '', '24 months', '1092,1091,1089,1087,1088,1090', 'Vector Analysis,Differntial Geometry,Abstract Algebra,Integral Calculus And Trigonometry,Differntial Equations ,Numerical Analysis ', 10, '100 Complete Course', 0, ''),
(85, 'Saturday', '7', 'Ndndbdhd', 'NDND3972', 'Qwerty', 107, 93, 'Mathematical Hons.', 'open', '10:00 AM', '11:00 AM', '2018-01-30', '', '24 months', '1092,1091,1090,1089,1087,1088', 'Vector Analysis,Differntial Geometry,Numerical Analysis ,Abstract Algebra,Integral Calculus And Trigonometry,Differntial Equations ', 5, '100 Complete Course', 0, ''),
(86, 'Saturday,Friday,Thursday,Wednesday', '7', 'Hlooo', 'HLOO1332', '', 136, 168, 'PUTHAT', 'open', '9:00 am', '10:00 am', '2018-01-30', '', '24 months', '1343,1342,1344', 'General Awareness,English Aptitude,Reasoning ', 5, '200 per Month', 0, ''),
(87, 'Saturday,Friday,Thursday,Wednesday,Tuesday,Monday,Sunday', '7', 'Dhdhsaowoeiejejjejndjdjjdkskksjjdjdjddksssskksjs', 'DHDH7706', 'jdhdhd', 136, 168, 'PUTHAT', 'open', '04:00 PM', '05:00 PM', '2012-08-24', '', '24 months', '1343,1342,1345', 'General Awareness,English Aptitude,Hospitality Industry Awareness', 50, '500 per Month', 0, ''),
(88, 'Saturday,Friday,Thursday', '3', 'Feb', 'FEB6405', 'Th', 75, 7, 'CLASS VII', 'open', '12:00 AM', '01:00 PM', '2018-02-02', '', '6 months', '344', 'JAPANESE', 90, '780 per Month', 0, ''),
(90, 'Monday,Wednesday,Tuesday,Thursday,Friday,Saturday', '2', '9a', '9A5701', 'Solve Sample Paper ', 155, 9, 'CLASS IX', 'close', '5:00 PM', '8:00 PM', '2018-02-05', '', '1 months', '361,363,362', 'MATHEMATICS,SCIENCE,ENGLISH', 20, '2000 per Month', 0, ''),
(91, 'Tuesday,Thursday,Saturday', '2', '3-4 ', '3-46387', '', 161, 9, 'CLASS IX', 'open', '08:00 a.m.', '08:00 p.m.', '2018-02-05', '', '6 months', '361', 'MATHEMATICS', 24, '600 per Month', 0, '05-02-2018 15:38:38'),
(92, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Learn With Expert', 'LEAR9009', 'Grammer', 162, 9, 'CLASS IX', 'open', '09:00 am', '06:00 pm', '2018-02-05', '', '1 months', '362', 'ENGLISH', 20, '600 per Month', 0, '05-02-2018 16:40:10'),
(93, 'Monday,Wednesday,Friday', '3', 'Maths Special', 'MATH7440', '', 163, 11, 'CLASS XI', 'open', '04:00 PM', '05:00 PM', '2018-04-01', '', '6 months', '47', 'MATHEMATICS', 15, '500 per Month', 2, '06-02-2018 14:14:20'),
(94, 'Monday,Wednesday,Friday', '3', 'Maths Special', 'MATH9779', '', 163, 12, 'CLASS XII', 'open', '05:00 PM', '06:00 PM', '2018-4-2', '', '6 months', '407', 'MATHEMATICS', 15, '500 per Month', 2, '06-02-2018 14:17:12'),
(95, 'Saturday,Friday,Thursday,Wednesday,Tuesday,Sunday,Monday', '3', 'Maths Classes', 'MATH4742', 'Straight Line', 164, 1, 'CLASSES I-V', 'open', '04:00 AM', '05:00 AM', '2018-02-06', '', '23 months', '1877,1876', 'CLASS II,CLASS I', 5, '500 per Month', 0, '06-02-2018 14:54:20'),
(96, 'Saturday,Friday', '5', 'Maths Classes', 'MATH5065', '', 164, 1, 'CLASSES I-V', 'open', '05:00 PM', '06:00 PM', '2019-04-06', '', '7 months', '1877,1876', 'CLASS II,CLASS I', 5, '150 per Month', 2, '06-02-2018 14:55:35'),
(98, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday', '2', 'Maths Ssc And 9th - I2th', 'MATH6072', '', 166, 9, 'CLASS IX', 'open', '03:00 pm', '04:00 pm', '2018-03-02', '', '6 months', '361', 'MATHEMATICS', 15, '400 per Month', 0, '06-02-2018 15:52:04'),
(99, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '3', 'Maths Special', 'MATH3983', 'Menstruation', 167, 9, 'CLASS IX', 'open', '04:00 pm', '06:00 pm', '2018-02-06', '', '6 months', '361,362,363', 'MATHEMATICS,ENGLISH,SCIENCE', 15, '500 per Month', 0, '06-02-2018 16:16:18'),
(100, 'Saturday,Friday,Thursday,Wednesday,Tuesday,Monday,Sunday', '7', 'Olooo', 'OLOO3036', '', 136, 168, 'PUTHAT', 'open', '05:00 AM', '07:00 AM', '2018-02-07', '', '24 months', '1342,1343', 'English Aptitude,General Awareness', 5, '200 per Month', 0, '07-02-2018 11:10:38'),
(101, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Maths Special', 'MATH2042', 'Trignometry', 80, 10, 'CLASS X', 'open', '03:00 PM', '06:00 PM', '2018-02-07', '', '3 months', '374', 'MATHEMATICS', 20, '500 per Month', 0, '07-02-2018 14:51:22'),
(102, 'Monday,Wednesday,Friday', '2', 'Maths Special', 'MATH1156', 'Geometry', 174, 10, 'CLASS X', 'open', '06:00 PM', '09:00 PM', '2018-02-07', '', '6 months', '375,374', 'ENGLISH,MATHEMATICS', 20, '500 per Month', 0, '07-02-2018 15:22:41'),
(103, 'Tuesday,Thursday,Saturday', '2', 'Chemistry ', 'CHEM3404', 'Revision ', 180, 11, 'CLASS XI', 'open', '05:00 PM', '06:00 PM', '2017-06-01', '', '10 months', '1843', 'CHEMISTRY', 20, '9000 Complete Course', 0, '08-02-2018 16:51:59'),
(104, 'Saturday,Friday', '7', 'Heloo', 'HELO6056', 'Hshsh', 136, 168, 'PUTHAT', 'open', '04:00 AM', '05:00 AM', '2018-02-08', '', '24 months', '1342,1343', 'English Aptitude,General Awareness', 150, '150 per Month', 0, '08-02-2018 16:54:11'),
(105, 'Monday,Wednesday,Friday', '2', 'Chemistry Special For Neet', 'CHEM5574', 'Revision ', 180, 12, 'CLASS XII', 'open', '06:01 PM', '08:00 PM', '2017-07-08', '', '9 months', '1859', 'CHEMISTRY', 25, '16000 Complete Course', 0, '08-02-2018 16:54:42'),
(106, 'Sunday,Monday,Tuesday,Wednesday', '5', 'Class 11 ', 'CLAS4646', '', 183, 11, 'CLASS XI', 'close', '06:30 a.m.', '07:30 a.m.', '2018-02-13', '', '5 months', '47', 'MATHEMATICS', 25, '1500 per Month', 0, '08-02-2018 17:08:20'),
(107, 'Friday,Thursday,Wednesday', '2', 'class 9', 'CLAS4769', '', 183, 9, 'CLASS IX', 'open', '06:00 a.m.', '08:00 a.m.', '2018-02-15', '', '3 months', '361', 'MATHEMATICS', 25, '1500 per Month', 0, '08-02-2018 17:09:27'),
(108, 'Wednesday', '5', 'Mathematics', '', 'ddd', 177, 0, 'CLASSES I-V', '', '2:00 AM', '10:00 PM', '08-02-2018', '', '1 months', '', 'HISTORY', 4, 'Per Month', 0, ''),
(109, 'Sunday,Monday,Wednesday', '5', 'Mathematics dd', '', 'dsg', 177, 0, 'AILET', '', '2:00 AM', '10:00 PM', '08-02-2018', '', '3 months', '', 'CLASS V,CLASS IV,HISTORY', 55, 'Per Month', 0, ''),
(110, 'Monday,Tuesday,Wednesday', '2', 'Super 30', 'SUPE9077', 'Reproduction', 183, 9, 'CLASS IX', 'open', '04:00 a.m.', '06:00 a.m.', '2018-02-06', '', '6 months', '363', 'SCIENCE', 30, '2000 per Month', 0, '08-02-2018 17:45:30'),
(111, 'Sunday,Tuesday,Thursday', '3', 'new batch', '', 'ddd', 136, 0, 'PUTHAT', '', '2:00 AM', '10:00 PM', '08-02-2018', '', '1 months', '', 'General Awareness,English Aptitude', 4, 'Per Month', 0, ''),
(112, 'Saturday,Friday,Thursday', '7', 'What To Do', 'WHAT2444', 'Hshdhd', 136, 12, 'CLASS XII', 'open', '05:00 AM', '06:00 AM', '2018-02-08', '', '24 months', '407,406,1875,65,1873,1858,408,64,1859,1861,1862,1863,1864,1865,1866,1867,1868,1870,1871,1872,1869', 'MATHEMATICS,ENGLISH,INFORMATION TECHNOLOGY,BUSINESS STUDIES,ECONOMICS,PHYSICS,PHYSICAL EDUCATION,ACCOUNTANCY,CHEMISTRY,BIOLOGY,HINDI,HISTORY,GEOGRAPHY,POLITICAL SCIENCE,MUSIC,PSYCHOLOGY,SOCIOLOGY,SANSKRIT,ANTHROPOLOGY,ARCHAEOLOGY,FASHION STUDIES', 15, '1500 per Month', 0, '08-02-2018 18:12:41'),
(113, 'Saturday,Thursday,Friday,Wednesday,Tuesday,Monday,Sunday', '7', 'Vikkuuuu Aaka', 'VIKK1608', '', 136, 218, 'Computer Science  Engineering', 'open', '05:00 AM', '07:00 AM', '2018-02-09', '', '24 months', '1637,1639,1640,1641,1642,1643,1644,1638,1645,1647,1648,1646,1649,1651,1653,1654,1652,1655,1656,1657,1658,1659,1660,1661,1662,1664,1665,1666,1667,1663,1668,1669,1670,1671,1672,1673,1674', 'Mathematics,	Engineering Graphics,	Chemistry,	Computer Programming,	Elements of Electrical Engineering,	Basic Electronics,	Digital Systems,	Physics,	Object Oriented Programming,	Data Communication,Computer Organization,Applied Mathematics,Data Structures,', 5, '1555 per Month', 0, '09-02-2018 11:49:43'),
(114, 'Sunday,Monday,Wednesday,Tuesday', '1', 'G', 'G7094', 'Tb', 74, 10, 'CLASS X', 'open', '03:00 AM', '04:00 AM', '2018-02-06', '', '6 months', '377', 'HISTORY', 20, '200 per Month', 0, '09-02-2018 12:13:07'),
(116, 'Monday,Wednesday,Friday', '2', 'Maths Classes', 'MATH2149', '', 189, 9, 'CLASS IX', 'open', '05:00 PM', '06:00 PM', '2018-03-15', '', '12 months', '361', 'MATHEMATICS', 10, '600 per Month', 2, '09-02-2018 12:54:43'),
(117, 'Tuesday,Thursday,Saturday', '2', 'Science Classes', 'SCIE6517', '', 189, 9, 'CLASS IX', 'open', '05:00 PM', '06:00 PM', '2018-03-16', '', '12 months', '363', 'SCIENCE', 10, '600 per Month', 2, '09-02-2018 12:55:33'),
(118, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Political Science ', 'POLI7412', 'Chapter 2', 193, 12, 'CLASS XII', 'open', '03:00 PM', '04:00 PM', '2017-08-01', '', '5 months', '1865', 'POLITICAL SCIENCE', 12, '500 per Month', 0, '09-02-2018 15:42:30'),
(119, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Economics', 'ECON5128', 'Revision', 144, 12, 'CLASS XII', 'open', '05:00 PM', '06:00 PM', '2017-04-02', '', '12 months', '1873', 'ECONOMICS', 20, '800 per Month', 0, '09-02-2018 17:43:56'),
(120, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', '4 To 5:30', '4TO52798', '', 198, 1, 'CLASSES I-V', 'open', '04:00 PM', '05:30 AM', '2018-01-01', '', '', '1881', 'ALL CLASSES', 15, '500 per Month', 0, '09-02-2018 18:49:27'),
(121, 'Monday,Wednesday,Friday', '1', '9th class', '', '', 209, 0, 'CLASS IX', '', '4:00 PM', '6:00 PM', '01-04-2017', '', '12 months', '', 'MATHEMATICS', 10, 'Per Month', 0, ''),
(122, 'Tuesday,Thursday,Saturday', '1', '10th', '', '', 209, 0, 'CLASS X', '', '4:00 PM', '6:00 PM', '01-04-2017', '', '12 months', '', 'MATHEMATICS,SCIENCE', 10, 'Per Month', 0, ''),
(123, 'Monday,Wednesday,Tuesday', '2', 'International', 'INTE3380', '', 183, 11, 'CLASS XI', 'close', '06:00 a.m.', '07:00 a.m.', '2018-02-12', '', '2 months', '1843', 'CHEMISTRY', 200, '100 per Month', 0, '11-02-2018 14:51:56'),
(124, 'Sunday,Saturday,Friday,Thursday,Wednesday,Monday,Tuesday', '1', 'Fee Pay', 'FEEP3551', '', 39, 15, 'GRAPHIC/ANIMATION', 'open', '12:00 AM', '01:00 PM', '2018-02-13', '', '1 months', '72', 'GRAPHIC DESIGINING', 1, '1 Complete Course', 0, '12-02-2018 12:57:35'),
(125, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1', 'Maths Special ', 'MATH8376', 'Revision', 154, 9, 'CLASS IX', 'open', '03:00 PM', '04:00 PM', '2017-04-01', '', '12 months', '322', 'MATHEMATICS', 20, '12000 Complete Course', 0, '12-02-2018 13:58:12'),
(126, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '1', 'Maths Special ', 'MATH2794', 'Revision', 154, 12, 'CLASS XII', 'open', '04:00 PM', '05:00 PM', '2017-04-01', '', '12 months', '407', 'MATHEMATICS', 20, '12000 Complete Course', 0, '12-02-2018 14:00:03'),
(127, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'English Speaking', 'ENGL5532', 'English Speaking ', 152, 9, 'CLASS IX', 'open', '05:00 PM', '06:00 PM', '2017-11-12', '', '3 months', '362', 'ENGLISH', 15, '700 per Month', 0, '12-02-2018 16:40:12'),
(128, 'Monday,Wednesday,Friday', '2', 'CLASS-9', 'CLAS5906', 'Revision', 138, 9, 'CLASS IX', 'open', '04:00 am', '05:00 am', '2017-04-03', '', '12 months', '361,362,363,364,365,366,371', 'MATHEMATICS,ENGLISH,SCIENCE,HISTORY,GEOGRAPHY,CIVICS,HINDI', 12, '700 per Month', 0, '13-02-2018 15:35:24'),
(129, 'Monday,Wednesday,Friday', '2', 'Political Science RCI', 'POLI8810', '', 113, 11, 'CLASS XI', 'open', '05:00 pm', '06:00 pm', '2018-04-10', '', '6 months', '1849', 'POLITICAL SCIENCE', 30, '10000 Complete Course', 2, '13-02-2018 16:01:57'),
(130, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Chemistry Special ', 'CHEM5588', 'Revision', 225, 11, 'CLASS XI', 'open', '04:00 p.m.', '05:00 p.m.', '2017-01-01', '', '12 months', '363,1843', 'SCIENCE,CHEMISTRY', 20, '15000 Complete Course', 0, '13-02-2018 16:55:44'),
(131, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday', '2', 'Chemistry New Batch', 'CHEM5798', '', 225, 11, 'CLASS XI', 'open', '04:00 p.m.', '05:00 p.m.', '2018-03-23', '', '12 months', '1843', 'CHEMISTRY', 20, '15000 Complete Course', 2, '13-02-2018 16:57:44'),
(132, 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Chemistry New Batch ', 'CHEM4908', '', 225, 12, 'CLASS XII', 'open', '05:00 p.m.', '06:00 p.m.', '2018-03-23', '', '12 months', '1859', 'CHEMISTRY', 20, '15000 Complete Course', 2, '13-02-2018 16:58:47'),
(133, 'Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday', '2', 'Maths ', 'MATH8675', 'Circle', 228, 9, 'CLASS IX', 'open', '06:30 p.m.', '07:30 p.m.', '2017-04-01', '', '12 months', '361', 'MATHEMATICS', 20, '5000 Complete Course', 0, '13-02-2018 18:36:37'),
(134, 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '2', 'Science ', 'SCIE2649', 'Revision ', 228, 9, 'CLASS IX', 'open', '04:30 p.m.', '05:30 p.m.', '2017-04-01', '', '12 months', '363,362', 'SCIENCE,ENGLISH', 20, '5000 Complete Course', 0, '13-02-2018 18:38:41'),
(135, 'Monday,Wednesday,Friday,Sunday,Tuesday,Thursday,Saturday', '2', 'English Classes ', 'ENGL2065', 'Revision ', 228, 10, 'CLASS X', 'open', '05:30 p.m.', '06:30 p.m.', '2017-05-01', '', '12 months', '375', 'ENGLISH', 20, '5000 Complete Course', 0, '13-02-2018 18:40:45'),
(136, 'Monday,Wednesday,Friday', '2', 'Maths ', 'MATH1411', '', 228, 10, 'CLASS X', 'open', '04:00 p.m.', '05:00 p.m.', '2018-04-01', '', '12 months', '374,375,376', 'MATHEMATICS,ENGLISH,SCIENCE', 20, '5000 Complete Course', 2, '13-02-2018 18:42:26'),
(137, 'Monday,Wednesday,Friday', '2', 'English ', 'ENGL2910', '', 228, 10, 'CLASS X', 'open', '05:00 p.m.', '06:00 p.m.', '2018-04-01', '', '12 months', '375', 'ENGLISH', 20, '5000 Complete Course', 2, '13-02-2018 18:45:51'),
(138, 'Monday,Wednesday,Friday', '2', 'Maths 11th Class', 'MATH5964', 'Revision', 232, 11, 'CLASS XI', 'open', '04:30 p.m.', '05:29 p.m.', '2017-03-20', '', '12 months', '47', 'MATHEMATICS', 20, '7000 Complete Course', 0, '13-02-2018 19:50:05'),
(140, 'Friday,Saturday', '1', 'A.Math-ll,A.physics,A.mechanics,F.M.,S.O.M.', 'A.MA1548', 'Diploma Course', 229, 220, 'Mechanical Engineering', 'open', '11:00 AM', '03:00 PM', '2018-01-27', '', '4 months', '1581', 'Mathematics', 10, '5000 Complete Course', 0, '13-02-2018 20:03:31'),
(141, 'Monday,Tuesday,Wednesday,Thursday,Friday', '2', 'Computer Classes', 'COMP2021', '', 237, 14, 'APPLICATION SOFTWARE', 'open', '01:00 pm', '02:00 pm', '2018-03-20', '', '3 months', '15,476,477,486,473,459', 'COMPUTER BASICS,MS OFFICE,MS EXCEL,MS POWER POINT,INTERNET,DATA ENTRY', 5, '2000 per Month', 2, '14-02-2018 14:26:53'),
(142, 'Monday,Tuesday,Wednesday,Thursday,Friday', '3', 'Accounts ', 'ACCO1089', '', 238, 11, 'CLASS XI', 'open', '06:00 pm', '07:00 pm', '2018-04-04', '', '9 months', '43', 'ACCOUNTANCY', 24, '1000 per Month', 2, '14-02-2018 15:05:48'),
(143, 'Sunday,Tuesday,Thursday,Saturday', '1', 'Ggf', 'GGF8059', 'bgc', 241, 13, 'PROGRAMMING LANGUAGE', 'open', '09:00 AM', '03:00 PM', '2018-02-12', '', '6 months', '13', 'C++', 20, '2000 Complete Course', 0, '15-02-2018 17:26:01'),
(144, 'Sunday,Saturday', '3', 'Y667', 'Y6679540', '', 241, 13, 'PROGRAMMING LANGUAGE', 'close', '03:00 AM', '04:00 PM', '2018-02-17', '', '2 months', '14', 'ASP.NET', 30, '500 per Month', 0, '15-02-2018 17:57:28'),
(145, 'Saturday,Friday', '4', 'Hdhd', 'HDHD9540', '', 75, 7, 'CLASS VII', 'open', '04:19 PM ', ' 05:20 PM', '2018-02-21', '', '5 months', '31,347', 'MATHEMATICS,GENERAL KNOWLEDGE', 32, '2 per Month', 0, '21-02-2018 16:17:01'),
(146, 'Saturday,Tuesday,Sunday', '3', 'H X', 'HX8768', 'Hdjdk', 75, 137, 'IMU-CET', 'open', '04:00 PM', '06:00 PM', '2018-02-21', '', '6 months', '1217,1218', 'Maths,English', 9898, '9898 per Month', 0, '21-02-2018 16:20:39'),
(147, 'Saturday,Thursday', '6', 'Gdhd', 'GDHD9556', '', 75, 137, 'IMU-CET', 'open', '04:00 AM', '05:00 AM', '2018-02-21', '', '6 months', '1217,1218', 'Maths,English', 3, '2 per Month', 0, '21-02-2018 16:54:23'),
(148, 'Friday,Saturday,Tuesday', '7', 'Ye', 'YE3340', '', 75, 137, 'IMU-CET', 'open', '03:00 AM', '04:00 AM', '2018-02-25', '', '6 months', '1220,1219', 'Aptitude ,General Knowledge', 65, '65 per Month', 0, '21-02-2018 16:54:57'),
(155, 'Wednesday', '4', 'Testing', 'TEST4973', '', 75, 7, 'CLASS VII', 'open', '04:00 PM', '05:00 PM', '2018-02-22', '', '1 months', '345', 'HINDI', 23, '20 per Month', 0, '21-02-2018 18:59:20'),
(156, 'Friday,Thursday', '6', 'Bajw', 'BAJW5886', '', 241, 13, 'PROGRAMMING LANGUAGE', 'open', '12:00 AM', '12:05 AM', '2018-02-23', '', '3 months', '13', 'C++', 31, '1 Complete Course', 0, '23-02-2018 18:34:34'),
(157, 'Saturday,Friday', '5', 'Dgy', 'DGY3826', '', 241, 13, 'PROGRAMMING LANGUAGE', 'open', '12:03 AM', '12:08 AM', '2018-02-25', '', '5 months', '13', 'C++', 53, '2 Complete Course', 0, '23-02-2018 18:54:45'),
(158, 'Thursday,Wednesday', '6', 'gsus', 'GSUS1804', '', 241, 13, 'PROGRAMMING LANGUAGE', 'open', '12:01 AM', '12:05 AM', '2018-02-25', '', '7 months', '13', 'C++', 42, '61 per Month', 0, '24-02-2018 11:21:53'),
(159, 'Saturday,Thursday,Wednesday', '5', 'Hdhhd', 'HDHH2313', 'Hdhd', 75, 7, 'CLASS VII', 'open', '12:00 AM', '02:00 AM', '2018-03-06', '', '6 months', '343', 'SANSKRIT', 6595, '65989 ', 1, '07-03-2018 17:18:03'),
(160, 'Thursday,Tuesday', '5', 'Gdx', 'GDX7984', 'Idis', 75, 7, 'CLASS VII', 'open', '12:00 AM', '01:00 AM', '2018-03-07', '', '6 months', '31', 'MATHEMATICS', 56565, '989 per Month', 1, '07-03-2018 17:39:14'),
(162, 'Thursday,Friday', '5', 'Bske', 'BSKE1645', 'Hs', 241, 28, 'NEGOTIATION', 'open', '12:02 AM', '03:11 AM', '2018-03-07', '', '6 months', '917', 'NEGOTIATION', 31, '64 per Month', 1, '09-03-2018 15:43:54'),
(163, 'Sunday,Monday,Tuesday,Wednesday,Thursday,Saturday,Friday', '1', 'Chat Test', 'CHAT6473', 'Sjjs', 74, 10, 'CLASS X', 'open', '12:00 AM', '02:00 AM', '2018-03-10', '', '5 months', '377', 'HISTORY', 10, '2500 per Month', 1, '10-03-2018 13:11:23'),
(164, 'Thursday,Friday', '4', 'Bnk', 'BNK2207', '', 241, 97, 'SRMJEEE', 'open', '12:00 AM', '12:01 AM', '2018-03-14', '', '5 months', '1133', 'Chemistry', 23, '3 per Month', 2, '14-03-2018 16:23:17'),
(165, 'Wednesday,Tuesday', '6', 'Yee', 'YEE3476', 'Hswd', 241, 94, 'JEE Advanced', 'open', '12:00 AM', '04:09 AM', '2018-03-14', '', '1 months', '1121', 'Mathematics', 9464, '1 per Month', 1, '14-03-2018 16:36:40'),
(166, 'Friday,Saturday', '5', 'Hinku', 'HINK4710', 'Hiokloy', 241, 97, 'SRMJEEE', 'open', '12:00 AM', '01:00 AM', '2018-02-14', '', '1 months', '1132', 'Physics', 13, '0 per Month', 1, '15-03-2018 18:02:21'),
(167, 'Tuesday,Wednesday', '4', 'Nsje', 'NSJE7208', 'Jw', 241, 10, 'CLASS X', 'open', '12:00 AM', '01:00 AM', '2018-02-16', '', '3 months', '375', 'ENGLISH', 31, '0 per Month', 1, '16-03-2018 14:36:21'),
(168, 'Friday,Thursday', '4', 'Mm', 'MM3697', 'Quw', 249, 14, 'APPLICATION SOFTWARE', 'open', '12:00 AM', '02:00 AM', '2018-02-16', '', '6 months', '16,15', 'LINUX,COMPUTER BASICS', 13, '1 per Month', 1, '16-03-2018 15:10:14'),
(169, 'Friday,Tuesday,Wednesday,Saturday', '6', 'Bwuw', 'BWUW3976', '', 249, 14, 'APPLICATION SOFTWARE', 'open', '12:00 PM', '02:00 PM', '2018-03-20', '', '6 months', '455,16', 'INFORMATION PRACTICE FOR CLASS XII,LINUX', 31, '1 per Month', 2, '16-03-2018 15:11:10'),
(170, 'Friday,Thursday,Saturday', '4', 'Gh', 'GH2528', 'Yy', 253, 1, 'CLASSES I-V', 'open', '12:00 PM', '01:00 PM', '2018-04-04', '', '4 months', '1880', 'CLASS V', 89, '2 per Month', 1, '04-04-2018 18:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `batch_join`
--

CREATE TABLE `batch_join` (
  `id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `batch_query`
--

CREATE TABLE `batch_query` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booked_activity_rel`
--

CREATE TABLE `booked_activity_rel` (
  `id` int(11) NOT NULL,
  `activity_id` varchar(255) NOT NULL,
  `academy_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_phone` varchar(255) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `activity_name` varchar(255) NOT NULL,
  `package_plan` varchar(255) NOT NULL,
  `plan_id` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booked_activity_rel`
--

INSERT INTO `booked_activity_rel` (`id`, `activity_id`, `academy_id`, `student_id`, `student_name`, `student_phone`, `student_email`, `activity_name`, `package_plan`, `plan_id`, `status`, `created_date`) VALUES
(1, '3', '244', '42', 'deeksha', '9004988285', 'deeksha237@gmail.com', 'Cricket', '1', '41', '0', '20-02-2018 14:53:37'),
(12, '14', '251', '25', 'vikas', '9990731159', 'vckvikash702@gmail.com', '', '1', '91', '0', '04-04-2018 14:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarkteacher`
--

CREATE TABLE `bookmarkteacher` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `isbookmark` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarkteacher`
--

INSERT INTO `bookmarkteacher` (`id`, `student_id`, `teacher_id`, `isbookmark`, `created`) VALUES
(1, 1, 1, 1, '2017-12-20 07:11:10'),
(2, 1, 2, 1, '2017-12-20 09:17:43'),
(3, 18, 2, 1, '2017-12-20 12:34:04'),
(4, 18, 1, 1, '2017-12-20 12:34:05'),
(5, 22, 5, 1, '2017-12-21 06:37:13'),
(6, 22, 2, 1, '2017-12-21 06:53:15'),
(7, 1, 9, 0, '2018-01-09 09:24:38'),
(8, 1, 18, 1, '2017-12-22 10:48:43'),
(9, 22, 9, 1, '2017-12-22 13:26:08'),
(10, 6, 26, 1, '2017-12-23 09:46:06'),
(11, 0, 28, 0, '2018-02-05 12:24:43'),
(12, 30, 2, 1, '2017-12-24 03:48:09'),
(13, 6, 30, 1, '2017-12-24 09:17:11'),
(14, 22, 30, 1, '2017-12-27 13:34:40'),
(15, 22, 26, 1, '2017-12-28 07:12:11'),
(16, 22, 60, 1, '2018-01-01 06:52:12'),
(17, 1, 30, 1, '2018-01-01 09:04:18'),
(18, 1, 54, 1, '2018-01-01 09:06:44'),
(19, 1, 60, 1, '2018-01-01 09:06:45'),
(20, 4, 57, 1, '2018-01-03 11:52:14'),
(21, 6, 2, 1, '2018-01-08 06:01:34'),
(22, 6, 28, 1, '2018-01-08 06:01:41'),
(23, 3, 26, 1, '2018-01-11 09:47:27'),
(24, 3, 2, 1, '2018-01-11 09:47:24'),
(25, 20, 2, 0, '2018-01-11 09:47:43'),
(26, 20, 26, 0, '2018-01-11 09:47:44'),
(27, 20, 28, 0, '2018-01-11 09:47:46'),
(28, 20, 30, 0, '2018-01-24 09:21:08'),
(29, 20, 61, 0, '2018-01-11 09:47:48'),
(30, 20, 74, 0, '2018-01-11 09:47:49'),
(31, 20, 80, 0, '2018-01-11 09:47:50'),
(32, 0, 26, 1, '2018-02-05 12:24:05'),
(33, 0, 2, 0, '2018-02-05 12:24:44'),
(34, 1, 26, 1, '2018-01-15 06:48:19'),
(35, 1, 79, 1, '2018-01-15 09:02:35'),
(36, 3, 49, 1, '2018-01-15 11:01:58'),
(37, 1, 39, 1, '2018-01-16 05:18:59'),
(38, 1, 15, 1, '2018-01-17 06:49:49'),
(39, 1, 49, 1, '2018-01-17 06:49:53'),
(40, 5, 15, 1, '2018-01-18 06:52:12'),
(41, 5, 9, 1, '2018-01-18 06:52:13'),
(42, 5, 49, 1, '2018-01-18 06:52:14'),
(43, 5, 83, 1, '2018-01-18 06:52:16'),
(44, 5, 26, 1, '2018-01-18 06:52:20'),
(45, 5, 2, 1, '2018-01-18 06:52:20'),
(46, 5, 28, 1, '2018-01-18 06:52:22'),
(47, 5, 30, 1, '2018-01-18 06:52:23'),
(48, 5, 39, 1, '2018-01-18 06:52:24'),
(49, 5, 61, 1, '2018-01-18 06:52:26'),
(50, 5, 79, 1, '2018-01-18 06:52:27'),
(51, 5, 80, 1, '2018-01-18 06:52:28'),
(52, 1, 61, 1, '2018-01-18 10:48:36'),
(53, 1, 90, 1, '2018-01-19 10:10:07'),
(54, 25, 97, 1, '2018-01-20 09:01:41'),
(55, 25, 15, 1, '2018-01-20 09:41:04'),
(56, 25, 49, 1, '2018-01-22 10:20:45'),
(57, 30, 74, 1, '2018-01-25 12:51:50'),
(58, 30, 107, 1, '2018-01-24 12:36:12'),
(59, 0, 107, 1, '2018-01-25 12:13:41'),
(60, 0, 99, 1, '2018-02-07 06:04:53'),
(61, 0, 83, 1, '2018-02-02 10:28:13'),
(62, 0, 49, 1, '2018-02-05 17:33:35'),
(63, 0, 15, 0, '2018-02-05 12:29:48'),
(64, 0, 30, 1, '2018-02-05 12:24:39'),
(65, 0, 87, 1, '2018-02-05 12:24:41'),
(66, 0, 9, 0, '2018-02-05 12:25:02'),
(67, 0, 80, 1, '2018-02-05 12:24:02'),
(68, 30, 49, 1, '2018-01-25 05:38:26'),
(69, 30, 83, 1, '2018-01-25 05:38:27'),
(70, 30, 9, 1, '2018-02-06 07:32:03'),
(71, 30, 87, 1, '2018-01-25 05:38:30'),
(72, 30, 15, 1, '2018-01-25 05:38:31'),
(73, 0, 74, 1, '2018-01-25 12:55:03'),
(74, 4, 74, 1, '2018-01-25 12:58:47'),
(75, 3, 74, 1, '2018-01-25 13:02:05'),
(76, 3, 107, 1, '2018-01-29 07:32:29'),
(77, 30, 105, 0, '2018-02-08 11:36:22'),
(78, 34, 105, 1, '2018-01-29 09:17:50'),
(79, 5, 107, 1, '2018-01-29 11:25:36'),
(80, 4, 75, 1, '2018-01-31 11:50:43'),
(81, 3, 99, 0, '2018-01-31 11:51:22'),
(82, 3, 105, 1, '2018-01-31 12:18:14'),
(83, 29, 66, 0, '2018-01-31 12:39:34'),
(84, 0, 46, 0, '2018-02-05 12:24:38'),
(85, 0, 24, 0, '2018-02-13 04:13:59'),
(86, 0, 39, 0, '2018-02-05 12:24:01'),
(87, 0, 105, 1, '2018-02-02 10:26:18'),
(88, 0, 66, 1, '2018-02-05 12:24:42'),
(89, 30, 136, 1, '2018-02-02 11:20:43'),
(90, 30, 75, 1, '2018-02-02 12:59:09'),
(91, 25, 136, 1, '2018-02-03 06:12:00'),
(92, 0, 136, 1, '2018-02-07 06:04:52'),
(93, 30, 24, 1, '2018-02-06 07:26:31'),
(94, 30, 66, 0, '2018-02-08 11:36:23'),
(95, 30, 164, 1, '2018-02-06 09:22:48'),
(96, 5, 164, 1, '2018-02-06 09:32:55'),
(97, 30, 166, 0, '2018-02-08 11:36:21'),
(98, 2, 105, 1, '2018-02-11 09:38:16'),
(99, 2, 136, 1, '2018-02-11 09:38:18'),
(100, 0, 176, 1, '2018-02-13 04:13:56'),
(101, 0, 182, 1, '2018-02-13 04:13:57'),
(102, 0, 194, 1, '2018-02-13 04:13:58'),
(103, 2, 183, 1, '2018-02-13 16:17:16'),
(104, 42, 238, 1, '2018-03-05 10:58:47'),
(105, 42, 237, 1, '2018-02-16 07:33:32'),
(106, 42, 189, 0, '2018-02-28 05:51:09'),
(107, 42, 105, 1, '2018-03-05 10:58:55'),
(108, 42, 200, 0, '2018-02-28 05:51:06'),
(109, 42, 198, 1, '2018-03-05 10:58:51'),
(110, 85, 213, 1, '2018-03-14 11:36:28'),
(111, 85, 238, 1, '2018-03-14 11:36:31'),
(112, 85, 237, 1, '2018-03-14 11:36:32'),
(113, 6, 171, 1, '2018-03-14 13:06:42'),
(114, 124, 213, 1, '2018-03-15 08:46:17'),
(115, 124, 238, 1, '2018-03-15 08:46:20'),
(116, 124, 237, 1, '2018-03-15 08:46:25'),
(117, 124, 144, 1, '2018-03-15 09:14:05'),
(118, 124, 193, 1, '2018-03-16 05:38:08'),
(119, 124, 241, 1, '2018-03-16 06:26:35'),
(120, 124, 249, 1, '2018-03-16 10:44:07');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `book_name` varchar(100) NOT NULL,
  `book_number` varchar(50) DEFAULT NULL,
  `book_code` varchar(50) NOT NULL,
  `student_id` int(11) NOT NULL,
  `book_author` varchar(50) NOT NULL,
  `created_by` bigint(22) NOT NULL,
  `books_other_details` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_activity_demo`
--

CREATE TABLE `book_activity_demo` (
  `id` int(11) NOT NULL,
  `activity_id` varchar(255) NOT NULL,
  `academy_id` varchar(255) NOT NULL COMMENT 'teacher id',
  `student_id` varchar(255) NOT NULL,
  `student_name` varchar(255) NOT NULL,
  `student_phone` varchar(20) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `demo_date` varchar(255) NOT NULL,
  `activity_name` varchar(255) NOT NULL,
  `package_plan` varchar(255) NOT NULL,
  `plan_id` varchar(255) NOT NULL,
  `all_data` text NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0' COMMENT '0: pending',
  `created_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_activity_demo`
--

INSERT INTO `book_activity_demo` (`id`, `activity_id`, `academy_id`, `student_id`, `student_name`, `student_phone`, `student_email`, `demo_date`, `activity_name`, `package_plan`, `plan_id`, `all_data`, `status`, `created_date`) VALUES
(1, '1', '244', '25', 'vikas', '9990731159', 'v@gm.com', '2018-02-22', 'Football', '918918', '40', '', '1', '20-02-2018 11:43:27'),
(2, '3', '244', '25', 'vikas', '9990731159', 'v@gm.com', '2018-02-22', 'Cricket', '999', '41', '', '1', '20-02-2018 11:48:09'),
(3, '18', '251', '25', 'vikas', '9990731159', 'vckvikash702@gmail.com', '2018-04-6', 'Gymnastics', '949464', '72', '4ef93867dd3f4fca7b9b0ad2b4932783a680e4deaa73309ba0119f59ddeca3fd8632e1e6d4f1a735273cf92bf40e1ffd08d8fa9c53710253d2acb9c2f27c3f30b96d894c3ee9c1fb1dd0594aaea66f4fcb8b7d012a254d397e75ee02603f38d1b8f24f7a336fcc6c9e31f2592e5799c2', '1', '04-04-2018 11:12:15'),
(4, '14', '251', '25', 'vikas', '9990731159', 'v@gm.com', '2018-04-5', 'Basket ball', '1', '91', '4ef93867dd3f4fca7b9b0ad2b4932783a680e4deaa73309ba0119f59ddeca3fdb99ddb77c49b43d5d482f20fd2d2da768c16b257dd4459e4b33c613f4300a0a9839cc23d9fa4810c51806a6ac081fbd035011c989231b44d71fe385d5958fd7474a20f7e68ce34c621843e2d90fbbdbf', '1', '05-04-2018 10:30:13');

-- --------------------------------------------------------

--
-- Table structure for table `book_batch_demo`
--

CREATE TABLE `book_batch_demo` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `req_date` varchar(255) NOT NULL,
  `req_time` varchar(255) NOT NULL,
  `batch_id` varchar(255) NOT NULL,
  `added_date` varchar(255) NOT NULL,
  `approve` int(11) NOT NULL DEFAULT '0' COMMENT '0: pending | 1:approved | 2: rejected | 3: completed',
  `status` int(11) NOT NULL DEFAULT '1',
  `is_view` enum('1','0','','') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_batch_demo`
--

INSERT INTO `book_batch_demo` (`id`, `student_id`, `teacher_id`, `req_date`, `req_time`, `batch_id`, `added_date`, `approve`, `status`, `is_view`) VALUES
(80, '125', '253', '2018-04-5', '12:00 PM-01:00 PM', '170', '04-04-2018 18:55:51', 1, 1, '1'),
(79, '124', '249', '2018-03-21', '12:00 PM-02:00 PM', '169', '16-03-2018 15:30:47', 1, 1, '1'),
(78, '124', '241', '2018-03-17', '12:00 AM-01:00 AM', '166', '16-03-2018 11:36:43', 1, 1, '1'),
(77, '124', '241', '2018-03-15', '12:00 AM-12:01 AM', '164', '15-03-2018 15:48:59', 1, 1, '0'),
(76, '124', '241', '2018-03-15', '12:00 AM-12:01 AM', '164', '15-03-2018 15:46:33', 1, 1, '1'),
(75, '124', '241', '2018-03-16', '12:02 AM-03:11 AM', '162', '15-03-2018 15:38:45', 1, 1, '1'),
(74, '42', '241', '2018-03-16', '12:02 AM-03:11 AM', '162', '12-03-2018 12:02:51', 1, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `book_session_demo`
--

CREATE TABLE `book_session_demo` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `req_date` varchar(255) NOT NULL,
  `req_time` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `student_lat` varchar(255) NOT NULL,
  `student_long` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `all_data` text NOT NULL,
  `approve` varchar(255) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT '1' COMMENT '0: pending | 1:approved | 2: rejected | 3: completed',
  `added_date` varchar(255) NOT NULL,
  `student_review` text NOT NULL,
  `teacher_review` text NOT NULL,
  `updated_date` varchar(255) NOT NULL,
  `is_view` enum('1','0','','') NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_session_demo`
--

INSERT INTO `book_session_demo` (`id`, `student_id`, `teacher_id`, `req_date`, `req_time`, `session_id`, `start_time`, `end_time`, `name`, `phone`, `address`, `student_lat`, `student_long`, `fees`, `all_data`, `approve`, `status`, `added_date`, `student_review`, `teacher_review`, `updated_date`, `is_view`) VALUES
(35, '6', '15', '2018-01-18', '05:00 am', '75', '', '', 'Rahul', '8700648727', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219975', '77.0765106', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1ad1e3a8828b6ab5c207ef0adab5b797d84058a61622a45edf369ad2574b6beb19aa2616c364346c7e45f84bcdb84df04d116cbf5fde2caf014ddb8c9b177b39101f3f056d04abd7e8d101c5a85f9404b50807ac9bed91bd8f4dcde11efed199e48304669de8149b2b0052f144b57af74751e974fb8ab9d9b94f786b191b75ff7e9eb05db4b7c0cc6de29b16dc7366f13281aca89f8d25514f649d513193f71242614bad30aac7f0845a8e36e215d08725f79b30e1592ce07bbb0c8037aa696e89643dbfbbc63d89cac3ba5bc302ff528dd6b68ad83845715517a9e4b47a357c016661cc6baf2edb5b868ee123917a8208e13d5b658365bc1c5046947f845bff32d4240b7d1522c5a0c1344748f813151d7badd2b4aa652977790e12aa1fb1adbdb97ee10485e75ad3845fa0fe535ad73b75edb366abe68a054e06bd3fe1eb4925260956bea3737e738ae8f5cea70b47794663174d958b7e5522d05736ab4c0a586c5a39d7a18b339143997372ba30c143f5401ec3b15025925de679e294185cd9ddf607ff853dc0501096ba734485836d934da55d8bace21fb0c1e6561dee153794bd04c7a92dc613bb182bba397d55e21623c6502813c2d388009a3bc7cd20399e716da9630bc4ac8fa8b2c943d2cd46f48092f1bdc80ecc99832a4b8ac5d170972c2cc447a97bb66afef554466d6092c788bd48410a83457f13e6a914f8020e9e0359edf051eb9b9cd0834b5a746be59a436882d42f1f6f7ebf57983c2d5e89b6d31ea94bb45e9cc07548ee78c42174b6a17d8928bfdcb605cdc08e21446fdea912dc6394a799b3b0682e14303e17955b55ad11a4d43bdb4b6fb5c9e8dbbd230deff84fe93dc6281b31661795f3d7d015431817b31f34996f85cb046947d3df42322dd793717b98215b9a67e25ececad49d8f586b576de27fc9cacd1bf2db3a3487d011316cc65f22c2cc74ee70313ff7e76b035b88fe7d3ce4bb4505abea7d23e5e3ea4021ca85d540cd4ac9487ab40c5db1438e85957a572236b4784a42a7cdcef1c1a58111959279cd68d5a691821a53eaee528685d69906dc51e9b23672', '1', '1', '18-01-2018 21:10:27', '', '', '18-01-2018 21:10:56', '0'),
(38, '25', '15', '2018-01-20', '06:00 am', '100', '', '', 'vikas.', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219886', '77.07652', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1ad1e3a8828b6ab5c207ef0adab5b797d84058a61622a45edf369ad2574b6beb1938b95d497c83f01766ea5e449fbfaff78033eb1210530f759567c4769747e17e46aec628750241267c1dd3db00f3c7158393601a2aaa34e1f0c8d22816a6605f3ae13427a3b8f9a93d01a6b4db33db9062bf3b0caac20802b2ccde7ebbf4ad0eb8c2b2d804e7d5a64a0778acfe0378ff9b351b4b3349a7e4ee3950de6afb4ad3710c295ec70ccdc5f2a23d3e262f9eca6e6d28532bce6a570638655c466d3f0fe5a3d0bc3154d7f9b9d568b5a4a4d573fe756c29214b1788967e30b24463db8313f2caa2a17b401880422f3b17b1aa918448013169a9306b6ee9862fb0942f7b5f318442a8eec558715071449744c031677d5300fcbef20d2ac9b08d74012984c9b6aaa46ca73dc4422e1f92fa6fb9b9881b69804ada9741a38dff2cb83c134e3f0353db4413529ba6369a0a93afc5c8e0e3a385e507886777beb30eba0992bff3c53193606efa711302e659a23e6191f787dac5005d8f298e3fb0e96a2c39735d26d5f14962375bba631f0771fd04ef1c55e675c2e785da70ae43d3ee69194c27cdf69e6a9ac17035334d1808eff9a42bb296cd03860f914240b6dc57d3bb4cc3997ed560aae9a6bf3a0adef7301044ed1841cc4b862b3b98b398c0c13ed12707d4a205f6d206a8d98fb0608b886f2e1ab8c0ec4180f82b1ce413dcf134837b9f574b7d6c4acb91346b50faf18660dd22081c12697eb63b0683746090283daef02a1fa0f1be3aa391c8cae2ab6f8458fb623bab6b03f90df547563fb692bbb9709c32f513f2d21f7d3cdd509384801ed5f847aefac40b66cfa41c5271c1cf36fd03aa8d4ce08eda4522beb39d4e41ca8294a83397337870fce315d4bbc29978175409521231e7e7f928dfe68c12f7c100a5921a1cb97e3d6aff5f407451640108916baba7bc27e92bd14a08393aaae3355d4d97c66f5dcbcec05048967ef8bfca5fa7497db0ff67cc68781fdfac6baebce2ca1238baf4f8b4511954c922e35b', '0', '2', '20-01-2018 15:11:58', '', '', '20-01-2018 15:12:23', '0'),
(54, '25', '87', '2018-01-23', '02:00 pm', '109', '', '', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219852', '77.076512', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440eba3eefa9b047dcb2bf60a5d476ba0b93c4330e49035a715f4cb89b25c0db41bad8828d42de65c4e426635ee575333fd6a15d5cff664ea0e71d5169f5ac7089ec904210468e412aba15751275d1b228470a2b99eec55fcb67431c2343695fc3dd8c5c6a3ef7661e484c10a1ffcf607431968437897b82e72648e41f2bbdc80d36369b1f5526533e54ea13c0dcff2f519084771021f9025919bbc6c07ddb64283c67bfd00ea45404bf47a8b828c9aebf30d5339f0aa360169fc1a0ff521e21689515967c29b41d1640341ec280b5115560bb9a5d2a921a0e671c86a5267392e297cc36339e6b56972d1675313ceb6337402d90f3f6a30aa7deebd4e9937545a29d898ed8e39be062cdd044ed89924d90ce3cc09dd6040102b172fcee4e0cccb6359fab35b990c0b0dec31e51fc9d6f155263cb73fe05096178a0adc7532ca3dc4eb8dd02edf934664df9cb5e88703d3c55aa08a3e696e8c1238b2fe948e008a85154c7bfb2360397e80490596a16883bcc554168f6ba0fee0829f4a0b1d45f49ea8351ce89dabcc047dc65ed84219f5426e6771fcb765fe49d00ba2090c258bb0c499a4209be0f552cf9bd7cb7ac253af52d2cc043ee007d39f658af996f3f4668d14df8ec609d4d705733a340f8e210124366778aa76fb6fc40107fa7d821fae0739e54e7eb201686d9c179ac406753d1e924a43cf84811d5d3aeb2285824365e16939cf3b2279b73ed5c260ea8b7ccc1646d7f0222e349faaabb15c4d52b2c6a276ec20008116c557aa31a2b126a03847c1c69ac20fb337c0ff221c93b6b3913596f24eda86bc53ccf53f5593329dba6e0479f327d910576755d4547fd9c3cafcea9ff1d19592bc33bb0bcbfa17d6c9d3be785d4d617810875f6acd334a945d86662cad37aee4158c25da7f18a1fe0f813dda4cae34ecc47b55c67e68b41dca435002abd33e3474a8065ac925996b76fd6c4fc984e14012a56470e8b779ea25d20676baff0ff4096e86eb2c809c2c3784daf463776dce7cdf9487442c6e58f07c2ddc3593110d51ef8213e3afd99f5d56c9f855665fae6834cad99ea343f1b4', '0', '2', '23-01-2018 18:40:59', '', '', '24-01-2018 16:25:03', '1'),
(57, '25', '87', '2018-01-23', '02:00 pm', '109', '', '', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219852', '77.076512', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440eba3eefa9b047dcb2bf60a5d476ba0b93c4330e49035a715f4cb89b25c0db41bad8828d42de65c4e426635ee575333fd6a15d5cff664ea0e71d5169f5ac7089ec904210468e412aba15751275d1b228470a2b99eec55fcb67431c2343695fc3dd8c5c6a3ef7661e484c10a1ffcf607431968437897b82e72648e41f2bbdc80d36369b1f5526533e54ea13c0dcff2f519084771021f9025919bbc6c07ddb64283c67bfd00ea45404bf47a8b828c9aebf30d5339f0aa360169fc1a0ff521e21689515967c29b41d1640341ec280b5115560bb9a5d2a921a0e671c86a5267392e297cc36339e6b56972d1675313ceb6337402d90f3f6a30aa7deebd4e9937545a29d898ed8e39be062cdd044ed89924d90ce3cc09dd6040102b172fcee4e0cccb6359fab35b990c0b0dec31e51fc9d6f155263cb73fe05096178a0adc7532ca3dc4eb8dd02edf934664df9cb5e88703d3c55aa08a3e696e8c1238b2fe948e008a85154c7bfb2360397e80490596a16883bcc554168f6ba0fee0829f4a0b1d45f49ea8351ce89dabcc047dc65ed84219f5426e6771fcb765fe49d00ba2090c258bb0c499a4209be0f552cf9bd7cb7ac253af52d2cc043ee007d39f658af996f3f4668d14df8ec609d4d705733a340f8e210124366778aa76fb6fc40107fa7d821fae0739e54e7eb201686d9c179ac406753d1e924a43cf84811d5d3aeb2285824365e16939cf3b2279b73ed5c260ea8b7ccc1646d7f0222e349faaabb15c4d52b2c6a276ec20008116c557aa31a2b126a03847c1c69ac20fb337c0ff221c93b6b3913596f24eda86bc53ccf53f5593329dba6e0479f327d910576755d4547fd9c3cafcea9ff1d19592bc33bb0bcbfa17d6c9d3be785d4d617810875f6acd334a945d86662cad37aee4158c25da7f18a1fe0f813dda4cae34ecc47b55c67e68b41dca435002abd33e3474a8065ac925996b76fd6c4fc984e14012a56470e8b779ea25d20676baff0ff4096e86eb2c809c2c3784daf463776dce7cdf9487442c6e58f07c2ddc3593110d51ef8213e3afd99f5d56c9f855665fae6834cad99ea343f1b4', '0', '2', '23-01-2018 18:41:36', '', '', '24-01-2018 16:25:06', '1'),
(56, '25', '87', '2018-01-23', '02:00 pm', '109', '', '', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219852', '77.076512', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440eba3eefa9b047dcb2bf60a5d476ba0b93c4330e49035a715f4cb89b25c0db41bad8828d42de65c4e426635ee575333fd6a15d5cff664ea0e71d5169f5ac7089ec904210468e412aba15751275d1b228470a2b99eec55fcb67431c2343695fc3dd8c5c6a3ef7661e484c10a1ffcf607431968437897b82e72648e41f2bbdc80d36369b1f5526533e54ea13c0dcff2f519084771021f9025919bbc6c07ddb64283c67bfd00ea45404bf47a8b828c9aebf30d5339f0aa360169fc1a0ff521e21689515967c29b41d1640341ec280b5115560bb9a5d2a921a0e671c86a5267392e297cc36339e6b56972d1675313ceb6337402d90f3f6a30aa7deebd4e9937545a29d898ed8e39be062cdd044ed89924d90ce3cc09dd6040102b172fcee4e0cccb6359fab35b990c0b0dec31e51fc9d6f155263cb73fe05096178a0adc7532ca3dc4eb8dd02edf934664df9cb5e88703d3c55aa08a3e696e8c1238b2fe948e008a85154c7bfb2360397e80490596a16883bcc554168f6ba0fee0829f4a0b1d45f49ea8351ce89dabcc047dc65ed84219f5426e6771fcb765fe49d00ba2090c258bb0c499a4209be0f552cf9bd7cb7ac253af52d2cc043ee007d39f658af996f3f4668d14df8ec609d4d705733a340f8e210124366778aa76fb6fc40107fa7d821fae0739e54e7eb201686d9c179ac406753d1e924a43cf84811d5d3aeb2285824365e16939cf3b2279b73ed5c260ea8b7ccc1646d7f0222e349faaabb15c4d52b2c6a276ec20008116c557aa31a2b126a03847c1c69ac20fb337c0ff221c93b6b3913596f24eda86bc53ccf53f5593329dba6e0479f327d910576755d4547fd9c3cafcea9ff1d19592bc33bb0bcbfa17d6c9d3be785d4d617810875f6acd334a945d86662cad37aee4158c25da7f18a1fe0f813dda4cae34ecc47b55c67e68b41dca435002abd33e3474a8065ac925996b76fd6c4fc984e14012a56470e8b779ea25d20676baff0ff4096e86eb2c809c2c3784daf463776dce7cdf9487442c6e58f07c2ddc3593110d51ef8213e3afd99f5d56c9f855665fae6834cad99ea343f1b4', '0', '2', '23-01-2018 18:41:34', '', '', '24-01-2018 16:25:19', '1'),
(58, '20', '15', '2018-01-25', '05:00 am', '75', '', '', 'Naved khan', '8802915021', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219769', '77.0765342', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1ad1e3a8828b6ab5c207ef0adab5b797d84058a61622a45edf369ad2574b6beb19aa2616c364346c7e45f84bcdb84df04d116cbf5fde2caf014ddb8c9b177b39101f3f056d04abd7e8d101c5a85f9404b50807ac9bed91bd8f4dcde11efed199e48304669de8149b2b0052f144b57af74751e974fb8ab9d9b94f786b191b75ff7e9eb05db4b7c0cc6de29b16dc7366f13281aca89f8d25514f649d513193f71242614bad30aac7f0845a8e36e215d08725f79b30e1592ce07bbb0c8037aa696e89643dbfbbc63d89cac3ba5bc302ff528dd6b68ad83845715517a9e4b47a357c016661cc6baf2edb5b868ee123917a8208e13d5b658365bc1c5046947f845bff32d4240b7d1522c5a0c1344748f813151d7badd2b4aa652977790e12aa1fb1adbdb97ee10485e75ad3845fa0fe535ad73b75edb366abe68a054e06bd3fe1eb4925260956bea3737e738ae8f5cea70b47794663174d958b7e5522d05736ab4c0a586c5a39d7a18b339143997372ba30c143f5401ec3b15025925de679e294185cd9ddf607ff853dc0501096ba734485836da5a5162ea5831a101e26c4199ef8af117f3dfb3793af41a2c4555ec053349412072393c4d0a81bc8dcfb8126a6a4b5d4a3dfa3f2f511f93f0b9d2d5a1b036aab376d958a421e4b5fa6b7d065e953aacd7a9dd4f5694148db292a69b54eb3646cf5ec2ce18271b3dcd55c94bab59d0356e87c6a88848fdc0065d709363734171cadb5d1a35c374ba38f42d81aa919ed3869d9b65c693f8af9002bbc4027226ca57a3a69e2bd456527d3064752c308fcd60fa6cda0fd0fa99c73742fbdd72bca79a127bb73b8bb170adb647788361b82f5b3f88b370f261c67c3f3acf79f1d897ba27600a4f3f78fbbe0eacd7f9ad4174f6aafa5707145a9d34962394bdeb5de976cc3371177495838858468e92d1682d7d2e39f6925b422fa6b09737e59bee95b94a5a198796f815ae4e9612f7d343940c4c4976ac052d9396839f9a30344e800a85e5b5466a75bbb973be8a06eae2611', '0', '1', '24-01-2018 14:49:52', '', '', '', '1'),
(59, '20', '87', '2018-01-29', '02:00 pm', '109', '', '', 'Naved khan', '1802915021', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219695', '77.0765192', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440eba3eefa9b047dcb2bf60a5d476ba0b93c4330e49035a715f4cb89b25c0db41bad8828d42de65c4e426635ee575333fd6a15d5cff664ea0e71d5169f5ac7089ec904210468e412aba15751275d1b228470a2b99eec55fcb67431c2343695fc3dd8c5c6a3ef7661e484c10a1ffcf607431968437897b82e72648e41f2bbdc80d36369b1f5526533e54ea13c0dcff2f519084771021f9025919bbc6c07ddb64283c67bfd00ea45404bf47a8b828c9aebf30d5339f0aa360169fc1a0ff521e21689515967c29b41d1640341ec280b5115560bb9a5d2a921a0e671c86a5267392e297cc36339e6b56972d1675313ceb6337402d90f3f6a30aa7deebd4e9937545a29d898ed8e39be062cdd044ed89924d90ce3cc09dd6040102b172fcee4e0cccb63fc592d3951fdaaafd2e329840a563a0c2864ce7201bb2c36f67a734e47c67746e50451df21280c4937bda44e0f385779dd8934da06e465cb07a8dda6961f0918353fb46813c7b5f9384ead97edddfcb84adfd38b008b366e0cf681242447dd4e5d0d59447cebb1492b34dad5c40e0e0520a19b8e216141fc8762b5ca27d1b35efbc3145bc7bff3384ab4a7cbd301830cf7335d2c0c891a8d4de6c25fb1da5931d005b99ac09e19cad0aad1b51a4095d7a605aff18513a4220383bcced474fba6683c9275a0ffa7ca7bc0bab2cb2b47bdee78ae1b6670b437f27e6c7b65b74b0623535872ef5760545556cfbf1efe2cbe4ce76f7c2c27e8eb67753599bc6dec6fc6dc61d0afee5912838a786b993e9901d1ba092d9c8ae5e44fe93d773a6933b288e4c57ec036f4e9c08d8479972d49d1c94a8bfd6dc175b26d2c28d363d52b81b5da1d2cc6a6c1ac96c5da65ff86a97532bc3f173ffb596e1b23786ac4b07a73954244ffa026fc07e328ff1fbf1458dff1fd3c545e4cf650204ffc80ef496f48', '0', '2', '24-01-2018 14:52:37', '', '', '24-01-2018 16:25:22', '1'),
(60, '4', '49', '2018-01-25', '06:00 am', '110', '', '', 'Amit K', '9560610340', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219692', '77.0765246', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e088ce35c70f5cd5300479a4ddb514f001ffee4ae1dfb2e441952149b967eaef27faff7a568dfb7e3f35e99d991dbe256136f1b2e028b697b7b7308ddba7226222366707c23d7956f01f56fbb6ffaa0ce945686387f06fcba845a34e776751a1f527a62ab40f2084f9e9fffb78e4f104b91b1c86fc7545387582a3e1c9f9547f9a6a91032eba405c2c5ef538b8e81d5be66595c2c894d93001faa59d54a46caa0aae44cbb597a85bbd18ae0bffaa5646b1725cd7892ce9b6a4428c13b7e85cc98f8d9d76c652b615184bfaa5792edda60f96b30ef9afc50a5d164a6ceae2e35ec7184d9b216be7545a63ae8750f512c6ac042aeb0c8364d2e710ecbeaa6f4ec2dd286df299105012dedcf7c956ae0bbd2bf55089b58b17ed63e4bddb63ad4833292377c8d69d51f1b110fa425478be4837e54d1baa9ab4f7b011cc9e5505943bc80f4d92bde0c7d63fa8e309e348c44512898715a99a04e20f6c17b1272008f9888df129461e8a517e25f358104386c03e66ec2941b7e3e678348fc89875177649798117f1a9aebdec72c774eba7dfcbee5beb605467d2343de399d3f29ef30410d042bea953f4eea077aec327c96b2905d6d5870300b592fa8a2e1ecbb6098cd945303831de437a9144db47572b3342204dcd7df8f4fdf6c1c6b076f6d1e589a3716913b42bc8be45cd1b3a1d18affacb98a8f11bf13c9df24e7c3c45f94e339fc607c8a1e8ad4faed9041d7acbecd2e9d110cc33121824cb68cd975955d4251c22742c12fec9b2a18b0eb5105debbcf757bcadf92ba35d424af05e4623276e99f8fa8add80d0ae251201d0815e9e48517e7b6a2f2a27a3e9a3130d638b479a0aa01eb3dab317d0e395974763eca17282687dc2265e2ecc78b1118a24b8834bdb7362402595032e358018ca8aa23c5afcffde7b144323ae43328278ed49fe5ad29c449ec17b58d93b1bef253c1f1f7e99760b756d4df9b3e84bc053be9dc99986666dc01ce617fab0cf40532104dc465aab18ddfe2ffb6eaa8ef8e95ce13a305d8dee88b5d52792840260d69d19df9a40c775fbd12d83626ddbdb5d49b186668bcdeebd9a82c4d5466c9b592b78c72b326ed98cd29caf90e2c03002d8267a83c290a68be1eb308b340fe00c2cbb886f372cc2192b6de5d3a969169691e0f8f8d33af5846a8f9a87d7602ca128c3d2bc21711d6042f5b5868b5361945609fbe83f79ce184ab919066cea3a7b9b35d3b7715513dd3ece2c69bf73cdb5858bf834a898f934d23d35fd3640b4fed048599cf8adb07a6f5d652c07ddd2391c32140e3adce48364d55b080437f8a7514cf3006c5', '0', '2', '24-01-2018 16:27:27', '', 'Student location student too far', '24-01-2018 16:32:38', '0'),
(68, '20', '9', '2018-01-29', '05:00 am', '118', '05:00 am', '06:00 am', 'rahul', '1802915021', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219689', '77.07653', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a65371753166306664a2f222585a923dee8d1e6f05295bd844abc1b1a54b075c7dc79a7e8d999c331c7014dcbe74429ab185c6b9d503282625687f74c6938c1b6d226dd3dcf0fdeff07bc7275b46c5a75cb9f187e179ab409786ce7ba69f69a70349af9c346eff0c4803d0f8b51273159940e69cc2a6fdbbe84cceeee9ae1eb69e145eda9c3cf88b37a408281ec6a7f0f2c02a282528980181733ad47cb4734f9b77b81f6e7e56cc456336e072852f35df768ab917c8d5e404007ca7130d1fbc854261cf9e451a78ce48b4d2ff9907f1ee6c74cd06c898643dcfd8b4d7f959f573849a1f409c127740888925c6f1b07a53656f2120e6d22af810f7cb98b18bb77716bca9bcec817cbd215fda8ccf5dfb24ba415055928a32400dd72b0bfa511910fc345c2fa51ff3cca01074159782b70aa1907f49a792f9a5a43b2ffaa65539f418f69ab6323d6409987a1cc1dd61a8e95b342194b52ab02238a056acf77eccd7b168360b2e6d6490e48298906881af0fba0dbcf1348827f8f101cc282c343e9d945035a2b833612ba5c8a17d19d43cf882f5c20e46d9d20f66b77e80498bab8f0951933fb6e435ddc52f837dcd1d09d7264443518e256addbfc7ce7d62508e51c60b6953d6314d1ad0575a48b2023508fb61a82780b8cea913c078c6be645114d1b7e2f5d7ee106ed4f98105dbdded36c6d6033889f979e28a39886de2b7c3881156eb1ba15ac2e41289028cff769d7c87aa6c3b44256203463b25962c74d76d29caaf39792e1c77aac40e890e5664351c63c01f4fb84f40f5759a0f0991369993d8fd14010a37a567169a632e47959b992e4a26239c0cd42928eeb64f59d21d21a431e15a1d081f32774d67f4502b81a8d613a9aaa178f8f754a39f7e6baddd1cbe40519c9059cd2d6d7d1650ab5d1a3c219c0be1876f32d0491627f2e87b87d262684e363edcfb4c9a1ad624fd5ed832ee91c0bde752dbfd45b2c7c2e4db5212d73346bdef3caa2744ae5b16c85d0d6b1b5f2990cdd54696582f4d2f860e95188d17371d66a8e3a97bcb827be2ff37ddb737aedad0ed6c0dedd1fe550c5ee36ffda9f3dfbbae331e22b30863961b7c2bd42b6a3bd940aa87bf3554efce56ab3b8de41069a4688abdef113bcb2f7f7487e09984c1bdef4332acb12cccdfe3102d3b3d423a23ee53e17424bff7d1434216ce8cbcd796d8e88fdfc3c790d730883ed03a138c35afa0e4d3f7641c38c6cc3565668d8ab1386a6078fedc8cac38c134fcc84cd0146ac489c3dcc09832462f39ecf2a7be40333eadc37ce13dcb129bfbd0915c7500315819244002f87a25fe63d6f578057e7620e0637ebd3553b01dfb1b0ec30d167bd7e3c3328d223ca4220e1bee33752502d9e45c068e24c270ca085172bea34c96468884c367aa1920ad8d5a3a316a0a588b823be3b261a541ca7b45a832a27bf6275d2c5572d1edaf93a5915ee0a910eebe1c696acb1b3a32bbe2a3dcedc7195b42c1b0e63b5829157', '1', '1', '25-01-2018 11:20:13', '', '', '25-01-2018 11:20:33', '1'),
(62, '25', '87', '2018-01-24', '02:00 pm', '109', '', '', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219633', '77.076514', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440eba3eefa9b047dcb2bf60a5d476ba0b93c4330e49035a715f4cb89b25c0db41bad8828d42de65c4e426635ee575333fd6a15d5cff664ea0e71d5169f5ac7089ec904210468e412aba15751275d1b228470a2b99eec55fcb67431c2343695fc3dd8c5c6a3ef7661e484c10a1ffcf607431968437897b82e72648e41f2bbdc80d36369b1f5526533e54ea13c0dcff2f519084771021f9025919bbc6c07ddb64283c67bfd00ea45404bf47a8b828c9aebf30d5339f0aa360169fc1a0ff521e21689515967c29b41d1640341ec280b5115560bb9a5d2a921a0e671c86a5267392e297cc36339e6b56972d1675313ceb6337402d90f3f6a30aa7deebd4e9937545a29d898ed8e39be062cdd044ed89924d90ce3cc09dd6040102b172fcee4e0cccb630f3e3aceff233684c6a5e60177667ff1092e33194b320ba53121854af59a72a2bc8fb889407cd29948a27e39726e794d3974c554f9ea0f8c9ddef292df8b8381f823dd0450365362a69877dda99de249223d0128ea86e80ffe48133e56425be718450c8fbc2cfa2d317f606132cac8477d40660bedab61a551c95c7899ea4147b9e2456e2757515d9347bf5959ffb40c016a7b938dba1f6966dc02db293d22778699ca9d10a2411ba5a53c4a52fbd799cdd2c86ee7a0556d3c23d0e3b8ca7c04b64d954bd396883934f9052ddd561673d519b1686e1ce9c703c4906cc94f099438cfe3868481c72f32c10ded42e5cfb5e1261ea73a33d17be595b58c5fdf1150793ce916dc274354cc5fb36ceeaf088610ab46d839a25f568905e050010bfc6fe1d7edc236e7b6c7d53d83a54df44256b0696ee0902552f70cea6e0818874f19df756bb9a8fd4d9ca96701dfac60e696d689ca8fba474d4b14d7ca25b2cf5fec1f5d4983831c82eea41460279925cf1f42950c18fbee85e08215b46f8b8e49905d2de1546a9de0cf41958b06ff6a5a1e2df968aa53698d2d86a956ceba2f174a3548c0909a1daf5241fbcb091cfd468a8c12a1feb803235969f918fee4a01ab2b4c4c6216a0064490249a1643598525fafbf7c7ec9e624a671b5326ec7aa6a1fb38092b331a31770913408404da67b171cf6f7b548c4bd1f30693e9b46cf41bc90795149573d8660255f444eb8a6f1797ca3f71da461ef13ab26da332c5e99b99837882820e34746d729d9a16f2ef1ba321afe8fbe8ea6dedeb7cda30f39300d245a5c1d30cbf88637ea4f4a984117e8baf588fc3f616ac3e858b89c91b9c9583dd6417ec6af76457ef457a84e75a01d98d86f57d1f80a456f9519c426c970d7b1037dba7599bd3340c8a51f2f9c1d178dc3d10cc666ae6c236003220c1b336f', '0', '2', '24-01-2018 16:48:30', '', 'Student location student too far', '24-01-2018 16:59:31', '1'),
(64, '30', '87', '2018-01-25', '08:00 am', '113', '08:00 am', '09:00 am', 'naved mirza khan', '7011712623', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219851', '77.0765284', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63ef9d2b324443a068d9ae32235af932698bf42d9b2ece4a24065e93e8d1e9bffdeb77c603f6a23039e06a99b2380dbc81f340ab7312ddf7ab0eef4483969a687c7938b6bc841fb2958d38b98d4172272145dafb2d11be54753082971282226d14c6c7f3e30b8a7a980b573f066de738140f8974d82e9e5a0dcf0aa1c423ecd956a9bad219a55e3887068caaa360ffb2dbb7ee0177b2dc47235a6c1c42f022f2cb46d839d350fd46e6fdf85bb818ed31d12b6e0d925ad16a27b851cecfbbe6dcda794e53f27483e76ac6d1ee5694f0d62c3d0dd3a6b899d0a787f0cad47016524d47545b80462d1086b9a2f6aeabb9d61e78f6939cab8e2a9fb368031c0f87ef89ec5a9853d0cad971905d18eb49ff2b32a84edc2bef8b116876003a15dbbd5061256e87674f648830ffb716f13cea162a5e99598da669f361a73c2838a38a12b684c1b58cc779222a69b0a4bb3552de4e276ae4091a1b8924d77056c4d2d606c96af5b6cd1959b63b62f82175f26032fbd0f8151829fdbed2814a067bcee911fcc7cae6b8e153a20f1778c8f02c2c0809b0605328bcb96489d81d0103c86f3e4a858b82eef0cabad161e3adfeddf0b71156644619c66cefdacbaf7701e86c8fff2c9a295a737b720aa0241f45842fb1203961bcc47b3ab466a44d7602563f3b427cbfe92740568736fde617027dc46fe93fd63f4e3782e2657a2cf5bc1598703dad05afb996c06f484306ccd89c92ee6fdf691d25becbdebe246e91ffa5a6458f2cfaff035807b2069740cc4c9985a0113ed42e38cc139ea73960299dd4bf5146c9f23da1edf48787597ef38a00ada5ef6a99f6f38456aff9383ced15cd418b8d97f532873eada1958739e269d47da34f46e0bb278e54c60109722dd8b1a390a027747d46c876199155c380bca9257189d32917e69181ea5963ca845f6d29b7ba195c7df859e111d721736796b10f3bad6cf6454ddc8d43bcc627a3c45c7767371efd180bbe12b56a553a5ca5771404bb9bdd7ae19d5bd81bf0f22e23aa5e86423bf902fe8554188af7c3ce530312efa73f7ebf0d9e657de90cacb0a4abf0e165d5d84b1b870f87218995d9151ff32f2b35b8790cc89ab4798523ae99fefcbd97', '1', '1', '24-01-2018 17:11:45', '', '', '24-01-2018 17:12:09', '1'),
(65, '30', '87', '2018-01-24', '05:00 am', '114', '05:00 am', '06:00 am', 'kallukhokla bakchod naved', '7011712623', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219771', '77.0765247', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee3744407fbc31bbde79ce18aa4b344754a82770a966222fb7983c2978f03e52b77d1bcb11d1c117fa9614968246cb16ef8b74e0259f95640b8e70d0b4adba9ab7fb7661a3441af8f308f0b95b535b91c5c3c61fb196482dc23a643638bd1665ea6ec3e8dbbeffb0f867fc997f2b55007a51cf03fd42d22d52a22f0ab65f7d6107b15c4f6213fe06806cd27776f974a4a745ec2d31ba0338e463bf9aa0d410ddb5f31bdaa48853530021dce475095bfed940f14c60ab0f20e9b1294b19fe82f16fa217b2eeef85e0e9a03f5618387acba9d1fd0e7d77bf6a8ea755281feed2a14978f73b241de1fe7cd1ed271bfa39dd2b0081fbc2050e25763c6f854bc130d8edf536dcee88ae5f95df3421cb2cb25d58ff44f10f23c5b7da9c9b9239375b1e729a1b92e4758e66380c670f1951b5fbce632b67e93af341fdcb8f28a35647f02e7253c92024fb88c6670c938505cd7a4761fbf6cccf4167c5fa2e091046b9b04859317cce53183015b5d84bfe41b28cfe3dca2310a6c152b063df36218a0902081121a924e39774c50da161c074b6c5a0f2dce52f86ea4094b9a0b1e1f1ca183669480b99eb1624f451c917b7cb5f0e091f6fff82ea9895465ff6099eb4a7fb4223066385b752c29eca69dc8b1acc65fec1a8eeac9f15d3591387cace7f36694932ff16ee6b5febf10c2305e470f3d72780b6a0067c5fbf30bb28cfb0e437401a93c522e246a88e7779f580ba0f4833bf819760e29e7bdfc73b0a03ea5a7b625b5e2c2c3451fb50a6639d55718946117a4723fa3d1e70ae04d06cdbdd9e1c1888453fb84d9bccea943b5f4c5fe714fdc8425a7c950cdc86cf0d7a07d6d1babf1bf82e53d5cb68b669fa09a37eacfc804f2451af022f67c24a73d877bd137dfbd6594dd70f2503afd27efd023965a5be6844868cf87e0e8292c8ec9d06623e6221649057c9f519b233d6c03076133015fa4bb81965b441aaf7fb5f117bdee3d911fea3c8de967dab1227d8585df70f45197882c9bc672d9f3699bf6866b8581ff6f740fbd8e221f497f5a227617bde300089e0dcf819f086f7e7e82f416c3c27a96410f16530501bde40af90d51fc6048b9cf1b4', '1', '1', '24-01-2018 17:15:23', '', '', '24-01-2018 17:16:02', '0'),
(67, '30', '87', '2018-01-24', '11:00 am', '116', '11:00 am', '12:00 pm', 'swetank demo', '7011712623', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219674', '77.0765162', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1af6bf6da782cc348d8165148cac527e08a013ef563edfe185e75a33974d04cf63076f66f5632cc75f274729fcb51558604b6356d80441b250f5ff1f12a1200b40838564639aa859b2d0b8a6276a8a9a9604b244ba0e85e71c45e898c7ee374440ca3b77c334a676bc16d4c9af294f11a66f532c4f46647aff13e23dba4eed0101e7ed354762fcda8fe1520532d47e10ab7c1961bf7887a7318d783855f74925e4df858589ecc5e75f10002d380b7d1c107169a077640f91eec6f067519f89629457a9ddd79afb2e1fc1108c1452a54577f1b27ec99ecef9df84d921d7eccc70294d6179d18a70204cff46c87264b7873fc2ec9acfc82c9967a683d8bd304c12e1e16facbb78563c9a4b73ad1d4f69138924b88b38bc44bd9865aaea2dc5cbc016e20cc195aab8040f97665f865ae57db95b6b5bdafc4c909368d639e459f06652fa9fb190f990b9f05a4a2741584ccac289c55d03df5d46226681ba27f8e66ad46d96881747c06114e5e0c872d5014199b03462ef7ea8b914767c82157c38acb6701a0f605e9fc66d88a6f850c2abf78461ad58a78baeccdd8ffc713bc63e48a769af97792daca9444a7d91ff1b7667f735ce71a5975de7c99dd45e64609c7d4acf55c7f44b179a88fca2546a809c953f78cf90ac293a89938f1514b7399a7e65e09052e7f1de84c13ff87359c512b9b5cc7af767f9a782362b7dc4fe5a6a738372b9752c8a2e21cd75aca28a5345788f0736716e80fea51c0693490f43baa75ea64f08bccc10351d1c53d7dca9c60a26a2d31a15b239f6fc023ba801ffec7d58fe79105b252505e4991712c1652b39ab37be4eb7b15b3e5b19155ad07ce2a1ad8c28b87b5c4a218ef418eed4b645191b3291f253a06d30a01bae00c323c2969befa9c9918aa84bc2f9dc9dbbe198c6ce22cb2522e904ddcc8bf0df301f71cb46f4e90631fa3a6d348649bee37dbd115240ef4c3ba87cf08c701350fbefc74f465691eab77236859f021069d882557c7c506138cccbaaf1e538a291fe96fb8332b612e53c6e2106b0416bd71626991b12afc9d1de95f4641d40a1c0a0ec9a4dc87c6d2fee340efa78a47633b3c1acde6723862f81b46fc22ffa2877a986133d4e19f455807cabcb9cbeb0d278ffb5ae7a42cc1fbbd84e47a1fa22c91376609e19739c08bf2b47392b1c96a764803c9bbacb46b66cdb09170c37e3cff63af5a9ef', '1', '1', '24-01-2018 17:58:50', '', '', '24-01-2018 17:59:03', '0'),
(69, '20', '9', '2018-01-25', '07:00 am', '119', '07:00 am', '08:00 am', 'Naved', '8802915021', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219692', '77.0765284', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a65371753166306664a2f222585a923dee8d1e6f05295bd844abc1b1a54b075c7dc79a7e8d999c331c7014dcbe74429ab185c6b9d503282625687f74c6938c1b6d226dd3dcf0fdeff07bc7275b46c5a75cb9f187e179ab409786ce7ba69f69a70349af9c346eff0c4803d0f8b51273159940e69cc2a6fdbbe84cceeee9ae1eb69e145eda9c3cf88b37a408281ec6a7f0f2c02a282528980181733ad47cb4734f9b77b81f6e7e56cc456336e072852f35d926197d1da2db2ce3618c5ea4a3ac217511469ccfbe8ab1acf9e8f568fd172eea980b5560cce01da505b055dd141ed6e2eb1e60159cdac0e9e4468055df00230f40c2a93628a3e0e09b22ecc894c74865ad7fb658b50d86eb2730c0bbf4a1425fffe2d250e7e1014db3d04361d090c57912ed6e722c37166aaceb85df516bc3cc21fa8506f3df709c78c75f31d6d4d7bc1605884aac0d0b8d17f6b91d7ce46e0273d4692420c85f09acb697cb11be7dc89f0c08612e360582feeacfaa1b4a3afa468e7b008baf113449fa9fcfa304a8b03878238543d3f612383d61a8d5399c1cc3131ff733cff3547c5cbb178d34b211b55ff93623a922d962d7f17cd9e813b22c2c86bd02c924d937a5b936633cb262d9b68262eb75202af6e3705a8d597ecb11566545342bc499ceac81b2edd25def19bb19970820c51cacfdb59ca5840aaa552886fabef155d57d3deb48eb28094f88e0d30e6d7d0a3926c0d31b492071ed580a94196fd0b5ba27b9ebcc06d64107fee2a6220f1660e78930d3e6e05d4619c4e5a358eddbce05c938b676ffac53d5b5785786c15e56bd01129996ea5acd432a4cfd2ae1d45a0c45274662742518946728ba683f10a875243dd548ae03085b81d648f45f69b74e916d7ba25168b9db789bfbaaa8e3cb9f6a48afaa8acbeb3635f6262be52e1c2d51b333a1009a06767cb3368c4d91fa979762cc56871d695b287365b889a8205474f9a9670cf2fe5e18d098596b9761b51f047389ce725ba7219a2202598161c939e4199c9e4b6008e935dbeffa4694680f55be1724f615701cc15d2e2016736561947652d84a886b7666f1f47f2add3ff6afc35623c97f69679c0d699d7ea740849eb1c0b1616dd6e6848f0e0d0f86a5e15dd916bc993863e1e4b50c4051b636dc2e965cab88a5a6f0fe7a4137a0ab9d63afff11409671e49fceb44996f1fff777c8925eb571cfae91738cc488acf753ead9795fc17368268596fa664c8311e6c991cb7d4eac46c34e21e2094464e367bdf22fb999a22b2422c75ee3b3bab5f37d830c41afaf1f689b34a2fac57e34fedf1859de985b8b553860b25fe5f8da199565d6af24a8b49e96709400fae6fad5110776016c0b0df', '1', '1', '25-01-2018 11:22:42', '', '', '25-01-2018 11:22:47', '0'),
(70, '30', '24', '2018-02-8', '09:00 am', '8', '09:00 am', '10:00 am', 'shantanu pathaj', '7011712623', '20021006, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219341', '77.076588', '0', '0b0f4267ffe05d32007d9fe41dd23fd41cea70fd5b6fea25252c20f4883cd84b8dc1150664fc4afe785308bbf41d8cd27a7be0452701a89a4e8d633aa327e32af1b1c712fa7bb0ed3ee586c65b54b9ea7b0dccedc8afe0666d19c0a6813378f4df3bb50aee25ab33de912eacf5fafbd2e4d3334ba2f0521ff7a557f74bfe20c0c9e5c15ee3b29a9663e0ef83423cea017db49154d4b8330e2d5c7f593aafca4940499316f95c303e5fec1f578eda53a0148bb3a5fed6019dc2662bf8150138ec1af6d50cc516d2ec47d471446ba1dc851dadda10835cfac172cf3ff270d0915b974b9a22ba5bcde460e3722a1d3a4a4d2c9adbd95220c809dac0cdc764614588adfc21286c6944e2e1c38a6a4aa1c9f919d52c682fcc8e9431e18c865480ed1e3ed87ed5de2531b81c41a96b6f8acf24bf8a03da73acc08e31078362b901d65aba4221f4c23d3c7400d432a93a6cee5254f51f85c24d5eeb745c27b38d67348cfe17378e40b8a29f0395d2748d87ad456906f8502d3629edabb7cf81c20750a9aa7ea52ace19906980332eb0583796a114e97946694c88a0c102625b7d0a1967e32fb924da9293aba611271e4a5d03193aeb1ee8d3d2f9afa2870f6d7b77b8dc66e5ffb5efd7c97c83e9290374bfc7cb68f217837273d0e97dc539909a9277a1d300abbee39bbf820e8d6c5f1666960aa51f62a77adb99ba59d4641a6feddc79652d352f5a9b27764f3ded780ef8034aeb7911f3a0095f1276535c8d57768aa75c03fdeb8cfdbd31bb3a79e0a47f651e6f27bd8767ee7eda824c1712380f220e384efd6e09c9bc3509abd433b7fb43e2a0ab316baac53b58fd76947971cfa85175c70f5a064bbfb3ccac26c4837209508c3c8cb69d359ced3a1ed2e79193b63f325f9d467d4655d1255268e6efd29ac2c3d993c7be91f013275217d2dbd7ce66a1c0be7ed47196c98c62c091aa20da5279ea5666e39cdd33d5ff531c067758cf73e0f5ca00c4c09631ab48cc61886a1b0357113d00e4fdc7d326c0bbba5743c8ecb9e090f798918fd27ab427594f7f09953d83c800e21c86f54023cf3a65536efa581e733293534a59b40c1df2cdf6c266dddf205b9696169d921288f62042ec2af93a0d849037ee90647c6e5d24aa98d6e9024418b8aefeade77cf3317d449d432c4b5ece89d82fda1b1bfb29543ea6411b8ff94b001c230e78859b01c1ad3ae7de6cc66a9032fe0be407c814905b262a8264d3df920d981ec70d4442abb0aca9cac5fbe074458b8b9d5dd5971bdf201d32d525122114ac6186b3e4a50cabb6355a411f41121a00de9f61c990b541268ca666f78a1fb548b383ed9aac07ed90e27dbe3e18c9566c515b6efcecea624952b889b047aad77ea54635099ca29b44dbbf1a8d650beb0472e0c637851269533063b3f41ed5311e09149d6db08bcbda7c3a091c39be7f3db9828ebbcadf6f5e303c2115c06178591a2ce6b1466d4cd42c59300d55b345f5f18dfe7d8f1abd9b256d8dea670d335fa62c6b63971f24276108ecee1d39e537631a12874258ebdde7982bd5bbc288d373d3f8499146bc5ab93fe30fbb9836cb008fdebf022bfdc4853f90635996b49e5388cd22eb3fa92bdeca5901978bcff764433deb5094639a8e9eefdaf1c38645d9bbdf9e13991f328b284e96809ea9088f93d9518a2aca2465ed3ea37ebc410cb2948f441ed48eb772cad415c705bf87b6333e01c81dca1cea621132e2c9b52753a79f8a617a74673354298568915bbbf282a18ebfa735bf029ec52f0ef435b0cffb5980d668cf42cb48f64cca00ed0b5bb141f1bc8efef6b5cdeba727398a1be06e3ccc5a0183f7432bbd4d0e389aa56a7d78db36fb2967', '1', '1', '30-01-2018 16:59:02', '', '', '08-02-2018 13:17:34', '1'),
(71, '30', '9', '2018-02-2', '08:00 pm', '132', '08:00 pm', '09:00 pm', 'swetank', '7011712623', 'Janakpuri, New Delhi, Delhi, India', '28.621899', '77.0878385', '0', 'de2db97c353c68fdad6a28165ac885d480aa1274b89920d907e98ac1d10a236c48fd10f4d289999ccfdd61746a61a31b893edb42ad7bf90b50dc92af3fdbd264607c9cfdd53c450f939cd5516ccb1b8b1e2b3d4122a1d43cd8921629a5fff754d7117c299e492205253902ce61ac2a052de4608c78579f48d3336cc2d9189f416ea5c7052a9817a5564b1b13c55c3758c7d808d4f16160720da8ddbd6879bf8a5e6cc95a561105619a6aa9f8663b46ea39d3f7b06a28e889cb519413ad90321282041c2ae757347fdde2f629ff63828a548341b035b97be9a559019931497175238332d1df244bee06f13846ce0977405ce6976da77ba52627b9254da434bb3f50359bcbb91b4ac70033f3214d6d31c24e33af7b53ded8e703f2ea13fb195fcef71a072be4515f8e3dc8b248321a8e6e638fb373d3f0b5f5036152a2f2ada7619f152ebea93517eeb9329fc1c2440837b91550613b2d24468ca1c80a50e314bf344c8faecb19a6af53caafff8c2d0c51ac281ce5de2406e915b696d4dd69bce4e6626b4f712686787ee9f1f3fa60c343ffef659011d54e0b6a438b921abc379a962bce96e3763543d0e05044d25bd15368008972e304a5c842b1242215044edb843eee7b836b4d0022523036b950721a55abc2acec57c5ef6866cdb64d09744762586247ded049793f14618c92196346351b3ef67c71e1e66c821589b395c965e54025c6ddd4fc6849e1ef07d51f4137d3deee115fcb38523190dd577dd00f676ea7295ce86e9f3a678fbbcbf9bed40506008767505d4be102af7d4cdf7ccc2afbba2114354d0edcee92714ec541459dfda520fb8afcb0492f4d6908a2816bb0b852a67a96bfbf632ef15776fa9c83398528d2f964c1d2011e841e16a00012600dd286b24e536fc8e22c35fbb5fda79922202b58f221f152e5f904b73c86822d2a6e7c218738ab8d367075e4f76ed4b9b31db9dc2bffac9c3b5157ddcf1b94d6588c47916968746221249e2855984dad4b7b0ad1863aa6fa88b8774ff6740f6f373cf02a7890a58ea42e55549e8a34d285c6e4a022454155f981c89f108484b20f0f3df99d32fbd8b2e1d77bbce8b80f0e88de61566b6ab5623b4b57200b0cb9dbe404b941cd4b9943ddeb15fd060e9e1bfffac4de7bbcc658aba870450dc6de77f6ad60f759f8c7f6015b6d97b72fbd4ed9fa9bdef42782f10fd46f3a172c6b13b79f927b99ca403ffe8d8a0e21e4e05c76900bcf5ddc3b38e84822b718f28e90a155b344933fe2fa852bd3de0d5da6768e9b7255c6a585f583533810f120fb5485cda7617c2dc11de0c08fda7ca57d108bc4b7e42c83d2be58e66321d2bc99e8d4fb73d5a1dfb0c14cceee3d98a6949caf028e1aabde0b86853c97864a554a8d3f59c75723025af0f0498fa8e50f6f06d32380205b16fef8d253447777780fa7eb2f36e3eb2d20dc7a21e1e8f018cad0ba53aea1e5b6f7e3831e2f3a5f107860905f547963c14d024dc297e9bbcdbc6aa8f21206200037ac412c788e20714fe531e84473cb57dce7120d302dc4340527dd6d84a83bb790610383df5ae40eeb5bacdcc7cf58e14c425f2971644e6887dcdb6d1247bde8ca7bce3a7cb56a8d6a17114d091671072652ccfc283e99f6161742ebe5e79f946cf8b11e64c76720f77b67a33d19ad71108d1ceed1b6426438a98826e0544d96e711e283437f0800f6a68556c4a432a7f4ab54c458202ebc341db5a335d436dcb9ac5c247e9cbd90ea', '1', '1', '02-02-2018 17:31:19', '', '', '03-02-2018 12:35:36', '0'),
(72, '30', '9', '2018-02-6', '08:00 pm', '132', '08:00 pm', '09:00 pm', 'swetank', '7011712623', 'Tilak Nagar, New Delhi, Delhi 110018, India', '28.6390693', '77.0867741', '0', '889e2461470fc651cec63d8845ab6adf921f0cf22b71c00fbd825c569543ec57e6c8f6a1bf2bbeee5bc495ff534607c3f3c8b3d8b0675d4ad3aa762bd8d60d949df1d7a12d2e74051c9c16c86b856c0076dd420f6e81580abbc5b1715b0d325f245f415be25711dcc9ac4da979a34eb35160df498c10f74ef06a5d02e7fe765d28322fd04476154c76835b38f630ae93382dfb6f3468dbcc65cc19de934df5ad246d63b1ce69068e52c41f5abd7f3acfd30e3b98f8b9d4a2a0ca7f92027757a362cfca8bc47fc84ff60f407d7caa91a17ee146c1737365a01cc1535eb7b35f3ea62798571a361e7e7798860ceb02824bdada8c54f797d1e6885be678db661a8b31ff81e6e55183679cb1d3b6d7ffd07cb41edfe1f01cb6469c41a43baa8126822b301d12a0877f9822bc50692e2ee2b2990bc95d18eb24d7cc6c40979cc3a606d3a213f8e499c9b5e44807a21330fab7d704ae1bb7515ea98441a4288511cdd10f313e3acbc80d9add49852248a64dc5e560845a87abe63927983da85f76b18e93e69ca3ddf5bb0df24c408ad798388da728cdd996c785f5d71d149f6454a7a6593ed023c35e1afa5cdf92208ae9c02717e35233419ab8cd949c5f5eb72a19286bfecb8a29cf824ca5a2a5a91bc494518ab927763e984f58d38c9fbcb0a29e52bb5b02a68e7b5171ffb9eb74881b3c3492f1d21bf412f0ae083d0beb0a96ea4ba3ff2b53d48a2f00791b14c7ab2532eb2f3614893b29868ef48f6491fe54dd729753a1b66d8aa3edb83481a4fdbf7ed12d3c49e5c499a0b517482e067e83af622628a7c8b4e1510a67394186002a84387f4201f5d88ace8a3d70a3ff2a7e92d23ad9621c8a4939c6f0fa4d221d78e0445a5cbdf1c520b2623ead4e688ee2541ec1247ccc7ce9bef954d76ce80c01fcf46da462d8fb1986e2330ea5bcdd0e5379e0fc1f7be56f1f3eb97015480dc92593bd139d5828aafd1214c4948598c7f8875b7c65ad6b3f18aa5e42c2ed1b66fcfccffba3c43ace475f5a4bbba2f70931cac00aed472c5259d1871e4a6adf9bfc613d5e2dc540965a63b5c6c8a8b7145d9f3fc6e9c0de67d81d934e3dae6679b428e6c85d0917c955e0f837005629696ab4e7b55cce80f2645e6eed3b93a7d58e491c3828f79531c7bf5c4e2b08c67c91591f04e931ced4a2cbb2847fffe4e84c1abf037d8ab6fbe2376328e01fb986838bc4f36fca945857d30420fc2cada606f4d376320e54fc9c3864b903441975a9b8595151fa30c5b356ffa66a83d3989c5848777b0a99e58c81bd23ca4edfa97c06c00732ddf4c4a8c7eff657733ea42431f17ebd8c32f5f8fed29761db0e28a5a0e6d2c00986b4d490f7c7ac466ea8afaac5d46a8e2529f75acf268cd80c11e86bb0cdf20029aa38c62458b6f3333a200d6d50b344cb7c2adcf19110de51b9c47d334a41e0d7a00d99a776e34430fd34c163bbcfb0d740224d71edc57ee2c0694986b30ac0f77c630669807c06404bfd7100780b606de433fe0bd129927018c558a90bf43591bf4036669780b40ad0ade8c9701c12d35f781dbd759e58ecd1ce08', '1', '1', '06-02-2018 13:08:13', '', '', '09-02-2018 14:26:51', '1'),
(73, '30', '9', '2018-02-10', '04:00 pm', '135', '04:00 pm', '05:00 pm', 'swetank', '7011712623', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219668', '77.07652', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a65371753166306664a2f222585a923dee8d1e6f05295bd844abc1b1a54b075c7dc79a7e8d999c331c7014dcbe74429ab185c6b9d503282625687f74c6938c1b6d226dd3dcf0fdeff07bc7275b46c5a75cb9f187e179ab409786ce7ba69f69a70060ca14cc8bb0e736d79cd1d4a219679a333489cf8da323b192fd580da0154cd0c3e614eded8f5b5671d82dcd4224756b8cbcbcb0e5ee0e887c7a27ac95de267d637a4de8ee6dd1c2afea71f19d1dd5f62b620cf7677a1502f947506f9af1cb0f2212551d2423c920b209bcc5eb101a481a6873317f61b8539f05af3f32570927130c2d14788ac6633acf4b61a5c72f19e685f5f1d504e4ed44c13f6249c9c19f951fe06c3213efad169bf67c9e1271987d41704fa7300727f4634bdb6722c80656b2dbc7ee72d14d84c0f92557a5436f2d4ee463207fdbea154a144dde2e9128105fe19cbb9acbaf7e8f07c0ae03e31851de1908299bc9bea7a9015981161b62b5d0c72c62f5dc2d0b863c6de4a235a0b4caeaed968b66bb6d393c178f04429df3af2f8f987cf7e2134744164fa12dfec9258cc82b43ff5383efe0b60dcf71973e8b0e6549275f5787dffb1edffd7910d2f3b9de87963b7160514f3319e4bdef08b5d7c46a5784372e627cfc901f84ddbb6c0b947dd6663554f612bb78c357bb0e20edc3462f3a823629e0a79c37726d5c66a5561419b862f4a0a9d6e0ad5abae5578e99bd4b3818ef704858adce4b9f2afe83b7dd32cb26cfa7fd5f8057858020b4eef7c887590e4d612420eaf64abb3b38563f3d3c50c1c90adab591da2ca0f2c23458aba16c721e8eb715121cc45a362d4ac7058413ec2899fcd40444d07a851f11d1a8821ae68030ffca8b289d6d27c73da318e9d2d65a759f340e8ac57efae977e2bae2da63727038fa13d4bf97e176ba540329058541187227f24f1180bb768d6a4a224402d6422cc431b70601b29e2a3977a11dbb56541c5b8c4208b45fa8612fb0e5a360b66b9d27855a0a21da686b4faba82d854b6c72c14de6d72a6d55f7a2c8575f52bcc8e845f5a907c5d7e34340675e0f08f6efcb76d6e4b2df0ec336ab635d4a4ae514fa5a9df0f9eeb7e2ab9752df41febc75a4c262c58324de3c183281aa5f5206689ea34c9281425c94dabd77218f1d859b7d549f51561f8069d01fda157c3a814e4d2ace4eafea82f57978dec30ad09a44dccfef86d916279800fd4d8e2a8f0891500a8a0162dba03996f03042ad8c8bb7762e2428783d3a33c0fece344a19f61183e368e05dac4d6e3096e61a2580c57016763dcc8691c931e5061eb8c2d14a00e3f22d614f5', '0', '0', '09-02-2018 14:26:30', '', '', '', '1'),
(74, '2', '194', '2018-02-12', '05:00 pm', '155', '05:00 pm', '06:00 pm', 'ashish sir', '9999575155', 'Janakpuri, New Delhi, Delhi, India', '28.621899', '77.0878385', '0', 'de2db97c353c68fdad6a28165ac885d480aa1274b89920d907e98ac1d10a236c48fd10f4d289999ccfdd61746a61a31b893edb42ad7bf90b50dc92af3fdbd264bf298165930e830d49a90c9e35a51ff0a9c72e58015d43c73a491334158669160d1dba2b23da839e6e50b0d149e6c8067d28152a27f57bd68ece9f0117239fb1f0879a6630258f6c647fc4c78f00eb74f2eaa8480b5dff2988c039693626fae623bb900688525dc82cfc2456902cd9191b9da97d74e09688dd48df4269c7a48de7e091ae71d933ab8b8037439f84e558b289a523698c3377bce92ee1de148147a9a55e5ec209c0c635f85e752806fd77d047756a2cf0d428865f629b75ccc27ec63a56ff754c40b3b0a2c6a1beb5c7733ef4833b2fad6bce46a4940dbc42b4f218138deaa6e7f23a8e90596c30159e801b628b1c90366fcb4284c3714828182430b6ed1be5fd227e1b654c85e17d012dae62b46c224c77fcb20065f710b4ec81edc950caa92b6d8b4f2ef8c06f422628cc7205c7837ea5e35f4f452f17294446b584c16254ba5892f01c44a923555f49e6c9ac337d9a7101c43767330aa4232f52e2f67d84f9c6b89d6d0349dc9084b1d0940e8c70151c406470f046ebfed312442d4f0ed756b250fa770e215be8f7039c2887b5d510dee29384dcc6de4af8d90d557461963791c863590dc7d1b71016229497a50a2bb9ce46d612f5ffaf6da779cf2c4b00423e05e8682c1a834393de94c1ec1375b19d24a1954093053db14721e67838e29c18812c2bdba9da57a452e2821421cc7bd9bacbbe167900fe0caf87a4c7c6a2e088d69c0271202769eb6002723ea8fc83288b4d788359af7fadfbe1f1e7c66bcc99b18370973824de831eb82f87f6489d4d8ceb71336d1f092501d207b29346f0890603f2060b922bd404f9a5a1afb83a6283dcc7a8edd30acf59a6e39b5fb17f7e66ff961e8921edc12059ce559496d6ad71af7fab5432d02d5c86eb63999b870771b3b0fd210c454cbbafc8ba4decaa84cffd5a5a2b46e098ff8f423e09019eef8b65a8d75ca40e9b251f1c0c5e07736071ca3fe928d52b56fd565b54c7f65b1bc58e65453b4f2e0305fc054f518937328dbe667ce80e2bd3c0de2e8290f47a1e80c784dbae48938567f8a58a07d26f950b046314520cde5368c1174327f6f60942d9b519a0921ef2721bb57a2497d87542a40060750205456aa0e662b40299029ead88a9b1a48063b67c0f509887bd7d15292ccb9fba98b728', '0', '0', '10-02-2018 17:25:17', '', '', '', '1'),
(75, '6', '24', '2018-02-15', '09:00 am', '8', '09:00 am', '10:00 am', 'Rahul', '8700648727', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219901', '77.076525', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a4eb017711f799d7773893cc9f6e4d43afaf13d59a99ca23c2496985bd5cd73feca0470c20029a0a720800b8b39270739bf3cbc33a24d5f9e282048703c6f6fb279b1226258663b5bccb5a9bc23b13b8e0780fdb8661ef0e7c5c4874fd5980ef8ad1bc061832e39c1b73d5e8be7917719978b1fb744c9a765f6887d613cb945cccdd61806baae4eaa01c8361367d0f781061f49c6ca7737ecdd3ab27d342462df144db20c54d6185b92ab82418a68c3f54df59b690462ecf13e1cf0f6574ec20016d2e4d24df9621dda48ee72ec6f9bc4ca95d9cfa5fe2a14f107166e20e531d36e80f1c836baa86aa5183727f574b92c56e992ab11fa72de35a1617f825fac177bdaf503f6d8971e4c3eb29fbbff908c48a411bdd05ea515dac5f24b8b23134b7e506afa07ef819402568f01d5eaa1a029d8d66d098857a1ef80f7d2386b342cb2cc609d2f559b4734a7c5b51150d57a662460e1521492258fef1d3f4dadf0d8759732ba58de4b4a84a73de82c81762173bf48aae6f99bf079cbb2f8b45b9d7c0d47766e0cac01def30a22bce42630cd7c212d0262d736d70b8c0e5ee4d59f432f2e095b03368056ce9a2e9a12899d915f066deeee773c3ca63e1e0ec3bc4333f4b5215ca07dfcec99a2683b825ef49a4bc742a2552e161bfa9fb34b76459cb4a94b458e15b021ad7b93622facf34e6c17af77cef396f9f5ec06fbf7ba9fce07d08d3be0aeb7f5d9f08798301f50b5a989948bdc8635188c2e5f4ee470290b99aa348bc56db4f0e3b12f354798359b2a461da93c48c4ba13f30de27b25abef38911e029890700642af95dd7b4f5fec2f553c4937f5b85c46379d82e16f8b03b5b285100cf0cfe55e587a4323d9924e8d26a9e8e40cdab14040d51e21f4e5a2e8028947cb0e196be1bc592631d438d9c721fc7265b809c621ea5d4f14e15c3dc81b9592ccf8fefc2017dcbf3622fe58123d2b2a398692689f661b899fd3e00ea922ba77ac2445d964a852d1abb7d27a1c4da564be5c505a5f1013d9b2adb666a5877f9ae6c3551b4851903f6daf6f47bfd1f147d8cfdc1f6bb9a419f724528bb7', '1', '1', '11-02-2018 13:05:51', '', '', '11-02-2018 13:06:07', '0'),
(76, '6', '236', '2018-02-14', '03:00 pm', '193', '03:00 pm', '04:00 pm', 'Rahul', '8700648727', '20021006, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219372', '77.0765688', '0', '0b0f4267ffe05d32007d9fe41dd23fd41cea70fd5b6fea25252c20f4883cd84b8dc1150664fc4afe785308bbf41d8cd27a7be0452701a89a4e8d633aa327e32af1b1c712fa7bb0ed3ee586c65b54b9ea7b0dccedc8afe0666d19c0a6813378f4e97e7fa1f602284045808cd6f4e0507f2d37c3ddfaa5baa70169e7c760fb6ecf0d02f9f0b010e3858bb27ac74e8bd621815f275bb3f1b4f4381c5734827b4010eb0bb68c31c6f14552da09734860a72e3f421fc9e414d93817ceff768a965f22ae13b87a3ebe34f599f558f402ed5ba2698cea293647508d12ce7a05518a9ac5acf38aa3c6a1882cf94a886f59c49f6932fa9ced657bfe11a0206b7ea7dac7dd0045c52fe365585aabee16c4badf4eebf358a20301990ff88ac490c9c47befebbf9f8f50f2f796e507ea05bb8741ffe6e987d4a573061e63dd48fd9fda2cdc1ca763cc3b029e2fcc8af7282b55eae2c0e16fef8cd957d58992db919ff33f26eb5fcc4034156bbb5a80cc1e09a2f2f471f7b664e5b3e448e42ae4c453a3743649ac4857131f71dcc8251ae7ce1f45e75b507a9cb2b244e00b154536592df1e1d647a98863d573f8eca278c89b93be8449d981d29012435538891570d81452baa3f453ded343e02667ef76215aa97732c5189ad43ea442334f56ce3c66d37bcc164db0490272aa630ad262bd1a12a265e57e97612784393bac5e11dba192cfe3d0998ad70a6daf2bf221033e2da1ed308b26ab4369f0b3906d758b6bb2e2ed9bfb1f9e9ff7357d19e9826c6959568be866ecaa59ab4fb97928418a8b1195bdf8b2771b93217fec8ae106cd7805258955e776b9476840e931bb23bfe800f7f4b5a74ce9d108942d4e9ee8af13ae05bc866184615e960f502d9d6cd5564e9eb31b87eb819024340b999923a55c5448844555466188d2a8e4904029a4504a54b1aa1cb5a02047e5448ab7ce1e5db79477cc713ba43c4dad8d9fcb3850368f791290f36d93639daa1bf950e1cd29fd4c917756eaff7333ebbef03f792d640059eb7a15a8862829c22c3fcc7040eb0a5ec1715fc982669b8607e804586aa3f5ae26fd7916e1e89cce2e2601356e8f219be10468e40ef01e2caf89cb1cada41e7ffd79c21929293250b763fed28eeaa02e9d6ef854ca4d76f450bf0a2b6c90795a5938c85bc568bc57411cc459cca998c372717854fad2de8c7a06ec12a7fcf50973e42ff5b17140074de94cf49a18669c3540b14d4d8ff48501d8ad3624bd4d620a1ad3cea7683285e1d72b61b2251b9702b9877a524d2be188208c5c22602c3d53c4c24d4de46c4f55cb8c0418302fa404bf3a160c29d1b57a81a95209ad478e5b03fc0a6331d6cce059e050bf11bd86b2b4ababa7f31e23275c990b0f27d53c2f7ccc61fb569fbf039b4b2c2df077a23fba94e959ce86ccb6198089769df333fa4937', '0', '0', '14-02-2018 15:33:17', '', '', '', '1'),
(77, '42', '176', '2018-02-21', '04:00 pm', '202', '04:00 pm', '05:00 pm', 'deeksha', '9004988285', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.621964', '77.0765547', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0a0c9810a5904d6cac4b677c46e336a9e22b4253e91a8f0d8ec2c05a4aa86cd3125f173fe1a5381112428205f2e2a9924a34ca6914e196313a83e19ff11e8150a4133a681f7bdbb7837e655d3cd4fc1b3605a6bfcf1f9465f30b1e326e8491835e2bb158f63d5b9701f6f64443594ddc42be9e9c03d1010db47ddcbc96d1409d7568a0ef82af6f65805502a4732997f50c036008ef1410e4750dbb166567e2df456e3e2c85b1d24a74e9debe7a22eb8e09169f58380c4a7e04c8ffcd37497058f64fdedd90a67cb8a3ea6851c396bee4137b8d8f27c2607434091a717310b6784e503e53446a773ed8aca1a4053fadb3108fe3bc22743f9171232d36bd18ad30ed3678b5a2361416988def9a5822fa58aa6d1825da3285b35287e02d5aea1498ebf3d062e65fff8a7109143fbb649044d8a95f7c8326d68451eabbca6a2504f9d32e460ba41ca5be743e3948be97f201e53f00d61cda6c23d46e4cc309394efc2c72a074d9c641e43af376ac5acb4333aea5d6838aea767d5c5ab40af371ff4a771c4d5e2fcf076e767872e8e33c246108a0751eaf3389774db13be1290f5b6d4ccff8952926e80e9d506f2faa1ba95d5dbbd00ad31b85d08fd8088c4d428c4b78c391bb242ab30cdde20c8d32221d5b13cbbac8a6ebe5e7282b4c8e54f6d1377185f3dfa7aa1dfe99293c8dfc81c892bb328327ed4fbb29b71994e01075142a8ddfa7e882f923f9b45afbbbbdac81c019c34cb310fafff99dac2a7fe7c7995141e2889d27be83c3aa937e19ceb4da18abd645b50a783484715c4dd61a9a9fe434235f477879e5659713230e24c4ce91cab5c6f753a576f03883d8228bee37bbd6b112787342670e39f6d18bead49f462e6cd764c6c7d7da4e8c88b2ccff64bc57b80f5db4e8d3110dd541138c04356c13da742704d57aea10d9efcb2f4cf8fc3', '1', '1', '15-02-2018 14:52:03', '', '', '15-02-2018 14:54:21', '0');
INSERT INTO `book_session_demo` (`id`, `student_id`, `teacher_id`, `req_date`, `req_time`, `session_id`, `start_time`, `end_time`, `name`, `phone`, `address`, `student_lat`, `student_long`, `fees`, `all_data`, `approve`, `status`, `added_date`, `student_review`, `teacher_review`, `updated_date`, `is_view`) VALUES
(78, '25', '176', '2018-02-15', '03:00 pm', '204', '03:00 pm', '04:00 pm', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219573', '77.0765518', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0a0c9810a5904d6cac4b677c46e336a9e22b4253e91a8f0d8ec2c05a4aa86cd31bb9a59ed14645afb20a32f6400a4881591043961cc4b031775113c478ec2e658c8278c6ba098d69adf3710c90551a1be3de3a73fa3e217b76dd7b660ecfb0df5d95c3b6f8c956fd8fbcc23f1ff82dfe9b5196aceed432abab5d001d8877519cdafe735894e7f9c18e8a6ecfffd3c7f1b98e69b224cf44ba051e1715a256b93f30e00e46a5e1c340cef589ce27955ab6fbdfffd033a7f0d6cc289cb76f618254578725ef508a370cb925cb88e0da17ebbf3073d2271828c8d1b8eb571267814f87af2c1eaa45606fbf174fc7a2039362e276ed92435528e36fa83b7095b74724a71e7196ac1bbc498196ae6e3d1cf5da7b5e4b4a94f7a163fcff9538a92babc7b3162ada7728906443166b55d61cf19b18a3683272b0f3e0fb8c0a203e5be9b9fbf6f9857f3521db5714f34334f4f2ba0df57904a75de2b8278a776f4bb9e572ee39ca6433249b29ff0d0fc994da13a5d4492c94eb588df662d94d6aee150841fc1207f9a9a15b9171bc3298de9b81727d853efe4bf776b7bb714259c02e72c4f75e461c78ee186a874b3e4e90ef645755d5e7607e30866bad3b96fed80db55ef7ef157b473b7dcffad91b4992725bee908c047f11087c4af16d2bb7ffc5622e64710c5829b6c89edff4123c8a2bf4832f2941614219ed7463136f42afd2387d28103f00f8de1d78e651919c8ff16eb45076780921c9e0ac3da6de991f395ea0fc06d293b6579adabb99b2db4d799153031f5ffd21b8729989f0e4eb5d93e615a3b3a7fcc132f81e5d0668932a62f1c71bf34128471b3899fb0811b7b6fdcf7ff9492703600ac6d85a748b87cc60401388f38e12485f1d25257a65a6b2027cd7c440492445846975ec8592ba60b311c1d098efdc65bafa3e49137f174dfd786f3', '1', '1', '15-02-2018 15:07:11', '', '', '15-02-2018 15:07:57', '1'),
(79, '25', '176', '2018-02-21', '04:00 pm', '202', '04:00 pm', '05:00 pm', 'vikas', '9990731159', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219689', '77.0765487', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0a0c9810a5904d6cac4b677c46e336a9e22b4253e91a8f0d8ec2c05a4aa86cd3125f173fe1a5381112428205f2e2a9924a34ca6914e196313a83e19ff11e8150a4133a681f7bdbb7837e655d3cd4fc1b3605a6bfcf1f9465f30b1e326e8491835e2bb158f63d5b9701f6f64443594ddc42be9e9c03d1010db47ddcbc96d1409d7568a0ef82af6f65805502a4732997f50c036008ef1410e4750dbb166567e2df456e3e2c85b1d24a74e9debe7a22eb8e09169f58380c4a7e04c8ffcd37497058f64fdedd90a67cb8a3ea6851c396bee4137b8d8f27c2607434091a717310b6784e503e53446a773ed8aca1a4053fadb3108fe3bc22743f9171232d36bd18ad30ed3678b5a2361416988def9a5822fa58a0fa7a7a4ac3f116ae5f62bd78117e7d7b37fd12f7dbe260a6ad6ce25b97c36563a016d90454d18275683843921163ce157d6a7d793430266976d704b1858f11a69faa1fe6a05383f5d6210c401665c55ae578db0749e8def401e17b6dbb82dbc4aaeb748bd412168a00ac58cab30378a26a87dc11f9a144b35cececcdc451f04a4cc6b459cfe3b77ef856e25dc8d291f2069f2fe6923310d210dc3654673fe700f79edd66211dcdf38bc52b6c872311f6ab5cd59669ee99e68bc2f0b5146c9b7a583b4e2d7afea9ca64c6a731e131e4c67dc7ff4f695b354f1a0f46e2cfb460d001e2026e4870028c8a3ba52d60c5d88aaa0f7a5096cc8ea827d2b78d9b44703d18a217c41ee2b07dec99c00f2166f8e3a4f7d922adb11db41f1500ff4e30b9cc1090e9cacbfcc41ad5ca8384df7f1a9a0c0544697e888c1ddbf7b521cceac3ba4ded6f62d97f1b0520c7f229f26549656420baf5cc79c583a0fceeae78bc88184cae96db0378072acf5b6199187cd82ea8689e33eb3c35d0911744cd5b21aa63170a16185b854a2fb6a46baf52bbca1', '1', '1', '15-02-2018 15:23:30', '', '', '15-02-2018 15:25:24', '1'),
(80, '42', '176', '2018-02-23', '03:00 pm', '190', '03:00 pm', '04:00 pm', 'ppp', '9004988285', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219705', '77.0765465', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0385cb9d93b787a4e2a324dbc78359c25b5142c767c5cfdf131fb4e2b6e5559130aa25811c43d1645060718e0ba2fa0dc80a630827e9afcde374f7e1fa95d5de0e2b8478dd3b3cfa1959034dab90711fba1a178c709bc373f69480e786d64884d0712d933aa9fd2ca64691008ea699ba57f434bed12e9aff5a824a24b25ffe5d346b4c9fcb0e3ea8ee71e883e70a4c3f414d417d542910dc98fe6dc2a221b51cdc512b899813f46bc19748befa1c04556dd9558d1841b0f15ea0c1586844f863c076f2c0c31f1f181c138d49f2565e7c06f84034a6dd36fdbc60a47068ab77cbf90e7bb2d0c0352c2b9e9e909362120b298c4b9f86701b7cc9e9dfc14a6e395506ec345990a80a010167d2a00b1ba81759b0ce85c52ca73dcc81cd6ece3eb0b20aa9b803cfe7be3d82aefdec5c70cedd0788256ba09635ab57dce6ea08877c091ef50b7de4e3f7dc2312f8922b2a177c8f001adba90657f4541c3fc685ef045418eddefde641cf52c1086efae907bf4d7ca7217f201b9a510bb7e5276041f790d45955ea2a5abe4e4523a5a5a4568b7f98832b621cc777bed9c37d31c5d21d834115c1db987f7c8bf07bf7ee973be495d11990739323ef9d77eddd574cdd08149300dd8990a4dbb3d74c57c0a52182268510738afe205e2bd0f4dae53dc6353afd9841d0fb6a452355d10d54ca2d48ca6b9544af719312ce6f4587ce3d1894cca951e30640fb4cf704fcc91588604730f48fe36c9c958268f79bf70261595a28f461855998f60ab26c23f5ec7f1c8354f8080db859dbb810665eff12aeb23dbd9922d3d5b184e7a16a924a2f64375547c6df9f9762e0bbe1827ddf516db0f9456878c9d34f17f57f17a379a20aeefe12cfcbc5e68a5c13444e0e03bc79f0d275493688670957bbd6c962f0ceeb161c1d0', '1', '1', '20-02-2018 14:02:28', '', '', '20-02-2018 14:03:45', '0'),
(81, '42', '176', '2018-02-28', '05:00 am', '183', '05:00 am', '06:00 am', 'uy', '9004988285', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219742', '77.0765462', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0385cb9d93b787a4e2a324dbc78359c2578401391d90493047d67383cc3c76386cbdd871fd6446c3558fb1586e44e82146b475bc58ecee8cb8c3487aa3f0e00b604b7877e1e22edf31fba51546bceddbe37e134d38c2069954b4a615b8b15a8fde3ceae506423046432f3fbb54d028b21086bfb7b2806c3160e8cae4f870dc1cb3ad3f78989c35ed6c0ec2368959e5fcafeba46057ad064caf928d30dc3eed6087f1dc6d4074ef63f9c0626c8f14e8b0fd013c9da1408846122bde61cd17dc42c2b5585f3521eb04882c69f4f04001d9ae7a3300f08078b9fd139e54c079b60fb4b2fd12896b61fd05ac2ef85ba43e71ddf43b3a6d4c6b797a246945ae6afc5a19bfcd9802e799fdf80740bbab8a9e0cedd4c1d8edccba78731525a357ab356ef9f61a1302b3dc95d59d67f1347293c2ee4b923e3935c08a6811f8574b4ffb050eb2237543d843be40f5d7539a2c5431bbf0f5f54f36f542e6415716d889f5682f7cebb2282f057f7a74af98a4e7d63d866f70c6e35f7d99f1492c210272982fa2b7af4f813680e2562a5ecda231b83d9e3527cb664aed528096c5b5130adb9c692125bb68d2f62501fe9535292962dfa404f7a256989ba3eca15c7bbe0c449264895fffb96691e160ffc68ebedf329daa1cbec04e9d5fa52ae534694cc421a5e66b4bc22add2f297d09e7cd1ede35a6f0f45903d8b2e6188e404dee14cc9f42d0df60895f7aa760255338bf0ce03e0922c6cdf07e7a3698d0af1ebed226175afa6b8e2e58d6cadb1e0cf6bb7849d0d49f3e967aa50597ef97d14de899be8ab9f88822d42cb6e691e37015821d5e5f27ca96e2ef018d24cd7eb3063755a80b7b6e8cba9b982cb562134c2a3294a82f8a30131631fd161fc90bec963a5f58fa74df78dc43ed21b80c30ab24dce4967d778ac9b64f36214ec7f33051dc7085dc14c', '1', '1', '26-02-2018 13:29:19', '', '', '28-02-2018 11:41:15', '1'),
(82, '42', '176', '2018-03-2', '09:00 am', '192', '09:00 am', '10:00 am', 'deeksha', '9004988285', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219504', '77.0765675', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0385cb9d93b787a4e2a324dbc78359c25b5142c767c5cfdf131fb4e2b6e555913e9a5c52f48514100f2b09dd81c748f31425c8b7dd8f0c3b40c54fb6c5f2afe072fa689adacb8157cbdcfdca2e2eda7ba50875396d6b5d56debd925276a066dbe5b3bb79726204e8e761a3177efd7aa8644dd604ffeb2df34cd27aa6e983dd630357bf1d9a639298aa1fe7c9d97b5fdcb6b466bd02a55071e9c8a0f0f3e3b2746e583969ff4f9186e53de0874af43c520ac5717328ae119340078f857ff7c20eae530e2f97b710819a9df7d6b70626a1daae1b4ece9b5be619a2216a68bb2bbf179ba9e656c35437c25f8edb04f49dac2704036509eba32a8fcc0479f5b27fa77955bbc7a6d650e1afad1dc518fea78a3d247c427a56689f1e8bc574de32d359cbb4cf02f6042170e84fb2b0aa43ece15ebcd29e5ebac97a9833269c9a2e4542ec345f516fd4591aac5fef5762ff41c603240a51d00298ea899a2b18a6fa87f320127295fa102fc5a59a566c99e2aed8e5616156231552a0677821818551d7c5df1ddac6091531812e0c017bdb632108dd2b98af796dbb5f45b05b2ebeacce98cf9bc911b84f0a3658a050b12b7dcaf5a37832f4ca4ccbcb01c942bb4e506707595a61e034ed47bb0924c0485f1c1b937e61180cbb625f8bfb6650529bf189c47b89d521cf9d094c4441bb4c862c7ff0816c59def6cf93f499309846602e8ac1bb4f873ceda023342e7fd96dd43581016537ba8b40377596b7e56fa7a79298f420057b778a75bc7c661bd2b2bdc9ba0add0d3456edd9fa8c149f9b37a795f0f02010cf3f2c24a9c4048f8a5a85dd462d1902de7fb446c962db4d096027f77b444042917659a00fed9e7dc8729a404c3893952474af44c06c62a85987716b36cec36c151082a1d9b23bba3246a063bf2fe', '1', '1', '28-02-2018 11:38:37', '', '', '28-02-2018 11:40:07', '1'),
(83, '124', '176', '2018-03-21', '09:00 am', '185', '09:00 am', '10:00 am', 'deeksha', '9039246053', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '28.6219614', '77.0765472', '0', 'cd301dc9b55d1996a95f4c542ab20adc218bf78fca0b60b9d77162b5873097d2a6a341d024dae9e9bb9e846f3ee6a83399958013a8c1ee8a308e5a2f6ee7253147c15041531f99311f90a17bc9b81b1a3d7d118aac72f4995b6a7aca3d3b559fa2667866e786e2037eee3029c669439e2b377f90e49a5061065f573271c8861d9e4c2fb4a3bb081981045b5eb0d0c5b0385cb9d93b787a4e2a324dbc78359c2578401391d90493047d67383cc3c76386cbdd871fd6446c3558fb1586e44e8214054e14fbc76e4da0156822f84bdcd6b44bab4067ef9cb4ffd90901d4592cd702945416671cec20fc83ca2787ba29c70917802a09b7df6fee530b906c58d8d53a05cc4c80675795aa704287930a0a77ea73113f8cadbf1b47d2b31cde4c08f4512a5fb722a462bf47c5f9be9c27dc0a3851f40592a17850dc42f842525dae112126424e988310adec08603faca7385baf08bba6bd691a486a192dbb53d3d573de4cf1fd52b25b944623a24634c89bc10603b05f1826c22ab8ef636dc1835f17f6eace40dee28ccda025772896d76f69a97423ebff05c5087b33d25810da72883a9825041b605e5d4b0de5484e57bd9b960dbde2d2d0ce838ce7182e7617a8a5860361e6930cf9bc04391a06a5c96d81143db5790c1bfa669f5e9054e110d6f4df3481ca019c0525ef306a91f39b3d7286a7e11726620a2512264419648b345304dc75423484f2d02da06b0a0a0c0c674f03a318651a13f6eef14c4f9b24118d701585b4a900f3d733158fcc50fc347585a51c72d2ac334d151b8dfacf967ad7c4bfa1e4c6840c1027542cae458f1864148cfe3af064d13983859e708c96a7098a676a55eb720712fb57d3fca688774824f30ef38ec69055865fc7cde584d5e67bdd303438cd2a24f18660c80f03c8a346621a734af6f8728524c07f286c0cba4cb5eb15d337c61d04ca89d5223041016e0d061c08af356b40e381c29b122837f83b7a5ce2fa7eeba3e126fc0d92451dd237c1d807f26efbe57a8ea82d20961f13e21911c73cab52c610d2ac532d0a87eeec8a8059b3639eff2cc2f888af66739f2ab7486943ad0e7623bb393bff71e28715510ae489fc8fae266d4a500657b1bc6b76ae2a97f6507e4188f10f361acf40bd73fc72290b604a6dedd03fadaa109da24dd9a09c2e7e690e8af3a70e66464c67f467506263f92d2c2d6c0999dc51bf0f5bf0a8e2c4b13cc6be1face294100e', '1', '1', '16-03-2018 11:48:58', '', '', '16-03-2018 11:50:06', '1');

-- --------------------------------------------------------

--
-- Table structure for table `centers`
--

CREATE TABLE `centers` (
  `id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `center_name` varchar(100) NOT NULL,
  `center_location` varchar(100) NOT NULL,
  `center_lat` varchar(25) NOT NULL,
  `priority` tinyint(4) NOT NULL DEFAULT '1',
  `center_long` varchar(25) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `certificates`
--

CREATE TABLE `certificates` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `teacher_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `issued` date DEFAULT NULL,
  `added` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` bigint(22) NOT NULL,
  `city` varchar(100) NOT NULL,
  `iso2` varchar(15) NOT NULL,
  `iso3` varchar(15) NOT NULL,
  `state_code` varchar(15) NOT NULL,
  `country_code` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `iso2`, `iso3`, `state_code`, `country_code`) VALUES
(1, 'Gurgaon', 'GGN', 'GGN', 'HR', 'IN'),
(2, 'DELHI', 'DL', 'DEL', 'DL', 'IN'),
(3, 'DELHI', 'NDLS', 'NDLS', 'DL', 'IN'),
(4, 'Noida', 'Noida', 'Noida', 'UP', 'IN'),
(5, 'Gurugram', 'GGN', 'GGN', 'HR', 'IN');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `stream` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'custom_subject.png',
  `teacher_id` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `category` varchar(255) NOT NULL,
  `course_type` int(11) NOT NULL COMMENT '1 for general ,2 for test prep',
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `stream`, `image`, `teacher_id`, `status`, `category`, `course_type`, `category_id`, `created`) VALUES
(1, 'I', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '2017-08-23 05:43:29'),
(2, 'II', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(3, 'III', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(4, 'IV', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(5, 'V', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(6, 'VI', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(7, 'VII', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(8, 'VIII', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(9, 'IX', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(10, 'X', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(11, 'XI', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(12, 'XII', 'custom_subject.png', 0, 1, 'Academic', 1, 1, '0000-00-00 00:00:00'),
(13, ' JEE', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(14, 'AIEEE', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(15, 'AIPMT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(16, 'MAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(17, 'CAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(18, 'XAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(19, 'XIMB', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(20, 'AIIMS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(21, 'BANK PO', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(22, 'UGC NET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(23, 'IAS/IPS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(24, 'B.Ed ', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(25, 'CLAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(26, 'BITSAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(27, 'GATE', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(28, 'GRE', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(29, 'IELTS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(30, 'TOEFL', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(31, 'GMAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(32, 'SAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(33, 'LSAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(34, 'SSC-CGL', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(35, 'SSC-CHSL', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(36, 'SSC-JE', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(37, 'JEE-MAINS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(38, 'JEE-ADVANCED', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(39, 'CMAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(40, 'IBPS-PO', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(41, 'IBPS-CLERK', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(42, 'SBI-PO', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(43, 'SBI-CLERK', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(44, 'RBI-OFFICER', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(45, 'RBI-ASSISTANT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(46, 'CTET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(47, 'SSC-CPO', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(48, 'SSC-MTS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(49, 'RRB', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(50, 'NDA', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(51, 'AILET(All INDIA Law Entrance test)', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(52, 'NIFT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(53, 'CPT(Common Proficiency Test)', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(54, 'NEET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(55, 'CDS', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(56, 'NEET PG', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(57, 'NEET UG', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(58, 'UGC NET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(59, 'NET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(60, 'AFCAT', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(61, 'C.A', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(62, 'C.S', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(63, 'TYPING ', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(64, 'NSD', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(65, 'NAT(National Aptitude test)', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(66, 'AICEE(All India Common Entrance Examination)', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(67, 'AFMC(Armed Forces Medical core Entrance)', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(68, 'J.B.T', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(69, 'K.V.S', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(70, 'STET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(71, 'HTET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(72, 'TET', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(73, 'DSSSB', 'custom_subject.png', 0, 1, 'Academic', 2, 1, '0000-00-00 00:00:00'),
(74, 'Sports', 'custom_subject.png', 0, 1, 'Non-Academic', 1, 3, '0000-00-00 00:00:00'),
(75, 'Co-curricular', 'custom_subject.png', 0, 1, 'Non-Academic', 1, 3, '0000-00-00 00:00:00'),
(81, 'Life Science', '', 0, 1, 'Academic', 0, 1, '2017-09-05 07:25:58'),
(89, 'B.Sc', '', 0, 1, 'Academic', 0, 1, '2017-09-08 12:41:15'),
(90, 'M.Sc', '', 0, 1, 'Academic', 0, 1, '2017-09-08 12:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `class_room`
--

CREATE TABLE `class_room` (
  `id` int(11) NOT NULL,
  `room_no` int(11) NOT NULL,
  `avatar` int(11) NOT NULL,
  `center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `duration` varchar(255) DEFAULT NULL,
  `duration_type` tinyint(1) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `category_id`, `duration`, `duration_type`, `fees`, `description`) VALUES
(1, 'Math', 6, '21', NULL, '3500.00', '35000');

-- --------------------------------------------------------

--
-- Table structure for table `course_category`
--

CREATE TABLE `course_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_category`
--

INSERT INTO `course_category` (`id`, `name`) VALUES
(1, 'BSC'),
(2, 'IT'),
(3, 'MCA'),
(4, '10th'),
(5, '11'),
(6, '12'),
(7, '5'),
(8, '6'),
(9, '7'),
(10, '8'),
(11, '9'),
(12, 'BA'),
(13, 'BA -Enghindi');

-- --------------------------------------------------------

--
-- Table structure for table `course_fee`
--

CREATE TABLE `course_fee` (
  `id` int(10) UNSIGNED NOT NULL,
  `faculty_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `payout` tinyint(1) DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `percent` decimal(10,2) DEFAULT NULL,
  `courses` text,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `datesheet`
--

CREATE TABLE `datesheet` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `heading` varchar(255) NOT NULL,
  `subheading` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `datesheet`
--

INSERT INTO `datesheet` (`id`, `teacher_id`, `batch_id`, `date`, `heading`, `subheading`, `description`, `created`) VALUES
(1, 1, 1, '2018-03-15', 'Get Rid Off Work', 'Reading', 'Get rid off work to much uploaded', '2017-12-20 11:37:17'),
(2, 1, 2, '2017-12-22', 'Exmas From Tomorrow', 'Stictly Formals', 'come along with id card', '2017-12-21 08:37:04'),
(3, 18, 3, '2018-01-23', 'Qwerty', 'Zxc', 'Xgg', '2017-12-22 11:19:50'),
(4, 18, 3, '2017-12-22', 'Hhh', 'Gh', 'Bbh', '2017-12-22 11:20:56'),
(5, 90, 46, '2018-05-24', 'Hshdd', 'Hdhddh', 'Nxnnddsnssnsnsmmkjsjsjssksalaoeurdkdkdjdjddkkskaaowksjddssssdsdjjddkkddkskskdddk', '2018-01-19 08:34:07'),
(6, 90, 46, '2018-01-19', 'Hhdhhs', 'Hdhs', 'Bdndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyxndddnndnddnsnsnsnsnydlydtxktkdkyxlxlyxlxylxltxkxtxlxyxtxtxlzlxyxlyxyxkxlxlxlxlyyyxyxyxyxlxlxyxyxyx', '2018-01-19 08:35:02'),
(7, 90, 46, '2017-08-17', 'Hdhseehe', 'Hehshe', 'Bdbdhbdjdbbdjjdddjdjjjdjddeawkejejdjdjdjdjdjdjdjdjdhjdjjdjejkwejjdjddjjdjdhjdjsnnwo??/??????', '2018-01-19 09:13:18'),
(8, 99, 56, '2018-01-24', 'Test Datesheet', 'In Coming Week', 'Regular datesheet', '2018-01-21 08:03:33'),
(9, 75, 52, '2018-01-19', '7tc7t', 'Tcctct', 'Ftfyggguvguy hubyu g6u8fiigc gvuoovohovp p pjjvpjvpjvppvochohc ococohcohcococohchcocococo f9o fofiy ff8 occ o c occx7ifo o coycyo c fiyofhohf9y fof o f9f9 ci. F9f99fofufpo 9f9f yp. P f9yoff o cofoffoy9fyyo yofoyfuocoo fofuo h fofoco foooh fo oucooocooc oo', '2018-01-23 07:38:24'),
(10, 75, 52, '2018-01-19', 'Datesheet Testing', 'Testing Sub Heading', 'This is formal discussion', '2018-01-24 09:14:26'),
(11, 49, 104, '2017-12-24', 'Ofkdkf', 'Gkxkckfkf', 'Uxhdgucjcic', '2018-01-24 12:37:57'),
(12, 87, 114, '2017-12-24', 'Yrur', 'Y rr', 'Hfhfbfbbd', '2018-01-24 12:54:51'),
(13, 49, 104, '2017-12-24', 'Irit', 'Fiid', 'Duud', '2018-01-24 13:38:46'),
(14, 49, 104, '2017-12-25', 'Kfkg', 'Lh', 'Iyitky', '2018-01-25 07:10:53'),
(15, 49, 104, '0000-00-00', 'Gjfkfg', '2017-12-25', '2', '2018-01-25 07:28:13'),
(16, 49, 104, '0000-00-00', 'Zxjjfz', '2017-12-27', '2', '2018-01-25 07:35:41'),
(17, 87, 114, '0000-00-00', 'Zbbsb', '2017-12-25', '2', '2018-01-25 07:42:26'),
(18, 87, 114, '0000-00-00', 'Hdhdhd', '2017-12-25', '2', '2018-01-25 08:57:28'),
(19, 87, 114, '0000-00-00', 'Sbbab', '2017-12-25', '2', '2018-01-25 10:44:07'),
(20, 107, 66, '2018-01-25', 'Hdhdhdhs', 'Hdhdhd', 'Hdhdbhhdhdhdksksjs', '2018-01-25 11:21:08'),
(21, 74, 64, '0000-00-00', 'Hhur', '2018-1-24', '1', '2018-01-25 11:42:21'),
(22, 117, 65, '0000-00-00', 'h', '2018-2-1', '1', '2018-01-29 06:16:40'),
(23, 136, 86, '2018-01-30', 'What Is Next', '', 'HdhsjskskslowiejehejGysktsktsktslyslyskyskyslyslydlyslyslydlydlysktsjtsktsk6s6lsl6sl6sk5sk5so5sl6dl6sk5sk5ak6sl6sk5sl6sl6sl6sl7sl6sk5aj5wmgdmysmydymsjtskysktsk6dl6sl6dl7dl7dl8fl7ek6sk5sj5sk5sk6sk6skysk6skyslysl6dl6dm6sk6sk6sl6sl6dl6dl7dl6d6ldl6sl6sl6sl6sl6sl6sl6sl6sl6sk6sk6sl6s', '2018-01-30 11:58:48'),
(24, 164, 95, '2018-02-06', 'Exam Date', '', 'Exam date are imp', '2018-02-06 09:29:09'),
(25, 174, 102, '2018-02-07', 'Hell', '', 'Hell yeah', '2018-02-07 09:58:22'),
(26, 174, 102, '2018-02-07', 'Mon -Sun', '', 'Mon -Sat', '2018-02-07 10:01:27'),
(27, 241, 143, '2018-02-13', 'Nnn', '', 'Uuh', '2018-02-15 12:02:33'),
(28, 241, 165, '2018-03-14', 'Bha', '', 'Usjn', '2018-03-15 09:58:09'),
(29, 249, 168, '2018-02-16', 'Hw', '', 'Jeje', '2018-03-16 09:41:58'),
(30, 250, 210, '2018-02-17', 'yeei', '', 'Hi t', '2018-03-16 11:37:57');

-- --------------------------------------------------------

--
-- Table structure for table `debug`
--

CREATE TABLE `debug` (
  `id` int(11) NOT NULL,
  `status` text COLLATE utf8_unicode_ci NOT NULL,
  `onapp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `res_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `demoes`
--

CREATE TABLE `demoes` (
  `id` int(11) NOT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `demo_name` varchar(100) NOT NULL,
  `demo_url` varchar(200) DEFAULT NULL,
  `demo_code` varchar(25) DEFAULT NULL,
  `student_id` int(11) NOT NULL,
  `created_by` bigint(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `enquires_status`
--

CREATE TABLE `enquires_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_time` timestamp NULL DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mobile` int(32) DEFAULT NULL,
  `preferred_time` time DEFAULT NULL,
  `status` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `course_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `gender` varchar(32) DEFAULT NULL,
  `dob` date NOT NULL,
  `remark` text,
  `handeled_by` varchar(255) DEFAULT NULL,
  `admitted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expanses`
--

CREATE TABLE `expanses` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `amount` decimal(10,2) DEFAULT NULL,
  `payment_method` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `category_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `expanses_category`
--

CREATE TABLE `expanses_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `explore_banner`
--

CREATE TABLE `explore_banner` (
  `id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `contant` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `explore_banner`
--

INSERT INTO `explore_banner` (`id`, `heading`, `contant`, `image`, `status`, `created_at`) VALUES
(88, 'LIFE SKILLS', '', 'banner_life_skills.jpg', 1, '2018-02-01 11:37:19'),
(89, 'TAKTII BACK TO BASICS', '', 'banner_backtobasics.jpg', 1, '2018-02-01 11:37:57'),
(81, 'TAKTii VIDEOss', '', 'banner_newtaktii5.jpg', 1, '2018-02-01 08:41:18'),
(84, 'NON-ACADEMIC', '', 'banner_new_academic.jpg', 1, '2018-02-01 11:36:54'),
(85, 'SAB PADHO,SAB BADHO', '', 'banner_child.jpg', 1, '2018-02-01 11:38:45'),
(86, 'HOME TUTOR', '', 'banner_Home_Tutor.jpg', 1, '2018-02-01 11:37:07');

-- --------------------------------------------------------

--
-- Table structure for table `explore_banner_video`
--

CREATE TABLE `explore_banner_video` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subtitle` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  `deccription` text NOT NULL,
  `banner_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `explore_banner_video`
--

INSERT INTO `explore_banner_video` (`id`, `title`, `subtitle`, `category`, `deccription`, `banner_id`, `image`, `video`, `status`, `created`) VALUES
(60, 'TAKTii', 'Application Explainer', 'Academic', 'Education is not the filling of a pail, but the lighting of a fire.\r\nTAKTii is a revolutionary mobile application for students and teachers.\r\n\r\nWatch Our Video and Share Your Feedback.\r\n\r\nLocation Courtesy: Feeling Great, Chhattarpur New-Delhi', 81, '', 'https://youtu.be/q31US-yTS90', 1, '2018-01-31 11:15:13'),
(59, 'Taktii', 'Introducing', 'Academic', 'Taktii is a need of the hour solution that helps a student connect to the best teachers both academic and non-academic in town through an integrated platform. the platform uses Geo-location to find the expert teachers near you.', 81, '', 'https://www.youtube.com/watch?v=x5yAiZ0ZxJY', 1, '2018-01-31 11:13:22');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_receipt`
--

CREATE TABLE `faculty_receipt` (
  `id` int(10) UNSIGNED NOT NULL,
  `receipt_no` int(11) DEFAULT NULL,
  `student_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `batch_name` varchar(255) NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `due_amount` decimal(10,2) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `payment_method` tinyint(1) NOT NULL DEFAULT '0',
  `payment_type` varchar(255) NOT NULL,
  `remark` text,
  `date` date DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fee_management`
--

CREATE TABLE `fee_management` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `pay_free` decimal(10,2) NOT NULL,
  `due_free` decimal(10,2) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `treacher_id` bigint(22) DEFAULT '0',
  `center_id` bigint(22) NOT NULL,
  `types` enum('documents','notes','tutorial','classroom','center','others') NOT NULL DEFAULT 'others',
  `student_id` bigint(22) DEFAULT '0',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uploaded_by` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `title`, `file_name`, `user_id`, `treacher_id`, `center_id`, `types`, `student_id`, `added`, `uploaded_by`) VALUES
(1, 'hgchjaghj', 'images1.png', 14, 0, 0, 'others', 0, '2017-03-25 03:16:46', 1),
(3, 'Rooms', 'images.jpg', 9, 0, 0, 'others', 0, '2017-03-27 23:01:28', 1),
(8, 'hgchjaghj', '53.jpg', 8, 0, 0, 'others', 0, '2017-03-28 15:08:35', 1),
(10, 'jfjskhfksj', '80x808.jpg', 9, 0, 0, 'others', 0, '2017-03-29 00:45:41', 1),
(29, 'cchjdwkcnjkk  iuiiiiii', 'amarnath1.jpg', 9, 9, 93, 'classroom', 0, '2017-03-29 06:26:08', 9),
(31, 'jrjerjvk', '80x802.jpg', 9, 9, 93, 'classroom', 0, '2017-03-29 03:07:03', 9),
(32, 'dxasxsasa', '75.jpg', 8, 8, 95, 'classroom', 0, '2017-03-29 16:39:20', 8),
(33, 'saaasasa', '76.jpg', 8, 8, 95, 'classroom', 0, '2017-03-29 16:39:31', 8),
(34, 'Back Image', 'amarnath11.jpg', 8, 8, 96, 'classroom', 0, '2017-04-02 03:28:24', 8),
(35, 'Front', '77.jpg', 8, 8, 96, 'classroom', 0, '2017-04-01 16:20:35', 8),
(36, 'Photo', '51.jpg', 8, 8, 96, 'classroom', 0, '2017-04-02 11:52:15', 8),
(37, 'Room Filesw jbfvhjdbj     nbrij    ww333333333', '5.jpg', 8, 0, 0, 'others', 0, '2017-04-03 05:20:00', 1),
(38, 'Room Files', '80x8012.jpg', 8, 0, 0, 'others', 0, '2017-04-03 09:44:37', 1),
(39, 'Room Files', '80x8013.jpg', 8, 0, 0, 'others', 0, '2017-04-03 09:45:08', 1),
(40, 'Room Files', '', 7, 7, 97, 'classroom', 0, '2017-04-03 15:42:37', 7),
(41, 'Room Files', '', 7, 7, 97, 'classroom', 0, '2017-04-03 15:43:07', 7);

-- --------------------------------------------------------

--
-- Table structure for table `institute_with_address`
--

CREATE TABLE `institute_with_address` (
  `id` int(50) NOT NULL,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inst_admins`
--

CREATE TABLE `inst_admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `inst_name` varchar(255) NOT NULL,
  `inst_email` varchar(255) NOT NULL,
  `inst_mobile` varchar(255) NOT NULL,
  `parrent` int(11) NOT NULL,
  `user_role` enum('1','2') NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1' COMMENT '0:disabled, 1:active, 2: Deleted',
  `inst_type` enum('single','multiple','admin') NOT NULL DEFAULT 'single',
  `logo` varchar(255) NOT NULL DEFAULT 'https://taktii.sgp1.digitaloceanspaces.com/static/30.png',
  `address` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inst_admins`
--

INSERT INTO `inst_admins` (`id`, `username`, `password`, `inst_name`, `inst_email`, `inst_mobile`, `parrent`, `user_role`, `status`, `inst_type`, `logo`, `address`, `created_at`, `updated_at`) VALUES
(1, 'taktii_admin', 'e10adc3949ba59abbe56e057f20f883e', 'taktti_institute', 'admin@taktii.com', '9560610340', 0, '1', '1', 'single', 'https://taktii.sgp1.digitaloceanspaces.com/static/30.png', '', '2018-04-28 09:35:05', '2018-05-01 07:12:54'),
(19, 'taktii_user', 'e10adc3949ba59abbe56e057f20f883e', 'taktii jankpuri', 'daraptoor@gmail.com', '9560610340', 1, '2', '1', 'admin', 'public/uploads/branch_img/taktii_user_1525159812.png', 'a2 asalatpur janakpuri', '2018-05-01 02:00:12', '2018-05-02 02:40:13'),
(20, 'usermain', 'e10adc3949ba59abbe56e057f20f883e', 'asd', '321@asd.asd', 'asdasdasd', 1, '2', '1', 'admin', 'public/uploads/branch_img/usermain_1525170503.jpg', 'asdasd', '2018-05-01 04:58:23', '2018-05-02 02:41:02');

-- --------------------------------------------------------

--
-- Table structure for table `inst_program`
--

CREATE TABLE `inst_program` (
  `id` int(11) NOT NULL,
  `prog_name` varchar(255) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `fees` varchar(255) NOT NULL,
  `inst_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inst_program`
--

INSERT INTO `inst_program` (`id`, `prog_name`, `course_id`, `course_name`, `class_id`, `class_name`, `subject_id`, `subject_name`, `duration`, `fees`, `inst_id`, `owner_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'test', '1', 'asd', '1', '1', '1', 'asd', '6 month', '2000', 1, 1, '2', '2018-05-01 11:11:07', '2018-05-02 02:16:08'),
(2, 'Test Program', 'NQ==', 'a255af6dc9dee2537fb6f7dc2c7f3098', 'MTM=', 'PROGRAMMING LANGUAGE', 'MTQ=', 'ASP.NET', '3 months', '2000', 1, 1, '2', '2018-05-02 02:03:13', '2018-05-02 02:16:06'),
(3, 'Test Program2', 'MQ==', 'Primary Classes', 'Ng==', 'CLASS VI', 'MzIz', 'ENGLISH', '3 months', '2000', 1, 1, '2', '2018-05-02 02:04:59', '2018-05-02 02:15:06'),
(4, 'asd', 'MjQ=', 'College Tuition', 'MjE2', 'B.Com. Hons. Tuition', 'MTc2MQ==', 'Principles of Micro Economics', 'asd', 'asd', 1, 1, '1', '2018-05-02 02:05:47', '2018-05-02 02:44:19'),
(5, 'asdasd', '24', 'College Tuition', '215', 'BBA Tuition', '1788', 'Business Law', 'asd', '213', 1, 1, '2', '2018-05-02 02:09:08', '2018-05-02 02:16:54'),
(6, 'asdasd', '23', 'B.Tech. &  B.E. Tuition', '220', 'Mechanical Engineering', '1596', 'Engineering Graphics', 'ads', '213', 1, 1, '2', '2018-05-02 02:10:01', '2018-05-02 02:16:39');

-- --------------------------------------------------------

--
-- Table structure for table `inst_teacher`
--

CREATE TABLE `inst_teacher` (
  `id` int(11) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `teacher_type` varchar(255) NOT NULL,
  `teacher_salary` varchar(255) NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interns_fileupload`
--

CREATE TABLE `interns_fileupload` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `std_name` varchar(255) NOT NULL,
  `type` varchar(100) NOT NULL,
  `file_upload` varchar(255) NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interns_fileupload`
--

INSERT INTO `interns_fileupload` (`id`, `unique_id`, `file_name`, `std_name`, `type`, `file_upload`, `added_date`) VALUES
(33, 'CXT93HIJRK', 'Screenshot', 'Sidharth Dhillon', 'PDF', '/etc/intern_upload/14694Screenshot_2018-03-16-22-59-19-509_com.android.vending.pdf', '2018-03-18 19:58:49'),
(32, 'Z6c9ly2ifk', 'Installations screen shot ', 'Anuj yadav', 'PDF', '/etc/intern_upload/4384IMG-20180317-WA0065 (4 files merged).pdf', '2018-03-18 17:31:21'),
(30, 'Z6c9ly2ifk', 'Screen shots', 'Anuj yadav', 'PDF', '/etc/intern_upload/60185IMG-20180317-WA0054 (7 files merged).pdf', '2018-03-17 19:50:55'),
(31, 'Z6c9ly2ifk', 'Installations screen shot', 'Anuj yadav', 'PDF', '/etc/intern_upload/69080IMG-20180317-WA0054 (6 files merged).pdf', '2018-03-17 22:14:01'),
(29, 'MJTJWAPISF', 'Screenshots', 'Shubhankar kukreti', 'PDF', '/etc/intern_upload/87884images.pdf', '2018-03-15 22:17:36'),
(34, 'CXT93HIJRK', 'Screenshot', 'Sidharth Dhillon', 'PDF', '/etc/intern_upload/22425Screenshot_2018-03-18-19-23-07-261_com.android.chrome.pdf', '2018-03-18 20:00:21'),
(35, 'CXT93HIJRK', 'Screenshot', 'Sidharth Dhillon', 'PDF', '/etc/intern_upload/67270IMG-20180319-WA0005 (8 files merged).pdf', '2018-03-19 11:31:33'),
(36, 'YFROUTTLWP', 'Screenshot ', 'Kartik Kumar', 'PDF', '/etc/intern_upload/35131Screenshot_2018-03-19-13-28-13.pdf', '2018-03-19 13:39:30'),
(37, 'YFROUTTLWP', 'Screenshot (rating)', 'Kartik Kumar', 'PDF', '/etc/intern_upload/59145Screenshot_2018-03-19-13-26-44.pdf', '2018-03-19 13:40:18'),
(38, 'YFROUTTLWP', 'Screenshot', 'Kartik Kumar', 'PDF', '/etc/intern_upload/24277Screenshot_2018-03-19-13-42-11.pdf', '2018-03-19 13:43:42'),
(39, '9B5RFKVBZY', 'Screenshot', 'Keshav Saini', 'PDF', '/etc/intern_upload/68917IMG-20180319-WA0021 (14 files merged).pdf', '2018-03-19 19:36:59'),
(40, 'MJTJWAPISF', 'Screenshots', 'Shubhankar kukreti', 'PDF', '/etc/intern_upload/32073IMG-20180319-WA0035 (10 files merged).pdf', '2018-03-19 19:59:38'),
(41, 'Z8Q6OCA1MQ', 'Screenshots', 'Tarushi bhatia', 'PDF', '/etc/intern_upload/86621jpg2pdf.pdf', '2018-03-20 13:49:03'),
(42, 'Z8Q6OCA1MQ', 'Screenshots', 'Tarushi bhatia', 'PDF', '/etc/intern_upload/39796jpg2pdf (2).pdf', '2018-03-20 14:13:13'),
(43, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/95766convert-jpg-to-pdf.net_2018-03-21_08-37-18.pdf', '2018-03-21 13:08:23'),
(44, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/49169convert-jpg-to-pdf.net_2018-03-21_08-47-50.pdf', '2018-03-21 13:20:19'),
(45, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'PDF', '/etc/intern_upload/31032ss of profile and installation.pdf', '2018-03-21 13:23:04'),
(55, 'Z8Q6OCA1MQ', 'Screenshots', 'Tarushi bhatia', 'JPG', '/etc/intern_upload/24461IMG-20180317-WA0033.jpg', '2018-03-23 09:10:36'),
(54, 'Z8Q6OCA1MQ', 'Screenshots', 'Tarushi bhatia', 'PDF', '/etc/intern_upload/4727IMG-20180320-WA0007.jpg', '2018-03-22 21:12:01'),
(53, 'IUGEBBC7N5', 'Screenshots', 'Harshpal singh juneja', 'JPG', '/etc/intern_upload/26172IMG-20180317-WA0008.jpg', '2018-03-22 08:43:22'),
(52, 'IUGEBBC7N5', 'Screenshots', 'Harshpal singh juneja', 'JPG', '/etc/intern_upload/75488IMG-20180317-WA0009.jpg', '2018-03-22 08:42:52'),
(56, 'Z8Q6OCA1MQ', 'Screenshots', 'Tarushi bhatia', 'JPG', '/etc/intern_upload/45061IMG-20180319-WA0005.jpg', '2018-03-23 09:11:04'),
(57, 'Y18PYKXAT4', 'Resume', 'Dhruv Chauhan', 'PDF', '/etc/intern_upload/40799cv.pdf', '2018-03-23 20:10:20'),
(58, 'IUGEBBC7N5', 'Screenshots', 'Harshpal singh juneja', 'JPG', '/etc/intern_upload/65699IMG-20180317-WA0008.jpg', '2018-03-24 09:58:06'),
(59, '5LSTMJ87D5', 'Resume', 'Suraj Singh', 'PDF', '/etc/intern_upload/61615Suraj_Resume.pdf', '2018-03-24 11:53:21'),
(60, 'YFROUTTLWP', 'Ss download 1', 'Kartik Kumar', 'JPG', '/etc/intern_upload/71902IMG-20180324-WA0009.jpg', '2018-03-24 15:10:53'),
(61, 'YFROUTTLWP', 'Ss download 2', 'Kartik Kumar', 'JPG', '/etc/intern_upload/27630IMG-20180324-WA0012.jpg', '2018-03-24 15:11:18'),
(62, 'YFROUTTLWP', 'Ss download 3', 'Kartik Kumar', 'JPG', '/etc/intern_upload/45430IMG-20180324-WA0014.jpg', '2018-03-24 15:11:44'),
(63, 'YFROUTTLWP', 'Ss download 4', 'Kartik Kumar', 'JPG', '/etc/intern_upload/17106IMG-20180324-WA0016.jpg', '2018-03-24 15:12:06'),
(64, 'YFROUTTLWP', 'Ss download 5', 'Kartik Kumar', 'JPG', '/etc/intern_upload/30660IMG-20180324-WA0017.jpg', '2018-03-24 15:12:32'),
(65, 'YFROUTTLWP', 'Ss download 6', 'Kartik Kumar', 'JPG', '/etc/intern_upload/63004IMG-20180324-WA0021.jpg', '2018-03-24 15:13:04'),
(66, 'YFROUTTLWP', 'Ss download 7', 'Kartik Kumar', 'JPG', '/etc/intern_upload/89818IMG-20180324-WA0022.jpg', '2018-03-24 15:13:34'),
(67, 'YFROUTTLWP', 'Ss download 8', 'Kartik Kumar', 'JPG', '/etc/intern_upload/33951IMG-20180324-WA0025.jpg', '2018-03-24 15:14:19'),
(68, 'YFROUTTLWP', 'Ss download 9', 'Kartik Kumar', 'JPG', '/etc/intern_upload/65113IMG-20180324-WA0028.jpg', '2018-03-24 15:24:50'),
(69, 'YFROUTTLWP', 'Ss download 10', 'Kartik Kumar', 'JPG', '/etc/intern_upload/54277IMG-20180324-WA0030.jpg', '2018-03-24 19:42:52'),
(70, 'YFROUTTLWP', 'Ss download 11', 'Kartik Kumar', 'JPG', '/etc/intern_upload/83784IMG-20180324-WA0034.jpg', '2018-03-24 19:43:30'),
(71, 'YFROUTTLWP', 'Ss download 12', 'Kartik Kumar', 'JPG', '/etc/intern_upload/54818IMG-20180324-WA0035.jpg', '2018-03-24 19:43:59'),
(72, 'YFROUTTLWP', 'Ss download 13', 'Kartik Kumar', 'JPG', '/etc/intern_upload/67360IMG-20180324-WA0036.jpg', '2018-03-24 19:44:34'),
(73, 'YFROUTTLWP', 'Ss download 14', 'Kartik Kumar', 'JPG', '/etc/intern_upload/90723IMG-20180324-WA0037.jpg', '2018-03-24 19:45:12'),
(74, 'YFROUTTLWP', 'Ss download 15', 'Kartik Kumar', 'JPG', '/etc/intern_upload/58898IMG-20180324-WA0038.jpg', '2018-03-24 19:46:00'),
(75, 'YFROUTTLWP', 'Ss download 16', 'Kartik Kumar', 'JPG', '/etc/intern_upload/22386IMG-20180324-WA0040.jpg', '2018-03-24 19:46:49'),
(76, 'YFROUTTLWP', 'Ss download 17', 'Kartik Kumar', 'JPG', '/etc/intern_upload/96773IMG-20180324-WA0039.jpg', '2018-03-24 19:47:06'),
(77, 'CIABDHFCMB', 'Screenshot', 'Preeti', 'JPG', '/etc/intern_upload/5087IMG-20180324-WA0004.jpg', '2018-03-24 22:41:27'),
(78, '', 'Ss download 18', 'Kartik Kumar', 'JPG', '/etc/intern_upload/41547IMG-20180324-WA0044.jpg', '2018-03-24 22:55:46'),
(79, 'YFROUTTLWP', 'Ss download 18', 'Kartik Kumar', 'JPG', '/etc/intern_upload/36329IMG-20180324-WA0044.jpg', '2018-03-24 22:56:19'),
(80, 'CIABDHFCMB', 'Jpg', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/99648IMG-20180324-WA0004.jpg', '2018-03-25 14:34:38'),
(81, 'CIABDHFCMB', 'Jpg', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/32284IMG-20180323-WA0026.jpg', '2018-03-25 14:40:05'),
(82, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/12904IMG-20180323-WA0025.jpg', '2018-03-25 14:40:45'),
(83, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/61667IMG-20180323-WA0023.jpg', '2018-03-25 14:41:22'),
(84, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/7676IMG-20180323-WA0024.jpg', '2018-03-25 14:41:46'),
(85, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/31312IMG-20180323-WA0020.jpg', '2018-03-25 14:42:14'),
(86, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/82951IMG-20180323-WA0022.jpg', '2018-03-25 14:42:36'),
(87, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/88048IMG-20180323-WA0013.jpg', '2018-03-25 14:43:09'),
(88, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/61873IMG-20180323-WA0009.jpg', '2018-03-25 14:43:32'),
(89, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/41392IMG-20180323-WA0007.jpg', '2018-03-25 14:44:28'),
(90, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/39843IMG-20180323-WA0008.jpg', '2018-03-25 14:44:52'),
(91, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/93876IMG-20180323-WA0006.jpg', '2018-03-25 14:46:05'),
(92, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/62100IMG-20180323-WA0005.jpg', '2018-03-25 14:46:30'),
(93, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/79430IMG-20180323-WA0000.jpg', '2018-03-25 14:46:57'),
(94, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/69746IMG-20180323-WA0001.jpg', '2018-03-25 14:47:24'),
(95, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/46941received_1996491497283253.jpg', '2018-03-25 15:51:49'),
(96, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/11202received_1998449777087425.jpg', '2018-03-25 15:52:28'),
(97, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/30968received_971762649648617.jpg', '2018-03-25 15:52:48'),
(98, 'CIABDHFCMB', 'Screenshot ', 'Preeti Jaiswal ', 'JPG', '/etc/intern_upload/92773received_972914949533387.jpg', '2018-03-25 15:53:05'),
(99, 'CEZSK0IQ1A', 'Task1', 'Suraj Bisht', 'JPG', '/etc/intern_upload/67357Screenshot_2018-03-25-22-23-14-473_com.takktistudentapp.jpg', '2018-03-26 10:26:51'),
(100, 'CEZSK0IQ1A', 'Task1', 'Suraj Bisht', 'JPG', '/etc/intern_upload/75516Screenshot_2018-03-25-22-27-15-348_com.android.vending.jpg', '2018-03-26 10:28:12'),
(101, 'CUZ1WBKQFN', 'Installation and review', 'Yashika Jindal', 'PDF', '/etc/intern_upload/65024C0DD6EFB-D865-4C8E-B0CE-958485F150B7.pdf', '2018-03-26 16:35:29'),
(102, 'CUZ1WBKQFN', 'Student profile ', 'Yashika Jindal ', 'PDF', '/etc/intern_upload/687154C394E44-7026-47A4-9D0E-A1F175CE8DD3.pdf', '2018-03-26 16:38:07'),
(103, 'A07JZYFFZ2', 'app ss', 'Siddharth Bhalla', 'PDF', '/etc/intern_upload/61571Screenshot_2018-03-26-16-50-27.pdf', '2018-03-26 17:09:06'),
(104, 'A07JZYFFZ2', 'app profile', 'Siddharth Bhalla', 'PDF', '/etc/intern_upload/1850Screenshot_2018-03-26-17-37-10.pdf', '2018-03-26 17:38:51'),
(105, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/33053convert-jpg-to-pdf.net_2018-03-26_21-25-57.pdf', '2018-03-27 00:56:34'),
(106, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/94547convert-jpg-to-pdf.net_2018-03-26_21-29-21.pdf', '2018-03-27 01:00:28'),
(107, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/25104convert-jpg-to-pdf.net_2018-03-26_21-33-09.pdf', '2018-03-27 01:03:39'),
(108, 'TLY9GG3XVQ', 'Screenshot', 'Sahaj singh kohli', 'PDF', '/etc/intern_upload/97930convert-jpg-to-pdf.net_2018-03-26_21-33-09.pdf', '2018-03-27 01:03:58'),
(109, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/68434Screenshot_2018-03-28-12-14-30-400_com.takktistudentapp.jpg', '2018-03-28 12:23:24'),
(110, 'CEZSK0IQ1A', 'Task2', 'Suraj Bisht', 'JPG', '/etc/intern_upload/50313Screenshot_2018-03-27-18-44-33-725_com.android.vending.jpg', '2018-03-28 20:34:28'),
(111, 'CEZSK0IQ1A', 'Task2', 'Suraj Bisht', 'JPG', '/etc/intern_upload/10717Screenshot_2018-03-27-18-44-47-217_com.android.vending.jpg', '2018-03-28 20:35:00'),
(112, 'CEZSK0IQ1A', 'Task3', 'Suraj Bisht', 'JPG', '/etc/intern_upload/13212Screenshot_2018-03-27-19-06-39-234_com.facebook.katana.jpg', '2018-03-28 20:39:08'),
(113, 'CEZSK0IQ1A', 'Task3', 'Suraj Bisht', 'JPG', '/etc/intern_upload/32434Screenshot_2018-03-27-19-17-27-600_com.google.android.youtube.jpg', '2018-03-28 20:39:44'),
(114, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/73374IMG-20180329-WA0001.jpg', '2018-03-29 12:36:35'),
(115, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/58901IMG-20180329-WA0018.jpg', '2018-03-29 15:55:15'),
(116, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/5535IMG-20180329-WA0017.jpg', '2018-03-29 15:55:37'),
(117, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/59597IMG-20180329-WA0009.jpg', '2018-03-29 15:55:51'),
(118, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/87264IMG-20180329-WA0011.jpg', '2018-03-29 15:56:04'),
(119, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/88115IMG-20180329-WA0012.jpg', '2018-03-29 15:56:19'),
(120, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/94905IMG-20180329-WA0008.jpg', '2018-03-29 15:56:39'),
(121, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/42692IMG-20180329-WA0005.jpg', '2018-03-29 15:56:57'),
(122, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/90563IMG-20180329-WA0006.jpg', '2018-03-29 15:57:23'),
(123, 'FSKO1MD6NZ', 'screenshots', 'prashant kalra', 'JPG', '/etc/intern_upload/23075IMG-20180329-WA0002.jpg', '2018-03-29 15:57:44'),
(124, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/97117IMG-20180329-WA0006.jpg', '2018-03-29 17:12:11'),
(125, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/59190IMG-20180329-WA0004.jpg', '2018-03-29 17:12:47'),
(126, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/5401Screenshot_2018-03-21-09-46-04-606_com.takktistudentapp_1521605798854.jpg', '2018-03-29 20:00:44'),
(127, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/77443Screenshot_2018-03-29-19-38-40-183_com.android.vending_1522332558653.jpg', '2018-03-29 20:01:05'),
(128, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/51621Screenshot_2018-03-21-09-59-37-658_com.takktistudentapp_1521606609501.jpg', '2018-03-29 20:01:21'),
(129, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/89402Screenshot_2018-03-29-19-45-26-280_com.android.vending_1522332951948.jpg', '2018-03-29 20:01:40'),
(130, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/42729IMG-20180329-WA0008.jpg', '2018-03-29 20:14:27'),
(131, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/30136IMG-20180329-WA0009.jpg', '2018-03-29 20:14:50'),
(132, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/3075IMG-20180320-WA0002.jpg', '2018-03-30 00:00:51'),
(133, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/75345IMG-20180329-WA0016.jpg', '2018-03-30 00:03:21'),
(134, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/96207IMG-20180330-WA0002.jpg', '2018-03-30 10:48:21'),
(135, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'PDF', '/etc/intern_upload/20504IMG-20180330-WA0004.jpg', '2018-03-30 11:18:37'),
(136, 'CUZ1WBKQFN', 'Review ', 'Yashika Jindal', 'PDF', '/etc/intern_upload/63906BA1A336F-1EE5-4A41-B074-DAD25CD7FBE5.pdf', '2018-03-30 14:30:45'),
(137, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/37418IMG-20180330-WA0009.jpg', '2018-03-30 18:52:41'),
(138, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/62103IMG-20180330-WA0011.jpg', '2018-03-30 19:38:34'),
(139, 'BM79ZVIMMH', 'Screenshot', 'Prateek Gaur', 'JPG', '/etc/intern_upload/18340image3A120307.jpg', '2018-03-31 10:04:09'),
(140, 'BM79ZVIMMH', 'Screenshot', 'Prateek Gaur', 'JPG', '/etc/intern_upload/95017image3A120305.jpg', '2018-03-31 10:04:35'),
(141, 'MA4JS7V3AW', 'Madhvi Arora', 'Madhvi Arora ', 'PDF', '/etc/intern_upload/9699IMG-20180330-WA0012.jpg', '2018-03-31 18:33:30'),
(142, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/74115IMG-20180331-WA0005.jpg', '2018-03-31 19:13:51'),
(143, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/7535IMG-20180331-WA0009.jpg', '2018-03-31 20:18:29'),
(144, '', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/88724IMG-20180331-WA0011.jpg', '2018-03-31 21:14:50'),
(145, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/6000IMG-20180331-WA0011.jpg', '2018-03-31 21:16:01'),
(146, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/89829IMG-20180321-WA0005.jpg', '2018-04-01 12:06:48'),
(147, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/86283IMG-20180331-WA0003.jpg', '2018-04-01 12:07:48'),
(148, 'CIABDHFCMB', 'Screenshot ', 'Preeti ', 'JPG', '/etc/intern_upload/82511IMG_20180401_121740.jpg', '2018-04-01 12:18:55'),
(149, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/45735IMG-20180324-WA0001.jpg', '2018-04-01 12:20:38'),
(150, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/52193IMG-20180401-WA0012.jpg', '2018-04-01 15:16:42'),
(151, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/66001IMG-20180401-WA0013.jpg', '2018-04-01 20:40:24'),
(152, 'CEZSK0IQ1A', 'Task4', 'Suraj Bisht', 'JPG', '/etc/intern_upload/40468IMG-20180401-WA0014.jpg', '2018-04-01 20:40:54'),
(153, 'GCSW3Y1FXT', 'Screenshot', 'Swasti jain', 'PDF', '/etc/intern_upload/39020IMG-20180402-WA0007.jpg', '2018-04-02 12:15:20'),
(154, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/60641IMG-20180402-WA0001.jpg', '2018-04-02 14:55:49'),
(155, 'CIABDHFCMB', 'Screenshot ', 'Preeti', 'JPG', '/etc/intern_upload/78368IMG-20180402-WA0002.jpg', '2018-04-02 14:56:16');

-- --------------------------------------------------------

--
-- Table structure for table `intern_register`
--

CREATE TABLE `intern_register` (
  `id` int(11) NOT NULL,
  `user_code` varchar(20) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile_no` varchar(20) NOT NULL,
  `course` varchar(255) NOT NULL,
  `semester` varchar(150) NOT NULL,
  `college` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `certificate` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `intern_register`
--

INSERT INTO `intern_register` (`id`, `user_code`, `full_name`, `email`, `mobile_no`, `course`, `semester`, `college`, `city`, `certificate`, `created_date`) VALUES
(23, 'MJTJWAPISF', 'Shubhankar kukreti', 'kukretishubhabkar@gmail.com', '9971602096', 'BBA(G)', '4th semester', 'Fairfield institute of management and technology', 'Gurugram', '', '2018-03-10 10:27:53'),
(24, '2X6J5PWQIU', 'Paramdeep Singh', 'paramdeep2828@gmail.com', '9654175068', 'Bba', '4th', 'Fimt', 'Gurgaon', '', '2018-03-10 10:30:34'),
(25, 'CPZSPXCA26', 'Kashish Sharma ', 'kashishakemi@gmail.com', '7290039091', 'BBA aviation operation', '2', 'School of Business, UPES', 'Dehradun', '', '2018-03-10 10:31:54'),
(27, '9B5RFKVBZY', 'Keshav Saini', 'Keshavsaini600@gmail.com', '7838890288', 'BBA General', '4th ', 'Fairfield Institute of Management and Technology', 'Gurugram', '', '2018-03-10 10:34:38'),
(29, 'IGN2MN6SLY', 'Ayushi Bansal', 'ayushibansal38@gmail.com', '8076164467', 'Bsc math honors', '4', 'Northcap university ', 'Gurgaon ', '', '2018-03-10 17:11:11'),
(30, '3PSDOPMZJO', 'Riya pahuja', 'riyapahuja74@gmail.com', '9654521446', 'Btech computer science', '4th', 'Northcap university', 'Gurgaon', '', '2018-03-10 17:31:20'),
(31, 'Z8Q6OCA1MQ', 'Tarushi bhatia', 'tarushib8@gmail.com', '7065949632', 'Bsc maths hon', '4', 'The NorthCap University', 'Gurugram', '', '2018-03-10 17:37:30'),
(32, 'KYWYUENODO', 'Parul Chhabra', 'parulchhabra97@gmail.com', '9716773231', 'Bba(g)', '3rd', 'FIMT', 'Delhi', '', '2018-03-10 18:35:39'),
(33, 'FDEHMRRLBP', 'Sahil Pulani', 'sahil.pulani1@gmail.com', '9911100856', 'Bcom(h)', '4th', 'FIMT', 'Gurgaon', '', '2018-03-10 19:10:34'),
(34, 'Z6C9LY2IFK', 'Anuj yadav', 'anujyadav7707@gmail.com', '8826583866', 'B.com hons ', '4', 'Fimt', 'Gurgaon', '', '2018-03-10 19:19:10'),
(35, 'C1OSYYBZWN', 'Shifa Khosla', 'shifakhosla4699@gmail.com', '9711422814', 'B.A (program)', '4', 'Kalindi college,DU', 'Delhi', '', '2018-03-10 19:40:21'),
(36, 'CXT93HIJRK', 'Sidharth Dhillon', 'sidharthdhillon9@gmail.com', '9999265979', 'B.com(h)', '4th', 'Fairfield Institute Of Management And Technology', 'Gurugram', '', '2018-03-10 19:51:27'),
(37, 'J3OVLTGOFV', 'Vaibhav Ghatak', 'vaibhav.ghatak98@gmail.com', '8377940921', 'BBA', '4th', 'Fairfield Institute of Management and Technology', 'New Delhi', '', '2018-03-10 23:20:57'),
(38, 'FK05TNO4RC', 'Rupesh barik', 'barikrupesh98@gmail.com', '8375059894', 'BJMC', '4th', 'Fairfield institute of Management technology', 'New delhi', '', '2018-03-10 23:43:13'),
(39, '313CFV3AEF', 'Shahrukh khan', 'Shahdakpathar99@gmail.com', '9811731300', 'B.A. Programme', '2nd year', 'DU SOL', 'New Delhi', '', '2018-03-11 00:07:42'),
(40, 'IUGEBBC7N5', 'HARSHPAL SINGH JUNEJA', 'harshpalsingh49@gmail.com', '9599432355', 'Btech cs cloud computing and information security ', '04', 'Sharda university', 'Uttar pradesh', '', '2018-03-11 00:09:30'),
(41, 'SHVHXBXJKH', 'Yashesvi sharma', 'sharma.yashesvi256@gmail.com', '9555807852', 'B-tech', '04', 'Sharda university', 'New delhi', '', '2018-03-11 00:18:45'),
(42, '5HHXTZN9NV', 'Prashant Rawat', 'prashantrawat904@yahoo.in', '8588086103', 'BBA', '4th', 'FIMT', 'Delhi', '', '2018-03-11 00:29:36'),
(43, 'WWZLV5H3N7', 'Adeeba Fatima', 'adeebafatima10@gmail.com', '7542027576', 'B.tech CS', '04', 'Sharda University', 'Noida', '', '2018-03-11 01:09:47'),
(44, 'SWPB97LJSS', 'Arun Kumar', 'arunyadav0208@gmail.com', '8130920735', 'Bcom(Hons)', '4th', 'FIMT', 'Delhi', '', '2018-03-11 10:01:53'),
(45, '7ZZBDC9LDK', 'Harshit Kataria', 'harshitkashish@gmail.com', '8587973611', 'BBA', '4', 'Fimt', 'Gurgaon', '', '2018-03-11 11:36:04'),
(46, 'UY02ONFI7G', 'Nitish rai', 'maacnitishrai@gmail.com', '8178184455', 'B. Tech', '4', 'Sharda university ', 'Greator noida ', '', '2018-03-11 12:47:51'),
(47, 'ZWZTOZ7S7Z', 'Kashish Rehan', 'Kashishrehan23@gmail.com', '7838660969', 'Bba llb', '4', 'Northcap university', 'Gurugram', '', '2018-03-11 12:49:05'),
(48, 'H3EGLTKABN', 'Kajal sharma', 'kajal200030@gmail.com', '8512073168', 'Bba general', '4', 'Fairfield college of management and technology', 'New delhi', '', '2018-03-11 15:26:38'),
(49, 'ZZXSLWQ11F', 'Shweta bhardwaj', 'sushantbhardwaj2000@gmail.co', '9910082653', 'Bba', '4th', 'FIMT', 'Delhi', '', '2018-03-11 15:47:12'),
(50, 'PYJIBHJAF1', 'Mohit tyagi', 'mohittyagi10@yahoo.in', '9643237290', 'Bcom (honors) ', '4', 'Ipu- fimt', 'Gurgaon', '', '2018-03-11 17:21:28'),
(51, 'IVHUC2G7BJ', 'Krishan Dudeja', 'dudejakrishan1508@gmail.com', '9999624868', 'BBA (G)', '4th', 'fimt kapshera', 'Gurgaon', '', '2018-03-11 18:26:24'),
(52, 'D3SQ4A68UZ', 'Nitesh dagar', 'niteshdagar21@gmail.com', '8700521303', 'BBA', '4', 'FIMT', 'Delhi', '', '2018-03-11 19:24:46'),
(53, 'GV9EB62GPC', 'Varsha yadav', 'varshayadav5514@gmal.com', '8368482283', 'B.com', '4', 'Kalinga university', 'Raipur', '', '2018-03-11 21:35:03'),
(54, 'S0ZVWUT7AQ', 'Vandana Mathur', 'vandana.mathur83@gmail.com', '8860693511', 'BBA', '4th', 'New Delhi Institute of Management', 'New Delhi', '', '2018-03-11 22:15:18'),
(55, 'CIABDHFCMB', 'Preeti Jaiswal', 'preetijaiswal2309@gmail.com', '8505831522', 'BBA(G) ', '4', 'FIMT', 'New Delhi', '', '2018-03-12 14:03:35'),
(56, 'LMXC5BEWHB', 'Kunika Hasija', 'kunika.hasija07@gmail.com', '8860627324', 'BBA(G)', '4th semester', 'Fairfield Institute of Management and Technology', 'Delhi', '', '2018-03-12 14:36:01'),
(57, 'KFV7HGLC3E', 'Chetan Rakheja', 'rakhejachetan@gmail.com', '9910454745', 'Bca', '4th', 'Jims,vk', 'Delhi', '', '2018-03-12 15:24:55'),
(58, 'YFROUTTLWP', 'KARTIK KUMAR', 'kartikk3434@gmail.com', '9811828776', 'Bba ', '4th', 'Fairfield Institute of management and technology', 'New delhi', '', '2018-03-12 17:32:15'),
(59, 'KXKMAZ0ECV', 'Prince Chhabra ', 'Chhabra.prince029@gmail.com', '8373993682', 'BBA(G) ', '4', 'FIMT, IPU', 'New Delhi', '', '2018-03-12 18:29:25'),
(60, 'IQ68ZS2FNQ', 'Khushboo Yadav', 'khushbooyadavaa@gmail.com', '8285396307', 'BBA(G) ', '4', 'FIMT, IPU', 'Delhi', '', '2018-03-12 18:33:14'),
(62, '8NFFEUN9MU', 'Apoorva Sehrawat', 'sehrawat.sonal1997@gmail.com', '8800104909', 'BBA', '6th', 'Fairfield Institute of Management and Technology', 'New Delhi', '', '2018-03-14 11:53:08'),
(63, 'TLY9GG3XVQ', 'Sahaj singh kohli', 'singhsahaj9.8@gmail.com', '9718397315', 'Bba(gen)', 'Fourth', 'Fairfield Institute of management and technology', 'Delhi', '', '2018-03-14 12:57:36'),
(64, '9I8R0AWFNA', 'Chanchal Sharma', 'chanchal200030@gmail.com', '7042345836', 'BBA', '4th', 'Fairfeild institute of management and technology ', 'New delhi', '', '2018-03-14 12:58:49'),
(65, 'V1VWWQOCIB', 'Debayan Adhikari', 'debayanadhikari@gmail.com', '8130027296', 'BBA', '4th', 'Fairfeild institute of Management and technology', 'Gurgaon', '', '2018-03-14 13:16:16'),
(66, 'DIG3MS9YLB', 'Vimal Dubey', 'vimaldubey8512@outlook.com', '8512006126', 'Bba', '4', 'Ims', 'Delhi', '', '2018-03-14 21:45:44'),
(67, 'VGBNLLJDET', 'Vivek kumar ', 'kvivek1094@gmail.com', '8802740138', 'B. Com hons ', '6', 'Arsd college. Delhi University. ', 'Delhi ', '', '2018-03-14 22:22:44'),
(68, 'CUZ1WBKQFN', 'Yashika Jindal', 'yashikajindal@gmail.com', '9711285868', 'Bba', '2nd ', 'Jims , Sector 5', 'New delhi ', '', '2018-03-14 22:33:57'),
(69, 'CEZSK0IQ1A', 'Suraj Bisht', '9953188485srj@gmail.com', '9953188485', 'BBA', '4', 'Fairfield institute of management and technology', 'Delhi', '', '2018-03-15 10:06:52'),
(70, 'RPKLE5LT1R', 'Sushant Negi', 'dragonballz.negi@gmail.com', '9818706320', 'BBA', '4', 'Fairfield institute of management and technology', 'Delhi', '', '2018-03-15 10:10:01'),
(74, 'A07JZYFFZ2', 'Siddharth Bhalla', 'siddharth2667@gmail.com', '9636292924', 'Btech MAE', '3rd', 'Niec', 'Delhi', '', '2018-03-16 16:16:19'),
(75, 'SX1RFT6YPT', 'Himanshu vashisht', 'himanshuvashisht007000@gmail.com', '9717783467', 'Bcom hons', '2nd ', 'Trinity institute of professional studies ', 'Gurgaon', '', '2018-03-17 20:56:13'),
(76, 'MGLYYKDB0W', 'stephen vincent', 'stefvincent63@gmail.com', '9958526441', 'Bba', 'IV', 'The Northcap University ', 'Gurugram', '', '2018-03-17 22:09:54'),
(77, 'VTSF8TMHFL', 'Sachin Sehrawat', 'sachinsehrawat85@yahoo.in', '8800155490', 'BBA', '4', 'Fimt', 'Gurgaon', '', '2018-03-17 22:18:40'),
(78, '2URC65TF44', 'mohit sachdeva', 'mohitsachdeva48@gmail.com', '8860873389', 'HRM', '4', 'college of vocational studies', 'DELHI', '', '2018-03-17 22:28:23'),
(79, 'DWAUULKBS2', 'Komal Sharma ', 'komalsharma0678@gmail.com', '9716321795', 'B.com program ', '6', 'Shaheed bhagat singh evening college ', 'Delhi ', '', '2018-03-17 22:55:38'),
(80, '49BY4UKWPN', 'Gaurav satija', 'gauravsatija17@gmail.com', '9711883205', 'BBA', '4th ', 'JIMS , Vasant kunj', 'New delhi', '', '2018-03-17 23:07:55'),
(81, '2TDADBSBZB', 'Shubham pandey', 'shubhampandey980@gmail.com', '9953173647', 'BBA', '4', 'FIMT', 'New delhi', '', '2018-03-17 23:22:57'),
(82, 'JMUIJGKWBK', 'Anshul', 'anshulk434@gmail.com', '7042814731', 'B.A program', '2', 'Bhagat Singh college', 'New delhi', '', '2018-03-18 01:27:29'),
(85, 'M2HJ7LGM2M', 'Vinay ', 'vinay1011998@gmail.com', '7678370953', 'Btech ', '2', 'Maharaja Agrasen institute of technology ', 'Faridabad ', '', '2018-03-18 22:52:09'),
(86, 'HG1RQH8LPD', 'Utkarsh Sharma', 'Utkarshsharma015@gmail.com', '8527920953', 'BBA (G)', '4th', 'Fairfield Institute of Management & Technology', 'New Delhi', '', '2018-03-18 23:17:01'),
(87, 'FSKO1MD6NZ', 'prashant kalra', 'prashantkalra69@gmail.com', '7292068131', 'BBA(G)', '4', 'Fairfield institute of management and technology', 'mew delhi', '', '2018-03-18 23:25:02'),
(88, '5NKCOEERKM', 'Harshit bhatnagar', 'bhatnagarharshit450@gmail.com', '8130520400', 'ITI', '1', 'ITI jail road', 'Delhi', '', '2018-03-18 23:26:50'),
(89, '6CJCVNLFAI', 'Damini Narang', 'damini1899@gmail.com', '9868823038', 'B.com(h)', '2', 'Institute of innovation in technology and management', 'New Delhi', '', '2018-03-18 23:36:42'),
(90, '26NUSXWRH8', 'Sarthak Singh Negi', 'sarthakfr@gmail.com', '9953294366', 'B.Tech.', '2', 'Maharaja Agrasen Institute of Technology ', 'New Delhi', '', '2018-03-19 09:20:31'),
(91, 'JJBWBFE6FN', 'Shivani Rana ', 'shivanirana10011999@gmail.com', '8447710690', 'Advance diploma in TV programme and news production ', 'IV semester ', 'Maharaja Agrasen college ', 'Delhi ', '', '2018-03-19 15:55:13'),
(92, 'QHY8DRAVOA', 'Sahib Kumar Singh ', 'sahibkumarsingh94@gmail.com', '9650556729', 'Bag', '1 year ', 'Jamia millia islamia', 'Delhi', '', '2018-03-19 16:02:42'),
(93, 'EVBAJ0O1LP', 'Aman Trehan', 'atamantrehan2896@gmail.com', '7838971993', 'Btech CSE', '4', 'The North cap university(ITM UNIVERSITY)', 'GURUGRAM', '', '2018-03-19 16:29:45'),
(94, '2NIGBFLKHA', 'Garusha chopra', 'GARUSHACHOPRRA435@GMAIL.com', '8800877646', 'BBA', '4th', 'Farefield institute of management and technology ', 'Gurgaon  haryana', '', '2018-03-19 17:47:46'),
(95, '4MZACDFNDP', 'himani pandey', 'princesshp007@gmail.com', '9911830066', 'bcom', 'first ', 'ramanujan college', 'new delhi', '', '2018-03-19 19:43:58'),
(96, 'FYZA5XV7GE', 'Neeran', 'neejlbn@gmail.com', '8368320550', 'Hindi honours', '2', 'Jesus and Mary college', 'Delhi', '', '2018-03-19 20:09:32'),
(97, '1PVHIAZMRU', 'Ritwik', '77330ritwik@gmail.com', '7733066511', 'English hons', '4', 'Deen dayal upadhaya collage', 'Delhi', '', '2018-03-19 20:13:52'),
(98, 'WZY1JOQIS2', 'Richa', 'richalodha34@gmail.com', '7549082936', 'B.A English hons', '4 th', 'Ramjas', 'New Delhi', '', '2018-03-19 20:15:14'),
(99, 'PDQTOVJWO3', 'Smriti', 'singhsmriti2036@gmail.com', '8527563102', 'Bba', '4th', 'Fimt', 'Delhi', '', '2018-03-19 20:27:54'),
(100, 'CAOUMKJV7X', 'Nisha', 'ns861999@gmail.com', '8447818935', 'Hindi hinours', '2', 'Kamla Nehru college', 'New delhi', '', '2018-03-19 20:45:40'),
(102, 'LTSNUIW3CQ', 'komal gupta', 'komalgupta245@gmail.com', '7206480334', 'mcom', '2', 'hindu girls college', 'sonipat', '', '2018-03-19 20:51:28'),
(103, 'K088WLIVJC', 'Garima gauba', 'ggauba55@gmail.com', '8750636384', 'Btech', '4th', 'Dronacharya college of engineering gurgaon ', 'GURUGRAM', '', '2018-03-19 20:55:32'),
(104, '7BSRZUNZY8', 'parul gupta', 'parulgupta7816@gmail.com', '9034539047', 'mcom', '2', 'hindu girls college', 'sonipat', '', '2018-03-19 20:56:28'),
(105, 'CJ9ET8DIYH', 'Ankit garg', 'Ankitguptaganaur@gmail.com', '9068356060', 'M. Com', '2', 'Arya college', 'Panipat', '', '2018-03-19 21:02:59'),
(106, '9RLAP2QARZ', 'anil gupta', 'anilsingla1968@gmail.com', '9416638217', 'ma', '2', 'hibdu boys college', 'sonipat', '', '2018-03-19 21:19:02'),
(107, 'KFPKMV1CYD', 'Simran walia', 'simranaw1923@gmail.com', '9650352596', 'B.com(hons)', '4', 'Delhi university', 'New delhi', '', '2018-03-19 21:25:23'),
(108, 'IKNJXB1YBB', 'Shikha shukla', 'ss5787548@gmail.com', '9654109700', 'Hindi honurs', 'Second ', 'Kamla nehru college', 'Delhi', '', '2018-03-19 21:30:27'),
(109, 'RWRWRAPTZ2', 'Tarun singh bisht', 'tarun.singh52555@gmail.com', '9958034852', 'Bcom', '3rd', 'Aurbindo ', 'New delhi', '', '2018-03-19 21:44:38'),
(110, 'MSS8ENFKFB', 'Sunaina Sharma ', 'sunaina3110@gmail.com', '9780737967', 'MCA', '6', 'LPU', 'Amritsar ', '', '2018-03-19 21:59:44'),
(111, '3664JSRKFV', 'A.V.Siddharth', 'avsid97@gmail.com', '9884204230', 'B.Tech (Mechanical)', '6', 'SRM University', 'Kattankulathur, Chennai', '', '2018-03-20 07:19:39'),
(112, 'IG4CHVYJCK', 'Party Manu', 'Parthmanu1410@gmail.com', '9461644142', 'BBALLB', '4', 'FIMT ', 'Gurgaon', '', '2018-03-20 10:57:53'),
(113, 'IWRTKTOJ1C', 'Jatin Janghu', 'Jatinjanghu@gmail.com', '9811926128', 'Bba', '4', 'Fimt', 'Gurgaon', '', '2018-03-21 10:58:03'),
(114, 'HKF60B7C0P', 'Preeti Mukherjee', 'pmukhopdhya@gmail.com', '9560174212', 'BBA(G)', '4th', 'Fairfield Institute of Management and Technology,GGSIPU', 'New Delhi', '', '2018-03-21 17:53:29'),
(117, 'DXXJNNVMH3', 'Khushboo Garg', 'cooldudkhushigarg@gmail.com', '9555051728', 'Fashion designing', '2', 'JD Institute of fashion technology ', 'New Delhi', '', '2018-03-21 22:54:00'),
(118, 'IIGVB8I6JR', 'Anand Rajpal ', 'andyrr.ar@gmail.com', '9560693905', 'Bsc mathematical sciences ', '2nd', 'Keshav Mahavidyalaya', 'New Delhi', '', '2018-03-22 12:02:00'),
(119, 'GCSW3Y1FXT', 'Swasti jain', 'swasti1510@gmail.com', '8826745903', 'Bsc maths honours', '4', 'Northcap university', 'Gurgaon', '', '2018-03-23 09:06:54'),
(120, 'V6LA2O8DGN', 'Sachin Kumar', 'sachin00705@gmail.com', '9971574123', 'BBA (G)', '4th Sem', 'FIMT', 'Delhi', '', '2018-03-23 12:43:58'),
(121, 'Y18PYKXAT4', 'Dhruv Chauhan', 'dhruv.chauhan@students.iiit.ac.in', '7032562718', 'BTech, Computer Science Engineering', '4th', 'IIIT-Hyderabad', 'Hyderabad', '', '2018-03-23 20:06:42'),
(122, 'NTQUPMHXYJ', 'Anubhav Jain', 'jainanubhav222@gmail.com', '7834842424', 'B. Tech(MAE)', '2', 'Maharaja Agrasen Institute of Technology', 'Delhi', '', '2018-03-23 20:23:08'),
(123, 'BTNKMPZF6U', 'Harshit Gupta ', 'guptaharshit1304@gmail.com', '9711021455', 'B. Tech ', '2nd', 'MAIT', 'New Delhi', '', '2018-03-23 22:29:25'),
(124, 'ZBVB3Z0CMV', 'Tanya ', 'tanyarebel.24@gmail.com', '7838925145', 'Abc', 'Abc', 'Abc', 'Delhi', '', '2018-03-23 23:35:43'),
(125, 'BM79ZVIMMH', 'Prateek Gaur', 'prateek007gaur99@gmail.com', '9958835953', 'BBA(General)', '4th', 'Fairfield Institute of management and technology (FIMT)', 'New Delhi', '', '2018-03-24 07:42:15'),
(126, '5LSTMJ87D5', 'Suraj Singh', 'sudkmr@live.in', '9650472679', 'BBA in E-commerce Marketing', '4', 'University of Petroleum and Energy Studies', 'Dehradun', '', '2018-03-24 11:51:53'),
(128, 'IFBUE6RZKN', 'SHUBHAM NOUGIEN', 'ssnaugain@gmail.com', '9958517748', '', '', '', '', '', '2018-03-25 02:47:13'),
(129, 'T6F6M22LGU', 'Rohit Arora', 'rohit.arora0721@gmail.com', '8588840561', 'B.com(h)', '2', 'Trinity institute of professional studies', 'Delhi', '', '2018-03-25 08:55:47'),
(130, 'OHRQRJHI1C', 'Nishant  ', 'Nishantrajpoot213@gmail.com', '9910119409', 'Bba', '2', 'Jagan institue of managememt studies', 'Delhi', '', '2018-03-25 12:55:16'),
(132, '6VDHADCKVC', 'Akshita', 'akshita220396@gmail.com', '9873040082', 'Fashion designing', 'Sem4', 'Jd institute of fashion technology', 'Delhi', '', '2018-03-26 23:03:03'),
(133, 'CWXY4MQK1E', 'KRITIKA BHARDWAJ ', 'kritikabhardwaj1996@gmail.com', '8800615336', 'MBA (MM)', '2', 'Amity University, Noida, Uttar Pradesh', 'New Delhi', '', '2018-03-27 01:18:47'),
(134, 'MWQSI26ZZQ', 'Vivek Pillai', 'vivekpillai7777@gmail.com', '8920832921', 'BBA(G)', '4th', 'IINTM, Janakpuri ', 'New Delhi', '', '2018-03-27 10:54:00'),
(135, 'HM64X86MQX', 'srishty malhotra', 'srishtymalhotra7@gmail.com', '8826289067', 'BBA', '4th', 'Fairfield Institute of Management and Technology', 'New Delhi', '', '2018-03-28 01:25:52'),
(136, 'TKYJYPMIQS', 'shivangi jain', 'shivangijn1998@gmail.com', '9205071968', 'BBA', '4th', 'Fairfield Institute of Management and Technology', 'New Delhi', '', '2018-03-28 10:47:55'),
(137, 'XLKUV3LA9G', 'Simran', 'simran7sunder@gmail', '8800311564', 'Bachelor of business administration ', '4th', 'Fairfield institute of management and technology', 'New delhi', '', '2018-03-28 12:17:13'),
(138, '99VUVW6WSM', 'Sonit', 'sonitb62@gmail.com', '9716272132', 'BBA', 'Fourth', 'Jims, Kalkaji', 'New Delhi', '', '2018-03-29 01:38:42'),
(139, 'GTZU8F3ZRI', 'Garusha chopra', 'garushachopra435@gmail.com', '9953791441', 'BBA', '4th', 'Farefield institute of management and technology ', 'Gurgaon  haryana', '', '2018-03-29 22:13:12'),
(140, 'TELUDDSBBZ', 'Jasleen Saluja', 'jk40358@gmail.com', '7000378297', 'M.A (English)', 'II', 'Amity University ', 'Noida', '', '2018-03-31 17:39:47'),
(141, 'CE4T3NV5PD', 'Furqaan Ahmad', 'shaanshahedi@yahoo.com', '9717663698', 'BBA', '2', 'IP University', 'Delhi', '', '2018-03-31 17:57:38'),
(142, 'MA4JS7V3AW', 'Madhvi Arora ', 'arora.madhvi19@gmail.com', '9560347621', 'BBA general', '4th', 'FIMT, GGSIPU ', 'Gurgaon ', '', '2018-03-31 18:30:30'),
(143, 'QUGMVLP4IB', 'mehak malhotra', 'mehakmalhotra7317@gmail.com', '9643258384', 'BBA', '5', 'Fairfield Institute of Management and Technology', 'Delhi', '', '2018-03-31 18:41:33'),
(144, '1C0J7WEKIE', 'Lakshay Munjal', 'akshaymunjal5899@gmail.com', '9582762688', 'BBA(G)', '2nd', 'Fairfield Institute Of Management & Technology', 'New Delhi', '', '2018-03-31 21:08:49');

-- --------------------------------------------------------

--
-- Table structure for table `intro_request`
--

CREATE TABLE `intro_request` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `intro_request`
--

INSERT INTO `intro_request` (`id`, `teacher_id`, `request_date`) VALUES
(1, '1', '20-12-2017'),
(2, '2', '20-12-2017'),
(3, '9', '21-12-2017'),
(4, '18', '22-12-2017'),
(32, '15', '27-01-2018'),
(6, '26', '23-12-2017'),
(7, '28', '23-12-2017'),
(8, '30', '24-12-2017'),
(9, '29', '24-12-2017'),
(10, '31', '24-12-2017'),
(11, '19', '24-12-2017'),
(12, '38', '26-12-2017'),
(13, '44', '26-12-2017'),
(14, '46', '26-12-2017'),
(15, '54', '28-12-2017'),
(16, '58', '28-12-2017'),
(17, '59', '28-12-2017'),
(18, '57', '28-12-2017'),
(19, '61', '28-12-2017'),
(20, '60', '29-12-2017'),
(21, '66', '02-01-2018'),
(22, '71', '04-01-2018'),
(23, '43', '04-01-2018'),
(24, '75', '05-01-2018'),
(25, '79', '11-01-2018'),
(26, '49', '11-01-2018'),
(27, '39', '11-01-2018'),
(28, '83', '11-01-2018'),
(29, '42', '20-01-2018'),
(30, '98', '20-01-2018'),
(31, '99', '21-01-2018'),
(33, '107', '29-01-2018'),
(34, '135', '30-01-2018'),
(36, '136', '31-01-2018'),
(37, '7', '04-02-2018'),
(38, '162', '05-02-2018'),
(39, '167', '06-02-2018'),
(40, '169', '06-02-2018'),
(41, '180', '08-02-2018'),
(42, '184', '08-02-2018'),
(43, '190', '09-02-2018'),
(44, '194', '09-02-2018'),
(45, '188', '11-02-2018'),
(46, '213', '12-02-2018'),
(47, '225', '13-02-2018'),
(48, '232', '13-02-2018'),
(49, '236', '14-02-2018'),
(50, '176', '14-02-2018'),
(51, '249', '16-03-2018');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `price` decimal(10,2) DEFAULT NULL,
  `qty` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'Assames'),
(2, 'Bengali'),
(3, 'Gujarati'),
(4, 'Kannada'),
(5, 'Kashmiri'),
(6, 'Konkani'),
(7, 'Malayalam'),
(8, 'Marathi'),
(9, 'Nepali'),
(10, 'Oriya'),
(11, 'Punjabi'),
(12, 'Sanskrit'),
(13, 'Tamil'),
(14, 'Telugu'),
(15, 'Urdu'),
(16, 'Hindi'),
(17, 'English');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `password_salt` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `user_type` enum('Admin','Store') DEFAULT NULL,
  `status` tinyint(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `username`, `passwd`, `password_salt`, `password`, `name`, `email`, `user_type`, `status`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '44468A', '', 'Taktii Admin', 'admin@admin.com', 'Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_classes`
--

CREATE TABLE `ms_classes` (
  `id` int(11) NOT NULL,
  `class_name` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active, 0 for inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_classes`
--

INSERT INTO `ms_classes` (`id`, `class_name`, `status`) VALUES
(1, 'I', 1),
(2, 'II', 1),
(3, 'III', 1),
(4, 'IV', 1),
(5, 'V', 1),
(6, 'VI', 1),
(7, 'VII', 1),
(8, 'VIII', 1),
(9, 'IX', 1),
(10, 'X', 1),
(11, 'XI', 1),
(12, 'XII', 1),
(13, 'B.A', 1),
(14, 'B.COM', 1),
(15, 'B.Sc', 1),
(16, 'BBA', 1),
(17, 'B.E/B.Tech', 1),
(18, 'B.Pharm', 1),
(19, 'M.Sc', 1),
(20, 'M.A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_subjects`
--

CREATE TABLE `ms_subjects` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `board` varchar(255) NOT NULL,
  `subject_code` varchar(255) NOT NULL,
  `category` enum('Academic','Non-Academic') NOT NULL,
  `category_id` int(11) NOT NULL,
  `course_type` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active, 0 for inactive',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_subjects`
--

INSERT INTO `ms_subjects` (`id`, `subject`, `image`, `class`, `board`, `subject_code`, `category`, `category_id`, `course_type`, `status`, `created_at`) VALUES
(1, 'English', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(2, 'E.V.S', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:01'),
(3, 'Maths', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:02'),
(4, 'Hindi', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:03'),
(5, 'G.K', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:04'),
(6, 'General Science', 'custom_subject.png', '1', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:05'),
(7, 'English', 'custom_subject.png', '2', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:06'),
(8, 'E.V.S', 'custom_subject.png', '2', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:07'),
(10, 'Hindi', 'custom_subject.png', '2', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:09'),
(11, 'G.K', 'custom_subject.png', '2', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:10'),
(12, 'General Science', 'custom_subject.png', '2', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:11'),
(13, 'English', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:12'),
(14, 'E.V.S', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:13'),
(15, 'Maths', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:14'),
(16, 'Hindi', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:15'),
(17, 'G.K', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:16'),
(18, 'General Science', 'custom_subject.png', '3', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:17'),
(19, 'English', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:18'),
(20, 'E.V.S', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:19'),
(21, 'Maths', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:20'),
(22, 'Hindi', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:21'),
(23, 'G.K', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:22'),
(24, 'General Science', 'custom_subject.png', '4', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:23'),
(25, 'English', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:24'),
(26, 'E.V.S', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:25'),
(27, 'Maths', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:26'),
(28, 'Hindi', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:27'),
(29, 'G.K', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:28'),
(30, 'General Science', 'custom_subject.png', '5', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:29'),
(31, 'Science', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:30'),
(32, 'Maths', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:31'),
(33, 'English', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:32'),
(34, 'Hindi', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:33'),
(35, 'Civics', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:34'),
(36, 'Geography', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:35'),
(37, 'History', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:36'),
(38, 'French', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:37'),
(39, 'Sanskrit', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:38'),
(40, 'Spanish', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:39'),
(41, 'German', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:40'),
(42, 'G.K', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:41'),
(43, 'Computer', 'custom_subject.png', '6', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:42'),
(44, 'Science', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:43'),
(45, 'Maths', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:44'),
(46, 'English', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:45'),
(47, 'Hindi', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:46'),
(48, 'Civics', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:47'),
(49, 'Geography', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:48'),
(50, 'History', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:49'),
(51, 'French', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:50'),
(52, 'Sanskrit', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:51'),
(53, 'Spanish', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:52'),
(54, 'German', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:53'),
(55, 'G.K', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:54'),
(56, 'Computer', 'custom_subject.png', '7', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:55'),
(57, 'Science', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:56'),
(58, 'Maths', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:57'),
(59, 'English', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:58'),
(60, 'Hindi', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:59'),
(61, 'Civics', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(62, 'Geography', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(63, 'History', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(64, 'French', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(65, 'Sanskrit', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(66, 'Spanish', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(67, 'German', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(68, 'G.K', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(69, 'Computer', 'custom_subject.png', '8', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(70, 'English', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(71, 'Science', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(72, 'Mathematics', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(73, 'Social science', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(74, 'Hindi', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(75, 'Sanskrit', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(76, 'Home science', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(77, 'French', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(78, 'German', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(79, 'Spanish', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(80, 'Foundation Information and Technology', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(81, 'Computers', 'custom_subject.png', '9', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(82, 'English', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(83, 'Science', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(84, 'Mathematics', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(85, 'Social science', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(86, 'Hindi', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(87, 'Sanskrit', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(88, 'Home science', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(89, 'French', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(90, 'German', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(91, 'Spanish', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(92, 'Foundation Information and Technology', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(93, 'Computers', 'custom_subject.png', '10', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(94, 'Physics', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(95, 'Mathematics', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(96, 'Chemistry', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(97, 'Biology', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(98, 'Biotechnology', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(99, 'Computer Science', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(100, 'Accountancy', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(101, 'Business Studies', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(102, 'Economics', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(103, 'English', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(104, 'Hindi', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(105, 'History', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(106, 'Political Science', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(107, 'Geography', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(108, 'Sociology', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(109, 'Home science', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(110, 'Engineering Drawing', 'custom_subject.png', '11', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(111, 'Physics', 'subject_physics1.jpg', 'VIII', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '2017-08-20 13:13:11'),
(112, 'Mathematics', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(113, 'Chemistry', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(114, 'Biology', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(115, 'Biotechnology', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(116, 'Computer Science', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(117, 'Accountancy', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(118, 'Business Studies', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(119, 'Economics', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(120, 'English', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(121, 'Hindi', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(122, 'History', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(123, 'Political Science', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(124, 'Geography', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(125, 'Sociology', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(126, 'Home science', 'custom_subject.png', '12', 'CBSE', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(128, 'Cricket', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(129, 'Football', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(130, 'Basket ball', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(131, 'Gymnastics', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(132, 'Base Ball', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(133, 'Rugby', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(134, 'Weight Lifting', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(135, 'Judo', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(136, 'Cycling', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(137, 'Boxing', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(138, 'Lawn Tennis', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(139, 'Badminton', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(140, 'Volleyball', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(141, 'Squash', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(142, 'Billiards', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(143, 'Swimming', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(144, 'Shooting', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(145, 'Kho-kho', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(146, 'Kabaddi', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(147, 'Hockey', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(148, 'Karrate', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(149, 'Kickboxing', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(150, 'Martial Arts', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(151, 'Golf', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(152, 'Horse Riding', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(153, 'Archery', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(154, 'Wrestling', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(155, 'Chess', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(156, 'Table Tennis', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(157, 'Dance', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(158, 'Singing', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(159, 'Writing/ Reading', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(160, 'Instruments', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(161, 'Computer ', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(162, 'Media', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(163, 'Photography', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(164, 'Cooking', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(165, 'Architecture/Interior Designing', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(166, 'Fashion Designing', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(167, 'Art', 'custom_subject.png', '75', 'XYZ', 'XYZ', 'Non-Academic', 3, 1, 1, '0000-00-00 00:00:00'),
(168, 'Maths', 'custom_subject.png', '13', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(169, 'Physics', 'custom_subject.png', '13', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(170, 'Chemistry', 'custom_subject.png', '13', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(171, 'Maths', 'custom_subject.png', '14', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(172, 'Physics', 'custom_subject.png', '14', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(173, 'Chemistry', 'custom_subject.png', '14', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(174, 'Physics', 'custom_subject.png', '15', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(175, 'Chemistry', 'custom_subject.png', '15', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(176, 'Biology', 'custom_subject.png', '15', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(177, 'Quantitative Ability', 'custom_subject.png', '16', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(178, 'DATA Interpretation ', 'custom_subject.png', '16', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(179, 'Logical Reasoning', 'custom_subject.png', '16', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(180, 'Verbal Ability', 'custom_subject.png', '16', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(181, 'Quantitative Ability', 'custom_subject.png', '17', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(182, 'DATA Interpretation ', 'custom_subject.png', '17', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(183, 'Logical Reasoning', 'custom_subject.png', '17', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(184, 'Verbal Ability', 'custom_subject.png', '17', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(185, 'Quantitative Ability', 'custom_subject.png', '18', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(186, 'DATA Interpretation ', 'custom_subject.png', '18', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(187, 'Logical Reasoning', 'custom_subject.png', '18', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(188, 'Verbal Ability', 'custom_subject.png', '18', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(189, 'Physics', 'custom_subject.png', '20', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(190, 'Chemistry', 'custom_subject.png', '20', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(191, 'Biology', 'custom_subject.png', '20', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(192, 'Quantitative Aptitude', 'custom_subject.png', '21', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(193, 'Reasoning', 'custom_subject.png', '21', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(194, 'English language', 'custom_subject.png', '21', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(195, 'General Awareness', 'custom_subject.png', '21', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(196, 'Computer Knowledge', 'custom_subject.png', '21', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(197, 'Economics', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(198, 'Political science', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(199, 'General paper on teaching & Research aptitude', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(200, 'Commerce ', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(201, 'Education ', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(202, 'Management', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(203, 'English', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(204, 'Human Resource Management', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(205, 'Library and Information services', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(206, 'Computer science and Application', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(207, 'Yoga', 'custom_subject.png', '22', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(208, '?Preliminary Examination or CSAT (Objective Section)', 'custom_subject.png', '23', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(209, 'Main Examination (Subjective Section)', 'custom_subject.png', '23', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(210, 'Mathematics', 'custom_subject.png', '24', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(211, 'General Knowledge', 'custom_subject.png', '24', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(212, 'Language Ability', 'custom_subject.png', '24', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(213, 'English', 'custom_subject.png', '25', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(214, 'General Knowledge', 'custom_subject.png', '25', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(215, 'Elementary Mathematics', 'custom_subject.png', '25', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(216, 'Legal Aptitude', 'custom_subject.png', '25', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(217, 'Reasoning', 'custom_subject.png', '25', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(218, 'Physics', 'custom_subject.png', '26', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(219, 'Chemistry', 'custom_subject.png', '26', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(220, 'Mathematics', 'custom_subject.png', '26', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(221, 'English', 'custom_subject.png', '26', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(222, 'Reasoning', 'custom_subject.png', '26', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(223, 'Analytical Writing', 'custom_subject.png', '28', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(224, 'Verbal Reasoning', 'custom_subject.png', '28', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(225, 'Quantitative Reasoning', 'custom_subject.png', '28', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(226, 'Analytical Writing', 'custom_subject.png', '31', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(227, 'Reasoning', 'custom_subject.png', '31', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(228, 'Quantitative Section', 'custom_subject.png', '31', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(229, 'Verbal Section', 'custom_subject.png', '31', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(230, 'English', 'custom_subject.png', '32', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(231, 'History', 'custom_subject.png', '32', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(232, 'Mathematics', 'custom_subject.png', '32', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(233, 'Science', 'custom_subject.png', '32', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(234, 'Quantitative Aptitude', 'custom_subject.png', '34', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(235, 'English Comprehension', 'custom_subject.png', '34', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(236, 'General Awareness', 'custom_subject.png', '34', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(237, 'General Intelligence and Reasoning', 'custom_subject.png', '34', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(238, 'Quantitative Aptitude', 'custom_subject.png', '35', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(239, 'English Comprehension', 'custom_subject.png', '35', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(240, 'General Awareness', 'custom_subject.png', '35', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(241, 'General Intelligence and Reasoning', 'custom_subject.png', '35', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(242, 'Quantitative Aptitude', 'custom_subject.png', '36', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(243, 'English Comprehension', 'custom_subject.png', '36', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(244, 'General Awareness', 'custom_subject.png', '36', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(245, 'General Intelligence and Reasoning', 'custom_subject.png', '36', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(246, 'Maths', 'custom_subject.png', '37', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(247, 'Physics', 'custom_subject.png', '37', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(248, 'Chemistry', 'custom_subject.png', '37', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(249, 'Advanced Physics', 'custom_subject.png', '38', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(250, 'Advanced Chemistry', 'custom_subject.png', '38', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(251, 'Advanced Mathematics', 'custom_subject.png', '38', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(252, 'Language', 'custom_subject.png', '39', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(253, 'Quantitative Techniques', 'custom_subject.png', '39', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(254, 'Logical Reasoning', 'custom_subject.png', '39', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(255, 'General Awareness', 'custom_subject.png', '39', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(256, 'Quantitative Aptitude', 'custom_subject.png', '40', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(257, 'Reasoning', 'custom_subject.png', '40', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(258, 'English language', 'custom_subject.png', '40', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(259, 'General Awareness', 'custom_subject.png', '40', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(260, 'Computer Knowledge', 'custom_subject.png', '40', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(261, 'Quantitative Aptitude', 'custom_subject.png', '41', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(262, 'Reasoning', 'custom_subject.png', '41', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(263, 'English language', 'custom_subject.png', '41', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(264, 'General Awareness', 'custom_subject.png', '41', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(265, 'Computer Knowledge', 'custom_subject.png', '41', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(266, 'Quantitative Aptitude', 'custom_subject.png', '42', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(267, 'Reasoning', 'custom_subject.png', '42', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(268, 'English language', 'custom_subject.png', '42', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(269, 'General Awareness', 'custom_subject.png', '42', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(270, 'Computer Knowledge', 'custom_subject.png', '42', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(271, 'Quantitative Aptitude', 'custom_subject.png', '43', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(272, 'Reasoning', 'custom_subject.png', '43', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(273, 'English language', 'custom_subject.png', '43', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(274, 'General Awareness', 'custom_subject.png', '43', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(275, 'Computer Knowledge', 'custom_subject.png', '43', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(276, 'Quantitative Aptitude', 'custom_subject.png', '44', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(277, 'Reasoning', 'custom_subject.png', '44', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(278, 'English language', 'custom_subject.png', '44', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(279, 'General Awareness', 'custom_subject.png', '44', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(280, 'Computer Knowledge', 'custom_subject.png', '44', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(281, 'Quantitative Aptitude', 'custom_subject.png', '45', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(282, 'Reasoning', 'custom_subject.png', '45', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(283, 'English language', 'custom_subject.png', '45', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(284, 'General Awareness', 'custom_subject.png', '45', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(285, 'Computer Knowledge', 'custom_subject.png', '45', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(286, 'Child Development & Pedagogy', 'custom_subject.png', '46', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(287, 'Language ', 'custom_subject.png', '46', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(288, 'Mathematics', 'custom_subject.png', '46', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(289, 'Science', 'custom_subject.png', '46', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(290, 'Social science/EVS', 'custom_subject.png', '46', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(291, 'Quantitative Aptitude', 'custom_subject.png', '47', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(292, 'English Comprehension', 'custom_subject.png', '47', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(293, 'General Awareness', 'custom_subject.png', '47', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(294, 'General Intelligence and Reasoning', 'custom_subject.png', '47', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(295, 'Quantitative Aptitude', 'custom_subject.png', '48', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(296, 'English Comprehension', 'custom_subject.png', '48', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(297, 'General Awareness', 'custom_subject.png', '48', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(298, 'General Intelligence and Reasoning', 'custom_subject.png', '48', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(299, 'Reasoning', 'custom_subject.png', '49', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(300, 'Numerical Aptitude ', 'custom_subject.png', '49', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(301, 'Quantitative Aptitude', 'custom_subject.png', '49', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(302, 'Mathematics', 'custom_subject.png', '50', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(303, 'General Ability', 'custom_subject.png', '50', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(304, 'General Knowledge', 'custom_subject.png', '51', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(305, 'English', 'custom_subject.png', '51', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(306, 'Basic Mathematics', 'custom_subject.png', '51', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(307, 'Legal Aptitude', 'custom_subject.png', '51', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(308, 'Reasoning', 'custom_subject.png', '51', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(309, 'Quantitative Aptitude', 'custom_subject.png', '52', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(310, 'Communication Aptitude', 'custom_subject.png', '52', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(311, 'English Comprehension', 'custom_subject.png', '52', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(312, 'Analytical Ability', 'custom_subject.png', '52', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(313, 'General Knowledge', 'custom_subject.png', '52', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(314, 'Fundamentals of Accounting', 'custom_subject.png', '53', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(315, 'Mercantile Law', 'custom_subject.png', '53', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(316, 'General Economics', 'custom_subject.png', '53', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(317, 'Quantitative Aptitude', 'custom_subject.png', '53', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(318, 'Physics', 'custom_subject.png', '54', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(319, 'Chemistry', 'custom_subject.png', '54', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(320, 'Mathematics', 'custom_subject.png', '54', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(321, 'English ', 'custom_subject.png', '55', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(322, 'General Knowledge', 'custom_subject.png', '55', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(323, 'Elementary Mathematics', 'custom_subject.png', '55', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(324, 'Anatomy', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(325, 'Physiology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(326, 'Biochemistry', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(327, 'Pharmacology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(328, 'Microbiology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(329, 'Pathology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(330, 'Forensic Medicine', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(331, 'Social & Preventive Medicine', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(332, 'Medicine Dermatology & Vernamology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(333, 'Surgery ,ENT , Orthopedics & Anaesthesia', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(334, 'Radiodiagnosis & Radiotherapy', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(335, 'Obstetrics & Gynaecology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(336, 'Paediatrics', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(337, 'Ophthalmology', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(338, 'Psychiatry', 'custom_subject.png', '56', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(339, 'Physics', 'custom_subject.png', '57', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(340, 'Chemistry', 'custom_subject.png', '57', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(341, 'Mathematics', 'custom_subject.png', '57', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(342, 'Economics', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(343, 'Political science', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(344, 'General paper on teaching & Research aptitude', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(345, 'Commerce ', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(346, 'Education ', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(347, 'Management', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(348, 'English', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(349, 'Human Resource Management', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(350, 'Library and Information services', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(351, 'Computer science and Application', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(352, 'Yoga', 'custom_subject.png', '58', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(353, 'Economics', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(354, 'Political science', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(355, 'General paper on teaching & Research aptitude', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(356, 'Commerce ', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(357, 'Education ', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(358, 'Management', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(359, 'English', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(360, 'Human Resource Management', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(361, 'Library and Information services', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(362, 'Computer science and Application', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(363, 'Yoga', 'custom_subject.png', '59', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(364, 'Verbal ability', 'custom_subject.png', '60', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(365, 'Numerical ability', 'custom_subject.png', '60', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(366, 'Reasoning & Military aptitude', 'custom_subject.png', '60', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(367, 'General Awareness', 'custom_subject.png', '60', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(368, 'Financial Reporting ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(369, ' Strategic Financial Management ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(370, 'Advanced Auditing and Professional Ethics ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(371, 'Corporate Laws and Other Economic Laws', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(372, 'Advanced Management Accounting ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(373, ' Financial Services and Capital Markets ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(374, 'Advanced Tax Management and ?International Taxation ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(375, ' Indirect Tax Laws ', 'custom_subject.png', '61', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(376, 'Financial Reporting ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(377, 'Strategic Financial Management ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(378, 'Advanced Auditing and Professional Ethics ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(379, 'Corporate Laws and Other Economic Laws', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(380, 'Advanced Management Accounting ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(381, 'Financial Services and Capital Markets ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(382, 'Advanced Tax Management and ?International Taxation ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(383, 'Indirect Tax Laws ', 'custom_subject.png', '62', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(384, 'English Comprehension & Language', 'custom_subject.png', '65', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(385, 'General Knowledge', 'custom_subject.png', '65', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(386, 'Numerical Ability', 'custom_subject.png', '65', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(387, 'Non Verbal & Verbal Reasoning', 'custom_subject.png', '65', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(388, 'Socio Economic Environment', 'custom_subject.png', '65', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(389, 'Chemistry', 'custom_subject.png', '66', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(390, 'Biology', 'custom_subject.png', '66', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(391, 'Physics', 'custom_subject.png', '66', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(392, 'Chemistry', 'custom_subject.png', '67', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(393, 'Biology', 'custom_subject.png', '67', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(394, 'Physics', 'custom_subject.png', '67', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(395, 'English and Logical Reasoning', 'custom_subject.png', '67', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(396, 'Hindi', 'custom_subject.png', '68', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(397, 'General English', 'custom_subject.png', '68', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(398, 'Numerical ability', 'custom_subject.png', '68', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(399, 'General Awareness', 'custom_subject.png', '68', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(400, 'General Awareness', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(401, 'Reasoning ', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(402, 'Quantitative Aptitude', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(403, 'Teaching Aptitude', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(404, 'English ', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(405, 'Hindi', 'custom_subject.png', '69', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(406, 'Child Development & Pedagogy', 'custom_subject.png', '70', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(407, 'English', 'custom_subject.png', '70', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(408, 'Hindi', 'custom_subject.png', '70', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(409, 'Mathematics', 'custom_subject.png', '70', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(410, 'EVS', 'custom_subject.png', '70', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(411, 'Child Development & Pedagogy', 'custom_subject.png', '71', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(412, 'English', 'custom_subject.png', '71', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(413, 'Hindi', 'custom_subject.png', '71', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(414, 'Mathematics', 'custom_subject.png', '71', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(415, 'EVS', 'custom_subject.png', '71', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(416, 'Child Development & Pedagogy', 'custom_subject.png', '72', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(417, 'English', 'custom_subject.png', '72', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(418, 'Hindi', 'custom_subject.png', '72', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(419, 'Mathematics', 'custom_subject.png', '72', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(420, 'EVS', 'custom_subject.png', '72', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(421, 'General Awareness', 'custom_subject.png', '73', 'XYZ', 'XYZ', 'Academic', 1, 2, 1, '0000-00-00 00:00:00'),
(422, 'Reasoning', 'custom_subject.png', '73', 'XYZ', 'XYZ', 'Academic', 1, 1, 1, '0000-00-00 00:00:00'),
(423, 'Mathematics', 'custom_subject.png', '73', 'XYZ', 'XYZ', 'Academic', 1, 2, 1, '0000-00-00 00:00:00'),
(424, 'English', 'custom_subject.png', '73', 'XYZ', 'XYZ', 'Academic', 1, 2, 1, '0000-00-00 00:00:00'),
(425, 'Hindi', 'custom_subject.png', '73', 'XYZ', 'XYZ', 'Academic', 1, 2, 1, '0000-00-00 00:00:00'),
(426, 'Cricket', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 2, 1, 1, '0000-00-00 00:00:00'),
(427, 'Football', 'custom_subject.png', '74', 'XYZ', 'XYZ', 'Non-Academic', 2, 1, 1, '0000-00-00 00:00:00'),
(428, 'toefl', 'custom_subject.png', 'TOEFL', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(429, 'toefl', 'custom_subject.png', 'TOEFL', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(430, 'chem', 'custom_subject.png', 'I', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(431, 'Test Subject', 'subject_adv-3.jpg', 'I', 'CBSE', '2324', 'Academic', 1, 0, 1, '2017-08-18 05:55:33'),
(432, 'snooker', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(433, 'snooker', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(434, 'snooker', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(435, 'snooker', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(437, 'Fielding', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(438, 'Fielding', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(439, 'Fielding', 'custom_subject.png', 'Sports', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(440, 'house', 'custom_subject.png', 'J.B.T', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(441, 'Chess 1', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(442, 'core english', 'custom_subject.png', 'IX', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(443, 'Microbiology', 'custom_subject.png', 'XII', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(444, 'Microbiology', 'custom_subject.png', 'NET', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(446, 'kho kho', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(448, 'sudo', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(451, 'hssh', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(452, 'hssh', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(455, 'jdjdh', 'custom_subject.png', '1', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(456, 'All', 'custom_subject.png', '5', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(457, 'math', 'custom_subject.png', '4', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(458, 'math', 'custom_subject.png', '4', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(462, 'Computer Science', 'custom_subject.png', '27', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(463, 'Yoga', 'custom_subject.png', '74', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(467, 'vocal ', 'custom_subject.png', '75', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(468, 'maths', 'custom_subject.png', '10', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(469, 'aperture ', 'custom_subject.png', '75', '', '', 'Non-Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(478, 'Physics', '', 'XII', 'CBSE', 'Phy', 'Academic', 1, 0, 1, '2017-09-08 12:09:35'),
(479, 'Life Science', '', 'NET', 'XYZ', 'LFS', 'Academic', 1, 0, 1, '2017-09-08 12:14:56'),
(480, 'Lifescience', 'custom_subject.png', '59', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(481, 'Physics', 'custom_subject.png', '12', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(483, 'physics ', 'custom_subject.png', '12', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(484, 'physics ', 'custom_subject.png', '12', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00');
INSERT INTO `ms_subjects` (`id`, `subject`, `image`, `class`, `board`, `subject_code`, `category`, `category_id`, `course_type`, `status`, `created_at`) VALUES
(485, 'organic chemistry ', 'custom_subject.png', '12', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(486, 'HELLOOOOO', '', 'XII', 'C.B.S.E', 'AS-201', 'Academic', 1, 0, 1, '2017-09-15 18:20:28'),
(487, 'physical chemistry', '', 'M.Sc', 'c.b.s.e', 'as-213', 'Academic', 1, 0, 1, '2017-09-16 12:53:33'),
(488, 'organiccgemistry', '', 'M.Sc', 'ukhkuh', 'hyuihy', 'Academic', 1, 0, 1, '2017-09-16 13:02:09'),
(489, 'dfbn', 'custom_subject.png', '63', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00'),
(490, 'Rajniti', 'custom_subject.png', '5', '', '', 'Academic', 0, 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ms_topics`
--

CREATE TABLE `ms_topics` (
  `id` int(11) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `topic_code` varchar(255) NOT NULL,
  `topic_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ms_topics`
--

INSERT INTO `ms_topics` (`id`, `class_id`, `subject_id`, `topic_code`, `topic_name`, `status`, `created_at`) VALUES
(96, '2', '10', '', 'All Topics', 1, '2017-09-05 09:08:58'),
(97, '2', '11', '', 'All Topics', 1, '2017-09-05 09:09:11'),
(98, '2', '12', '', 'All Topics', 1, '2017-09-05 09:09:39'),
(100, '3', '13', '', 'All Topics', 1, '2017-09-05 09:10:59'),
(101, '3', '14', '', 'All Topics', 1, '2017-09-05 09:11:23'),
(102, '3', '15', '', 'All Topics', 1, '2017-09-05 09:12:29'),
(103, '3', '16', '', 'All Topics', 1, '2017-09-05 09:12:58'),
(104, '3', '17', '', 'All Topics', 1, '2017-09-05 09:13:14'),
(105, '3', '18', '', 'All Topics', 1, '2017-09-05 09:13:32'),
(106, '4', '19', '', 'All Topics', 1, '2017-09-05 09:14:54'),
(107, '4', '20', '', 'All Topics', 1, '2017-09-05 09:15:07'),
(108, '4', '21', '', 'All Topics', 1, '2017-09-05 09:15:21'),
(109, '4', '22', '', 'All Topics', 1, '2017-09-05 09:15:34'),
(110, '4', '23', '', 'All Topics', 1, '2017-09-05 09:16:09'),
(111, '4', '24', '', 'All Topics', 1, '2017-09-05 09:16:24'),
(112, '5', '25', '', 'All Topics', 1, '2017-09-05 09:18:34'),
(113, '5', '26', '', 'All Topics', 1, '2017-09-05 09:19:31'),
(114, '5', '27', '', 'All Topics', 1, '2017-09-05 09:19:46'),
(115, '5', '28', '', 'All Topics', 1, '2017-09-05 09:20:00'),
(116, '5', '29', '', 'All Topics', 1, '2017-09-05 09:20:15'),
(118, '5', '30', '', 'All Topics', 1, '2017-09-05 09:22:16'),
(119, '6', '31', '', 'All Topics', 1, '2017-09-05 09:22:38'),
(120, '6', '32', '', 'All Topics', 1, '2017-09-05 09:22:52'),
(121, '6', '33', '', 'All Topics', 1, '2017-09-05 09:23:07'),
(122, '6', '34', '', 'All Topics', 1, '2017-09-05 09:23:20'),
(123, '6', '35', '', 'All Topics', 1, '2017-09-05 09:23:35'),
(124, '6', '36', '', 'All Topics', 1, '2017-09-05 09:23:51'),
(126, '6', '37', '', 'All Topics', 1, '2017-09-05 09:25:27'),
(127, '6', '42', '', 'All Topics', 1, '2017-09-05 09:26:00'),
(128, '6', '43', '', 'All Topics', 1, '2017-09-05 09:26:16'),
(129, '6', '39', '', 'All Topics', 1, '2017-09-05 09:26:51'),
(130, '7', '44', '', 'All Topics', 1, '2017-09-05 09:27:14'),
(131, '7', '45', '', 'All Topics', 1, '2017-09-05 09:28:49'),
(132, '7', '46', '', 'All Topics', 1, '2017-09-05 09:29:03'),
(133, '7', '47', '', 'All Topics', 1, '2017-09-05 09:29:24'),
(134, '7', '48', '', 'All Topics', 1, '2017-09-05 09:29:43'),
(136, '7', '49', '', 'All Topics', 1, '2017-09-05 09:30:43'),
(137, '7', '50', '', 'All Topics', 1, '2017-09-05 09:31:06'),
(138, '7', '52', '', 'All Topics', 1, '2017-09-05 09:31:39'),
(139, '7', '55', '', 'All Topics', 1, '2017-09-05 09:32:05'),
(140, '7', '56', '', 'All Topics', 1, '2017-09-05 09:32:35'),
(141, '8', '57', '', 'All Topics', 1, '2017-09-05 09:40:30'),
(142, '8', '58', '', 'All Topics', 1, '2017-09-05 09:40:44'),
(143, '8', '59', '', 'All Topics', 1, '2017-09-05 09:41:29'),
(144, '8', '60', '', 'All Topics', 1, '2017-09-05 09:41:44'),
(145, '8', '57', '', 'All Topics', 1, '2017-09-05 09:42:29'),
(146, '8', '61', '', 'All Topics', 1, '2017-09-05 09:42:54'),
(147, '8', '62', '', 'All Topics', 1, '2017-09-05 09:43:07'),
(148, '8', '63', '', 'All Topics', 1, '2017-09-05 09:43:39'),
(149, '8', '65', '', 'All Topics', 1, '2017-09-05 09:43:57'),
(150, '8', '68', '', 'All Topics', 1, '2017-09-05 09:44:22'),
(151, '8', '69', '', 'All Topics', 1, '2017-09-05 09:44:40'),
(152, '9', '70', '', 'Grammar', 1, '2017-09-05 09:47:27'),
(153, '9', '70', '', 'Vocabulary', 1, '2017-09-05 09:48:23'),
(154, '9', '71', '', 'Physics', 1, '2017-09-05 09:50:44'),
(155, '9', '71', '', 'Chemistry', 1, '2017-09-05 09:52:26'),
(156, '9', '71', '', 'Biology', 1, '2017-09-05 11:02:54'),
(157, '9', '70', '', 'Literature', 1, '2017-09-05 11:04:10'),
(158, '9', '72', '', 'All Topics', 1, '2017-09-05 11:08:42'),
(159, '9', '73', '', 'All Topics', 1, '2017-09-05 11:08:55'),
(160, '9', '74', '', 'All Topics', 1, '2017-09-05 11:09:11'),
(161, '9', '75', '', 'All Topics', 1, '2017-09-05 11:09:25'),
(162, '9', '76', '', 'All Topics', 1, '2017-09-05 11:09:43'),
(163, '9', '71', '', 'All Topics', 1, '2017-09-05 11:10:22'),
(164, '9', '80', '', 'All Topics', 1, '2017-09-05 11:10:52'),
(165, '9', '81', '', 'All Topics', 1, '2017-09-05 11:11:08'),
(166, '9', '70', '', 'All Topics', 1, '2017-09-05 11:14:08'),
(167, '10', '82', '', 'Grammar', 1, '2017-09-05 11:16:19'),
(168, '10', '82', '', 'Literature', 1, '2017-09-05 11:16:43'),
(169, '10', '82', '', 'Vocabulary', 1, '2017-09-05 11:20:00'),
(170, '10', '82', '', 'All Topics', 1, '2017-09-05 11:20:17'),
(171, '10', '83', '', 'Physics', 1, '2017-09-05 11:20:52'),
(172, '10', '83', '', 'Chemistry', 1, '2017-09-05 11:21:08'),
(173, '10', '83', '', 'Biology', 1, '2017-09-05 11:21:24'),
(174, '10', '83', '', 'All Topics', 1, '2017-09-05 11:21:36'),
(175, '10', '84', '', 'All Topics', 1, '2017-09-05 11:21:50'),
(176, '10', '85', '', 'History', 1, '2017-09-05 11:22:07'),
(177, '10', '85', '', 'Civics', 1, '2017-09-05 11:22:26'),
(178, '10', '85', '', 'Geography', 1, '2017-09-05 11:22:49'),
(179, '10', '85', '', 'All Topics', 1, '2017-09-05 11:23:01'),
(180, '10', '86', '', 'All Topics', 1, '2017-09-05 11:23:18'),
(181, '10', '87', '', 'All Topics', 1, '2017-09-05 11:23:36'),
(182, '10', '88', '', 'All Topics', 1, '2017-09-05 11:23:55'),
(183, '10', '92', '', 'All Topics', 1, '2017-09-05 11:24:15'),
(184, '10', '93', '', 'All Topics', 1, '2017-09-05 11:24:29'),
(185, '11', '97', '', 'Botany', 1, '2017-09-05 11:26:12'),
(186, '11', '97', '', 'Zoology', 1, '2017-09-05 11:26:28'),
(187, '11', '97', '', 'All Topics', 1, '2017-09-05 11:26:47'),
(188, '11', '96', '', 'Inorganic', 1, '2017-09-05 11:27:25'),
(189, '11', '96', '', 'Organic', 1, '2017-09-05 11:27:42'),
(190, '11', '96', '', 'Physical', 1, '2017-09-05 11:27:59'),
(191, '11', '96', '', 'All Topics', 1, '2017-09-05 11:28:11'),
(192, '11', '94', '', 'All Topics', 1, '2017-09-05 11:34:19'),
(193, '11', '98', '', 'All Topics', 1, '2017-09-05 11:34:39'),
(194, '11', '99', '', 'All Topics', 1, '2017-09-05 11:34:52'),
(195, '11', '100', '', 'All Topics', 1, '2017-09-05 11:35:14'),
(196, '11', '101', '', 'All Topics', 1, '2017-09-05 11:35:53'),
(197, '11', '102', '', 'All Topics', 1, '2017-09-05 11:36:19'),
(198, '11', '103', '', 'All Topics', 1, '2017-09-05 11:37:01'),
(199, '11', '104', '', 'All Topics', 1, '2017-09-05 11:37:32'),
(200, '11', '105', '', 'All Topics', 1, '2017-09-05 11:37:51'),
(201, '11', '105', '', 'All Topics', 1, '2017-09-05 11:38:29'),
(202, '12', '112', '', 'All Topics', 1, '2017-09-05 11:40:15'),
(203, '12', '113', '', 'Organic', 1, '2017-09-05 11:41:09'),
(204, '12', '113', '', 'Inorganic', 1, '2017-09-05 11:41:38'),
(205, '12', '113', '', 'Physical', 1, '2017-09-05 11:42:00'),
(206, '12', '113', '', 'All Topics', 1, '2017-09-05 11:42:50'),
(207, '12', '114', '', 'Zoology', 1, '2017-09-05 11:43:15'),
(208, '12', '114', '', 'Botany', 1, '2017-09-05 11:43:27'),
(209, '12', '114', '', 'All Topics', 1, '2017-09-05 11:44:21'),
(210, '12', '115', '', 'All Topics', 1, '2017-09-05 11:44:37'),
(211, '12', '116', '', 'All Topics', 1, '2017-09-05 11:44:58'),
(212, '12', '117', '', 'All Topics', 1, '2017-09-05 11:45:16'),
(213, '12', '118', '', 'All Topics', 1, '2017-09-05 11:45:38'),
(214, '12', '119', '', 'All Topics', 1, '2017-09-05 11:45:54'),
(215, '12', '120', '', 'Literature', 1, '2017-09-05 11:46:22'),
(216, '12', '120', '', 'Grammar', 1, '2017-09-05 11:46:55'),
(217, '12', '120', '', 'All Topics', 1, '2017-09-05 11:47:12'),
(218, '12', '121', '', 'All Topics', 1, '2017-09-05 11:47:30'),
(219, '', '463', '', 'Yoga for life', 1, '0000-00-00 00:00:00'),
(220, '', '351', '', 'Computer science and data enterprise ', 1, '0000-00-00 00:00:00'),
(221, '2', '7', '', 'All Topics', 1, '2017-09-06 12:08:49'),
(222, '1', '1', '', 'All Topics', 1, '2017-09-06 12:09:37'),
(223, '1', '3', '', 'All Topics', 1, '2017-09-06 12:10:46'),
(224, '1', '4', '', 'All Topics', 1, '2017-09-06 12:11:04'),
(225, '1', '5', '', 'All Topics', 1, '2017-09-06 12:13:14'),
(226, '', '160', '', 'play all chords on guitar', 1, '0000-00-00 00:00:00'),
(227, '', '467', '', 'sargam and alankar in classical music', 1, '0000-00-00 00:00:00'),
(228, '', '362', '', 'Computer science and data enterprise ', 1, '0000-00-00 00:00:00'),
(229, '', '469', '', 'aperture and focus ', 1, '0000-00-00 00:00:00'),
(230, '11', '94', '', 'Physics and Measurement', 1, '2017-09-07 14:10:59'),
(231, '11', '94', '', 'Physics and Measurement', 1, '2017-09-07 14:11:21'),
(232, '11', '94', '', 'Kinematics', 1, '2017-09-07 14:11:59'),
(233, '11', '94', '', 'Laws of Motion', 1, '2017-09-07 14:12:22'),
(234, '11', '94', '', 'Work, Energy and Power', 1, '2017-09-07 14:12:47'),
(235, '11', '94', '', 'Motion of System of Particles and Rigid Body', 1, '2017-09-07 14:13:44'),
(236, '11', '94', '', 'Gravitation', 1, '2017-09-07 14:14:15'),
(237, '11', '94', '', 'Properties of Bulk Matter', 1, '2017-09-07 14:14:56'),
(238, '11', '94', '', 'Thermodynamics', 1, '2017-09-07 14:15:21'),
(239, '11', '94', '', 'Behavior of Perfect Gases and Kinematics Theory of Gases', 1, '2017-09-07 14:16:14'),
(240, '11', '94', '', 'Oscillations', 1, '2017-09-07 14:18:49'),
(241, '11', '95', '', 'Sets and Function', 1, '2017-09-07 14:19:24'),
(242, '11', '95', '', 'Algebra', 1, '2017-09-07 14:19:48'),
(243, '11', '95', '', 'Coordinate Geometry', 1, '2017-09-07 14:20:18'),
(244, '11', '95', '', 'Calculus', 1, '2017-09-07 14:20:42'),
(245, '11', '95', '', 'Mathematical Reasoning', 1, '2017-09-07 14:21:12'),
(246, '11', '95', '', 'Statistics and Probability', 1, '2017-09-07 14:22:22'),
(247, '11', '96', '', 'Some Basic Concept of Chemistry', 1, '2017-09-07 14:26:22'),
(248, '11', '96', '', 'Structure of Atom', 1, '2017-09-07 14:26:46'),
(249, '11', '96', '', 'Classification of Elements and Periodicity in Properties', 1, '2017-09-07 14:33:47'),
(250, '11', '96', '', 'Chemical bonding and Molecular structure', 1, '2017-09-07 14:34:14'),
(251, '11', '96', '', 'States of matter', 1, '2017-09-07 14:34:43'),
(252, '11', '96', '', 'Chemical Thermodynamics', 1, '2017-09-07 14:35:09'),
(253, '11', '96', '', 'Equilibrium', 1, '2017-09-07 14:35:40'),
(254, '11', '96', '', 'Environmental Chemistry', 1, '2017-09-07 14:36:41'),
(255, '11', '97', '', 'Diversity of Living organisms', 1, '2017-09-07 14:37:23'),
(256, '11', '97', '', 'Structural organizations in plants and animals', 1, '2017-09-07 14:37:49'),
(257, '11', '97', '', 'Cell structure and plant function', 1, '2017-09-07 14:41:55'),
(258, '11', '97', '', 'Plant physiology and human physiology', 1, '2017-09-07 14:42:26'),
(259, '11', '100', '', 'Theoretical Framework', 1, '2017-09-07 14:56:13'),
(260, '11', '100', '', 'Accounting Process', 1, '2017-09-07 14:56:44'),
(261, '11', '100', '', 'Financial statements of sole proprietorship from complete and incomplete records', 1, '2017-09-07 14:57:41'),
(262, '11', '100', '', 'Financial statements of not for profit', 1, '2017-09-07 14:58:11'),
(263, '11', '100', '', 'Organization and Computers in accounting', 1, '2017-09-07 15:24:47'),
(264, '11', '101', '', 'Nature and purpose of business', 1, '2017-09-07 15:25:32'),
(265, '11', '101', '', 'Form of business organization', 1, '2017-09-07 15:26:06'),
(266, '11', '101', '', 'Public partner and Global enterprise', 1, '2017-09-07 15:26:29'),
(267, '11', '101', '', 'Business service', 1, '2017-09-07 15:30:13'),
(268, '11', '101', '', 'Emerging modes of business', 1, '2017-09-07 15:30:52'),
(269, '11', '101', '', 'Social responsibility of business and business ethics', 1, '2017-09-07 15:31:40'),
(270, '11', '101', '', 'Source of business finance', 1, '2017-09-07 15:32:03'),
(271, '11', '101', '', 'Small business', 1, '2017-09-07 15:32:27'),
(272, '11', '101', '', 'Small business', 1, '2017-09-07 15:33:01'),
(273, '11', '101', '', 'Internal trade', 1, '2017-09-07 15:33:42'),
(274, '11', '101', '', 'International Business', 1, '2017-09-07 15:34:05'),
(275, '11', '101', '', 'Project Work', 1, '2017-09-07 15:34:44'),
(276, '11', '102', '', 'Statistics of economics', 1, '2017-09-07 15:35:15'),
(277, '11', '102', '', 'Indian economic and development', 1, '2017-09-07 15:36:06'),
(278, '12', '112', '', 'Algebra', 1, '2017-09-07 15:36:42'),
(279, '12', '112', '', 'Calculus', 1, '2017-09-07 15:36:59'),
(280, '12', '112', '', 'Vectors and Three dimensional geometry', 1, '2017-09-07 15:38:48'),
(281, '12', '112', '', 'Linear Programming', 1, '2017-09-07 15:39:10'),
(282, '12', '112', '', 'Probibility', 1, '2017-09-07 15:39:29'),
(283, '12', '112', '', 'Relation and Functions', 1, '2017-09-07 15:40:04'),
(284, '12', '113', '', 'Solid state', 1, '2017-09-07 16:09:20'),
(285, '12', '113', '', 'Solutions', 1, '2017-09-07 16:09:50'),
(286, '12', '113', '', 'Electrochemistry', 1, '2017-09-07 16:10:15'),
(287, '12', '113', '', 'Chemical Kinematics', 1, '2017-09-07 16:10:40'),
(288, '12', '114', '', 'Reproduction genetics and evolution', 1, '2017-09-07 16:20:37'),
(289, '12', '114', '', 'Biology and human welfare', 1, '2017-09-07 16:21:08'),
(290, '12', '114', '', 'Biotechnology and its application', 1, '2017-09-07 16:21:47'),
(291, '12', '114', '', 'Ecology and environment', 1, '2017-09-07 16:22:06'),
(292, '12', '119', '', 'Introductory Microeconomics', 1, '2017-09-07 16:22:40'),
(293, '12', '119', '', 'Introductory Macroeconomics', 1, '2017-09-07 16:23:05'),
(294, '12', '118', '', 'Principles and Functions of Management', 1, '2017-09-07 16:24:13'),
(295, '12', '118', '', 'Business and Finance and Marketing', 1, '2017-09-07 16:24:39'),
(305, '', '481', '', 'Electrostatics', 1, '0000-00-00 00:00:00'),
(306, '12', '481', '', 'Current Electricity', 1, '2017-09-08 12:21:41'),
(307, '12', '481', '', 'Magnetic effects of Current and Magnetism', 1, '2017-09-08 12:22:15'),
(308, '12', '481', '', 'Electromagnetic Induction and Alternating Currents', 1, '2017-09-08 12:22:45'),
(309, '12', '481', '', 'Electromagnetic waves', 1, '2017-09-08 12:23:09'),
(310, '12', '481', '', 'Optics', 1, '2017-09-08 12:23:32'),
(311, '12', '481', '', 'Dual natutre of radiation and matter', 1, '2017-09-08 12:23:54'),
(312, '12', '481', '', 'Atoms and Nuclei', 1, '2017-09-08 12:24:13'),
(313, '12', '481', '', 'Electronic devices', 1, '2017-09-08 12:24:33'),
(314, '12', '481', '', 'Communication system', 1, '2017-09-08 12:24:54'),
(315, '12', '481', '', 'All Topics', 1, '2017-09-08 12:29:45'),
(316, '13', '168', '', 'Sets', 1, '2017-09-08 16:58:26'),
(317, '13', '168', '', ' Relations and Functions ', 1, '2017-09-08 16:58:26'),
(318, '13', '168', '', 'Complex Numbers and Quadratic Equations', 1, '2017-09-08 16:58:26'),
(319, '13', '168', '', 'Matrices and Determinants ', 1, '2017-09-08 16:58:26'),
(320, '13', '168', '', 'Permutations and Combinations', 1, '2017-09-08 16:58:26'),
(321, '13', '168', '', 'Mathematical Induction', 1, '2017-09-08 16:58:26'),
(322, '13', '168', '', 'Sequences and Series', 1, '2017-09-08 16:58:26'),
(323, '13', '168', '', 'Binomial Theorem', 1, '2017-09-08 16:58:26'),
(324, '13', '168', '', 'Limit', 1, '2017-09-08 16:58:26'),
(325, '13', '168', '', ' Continuity and Differentiability', 1, '2017-09-08 16:58:26'),
(326, '13', '168', '', 'Integral Calculus', 1, '2017-09-08 16:58:26'),
(327, '13', '168', '', 'Differential Equations', 1, '2017-09-08 16:58:26'),
(328, '13', '168', '', 'Co-Ordinate Geometry', 1, '2017-09-08 16:58:26'),
(329, '13', '168', '', 'Three Dimensional Geometry', 1, '2017-09-08 16:58:26'),
(330, '13', '168', '', 'Vector Algebra', 1, '2017-09-08 16:58:26'),
(331, '13', '168', '', 'Statistics and probability', 1, '2017-09-08 16:58:26'),
(332, '13', '168', '', 'Trigonometry', 1, '2017-09-08 16:58:26'),
(333, '13', '168', '', 'Mathematical Reasoning', 1, '2017-09-08 16:58:26'),
(334, '13', '168', '', 'All Topics', 1, '2017-09-08 16:59:32'),
(335, '13', '169', '', 'Physics and Measurement ', 1, '2017-09-08 17:07:14'),
(336, '13', '169', '', 'Kinematics', 1, '2017-09-08 17:07:14'),
(337, '13', '169', '', 'Laws of Motion', 1, '2017-09-08 17:07:14'),
(338, '13', '169', '', 'Work energy and Power', 1, '2017-09-08 17:07:14'),
(339, '13', '169', '', 'Rotational motion', 1, '2017-09-08 17:07:14'),
(340, '13', '169', '', 'Gravitation', 1, '2017-09-08 17:07:14'),
(341, '13', '169', '', 'Properties of solid and liquids', 1, '2017-09-08 17:07:14'),
(342, '13', '169', '', 'Thermodynamics', 1, '2017-09-08 17:07:14'),
(343, '13', '169', '', 'Kinetic Theory of Gases', 1, '2017-09-08 17:07:14'),
(344, '13', '169', '', 'Oscillations and wave', 1, '2017-09-08 17:07:14'),
(345, '13', '169', '', 'Electrostatics', 1, '2017-09-08 17:07:14'),
(346, '13', '169', '', 'Current Electricity', 1, '2017-09-08 17:07:14'),
(347, '13', '169', '', 'Magnetic Effects of Current and Magnetism', 1, '2017-09-08 17:07:14'),
(348, '13', '169', '', 'Electromagnetic Induction and Alternating Currents', 1, '2017-09-08 17:07:14'),
(349, '13', '169', '', 'Electromagnetic Waves', 1, '2017-09-08 17:07:15'),
(350, '13', '169', '', 'Optics', 1, '2017-09-08 17:07:15'),
(351, '13', '169', '', 'Dual Nature of Matter and Radiation', 1, '2017-09-08 17:07:15'),
(352, '13', '169', '', 'Atoms and nuclei', 1, '2017-09-08 17:07:15'),
(353, '13', '169', '', 'Electronic Devices', 1, '2017-09-08 17:07:15'),
(354, '13', '169', '', 'Communication Systems ', 1, '2017-09-08 17:07:15'),
(355, '13', '169', '', 'All Topics', 1, '2017-09-08 17:07:15'),
(356, '', '41', '', 'German Language', 1, '0000-00-00 00:00:00'),
(357, '', '406', '', 'fgoo', 1, '0000-00-00 00:00:00'),
(358, '', '490', '', 'Dalit rajniti', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_question`
--

CREATE TABLE `multiple_question` (
  `id` int(11) NOT NULL,
  `types` varchar(255) NOT NULL,
  `ques` text NOT NULL,
  `option_a` varchar(255) NOT NULL,
  `option_b` varchar(255) NOT NULL,
  `option_c` varchar(255) NOT NULL,
  `option_d` varchar(255) NOT NULL,
  `ans` varchar(255) NOT NULL,
  `q_id` int(11) NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_question`
--

INSERT INTO `multiple_question` (`id`, `types`, `ques`, `option_a`, `option_b`, `option_c`, `option_d`, `ans`, `q_id`, `added_date`) VALUES
(27, 'Quiz', 'While talking about the human eye, what is the \"pupil\"?', ' A light-sensitive screen', 'One of the unfortunate victims of the System', 'A viscous fluid', 'A hole of variable size', 'D', 93, '21-03-2018 02:49:36'),
(28, 'Quiz', 'What are the three parts of the ear?', ' Drum cymbol and gitar', '  Middle ear, inner ear, outer ear', '  Inner ear , ear drum, middle ear', 'none', 'A', 94, '21-03-2018 02:57:00'),
(29, 'Question', 'Who is the founder of Microsoft?', '', '', '', '', '', 95, '21-03-2018 02:58:42'),
(32, 'Quiz', 'he average of first 50 natural numbers is …………. .', ' 25.30', '. 25.5', ' 25.00', ' 12.25', 'B', 97, '21-03-2018 03:07:51'),
(33, 'Quiz', 'Consider the FDs given in above question. The relation R is', 'in 1NF, but not in 2NF.', 'in 2NF, but not in 3NF', 'in 3NF, but not in BCNF.', 'in BCNF', 'A', 98, '21-03-2018 03:12:18'),
(34, 'Quiz', 'Which of the following is TRUE?', 'Every relation in 3NF is also in BCNF', 'A relation R is in 3NF if every non-prime attribute of R is fully functionally dependent on every key of R', 'Every relation in BCNF is also in 3NF D', 'No relation can be in both BCNF and 3NF', 'C', 98, '21-03-2018 03:12:19'),
(35, 'Question', 'Which one of the following options is the closest in meaning to the word given below? Nadir', '', '', '', '', '', 99, '21-03-2018 03:14:26'),
(36, 'Question', 'Complete the sentence: Universalism is to particularism as diffuseness is to _________________', '', '', '', '', '', 99, '21-03-2018 03:14:26'),
(37, 'Question', 'After several defeats in wars, Robert Bruce went in exile and wanted to commit suicide. Just before committing suicide, he came across a spider attempting tirelessly to have its net. Time and again, the spider failed but that did not deter it to refrain from making attempts. Such attempts by the spider made Bruce curious.', '', '', '', '', '', 99, '21-03-2018 03:14:26'),
(38, 'Question', 'Choose the most appropriate alternative from the options given below to complete the following sentence: Despite several ––––––––– the mission succeeded in its attempt to resolve the ', '', '', '', '', '', 99, '21-03-2018 03:14:26'),
(39, 'Question', 'Which one of the following options is the closest in meaning to the word given below? Mitigate', '', '', '', '', '', 99, '21-03-2018 03:14:26'),
(49, 'Quiz', 'Select the equation represented by the graph below.', ' Y = cos(x)', 'Y = sin(x)', 'Y = tan(x)', 'Y = -sin(x)', 'C', 103, '21-03-2018 04:33:33'),
(50, 'Quiz', 'Select the equation represented by the graph below.', '  Y = cos(x)', 'Y = sin(x)', 'Y = tan(x)', 'Y = -sin(x)  E.  Y = 2cos(x)', 'B', 103, '21-03-2018 04:33:33'),
(51, 'Quiz', 'Value of , for  = 1 Where 0° <  < 90° is:', '.  60°', '.  50°', '.  70°', '.  90°', 'A', 103, '21-03-2018 04:33:33'),
(52, 'Quiz', 'Select the equation represented by the graph below.', ' Y = cos(x)', 'Y = sin(x)', 'Y = tan(x)', 'Y = -sin(x)', 'C', 104, '21-03-2018 04:35:05'),
(53, 'Quiz', 'Value of sec2 26° – cot2 64° is:', '70°', '90°', '60°', '80°', 'A', 105, '21-03-2018 04:37:40'),
(54, 'Quiz', 'Value of sec2 26° – cot2 64° is:', '40°', '70°', '90°', '60°', 'A', 105, '21-03-2018 04:37:40'),
(55, 'Quiz', 'Value of sec2 26° – cot2 64° is:', '30°', '20°', '90°', '80°', 'C', 105, '21-03-2018 04:37:41'),
(59, 'Quiz', 'dfdf', 'd', 'd', 'gd', 'dg', 'C', 107, '23-03-2018 03:49:01'),
(60, 'Quiz', 'dgdfg', 'e', 'dd', '', '', 'D', 107, '23-03-2018 03:49:01'),
(61, 'Quiz', 'sddfg', 'dd', 'dfg', 'dfg', 'dfgh', 'C', 108, '23-03-2018 03:50:40'),
(62, 'Quiz', 'dfgfd', 'dd', 'fg', 'gf', 'fgf', 'C', 108, '23-03-2018 03:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `myplans`
--

CREATE TABLE `myplans` (
  `id` int(10) NOT NULL,
  `plan_name` varchar(50) NOT NULL,
  `plan_code` varchar(25) NOT NULL,
  `plan_amt` int(10) NOT NULL,
  `plan_detail` text NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `na_gallery`
--

CREATE TABLE `na_gallery` (
  `id` int(11) NOT NULL,
  `academy_id` varchar(255) NOT NULL,
  `img_url` text NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `na_gallery`
--

INSERT INTO `na_gallery` (`id`, `academy_id`, `img_url`, `added_date`) VALUES
(5, '251', 'na_251_4036.jpg', '27-03-2018 14:26:12'),
(4, '251', 'na_251_2008.jpg', '27-03-2018 14:16:28'),
(3, '251', 'na_251_1186.jpg', '27-03-2018 14:15:33'),
(6, '251', 'na_251_7376.jpg', '27-03-2018 14:38:20'),
(7, '251', 'na_251_2519.jpg', '27-03-2018 14:48:28'),
(8, '251', 'na_251_5366.jpg', '27-03-2018 15:14:11'),
(9, '251', 'na_251_5496.jpg', '27-03-2018 15:16:05'),
(10, '251', 'na_251_9455.jpg', '27-03-2018 15:17:39'),
(11, '251', 'na_251_6282.jpg', '27-03-2018 15:17:59'),
(12, '251', 'na_251_2086.jpg', '27-03-2018 15:18:08'),
(13, '251', 'na_251_8657.jpg', '27-03-2018 15:18:20'),
(14, '251', 'na_251_4897.jpg', '27-03-2018 15:18:40'),
(15, '251', 'na_251_9793.jpg', '30-03-2018 13:58:35');

-- --------------------------------------------------------

--
-- Table structure for table `non_academic_coach`
--

CREATE TABLE `non_academic_coach` (
  `id` int(11) NOT NULL,
  `academy_id` int(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `activity_name` varchar(255) NOT NULL,
  `activity_id` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `added` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `non_academic_coach`
--

INSERT INTO `non_academic_coach` (`id`, `academy_id`, `name`, `experience`, `activity_name`, `activity_id`, `gender`, `status`, `added`) VALUES
(1, 3, 'Uvjbb', 'Hjb', 'Gymnastics', '1394', 'Male', '1', '2017-12-20 16:33:26'),
(2, 3, 'Uvyb', 'Hhh', 'Weight Lifting', '1397', 'Female', '1', '2017-12-20 16:35:18'),
(3, 3, 'Yh', 'Hb.N', 'Gymnastics', '1394', 'Male', '1', '2017-12-20 16:35:28'),
(4, 3, 'Ycyv', 'Yg', 'Weight Lifting', '1397', 'Male', '1', '2017-12-20 16:35:59'),
(5, 4, 'Saurabh', '3', 'Computer', '1424', 'Male', '1', '2017-12-20 17:28:31'),
(6, 4, 'Nik', '5', 'Computer', '1424', 'Male', '1', '2017-12-20 17:28:48'),
(7, 5, 'Saurabh', '6', 'Football', '1392', 'Male', '1', '2017-12-20 17:38:14'),
(8, 5, 'Tctct', '2828', 'Base Ball', '1395', 'Male', '1', '2017-12-21 14:03:57'),
(9, 5, 'Fccg', '388596', 'Football', '1392', 'Male', '1', '2017-12-21 14:05:51'),
(10, 5, 'Gyyg', '66', 'Base Ball', '1395', 'Male', '1', '2017-12-21 14:10:33'),
(11, 5, 'Vikash', '23', 'Football', '1392', 'Female', '1', '2017-12-21 14:20:27'),
(12, 5, 'Amit', '3', 'Base Ball', '1395', 'Male', '1', '2017-12-21 14:22:11'),
(13, 10, 'Amit K', '5', 'Kho-kho', '1408', 'Male', '1', '2017-12-21 17:55:21'),
(14, 10, 'Shew', '6', 'Basket ball', '1393', 'Male', '1', '2017-12-21 17:55:35'),
(15, 11, 'Harish', '2', 'Rugby', '1396', 'Male', '1', '2017-12-21 17:59:49'),
(16, 11, 'Manish', '3', 'Weight Lifting', '1397', 'Male', '1', '2017-12-21 18:00:04'),
(17, 13, 'Drona', '12', 'Photography', '1426', 'Male', '1', '2017-12-22 11:30:19'),
(18, 5, 'Saurabh', '10', 'Base Ball', '1395', 'Male', '1', '2017-12-22 11:50:06'),
(19, 14, 'Fhjgjg', '68', 'Martial Arts', '1413', 'Male', '1', '2017-12-22 12:00:27'),
(20, 14, 'Gshd', '56', 'Martial Arts', '1413', 'Male', '1', '2017-12-22 12:01:26'),
(21, 16, 'Rakesh', '3', 'Football', '1392', 'Male', '1', '2017-12-22 13:16:27'),
(22, 17, 'om gaurav singh', '5', 'Photography', '1426', 'Male', '1', '2017-12-22 13:24:55'),
(23, 17, 'om gaurav singh', '5', 'Photography', '1426', 'Male', '1', '2017-12-22 13:24:58'),
(24, 12, 'sweta', '80', 'Cycling', '1399', 'Female', '1', '2017-12-22 14:06:58'),
(25, 12, 'Hello', '5', 'Rugby', '1396', 'Male', '1', '2017-12-22 14:48:10'),
(26, 17, 'gaurav singh', '10', 'Photography', '1426', 'Male', '1', '2017-12-22 17:22:32'),
(27, 20, 'Amit Kumar', '6', 'Basket ball', '1393', 'Male', '1', '2017-12-22 19:00:08'),
(28, 21, 'Amrendra  Babaubai', '10', 'Base Ball', '1395', 'Male', '1', '2017-12-22 19:03:14'),
(29, 21, 'Bhallaldev', '50', 'Weight Lifting', '1397', 'Male', '1', '2017-12-22 19:15:42'),
(30, 22, 'Bd', '61', 'Cricket', '1391', 'Female', '1', '2017-12-22 19:19:22'),
(31, 23, 'Bahubali', '5', 'Judo', '1398', 'Male', '1', '2017-12-22 19:32:05'),
(32, 23, 'Bhalla', '5', 'Cycling', '1399', 'Male', '1', '2017-12-22 19:32:26'),
(33, 23, 'Bdnd', '87', 'Weight Lifting', '1397', 'Male', '1', '2017-12-22 19:33:54'),
(34, 2, 'Suresh', '2', 'Boxing', '1400', 'Male', '1', '2017-12-22 22:44:03'),
(35, 2, 'Ramesh', '1', 'Weight Lifting', '1397', 'Male', '1', '2017-12-22 22:44:37'),
(36, 2, 'Rahul', '3', 'Gymnastics', '1394', 'Male', '1', '2017-12-23 13:50:57'),
(37, 20, 'Fing Chung', '10', 'Judo', '1398', 'Male', '1', '2017-12-23 13:58:57'),
(38, 25, 'Chung Lee', '20', 'Martial Arts', '1413', 'Female', '1', '2017-12-23 14:02:37'),
(39, 25, 'Sahina', '10', 'Badminton', '1402', 'Female', '1', '2017-12-23 14:03:00'),
(40, 2, 'Raghav', '2', 'Judo', '1398', 'Male', '1', '2017-12-23 14:16:00'),
(41, 2, 'Mr Perfect', '6', 'Gymnastics', '1394', 'Male', '1', '2017-12-23 14:31:26'),
(42, 26, 'S K Singh', '7', 'Writing/ Reading', '1422', 'Male', '1', '2017-12-23 16:09:13'),
(43, 28, 'Rahul', '2', 'Cricket', '1391', 'Male', '1', '2017-12-23 18:15:57'),
(44, 28, 'Ram', '2', 'Weight Lifting', '1397', 'Male', '1', '2017-12-23 18:21:29'),
(45, 28, 'Ram', '2', 'Cricket', '1391', 'Male', '1', '2017-12-23 18:35:41'),
(46, 28, 'Ram', '2', 'Cricket', '1391', 'Male', '1', '2017-12-23 20:23:33'),
(47, 15, 'Shikhar Sir', '8', 'Dance', '1420', 'Male', '1', '2017-12-23 21:33:39'),
(48, 23, 'Apporv Srivastava', '10', 'Weight Lifting', '1397', 'Male', '1', '2017-12-24 00:01:04'),
(49, 23, 'Heooo', '10', 'Weight Lifting', '1397', 'Female', '1', '2017-12-24 00:01:20'),
(50, 15, 'Amit sir', '2', 'Art', '1430', 'Male', '1', '2017-12-24 14:56:18'),
(51, 30, 'Ram', '2', 'Cricket', '1391', 'Male', '1', '2017-12-24 15:02:46'),
(52, 30, 'Ram', '2', 'Cricket', '1391', 'Male', '1', '2017-12-24 15:02:46'),
(53, 30, 'Mfjx', '2', 'Cricket', '1391', 'Male', '1', '2017-12-24 15:27:24'),
(54, 15, 'Haan', '2', 'Art', '1430', 'Male', '1', '2017-12-24 19:12:41'),
(55, 2, 'Wjejrk', '65', 'Rugby', '1396', 'Male', '1', '2017-12-25 10:18:18'),
(56, 2, 'Iffgkgkg', '53', 'Base Ball', '1395', 'Male', '1', '2017-12-25 10:32:15'),
(57, 34, 'Bv', '10', 'Basket ball', '1393', 'Male', '1', '2017-12-26 10:47:59'),
(58, 34, 'Awe', '10', 'Gymnastics', '1394', 'Male', '1', '2017-12-26 10:51:02'),
(59, 34, 'Hsjsh', '19', 'Rugby', '1396', 'Female', '1', '2017-12-26 10:53:23'),
(60, 35, 'Bholu', '3', 'Cycling', '1399', 'Female', '1', '2017-12-26 10:55:27'),
(61, 34, 'Swe', '8', 'Base Ball', '1395', 'Female', '1', '2017-12-26 10:55:37'),
(62, 37, 'Rahul', '5', 'Dance', '1420', 'Male', '1', '2017-12-26 11:36:19'),
(63, 41, 'Tansen', '5', 'Fashion Designing', '1429', 'Male', '1', '2017-12-26 11:57:16'),
(64, 41, 'Alo', '2', 'Fashion Designing', '1429', 'Male', '1', '2017-12-26 11:57:27'),
(65, 41, 'Helo', '5', 'Fashion Designing', '1429', 'Male', '1', '2017-12-26 11:57:56'),
(66, 35, 'Ry', '53', 'Cycling', '1399', 'Male', '1', '2017-12-26 12:02:35'),
(67, 35, 'Ru', '86', 'Football', '1392', 'Female', '1', '2017-12-26 12:07:13'),
(68, 2, 'Ertg', '5', 'Judo', '1398', 'Male', '1', '2017-12-26 12:58:22'),
(69, 43, 'Satyender Pal', '10', 'Cricket', '1391', 'Male', '1', '2017-12-26 13:28:26'),
(70, 42, 'Rajan', '5', 'Cricket', '1391', 'Male', '1', '2017-12-26 13:58:52'),
(71, 18, 'harinder singh', '10', 'Art', '1430', 'Female', '1', '2017-12-26 15:09:32'),
(72, 45, 'Harinder Singh', '5', 'Cycling', '1399', 'Female', '1', '2017-12-26 15:13:05'),
(73, 45, 'Thg', '5', 'Judo', '1398', 'Female', '1', '2017-12-26 15:15:40'),
(74, 45, 'Smriti Kushwaga', '99', 'Art', '1430', 'Male', '1', '2017-12-26 15:17:43'),
(75, 45, 'Hjj', '50', 'Photography', '1426', 'Female', '1', '2017-12-26 15:21:57'),
(76, 45, 'Hello', '88', 'Fashion Designing', '1429', 'Male', '1', '2017-12-26 15:23:33'),
(77, 44, 'Hr Ruge', '55', 'Gymnastics', '1394', 'Male', '1', '2017-12-27 12:31:02'),
(78, 47, 'Saurabh', '2', 'Base Ball', '1395', 'Female', '1', '2017-12-27 12:39:28'),
(79, 45, 'fggv', '80', 'Cricket', '1391', 'Male', '1', '2017-12-27 12:48:13'),
(80, 2, 'Fhj', '2', 'Judo', '1398', 'Male', '1', '2017-12-27 14:23:41'),
(81, 9, 'Simi', '2', 'Cycling', '1399', 'Female', '1', '2017-12-27 17:05:51'),
(82, 45, 'Ashish', '5', 'Dance', '1420', 'Male', '1', '2017-12-27 18:51:39'),
(83, 45, 'Sunny', '5', 'Martial Arts', '1413', 'Male', '1', '2017-12-27 18:57:59'),
(84, 45, 'Khatri', '6', 'Weight Lifting', '1397', 'Male', '1', '2017-12-27 18:58:22'),
(85, 45, 'sidhant', '5', 'Cricket', '1391', 'Male', '1', '2017-12-27 19:01:53'),
(86, 45, 'Pixel', '5', 'Cricket', '1391', 'Male', '1', '2017-12-28 10:45:32'),
(87, 45, 'Pixel', '8', 'Rugby', '1396', 'Male', '1', '2017-12-28 10:46:13'),
(88, 45, 'asd', '10', 'Football', '1392', 'Male', '1', '2017-12-28 10:51:19'),
(89, 56, 'Harinder', '2', 'Cricket', '1391', 'Male', '1', '2017-12-28 11:02:30'),
(90, 60, 'hariu', '22', 'Squash', '1404', 'Male', '1', '2017-12-29 18:29:18'),
(91, 61, 'Tt', '5', 'Judo', '1398', 'Male', '1', '2017-12-30 14:32:35'),
(92, 61, 'Ashi', '5', 'Judo', '1398', 'Male', '1', '2017-12-31 18:19:08'),
(93, 30, 'Arjun', '2', 'Cricket', '1391', 'Male', '1', '2017-12-31 21:55:35'),
(94, 65, 'Ujjwal', '3', 'Weight Lifting', '1397', 'Male', '1', '2018-01-01 19:39:15'),
(95, 24, 'Jyoti', '3', 'Computer', '1424', 'Female', '1', '2018-01-02 12:30:28'),
(96, 66, 'Anshu Singh', '15', 'Cooking', '1427', 'Female', '1', '2018-01-02 13:55:56'),
(97, 66, 'Ttg', '5', 'Cricket', '1391', 'Male', '1', '2018-01-02 14:17:08'),
(98, 61, 'Gshej', '6', 'Judo', '1398', 'Male', '1', '2018-01-02 14:28:34'),
(99, 69, 'Hgdc', '1', 'Cricket', '1391', 'Male', '1', '2018-01-02 14:29:47'),
(100, 9, 'Et', '2', 'Cycling', '1399', 'Male', '1', '2018-01-02 16:24:59'),
(101, 46, 'Pawan Kumar', '8', 'Writing/ Reading', '1422', 'Male', '1', '2018-01-31 08:52:46'),
(102, 147, 'Satyender Pal', '5', 'Cricket', '1391', 'Male', '1', '2018-02-01 12:36:27'),
(103, 147, 'Satyender', '8', 'Basket ball', '1393', 'Male', '1', '2018-02-01 12:36:45'),
(104, 244, 'Gaurav', '30', 'Football', '1392', 'Male', '1', '2018-02-17 14:32:28'),
(105, 244, 'Saurabh', '28', 'Rugby', '1396', 'Male', '1', '2018-02-17 16:24:22'),
(106, 244, 'Tilak', '23', 'Judo', '1398', 'Male', '1', '2018-02-17 16:30:28'),
(107, 244, 'Tushar', '39', 'Base Ball', '1395', 'Male', '1', '2018-02-17 16:44:36'),
(108, 244, 'Jdo', '36', 'Judo', '1398', 'Male', '1', '2018-02-17 17:09:05'),
(109, 244, 'Krie', '33', 'Judo', '1398', 'Female', '1', '2018-02-17 17:29:13'),
(110, 244, 'Yashima', '3', 'Judo', '1398', 'Female', '1', '2018-02-17 17:31:18'),
(111, 244, 'Hiwxxxwx', '61', 'Football', '1392', 'Male', '1', '2018-02-17 18:02:56'),
(112, 244, 'Uxucuc', '2', 'Cricket', '1391', 'Male', '1', '2018-02-17 18:08:03'),
(113, 244, 'Salman', '39', 'Football', '1392', 'Male', '1', '2018-02-17 18:37:54'),
(114, 244, 'Thchdh', '35', 'Basket ball', '1393', 'Male', '1', '2018-02-19 10:35:06'),
(115, 245, 'Vikash Modi', '20', 'Skating', '1890', 'Male', '1', '2018-03-06 11:03:44'),
(116, 245, 'Swetank Modi', '30', 'Ice Hockey', '1889', 'Male', '1', '2018-03-06 11:03:58'),
(117, 245, 'Mehul Chowksi', '40', 'Yoga', '1888', 'Female', '1', '2018-03-06 11:04:20'),
(118, 245, 'Deeksha', '50', 'Yoga', '1888', 'Female', '1', '2018-03-06 14:06:00'),
(119, 247, 'C V', '83', 'Cricket', '1391', 'Female', '1', '2018-03-09 12:15:16'),
(120, 251, 'Dilwar Khan', '2', 'Cricket', '1391', 'Male', '1', '2018-03-20 11:07:26'),
(121, 251, 'Bilal Khan', '3', 'Football', '1392', 'Male', '1', '2018-03-20 11:07:40'),
(122, 251, 'Billu Bawali', '36', 'Basket ball', '1393', 'Male', '1', '2018-03-20 12:18:27'),
(123, 251, 'Ratan Tata', '22', 'Basket ball', '1393', 'Male', '1', '2018-03-21 10:47:03'),
(124, 252, 'Ram', '5', 'Football', '1392', 'Male', '1', '2018-03-21 15:07:10'),
(125, 252, 'Sham', '6', 'Basket ball', '1393', 'Male', '1', '2018-03-21 15:07:25');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `notes` text,
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `posted_by` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `otp`
--

CREATE TABLE `otp` (
  `id` int(11) NOT NULL,
  `otp` double NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otp`
--

INSERT INTO `otp` (`id`, `otp`, `mobile`, `created`) VALUES
(1, 5943, '7011712623', '2018-02-10 13:30:43'),
(2, 1596, '8552921885', '2017-11-16 11:03:59'),
(3, 9994, '9560613271', '2018-04-02 17:29:54'),
(4, 2802, '8700648727', '2018-03-16 23:18:04'),
(5, 3348, '7566981318', '2017-11-16 11:25:23'),
(6, 7877, '8010376485', '2017-11-16 11:27:30'),
(7, 4939, '7906398702', '2018-03-19 11:24:25'),
(8, 6418, '9458055154', '2017-12-04 12:54:20'),
(9, 9670, '9958054435', '2018-03-15 15:28:41'),
(10, 2130, '9560610340', '2018-03-16 11:48:37'),
(11, 7430, '7011770633', '2017-11-16 19:45:32'),
(12, 6914, '7011303230', '2017-12-12 19:43:21'),
(13, 6872, '8447075305', '2018-01-26 20:33:33'),
(14, 2359, '7065199180', '2017-12-10 14:36:31'),
(15, 1206, '7011602344', '2017-12-04 12:59:45'),
(16, 1169, '9015972515', '2017-11-18 17:19:08'),
(17, 6909, '8860171151', '2018-01-25 16:47:59'),
(18, 1642, '9990600078', '2017-11-18 21:55:27'),
(19, 6676, '9599900901', '2018-01-19 18:49:52'),
(20, 1892, '9555335552', '2017-11-18 22:03:19'),
(21, 2285, '9990731159', '2018-04-04 13:39:19'),
(22, 3167, '9911338949', '2017-11-19 09:43:01'),
(23, 2755, '7678588083', '2017-11-19 14:46:12'),
(24, 6002, '8076333042', '2017-11-19 18:13:50'),
(25, 5364, '9990660914', '2017-11-19 18:14:33'),
(26, 2963, '8178779088', '2017-11-19 18:15:33'),
(27, 4052, '7982820618', '2017-11-21 01:11:07'),
(28, 7080, '9818701739', '2018-02-12 20:28:37'),
(29, 4010, '8244', '2017-11-21 10:58:47'),
(30, 9179, '3157448423', '2017-11-21 11:12:20'),
(31, 5337, '501', '2017-11-21 15:36:00'),
(32, 6861, '2390311006', '2017-11-21 15:37:37'),
(33, 3467, '14', '2017-11-21 15:44:20'),
(34, 8485, '8826465880', '2018-03-09 17:27:15'),
(35, 8871, '9797979979', '2017-11-21 18:10:49'),
(36, 9754, '0000000000', '2017-11-21 18:11:02'),
(37, 5646, '9797964759', '2017-11-22 10:08:46'),
(38, 7208, '8802915021', '2018-03-10 13:23:02'),
(39, 6900, '9999709484', '2017-11-22 16:10:28'),
(40, 2157, '9560758447', '2017-11-22 18:02:27'),
(41, 5782, '5738833776', '2017-11-22 18:03:31'),
(42, 3655, '8800956246', '2017-11-22 21:19:01'),
(43, 4516, '7417441782', '2017-11-26 09:15:25'),
(44, 9713, '9711530493', '2017-12-15 00:21:58'),
(45, 5065, '9891222013', '2017-11-27 11:11:24'),
(46, 5385, '9000000000', '2017-11-27 12:30:24'),
(47, 3818, '9111111111', '2017-11-27 12:35:41'),
(48, 8580, '8588832526', '2017-11-28 12:58:53'),
(49, 1359, '9368074476', '2017-11-28 15:24:45'),
(50, 8386, '7838998998', '2017-11-28 15:32:54'),
(51, 7207, '8864874378', '2017-11-28 16:13:01'),
(52, 1250, '9756943502', '2018-01-11 18:53:54'),
(53, 5073, '8755066233', '2017-11-28 16:54:29'),
(54, 2794, '9013435958', '2017-11-28 21:52:00'),
(55, 5246, '9971194897', '2018-03-15 12:02:32'),
(56, 7961, '9999706763', '2017-11-29 18:41:12'),
(57, 2106, '9739831787', '2017-11-30 10:31:39'),
(58, 6557, '7011712653', '2017-12-01 11:04:54'),
(59, 7792, '9990733159', '2017-12-01 12:39:16'),
(60, 4366, '8920822668', '2017-12-14 13:49:09'),
(61, 9317, '8700521303', '2017-12-03 10:21:04'),
(62, 5571, '8439766646', '2017-12-03 21:17:59'),
(63, 1717, '9868398578', '2017-12-03 21:18:56'),
(64, 1064, '8447091440', '2017-12-03 21:25:26'),
(65, 2062, '7011', '2017-12-04 12:46:31'),
(66, 5943, '6464646464646464646464644646', '2017-12-04 12:46:47'),
(67, 3854, '9990731189', '2017-12-04 18:38:37'),
(68, 1239, '9990080817', '2017-12-05 17:25:47'),
(69, 7066, '8860646503', '2017-12-14 17:47:13'),
(70, 2602, '9211367510', '2017-12-06 19:29:29'),
(71, 4108, '9411141902', '2017-12-07 12:27:01'),
(72, 5906, '7972283161', '2017-12-07 14:52:06'),
(73, 3723, '8588050974', '2017-12-08 14:51:06'),
(74, 1727, '8586966308', '2017-12-09 18:20:03'),
(75, 1528, '8802422562', '2017-12-09 21:49:07'),
(76, 9866, '8368164392', '2018-02-01 10:40:29'),
(77, 1638, '9654968661', '2017-12-09 21:52:16'),
(78, 8705, '8700303484', '2017-12-11 22:11:42'),
(79, 5823, '8287501009', '2017-12-10 10:49:00'),
(80, 6969, '8826516394', '2017-12-10 13:06:52'),
(81, 9001, '0068694845', '2017-12-11 12:04:40'),
(82, 2688, '9718240430', '2017-12-11 12:28:17'),
(83, 3644, '8218071654', '2017-12-11 13:30:21'),
(84, 3257, '9999575155', '2018-02-05 16:37:24'),
(85, 9425, '9899575155', '2018-01-09 23:47:52'),
(86, 8069, '8368983427', '2017-12-12 06:07:58'),
(87, 9629, '9582306364', '2017-12-13 11:24:00'),
(88, 2703, '9410433431', '2017-12-12 20:02:13'),
(89, 5193, '9891579750', '2017-12-16 18:34:03'),
(90, 9900, '9958865581', '2017-12-12 21:15:17'),
(91, 9447, '9997068934', '2017-12-13 10:42:39'),
(92, 9233, '9460610340', '2017-12-13 11:03:19'),
(93, 9688, '9461644142', '2017-12-13 16:35:46'),
(94, 1395, '7982880455', '2017-12-14 13:18:08'),
(95, 7390, '7838925145', '2017-12-20 10:31:49'),
(96, 1018, '7290039091', '2017-12-14 13:59:12'),
(97, 4519, '9643996292', '2017-12-14 14:22:55'),
(98, 2991, '8375083827', '2017-12-14 14:26:48'),
(99, 6464, '9911103077', '2018-01-08 18:54:45'),
(100, 9005, '7053428729', '2017-12-14 17:02:38'),
(101, 9473, '7678689405', '2017-12-14 17:14:33'),
(102, 5030, '8368482283', '2017-12-14 17:21:11'),
(103, 3683, '9643110368', '2017-12-14 17:31:11'),
(104, 6519, '9971788484', '2017-12-14 17:31:58'),
(105, 2241, '9555516655', '2017-12-14 18:08:59'),
(106, 2613, '9717769396', '2017-12-14 17:52:07'),
(107, 5374, '9540828949', '2017-12-14 17:55:20'),
(108, 2393, '9506612854', '2017-12-14 18:07:29'),
(109, 3529, '7292009440', '2017-12-14 18:35:22'),
(110, 2531, '8860608532', '2017-12-14 19:01:16'),
(111, 8466, '7065996232', '2017-12-14 19:03:07'),
(112, 6235, '9716361135', '2017-12-14 19:07:08'),
(113, 2224, '8467012984', '2017-12-14 19:18:23'),
(114, 6418, '9899816037', '2018-01-09 16:02:26'),
(115, 8169, '9911954510', '2017-12-14 19:58:47'),
(116, 9108, '9711242846', '2017-12-14 19:45:48'),
(117, 4411, '9953536448', '2017-12-14 19:54:25'),
(118, 2376, '9711807543', '2017-12-14 20:03:58'),
(119, 9050, '9899661240', '2017-12-14 20:30:16'),
(120, 5589, '9811508657', '2017-12-14 20:57:04'),
(121, 7094, '9818156931', '2017-12-14 21:08:13'),
(122, 6142, '9821403028', '2017-12-14 21:37:39'),
(123, 4184, '8826823377', '2017-12-14 21:44:13'),
(124, 1447, '9953039431', '2017-12-14 23:40:49'),
(125, 5510, '9015470320', '2017-12-15 09:44:31'),
(126, 7701, '9910925784', '2017-12-15 12:41:57'),
(127, 4099, '9971446842', '2017-12-15 19:01:18'),
(128, 3546, '9818270141', '2017-12-16 09:43:52'),
(129, 6025, '8347554684', '2017-12-16 12:12:15'),
(130, 7206, '9724393603', '2017-12-17 00:16:02'),
(131, 7762, '7618350615', '2017-12-17 10:27:05'),
(132, 6595, '9958361423', '2017-12-18 13:34:56'),
(133, 6464, '7011715623', '2017-12-18 18:07:10'),
(134, 2444, '9312744333', '2017-12-18 22:02:30'),
(135, 6408, '9643691758', '2017-12-19 10:39:00'),
(136, 6954, '7096398702', '2017-12-19 11:20:17'),
(137, 6331, '9412511980', '2017-12-19 16:25:24'),
(138, 6439, '9958744876', '2017-12-20 00:41:10'),
(139, 8913, '9997311778', '2017-12-21 11:56:20'),
(140, 4417, '9911508864', '2017-12-21 12:35:33'),
(141, 5765, '8447208109', '2018-02-06 15:02:19'),
(142, 6634, '0000000001', '2017-12-21 17:46:17'),
(143, 6259, '0000000002', '2017-12-21 17:56:58'),
(144, 8957, '0000000003', '2017-12-22 11:23:53'),
(145, 4633, '8449951234', '2017-12-22 15:13:31'),
(146, 7300, '0000000004', '2017-12-22 18:59:31'),
(147, 4163, '9869575155', '2017-12-22 22:49:19'),
(148, 5643, '8587973411', '2017-12-23 13:48:12'),
(149, 8932, '8076423788', '2018-02-23 14:55:06'),
(150, 4096, '8650462625', '2017-12-23 18:45:18'),
(151, 8507, '5787515045', '2017-12-23 14:46:39'),
(152, 2311, '8757515045', '2017-12-23 14:47:05'),
(153, 2004, '9927369333', '2017-12-23 17:54:07'),
(154, 1169, '9917078422', '2017-12-23 18:31:00'),
(155, 8410, '8279935760', '2017-12-23 19:57:08'),
(156, 4759, '7017288957', '2017-12-24 09:07:53'),
(157, 1202, '9198730069', '2017-12-24 14:17:55'),
(158, 1650, '9410155522', '2017-12-24 14:21:00'),
(159, 6933, '9756049208', '2017-12-24 18:12:08'),
(160, 7545, '1234567890', '2017-12-26 13:00:22'),
(161, 6730, '9015187618', '2018-02-07 12:27:03'),
(162, 2377, '9568403225', '2017-12-26 18:44:57'),
(163, 7584, '0110000009', '2017-12-27 12:37:44'),
(164, 8218, '9590613271', '2017-12-27 12:37:50'),
(165, 8150, '8160190721', '2017-12-27 12:43:37'),
(166, 8637, '9411500555', '2017-12-27 22:21:15'),
(167, 3353, '8789998960', '2018-03-15 12:00:44'),
(168, 8407, '7982931713', '2017-12-29 12:49:44'),
(169, 1170, '1234567345', '2017-12-29 23:45:58'),
(170, 3642, '8800232593', '2017-12-30 14:11:53'),
(171, 2746, '9211467916', '2017-12-30 15:02:30'),
(172, 6638, '9540897827', '2017-12-30 23:39:46'),
(173, 4653, '8982096306', '2017-12-31 11:03:54'),
(174, 3106, '8982096303', '2017-12-31 11:04:39'),
(175, 9810, '6543541351', '2018-01-01 11:06:35'),
(176, 6398, '9873793968', '2018-01-01 13:15:33'),
(177, 2922, '9917865368', '2018-01-01 21:29:32'),
(178, 1661, '8368792868', '2018-01-02 14:08:41'),
(179, 9674, '8726099310', '2018-01-03 17:34:45'),
(180, 3513, '9511675808', '2018-01-04 17:00:27'),
(181, 9345, '8445680462', '2018-01-05 17:05:23'),
(182, 5874, '8340504238', '2018-01-06 13:54:21'),
(183, 4865, '9760105999', '2018-01-08 13:17:51'),
(184, 8791, '7880481764', '2018-01-13 21:39:14'),
(185, 6005, '9958098979', '2018-01-13 21:41:56'),
(186, 9374, '9990171126', '2018-01-13 21:44:14'),
(187, 5922, '9810611021', '2018-01-18 15:35:37'),
(188, 9427, '9177038330', '2018-01-18 21:51:42'),
(189, 1420, '7703833026', '2018-01-18 22:10:39'),
(190, 5216, '9555518585', '2018-01-19 14:02:30'),
(191, 7110, '8384855998', '2018-01-19 22:11:49'),
(192, 9197, '8375847783', '2018-01-20 15:56:32'),
(193, 4920, '7011087771', '2018-02-10 21:57:06'),
(194, 3209, '9953130689', '2018-01-20 19:42:55'),
(195, 2798, '9212521116', '2018-01-21 14:28:17'),
(196, 5515, '7291889848', '2018-01-21 19:45:45'),
(197, 3198, '9211223301', '2018-01-21 20:21:19'),
(198, 3244, '9891542258', '2018-01-22 00:04:50'),
(199, 2693, '8285201125', '2018-01-22 10:13:00'),
(200, 4827, '9953723637', '2018-01-22 21:33:33'),
(201, 7184, '8826504964', '2018-01-24 14:27:02'),
(202, 4148, '9999275155', '2018-01-26 18:15:54'),
(203, 8605, '8076423778', '2018-01-25 12:18:24'),
(204, 6689, '9560102353', '2018-01-25 20:20:57'),
(205, 4690, '9540316161', '2018-01-26 08:34:33'),
(206, 5301, '8285727715', '2018-01-26 22:16:04'),
(207, 8417, '9654379393', '2018-01-27 13:16:07'),
(208, 5782, '9810453928', '2018-02-09 16:46:58'),
(209, 1242, '9560609501', '2018-01-31 11:56:56'),
(210, 9531, '7058689632', '2018-01-31 14:01:44'),
(211, 6724, '7206493529', '2018-01-31 17:49:18'),
(212, 4257, '9868012537', '2018-01-31 17:56:03'),
(213, 9283, '9871780318', '2018-01-31 18:05:52'),
(214, 1670, '9958754103', '2018-01-31 18:24:32'),
(215, 5849, '8506002791', '2018-02-01 18:46:32'),
(216, 5354, '9267905903', '2018-02-02 13:16:06'),
(217, 4854, '8950215049', '2018-02-02 13:22:08'),
(218, 3769, '8285980400', '2018-02-02 14:21:22'),
(219, 2360, '7011294243', '2018-02-02 15:42:59'),
(220, 6114, '9582804402', '2018-02-02 15:44:01'),
(221, 7523, '7503604714', '2018-02-02 17:34:58'),
(222, 2148, '9999861817', '2018-02-02 22:22:42'),
(223, 5282, '8744958496', '2018-02-03 10:11:39'),
(224, 3074, '8433230486', '2018-02-03 12:28:39'),
(225, 8890, '9973628865', '2018-02-03 12:36:36'),
(226, 1396, '9873452420', '2018-02-03 23:48:20'),
(227, 5662, '9654747877', '2018-02-05 15:35:31'),
(228, 5147, '8076741588', '2018-02-05 16:29:02'),
(229, 1922, '9251888333', '2018-02-05 19:41:49'),
(230, 2757, '9873960351', '2018-02-06 14:07:17'),
(231, 8948, '7530837293', '2018-02-06 15:17:24'),
(232, 5335, '8750968190', '2018-02-06 15:41:45'),
(233, 7074, '8750821922', '2018-02-06 16:05:25'),
(234, 2388, '9810931092', '2018-02-06 16:46:14'),
(235, 9617, '8802010347', '2018-02-06 17:54:21'),
(236, 5751, '8800809791', '2018-02-06 18:46:09'),
(237, 4931, '9821679299', '2018-02-07 10:49:50'),
(238, 9308, '9718885432', '2018-02-07 10:57:54'),
(239, 7002, '9654288604', '2018-02-08 09:49:09'),
(240, 6523, '9004988285', '2018-04-04 17:57:04'),
(241, 1419, '9999394997', '2018-02-08 16:03:31'),
(242, 4461, '9211843027', '2018-02-08 16:23:40'),
(243, 8575, '9899009023', '2018-02-08 16:28:22'),
(244, 7044, '7290926857', '2018-02-08 16:33:58'),
(245, 7128, '9871080006', '2018-02-08 17:51:52'),
(246, 5121, '9213110551', '2018-02-08 19:00:08'),
(247, 5912, '9868373466', '2018-02-08 20:41:32'),
(248, 6249, '9899242238', '2018-02-08 21:30:38'),
(249, 7674, '9540899529', '2018-02-09 12:43:16'),
(250, 1681, '9871210063', '2018-02-09 12:46:25'),
(251, 8493, '9910803208', '2018-02-09 12:59:36'),
(252, 1806, '8510888071', '2018-02-09 14:57:15'),
(253, 9580, '9990914612', '2018-02-09 15:33:11'),
(254, 3693, '7982445266', '2018-02-09 16:34:48'),
(255, 1016, '9910816012', '2018-02-09 16:58:02'),
(256, 3720, '9650382164', '2018-02-09 17:20:22'),
(257, 7954, '8512020518', '2018-02-09 17:20:35'),
(258, 3752, '9729411559', '2018-02-09 17:25:45'),
(259, 5638, '9210099067', '2018-02-09 18:07:53'),
(260, 5695, '8826071169', '2018-02-09 18:19:53'),
(261, 8815, '9910682371', '2018-02-09 19:26:30'),
(262, 4449, '7011382865', '2018-02-09 22:34:05'),
(263, 1613, '8826832108', '2018-02-09 23:48:36'),
(264, 1327, '8527656418', '2018-02-10 10:42:16'),
(265, 7563, '9650035973', '2018-02-10 19:15:13'),
(266, 4852, '9910682374', '2018-02-10 20:00:43'),
(267, 7216, '9810534234', '2018-02-13 14:53:22'),
(268, 6520, '9560899141', '2018-02-10 20:53:35'),
(269, 6995, '8860186898', '2018-02-10 23:33:09'),
(270, 4841, '9891262647', '2018-02-13 15:25:34'),
(271, 4996, '8076512061', '2018-02-11 16:55:09'),
(272, 3185, '9871551197', '2018-02-12 15:41:33'),
(273, 7200, '9871010809', '2018-02-12 15:46:31'),
(274, 8971, '9818800892', '2018-02-12 16:54:48'),
(275, 3516, '8802417295', '2018-02-12 17:27:57'),
(276, 1058, '8447385508', '2018-02-12 18:33:34'),
(277, 6863, '7011171839', '2018-02-12 18:51:50'),
(278, 1981, '9873110537', '2018-02-12 19:08:38'),
(279, 1308, '7289949833', '2018-02-12 20:08:39'),
(280, 3161, '8802215766', '2018-02-12 20:23:53'),
(281, 4291, '9811648007', '2018-02-12 23:29:07'),
(282, 5418, '9017032191', '2018-02-13 12:21:24'),
(283, 2384, '8588011206', '2018-02-13 13:10:43'),
(284, 3568, '9953803132', '2018-02-13 13:40:55'),
(285, 6842, '9990897898', '2018-02-13 14:07:18'),
(286, 9901, '9015890075', '2018-02-13 15:04:00'),
(287, 4472, '9871630540', '2018-02-13 15:11:45'),
(288, 2982, '9582824711', '2018-02-13 15:45:47'),
(289, 1211, '9899382196', '2018-02-13 15:56:34'),
(290, 7197, '9253442200', '2018-02-13 16:24:31'),
(291, 6636, '9650855052', '2018-02-13 16:26:44'),
(292, 5910, '8700145260', '2018-02-13 16:27:51'),
(293, 9120, '9810563093', '2018-02-13 16:39:50'),
(294, 3287, '9555562364', '2018-02-13 17:52:55'),
(295, 6932, '9654142590', '2018-02-13 18:14:33'),
(296, 3699, '9911558346', '2018-02-13 18:22:18'),
(297, 4470, '9953576856', '2018-02-13 19:44:40'),
(298, 6226, '9015045742', '2018-02-13 18:54:51'),
(299, 7933, '9999574088', '2018-02-13 19:13:02'),
(300, 7392, '7011462277', '2018-02-13 19:34:43'),
(301, 2003, '9811617558', '2018-02-13 22:06:50'),
(302, 9285, '7838230479', '2018-02-13 22:57:30'),
(303, 4238, '9910787177', '2018-02-13 23:26:22'),
(304, 7451, '9889101340', '2018-02-14 14:00:47'),
(305, 6445, '8802572566', '2018-02-14 14:15:06'),
(306, 2046, '8860312351', '2018-02-14 14:54:54'),
(307, 6401, '8700601699', '2018-02-14 15:57:13'),
(308, 3815, '9897577007', '2018-02-14 18:50:52'),
(309, 2276, '7906655031', '2018-02-14 18:52:21'),
(310, 6305, '9911567735', '2018-02-14 23:16:19'),
(311, 7814, '9039246053', '2018-03-19 11:33:02'),
(312, 2368, '9407006686', '2018-03-06 14:12:31'),
(313, 6742, '8447195992', '2018-03-12 12:32:57'),
(314, 5526, '9004988258', '2018-03-16 16:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `passion_for_life`
--

CREATE TABLE `passion_for_life` (
  `id` int(11) NOT NULL,
  `passion_for_life` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_type` varchar(255) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `amount` decimal(7,2) NOT NULL,
  `txn_id` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `plan_name` varchar(255) NOT NULL,
  `activity` varchar(255) NOT NULL,
  `payment_mode` tinyint(4) NOT NULL COMMENT '1 for online, 2 for offline',
  `paid_on` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `teacher_id`, `student_id`, `batch_type`, `batch_id`, `amount`, `txn_id`, `order_id`, `plan_name`, `activity`, `payment_mode`, `paid_on`, `created_at`) VALUES
(1, 49, 25, 'session', 103, '200.00', '903', '', '', '', 0, '2018-01-23 15:35:20', '0000-00-00 00:00:00'),
(2, 74, 25, 'Batch', 59, '500.00', '333', '', '', '', 1, '2018-01-24 11:15:15', '24-01-2018 11:15:16'),
(3, 74, 25, 'Batch', 59, '500.00', '333', '', '', '', 1, '2018-01-24 11:15:15', '24-01-2018 11:16:41'),
(4, 74, 4, 'Batch', 59, '500.00', '24', '', '', '', 1, '2018-01-24 11:23:21', '24-01-2018 11:23:22'),
(5, 75, 4, 'Batch', 52, '6868.00', '802', '', '', '', 1, '2018-01-24 11:25:19', '24-01-2018 11:25:20'),
(6, 75, 4, 'Batch', 52, '6868.00', '556', '', '', '', 1, '2018-01-24 11:25:59', '24-01-2018 11:26:00'),
(7, 75, 1, 'Batch', 31, '65.00', '486', '', '', '', 1, '2018-01-24 11:28:36', '24-01-2018 11:28:37'),
(8, 75, 1, 'Batch', 51, '698.00', '98', '', '', '', 1, '2018-01-24 11:29:32', '24-01-2018 11:29:33'),
(9, 75, 30, 'Batch', 52, '6868.00', '329', '', '', '', 1, '2018-01-24 11:34:14', '24-01-2018 11:34:14'),
(10, 74, 30, 'Batch', 59, '500.00', '392', '', '', '', 1, '2018-01-24 11:38:37', '24-01-2018 11:38:38'),
(11, 75, 30, 'Batch', 63, '200.00', '202', '', '', '', 1, '2018-01-24 13:07:22', '24-01-2018 13:07:23'),
(12, 74, 30, 'Batch', 64, '100.00', '218', '', '', '', 1, '2018-01-24 14:32:18', '24-01-2018 14:32:18'),
(13, 75, 20, 'Batch', 52, '6868.00', '656', '', '', '', 1, '2018-01-24 15:03:42', '24-01-2018 15:03:42'),
(14, 49, 30, 'session', 104, '666.00', '486', '', '', '', 1, '2018-01-24 15:23:59', '24-01-2018 15:24:00'),
(15, 87, 30, 'session', 109, '800.00', '141', '', '', '', 1, '2018-01-24 15:26:04', '24-01-2018 15:26:05'),
(16, 49, 30, 'session', 110, '5665.00', '328', '', '', '', 1, '2018-01-24 15:32:38', '24-01-2018 15:32:39'),
(17, 49, 30, 'session', 111, '639.00', '245', '', '', '', 1, '2018-01-24 15:34:11', '24-01-2018 15:34:12'),
(18, 49, 30, 'session', 104, '666.00', '386', '', '', '', 1, '2018-01-24 16:16:47', '24-01-2018 16:16:47'),
(19, 49, 4, 'session', 104, '666.00', '750', '', '', '', 1, '2018-01-24 16:20:48', '24-01-2018 16:20:49'),
(20, 49, 30, 'session', 110, '5665.00', '646', '', '', '', 1, '2018-01-24 16:26:50', '24-01-2018 16:26:50'),
(21, 49, 30, 'session', 111, '639.00', '775', '', '', '', 1, '2018-01-24 16:36:03', '24-01-2018 16:36:04'),
(22, 87, 30, 'session', 114, '630.00', '807', '', '', '', 1, '2018-01-24 17:19:21', '24-01-2018 17:19:20'),
(23, 87, 30, 'session', 115, '630.00', '873', '', '', '', 1, '2018-01-24 17:36:32', '24-01-2018 17:36:32'),
(24, 87, 30, 'session', 116, '650.00', '62', '', '', '', 1, '2018-01-24 17:49:09', '24-01-2018 17:49:09'),
(25, 87, 30, 'session', 116, '0.00', '657', '', '', '', 1, '2018-01-24 17:59:20', '24-01-2018 17:59:22'),
(26, 87, 20, 'session', 116, '650.00', '577', '', '', '', 1, '2018-01-24 18:23:14', '24-01-2018 18:23:15'),
(27, 39, 20, 'Batch', 42, '500.00', '762', '', '', '', 1, '2018-01-24 18:32:33', '24-01-2018 18:32:34'),
(28, 87, 30, 'session', 117, '665.00', '911', '', '', '', 1, '2018-01-24 18:38:08', '24-01-2018 18:38:09'),
(29, 87, 25, 'session', 116, '650.00', '906', '', '', '', 1, '2018-01-24 19:13:04', '24-01-2018 19:13:12'),
(30, 9, 30, 'session', 118, '150.00', '529', '', '', '', 1, '2018-01-25 11:03:00', '25-01-2018 11:02:59'),
(31, 9, 20, 'session', 119, '0.00', '224', '', '', '', 1, '2018-01-25 11:28:49', '25-01-2018 11:28:49'),
(32, 107, 30, 'Batch', 66, '10000.00', '887', '', '', '', 1, '2018-01-25 11:52:37', '25-01-2018 11:52:36'),
(33, 87, 4, 'session', 114, '630.00', '886', '', '', '', 1, '2018-01-25 15:56:39', '25-01-2018 15:56:39'),
(34, 107, 30, 'Batch', 68, '963.00', '508', '', '', '', 1, '2018-01-25 16:55:01', '25-01-2018 16:55:00'),
(35, 61, 34, 'Batch', 18, '5000.00', '263', '', '', '', 1, '2018-01-26 22:18:37', '26-01-2018 22:18:38'),
(36, 99, 30, 'Batch', 57, '1000.00', '723', '', '', '', 1, '2018-01-29 11:31:10', '29-01-2018 11:31:08'),
(37, 107, 5, 'Batch', 85, '100.00', '153', '', '', '', 1, '2018-01-29 17:12:02', '29-01-2018 17:12:01'),
(38, 107, 5, 'Batch', 81, '100.00', '294', '', '', '', 1, '2018-01-29 17:22:14', '29-01-2018 17:22:13'),
(39, 75, 5, 'Batch', 52, '6868.00', '848', '', '', '', 1, '2018-01-29 17:22:26', '29-01-2018 17:22:25'),
(40, 107, 5, 'Batch', 78, '100.00', '952', '', '', '', 1, '2018-01-29 17:29:15', '29-01-2018 17:29:14'),
(41, 107, 5, 'Batch', 83, '99999.99', '431', '', '', '', 1, '2018-01-29 17:53:20', '29-01-2018 17:53:19'),
(42, 107, 5, 'Batch', 66, '10000.00', '128', '', '', '', 1, '2018-01-29 18:28:13', '29-01-2018 18:28:12'),
(43, 87, 25, 'Session', 128, '1.00', '7504201778', '1517565968316', '', '', 1, '2018-02-02 15:36:25', '02-02-2018 15:43:25'),
(44, 87, 25, 'Session', 129, '1.00', '7504231287', '1517566637483', '', '', 1, '2018-02-02 15:47:29', '02-02-2018 15:47:30'),
(45, 136, 30, 'Batch', 87, '500.00', 'Later', '1517572365862', '', '', 1, '2018-02-02 17:22:45', '02-02-2018 17:22:45'),
(46, 136, 30, 'Batch', 86, '200.00', 'Later', '1517572467749', '', '', 1, '2018-02-02 17:24:27', '02-02-2018 17:24:27'),
(47, 9, 30, 'Session', 119, '690.00', 'Later', '1517572739001', '', '', 1, '2018-02-02 17:28:59', '02-02-2018 17:28:58'),
(48, 75, 30, 'Batch', 88, '780.00', 'Later', '1517576383800', '', '', 1, '2018-02-02 18:29:43', '02-02-2018 18:29:43'),
(49, 30, 2, 'Batch', 10, '800.00', 'Later', '1517829426493', '', '', 1, '2018-02-05 16:47:06', '05-02-2018 16:47:07'),
(50, 9, 30, 'Session', 132, '680.00', 'Later', '1517898922582', '', '', 1, '2018-02-06 12:05:22', '06-02-2018 12:05:22'),
(51, 164, 30, 'Batch', 95, '500.00', 'Later', '1517909155752', '', '', 1, '2018-02-06 14:55:55', '06-02-2018 14:55:55'),
(52, 164, 5, 'Batch', 95, '500.00', 'Later', '1517909582672', '', '', 1, '2018-02-06 15:03:02', '06-02-2018 15:03:03'),
(53, 75, 5, 'Batch', 88, '780.00', 'Later', '1517911045747', '', '', 1, '2018-02-06 15:27:25', '06-02-2018 15:27:26'),
(54, 80, 17, 'Batch', 101, '500.00', 'Later', '1517997559790', '', '', 1, '2018-02-07 15:29:19', '07-02-2018 15:29:19'),
(55, 80, 17, 'Batch', 36, '1000.00', 'Later', '1517997589451', '', '', 1, '2018-02-07 15:29:49', '07-02-2018 15:29:49'),
(56, 161, 30, 'Batch', 91, '600.00', 'Later', '1518070936754', '', '', 1, '2018-02-08 11:52:16', '08-02-2018 11:52:16'),
(57, 24, 30, 'Session', 8, '1200.00', 'Later', '1518072318522', '', '', 1, '2018-02-08 12:15:18', '08-02-2018 12:15:18'),
(58, 24, 30, 'Session', 8, '1200.00', 'Later', '1518072424353', '', '', 1, '2018-02-08 12:17:04', '08-02-2018 12:17:04'),
(59, 9, 30, 'Session', 132, '680.00', 'Later', '1518073354468', '', '', 1, '2018-02-08 12:32:34', '08-02-2018 12:32:36'),
(60, 15, 6, 'Session', 100, '99999.99', 'Later', '1518267645106', '', '', 1, '2018-02-10 18:30:45', '10-02-2018 18:30:45'),
(61, 39, 42, 'Batch', 124, '1.00', '7530302930', '1518425238243', '', '', 1, '2018-02-12 14:19:24', '12-02-2018 14:19:26'),
(62, 80, 42, 'Batch', 101, '500.00', 'Later', '1518434392828', '', '', 1, '2018-02-12 16:49:52', '12-02-2018 16:49:54'),
(63, 80, 42, 'Batch', 36, '1000.00', 'Later', '1518434586114', '', '', 1, '2018-02-12 16:53:06', '12-02-2018 16:53:07'),
(64, 80, 42, 'Batch', 33, '800.00', 'Later', '1518434786320', '', '', 1, '2018-02-12 16:56:26', '12-02-2018 16:56:30'),
(65, 176, 1, 'Session', 198, '1.00', 'Later', '1518606113858', '', '', 1, '2018-02-14 16:31:53', '14-02-2018 16:31:54'),
(66, 176, 2, 'Session', 198, '1.00', '7535713119', '1518606262471', '', '', 1, '2018-02-14 16:38:42', '14-02-2018 16:38:43'),
(67, 176, 3, 'Session', 202, '0.00', 'Later', '1519631794349', '', '', 1, '2018-02-26 13:26:34', '26-02-2018 13:26:34'),
(68, 176, 1, 'Session', 204, '5.00', 'Later', '1519797875328', '', '', 1, '2018-02-28 11:34:35', '28-02-2018 11:34:35'),
(69, 176, 1, 'Session', 183, '0.00', 'Later', '1520246129930', '', '', 1, '2018-03-05 16:05:29', '05-03-2018 16:05:29'),
(70, 74, 20, 'Batch', 163, '2500.00', 'Later', '1520668413754', '', '', 1, '2018-03-10 13:23:33', '10-03-2018 13:23:34'),
(71, 248, 25, 'Session', 209, '2000.00', 'Later', '1520674250781', '', '', 1, '2018-03-10 15:00:50', '10-03-2018 15:00:52'),
(72, 74, 4, 'Batch', 163, '2500.00', 'Later', '1520683324623', '', '', 1, '2018-03-10 17:32:04', '10-03-2018 17:32:05'),
(73, 24, 6, 'Session', 8, '0.00', 'Later', '1520793092723', '', '', 1, '2018-03-12 00:01:32', '12-03-2018 00:01:32'),
(74, 74, 25, 'Batch', 163, '2500.00', 'Later', '1520832164548', '', '', 1, '2018-03-12 10:52:44', '12-03-2018 10:52:47'),
(75, 241, 42, 'Batch', 162, '64.00', 'Later', '1520837200424', '', '', 1, '2018-03-12 12:16:40', '12-03-2018 12:16:43'),
(76, 241, 121, 'Batch', 162, '64.00', 'Later', '1520838251075', '', '', 1, '2018-03-12 12:34:11', '12-03-2018 12:34:11'),
(77, 241, 6, 'Batch', 162, '64.00', 'Later', '1520838334780', '', '', 1, '2018-03-12 12:35:34', '12-03-2018 12:35:35'),
(78, 241, 85, 'Batch', 165, '1.00', '7604864735', '1521025688831', '', '', 1, '2018-03-14 16:40:34', '14-03-2018 16:40:34'),
(79, 241, 124, 'Batch', 165, '1.00', 'Later', '1521107437192', '', '', 1, '2018-03-15 15:20:37', '15-03-2018 15:20:38'),
(80, 241, 124, 'Batch', 162, '64.00', 'Later', '1521109338608', '', '', 1, '2018-03-15 15:52:18', '15-03-2018 15:52:18'),
(81, 241, 124, 'Batch', 164, '3.00', 'Later', '1521115741417', '', '', 1, '2018-03-15 17:39:01', '15-03-2018 17:39:01'),
(82, 238, 124, 'Batch', 142, '1000.00', 'Later', '1521116995372', '', '', 1, '2018-03-15 17:59:55', '15-03-2018 17:59:55'),
(83, 87, 124, 'Session', 116, '650.00', 'Later', '1521182638917', '', '', 1, '2018-03-16 12:13:58', '16-03-2018 12:13:59'),
(84, 241, 124, 'Batch', 166, '0.00', 'Later', '1521183349213', '', '', 1, '2018-03-16 12:25:49', '16-03-2018 12:25:49'),
(85, 241, 124, 'Batch', 166, '0.00', 'Later', '1521190622720', '', '', 1, '2018-03-16 14:27:02', '16-03-2018 14:27:03'),
(86, 249, 124, 'Batch', 168, '1.00', 'Later', '1521194381593', '', '', 1, '2018-03-16 15:29:41', '16-03-2018 15:29:42'),
(87, 249, 124, 'Batch', 169, '1.00', 'Later', '1521196913694', '', '', 1, '2018-03-16 16:11:53', '16-03-2018 16:11:56'),
(88, 250, 124, 'Session', 210, '1.00', 'Later', '1521200056788', '', '', 1, '2018-03-16 17:04:16', '16-03-2018 17:04:16'),
(89, 0, 125, 'Activity', 14, '1.00', '7656639177', '321321', '', '', 1, '123', '04-04-2018 14:14:30'),
(90, 251, 25, 'Activity', 14, '1.00', '7656698146', '7656698146', '', '', 1, '04/Apr/2018 02:27 PM', '04-04-2018 14:28:24'),
(91, 253, 125, 'Batch', 170, '2.00', 'Later', '1522848396677', '', '', 1, '2018-04-04 18:56:36', '04-04-2018 18:56:37');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL,
  `qualification_name` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_quiz`
--

CREATE TABLE `question_quiz` (
  `id` int(11) NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `chapter` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `types` varchar(255) NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_quiz`
--

INSERT INTO `question_quiz` (`id`, `course_id`, `class_id`, `subject_id`, `chapter`, `topic`, `types`, `created_date`) VALUES
(93, '2', '37#MEDICAL', '505#NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-PHYSICS', 'Medical', 'eyes', 'Quiz', '2018-03-21 14:49:36'),
(94, '3', '111#NEET', '1464#Physics', 'Medical', 'ears', 'Quiz', '2018-03-21 14:56:59'),
(95, '2', '38#MARINE,NAVY(DEFENCE)', '95#INDIAN MARITIME UNIVERSITY COMMON ENTRANCE TEST', 'computer sc', 'computer', 'Question', '2018-03-21 14:58:41'),
(97, '1', '6#CLASS VI', '322#MATHEMATICS', 'apptitude', 'Airthmetic', 'Quiz', '2018-03-21 15:07:51'),
(98, '2', '116#AIEEE', '1449#Mathematics', 'computer', 'DBMS', 'Quiz', '2018-03-21 15:12:18'),
(99, '2', '37#MEDICAL', '505#NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-PHYSICS', 'English senctance', 'normal', 'Question', '2018-03-21 15:14:26'),
(103, '1', '6#CLASS VI', '322#MATHEMATICS', 'trigonomerty', 'math', 'Quiz', '2018-03-21 16:33:32'),
(104, '3', '109#AIPMT', '1453#Biology', 'trigonomerty', 'math', 'Quiz', '2018-03-21 16:35:05'),
(105, '1', '6#CLASS VI', '322#MATHEMATICS', 'trigonomerty', 'maths', 'Quiz', '2018-03-21 16:37:40'),
(107, '1', '6#CLASS VI', '322#MATHEMATICS', 'trigonomerty', 'hasdh', 'Quiz', '2018-03-23 15:49:01'),
(108, '2', '37#MEDICAL', '505#NEET(NATIONAL ELIGIBILITY CUM ENTRANCE TEST)-PHYSICS', 'xgfd', 'dfg', 'Quiz', '2018-03-23 15:50:40');

-- --------------------------------------------------------

--
-- Table structure for table `receipts`
--

CREATE TABLE `receipts` (
  `id` int(10) UNSIGNED NOT NULL,
  `receipt_no` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `course_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `batch_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  `payment_method` tinyint(1) NOT NULL DEFAULT '0',
  `remark` text,
  `date` date DEFAULT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `referal`
--

CREATE TABLE `referal` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `date` date DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `referals`
--

CREATE TABLE `referals` (
  `id` bigint(22) NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `wallet` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reffer_code`
--

CREATE TABLE `reffer_code` (
  `id` int(11) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `reffercode` varchar(50) NOT NULL,
  `added_date` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reffer_code`
--

INSERT INTO `reffer_code` (`id`, `user_type`, `user_id`, `reffercode`, `added_date`) VALUES
(3, 'S', '25', 'kbbibibb', '04-04-2018 13:28:38'),
(2, 'S', '25', 'djdkdjf', '04-04-2018 13:23:41'),
(4, 'S', '25', 'fhioolc', '04-04-2018 15:25:20');

-- --------------------------------------------------------

--
-- Table structure for table `rel_student_batch`
--

CREATE TABLE `rel_student_batch` (
  `id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `batch_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `added` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session_slot`
--

CREATE TABLE `session_slot` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) NOT NULL COMMENT '0: Inactive, 1: Active'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `session_slot`
--

INSERT INTO `session_slot` (`id`, `title`, `status`) VALUES
(1, '05:00 am - 06:00 am', 1),
(2, '06:00 am - 07:00 am', 1),
(3, '07:00 am - 08:00 am', 1),
(4, '08:00 am - 09:00 am', 1),
(5, '09:00 am - 10:00 am', 1),
(6, '10:00 am - 11:00 am', 1),
(7, '11:00 am - 12:00 pm', 1),
(8, '12:00 pm - 01:00 pm', 1),
(9, '01:00 pm - 02:00 pm', 1),
(10, '02:00 pm - 03:00 pm', 1),
(11, '03:00 pm - 04:00 pm', 1),
(12, '04:00 pm - 05:00 pm', 1),
(13, '05:00 pm - 06:00 pm', 1),
(14, '06:00 pm - 07:00 pm', 1),
(15, '07:00 pm - 08:00 pm', 1),
(16, '08:00 pm - 09:00 pm', 1),
(17, '09:00 pm - 10:00 pm', 1),
(18, '10:00 pm - 11:00 pm', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` text,
  `registration_no` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `receipt_no` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `faculty_receipt_no` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `batch_alert` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `logo`, `phone`, `email`, `address`, `registration_no`, `receipt_no`, `faculty_receipt_no`, `batch_alert`) VALUES
(1, 'TAKTI', NULL, NULL, NULL, NULL, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `site_setting`
--

CREATE TABLE `site_setting` (
  `id` int(50) NOT NULL,
  `site_range` int(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_setting`
--

INSERT INTO `site_setting` (`id`, `site_range`) VALUES
(1, 20);

-- --------------------------------------------------------

--
-- Table structure for table `slot`
--

CREATE TABLE `slot` (
  `id` bigint(22) NOT NULL,
  `slot_name` varchar(50) NOT NULL,
  `slot_time` varchar(50) NOT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `slot_capacity` int(5) NOT NULL,
  `available_seat` int(11) NOT NULL,
  `center_id` bigint(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `star_achievers`
--

CREATE TABLE `star_achievers` (
  `id` bigint(22) NOT NULL,
  `student_id` varchar(255) DEFAULT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `subject_id` varchar(255) DEFAULT '0',
  `markes` int(10) NOT NULL,
  `gain_marks` int(10) NOT NULL,
  `report_docs` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `profile_img` varchar(255) NOT NULL,
  `ach_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `std_sample_papers`
--

CREATE TABLE `std_sample_papers` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `course_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `up_file` varchar(255) NOT NULL,
  `file_ext` varchar(10) NOT NULL,
  `status` varchar(1) NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `std_sample_papers`
--

INSERT INTO `std_sample_papers` (`id`, `title`, `description`, `course_id`, `class_id`, `subject_id`, `up_file`, `file_ext`, `status`, `added_date`) VALUES
(2, 'sd', 'asd', '1', '6#CLASS VI', '323#ENGLISH', '/assest/sample_papers/sample_papers_1519382817_67379111_1518066005759.xls', 'xls', '1', '2018-02-23 16:16:57'),
(3, 'asd', 'asd', '17', '139#Indian Navy Sailors', '1228#Chemistry', '/assest/sample_papers/sample_papers_1519382860_ericksha.pdf', 'pdf', '1', '2018-02-23 16:17:40');

-- --------------------------------------------------------

--
-- Table structure for table `story`
--

CREATE TABLE `story` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `add_date` varchar(255) NOT NULL,
  `topic` text NOT NULL,
  `heading` text NOT NULL,
  `story` text NOT NULL,
  `story_img` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '0',
  `trending` int(2) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `story`
--

INSERT INTO `story` (`id`, `user_id`, `user_type`, `add_date`, `topic`, `heading`, `story`, `story_img`, `status`, `trending`, `created`) VALUES
(79, 0, 'admin', '2018-Feb-08', '', 'Pranay Chulet Son of a government officer & the founder of Quikr.com', 'Pranay Chulet is the Founder & the mind behind India’s largest online classifieds portal – Quikr.com!\r\n\r\nLike most of us already know, Quikr acts as a medium to connect the buyers & sellers of any given product, and is pretty much similar to Craigslist in the US. A portal that was launched with a vision of providing a platform to the buyers and sellers to “meet online, transact offline”, today holds more than 4.2 million listings and also has generated over 150 million replies.\r\n\r\nHeadquartered in the posh areas of south Mumbai, Quikr offers over 13 main categories and in 170 sub-categories including mobile phones, household goods, cars, real estate, jobs, services, education, etc. Additionally, it holds an actual presence in over 1,000 cities in India, and performs a large scale cross-category classifieds business amongst its more than 30 million consumers.\r\n\r\nPersonally speaking; Pranay holds an Undergraduate degree in Chemical Engineering from the Indian Institute of Technology in Delhi (IIT-D) and an MBA from the Indian Institute of Management Calcutta (IIM-C).\r\n\r\nHe recently got married to his lady love Tina Chulet, who coincidently is also the Founder of Waltzz – a mobile dating app! Together they live in their apartment in Bandra, Mumbai.\r\n\r\nA Journey that led to Quikr.com!\r\nPranay was born in a small town of Rajasthan to a family where his father was a small time government officer working in mines as a general manager and his mother was a homemaker.\r\n\r\nMost of his early childhood days were spent in the mining towns of Dariba, Zawar and Maton in Rajasthan. But his dream was to become an entrepreneur since day one!\r\n\r\nHe began his career, soon after he completed his graduation from IIM-C! Very few are aware that Procter & Gamble was his first job. He had joined in as a brand management executive. But this stint didn’t go on for long and he quit in 1997 itself.\r\n\r\nIn the same year, Pranay started working for Mitchell Madison Group as a Management Consultant / Engagement Manager. His basic job profile was to advice his clients in financial services and media industries.\r\n\r\nHe worked with the group for almost 3 years, after which in 2000, looking at the Dot Com boom in the environment, he started his first venture called – Reference Check. Interestingly, it was pretty much similar to his current venture Quikr. Reference Check was an online medium between Service Providers (Eg: Plumbers, Electrician, etc) and Consumers. The company eventually got merged with another bigger incubator called Walker Digital!\r\n\r\nHe went on to work with Walker for a brief period to built and stabilize the company, before he joined PricewaterhouseCoopers (Associate Partner) and then Booz Allen Hamilton (Principal) in 2002 & 2005, respectively.\r\n\r\nHis primary roles at both the companies were more or less, advising or consulting their clients from industries like Investment Banking, Media, etc.\r\n\r\nAfter giving almost 5 years of his life, he even started another company of his own called – Excellere in 2007. Excellere was all about developing web-based educational products.\r\n\r\nAlthough, for unknown reasons this venture of his didn’t function for more than a year, but during that phase what he saw back in India, changed his life forever.\r\n\r\nHe founded – Quikr.com!THE RISE…\r\nWith such a unique model (in respect to India), along with such innovative strategies, in all aspects, the growth and rise of the company was eminent!\r\n\r\nIn the next few years, not only did the company grow at the pace of fire, but also was now getting transformed into a generic brand in terms of classifieds. Some of the most prominent developments are mentioned below: –\r\n\r\n2012 –\r\nBy now, Quikr had crossed a lot of milestones, like reaching from 1 million to 4 million, then to 10 million and has now finally reached to point where it had a monthly user base of more than 22 million spread across 65 Indian cities, and had also become India’s largest horizontal classifieds company.\r\nAdditionally, their first ever television campaign was launched and had also tied up with big brands like Nokia where Quikr apps would be available for users.\r\n2014 –\r\nIn the last 3-4 years, not only had their traffic doubled, but they had tripled their consumer engagement and had also grown five times in terms of revenues – which now was touching somewhere around Rs. 200 Cr.\r\nThey had now reached to another level and were catering to one-in-every six internet users in India i.e. 32 million users roughly, and were not only foreseeing a high growth in traffic and engagement, but also monetisation at the same time.\r\nWith an increase in position, Quikr was largely spending on marketing efforts and improving its data analytics and algorithms as well.\r\nTheir staffing had also increased to around 80-90 members, in their short yet lavish headquarters of South Mumbai.\r\nThis shortage of space had also led to the invention of an innovative idea. Quikr also started one-of-its-kind – ‘missed called centre’. To get to Quikr, all a customer had to do was, give a missed call to Quikr, and someone would call them back. That way, the company was also able to reach out to a larger section of mobile users who, so far were hesitant to spend money and make a call.\r\nAnd lastly, they also had another great instant messaging (IM) feature named Quikr Nxt – which enabled the users to chat with multiple users and share images.\r\nTalking about the developments of 2015; Quikr with a strength of 1,200-1,300 people, now reports 15 lakh (1.5 million roughly) transactions every month, which are valued somewhere around $5 billion.\r\n\r\nToday, the portal has a presence in 900 cities across India and addresses the needs of many across 12 categories & over 140 sub-categories which range from – Mobiles, services, cars, real estate, entertainment, jobs, furniture, electronics etc.\r\n\r\nQuikr is also in the process of launching a new classifieds website for real estate called quikrhomes.com. This would allow B2C (Business 2 Customer) as well as C2C (Customer 2 Customer) discovery of properties. At the moment, Quikr has over 2 million listings in real estate, one-third out of which are for apartment rentals.\r\n\r\nQuikr would also be launching new micro-sites under separate domain names like QuikrHomes, QuikrAuto, etc, to simply business for its users.\r\n\r\nAnd lastly, since inception, Quikr has received a total amount of $346 Million in funding from various investors including – Steadview Capital, Investment AB Kinnevik, Tiger Global Management, Warburg Pincus, Omidyar Network, Norwest Venture Partners – NVP, Nokia Growth Partners, Matrix Partners India and eBay.\r\n\r\nThese rounds include $150 million (2015), $60 million (2014), $90 million (2014), $32 million (2012), $8 million (2011), and $6 million (2010).Achievements\r\nThe only classifieds platform to be featured in top ten ‘Best E-commerce Brands in India Amongst Youth’, in BAV (a Brand Asset Valuator) by Rediffusion-Y&R (2013)\r\nReceived the ‘Best Classifieds website of the Year 2012’ award at the WAT Awards (2011 – 2012)\r\nRecognized as ‘one of India’s hottest internet companies for 2012’ by Young Turks Awards (2010 – 2011)\r\nAwarded as selected ‘AlwaysOn Global 250 Winner’ by AlwaysOn (2009 – 2010)\r\nListed as Red Herring Asia 2010 & Global 2011 Top 100 finalist (2010 – 2011)\r\nReceived the Certificate of Appreciation by Harvard Business School (2011 – 2012', 'story_pranay-1.png', '1', 1, '2018-02-08 05:24:43'),
(78, 0, 'admin', '2018-Feb-08', '', 'IN BRIEF: This post is dedicated to Cricketing sensation Yuvraj Singh’s battle with Cancer, and his determination to fight back.', 'Twenty-Twenty World Cup 2007, six sixes in an over against England, Vice-Captain of Indian Cricket team for (ODI) 2007-2008. A young, handsome and successful cricketer who is a true charmer. A number of matches were won by him single-handedly. Yuvraj and Dhoni formed a formidable pair.\r\n\r\nYuvraj started his career in International cricket in 2000 and in no time became the star of Indian Cricket team, not only Cricket lovers but whole India knew him, he was never less than any film star . Yuvraj, son of Yograj Singh, former Indian bowler, had cricket in his genes whether it was World Cup or ODI he has always played with passion.\r\n\r\nIt was during the time of World Cup when he would wake up in the middle of the night and would find it difficult to breathe and used to cough immensely. After the world cup, a scan for chest cavity revealed a tumour but like any of us he could not believe it and more than that he ignored it as for him cancer meant leaving cricket, therefore, he kept playing matches, it was only in January when the further tests revealed that tumour was malignant — a germ cell cancer called a mediastinal seminoma was diagnosed, it was located in between his heart and his (left) lung and it grew about 14 centimetres like a ball over his chest which was a concern. And then came the time to face the fears and he chose to face them in 2011.\r\n\r\nYuvraj’s doctor planned for chemotherapy and on 26 January he went to U.S for chemotherapy, his chemotherapy started in February and ended in March. Along with this treatment he also took Ayurveda treatment and went for rehabilitation in May, the same year.Cancer, one of the most common disease in the world right now which was once considered as a deadly disease whose name shakes the person from head to toe, but chose to battle it out.\r\n\r\nWas he not scared? How could he fight it? Yes, of course he was scared and therefore he delayed the treatment. But, can anyone run from the truth? He faced the fears to get batter during which he realised “Cancer does not mean death. In one of his Interviews he said “Don’t be scared… Be positive… Please don’t think that your life is about to end… Take the right advice and act as soon as you can… If there’s an issue in your system, don’t ignore it… Look at me, I never thought I’d get the chance to again play for India, but I’ve beaten cancer… God has given me this opportunity and my love for cricket has brought me back. I see this alone as a huge achievement… The experience has made me stronger (in the mind)… If you’re strong, then you automatically become a positive person. Cancer doesn’t mean that you’re going to die.”Inspirational Yuvraj\r\nYes, our hero was inspired by Lance Armstrong who has made many  records in his career as well and was also diagnosed with the same kind of tumour in 1996. He was treated with tumour and despite that, he began his career again. Yuvraj Singh has also been treated by the same doctor. During his time in the U.S, he used to read the autobiography of Lance Armstrong for motivation and to get his treatment done perfectly.\r\n\r\nBack to Pavilion\r\nIn April 2012, Yuvraj made his comeback with Twenty-Twenty against New Zealand in September 2012 and is still the star of India.\r\n\r\nLesson For Us\r\n“Strength does not come from winning. Your struggles develop your strengths. When you go through hardships and decide not to surrender, that is strength.”(Arnold Schwarzenegger).\r\n\r\nIt’s about how you decide to live, you have two options either accept the defeat and be upset about or stand up and keep fighting till the time you win and winning does not mean reaching the top, it means living a beautiful life in the allotted time. So it’s a choice you need to make. LIVE or LEAVE.', 'story_maxresdefault.jpg', '1', 1, '2018-02-08 05:24:52'),
(74, 0, 'admin', '2018-Feb-07', '', 'The tough get going: 5 Indians who studied their way to success,They rose above their grim financial circumstances, overcame all sorts of odds and defied stereotypes to top exams, get into IIMs and even pursue a PhD at 15! Here are their incredible stories: of unrelenting struggle and hard work. Are you ready to be inspired?', 'They rose above their grim financial circumstances, overcame all sorts of odds and defied stereotypes to top exams, get into IIMs and even pursue a PhD at 15! Here are their incredible stories: of unrelenting struggle and hard work. Are you ready to be inspired?\r\n\r\nShiva Kumar Nagendra\r\n\'For years I slept at midnight and woke up at 4am\'\r\n\r\nShiva Kumar Nagendra spots a man waiting outside the beautiful, white house. The sky is a brilliant shade of purple and a lone star dots the velvety morning sky.“Should I or should I not?” There\'s a battle going on between his heart and his mind.\r\n\r\n\r\n“But you HAVE to do it!”\r\n\r\n\r\n\r\nHis heart wins. And off walks 12-year-old Shiva towards the man in the white night suit, towards the moment that is going to change his life forever.\r\n\r\n\r\n\r\nShiva Kumar Nagendra’s parents had migrated to Bengaluru 25 years ago from Mysore. But his father, a truck driver, met with an accident that left him bed-ridden for almost three years. The family started facing financial problems. His mother took to making garlands so Shiva and his sister could sell them. But it wasn’t enough. “I started delivering newspapers. It was a morning job, so I could continue my studies,” says Shiva.However, with time, expenses went up further. “I could’ve worked at a restaurant or as a mechanic. But I didn’t give in.”\r\n\r\n\r\n\r\nOne morning, he spotted a gentleman waiting for his newspaper. “I handed him his paper and asked if I could wash his car in the evening for some more money.” He agreed. So that evening, after washing his car, Shiva mustered up the courage to ask, “Sir, I need Rs 15,000 to pay my school fees.” The man was taken aback. Shiva suggested the man visit his school, and deposit the money directly if he found any truth in Shiva’s case.\r\nThe next day, Krishna Veda Vysa visited Shiva’s school and discovered that he had always been a class topper. Convinced, he told Shiva, ‘I’ll take care of all your education expenses. Just focus on your studies and don’t quit!’\r\n\r\n\r\n\r\nSo Shiva carried on his daily grind along with his studies. But he was ambitious even at that age. He started a newspaper distribution agency, appeared for his class 10 exams and topped. He went on to get a seat at the Bangalore Institute of Technology (BIT) and was soon offered a job by Wipro.\r\n\r\n\r\n\r\n“Vyas uncle, an entrepreneur, thought that taking up the job would get me a fixed salary, but it wouldn’t change my life.” He insisted Shiva pursue higher studies.\r\n\r\nShiva was in a fix. He didn’t know how to break the news to his parents. “When I told them that I planned to decline the offer, they pleaded with me to work at least for a few years.” Shiva tried to explain to them, how an IIM/IAS student has far better career prospects. Eventually, his parents relented.\r\n\r\n\r\n\r\nShiva decided to take the Common Admission Test (CAT). Mr Vyasa got Shiva admitted to a tutorial centre. At this point, Shiva was balancing his engineering classes, his MBA preparation, and his fast-expanding newspaper agency.\r\n\r\n\r\n\r\nThe day the CAT results were due, Shiva was a nervous wreck. Though he had the Wipro job in hand, he wanted to walk through the gates of an IIM. And then, IIM Calcutta happened!Initially he felt out of place. “It was competitive and difficult to match the students — many from the best colleges of the country.” His background, however, had never been a matter of shame to Shiva. People at IIM respected where he came from and all that he had done to get where he had reached. He attributes his achievements not only to intelligence, but also hard work. “For years, I woke up at 4am and slept at midnight. I wasn\'t born with a silver spoon. I’ve worked hard for what I wanted.”\r\n\r\n\r\n\r\nShiva graduated from IIM this year and is working with a Bengaluru-based startup. Life, he says, will always try to knock you down. “It depends on how strong you are to get up and fight back.”Shalini Arnugam\r\n\r\n\'I didn\'t want anyone\'s pity\'\r\n\r\n\"Forgive me, my English is very poor,” says 17-year-old Shalini Arnugam. But for someone who has gone from a Tamil-medium to a Kannada-medium school, and is in an English-medium engineering college – Bengaluru-based Shalini has caught up pretty well.\r\n\r\n\r\n\r\nShe was the school topper in class 10 and scored 84.8 per cent in class 12. But while students took breaks from their exam routine, Shalini shuttled between houses, doing household chores to keep her family afloat.Shalini’s father, who used to paint hoardings, has been bed-ridden for over a decade after he fell off a building. Her mother began working as a domestic help. But they were hit by another tragedy: Shalini’s brother was diagnosed with blood cancer earlier this year, just before her class 12 exams.Immediately after the exams ended, Shalini took over her mother’s part-time jobs, while her mother stayed with her brother at the hospital.\r\n\r\n\r\n\r\nShalini wakes up at 4:30am – finishes chores at home, draws rangolis at five different houses, scrubs floors, washes utensils and clothes – her day passes by in a haze, with college classes in between. She studies late into the night, sitting at the entrance of her house and reading in the orange of the streetlamps. “Tuitions cost no less than Rs 60,000 a year. We don’t have that kind of money. But there\'s no point brooding over that.”\r\n\r\n\r\n', 'story_theclassroomoflife_7ef67a93-59e3-11e5-ac8c-005056b4648e.jpg', '1', 1, '2018-02-07 12:49:39'),
(75, 0, 'admin', '2018-Feb-07', '', 'Bernita Mondal  \'I want to work and relieve my family of their financial burden\'', 'Bernita Mondal can hear her parents snore in the next room. A single bead of sweat trickles down her temple and lands on her book. It\'s 2.30am. She\'s been sitting in the kitchen without a fan for two hours. Just then the bulb in the kitchen goes out. “Not again!\" Bernita sighs. She lights a candle and goes back to her book. \"Where was I? Yes, the workings of the motor.”\r\n\r\n\r\n\r\nBernita prefers studying at night even if it means having to sit in the sweltering heat in the kitchen (which adjoins the only room in the house). “I fan myself while studying. And try and not think about anything. Doing well in the exams is my top priority,” says the 18-year-old student.Bernita\'s father is an auto-rickshaw driver and her mother a homemaker. They moved out of a small town in West Bengal to settle in Bengaluru when she was a little girl. “Both my parents are illiterate. Perhaps that\'s why they\'ve always encouraged me to study,” says Bernita.\r\n\r\n\r\n\r\nHer maternal grandparents never had the wherewithal to pay for her mother\'s education. “My mother doesn\'t want me to suffer the same fate. I think she is living her dreams through me.”\r\n\r\n\r\n\r\nHer father\'s income was just enough to see the family through till Bernita\'s high school. He used to do odd jobs at a restaurant - cooking, cleaning dishes, scrubbing floors till he rented an auto. “To make more money, he would help carry people\'s luggage on the railway platform,” says Bernita\'s mother in broken Hindi. But as Bernita entered high school, her fees and the cost of books also increased. “It was getting very difficult to manage the cost of education along with the household expenses,” she says.That\'s when one of the nuns from her Christian school suggested they approach an NGO called Vidya that funds children from underprivileged backgrounds. “Someone from the NGO met my parents and after they were convinced, Vidya agreed to pay half of my fee. They have been funding me since then,” says Bernita.\r\n\r\n\r\n\r\nHer hard work has paid off – Bernita scored 95 per cent in her class 12 exams.\r\n\r\n\r\n\r\nBernita is in her first year of BTech. Her immediate aim is to get the gold medal that is given to rank-holders in her college every semester. After her engineering course, Bernita hopes to go abroad for an MTech. “A scholarship would be great. But if I don\'t get one, I hope to get a job and relieve my father of the family\'s financial burden.”\r\n\r\n\r\n\r\nBernita prepares in advance for her lessons next day. “I might not have money to take tuitions but what I have is the ability to work hard,” she says.', 'story_getimage.jpg', '1', 0, '2018-02-07 12:53:27'),
(76, 0, 'admin', '2018-Feb-07', '', 'Yogendra Singh    \'My father was a rickshaw-puller\'', '\"Why would you want to feature me in your magazine?” asks Yogendra Singh incredulously. The 28-year-old IIM Lucknow student has a dilemma: should he continue with his studies or resume working?\r\n\r\n\r\n\r\n“Your story is about people who have succeeded in life, isn\'t it? But if I quit IIM and go back to my village, that would make me a failure, right? Koi successful person ka story chaapiyega to woh theek rahega,” says the young man from a village near Daltonganj in Jharkhand.\r\n\r\n\r\n\r\nBut Yogendra is already a success.\r\n\r\n\r\n\r\nHe remembers the time he went off with his neighbour to get admitted to the nearest government school in his village. He studied there till class 8 and then moved to a school in a town eight kilometres away. “My father was a rickshaw-puller and my mother a homemaker. But they never had to pay for my education.” The pride in his voice is unmissable.Being the eldest of seven siblings – four sisters, and three brothers – Yogendra\'s struggles started early. He would graze cattle and soon after his class 10 Board exams, he started giving tuition to younger students. “I began making enough money to take care of the household expenses.”\r\n\r\n\r\n\r\nThat was when he met his future wife. She was one of his students before his prospective father-in-law, a primary school teacher, decided to make him the offer that would change his life forever.\r\n\r\n\r\n\r\nYogendra was juggling his life as a student and a teacher. But someone suggested that he should take the Polytechnic Diploma exam. He did and was selected too. “Suddenly I was famous in my gaon. That\'s when my to-be father-in-law proposed to fund my education on the condition that I marry his daughter,” he says. He did.\r\n\r\nYogendra discontinued his coaching classes, but worked for a few years while finishing BTech from BIT Sindri in Dhanbad, to be able to support his family. While his job took care of the household expenses, he took coaching for a few months to crack the CAT exam. “My happiness knew no bounds when I got a call from IIM Lucknow,” remembers Yogendra.\r\n\r\n\r\n\r\nWalking through the gates of one of India\'s most reputed and toughest B-Schools is a dream come true for any student. But accepting the offer from IIM Lucknow has given Yogendra sleepless nights. He can\'t stop wondering if he made the right choice. “There is no one to support my family right now. I spent all my savings on my sisters\' weddings. My younger brothers are in school. I am contemplating quitting and going back to my village,” says Yogendra.', 'story_brunchimg4.jpg', '1', 0, '2018-02-07 12:55:28'),
(77, 0, 'admin', '2018-Feb-07', '', 'Sushma Verma  \'I didn\'t even know what the Boards meant\'', 'Sushma Verma has always been filled with a deep sense of wonder at how life exists beyond what the naked eye recognises. This prompted her to take up microbiology for her post-graduation from the Babasaheb Bhimrao Ambedkar University (BBAU) in Lucknow. \r\n\r\n\r\n\r\nHer scorecard shows a first rank in her first, second and fourth semesters. She has now enrolled in a PhD course. And Sushma is all of 15 years!\r\n\r\n\r\n\r\nWhen she was just two-and-a-half years old, Sushma recited Ramayana \'chaupayis\' at a local function. She considers that the first proud moment of her life.When she was just two-and-a-half years old, Sushma recited Ramayana \'chaupayis\' at a local function. She considers that the first proud moment of her life.Born in the outskirts of Lucknow, Sushma\'s father was a daily-wage labourer, and mother, a homemaker. \"Our home comprised of a single room with a leaking ceiling. The main thought in my mind then was that all I have are my brother\'s books. So I have to study with his support.\" She was not even three years old then! \r\n\r\n\r\n\r\nWhen she was five years old, looking at her prodigious memory and skills, Sushma\'s family suggested she take the Board exam. Finally, she enrolled at St Meera\'s Inter College in Lucknow, in class 9. \"When we submitted my application form, the principal thought there had been some mistake, that my application was for nursery,\" laughs Sushma. She had to take a test which covered the entire syllabus from class 1 to 8 to check if she was eligible for class 9. She was. \r\n\r\n\r\n\r\nThe first few days in school left her tired. It was a task explaining to her classmates that she had not entered the class by mistake or that she didn\'t study 20 hours a day.\r\n\r\n\r\n\r\nIn June 2007, Sushma created history. Limca Book of Records recognised her as the youngest student, aged 7 years, 3 months and 28 days, to pass the class 10 Board exam in the country. \"At the time, I didn\'t even understand the significance of Board exams. This was only the second time in my entire life that I was taking an exam!\"She was also the subject of a documentary film by a Japanese television channel. \"It was a matter of great pride. We thought, why would they want to film us of all the people in the city,\" she says.\r\n\r\n\r\n\r\nSushma went on to graduate in Botany from Lucknow University at the age of 13, and finished her MSc at 15. However, she doesn\'t think of this as a feat. \"Most people think that only after the child turns 5-6 should s/he be taught to read and write. But it\'s important to pay attention to what he or she learns even before that.\"\r\n\r\n\r\n\r\nWhat makes her achievements special is that her father was appointed as a sanitation assistant at the same university she graduated from. Besides, just by being around the Verma siblings while they study, her mother today can read basic Hindi and English.\r\n\r\n\r\n\r\nWhat does the future hold for Sushma? \"It\'s impossible to know ki hum life mein kya banenge. Maybe my life will take a new turn.\"\r\n\r\n', 'story_download2.jpg', '1', 0, '2018-02-07 12:58:27'),
(80, 0, 'admin', '2018-Feb-08', '', 'Real life story of the innovator who inspired the upcoming Bollywood film PadMan', 'In 1998, when Arunachalam Muruganantham saw his wife using old rags for sanitary pads, he made a prototype that failed terribly. Thereafter, he used different materials and came up with new models for sanitary pads every month. Since there was a month gap between each prototype tested by his wife, he had no other choice except to ask for a few volunteers from a nearby medical college. Though a few female students agreed to try them, they were shy to give him the right feedback. So Muruganantham decided to test them himself.It took him two years to find the right material and another four years to come up with a way to process it. The result was an easy-to-use machine for producing low-cost sanitary pads. With the imported machines costing more than $5,00,000, Muruganantham\'s prototype came at just $950. As a result, women’s groups or schools can buy his machine, produce their own sanitary pads, and sell the surplus. In this way, Muruganantham\'s machine has created jobs for women in rural India. He has started a revolution in his own country, selling 1,300 machines to 27 states, and has recently begun exporting them to developing countries all over the world.Today, Muruganantham is one of India’s most well-known social entrepreneurs and TIME magazine named him as one of the 100 most influential people in the world in 2014.A Bollywood movie named PadMan has been made out of Muruganantham\'s story, starring Akshay Kumar, Radhika Apte, and Sonam Kapoor. The trailer which was released recently gathered more than 20 million views on YouTube. It was Twinkle Khanna, the lead actor\'s wife, who came up with the idea for this movie.\r\n\r\nAccording to a report by The Quint, Muruganantham, also known as ‘India’s Menstrual Man’, has already been widely documented and reported about by both the Indian and international media. Twinkle herself has based the story ‘The Sanitary Man from a Sacred Land’ in her new book The Legend of Lakshmi Prasad on this Padma Shri winner.\r\n\r\nHere\'s one of the TED talks that Muruganatham gave in Bengaluru:', 'story_yourstory-padman-bollywood-real-life.jpg', '1', 1, '2018-02-08 05:25:04'),
(81, 176, 'teacher', '2018-Feb-12', '', 'Fgghh@', 'Cf', '', '0', 0, '2018-02-12 11:59:07'),
(82, 241, 'teacher', '2018-Feb-15', '', 'Ahquh171', 'Ahuqva', '', '0', 0, '2018-02-15 11:23:24'),
(83, 241, 'teacher', '2018-Feb-15', '', 'VahbBha', 'ha', '', '0', 0, '2018-02-15 11:23:40'),
(84, 42, 'student', '2018-Feb-28', '', 'hbnb', 'vbhjh', '', '0', 0, '2018-02-28 05:36:27'),
(85, 85, 'student', '2018-Mar-14', '', 'vh', 'gg', '', '0', 0, '2018-03-14 11:37:20'),
(86, 124, 'student', '2018-Mar-15', '', 'hzs', 'usyss', '', '0', 0, '2018-03-15 10:10:11');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `parent_mob` varchar(20) NOT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `guardian_name` varchar(30) DEFAULT NULL,
  `guardian_email` varchar(50) DEFAULT NULL,
  `guardian_mobile` varchar(10) DEFAULT NULL,
  `guardian_relation` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `longitude` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `city` varchar(30) DEFAULT NULL,
  `state` varchar(30) DEFAULT NULL,
  `zip` varchar(30) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `school` varchar(50) DEFAULT NULL,
  `last_qualification` text NOT NULL,
  `aim` varchar(200) DEFAULT NULL,
  `passion_for_life` text,
  `passion` varchar(200) DEFAULT NULL,
  `preferred_coaching_time` time DEFAULT NULL,
  `teacher_in_range` decimal(4,2) DEFAULT NULL COMMENT 'range in km',
  `success_story` int(11) DEFAULT NULL,
  `profile_pic` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role` tinyint(4) NOT NULL DEFAULT '3',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_login` int(11) NOT NULL DEFAULT '1',
  `mobile_verify` int(11) NOT NULL DEFAULT '0',
  `fcm` varchar(255) NOT NULL,
  `referral_code` varchar(20) DEFAULT NULL,
  `added_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `parent_mob`, `gender`, `dob`, `guardian_name`, `guardian_email`, `guardian_mobile`, `guardian_relation`, `address`, `longitude`, `latitude`, `city`, `state`, `zip`, `country`, `class_id`, `school`, `last_qualification`, `aim`, `passion_for_life`, `passion`, `preferred_coaching_time`, `teacher_in_range`, `success_story`, `profile_pic`, `created_at`, `modified_at`, `role`, `status`, `is_login`, `mobile_verify`, `fcm`, `referral_code`, `added_by`) VALUES
(1, 'swetank', '', 'aradhna@gmail.com', '', '7011712000', '', 'Female', '1900-01-01', NULL, NULL, NULL, NULL, 'Asalatpur Village, Janakpuri, Delhi, 110058, India', '77.0765126', '28.6219977', NULL, NULL, NULL, NULL, NULL, 'abes engineering college', 'B.tech', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_1_1515586464.jpg', NULL, '2018-01-24 06:03:00', 3, 1, 0, 1, 'e20mt3nP10o:APA91bGXERxoH5Wj3C995Bc1dN745E52GOw1nvQLw4Lco9NE13kgeAU2Ewmh8v4YTKGWeWoMC-knPaoEYkDe5fKL41udCxwBxSpGdjwvNijDG4Rrec_Cl3r-wlToZEN-gNQ1iVWX12fH', NULL, 0),
(2, 'ashish sir', '', '', '', '9999575155', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_2_1518007117.jpg', NULL, '2018-02-07 12:38:37', 3, 1, 1, 1, 'fnsX5v8yStc:APA91bHlaIAD5kKyc9vQcBpDYckQpz-O44BBe3u6eKkW4vYIUTKm6lmjJnxOoOZrHZf8t2ppoWQmQQwFEnlNai35HMNEvGmmnIyjNYCcvwLMCQb1n32bAckfFgwAJVlcBrvKzBER-2h0', NULL, 0),
(3, 'Aman RajpuT', '', '', '', '8826465880', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.0765576', '28.6219574', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-03-09 11:57:20', 3, 1, 0, 1, 'eN9E_eH0vsI:APA91bG7tO-Vr8MoQcNcL5nd2xE1MG5OnWVV2w0KC0w5qj7wu9NvkpDgPDpU8G-0OkRFq3HHwD6zozEXAJUWBB6eeKJ-ziSWrqDSEdHpZIcQnnRTrWNL20rI7aqoiiNK5k8iwBad9F1g', NULL, 0),
(4, 'Amit K', '', '', '', '9560610340', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_4_1521182676.jpg', NULL, '2018-03-16 06:44:36', 3, 1, 0, 1, 'cM8ywDbG-fQ:APA91bGjyHxR5pF2PxwLW_JJML0bSEV9qVvo0vjD9LjIcD22lDJbZ_S_1mFAmm08DiqiVNEYgCzhjN4bRlKJNWsRD054NufE9CncVKfpvvAakBMaPHR03PgfIGs-BCfd_OJ0NiZLZ_jV', NULL, 0),
(5, 'smarti', '', '', '', '8447208109', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_5_1513848193.jpg', NULL, '2018-02-06 09:32:34', 3, 1, 0, 1, 'd-HbCI7uIyw:APA91bFqyT7ucHcizTZM2johJNz2Us8zLaiZF4Lo-6Ra1ECdRgTLXeGhWjf-RSHwHbliT3f_KyiUGSN9w1QOJPZgJzTl23rPNy0w81MrXwl43---AJs-RNSKRyjpQoUCdwOF8Y_Qfz57', NULL, 0),
(6, 'Rahul', '', 'rrahulkkhatri@gmail.com', '', '8700648727', '', 'Male', '1994-12-23', NULL, NULL, NULL, NULL, 'Janakpuri District Center, Janakpuri, Delhi, India', '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_6_1515509091.jpg', NULL, '2018-03-16 17:48:12', 3, 1, 0, 1, 'c1n9jPX6cQA:APA91bG4zJXHUa0EPMQ322av9uaQJetbE9jOPlSeQLXUXYALabfRkwEzHrypq7nYr6aPYagISlWGYGeg7X_zqKkHLVLvDPeecWVYO_B9_tiQmzeADfhg1qNE28tCpmTvc8WmWUT31_xj', NULL, 0),
(7, 'SK Singh', '', 'sonu.rrk@gmail.com', '', '8650462625', '', 'Male', '1987-10-07', NULL, NULL, NULL, NULL, 'Shanthipuram, Dhandera, Roorkee, Uttarakhand 247667, India', '77.8970665', '29.8404492', NULL, NULL, NULL, NULL, NULL, 'hnbgu', 'B.com,M.com,CA-CPT,Diploma in computer application .', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_7_1514034174.jpg', NULL, '2017-12-23 13:17:41', 3, 1, 0, 1, 'cFkZUEF9Xfc:APA91bECcrR39hhyAxhrDOKzpUdua5u-Gz7dQENLwyo5is6x07Gour82fdb-jHs5UWfTSGWhpIX0EaeaAbHt_3Vhph-mN6z3yX2N8qyOjZznFiipnMtFlT3pcxFdc1TYL-68QXttWgEF', NULL, 0),
(8, 'rajeev', '', '', '', '9927369333', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '78.145932', '29.9303313', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-23 12:24:32', 3, 1, 1, 1, 'fvlmwNh2_7Y:APA91bHHbUhuW5mxcG7eyCQaqNSHJ4G0_4dd1wnRnBwRRlLjnAlJkVJdFOg3WuU2GUdZzv7QFLzsf98pzo6OEK4c2uNZ9JC5HDQMtbvVOd2d24X9LhF2Yw0qHWnOqX_I1e4X9XJefr9T', NULL, 0),
(9, 'shikha', '', '', '', '9917078422', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '78.143817', '29.9292803', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-23 13:02:11', 3, 1, 1, 1, 'cMa2NR26oGc:APA91bHB2h9iZNHC54pGsVKOZz5Lp9HA8Fjv30tAohfeKo5se68BVadhbXvV4zhyMyzwHK9tRA_0CTby-CMTc1Vfr57cX9ceA-37gJu44wiT1Zbs_seuPRo3Vw_PL-YxUhIWvkNLTWZ8', NULL, 0),
(10, 'ajay singh rawat', '', '', '', '8279935760', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-23 14:27:29', 3, 1, 1, 1, 'fjGyBGB1iQA:APA91bHU3tSt5-mXw3LCSEaMOY0d4TN4EH7tdDR2fd_owZQiFzCfdjWWFae31R0wbBq6l7LLFsXqkc6k4jXbTPUod4T6wJKrvtlgaxUDiKV2MkVq8FeeIqOq8YMm1nhczack_40nwTJC', NULL, 0),
(11, 'devang varshney', '', '', '', '7017288957', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-24 03:38:16', 3, 1, 1, 1, 'd4fKXs7U354:APA91bGJGH5E564MV4oxfjPnL8OnoNNGF9btXCXhLUpNyo6-juZVWZqrqCPeGUVMq0pdEa3N1-dI0hhTdd5Gk0T0f2Bdj8Ywp6T7KL4OnlaWuYqtXqpgQ8l4cApOYzCHfjRljhC9sUbD', NULL, 0),
(12, 'pawan Kumar', '', '', '', '9568403225', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '78.0892607', '29.9205456', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-26 13:15:42', 3, 1, 1, 1, 'ftsMReQawcM:APA91bE0rPIgCyl34qtQpfee-CE1K6rtJxk_hCK7AnPNjDSYGrx3lpYql67H-b8-xbTbVjxLQIvHvTLECYwH5Ovqrqlg_hlTIN7F0sda9fmoAaXC34NMN3S8N4cFddl0umqD8a7iBym_', NULL, 0),
(13, 'Vishal', '', '', '', '8447075305', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.0764322', '28.6220348', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_13_1516979209.jpg', NULL, '2018-01-26 15:06:49', 3, 1, 1, 1, 'flg3bVaNOfE:APA91bH-ZqNF0hGPLrJPnu1c59d8FA9mX3hWOROEY4Lmq4ZslBVSSarIurEkk8iqrHVDx48aZevURFxLDhiJ0MwRutDlbARl4nW-TXCW8V93RHY5JGJ_RPsioVPc3eNtx4HB8f662Wbb', NULL, 0),
(14, 'rajan', '', '', '', '8368164392', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.06790166666666', '28.703098333333333', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-01 05:11:34', 3, 1, 1, 1, 'eMy6sFFalUw:APA91bFTgG0nLyOZ_8k93fwNhEObYZEzl5Rrtq-gFQAgP5ZLN-nEtulbx8D46Ks0XIj6poiwvY3OER_8R6z9UjmKHEslzVqC_RDw3ilm4CanIizKh5xOGtvEECsWgp-JTehWQTjKXUB-', NULL, 0),
(15, 'Ankit shokeen', '', '', '', '9211467916', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-30 09:33:15', 3, 1, 1, 1, 'dFjP6Bz5Z8c:APA91bFWoUnwoyNkoWsJ7JQCz_iFqTQq_aQ3fm2po6cwqcXpmOlPBpiztK6-OUzxEn5_dmyerlaZ9jKfMWtk515AWyTBBZAk2aqMn8YLhq6NsQWISTirNu6orlEyCPP5ITRG_RYiUgIv', NULL, 0),
(16, 'Bharat', '', '', '', '9540897827', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2017-12-30 18:10:12', 3, 1, 1, 1, 'fGr5dQ8ZroI:APA91bGhQ0eJTZcyCFdSkGR3d5coJVpbvZxjlnW--23vp9fDdUVbymNLW5QO_aYwoVS_y15sZRy-C98vZvZtbWse3pSB7_h5lZGL0WmPzglmObJo9LlMe1Iuq7zlR0qz78dfUNPm59WP', NULL, 0),
(17, 'Satyender', '', '', '', '9015187618', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-07 06:57:30', 3, 1, 1, 1, 'dnAOxlbOxwQ:APA91bFyR2gsekRuZCP5m_KBuwoTknIQw_1cZHXm08kVmC1edMWvAttYqz3yPatCwCxBKYkRlRUM_hjJBrf_sDzPTkjrW63DPauiLTjxNpe1FYtXuDt5aEwg7Ky5xNWqizuqB6sY4Yan', NULL, 0),
(18, 'Tapan KUMAR', '', '', '', '9760105999', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-08 07:48:52', 3, 1, 1, 1, 'd6LruZ9Oa_s:APA91bEfv2A93fUFGG8hBTrlEYn9Q-wC7pkrjP7x4PLTu_eir6btV3q7xOI0Ei8f9z91TaOkWU0vBvZibjIarS82TWLaaKzY_PhGoKNObfmGiT_oh1uDlvFV2wZCqh9f7ZgtteUb64m5', NULL, 0),
(19, 'jay kataria', '', '', '', '9911103077', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.0443249', '28.4760749', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-08 13:25:25', 3, 1, 1, 1, 'djYkEUQ6rVk:APA91bEE1LZ7CSs-hJM1oJz21do5h5NtUVPmzChExIAWG0zpxUot5uDvF4L_DH_-XWmerXZb0lWcnzYVqsheeGjmxgLksTpaPKqNQ-3TLGF8mCxiS8SZr4ibNOCisYlQfmxfNalpLJST', NULL, 0),
(20, 'Naved khan', '', '', '', '8802915021', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-03-10 07:53:12', 3, 1, 1, 1, 'cZEGzyuFmFQ:APA91bFLUOAJ8PCylQ_yeb943eQoBQcMC68p4qTGzhjyWpl4G5iksmCM5fgUnvvtOJ14TgRkObu93Y52ZcFZHlRkRd92ADdbNuv3ktXNNm0VkddqWKpdZR8U4otFQzBAx7HD-erNumnG', NULL, 0),
(21, 'Chetan Sharma', '', '', '', '9899816037', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-09 10:32:49', 3, 1, 1, 1, 'eg3zeecpp74:APA91bEB3behD-Gds_PiTeL9HSzIKnHFxTGEuxlz24XNtDrdLvbDk1IWTHg6DnpXtHU37Lb1cY_0xbwux9cJ1Cl99OufSXBAUBfuONvrAi3kbr9Rz6cwjTPSKmJNB4bd7DuI3qMKbhGG', NULL, 0),
(22, 'Vikas K', '', 'v@g.com', '', '9899575155', '', 'Male', '1994-12-21', NULL, NULL, NULL, NULL, 'Clock Tower (West Pin Code-110064 (state), Hari Nagar Ghanta Ghar (Clock Tower), Nanak Pura, Hari Nagar, New Delhi, Delhi 110064, India', '77.1108967', '28.624854300000003', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-12 08:47:07', 3, 1, 0, 1, 'fXtdb7ULtEE:APA91bFUMW25bmwZfLO7BgLjmWcgivjly1-4HH9s9LZz-IxrDkqg77EfewhRujtcmPbKjbfD5IWKy-Sdneztc0WNqbpP-oQ_WxhN0YcFx7DcpugNI5Sr5-6b5iVFXZ1FZEHJvHxdfGwV', NULL, 0),
(23, 'Gaurav kumar singh', '', '', '', '9971194897', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-06 12:38:34', 3, 1, 1, 1, 'edRBbrXNGe0:APA91bGBHfmNrAFdXXgfAjHU38WuC1p2z-1RqCNptVS3-5t7kuxNJd-lUitZxMtQs4J2l1hkZu7B3WcAeGQnCRyNdbqx7xjGnpsThC9TkJdvTfk0bW6FD3UtX8KD4l8XX5yIyzIJ4o2w', NULL, 0),
(24, 'Ruchi Kapil', '', '', '', '9756943502', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-11 13:24:31', 3, 1, 1, 1, 'fT7J1h8U_AI:APA91bFPltINNVdYQNjdRw6d4es9xL3pp6B0JgUSQLUGBYmeO39jBCB1k20gdj1VtuBYawFOvnMsO0n9Y6r97MfIB1OeA5pSNo8jiMneyAzvDfSiVXC8iqswcYo-oyxmmQJYvvTmiwzz', NULL, 0),
(25, 'vikas', '', 'v@gm.com', '', '9990731159', '', 'Male', '1990-12-08', NULL, NULL, NULL, NULL, 'Tilak Nagar, New Delhi, Delhi 110018, India', '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_25_1521006067.jpg', NULL, '2018-04-04 08:09:37', 3, 1, 0, 1, 'eT1vDYnAJlI:APA91bFED7AvJWWzY3PPJ7xfR-y_MRA6MO8FwBa3DvS3S2mv9h0KjCuF7Q8uXT0-yrVcuSnhudOL2zA7NfI_rLsUK0vrS0q2NhTA7AawKfbOhHl0BdAITJopUBfbNT6FwNdLf-75jPwZ', NULL, 0),
(26, 'Abhishek', '', '', '', '7703833026', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-18 16:40:54', 3, 1, 1, 1, 'emc9NwHZMHs:APA91bE5_Q0rvwk6PJacRyEd7CG5mVhjcni32BXj-xW6Tl6RJY7s07VeCWRhv1HbZGHfDw-EjEMs2xp6IRUjOWcvIY4nJMooxXCWGaAARKNC64hnmDzyn-iOAwZN34T-irK1KsI8OmNh', NULL, 0),
(27, 'RAHUL', '', '', '', '9599900901', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-19 13:20:16', 3, 1, 1, 1, 'ejNvupPwNbs:APA91bFnh_6B8LGvLNpJzZdu4H7B9p3aYXz1O25Xi1Nzc8c7mAAmQQpl2HGb9g7-fZ0ToFmPOAAcr9ezBgbvJaNmpLSMIqJdDmTKkLzOxTybE8zuVutqcg2t0T_x8eV0tso7jVvt9XWd', NULL, 0),
(28, 'Gurdyal Singh', '', '', '', '9211223301', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.0857794', '28.4808166', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-21 14:51:50', 3, 1, 1, 1, 'd1wepnzPERY:APA91bHHrIIreR8m_oEV-aWagzcAK_HI9dxEOVf0b0jjlCYF0rPG_WTcS76Mn6b2QfLZuXVVSWUBlCyyrrTbHCELH7lkDTQAQ4AKROEB-b2l1ywYvB0hk6U-mE_YCt5kpgwPdR3lw5To', NULL, 0),
(29, 'sidhant sehrawat', '', '', '', '9818701739', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.060629', '28.617513999999996', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_29_1516606016.jpg', NULL, '2018-02-12 14:58:46', 3, 1, 1, 1, 'd7ago_Vk2nw:APA91bECAI_Puzln0GMIqWHG2-FX4HIw9wGEJwZ6ZPh6NsMIIByNk5tOEDZu4BBKqiwGgnRSz83HSSfkkEW2nfveRIsQHt3OatI_4I9U_g0OBQ-lCvhLywr3dtvhMlFYK8A-BsMvTpm6', NULL, 0),
(30, 'swetank', '', 'swetank@gmal.com', '', '7011712623', '', 'Male', '1980-01-01', NULL, NULL, NULL, NULL, 'D-976, New Friends Colony, Block D, Friends Colony East, New Friends Colony, New Delhi, Delhi 110025, India', '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_30_1516795426.jpg', NULL, '2018-02-10 08:00:47', 3, 1, 0, 1, 'ccdedIDgt7E:APA91bFLJNWCc_oG5pgIvbh2fVHIQhp_7HqFUBYw_2NHlBYzKKR66AVQraQ6byrlSsAVs9cheVYilnTq7kYv_mxNAekXiE8NFr4zZevtIwXP9OVwBcBKbASReTQm0uJU6ojFG7T8wqG8', NULL, 0),
(31, 'Kinshuk', '', '', '', '8826504964', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-24 08:57:31', 3, 1, 1, 1, 'en4COE260WQ:APA91bGyu5SoDqsJ3w_zqXAwO1OSD2cipFoCYCxDw5QL1AX_HQSehRK2l3_s-5ku3CAR9nEnTwQoGdM84tXYe2LHGVewsIeXCpDe48Bej6YbwX8JBSxNyIlBFP0zzcNNc5ZBFWfz73Qg', NULL, 0),
(32, 'Sunita', '', '', '', '8860171151', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-25 11:18:16', 3, 1, 1, 1, 'fFs3V-nMCxw:APA91bEw9_QNrsc_KlBFlf545SH2lO3KWw4EIESY9xLuIA5EOhGxEhY6adpZNjYhPJzhchS5iQFpYHuFBrKTtKVTwxP0QgavYr42cB0JLfi92ToiO5LjKMaQ8AN4Zb74D7L0_r99u_v_', NULL, 0),
(33, 'Sikander', '', '', '', '9540316161', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-26 03:04:56', 3, 1, 1, 1, 'fbkVEKbtNyg:APA91bHcHB_Q7GrHnOpi41crGyu-MhFktAxWSMdfNjk5LUxaCjMFadGSd2kbEIU7tgGkbLEMS_xA82T2y8tXabu62prZsjDlsveGUuGqgtAa5Q6HZIUcC-L29WM519xU2ItTWPi98N5U', NULL, 0),
(34, 'Ashish chikkara', '', 'ashish.chikkara90@gmail.com', '', '9999275155', '', 'Male', '1980-01-03', NULL, NULL, NULL, NULL, 'Janakpuri District Center, Janakpuri, Delhi, India', '77.0801899', '28.6295538', NULL, NULL, NULL, NULL, NULL, 'Mait rohini', 'B.tech', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-29 09:15:31', 3, 1, 1, 1, 'c5tG9-gAclY:APA91bHzGxVXrlZV_Zk2vOkcRkhLOPnBFVNwk1cw3iVxsDx6nQuSVdB-GyVyT5gercBSSvx_GiSdE4jgKqaOWbtWaBRWOXBocLfX6eUALhx1BeLHpeqOTaQ8IdH_vi_K6sczRkVA20aI', NULL, 0),
(35, 'Sudhin Biswas', '', 'Sudhin.biswas@gmail.com', '', '9810453928', '', 'Male', '1975-01-17', NULL, NULL, NULL, NULL, 'Dwarka, New Delhi, Delhi, India', '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-09 11:17:03', 3, 1, 0, 1, 'f3QF-vR4eTM:APA91bFTvPx9C0vv_0hRJo3g1Mv-WCuXCDFjXYFT_JzfLt01gXsJWViD8FjmHlJ40kcXEDXG9RCaNDIaB5SuqrXaMc2sAcfwmmVltvrpu8fQyE2jfy3Nw0oAjRUQZpRIppJWi3JSn4sv', NULL, 0),
(36, 'Saurabh Singh', '', '', '', '9560613271', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_36_1517207628.jpg', NULL, '2018-04-02 12:03:08', 3, 1, 1, 1, 'e5aMk0GYloM:APA91bHT3HkLj7GiiiIOT7jEwPrLnNLSqv6_DPUWL-Cq6DU8pPSewruQtPH5B3Mgm4Dxbw-5zUS6zRSM38bOoAjv3iSKcubGVvLo_BzGERbNfAREFD4W9Mry6ppaZd1YtDIRbihW6rYT', NULL, 0),
(37, 'nishant arora', '', '', '', '9560609501', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_37_1517380080.jpg', NULL, '2018-01-31 06:28:00', 3, 1, 1, 1, 'cvLm8RHSLzM:APA91bHCtk4O73o0R0lWqzsuwlBcr5cuY5B0YPG7uOA8DDhnTCXh-3hWqrUqzOmOCbIV6VsucpCQCRWdJqmdWjuEPXg2_IwiZzar1G3WpcWjO7AMf4yg8FXk867YjilTGj56fXeCrO6u', NULL, 0),
(38, 'Rakesh lal Verma', '', '', '', '9958754103', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.089043', '28.8509409', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-01-31 12:54:57', 3, 1, 1, 1, 'c0FmhEtMhkE:APA91bF7ptiDO3FkhEaF6eyFWLJeJPCHDzvuvuJmroNowVNS6EQ7EA3Nl1U_6PqcmZzE2x-r8U8g_O3DIOwGHwBdUURftZSfj-XPLF_sNFzH8pR3JjDXH5_2fMd0NGmdzFh-b7ef7gDH', NULL, 0),
(39, 'noor alam', '', '', '', '8744958496', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '77.0732958', '28.7024585', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-03 04:42:53', 3, 1, 1, 1, 'feTB3QZwMT4:APA91bEjJn1vMmdmBRA2_nsoyxmRM48rYeB7neCJIF8tlVq3L5L6uZuOPYox_eXGoeTFYQdUx3YzJTpJ4aBcFzuENe85tJBv6OZ5Leg1tCZtodrEt5GzV8ltOZDVsCVa4G_2tuNlv61H', NULL, 0),
(40, 'Sumit Mathur', '', '', '', '9873452420', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-03 18:18:44', 3, 1, 1, 1, 'd7zpve9EQ_0:APA91bFsBDVOFFkdbZ3xrJM4-R51XEwdXB4nzkthLYK0DracLAVOAdaaS6KMInJ8udfC-M81fANX2VQTB7zk-EjUK8HCkSHDwJqY5CQBbVSa2MEwJ3HgBagZqfB-vAeMjeKMd5O1QpHA', NULL, 0),
(41, 'Satyavir', '', '', '', '9251888333', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-05 14:12:30', 3, 1, 1, 1, 'f2S8ERzWh_0:APA91bHBW1yN07-1dXCiRr3r1haTenwmAZ8zhSAF5ABgE7KHEkinnLTNiP_xdmvGB4HI9y83qcK7jLTYsE5En20kn0mcdT6_CMn_tkI9pBNSE7Agxlomtpu8h97LACr79XL-793GPT6o', NULL, 0),
(43, 'neeta', '', '', '', '9910803208', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-09 07:29:59', 3, 1, 1, 1, 'dpyk7uyF9ts:APA91bGRWk-hGfOoPcoQCJ-j3NMZDHag8fb491E8GkgePVwFuRQTQkSHfkaQgtfeJxsz03jhCoMzScWHmfFWSLEuDTnZi-X2LroMQKqOnZSddtLBB0SIWSRmsEiXW2XIay7rECJUAiQy', NULL, 0),
(44, 'kanishka sehrawat', '', '', '', '9210099067', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '77.04137745313346', '28.47854917868972', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-09 12:39:18', 3, 1, 1, 1, 'czdxIa8LAbw:APA91bEq0Fm1Xz6uCJN4yFdmftUJsgP4xEdwoEI0UX2XCVlUHU4bAUa5XcUTX7WnGTwYYEhtc2zgFfo3Zg4igKvFgnqCznagA7uz5PXCCc_AusnrPlnobr3SUPOGb0wQsPaTYSQxFpW_', NULL, 0),
(45, 'Brinda khattar', '', '', '', '9650035973', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-10 13:45:55', 3, 1, 1, 1, 'dNW0BS1uJtI:APA91bGZo0EPv3S8zEwYD795K6orWZR4P3OfKKARialoz2EiXmeiQNhkNEzkZQWKYTZ1cOn4bAXflvoH7m5SzMcj80tQ_EaLGi5xsGzs_03TUHdmuBvfZlbhRu2BFVqgdqs6y9h70xIj', NULL, 0),
(46, '', '', '', '', '7011087771', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-10 16:27:09', 3, 1, 1, 1, 'd7ago_Vk2nw:APA91bECAI_Puzln0GMIqWHG2-FX4HIw9wGEJwZ6ZPh6NsMIIByNk5tOEDZu4BBKqiwGgnRSz83HSSfkkEW2nfveRIsQHt3OatI_4I9U_g0OBQ-lCvhLywr3dtvhMlFYK8A-BsMvTpm6', NULL, 0),
(47, 'rahul', '', '', '', '8860186898', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-10 18:03:38', 3, 1, 1, 1, 'e76MI6tlylQ:APA91bGdXAKH9SqgJo3AJx_7OOT2_iYA6S6kfNJ26gCjfMFiCJiR8UV073VYklHkS6laVWZj_2446xRu9m6V8d7DBIGjmiX9vqT14RfLzEB56KD3d8sMBMSO2WjsaAx-Q8McOYONI-UL', NULL, 0),
(48, 'seema dabas', '', '', '', '9871010809', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-12 10:17:22', 3, 1, 1, 1, 'fy5SS0SuyCw:APA91bG6Tw53RTJkDbiG0CXdThRk2eVxL8FNpsK1i0sO3N4JygmDFdWpBNscVfdY2ud72unMDQlhLTD9NKPaJS2gJ8J4Z4wTyRgZsGjnVch-8j21B7M-5dXB_EyimpioIssbDcoGHad5', NULL, 0),
(49, 'Prashant', '', '', '', '8447385508', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-12 13:04:12', 3, 1, 1, 1, 'e0z5HZ0kmFU:APA91bGiPCnaNYMbwJrgzfSMJEmdz2Nf94lR0i1Xpggc38Ac9jQjLtda-CVu59yk47Bx6fK4kG9-bwpRi-ntuosA6-jiCXw_VgHLZgZvR-MfM6amQNDyiUJtyOwB5O7BysOZLOmabwJd', NULL, 0),
(50, 'rishi', '', '', '', '9873110537', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-12 13:38:54', 3, 1, 1, 1, 'dcfHjK4sQa0:APA91bFwhH30fJttS_EHpiCiR7inFBNRPoJB0xWRHfx7JYO90S4alKGFOoOT2w8tdEJgoj0D71qZGNJX9EJsRNTT1pp7JyCcaIJrZh367QrLdJwm0xTEKXZSceab6ceZ5HQtF5l7MDz3', NULL, 0),
(51, 'riya', '', '', '', '7289949833', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-12 14:38:59', 3, 1, 1, 1, 'csXSi6pljkY:APA91bG5JC8HYbBsp-QFrhS3HisA9TeuSD98e-pSAXqj7AAWD_Mws6Fo16H3CLyFJ3dqhp3DdGF-7MZje__qPckaGAKombGqd8_pCtz38fS0ZOSnaXyPMgiuompqAIzcy4dkcKorcKuN', NULL, 0),
(52, 'Mohan', '', '', '', '9017032191', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-13 06:51:45', 3, 1, 1, 1, 'dvoT9fgDNF8:APA91bGIhC1SRPDnY3X8RTR8lHKN1UC2FtKPT_CvNioTwTbP7x5-aarm2C55vJ6uUlc-7s8q5fOG0eC5x-PTcTL-YDJJKTAKGXvi5yWhYlMddPKRg5NT7zhxPsLOKJoWO9IeVevatK62', NULL, 0),
(53, 'parul', '', '', '', '8588011206', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-13 07:41:01', 3, 1, 1, 1, 'dutB7OeJmi8:APA91bFRTMPJrR22CAFXOe6ve4JKhDKyjEg-qUzdDaXPbnRm0tBRm-nFllttCcZQCDlgs0EO0tsbgMqfzBHRPNmRACFyts3k3lXdUFXUq_i7DGQ7SlGuicW9Y0X9YZoKHqS0TqsoqAgt', NULL, 0),
(54, 'Manjula', '', '', '', '9810534234', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-13 09:24:01', 3, 1, 1, 1, 'cYJE5cZTOWc:APA91bGCLOOkA5kBtXTgKZJExrE9j3007o2kpjcE7cBLZlks-C-AdhR5o29uJRBgJzij531hmhl_Fc_EHikhK83gjojqrtdYSW6pQ3LFNSisP7UVMYOQaZ2PIiZqW8Y9TAXcLvYW2Exy', NULL, 0),
(55, 'renu', '', '', '', '9891262647', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-13 09:55:59', 3, 1, 1, 1, 'fiqsea3ohB4:APA91bFutF8KMR10BG3wMcvFWmAM8b3ndGyzSlZAWJRV8J8vYrAti9rZyg0HJJEONb-HqBJ5j8KN-0u-eORkKvs4JdgQMsorlZPKMwuSHLDWoq8-7UoEt6k3t0bWuwgOc6q96o_pEwqT', NULL, 0),
(56, '', '', '', '', '9953576856', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '77.0705252', '28.6895699', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-13 14:14:48', 3, 1, 1, 1, 'f-mg-asoECU:APA91bGnZdBDS7dHisLSFW4K4MH0x9vWB7p5kcrliRJYro9p7qw9r79eUP3hsrDaJS7bYKEQG5J985GAZRJ_MzYGcd9uBCeu44_1YmDC0TCF0OUodGSko5HOxsBJkZQNljKM-xGAfcTZ', NULL, 0),
(57, 'Aditya', '', '', '', '7906655031', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-02-14 13:22:44', 3, 1, 1, 1, 'fzF7UZjSfyM:APA91bHpC2voabi4E35oA6YUIv5ivjR_K2vKqLg4WCfSRVl7YeotKOQcePi0EcztGylbh0qZbWwrEHYivQzDgV7GJlLIIXYhx26vl-rxUVDjGeA8A562obI8z6jifojmBp5NJGjtBQOP', NULL, 0),
(58, 'Amit K', '', '', '', '8076423788', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_58_1519377941.jpg', '0000-00-00 00:00:00', '2018-02-23 11:54:14', 3, 1, 0, 1, 'e-KOMrOYZt8:APA91bEjsohEPC1aUX4GF_VVLOovbMDx-Lcisg-0CwZ0tM_7ljBFkbx4M_2DIkJFVz5rSuhEx2c4GjaHpZ6TNGU_G3FOXrERuHovCAWz7vQoA_uNcnQLV-27PZYGIVOWxucIjDkW4YWm', NULL, 75),
(61, 'Din', '', '', '', '123456789', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(62, 'Vhhh', '', '', '', '6895868868', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(63, 'Huhgvhvbiibb', '', '', '', '9669968683', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(64, 'Chhv', '', '', '', '8955458686', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(65, 'Hbubub', '', '', '', '9668692839', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(66, 'Vikedjdj', '', '', '', '9864864064', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(67, 'Jvjdjdih', '', '', '', '8613514681', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(68, 'Hsccbidckbdc', '', '', '', '3551148658', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(69, 'Jv sd jhfshicsckbsdxbo', '', '', '', '6846516188', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(70, 'Pinky12', '', '', '', '12349', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 176),
(71, 'Ttt', '', '', '', '2554436984', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 176),
(72, 'Hha', '', '', '', '9461318186', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 176),
(73, '75', '', '', '', '9995636369', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(74, 'Pinkt', '', '', '', '9431812973', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 176),
(75, 'Bjk', '', '', '', '9658066382', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 176),
(76, 'Bsjsj', '', '', '', '6595959864', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(77, 'Pinky', '', '', '', '9604513248', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(78, 'Jaia18#-#!', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(79, 'Chj', '', '', '', '0994', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(80, 'Dtyihczas', '', '', '', '8523443985', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(81, 'B', '', '', '', '9431012491', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(82, 'Hcjciviv.', '', '', '', '9868885586', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(83, 'Jauwnwiw', '', '', '', '6131461349', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(84, 'Jskw', '', '', '', '9431131461', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(86, 'Hauw', '', '', '', '9461813181', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(87, 'Jwuw', '', '', '', '6431842191', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(88, 'Hdhdhd', '', '', '', '9464944649', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(89, 'Ggy', '', '', '', '5654473395', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(90, 'Vjko', '', '', '', '8886658523', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(91, 'Bshwks', '', '', '', '6431813181', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(92, 'Xghu', '', '', '', '5563148632', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(93, 'Ycc', '', '', '', '6773842685', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(94, 'Xgjkff', '', '', '', '8535423841', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(95, 'Chhfd', '', '', '', '5665233254', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(96, 'Yuery', '', '', '', '8566425249', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(97, 'Yttu', '', '', '', '5224123699', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(98, 'Hshw', '', '', '', '9431843316', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(99, 'Nshsbs', '', '', '', '9461319131', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(100, 'Jajw', '', '', '', '4613173419', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(101, 'Pooja Sharma', '', '', '', '9288282551', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(102, 'Hshww', '', '', '', '9431318134', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(103, 'Shwi', '', '', '', '9464943146', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(104, 'Ghjk', '', '', '', '6982241236', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(105, 'Vgjj', '', '', '', '8523695223', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(106, 'Hsus', '', '', '', '9348131249', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(107, 'Bshs', '', '', '', '9464643446', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(108, 'Bahws', '', '', '', '9764913191', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(109, 'Jaija', '', '', '', '9464319131', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(110, 'Dnydnhdhn', '', '', '', '9595469446', '5119191616', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(111, 'Ggfdf', '', '', '', '8536082238', '5364846454', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(112, 'Bshe', '', '', '', '9431846161', '9464131913', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(113, 'Hjj', '', '', '', '5336985556', '9690066582', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(114, 'Hh', '', '', '', '9666335853', '8566335241', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(115, 'Ieh', '', '', '', '9734618134', '9461265913', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(116, 'Baja', '', '', '', '9464943191', '9161', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(118, 'Deekhsa', '', '', '', '9131913191', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(119, 'Chjvjvvi', '', '', '', '8966866860', '6686506805', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 75),
(120, 'Bjj', '', '', '', '9666431913', '964', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(121, 'harish', '', '', '', '8447195992', '', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, '0.0', '0.0', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-03-12 09:24:51', 3, 1, 0, 1, 'cUDqF2cByWk:APA91bFVpcGUJzFgRNZrnNOnR14pRIjFgbbBRwRYwOOaMbcduc8qAf3w--o-9f_1mD-Yb3sfccmGXPiI-vdMBBIVJHboR97tNATum-tCA_pFo3k4MQboOSyzbqUqsNZrUWjgC-7PaJsl', NULL, 0),
(122, 'Hshs', '', '', '', '9464311931', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(123, 'Hjss', '', '', '', '6416131643', '9461', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(124, 'deeksha', '', 'guptadeeksha1709@gmail.com', '', '9039246053', '', 'Female', '2018-03-08', NULL, NULL, NULL, NULL, 'Hidalgo NSN, Zona Centro, 87500 Valle Hermoso, Tamps., Mexico', '77.0765476', '28.6219611', NULL, NULL, NULL, NULL, NULL, 'elekdnskw', 'bcom', NULL, NULL, NULL, NULL, NULL, NULL, 'profile_124_1521116650.jpg', NULL, '2018-03-19 06:03:12', 3, 1, 0, 1, 'f0VQlYV2PA4:APA91bF874newxZ9Bhlv0PrCtnDzhDMyU6U_8Pkg3v0WsEMTF9VEJPmhOcobkucypGcNIjxXksawfwj0f1bqPqCxtx5OvBg4eS0kGWPFUH46jBncu6m2DR8AMhS_D8mmE5tuZaNtQkMl', NULL, 0),
(125, 'deeksha', '', 'deekshagupta237@gmail.com', '', '9004988285', '', 'Female', NULL, NULL, NULL, NULL, NULL, NULL, '77.076332', '28.6139205', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'default.jpg', NULL, '2018-04-04 12:27:12', 3, 1, 1, 1, 'f352C7rf7-Q:APA91bH-iNP-p_JZvgkyiEPcy7JcvIr_c5oxCuuV-gcpxpbiZZ8rGN9hJgY4qN0zFmuEa5pMTauw9t2hJxvX2K4Vim0vj2ocHUxxJWJbmMHEXXV7H14jPHGhRxX71WfhZekg_mjl6_L2', NULL, 0),
(126, 'Bsjskw', '', '', '', '9464191334', '9464', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(127, 'Gg', '', '', '', '9653523685', '963', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(128, 'Bziskemw', '', '', '', '9464913491', '943', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(129, 'Bsuw', '', '', '', '6431913191', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 241),
(130, 'Yyyy', '', '', '', '9039253248', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(131, 'Hhjiff', '', '', '', '8633546335', '8532523352', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(132, 'Vbhu', '', '', '', '8863335486', '8532179331', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(133, 'Vhhi', '', '', '', '8635785324', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(134, 'Bjk', '', '', '', '0966523366', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(135, 'Vjj', '', '', '', '9666558632', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 249),
(136, '', '', '', '', '7906398702', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '77.0765463', '28.6219619', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-19 05:54:38', 3, 1, 1, 1, 'f0VQlYV2PA4:APA91bF874newxZ9Bhlv0PrCtnDzhDMyU6U_8Pkg3v0WsEMTF9VEJPmhOcobkucypGcNIjxXksawfwj0f1bqPqCxtx5OvBg4eS0kGWPFUH46jBncu6m2DR8AMhS_D8mmE5tuZaNtQkMl', NULL, 0),
(137, 'Bsjs', '', '', '', '9131613613', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 253),
(138, 'Bsh', '', '', '', '6431913195', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 253),
(139, 'Bhh', '', '', '', '8532217639', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3, 1, 1, 0, '', NULL, 253);

-- --------------------------------------------------------

--
-- Table structure for table `student_assigment`
--

CREATE TABLE `student_assigment` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `assigment_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_batch`
--

CREATE TABLE `student_batch` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `installment_plan` int(11) DEFAULT NULL,
  `feedback` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_contribution`
--

CREATE TABLE `student_contribution` (
  `id` int(11) NOT NULL,
  `contribution_type` varchar(255) NOT NULL,
  `student_id` double NOT NULL,
  `contribution_detail` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modify` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_event_rel`
--

CREATE TABLE `student_event_rel` (
  `id` bigint(22) NOT NULL,
  `student_id` double NOT NULL,
  `event_id` double NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_event_rel`
--

INSERT INTO `student_event_rel` (`id`, `student_id`, `event_id`, `status`) VALUES
(1, 1, 1, 1),
(2, 6, 6, 1),
(3, 11, 6, 1),
(4, 6, 7, 1),
(5, 1, 6, 1),
(6, 1, 7, 1),
(7, 25, 12, 1),
(8, 25, 22, 1),
(9, 1, 26, 1),
(10, 29, 12, 1),
(11, 29, 6, 1),
(12, 29, 7, 1),
(13, 3, 35, 1),
(14, 3, 38, 1),
(15, 20, 12, 1),
(16, 30, 39, 1),
(17, 30, 37, 1),
(18, 42, 12, 1),
(19, 42, 38, 1),
(20, 25, 43, 1),
(21, 42, 39, 1),
(22, 85, 39, 1),
(23, 124, 39, 1),
(24, 124, 22, 1),
(25, 124, 52, 1),
(26, 124, 53, 1),
(27, 125, 55, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student_package_info`
--

CREATE TABLE `student_package_info` (
  `id` int(11) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `yearly_package` varchar(255) NOT NULL,
  `monthly_fee` double NOT NULL,
  `paid_fee` double NOT NULL,
  `due_fee` double NOT NULL,
  `mode_of_payment` varchar(255) NOT NULL,
  `student_feedback` longtext NOT NULL,
  `created_date` datetime NOT NULL,
  `modify` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_search_query_pending`
--

CREATE TABLE `student_search_query_pending` (
  `id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `class_id` varchar(10) NOT NULL,
  `search_type` varchar(30) NOT NULL,
  `student_lat` varchar(30) NOT NULL,
  `student_long` varchar(30) NOT NULL,
  `subject_id_list` text NOT NULL,
  `added_date` varchar(30) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0: pending , 1: completed'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_search_query_pending`
--

INSERT INTO `student_search_query_pending` (`id`, `student_id`, `class_id`, `search_type`, `student_lat`, `student_long`, `subject_id_list`, `added_date`, `status`) VALUES
(1, '25', '1', 'Institute & Home Tutor', '28.6219442', '77.0765636', 'a:3:{i:0;a:1:{s:2:\"id\";s:4:\"1880\";}i:1;a:1:{s:2:\"id\";s:4:\"1879\";}i:2;a:1:{s:2:\"id\";s:4:\"1878\";}}', '23-02-2018 11:05:41', '0'),
(2, '25', '109', 'Institute & Home Tutor', '28.6219313', '77.0765492', 'a:2:{i:0;a:1:{s:2:\"id\";s:4:\"1453\";}i:1;a:1:{s:2:\"id\";s:4:\"1454\";}}', '23-02-2018 11:17:39', '0'),
(3, '25', '1', 'Institute & Home Tutor', '28.6219442', '77.0765636', 'a:1:{i:0;a:1:{s:2:\"id\";s:4:\"1880\";}}', '23-02-2018 11:29:51', '0'),
(4, '25', '109', 'Institute & Home Tutor', '28.6219323', '77.0765751', 'a:2:{i:0;a:1:{s:2:\"id\";s:4:\"1454\";}i:1;a:1:{s:2:\"id\";s:4:\"1456\";}}', '27-02-2018 15:22:45', '0'),
(5, '42', '261', 'Institute & Home Tutor', '28.6219388', '77.0765708', 'a:2:{i:0;a:1:{s:2:\"id\";s:4:\"1892\";}i:1;a:1:{s:2:\"id\";s:4:\"1891\";}}', '28-02-2018 11:13:46', '0'),
(6, '42', '111', 'Institute & Home Tutor', '28.6219447', '77.0765732', 'a:2:{i:0;a:1:{s:2:\"id\";s:4:\"1465\";}i:1;a:1:{s:2:\"id\";s:4:\"1466\";}}', '28-02-2018 12:08:04', '0'),
(7, '42', '109', 'Institute & Home Tutor', '28.6219431', '77.0765734', 'a:1:{i:0;a:1:{s:2:\"id\";s:4:\"1456\";}}', '28-02-2018 12:08:33', '0'),
(8, '42', '130', 'Institute & Home Tutor', '28.6219594', '77.0765472', 'a:1:{i:0;a:1:{s:2:\"id\";s:4:\"1180\";}}', '06-03-2018 12:19:55', '0'),
(9, '42', '109', 'Institute & Home Tutor', '28.6219584', '77.0765492', 'a:1:{i:0;a:1:{s:2:\"id\";s:4:\"1456\";}}', '06-03-2018 12:20:55', '0'),
(10, '124', '261', 'Institute & Home Tutor', '28.6219618', '77.076548', 'a:2:{i:0;a:1:{s:2:\"id\";s:4:\"1892\";}i:1;a:1:{s:2:\"id\";s:4:\"1891\";}}', '15-03-2018 15:16:22', '0'),
(11, '124', '96', 'Institute & Home Tutor', '28.6219627', '77.0765463', 'a:3:{i:0;a:1:{s:2:\"id\";s:4:\"1126\";}i:1;a:1:{s:2:\"id\";s:4:\"1128\";}i:2;a:1:{s:2:\"id\";s:4:\"1131\";}}', '15-03-2018 15:16:36', '0'),
(12, '124', '7', 'Institute & Home Tutor', '28.621963', '77.076547', 'a:1:{i:0;a:1:{s:2:\"id\";s:3:\"339\";}}', '15-03-2018 16:01:34', '0'),
(13, '124', '8', 'Institute & Home Tutor', '28.6219615', '77.0765471', 'a:5:{i:0;a:1:{s:2:\"id\";s:3:\"349\";}i:1;a:1:{s:2:\"id\";s:3:\"348\";}i:2;a:1:{s:2:\"id\";s:3:\"351\";}i:3;a:1:{s:2:\"id\";s:3:\"352\";}i:4;a:1:{s:2:\"id\";s:3:\"353\";}}', '15-03-2018 16:01:48', '0'),
(14, '124', '8', 'Institute & Home Tutor', '28.6219613', '77.076547', 'a:6:{i:0;a:1:{s:2:\"id\";s:3:\"350\";}i:1;a:1:{s:2:\"id\";s:3:\"349\";}i:2;a:1:{s:2:\"id\";s:3:\"348\";}i:3;a:1:{s:2:\"id\";s:3:\"351\";}i:4;a:1:{s:2:\"id\";s:3:\"352\";}i:5;a:1:{s:2:\"id\";s:3:\"353\";}}', '15-03-2018 16:02:06', '0'),
(15, '124', '6', 'Institute & Home Tutor', '28.6219614', '77.076547', 'a:6:{i:0;a:1:{s:2:\"id\";s:3:\"322\";}i:1;a:1:{s:2:\"id\";s:3:\"323\";}i:2;a:1:{s:2:\"id\";s:3:\"324\";}i:3;a:1:{s:2:\"id\";s:3:\"325\";}i:4;a:1:{s:2:\"id\";s:3:\"326\";}i:5;a:1:{s:2:\"id\";s:3:\"327\";}}', '15-03-2018 16:02:19', '0'),
(16, '124', '10', 'Institute & Home Tutor', '28.6219613', '77.076547', 'a:6:{i:0;a:1:{s:2:\"id\";s:3:\"374\";}i:1;a:1:{s:2:\"id\";s:3:\"375\";}i:2;a:1:{s:2:\"id\";s:3:\"376\";}i:3;a:1:{s:2:\"id\";s:3:\"377\";}i:4;a:1:{s:2:\"id\";s:3:\"378\";}i:5;a:1:{s:2:\"id\";s:3:\"379\";}}', '15-03-2018 16:02:33', '0'),
(17, '124', '7', 'Institute & Home Tutor', '28.6219613', '77.076547', 'a:6:{i:0;a:1:{s:2:\"id\";s:2:\"31\";}i:1;a:1:{s:2:\"id\";s:2:\"63\";}i:2;a:1:{s:2:\"id\";s:3:\"336\";}i:3;a:1:{s:2:\"id\";s:3:\"337\";}i:4;a:1:{s:2:\"id\";s:3:\"339\";}i:5;a:1:{s:2:\"id\";s:3:\"340\";}}', '15-03-2018 16:02:57', '0'),
(18, '124', '8', 'Institute & Home Tutor', '28.6219612', '77.0765468', 'a:7:{i:0;a:1:{s:2:\"id\";s:3:\"350\";}i:1;a:1:{s:2:\"id\";s:3:\"349\";}i:2;a:1:{s:2:\"id\";s:3:\"348\";}i:3;a:1:{s:2:\"id\";s:3:\"351\";}i:4;a:1:{s:2:\"id\";s:3:\"352\";}i:5;a:1:{s:2:\"id\";s:3:\"353\";}i:6;a:1:{s:2:\"id\";s:3:\"354\";}}', '15-03-2018 16:03:15', '0'),
(19, '124', '96', 'Institute & Home Tutor', '28.6219637', '77.0765461', 'a:6:{i:0;a:1:{s:2:\"id\";s:4:\"1125\";}i:1;a:1:{s:2:\"id\";s:4:\"1126\";}i:2;a:1:{s:2:\"id\";s:4:\"1128\";}i:3;a:1:{s:2:\"id\";s:4:\"1129\";}i:4;a:1:{s:2:\"id\";s:4:\"1130\";}i:5;a:1:{s:2:\"id\";s:4:\"1131\";}}', '15-03-2018 16:03:31', '0'),
(20, '124', '1', 'Institute & Home Tutor', '28.6219607', '77.0765472', 'a:6:{i:0;a:1:{s:2:\"id\";s:4:\"1880\";}i:1;a:1:{s:2:\"id\";s:4:\"1879\";}i:2;a:1:{s:2:\"id\";s:4:\"1878\";}i:3;a:1:{s:2:\"id\";s:4:\"1877\";}i:4;a:1:{s:2:\"id\";s:4:\"1876\";}i:5;a:1:{s:2:\"id\";s:4:\"1881\";}}', '15-03-2018 16:06:19', '0'),
(21, '124', '7', 'Institute & Home Tutor', '28.6219608', '77.0765471', 'a:9:{i:0;a:1:{s:2:\"id\";s:2:\"31\";}i:1;a:1:{s:2:\"id\";s:2:\"63\";}i:2;a:1:{s:2:\"id\";s:3:\"336\";}i:3;a:1:{s:2:\"id\";s:3:\"337\";}i:4;a:1:{s:2:\"id\";s:3:\"339\";}i:5;a:1:{s:2:\"id\";s:3:\"340\";}i:6;a:1:{s:2:\"id\";s:3:\"341\";}i:7;a:1:{s:2:\"id\";s:3:\"342\";}i:8;a:1:{s:2:\"id\";s:3:\"343\";}}', '15-03-2018 16:06:40', '0'),
(22, '124', '8', 'Institute & Home Tutor', '28.6219608', '77.0765476', 'a:7:{i:0;a:1:{s:2:\"id\";s:3:\"350\";}i:1;a:1:{s:2:\"id\";s:3:\"349\";}i:2;a:1:{s:2:\"id\";s:3:\"348\";}i:3;a:1:{s:2:\"id\";s:3:\"351\";}i:4;a:1:{s:2:\"id\";s:3:\"352\";}i:5;a:1:{s:2:\"id\";s:3:\"353\";}i:6;a:1:{s:2:\"id\";s:3:\"354\";}}', '15-03-2018 16:07:02', '0'),
(23, '124', '10', 'Institute & Home Tutor', '28.6219491', '77.0765643', 'a:6:{i:0;a:1:{s:2:\"id\";s:3:\"374\";}i:1;a:1:{s:2:\"id\";s:3:\"375\";}i:2;a:1:{s:2:\"id\";s:3:\"376\";}i:3;a:1:{s:2:\"id\";s:3:\"377\";}i:4;a:1:{s:2:\"id\";s:3:\"378\";}i:5;a:1:{s:2:\"id\";s:3:\"379\";}}', '15-03-2018 16:07:39', '0'),
(24, '124', '218', 'Institute & Home Tutor', '28.6219608', '77.0765477', 'a:5:{i:0;a:1:{s:2:\"id\";s:4:\"1637\";}i:1;a:1:{s:2:\"id\";s:4:\"1638\";}i:2;a:1:{s:2:\"id\";s:4:\"1639\";}i:3;a:1:{s:2:\"id\";s:4:\"1640\";}i:4;a:1:{s:2:\"id\";s:4:\"1641\";}}', '15-03-2018 16:08:09', '0'),
(25, '124', '8', 'Institute & Home Tutor', '28.6219607', '77.0765476', 'a:6:{i:0;a:1:{s:2:\"id\";s:3:\"349\";}i:1;a:1:{s:2:\"id\";s:3:\"348\";}i:2;a:1:{s:2:\"id\";s:3:\"351\";}i:3;a:1:{s:2:\"id\";s:3:\"352\";}i:4;a:1:{s:2:\"id\";s:3:\"353\";}i:5;a:1:{s:2:\"id\";s:3:\"354\";}}', '15-03-2018 16:11:08', '0');

-- --------------------------------------------------------

--
-- Table structure for table `student_subject_rel`
--

CREATE TABLE `student_subject_rel` (
  `id` bigint(22) NOT NULL,
  `student_id` bigint(22) NOT NULL,
  `subject_id` bigint(22) NOT NULL,
  `class_id` bigint(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `study_material`
--

CREATE TABLE `study_material` (
  `id` int(10) UNSIGNED NOT NULL,
  `course_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `file_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `course_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `duration` varchar(255) DEFAULT NULL,
  `duration_type` tinyint(1) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teachernotification`
--

CREATE TABLE `teachernotification` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `notification` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachernotification`
--

INSERT INTO `teachernotification` (`id`, `teacher_id`, `batch_id`, `heading`, `batch_name`, `notification`, `created`) VALUES
(1, 1, 2, '', 'ENglish Batch', 'tomortow will be no claas', '2017-12-21 08:31:45'),
(2, 26, 6, '', '11 Class Accounts', 'holiday', '2017-12-23 09:34:39'),
(3, 28, 9, '', 'Class8', 'kal party h', '2017-12-23 14:45:00'),
(4, 9, 1, '', '11:00 am - 12:00 pm', 'hellooo', '2018-01-10 12:14:54'),
(5, 99, 56, '', 'A1', 'I won\'t be attending the class', '2018-01-21 08:05:17'),
(6, 75, 52, '', 'Ie8its', 'Hello student 26 January will be holiday', '2018-01-24 09:33:38'),
(7, 49, 104, '', '06:00 am - 07:00 am', 'usifdidt', '2018-01-24 12:28:23'),
(8, 49, 104, '', '06:00 am - 07:00 am', '84e85edof', '2018-01-24 13:39:28'),
(9, 49, 104, '', '06:00 am - 07:00 am', 'jfjjfj', '2018-01-25 07:29:52'),
(10, 49, 104, '', '06:00 am - 07:00 am', 'usrutsjf', '2018-01-25 07:35:10'),
(11, 49, 104, '', '06:00 am - 07:00 am', 'jfsjjf', '2018-01-25 07:35:27'),
(12, 87, 114, '', '05:00 am - 06:00 am', 'Snsnsns', '2018-01-25 07:45:18'),
(13, 87, 114, '', '05:00 am - 06:00 am', 'Sb so s sbs s s', '2018-01-25 10:44:51'),
(14, 74, 64, '', 'Helloo', 'Bzbzbz', '2018-01-25 11:46:21'),
(15, 74, 64, '', 'Helloo', 'Nsnsbs', '2018-01-25 11:46:45'),
(16, 117, 65, '', 'Robotics', 'tomorrow is holiday', '2018-01-29 06:13:52'),
(17, 164, 95, '', 'Maths Classes', 'Alta d vista', '2018-02-06 09:36:10'),
(18, 176, 204, '', '03:00 pm - 04:00 pm', 'hiii', '2018-03-06 06:28:22'),
(19, 176, 202, '', '04:00 pm - 05:00 pm', 'ghjm', '2018-03-06 10:22:34'),
(20, 176, 202, '', '04:00 pm - 05:00 pm', 'bbjj', '2018-03-06 10:24:23'),
(21, 176, 183, '', '05:00 am - 06:00 am', 'hsjs', '2018-03-06 10:26:37'),
(22, 176, 183, '', '05:00 am - 06:00 am', 'bbn', '2018-03-06 10:28:37'),
(23, 241, 162, '', 'Bske', 'guuuu', '2018-03-09 10:43:05'),
(24, 241, 162, '', 'Bske', 'bbb', '2018-03-09 10:47:39'),
(25, 241, 162, '', 'Bske', 'nnn', '2018-03-09 10:47:47'),
(26, 241, 165, '', 'Yee', 'ggh', '2018-03-14 11:16:57'),
(27, 241, 165, '', 'Yee', 'huww', '2018-03-15 09:59:56'),
(28, 241, 165, '', 'Yee', 'bswww', '2018-03-15 10:01:07'),
(29, 241, 165, '', 'Yee', 'bajasm', '2018-03-15 10:01:38'),
(30, 241, 165, '', 'Yee', 'bqjqnw', '2018-03-15 10:01:44'),
(31, 241, 165, '', 'Yee', 'bjkhff', '2018-03-15 11:15:07'),
(32, 241, 165, '', 'Yee', 'chipjf', '2018-03-15 11:15:33'),
(33, 241, 165, '', 'Yee', 'ghjk', '2018-03-15 11:15:39'),
(34, 241, 166, '', 'Hinku', 'benebbb', '2018-03-16 06:53:02'),
(35, 241, 167, '', 'Nsje', 'bsjw', '2018-03-16 09:08:31'),
(36, 249, 168, '', 'Mm', 'dgg', '2018-03-16 09:44:43'),
(37, 250, 210, '', '07:00 am - 08:00 am', 'fv bd', '2018-03-16 11:35:58');

-- --------------------------------------------------------

--
-- Table structure for table `teacherrating`
--

CREATE TABLE `teacherrating` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `subject_knowledge` varchar(255) NOT NULL,
  `teaching_method_skills` varchar(255) NOT NULL,
  `punctuality` varchar(255) NOT NULL,
  `study_material` varchar(255) NOT NULL,
  `communication` varchar(255) NOT NULL,
  `perfarmance` varchar(255) NOT NULL,
  `review` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacherrating`
--

INSERT INTO `teacherrating` (`id`, `student_id`, `teacher_id`, `rating`, `subject_knowledge`, `teaching_method_skills`, `punctuality`, `study_material`, `communication`, `perfarmance`, `review`, `created`) VALUES
(1, 1, 1, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2017-12-21 08:42:49'),
(2, 1, 18, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2017-12-22 11:08:36'),
(3, 1, 9, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2018-01-09 09:19:53'),
(4, 25, 99, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2018-01-21 07:57:38'),
(5, 5, 107, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2018-01-29 12:33:41'),
(6, 30, 136, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2018-02-02 12:54:54'),
(7, 30, 74, '1.0', '1.0', '1.0', '1.0', '1.0', '1.0', '', '', '2018-02-02 12:55:30'),
(8, 30, 75, '1.0', '1.0', '1.0', '1.0', '1.0', '1.0', '', '', '2018-02-06 09:56:21'),
(9, 42, 176, '3.9', '4.0', '4.0', '3.5', '3.0', '5.0', '', '', '2018-03-06 07:17:08'),
(10, 124, 241, '5.0', '5.0', '5.0', '5.0', '5.0', '5.0', '', '', '2018-03-16 06:04:15');

-- --------------------------------------------------------

--
-- Table structure for table `teacherrating_demo`
--

CREATE TABLE `teacherrating_demo` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `demo_type` varchar(255) NOT NULL,
  `demo_id` int(255) NOT NULL,
  `was_teacher_on_time` varchar(255) NOT NULL,
  `was_he_easy_to_understand` varchar(255) NOT NULL,
  `how_well_he_she_delivered` varchar(255) NOT NULL,
  `how_was_teachers_behaviour` varchar(255) NOT NULL,
  `rating_comment` text NOT NULL,
  `rating_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacherrating_demo`
--

INSERT INTO `teacherrating_demo` (`id`, `teacher_id`, `student_id`, `demo_type`, `demo_id`, `was_teacher_on_time`, `was_he_easy_to_understand`, `how_well_he_she_delivered`, `how_was_teachers_behaviour`, `rating_comment`, `rating_date`) VALUES
(1, '2', '22', '1', 5, '2', '2', '3', '3', '', '15/Jan/2018'),
(2, '49', '22', '0', 64, '3', '4', '5', '5', '', '16/Jan/2018'),
(3, '49', '22', '0', 64, '2', '2', '3', '3', 'fine teacher', '16/Jan/2018'),
(4, '49', '25', '0', 65, '0', '0', '0', '0', '', '17/Jan/2018'),
(5, '49', '25', '0', 65, '0', '0', '0', '0', '', '17/Jan/2018'),
(6, '49', '25', '0', 65, '3', '3', '3', '3', 'fine teacher', '18/Jan/2018'),
(7, '49', '25', '0', 65, '2', '3', '3', '3', 'fine', '18/Jan/2018'),
(8, '49', '25', '0', 65, '0', '0', '0', '0', '', '18/Jan/2018'),
(9, '49', '25', '0', 65, '0', '0', '0', '0', '', '18/Jan/2018'),
(10, '15', '6', '0', 75, '3', '2', '4', '3', '', '18/Jan/2018'),
(11, '15', '6', '0', 75, '0', '0', '0', '0', '', '18/Jan/2018'),
(12, '15', '6', '0', 75, '0', '0', '0', '0', '', '18/Jan/2018'),
(13, '15', '6', '0', 75, '3', '2', '3', '2', '', '18/Jan/2018'),
(14, '15', '6', '0', 75, '2', '2', '4', '4', '', '20/Jan/2018'),
(15, '15', '6', '0', 75, '2', '3', '2', '0', '', '20/Jan/2018'),
(16, '74', '30', '1', 64, '4', '5', '5', '5', 'hdhhhhjdd', '24/Jan/2018'),
(17, '87', '30', '0', 114, '4', '5', '5', '5', 'hdhdhjdhd', '24/Jan/2018'),
(18, '74', '4', '1', 59, '5', '5', '5', '5', '', '24/Jan/2018'),
(19, '9', '20', '0', 119, '5', '4', '4', '5', 'i like teaching way', '25/Jan/2018'),
(20, '15', '6', '0', 75, '3', '3', '4', '3', '', '27/Jan/2018'),
(21, '107', '5', '1', 66, '5', '5', '5', '5', 'awesome', '29/Jan/2018'),
(22, '9', '30', '0', 132, '5', '5', '5', '5', 'awesome views and finally awesome result', '06/Feb/2018'),
(23, '176', '25', '0', 204, '1', '2', '2', '2', '', '24/Feb/2018'),
(24, '176', '42', '0', 190, '0', '0', '0', '0', '', '24/Feb/2018'),
(25, '176', '42', '0', 202, '0', '0', '0', '0', 'baa', '26/Feb/2018'),
(26, '176', '42', '0', 183, '0', '0', '0', '0', 'vgh', '05/Mar/2018'),
(27, '176', '42', '0', 183, '0', '0', '0', '0', '', '05/Mar/2018'),
(28, '176', '42', '0', 192, '0', '0', '0', '0', '', '06/Mar/2018'),
(29, '241', '124', '1', 164, '3', '3', '3', '3', 'hsis', '15/Mar/2018'),
(30, '241', '124', '1', 164, '3', '3', '3', '3', 'hsis', '15/Mar/2018'),
(31, '241', '124', '1', 164, '0', '4', '0', '3', '', '15/Mar/2018'),
(32, '241', '124', '1', 164, '0', '3', '0', '2', 'chh', '16/Mar/2018'),
(33, '241', '124', '1', 162, '3', '3', '2', '0', 'vb', '16/Mar/2018'),
(34, '241', '124', '1', 162, '3', '3', '2', '0', 'vb', '16/Mar/2018'),
(35, '241', '124', '1', 162, '0', '0', '0', '0', '', '16/Mar/2018'),
(36, '241', '124', '1', 164, '0', '0', '0', '0', 'bshs', '16/Mar/2018'),
(37, '241', '124', '1', 162, '0', '2', '2', '0', 'cjxj', '16/Mar/2018'),
(38, '241', '124', '1', 164, '0', '0', '0', '2', 'yy', '16/Mar/2018');

-- --------------------------------------------------------

--
-- Table structure for table `teacherrating_demo_nonaca`
--

CREATE TABLE `teacherrating_demo_nonaca` (
  `id` int(11) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `demo_type` varchar(255) NOT NULL,
  `demo_id` varchar(255) NOT NULL,
  `facility` varchar(255) NOT NULL,
  `instructor` varchar(255) NOT NULL,
  `offering` varchar(255) NOT NULL,
  `value_for_money` varchar(255) NOT NULL,
  `rating_comment` varchar(255) NOT NULL,
  `rating_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacherrating_demo_nonaca`
--

INSERT INTO `teacherrating_demo_nonaca` (`id`, `student_id`, `teacher_id`, `demo_type`, `demo_id`, `facility`, `instructor`, `offering`, `value_for_money`, `rating_comment`, `rating_date`) VALUES
(1, '25', '244', 'Academy', '3', '2', '2', '3', '3', 'higg7v', '21/Feb/2018'),
(2, '25', '244', 'Academy', '3', '2', '0', '1', '2', '', '22/Feb/2018');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `alt_number` varchar(255) NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `access_token` varchar(50) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `dob` date NOT NULL,
  `password` varchar(50) NOT NULL,
  `qualification` varchar(15) DEFAULT NULL,
  `tenure` varchar(255) DEFAULT NULL,
  `batches_taught` varchar(255) NOT NULL,
  `student_taught` varchar(255) NOT NULL,
  `join_as` varchar(15) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `per_address` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` datetime DEFAULT NULL,
  `tutor_type` varchar(255) DEFAULT NULL,
  `specialization` varchar(255) NOT NULL,
  `stream` varchar(255) NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `display_institution_name` varchar(255) NOT NULL,
  `is_institute` tinyint(4) DEFAULT NULL COMMENT '0 for individual teacher, 1 for an institute',
  `is_academic` tinyint(4) DEFAULT NULL COMMENT '1 for academic teacher, 0 for non-academic',
  `profile_img` varchar(255) DEFAULT NULL,
  `about_me` longtext,
  `video_url` varchar(255) DEFAULT NULL,
  `mission` text NOT NULL,
  `vision` text NOT NULL,
  `inst_lat` varchar(255) NOT NULL,
  `inst_long` varchar(255) NOT NULL,
  `current_availblty_lat` varchar(255) NOT NULL,
  `current_availblty_long` varchar(255) NOT NULL,
  `contact_by_email` varchar(255) NOT NULL DEFAULT '0',
  `contact_by_sms` varchar(255) NOT NULL DEFAULT '0',
  `contact_by_call` varchar(255) NOT NULL DEFAULT '0',
  `call_start_time` varchar(255) NOT NULL,
  `call_end_time` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 for inactive, 1 for active',
  `is_login` int(11) NOT NULL DEFAULT '0',
  `is_online` int(1) NOT NULL DEFAULT '0' COMMENT '0:ofline, 1:online',
  `user_role` int(11) NOT NULL DEFAULT '2' COMMENT '1 for Institute Tutor 2 for Home Tutor 3 for Non-Academic',
  `language` varchar(255) NOT NULL,
  `referal_code` varchar(20) DEFAULT NULL,
  `rating` varchar(255) NOT NULL DEFAULT '0',
  `is_anonymus` enum('0','1') NOT NULL DEFAULT '0',
  `mobile_verify` int(11) NOT NULL DEFAULT '0',
  `profile_verify` int(11) NOT NULL DEFAULT '0',
  `fcm` text NOT NULL,
  `device_id` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `guardian_contact_number` varchar(255) NOT NULL,
  `relation_ship` varchar(255) NOT NULL,
  `guardian_email` varchar(255) NOT NULL,
  `academic_aim` varchar(255) NOT NULL,
  `passion_for_life` varchar(255) NOT NULL,
  `last_qualification` varchar(255) DEFAULT NULL,
  `total_experience` varchar(255) NOT NULL,
  `total_students` int(10) NOT NULL DEFAULT '0',
  `adhar_status` varchar(255) DEFAULT NULL,
  `adhar_img` varchar(255) DEFAULT NULL,
  `view` int(255) NOT NULL DEFAULT '0',
  `bank_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `account_hold_name` varchar(255) NOT NULL,
  `referral_code` varchar(20) DEFAULT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `first_name`, `last_name`, `email`, `mobile`, `alt_number`, `web_link`, `user_type`, `access_token`, `gender`, `dob`, `password`, `qualification`, `tenure`, `batches_taught`, `student_taught`, `join_as`, `longitude`, `latitude`, `address`, `per_address`, `city`, `zip`, `state`, `country`, `modified_by`, `modified_at`, `created_at`, `tutor_type`, `specialization`, `stream`, `institution_name`, `display_institution_name`, `is_institute`, `is_academic`, `profile_img`, `about_me`, `video_url`, `mission`, `vision`, `inst_lat`, `inst_long`, `current_availblty_lat`, `current_availblty_long`, `contact_by_email`, `contact_by_sms`, `contact_by_call`, `call_start_time`, `call_end_time`, `status`, `is_login`, `is_online`, `user_role`, `language`, `referal_code`, `rating`, `is_anonymus`, `mobile_verify`, `profile_verify`, `fcm`, `device_id`, `type`, `guardian_contact_number`, `relation_ship`, `guardian_email`, `academic_aim`, `passion_for_life`, `last_qualification`, `total_experience`, `total_students`, `adhar_status`, `adhar_img`, `view`, `bank_name`, `account_number`, `ifsc_code`, `account_hold_name`, `referral_code`, `added_date`) VALUES
(2, 'Rahul', NULL, 'rrahulkkhatri@gmail.com', '8700648727', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, 'a asd asd asd asd', 'Outer Ring Rd, National Market, Bhera Enclave, Paschim Vihar, Delhi, 110087, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_2_4071.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'ee7qkcvQOes:APA91bGDs29YIHwy9vaR8D2rDIai7AjX-y-sBolbsWDSqx9wG2j_kxk3qKbwgxGfnVYA1cbspFLz8M-XxQIQjqhEADPZJBEghWSOWOgzWVUM-fe4YIOvY2biHksDLhXua3Lo78UXIA9L', '', '', '', '', '', '', '', NULL, '0', 0, '1', NULL, 228, '', '', '', '', 'TASOR1GQ', ''),
(6, 'sona', NULL, '', '9997311778', '', '', '', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cVqYZeWksd8:APA91bHCLzK2wiFLmamETaVqTF8fnm-AakCVtZM1iZ_Z0RrRvd2jyDWE3nC5wvJR0_F56GFJWHD-AmgdbbNe6OAV4m1BZbTVzY5WFkcAbOuIfajDY1tn8gMA1gLni2cA1CwTWXM-M1ov', '', '', '', '', '', '', '', NULL, '0', 0, NULL, NULL, 2, '', '', '', '', 'TJJSJCML', ''),
(7, 'Monu MOTLA', NULL, '7', '9911508864', '7', '7', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_7_1517715903.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'fAGsOuTkups:APA91bGajo2iVbhj4U64NI1y2SBwWFNCfMqzgjfC7wBekPaTgNuHYlc654QvcI8m-MtD6MSfY3GAeYHMySe1Jh69QbQoeba2rW41-LEaSYbQdFryYdaoNWP1W6G44dy9VaBwWwC6plbo', '', '', '', '', '', '', '', NULL, '0', 0, NULL, NULL, 2, '', '', '', '', 'TWVFTNCD', ''),
(9, 'smrati priya', NULL, 'cs.smrati27@gmail.com', '8447208109', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, 'WZ-13, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_9_1515492828.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 2, 'sd', NULL, '0', '0', 1, 0, 'cu65XdEGDtQ:APA91bG_rVnY-qKhd9Ms--MTD6fjKTzD5Xq3ZuYIOJs11EzrgV4t5b8KmsLNEFrRutRMT6sm54N_GSqpKF8Bn8ttrfpJXXuL4MNyTf7S9U1v_vLJJooZ9i-WDN3N2MzNCmt8AYNHdDH0', '', '', '', '', '', '', '', NULL, '1', 0, '1', NULL, 387, '4cfaa0018af79f056a3c3268ec1b6993', 'f7415a30f0f01ea28a8e42098054e3fa', '57c705f8992ed2eac76aa2f408880c77', '3b49afb062a2e0f6d27e97def0b3def3', 'TJOKCT5P', ''),
(19, 'Manu Pratap', NULL, 'bemusical225@gmail.com', '8449951234', '', '', 'teacher', NULL, 'M', '1995-08-09', '', NULL, NULL, '', '', NULL, '77.8880002', '29.854262600000002', '', 'Roorkee, Uttarakhand, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'music is my life  Life is music', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'dDiMAeaJ0Eo:APA91bHmzbQIes4BqzWHae7OHoyOWYzY228h8qX-i-GARWDqFnCAL4UfXRp58vP3_wKiCZmx4eisjZlNOdqtURd36PYycRmfkizKEpjA6oZf2EPEcjFTyFOFct93fxp6RsNV3iXb9dFu', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 0, '', '', '', '', 'TOSZYUOQ', ''),
(24, 'Jyoti Behwal Reniwal', NULL, 'jyotibehwal2013@gmail.com', '8587973411', '', '', 'teacher', NULL, 'F', '1990-09-12', '', NULL, NULL, '', '', NULL, '77.0287944', '28.460882999999995', 'Adarsh Nagar, Sector 12, Gurugram, Haryana 122001, India', 'Sadar Bazar Rd, Sadar Bazar, Jacobpura, Roshan Pura, Gurugram, Haryana 122001, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'Only  education makes  a man  perfect ', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English', NULL, '0', '0', 1, 0, 'fIz6exFLQXI:APA91bG9oyBcRt12tQxnOsQ77XsRdchEvTml0gORDxCOm27TE-vQKYuLmWB5uLDvxw5E65KiZRESd67tG0KVRC949O3dTRZAz15j9A9MVNHIM0bUVx5THOWKofVyvmjSNo6Ld36igv1Q', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 48, '', '', '', '', 'TW5HGO90', ''),
(26, 'S K SINGH', NULL, 'sonu.yoman@gmail.com', '8650462625', '', '', 'teacher', NULL, 'M', '1989-10-07', '', NULL, NULL, '', '', NULL, '77.8969569', '29.8404273', '', 'Laksar Rd, Ashok Nagar, Roorkee, Uttarakhand 247667, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'Highly ambitious and passionate about my subject', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'c8dQw4TZufA:APA91bEGE8A-hVzuhf4IQSStZrBH1qWvW0I4e0YatcU_yEoYv4uD5ZyPSA_jy3PlyRcGsk5TvHDnKhyTODlYMwcpQGGmXBje9Wma4XGX4EuS-4w_WGW59IDtBKUUhMm4uJ1_SYdc_DOr', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 146, '', '', '', '', 'TBWL8RR1', ''),
(27, 'Ritesh', NULL, 'rkritesh282@gmail.com', '8757515045', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dB_iR-v5ras:APA91bGtAeqerCEFvtjM_bEKpIvmfOQikJf-IMzbQieQ4gfU_-q4LtNE934U78t5RnmacWWH3_AQ40dKcDFTsERK13kFACEl2wgfz8TrGrIEQ1lvqar24XekyVcdYJkUSTCPC580jCSL', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 2, '', '', '', '', 'TP4F8SPL', ''),
(28, 'Rajeev Kaushik', NULL, 'r.kaushik0509@gmail.com', '9927369333', '', '', 'teacher', NULL, 'M', '1976-09-03', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Mission Rd, Kankhal, Haridwar, Uttarakhand 249408, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'our motto is to improve a student rather focusing on marks', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'cFSqpbEcKLo:APA91bH79pCpELLxXTvZWJR9zEmJKAKRSQ_f6H_vR66-ITTjW5b4nxO63nKeKtOUQjeLd7b18vh68-Sr0CZcUwe2BN8_8OfliuFOAsgYKp_TDpZhw5T_5xWw3AUpPuImj-FlsMDyPJt7', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 98, '', '', '', '', 'TMVAKXAJ', ''),
(29, 'Amit Sachan', NULL, 'amitsachan1985@gmail.com', '9198730069', '', '', 'teacher', NULL, 'M', '1985-06-06', '', NULL, NULL, '', '', NULL, '77.8783572', '29.861418099999995', '', 'Bhagirath Kunj, Roorkee, Uttarakhand 247667, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_29_1514106960.jpg', 'I AM VERY PASSIONATE  ABOUT SCIENCE AND WANT TO GIVE BEST EDUCATION', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'cr-8zDKs2C0:APA91bGzIcs_tDK_73nxKOi-yJlTRS3pSxausPA04jclxfJuAyQyZs-jI0VYNd457MFvysSo9xlYwnTugjPtp79Bg4N7gelMk12DT2PhOnP36a8hrxv28S2yRocgTvegk1zlrBj56JN-', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 2, '', '', '', '', 'TSYAPDH5', ''),
(30, 'Dr Nishesh ', NULL, 'nishesh21@gmail.com', '9410155522', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Sheikhpuri-Azadnagar Rd, Gandhi Nagar, Roorkee, Uttarakhand 247667, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'cgCSDuKjVfM:APA91bFDw4F1KDSBuyulFPMF2yO-LFJL6y906lLhMYHuniiKbncAfuCMyA4ux8o4FggXQi7Qem3I1Xew8jzfDBrSv5QkONQJ0kzaqVp9pYCgIMSx_60qLpOVHVVrPNYfD1VBV3_jKC8W', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 43, '', '', '', '', 'T2R5DXRL', ''),
(31, 'Abhinav Saini', NULL, 'abhinavsaini9995@gmail.com', '9756049208', '', '', 'teacher', NULL, 'M', '1999-04-27', '', NULL, NULL, '', '', NULL, '77.8887104', '29.8679209', '', 'Dr Baldev Singh Marg, Civil Lines, Roorkee, Uttarakhand 247667, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_31_1514119859.jpg', 'abhinav saini', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'dTPq1cwSikw:APA91bGnaWipm1qyMfe5h9gw0DxjXaktoPsyvDUYMhsywO4EYwG--RmIevv7WtRLYsN2G_rtF-AsdmBnnu7X0bV_TOUCwNc7t4QsDXhE8CLo5zvkcIrpK218InKelU_A3__bxjYnFfvb', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 0, '', '', '', '', 'TPIMEMSN', ''),
(46, 'Pawan Kumar', NULL, 'pawankumarssb@gmail.com', '9568403225', '', '', 'teacher', NULL, 'M', '1983-12-08', '', NULL, NULL, '', '', NULL, '78.0908651', '29.9254999', '', 'Subhash Nagar, Jwalapur, Uttarakhand, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'I am highly ambitious for my professional I believe in practical form of education rather   \nlearning facts and figure', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'fKiR33x23zc:APA91bHM3n6qbuxssrnx8qFUEsfX2henbf9zaEXIIjVpvxVSx_lBkpx8psAWqTXMhqjfDPFyHyATIg3W73WwJwY0UBGvCYVRUd2M4J-QCuHzm5BRNIntV3r054X6UavHHx9OUSPYAbJW', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 8, '', '', '', '', 'TER8ANJK', ''),
(48, NULL, NULL, NULL, '8160190721', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'f8aGNx41aMU:APA91bH37lN9oA53Q9s-IyjkWSGrpSoMnx1EdCqnA8iz53TNt59M9TD562TjWfdTDuBLwD6J744YZP-AT8WYps062_F7VI2OcZ3BfMw9ImxpWxqB9hyb13M3p2brk-p5_wYQln_XxQNi', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TLT4ESE3', ''),
(52, 'Vishal', NULL, 'vishaldagar8394@gmail.com', '8447075305', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_52_1516889009.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 1, 0, 'f7vZSQ30ZvU:APA91bHa7cT84Xtt8jVmhk6itXA_24IZ_UchCveoMtuEyW3pRAB73igUwPUXq6Mwa4LPeY66KMpBIlzZlgLqQg-ylz6Y-agfvrPQTgUcffuzHPH_CRKX6ESVxAXfPlRqrlQodejEWKuY', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T6V98NFL', '20-01-2018 18:28:25'),
(54, 'Ruchi Kapil', NULL, 'kapilruchi.kapil43@gmail.com', '8700303484', '', '', 'teacher', NULL, 'F', '1992-05-21', '', NULL, NULL, '', '', NULL, '77.8697782', '29.86652119999999', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', 'Subhash Nagar, Rajendra Nagar, Roorkee, Uttarakhand 247667, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'passionate', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fYpayDjpVIw:APA91bHMTrY90AsP-iE9nJhJZlOqVTZBawtdtHOldnyTjafDlFF836PoygEgMx2hrv1asr0ind0xELcm8b_Lyjame0XaG8KK1vD9zPqSyrIqk-j2bpSe5FlFadTf4pRLdKplrZsZ9ubW', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 12, '', '', '', '', 'TL6XLOK0', ''),
(61, 'Ashish chikkara', NULL, 'ashish.chikkara9@gmail.com', '9999575155', '', '', 'teacher', NULL, 'M', '1980-01-01', '', NULL, NULL, '', '', NULL, '77.0764955', '28.6219627', '', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'Education is the most influencial weapon', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 1, 0, 'eiruaeC2FeU:APA91bHTdL0upvMPzLSx5pWlGtoau0w3DOVwGgQi3cE6YmJ_BcFDqcl6oGzAUtHA82-MCvkbNQFwpviz9huIOjX5ijd62h7ZVWIqElcG6YS2CfFPllhRHbLUYkykwFCMrH-qPpZqVCGl', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 23, '', '', '', '', 'TLMNZEWA', ''),
(62, 'gaurav singh', NULL, 'gauravsingh191192@gmail.com', '8789998960', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0765296', '28.622012', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_62_1514538400.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eVsRqMqR87w:APA91bFD5vFbhhdJssbx8OqIQz-_tEnUDnolSfmntYcxCfVUCcbSIOCem2_t7V48kTb_0fRw7Ww4_2Mv_Mbv9chI9NCLhqUce0V1OF5RJjVX1aXLg0telfvYMkcl424eWTNFFbGmUpIr', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 0, '', '', '', '', 'T1FVN9ZB', ''),
(63, NULL, NULL, NULL, '7982931713', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cNwPWSMFEa0:APA91bGBXELh1703QnJ6zigAXOlX-PtT9pONKk0ekhXPKNq4dZ9VrUwD03gxESdX4DjfEb4PCh5iJYRutJo3ylRNFXmcqWjB2lHFrVFxeKjGXNxRY_2YwVz78YG6MY163nrocdRRbLEX', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TBEFHZOP', ''),
(64, NULL, NULL, NULL, '8800232593', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eSZ4-OGmOyA:APA91bECpXtRJHDc-6WlAEnsVmAEExr2VxMBDNLhKQjIsusEpGnPRtTR4hhOEo_1m5UFkwALdfb0jkopAqQvOvrSxvaeDqTmuxdOfsRMYhf5bHKk1oyPb2BBU6_a_-2V7Gv2F10LTUOi', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TX4BIB83', ''),
(65, 'Ujjwal', NULL, 'ujjwaltiwari2@gmail.com', '8982096303', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_65_1514698645.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'f08g63Kt4TU:APA91bE2mPZTz2LT2rCEUEIevzRU25AkMXrEcHeqdNfCOTVwm2eNYAKjYvWQRzquFO3yFaKw7jC-6S01lQiFETj7a1TnIKf7QEYGEmg9h04Qzi0fBUiB5CljNnObshkYNwUwFYX2lvAM', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TZT3LFRL', ''),
(66, 'Bishwajeet', NULL, 'biswasingh123@gmail.com', '9873793968', '', '', 'teacher', NULL, 'M', '1994-06-20', '', NULL, NULL, '', '', NULL, '77.1035129', '28.4340617', 'B-54, Golf Course Road, Suncity, Sector 54, Haiderpur, Haryana 122011, India', 'B-54, Golf Course Road, Suncity, Sector 54, Haiderpur, Haryana 122011, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_66_1514882459.jpg', 'Interesting Teaching Skills', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, '', NULL, '0', '0', 0, 0, 'eL_ZpllSfsU:APA91bHEDyc2v2OUrZzWCybdxnKJfgdfBfnCUnoIKIIVw2bRY4GLHwWvKNF7shN4djk_zuX6xFy7GZTB-MP--_hu2cwRmtxmV7z6Gs7K8mRIlX3_o3Oesaclp4gEdcoMy6c91m8dNCj2', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 25, '', '', '', '', 'TUXVWDRJ', ''),
(67, NULL, NULL, NULL, '9917865368', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'esig_7Oyz_0:APA91bEiSz2SqCthKlODmsAz7Y63QIiUal0zJwhXLTvg9qexAaDejcDF0Vz4ITOiLIL6uWeqvY2p5x80womNeHVCgKWIbqUs91524M-tC3mzdKx2dVdAIRkVgfFBZTg3btFPRGKemchb', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TMRVXVAF', ''),
(70, 'Akansha Chaddha', NULL, 'akanshachaddha07@gmail.com', '8726099310', '', '', 'teacher', NULL, 'F', '1995-10-07', '', NULL, NULL, '', '', NULL, '80.9042966', '26.8459816', '', '21, Aishbagh Rd, Moti Jheel Colony, Aishbagh, Lucknow, Uttar Pradesh 226004, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_70_1518337146.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 2, 'English', NULL, '0', '0', 1, 0, 'eQwOF7PqNfA:APA91bGQmiJv8Al72AE-dBB3EuHAhRbmjzQ3u1wQVEdjuXhyRF3TkSId9qSXOnTDLF0i3aR3-t1kdZzvDWEluLN6YGqXMH6l5zGAm_z45NNMw3zw2U05R6tEm3bPGUCZwn9AIHm6cez9', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 0, '', '', '', '', 'T5PU2T6K', ''),
(72, 'Vaibhav Balasaheb Parhad', NULL, 'vaibhavparhad5872@gmail.com', '9511675808', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'f7uSpNW59iM:APA91bEjy-1RQu_yi0eRCLxHzm0BaQUtt8N7AR8q40CVg3oaN7-r-C1pjrvt0qVvjsBXpBodudjuT9AahaMn9w7sRYYMQiIwpdtQoDdoCqARtdPvMoNiqvhVW-smX2wVsASRFrN-3ew1', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TEEOEISZ', ''),
(74, 'Amit K', NULL, 'daraptoor@gmail.com', '9560610340', '', '', 'teacher', NULL, 'M', '1989-08-28', '', NULL, NULL, '', '', NULL, NULL, NULL, '', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 11:10:39', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_74_1516711175.jpg', 'Hello world! ', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English, Bengali, Kannada, Kashmiri, Konkani, Malayalam, Oriya', NULL, '0', '0', 1, 0, 'fZgmLQ8H5jc:APA91bEtagTuKfquy-4zQkQPV1UYko4yWhoJIqKlMzT5R79Qa5nib6I_mmVyqQEbQLxL2jVVuogm7d-YgCK97z6GtUUSo01GEiiazSKIy-BeFwTCUgPUuoKE7nOeGJrWxOhA-yogXZcT', '', '', '', '', '', '', '', NULL, '7 years', 0, '1', NULL, 87, '61d2df0bca1deb24fbdd503f9f9ffaae', 'b8b7950fb7b09bf5b1d646f242686237', '39f6bed0e0e7babd143eeda863e6f0fd', 'e667a5487871ccbea11f16cf8ab1735e', 'TXTUSRQO', '05-01-2018 12:57:31'),
(75, 'Saurabh Singh', NULL, 'saurabh.singh@taktii.com', '9560613271', '', '', 'teacher', NULL, 'M', '1990-01-22', '', NULL, NULL, '', '', NULL, NULL, NULL, '', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_75_1522652469.jpg', 'About.me is a personal web hosting service co-founded by Ryan Freitas, Tony Conrad and Tim Young in October 2009. The site offers registered users a simple platform from which to', 'https://youtu.be/RviBCy-3cXY', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Kashmiri', NULL, '0', '0', 1, 0, 'e8pPgu6gwvg:APA91bFaKextTVMXNO6Oyd-H0C3uvqXp7h6D_FgAuSy7yMBquFJ6-DFFtu9KxXqLOp5mAJDUoTb-f3JTcNzjxbWVuinOQ1txKocK75AA3ujkQFjml9ErnbOd0wwUZ2-Op4xbOTdu_hpE', '', '', '', '', '', '', '', NULL, '< 1 year', 0, '1', NULL, 73, '546e6c0df90140af833d40fce73d72cc', '181265ed49993cc6bd58809c7205b111', 'ce25b6afe07adc43a9070feb5cb6ee54', '8bc2c492104fac77027337dce06e874a', 'T5H8RZ3P', '05-01-2018 14:28:19'),
(76, 'Satvik Singhal', NULL, 'ishasatvik123@gmail.com', '8445680462', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 1, 0, 'c-ZFYlWa9Qc:APA91bEbghAdS3gUdOg6TcpJ0nQrYrIeb2-tMSvsykhMLkFZZDgr6GCjkP6JRGuaDmOyYn2X1SLX5MMjyY9K8bMITf5_il69SKUPXiJ2P_uC5eVukkv97A3lt0GDOf6M9A_xPbu3Srzd', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T4D492ZB', '05-01-2018 17:06:20'),
(77, NULL, NULL, NULL, '8340504238', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'd5TnJNNZyBc:APA91bEAY0UlhoMaW2suXpsgwewc2gZUCmFYdFEJzrI36xksZy11RlreXtUq5YCTvOcx98NrZz1DoDx1DlzogRmvobO7-82e5inK00HDrOvxlRV8lyjC-17AKVE85Xtf2BtONEWWdlIg', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TWFWAUUP', ''),
(78, NULL, NULL, NULL, '9756943502', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'd6Md8vwbE1s:APA91bEawdkXIpr73kBIAqMvxIh4CDd6V3sjWcrjzusnU1p_jEVXgQq6IwAT1cLLnP8mD0QFmuInNzseggtktbjsDvD8COGDCGGwwXk8wjAeVLR7NALNm2oo8vNZJZAM2xp3ZlDOqVlI', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TCNYAGTS', ''),
(80, 'Satyender Pal', NULL, 'satyenderpal1990@gmail.com', '9015187618', '', '', 'teacher', NULL, 'M', '1980-01-01', '', NULL, NULL, '', '', NULL, '77.0895421', '28.718565500000004', '', 'Budh Vihar Phase II, Budh Vihar, Delhi, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_80_1515566284.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'caMRaRt9D3o:APA91bFx4Qvnm2jaWZMlss3bQWa5PSCCrne-DWHBV19xLMQqbZBWXLPz0Efq4zM80BnmP3uoQFs5tdnhl4zYMd3S0BnuezVV20m8eoOZaaQ9EGjTaGPr3GN7vr4um39VmBigCy4ZwcqL', '', '', '', '', '', '', '', NULL, '6 years', 0, '1', NULL, 101, '', '', '', '', 'T8KGDSPF', '10-01-2018 11:51:47'),
(82, 'Gaurav Kumar Singh', NULL, 'gaurav@taktii.com', '9971194897', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_82_1517907642.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 2, '', NULL, '0', '0', 1, 0, 'dQgLlU0ws-I:APA91bF24e5Qyf5RZuOTf19r0Fy-faUq_Bc2IKgxKVSBMt21W9YEP2e_6pTjdNX2vB_78c9C7SqJZh_A0pV2Muc88hJxJ0w9uigAz7hHYYH4uZTUKXanOQ2yMgTVELNq5nvZgV69sgVW', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 2, '', '', '', '', 'T1S5559E', '10-01-2018 16:50:38'),
(84, NULL, NULL, NULL, '7880481764', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cxgOHrEs46w:APA91bH4zicx7i_2j00fwOH8MLN7-fR6-jTmTaK6i63vYMnlRAVSvoPn5559Ltk2qfTY10zj3nJry0MCKpLQjzJKAwI5jYWqIZ3B3tqgNNi0SMMQdvXa-lVuxHZCb0YYY8bF8SHn6qrp', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T8JQEOWO', ''),
(85, NULL, NULL, NULL, '9958098979', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'etsvk-6_mXk:APA91bHWgSDauYJitnFAKHz8lReUR2KyWmFUsvoSGkgsOoolLoOara-IM02ZmW2x6ZSl4iRt6utxvQh7r-a8lvdpjf9AiFt6AKAA9-cd6HmU81rxRpBUuQdIvWcabG1jZ2rNuNHfL9Y4', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TIRDVECV', ''),
(86, 'Md Farman Ansari', NULL, 'mdfarmanerki@gmail.com', '9990171126', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cT2uBfiUmfE:APA91bHlZ6kiyoKwZwW20_CwQ3wZmKWpcNrA5VKi6pU7gBWCJEmxJVODvacGycqLMMC8PCENingsiPiVqgM4XE7s3xgCYUwHrLthn-hwxoQIeMmNWm9Oe7AAorYB7nwsiylzlKoUPaUE', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TV6XERER', '13-01-2018 21:45:03'),
(87, 'Amit', NULL, 'raptoor@ymail.com', '8076423788', '', '', 'teacher', NULL, 'M', '1989-08-28', '', NULL, NULL, '', '', NULL, '77.08783849999999', '28.621899', 'Janakpuri, New Delhi, Delhi, India', 'IG Indoor Stadium, ITO, Vikram Nagar, New Delhi, Delhi 110002, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_87_645.jpg', 'Hello i am home tutor', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English, Hindi ', NULL, '0', '0', 1, 0, 'fUg7hYd-bpM:APA91bG-aVKn7DIPotCBraqbhOu038ZA4vQBITUUfE7HgK3_8F8psSXTzu6UcTqXVKrYjUlr2sS49pGsHnW8Y0yMM7krjRzM50sOQYD3jo6ZAnW1jCCyjwrie4p0XPWZpMthJilbopvk', '', '', '', '', '', '', '', NULL, '4 years', 0, '1', NULL, 87, '', '', '', '', 'TJDXK6CQ', '16-01-2018 14:17:30'),
(88, 'Arun Kumar', NULL, 'arunrao74@gmail.com', '9540828949', '', '', 'teacher', NULL, 'M', '1991-10-23', '', NULL, NULL, '', '', NULL, '73.02430939999999', '26.238946899999995', '', 'Jodhpur, Rajasthan, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'dF_jGUeNLRE:APA91bEoDfVedFrjo7-mmIPHk5nJocRiR8HdjrnlIERTiOOFS_6OfhWbs7FGFV6k61iS7J0vCYdCoLSnMTFOgqdD3TbSY1oZMVQR1lFiHFCPDLs9Fqu1F3iCwEkVwoi5ZHIPvIww_ZeM', '', '', '', '', '', '', '', NULL, '', 0, '1', NULL, 0, '', '', '', '', 'TBMEJV5X', '17-01-2018 08:48:04'),
(89, 'Sourabh Thakur', NULL, 'et.sourabh@gmail.com', '9810611021', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'cmx_HOCaooE:APA91bFbeGL59uxzgLu1yXaqv0xglnvq0u_bjNkGbloZVa5Ed-Mwo3TbMOTAX8H5as45M-CkTnbD9Cb97wHxKQR9-y66mOLpQqSApKztzFe6Fk-9Alv4bS9EZ9XL2T__sA1KyZRrgUAv', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TT1MCT07', '18-01-2018 15:36:12'),
(91, NULL, NULL, NULL, '7703833026', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dvvSSMwtG0M:APA91bHPcCA5xp5uboc4UwS4WnzgjzOZ7pe7ToQDHfceK-a51B-cznbyToabk36ke88MMn2BCISlUYY9tWN1h7dfRyPQKSK2b1dIIssu-i-kx41biiZuMWNymS_n6gzU-UDD2LTL0v7U', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T7DCDKAH', ''),
(92, 'Harish mehra', NULL, 'harrybornlegend@gmail.com', '9555518585', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_92_1516351035.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 1, 0, '', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TBP9UTGP', '19-01-2018 14:03:44'),
(93, 'Rahul', NULL, 'rahul92chhikra@gmail.com', '9599900901', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'ehjbd26a_QQ:APA91bFsBQSIUesgD92VO1pcIxjDe47nq8V_pBSUJQ78MtdH6lKcczg8I34MDrQx3x8We1sOCGnfG39V_uNkNiCUvjdHMDj_CZdi62ZM0XYuUmY2nw9-luibRV96KAR9CoGLX68ymtjt', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TZJFL5U4', '19-01-2018 18:52:05'),
(94, 'Sonu Thapa', NULL, 'thapasonu277@gmail.com', '8384855998', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'cabmEdpkcOc:APA91bGFIhDWvWxFccUTbEK2z3xcM5GQZ0tZjI7jC44a2H92Cc-uGxHcMgSzB8Z7szwj4qiC74uXoVi66dTTGllUD8E1_cnM4V_1uhMESkqTV4047narAuFrFCc1isvnLgpZ1XMtgISo', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TAZ2U1OG', '19-01-2018 22:13:57'),
(99, 'Rohit Khatri', NULL, 'surya87khatri97@gmail.com', '8375847783', '', '', 'teacher', NULL, 'M', '1980-01-01', '', NULL, NULL, '', '', NULL, NULL, NULL, '', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_99_1516523769.jpg', 'hdhdjdjsbsnnsnmsns', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English', NULL, '0', '0', 1, 0, 'ewEp3JvDia8:APA91bGLTdAAA0mQv3X9OJEo5t0GKn1wTvESHfbi07ggEZTbtamPH0ni712cZSOuekWI_Y4iTpJ-RZaVFbgmZeZVZnrH8rif5qyT0r210nyXNm3g6rpNlFSmncfvKrb-Wmbh3OsOlzi7', '', '', '', '', '', '', '', NULL, '10 years', 0, '1', NULL, 55, '', '', '', '', 'TUPO22RG', '20-01-2018 15:57:18'),
(100, 'Rahul Chhikara', NULL, 'rahulchhikara13@gmail.com', '9990600078', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_100_2182.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fH73yC1VtGs:APA91bHz8adZB5cevAwUTKVUlwVLeMoVw8Ouz70dRfK1Fv61pdYY0ELNQ_7vnuAXMmEsOyy5QzyEu5bArgxqlWr7xRxfvVqwXi6GKy4puGQpU9fsXVlFii-ehrRROelXESCeGLmXPvVO', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TM2XXR72', '20-01-2018 16:51:47'),
(101, 'Dev', NULL, 'rahulchhikara14@gmail.com', '7011087771', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.08783849999999', '28.621899', 'Janakpuri, New Delhi, Delhi, India', 'Janakpuri, New Delhi, Delhi, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, 'Nepali', NULL, '0', '0', 0, 0, 'fH73yC1VtGs:APA91bHz8adZB5cevAwUTKVUlwVLeMoVw8Ouz70dRfK1Fv61pdYY0ELNQ_7vnuAXMmEsOyy5QzyEu5bArgxqlWr7xRxfvVqwXi6GKy4puGQpU9fsXVlFii-ehrRROelXESCeGLmXPvVO', '', '', '', '', '', '', '', NULL, '9 years', 0, '1', NULL, 0, '', '', '', '', 'TLORU76F', '20-01-2018 19:08:56'),
(102, 'Amit', NULL, 'akdcantt@gmail.com', '9953130689', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'egLwvzheWks:APA91bEBVhU8emc3UpYvgLiYjxW2s3hsmeWi_E_o4EJjZ17qPulnt4TyDArnnHpj04iBhD8maAcbgTKhoHfoKm1aK5EqUfTQin0V7TjNsbdXTbrTHBS6CdLmkmfVV8lYfAOX8yKS7b5s', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TC0KNZMH', '20-01-2018 21:32:27'),
(103, 'Rishi Dua', NULL, 'rishidua1982@gmail.com', '9212521116', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'cVG7Gsf7tKs:APA91bFekfU0LwUxzRmUt0JM5tniZ5CWrFRS_YT3iNetfMt__b77ZDMJKbWkUJFNkXOLn2cmNnwd0Tr6N9iUXWXtPkVuNVrKkkspA1k3n74Jym2HSsQ2Tr-Eh7wuJ9mTd9Vd4b4434Mg', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 1, '', '', '', '', 'T0BYVQNX', '21-01-2018 14:29:10'),
(104, 'ashish', NULL, 'kouchan.ashish@gmail.com', '7291889848', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dB6qY4YBlUg:APA91bG2gU9lPOGhY2SFKXXnH4MqyrZAcAoMqGzULnRB81m6v3ZHvf9_x7ixThqlrl2XMcAChHaxfbIzJk5-_k4oRc91Y_jkD-6ypb-eRwxLhVNDJe6lLgQTIPp_zGxPXS2XzvRvG9f9', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TSEEPGBM', '21-01-2018 19:46:28'),
(105, 'Bhagwan Dass Gupta', NULL, 'mr.bdgupta@gmail.com', '9891542258', '', '', 'teacher', NULL, 'M', '1990-06-18', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'RZ-6, Manas Kunj Rd, Santosh Park, Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_105_7299.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'dN8dk0YP8Qs:APA91bH-OHRHtit5ohUiV7KpnynyP4IFLOJdvxnxpi8DUjTVvFJuxTQ3fIFwWHW4oL3DiceTVS1DIA-gVC8zmFx8zn0CPnicITNA9lFGTmq1rVGVDxp_7FUQ2_--Gw7GvsgGrrfbUivI', '', '', '', '', '', '', '', NULL, '6 years', 0, '1', NULL, 71, '77a0ce9aecb148be6d4c1f500925734b', '104ae80ede4d7216f0cdc4b902010416', 'f2bbea912eac24c5bb9d1ad5fd97ec19', 'feb51b348dac874bd7151fdeaeb1da087da718f9a998a8c9e05ad09e49e321e8', 'T8IOTWFN', '22-01-2018 00:39:33'),
(109, 'Rakesh Kumar', NULL, 'info@nextgeducation.com', '7838370333', '', '', '', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, '', '', '', '', '', '', '', '', NULL, '3 Year', 0, NULL, NULL, 0, '', '', '', '', 'TEM3RMOE', ''),
(113, 'Vivek Arora', NULL, 'vivek9932@gmail.com', '9899382196', '', '', 'teacher', NULL, 'M', '1994-08-12', '', NULL, NULL, '', '', NULL, '77.1098176', '28.6128613', '', '1671, Jail Rd, Block WZ, Lajwanti Garden, Janakpuri, New Delhi, Delhi 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_113_1518590957.jpg', '', 'https://www.youtube.com/channel/UCGssJMjZ3JmZwUTSU9AeqXQ', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'eUg-u7GcU98:APA91bEPLFmXveQs6HG2LP94E0hVWsjXBnuLYLUJks4evM-DhL30z3qz8Td7rLOGgG7x0-cHQRKnWZN54iRu-XcjUrY2uiv3ry2o4HPMyWIx4T4iBuP1hHtY19OZC-EfUuz7HU5kQy43', '', '', '', '', '', '', '', NULL, '6 years', 0, '1', NULL, 0, '', '', '', '', 'TLAWMCUI', '13-02-2018 15:58:55'),
(114, 'Sandeep Gupta', NULL, 'realconceptinstitute@gmail.com', '9899393734', '', '', '', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, '', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, '', '', '', '', '', '', '', '', NULL, '11 Years', 0, NULL, NULL, 0, '', '', '', '', 'TCHG4WUU', ''),
(118, 'Sidhant Sehrawat', NULL, 'sunnysehrawat438@gmail.com', '9818701739', '', '', 'teacher', NULL, 'M', '1998-02-14', '', NULL, NULL, '', '', NULL, '77.0764598', '28.6221107', '', 'A-2, Janakpuri, Asalatpur Village, Janakpuri, New Delhi, Delhi 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English, Hindi ', NULL, '0', '0', 0, 0, 'fm5kRVz0QIg:APA91bEDkeu-LzplHi6jh9zw8roteqEc5y_ByzHG4bLqkRPel-7AZKwD4_jrzpsI2mzuCszLm8Xxiy5-QqkUgVAwOowZ_lIjPfV9SxoiLYH6SWM3aPY53-cH91kOnVCeqQN9WtaK30pO', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 1, '', '', '', '', 'TCVGKEFO', '25-01-2018 14:15:27'),
(123, 'Gaurav', NULL, 'gauravchhikara1790@gmail.com', '8920822668', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fNYmPR5Y56U:APA91bGg84jvsPxzCU1_Kyl3A9zOSk9gREtl1rumf6z_WC63O2_-jbBRe_tpkDljFHjDXrdoCoDZ5Iw5QqURpg2Vs0Ors_429c9hAlwi-hMYMtzfaLaKJy2TclqzRwHHQCH2F8ThIxZE', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TLVCPHFG', '25-01-2018 19:26:41'),
(124, NULL, NULL, NULL, '7065199180', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fWYpxT6Csms:APA91bHJFJxL2WaY9HIVwt5udkZjC49rA6kJA4cWwg2LSvT_VJx5paf1Zl1UQQNKSejW430xSfEFiLrMgHg4mxs9IN4YyBxfLhyIVoT4sa36Tk6BtGOnLZvyNd04kjfRG1XWyjLCx2KN', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TD3VZERM', ''),
(125, 'Aman Gupta', NULL, 'manutheemo007@gmail.com', '9560102353', '', '', 'teacher', NULL, 'M', '1983-12-01', '', NULL, NULL, '', '', NULL, '77.0613266', '28.621271800000002', '', 'Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, 'English', NULL, '0', '0', 1, 0, 'dqseaW5lLJ4:APA91bHSUc79pD_aNYWXzeNbaKSFg9MpffCsjjHqBHbNiEQ62xf6lmwShjShczQ8NFLejv2lxccrYWd-yJFDtlyAu1A1gs44otKgofvtAby_qW75SU8SnEEaamW_S-5IKRUsldH4VtOp', '', '', '', '', '', '', '', NULL, '1 year', 0, '1', NULL, 0, '', '', '', '', 'TQMESUUX', '07-02-2018 13:03:07'),
(127, 'Udayan Kumar Jha', NULL, 'ukj26.uj@gmail.com', '9654379393', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'eequOwHpSPw:APA91bGsSeEYcmyXQQhjax7vYXgagl4CAN5r3bLHtwqYEJmkc3emWgvm_pc6sj6yWb8-qKBtO-iLtDNnSTbCwcs-gs4ZdrBDjQtB5Mv35dJy5-6b-PN6rg3ScIJsKP2HUuXZrl_9DHug', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TQORDKIY', '27-01-2018 13:17:38'),
(128, NULL, NULL, NULL, '7838998998', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'd_Wj39pVtxQ:APA91bEzm1LF09QEqbCpQnqgxq0q-IvxJcsm9O0F0K14BLea-O16tZffZ9eRhSOy4kXyai9FPTMqD_DiN2r1XF7o_C4IjqRgRa1eJPpRBLKy-LdbP2hx_dNhsLTuLeNd-uaZqokEyhb_', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TYOMKJYK', ''),
(129, 'sefsdfsdfsedfsedfewfewfewfew', NULL, 'wqwqwq@gmail.com', '', '', '', '', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, '', '', '', '', '', '', '', '', NULL, '5', 0, NULL, NULL, 0, '', '', '', '', 'TBE1EIWE', ''),
(135, 'Sudhin Biswas', NULL, 'sudhin.biswas@gmail.com', '9810453928', '', '', 'teacher', NULL, 'M', '1975-12-17', '', NULL, NULL, '', '', NULL, '77.0460481', '28.592140099999998', '', 'Dwarka, New Delhi, Delhi, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_135_2075.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English, Hindi , Bengali', NULL, '0', '0', 1, 0, 'd_43TRVpeu4:APA91bF1wYbwSs-t6CYq3J4aUQC4SuOnsm6r64cHu1rzDZacC1_FlZcoyP533nM-6dx_fO0ixXB1ZEGWVU7bbpm8RvXKLmmqwQwtOkeVuKbLgCaGkcaBQw3qP2PQjfg4qVBGRIF_TTZk', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 0, '', '', '', '', 'TXOANAFG', '30-01-2018 13:43:25'),
(138, 'RACHNA JAIN', NULL, 'rachnasinghal@gmail.com', '9871630540', '', '', 'teacher', NULL, 'F', '1985-08-22', '', NULL, NULL, '', '', NULL, '77.0573196', '28.6200328', '', '26C,Prem Nagar, 23, Prem Nagar, Block E, Param Puri, Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi ', NULL, '0', '0', 1, 0, 'fyIy74EkfpI:APA91bGNjLeq6dKO7LmwUtO5Ve50URSewFBF1BJKl_FQ44iGDsVlkgs1k2DE7qfNNpT_Edg5kSaj-xoMZJu0thQOm5M938eW6sXXTRoTdouq-CL8afXP_FJo0ld3Bt9VAnScEN6O8GzT', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 6, '', '', '', '', 'T59OWYG9', '13-02-2018 15:12:52'),
(139, NULL, NULL, NULL, '9560609501', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dS7ZP_lbJv8:APA91bF38nDUfy047m53TUgtMYIHtZ7VyFDoLPoMykQgau30mvf9K-l9913yyvUVzQbQugd8vWE29Apil7YdJjBnJCZveyQVTc9rUsz8kyMHW_LR5TnEl1DVxQ0400flpv5wcg1OFFsP', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TIZ8RNVB', ''),
(140, NULL, NULL, NULL, '9711242846', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fpXlBi7ZeFo:APA91bExKomz81XZids9vpDUaHH9qkj53iwCbkunY4BUNleHivGaQDbhVrxdDLAinshLVQoPKU5DoJI61fHRZGsxr1eQtvSyam38Vt2Bz504N1lPzM-Je2M7NbV3EBWcGpUlFlwuwjgD', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'THTVI8WX', ''),
(143, NULL, NULL, NULL, '7206493529', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fzY0F2-8kek:APA91bFz-YPuAzG5_4BJ2mdvRKPthVC_Ag4ri1ealvKLEv2jqzo9vXri-NYPKgHIfIWDU5krHyeQT41jNMziXWJIXO77T19LamLlD24QNsC9TBm1i_2ppsdTru3hbfWnyTAJ9TlMvkFT', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TQTLNHMB', ''),
(144, 'Amarjeet Singh', NULL, 'amarjeetthakran@yahoo.in', '9868012537', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.069307', '28.713141', '', '121, Pocket 9, Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_144_1518178242.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'fPOBrVzPK1I:APA91bGEY-MX4gWWZp6jLosh7db-7UHbxtbKJEE833kES1L9JTC9FGFETMbpZBquVu6POTgbs1VOPfvr6FDnpEZDj0e7ENpn3Byz5A9VwiIlECtrY4XrWV2EOrLRHdZPFLW6oQB-onpk', '', '', '', '', '', '', '', NULL, '9 years', 0, '1', NULL, 27, '', '', '', '', 'TIBSNLGT', '31-01-2018 17:56:50'),
(145, 'John', NULL, 'testing@testing.com', '9871780318', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'f3rgbczdfHY:APA91bH5i19GJjRoYe9BVngwwOAoFZZiTQuOk_538g-TOH4iN7jlz3gVMMHM2N8Q-ei074jynGn__6y9o-8hwXRCMKpLsCK-SZt88ZQ0BXpVfVmdX67zcYlk6rCxqTLIzdgGBhOOC_yy', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TJX31WBS', '31-01-2018 18:06:16'),
(147, 'Cricket Academy', NULL, 'satyender@taktii.com', '8368792868', '', '', 'Non-Academic', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0881034', '28.716994299999996', 'Budh Vihar, Delhi, India', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, 0, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 3, '', NULL, '0', '0', 0, 0, 'fcTEE3NrGdI:APA91bF1Pmjfb_2pELxzOWSkPto-uDXgn5I6MOA7xNslQUBscGovbDF_qwCAOJFdQUJkWgeQnh1k5LHIhKGICesoN4qYRA0aEiuVhtvnlrR08Ta8Y1AnYihOyjjSBEoxjRYUHA2KRciP', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TT64A00T', ''),
(152, 'Lekhram', NULL, '1991lekhram@gmail.com', '8506002791', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0734403', '28.7063397', '', 'N 48, SP Rd, Sector 20 Extension, Block P 1, Block D5, Krishan Vihar, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_152_1518433383.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'cM3Cd16qVfs:APA91bG3f8qsEpr1Rq-Gzz27AHvEMBI16gEuPAiWpejVTGC3EfYgfI7Y2Gq6e5vZXExh22loXlUgxCJhNP3SsyecOAsFJ3LHUYUzHPl9PvKXqyWJnBXoeOq1RN4sJ7sbT9Lo0xxwN_WB', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 3, '', '', '', '', 'T8WQZQCX', '01-02-2018 18:47:47'),
(153, 'Vikas Teacher', NULL, 'v@gmail.com', '9958054435', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_153_9564.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'fHMQoe4sOvg:APA91bG2edLdLlTAusX7YJJ0wi1lxPlsAMP4RFaBaFb3vE-mMgW9qzNgVJtFBkQkxXFKIb-8mwh4YETDI5vHHUxqzLltW117Tsb5xcENWP0Z78I6u-w75UQXGxI_13MB8cdZ0FQUglM8', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TXZ8PBAD', '02-02-2018 11:22:10'),
(154, 'HimanshuRelhan', NULL, 'himanshurelhan1004@gmail.com', '8950215049', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0692418', '28.7159005', '', '399, Pocket 9, Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_154_1518423129.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'dhHEp4SI8Io:APA91bHrs2keilm-9XZpwD5KuBIYUrxUN1_tKdHyKywh2NSutXQq5QfhF0yvkJ66BBK3QrcRT_OFctesTZiFZRLT6XThbs2OCHTAVjCJfY8i8ZtbYkFxwN713ZZlzMrjNAlwe1_XBfw1', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 23, '', '', '', '', 'TWR6FOAH', '02-02-2018 13:26:32'),
(155, 'Ayush Study Point', NULL, 'vkp1979@gmail.com', '8285980400', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.06924', '28.712158', '', '144, Pocket- 6, Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_155_1518082936.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'c4jGI2_b-5Q:APA91bFu3Z6C0o4YluIhoSA68WqYI5qPP9On37xhYHutHaYldIcfEIUVH8VRYc7mZTV2GwEsbuAOAXxh8ICUTN3ZnQBn0Pp4zShTNzbBNenm2_yog_9xVjBbBWdVraiTx4dBtXIQ0KTS', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 44, '', '', '', '', 'TUMAOTEP', '02-02-2018 14:22:16'),
(156, 'ASHOK KUMAR', NULL, 'alpha.life.skills@gmail.com', '7011294243', '', '', 'teacher', NULL, 'M', '1969-01-01', '', NULL, NULL, '', '', NULL, '77.0699357', '28.7125512', '', '53 A, Pocket 12, Sector 20, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_156_1517566592.jpg', 'An English teacher. \nRetired Inspector, left police job to pursue passion of teaching.\nAlso a Delhi University certified carrier-counsellor. ', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'English, Hindi ', NULL, '0', '0', 0, 0, 'e_7Hkzf58js:APA91bHoLd75tqGIulbiyT7yTb4W4GAwVuKLD31HSneBgq6oPZxOlDp2ouJItehCXdeDBY6hwfZURLCtc477mN9jecPi3WljnI5pq4pYaS3DtGWk7CNqigBvFtvghHSu0ncUhvqgQtZO', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 0, '', '', '', '', 'TTEICBJC', '02-02-2018 15:44:20'),
(157, 'Neha Kumari', NULL, 'pizaneha33@gmail.com', '9582804402', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fqYnAKRDVdk:APA91bFjmtbJ3U0iGTYkl47dUwmBtnPHDZjqizfnWZjrGDEO9hlt_FvrF3DdEQE01120OjNOG7JFcunyBdtlXwRNfdfnGxHiYIfHCLsIYafLdu4eRwkL1E1I7DresStaoBCmssyEQXlb', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TSV9ZUIP', '02-02-2018 15:44:53');
INSERT INTO `teachers` (`id`, `first_name`, `last_name`, `email`, `mobile`, `alt_number`, `web_link`, `user_type`, `access_token`, `gender`, `dob`, `password`, `qualification`, `tenure`, `batches_taught`, `student_taught`, `join_as`, `longitude`, `latitude`, `address`, `per_address`, `city`, `zip`, `state`, `country`, `modified_by`, `modified_at`, `created_at`, `tutor_type`, `specialization`, `stream`, `institution_name`, `display_institution_name`, `is_institute`, `is_academic`, `profile_img`, `about_me`, `video_url`, `mission`, `vision`, `inst_lat`, `inst_long`, `current_availblty_lat`, `current_availblty_long`, `contact_by_email`, `contact_by_sms`, `contact_by_call`, `call_start_time`, `call_end_time`, `status`, `is_login`, `is_online`, `user_role`, `language`, `referal_code`, `rating`, `is_anonymus`, `mobile_verify`, `profile_verify`, `fcm`, `device_id`, `type`, `guardian_contact_number`, `relation_ship`, `guardian_email`, `academic_aim`, `passion_for_life`, `last_qualification`, `total_experience`, `total_students`, `adhar_status`, `adhar_img`, `view`, `bank_name`, `account_number`, `ifsc_code`, `account_hold_name`, `referral_code`, `added_date`) VALUES
(158, 'Amit', NULL, 'amitgoyal9268@gmail.com', '7503604714', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 2, '', NULL, '0', '0', 0, 0, 'fVF7fKEHlX8:APA91bFTalLNup3JdcptoLRcwO6HVdooOYb5EcKPQt6dVJgBFU49lsEgJauHFimSYaVgLce0V2HSCWnKDWtAfj6rQgHzHxcFaygYtKSaaB93YnCiF4PEywPeSpZkyvydSrb4eLqPt5PV', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TFS2CK9I', '02-02-2018 17:35:55'),
(159, 'M K Gupta Sir Classes Gurugram', NULL, 'mkguptaclassesgurugram@gmail.c', '9999861817', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'epIvkXWIwQA:APA91bHOZytNVhST0nTmpJBwK7-fn9Uxn1zL_6V7SpQ-jlSfPKzVmJZs8eS0dqPxms_MgHSnwuTFuNgNnGiNHPq82vCmGZvZf6F9crql44zqdvGsJJA0gSWQaowCxYzvWnRFl69L2vK-', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T8JZ3GAS', '02-02-2018 22:23:45'),
(161, 'Raj Gupta', NULL, 'rajlacs77@gmail.com', '9654747877', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'eAw_RDP0Ybc:APA91bEQOcCuFjD9nLsuDm3jObYLJ_eFSQlVaSbCtCWuO9wJnvwnfE28Le7nSkI_TA-jsGwwHrUyTwL_iIIl18nkLJ6Eyix_g9aSVRbDRqce7GqkZcxWeViwvxW3-z_c-GXVFpKz1Nnt', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T9FIDUAG', '05-02-2018 15:36:15'),
(162, 'K Tech Institute', NULL, 'kamalaggarwal6@yahoo.com', '8076741588', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0665831', '28.6801066', '', '495, Nangloi Najafgarh Road, Saini Mohalla, Nangloi Jat, Nangloi, Delhi, 110041, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'c5Vr12i-4mA:APA91bG4MyLM3pJpHdUp0EdsJvVlQ4FxdpgUuGmBHtNyw2Lzvcmb0hOEc36666m9fBTyEbM_DvHeTVjTX8S7c5NvivnR0h6zCGDRo7Bkp0bmdHBs-cC7vDUOE0Hg3VDnH5-wIORRmTE5', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 0, '', '', '', '', 'TVKSXFBX', '05-02-2018 16:30:38'),
(163, 'Kamal', NULL, 'kamal.kmr20@gmail.com', '9873960351', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'hindi english', NULL, '0', '0', 0, 0, 'c6H5K7wThlU:APA91bGIuv9BTIKbn-F5NltDAQCRQqNcByBacELePg4H3LNJQBlh2bI5xieziL78KUMQAAwkY6HCOR-5WLSD1KFITQC4PMhZkDKPw9Wk6Fv6FE4DA2mSpXu5LTzYcnmT6iLmUw0hLRiz', '', '', '', '', '', '', '', NULL, '9 years', 0, NULL, NULL, 0, '', '', '', '', 'TKUNPMP2', '06-02-2018 14:08:06'),
(165, 'Keshav Jha', NULL, 'keshavjha2011@gmail.com', '7530837293', '', '', 'teacher', NULL, 'M', '1996-12-25', '', NULL, NULL, '', '', NULL, '77.0670407', '28.6702642', '', 'G-129/4, Laxmi Park Rd, Lakshmi Park, Nangloi, Delhi, 110041, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'eZsXUiu3mio:APA91bEcZi2jXIyCvtr5-joT_AnYF4aY-r2vT0Iyndl6muDwxMAr0S3moODLhg4aHWQMBGiAXNSubRTX2bPl66dNnVc7qgeKaAZrsDHWKKzTW7xlPGTiGWr5D-tbkLIXZLiAUqj5aajh', '', '', '', '', '', '', '', NULL, '4 years', 0, '1', NULL, 0, '', '', '', '', 'TXZKFIJJ', '06-02-2018 15:18:04'),
(166, 'Manoj Kumar', NULL, '163careercoachingcentre@gmail.', '8750968190', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'cA1k9iVa0V8:APA91bEN5ncx3hcBxx_-BoI2PReVLl1Wu16HonkvbW9wTg8vrtZ22vFH5NuMx1ENh7f5nCCBdeAnyFHETArQJFVe99F12ok63mHWf1FrxGFVaxdlVs6XzDqCp5qxxMW8TvA8gcJTmQyP', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 9, '', '', '', '', 'TYUC89UL', '06-02-2018 15:43:07'),
(167, 'Sikander Chaudhary', NULL, 'skchaudhary8959@gmail.com', '8750821922', '', '', 'teacher', NULL, 'M', '1997-04-20', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'F-74, Saini Mohalla, Laxmi Park, Block B, Lakshmi Park, Nangloi, Delhi, 110041, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_167_1517994279.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'd3kWTSqQUxs:APA91bGr45ZniVUvtVCSQ7Dbb9zAnr53vMHgZcNn8V_gCo25EcfQEHkpEJrUETzSn8xXIg46r4DxFiZiXJ_P8jBzZxBQoJ03rZsOTsrewkVXszvlqQ76NA3AgKv_NNkE5JJ9WCvx5mFl', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 25, '', '', '', '', 'T3U1ZPTX', '06-02-2018 16:06:46'),
(168, 'Sonali Tiwary', NULL, 'sonal.tiwary85@gmail.com', '9810931092', '', '', 'teacher', NULL, 'F', '1984-03-23', '', NULL, NULL, '', '', NULL, '77.0613266', '28.621271800000002', '', 'Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_168_1517916111.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'English', NULL, '0', '0', 1, 0, 'f4nfLC6TtVQ:APA91bGXI6k3CqvurD2qYZKfQiCYjirgQJD8MsjmpuJpxVcb2jvLIDp57BmhzNesMIS2NLG9dPdefENz--L3D5TwkTnlZM9E5NPumf_-xcgTcGZMNf22-yG2TfM9TNNXDy7gQ65df4Ha', '', '', '', '', '', '', '', NULL, '6 years', 0, '1', NULL, 0, '', '', '', '', 'T45VOZIE', '06-02-2018 16:47:20'),
(169, 'bablu kumar', NULL, 'bablu7587@gmail.com', '8802010347', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'dLoRrxqOrjk:APA91bE_DI-lcd7sBndnKR5Q_tMRSech_4EGnCyoBvtbYaTgl3zvFcLIPYkA0kNo4aLqW7oWEc_KqclGbbwhVG6UFKvFwYDGU5IyRBq1nkKBxbRc7ivT9AocmQ1CSYhz15N3Gy9LE1aW', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TM7GJ6AZ', '06-02-2018 17:55:13'),
(171, 'Dharmendra Mishra', NULL, 'dm1046230@gmail.com', '8800809791', '', '', 'teacher', NULL, 'M', '1975-05-14', '', NULL, NULL, '', '', NULL, '77.0738028', '28.6387532', 'Vikaspuri, New Delhi, Delhi 110018, India', 'DG2/85, Pocket DG 2, Vikaspuri, Delhi, 110018, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_171_1517923852.jpg', 'I m an experienced professional teacher from last 21 years. I taught till the 8th standard all subjects.', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English', NULL, '0', '0', 0, 0, 'dlKZ3ysHZfY:APA91bE_hw97kC-x3uJdgJu6h1-6Ae3TeqD9OM_zI8QKPjL6flHIONErvBI_JQu6M8tBYJ4CzyDIvyVStlJzTuGFTa8izqBYvYayioDAy2DsRV1OgPZ2qlt-MrBk-yxPXtJ_JiNu9nWD', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 18, '', '', '', '', 'TPUJOPLW', '06-02-2018 18:47:19'),
(172, NULL, NULL, NULL, '9821679299', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'el9sJqSPGOE:APA91bEzN404HPFObyUbMyoZKBR5ysrSR8iB5Tg4FY-3_AurmDS5I5X_05kEwiNTTP90PUr5SG5zCMvs9uem_PNqYzeorI5Da7FZLmYv0xCS0aT0Lf4rkKOY1EXThtIneGU1e-NEl1V-', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TYFH2KJB', ''),
(173, 'Sunit Singh', NULL, 'sunitsinghgalaxy@gmail.com', '9718885432', '', '', 'teacher', NULL, 'M', '1989-01-20', '', NULL, NULL, '', '', NULL, '77.0349204', '28.624530999999998', '', 'Bhagwati Garden, Nawada, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '9 years teaching experience in mathematics subject', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'ca1wPytx3oY:APA91bHwTxFqw4pPejXfEcSKVP1O56fr6kN4_nLzSi62Xj_KypJzzYPAsClLLqa039k6t3IMALqzoK_a56cLXUZ2u2jeMbLA_yZM8S7GCzBUrwl1njiLkPvP2vyrwd2-PqmzbqVIwmCt', '', '', '', '', '', '', '', NULL, '9 years', 0, '1', NULL, 1, '', '', '', '', 'TZN9ETET', '07-02-2018 10:59:28'),
(174, 'Rajan Jha', NULL, 'rajan.allthebest@gmail.com', '8368164392', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_174_1517996633.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'fJJyXlIPF44:APA91bEp6YXwx-lMocNXRDY6StEJ2EqqkQ4ySenujd0ijmV_46MtohSjQaIRzuoVyIqmV8tljR3Om7dfVu0kivFP_6JlwHp1UPo_DwzqOPGag21PO2K_tpt7w_Xa0sFVMdth5w4YqJyR', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 1, '', '', '', '', 'TTWIF4YZ', '07-02-2018 15:13:13'),
(177, 'Smarty', NULL, 'cs.smriti27@gmail.com', '9973628865', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'cu65XdEGDtQ:APA91bG_rVnY-qKhd9Ms--MTD6fjKTzD5Xq3ZuYIOJs11EzrgV4t5b8KmsLNEFrRutRMT6sm54N_GSqpKF8Bn8ttrfpJXXuL4MNyTf7S9U1v_vLJJooZ9i-WDN3N2MzNCmt8AYNHdDH0', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TAZZAUJZ', '08-02-2018 11:48:20'),
(179, 'Prateek Sood', NULL, 'studykingrohini@gmail.com', '9999394997', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'cTot3kSYCN4:APA91bE9-TYxCe9qkd2bnyY86xPXmFQfiFqwsJNQRyCDK4Mx-N-5Ibs6jppa_jcp4533Lv-Eu4Q-t0vElH2h2jPzF1eskMImwEy0E5BfjueD4vockhPkytSsGIgaXnC3YFpBdudfscxe', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TJ4VIKNK', '08-02-2018 16:04:09'),
(180, 'Ajay Tiwari', NULL, 'yash23ayush@gmail.com', '9211843027', '', '', 'teacher', NULL, 'M', '1978-07-01', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_180_1518090199.jpg', 'myself Ajay Tiwari Msc from Allahabad university.  due to keen interest in teaching guided thousands of students in last 15 years in his career.  now my future plan is to develop new sphere of education In chemistry and science.  ', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 1, 0, 'fbWgh7Y24HA:APA91bEhWLmWZjhDYRl07cJS4H67Lc5yyImS5QRVSQZqHmD6tGj0Eubdg9l8bJ4jW5wfXjQJzkpzU8mzrnsJI60hXTkJgOVbQgqAohCueLA56JX7XtHclxKoGE6jXlC9rs36fwxotsi_', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 24, '', '', '', '', 'T56WFU5J', '08-02-2018 16:28:04'),
(181, 'Lalit Kumar', NULL, 'lalitkumarcsi@gmail.com', '9899009023', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'dSIJjrbljJA:APA91bFkTjvzHKDZkKegyqjOSTH-6MjpxW-cgopmR3hEaFr0_pKXlmU2DOyNaITA47d04sJZFm6MOpBiY7uCICUOzYy8FXxnRv3x5fvpB087RMCUyJ429rdXyGzq-NbHiJ4HpVygU9vH', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TNJMGGVM', '08-02-2018 16:29:56'),
(182, 'Sonia', NULL, 'sonia01@g.mail.com', '7290926857', '', '', 'teacher', NULL, 'F', '1985-01-08', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Pocket 22, Sector-24, Rohini, Delhi, 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English', NULL, '0', '0', 1, 0, 'fqJn7ayu-zA:APA91bEYhhJRo66_a2JKcBym6YziR6X4Ow9rKSEnuiwmDQGgQgO-NHX-cwVcfzKDIggWEgBziv0NRW3Cf80P3F2NvdurCtCxKVyA4MGsZdl9PohHNM0MPIW1J3mJwWegjwpClTuFGFjj', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 9, '', '', '', '', 'TKTMV3MV', '08-02-2018 16:35:11'),
(183, 'Ashish', NULL, 'ashish.chikkara90@gmail.com', '9999275155', '', '', 'teacher', NULL, 'M', '1989-09-22', '', NULL, NULL, '', '', NULL, '77.0890993', '28.630736499999998', '', 'A2, Janakpuri, Block B1, Janakpuri, New Delhi, Delhi 110058, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'eiruaeC2FeU:APA91bHTdL0upvMPzLSx5pWlGtoau0w3DOVwGgQi3cE6YmJ_BcFDqcl6oGzAUtHA82-MCvkbNQFwpviz9huIOjX5ijd62h7ZVWIqElcG6YS2CfFPllhRHbLUYkykwFCMrH-qPpZqVCGl', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 13, '', '', '', '', 'TY5URA0A', '08-02-2018 16:54:46'),
(184, 'Sunita', NULL, 'sunitagupta42@gmail.com', '9871080006', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.063141', '28.711454999999997', 'Sector 21, Rohini, Delhi, 110086, India', 'Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'Hindi , English', NULL, '0', '0', 0, 0, 'dbKhHw0g4rg:APA91bEkI9lz3KjoSlQ50rdVMIhouuivx53Hj5gcNESI_P6Afpn1Fh9Q4ivHkxNJaqcBOICeWui2p6sekYOo3oLgV_oSIPDMGHXm5X6649wJoHPAbKlQrC7VLiGbsfQkyHHZQaUufHxv', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 8, '', '', '', '', 'TUOUAVRF', '08-02-2018 17:53:17'),
(185, 'Mansi Marwah', NULL, 'atulmarwah24@gmail.com', '9971270642', '', '', '', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, 'House no. 8, PKT-9, sector-21 Rohini', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, '', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'English,Hindi', NULL, '0', '0', 0, 0, '', '', '', '', '', '', '', '', NULL, '7  Year', 0, NULL, NULL, 0, '', '', '', '', 'TPXZCGMI', '2018-02-08 12:45:46'),
(186, 'Himani', NULL, 'joyabaniwal551@gmail.com', '9213110551', '', '', 'teacher', NULL, 'F', '1995-03-12', '', NULL, NULL, '', '', NULL, '77.1001524', '28.7072357', '', 'A-1A, Hargovind Vihar, Pocket 2, Sector 4, Rohini, Delhi, 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'English', NULL, '0', '0', 0, 0, 'd7Wh1GtSD6Q:APA91bGSqw-exW3-AYmWAHCKy7bTHZk25HTMcGNyuQynLdZ4ff0xZDlJbehBAcVs1pSWn18HLNyc7Rk3XQayVCOS35EoMjaaK5fUyZPAWmYIKVBydWigMzvBDsm_Mb7UKCqSOKL0XIdi', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 0, '', '', '', '', 'TXRFHL1C', '08-02-2018 19:00:42'),
(187, NULL, NULL, NULL, '9868373466', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'c2NgJDUuUM4:APA91bHa8FNSycyhvt3z6PYdmwKl-jG6uYHEZyOGOZ9sHpno4CW0ftawWRU5htLmMYveBO5hiPlExL0kKZy1vpK-jzABA-00Bjg6QPARv16HYs4lOqU-cRxkzNUu_B1Iq0qbhuWv1932', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TON8WSDO', ''),
(188, 'Sachin Kamra', NULL, 'sachintuitioncentre@gmail.com', '9899242238', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'F-25/128, Ayodhya Chowk, Sector 7, Pocket 25, Sector 7F, Rohini, Delhi 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:56', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_188_1518318161.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English, Hindi ', NULL, '0', '0', 0, 0, 'eDBrkojXSVY:APA91bFDgf7ZuqrVqEdHabxdhlHCbL8FTOQq5AyxDRZo3Mj-OEv4tU9leb8GeCIZQ5iiA9qcAASis3ksO8QfgEvVgmE0mOp0IsBnS8Xes-kO1qYornK4SMq6G5EBF14IHlAHevvCJT6U', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 7, '', '', '', '', 'TCEY7STI', '08-02-2018 21:31:30'),
(189, 'Manika', NULL, 'manikagarg786@gmail.com', '9540899529', '', '', 'teacher', NULL, 'F', '1993-08-14', '', NULL, NULL, '', '', NULL, '77.0796189', '28.525589699999998', '', 'Kapas Hera Extension, Kapashera, New Delhi, Delhi, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_189_1518163779.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'f0UhYe_rdBg:APA91bFaJDrMwnbfNYK7y2tGJRfS2Vh704eY9ILlxMxalDGUzF0HrsiL8BzYnR_EbokVzjJpFqXEijFSEqF_1L8L155OjUPkKRvyfPYLBTflTnq9qQWh9aqtE03JY2zAmzTTqvGFQ8uU', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 13, '26c1a23a970c69f16fcc19a091881e03', 'e7cbb9dcda49f86257219fbdb7159ded', 'ecaacb8edac812000ce20cdcadde8769', '0136df51ba40f03f91658fe1251db589', 'TNUXCSXO', '09-02-2018 12:43:51'),
(191, NULL, NULL, NULL, '8510888071', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'e2Rfah-9GIs:APA91bFJFR8NwM8ZkoXVO81pJ-XybETCxxsXl3-IleSqcyUlF0Azd892-rmJIRzoprR0iaNvkUQcp-eKoaOr8VZ2hMOsTVy0cHf4rcT4MVW7-0EilRpp0cT-Ddz88izPRdCqQrQTcC2t', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TJT7GKMN', ''),
(192, 'Shweta Pal', NULL, 'shwetapal004@gmail.com', '9654288604', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0881034', '28.716994299999996', 'B-13  veer singh colony budh vihar phase-2 delhi-86', 'Budh Vihar, Delhi, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_192_1518173358.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English, Hindi', NULL, '0', '0', 0, 0, 'c28mviLQ3K4:APA91bHR-01o5W-JIyrJOu94hnLuz4ZONFa_TZacCdoz82vvrzp7Mq49jpnmAXGcng6wT6Xz4JrP5GTEjtWFT4AHvlmGaC8e6QYSa5TPW7wgFa6kR2Ua1dbszrJKIJgx80c2ZWKEz_2_', '', '', '', '', '', '', '', NULL, '2 years', 0, '1', NULL, 5, '', '', '', '', 'T5O0UB9R', '09-02-2018 15:28:58'),
(193, 'Wilson David', NULL, 'Davidwilson623@gmail.com', '9990914612', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0730892', '28.7102485', '', '2/217, Block D, Pocket 6, Sector 20, Rohini, Delhi, 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_193_1518171031.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'eqT7kdzBEew:APA91bG37hXgWt4xmnggALnHVYqPw6FdWPzleb7rq7Igw6tZqb9LyDzmiMoorhWYUkQmPTvK4jjWEfJqDCechw7wiFT5UqDNbJZ6mrKTATCRNOLVO2GMhFcnNsLUQOy63aLJ1W0znDRd', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 6, '', '', '', '', 'TEMFHQEO', '09-02-2018 15:33:43'),
(194, 'shanni sharma', NULL, 'cashannisharma@gmail.com', '9871210063', '', '', 'teacher', NULL, 'M', '1992-05-25', '', NULL, NULL, '', '', NULL, '77.0266383', '28.459496499999997', 'Gurugram, Haryana, India', '42, Old Delhi Gurgaon Rd, Kapas Hera Extension, Kapas Hera Estate, New Delhi, Delhi 110037, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'Hindi , English', NULL, '0', '0', 0, 0, 'fZc04qST3ME:APA91bF2_dF2OPxdi06GZLeEVjqyrVTrAAd_wy8r5gMeMD2mO7lpAZWU1gPVF5mvKfeBiCwOjdEud_H8H4hybLSUK-XYvuq4WlpWLGOkvQC7p2bFwL0hOYwjwHgkzUVYKcdFw0eJSDgj', '', '', '', '', '', '', '', NULL, '4 years', 0, '1', NULL, 7, '', '', '', '', 'TTI7GD4S', '09-02-2018 16:09:40'),
(195, NULL, NULL, NULL, '7982445266', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'c3XAdxd4wS0:APA91bG7I4HgCbeYdLes2EMXXkUD8kdI5nuA-SGVi09H8fs2SSss-THK2XmA9sVPFtKY7Tb7nqtBFlaMRHK0g7sFk5u1AI6gewQHnuV635GBzYOOUjOo9JNuIkivWkG9J0RfKvzeZ98P', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T5CHOVPU', ''),
(196, 'Palak Mehra', NULL, 'mehraharshul13@gmail.com', '9910816012', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cjJS9-ULEtM:APA91bE6Gm3pztHxYIx-KKn2hGysukYi-1Huw40Kpi7Fj56E8t4nrZw6a73_HV4lrDEhlFSZSACFgU4BQLFVpa7zqLsXkUxNgNZ8QT9uPQMsR4LspsTlyV5485Nf0zBF36VqBgz780FD', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TGCSM0SQ', '09-02-2018 16:59:10'),
(197, 'Nisha Rajput', NULL, 'nisha.rajput1992@gmail.com', '9650382164', '', '', 'teacher', NULL, 'F', '1992-06-11', '', NULL, NULL, '', '', NULL, '77.0441231', '28.5609483', '', 'Sector 23, Pochanpur, Delhi, 110075, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_197_1518177292.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'dJ7eg5vC28w:APA91bFJOPw05varg-hN9jajIvT8T7faNEv1d5H8nObpd2oipK6A5sRclTtgsy9w2w_PH0_lVTwVtp3W3fc6yJ6r_fbCkXDfa880TOjhL470Em1ARJz_BrQvCC_T6asDOHh7TVDREq2S', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 0, '', '', '', '', 'TB1HPOX6', '09-02-2018 17:21:01'),
(198, 'Ekta Chaudhary', NULL, 'coolekta08@gmail.com', '8512020518', '', '', 'teacher', NULL, 'F', '1993-06-08', '', NULL, NULL, '', '', NULL, '77.0735173', '28.6067256', '', 'Rawat\'s apartment, Mahavir Enclave Part 2, Mahavir Enclave Part 3, Mahavir Enclave, New Delhi, Delhi 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 12:32:36', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_198_1518177264.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English, Hindi ', NULL, '0', '0', 0, 0, 'cvxdZ2VnozU:APA91bFb3N9cYmgDLCjccX2Ilc4UhW9vbqwqEBt-1bXMsS3FBHen4yjRq9yI6kryiLbGVnblR0IqyCqNZurOJB1I3Fb8GcbKBOcuCgMg7RGxNp2HKgXAF2rfj00ZMib2xSbGlOa9qHUH', '', '', '', '', '', '', '', NULL, '3 years', 0, '1', NULL, 45, '', '', '', '', 'TFLV8UCO', '09-02-2018 17:21:34'),
(199, 'Ashok Kumar', NULL, 'kpkumar6265@gmail.com', '9729411559', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dUbf_oPvZ9s:APA91bGf-f6sN0rqNS914s4AI92cZ7HMH2LBSOG-G8fYt6y7xTpB_vIoB0ysxgNiRKP9gGDJcF9wF2lTw8qK2OR9bMP9euUjjZzumpaYpE_Fw8WLTtraev0GU9OMH-0kLbtb5-jYcy9B', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T7HRCJ90', '09-02-2018 17:27:26'),
(200, 'Aone Institute And Home Tutors', NULL, 'Rahjha2011@gmail.com', '8826071169', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Sector 21, Rohini, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_200_1518180976.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'cEsU0XmoXr4:APA91bHxGj4IlbPQCHl79JETr5sBn4GlTVHekJcE8_ORb16ugNwGLxJwqtbMKIfaGL-KXgh3ZWiplxYe-TDXWu9pDJ4qXp5cdOy-t6XAAa_vOffBpceUL2OalVBe1MclGN-NlONk-zSW', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 11, '', '', '', '', 'TPYU5KMR', '09-02-2018 18:20:30'),
(201, NULL, NULL, NULL, '9210099067', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eJzW8htQ7EQ:APA91bG8pYxm-asV_pOmWihoB25690q1L2OYYkKfNTJBAVvfno34LNNUMu8sIb0xGPxv0NijJ3dQPui3Ye-Gi9zPQJpvzisGlMeKamRp8VH8Y_2CwihbZMGWzXT2OsGbY2lY3ShdrzLT', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TLFIXGZM', ''),
(202, 'Himanshu', NULL, 'himurajput22@gmail.com', '9910682371', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 1, 0, 'etSFhHrWC-E:APA91bE1S50WwMXKAQqIytDu9FL_Z0cMxjV9y7mZQtmYD-p7SyH5LNRAcApCD2Mi8Hk7K_vrfmOaEP8_nNpsvqIBrH6BK-tJDcCDeqYfHkiD5nv8rDAyWwW2ejdBIlaaIo-wk9iJxJeI', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T57TKTPS', '09-02-2018 19:46:21'),
(203, NULL, NULL, NULL, '7011382865', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fsKYG6ACmvM:APA91bEurMoKOM36CaADLBVB075vI_SwcyqKcDgwj5mSqbOpaszxLBcZR9fxtRxy4by8FwwVPZsjFvvC9azjoTVia4-kS59-AbRsroEfGoFtsBAEWesa7SFzUWtOtAY5pxat6g5Vp5tC', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TN2RUKIG', ''),
(204, 'Renu', NULL, 'renupoetess@gmsil.com', '8826832108', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_204_1518200481.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'ewljWVmaEC4:APA91bHuxeDET-I6XJ1sEZOVfdCvPmkwxlgszX8cW17AiEvfx8zRXgqL7ajowiPDWa21r_JBeMvkMB7XztIGhKYPnSlFhPzu3h_FwWVqXVJyZzpxu8sPTqcKTIZN83wReq2lBVeCrsKr', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T3RGTQBY', '09-02-2018 23:49:32'),
(205, NULL, NULL, NULL, '8527656418', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eQCWa3rKFIQ:APA91bGVKbNpEwJqEDOtd7ifD77XZd0_G331c-OrCwVpuF60vCNBwR7yjjEohIkX0mCnRhVhgbaKHGxrBVoM5YwMbp3R901qPIw4HsE5ffb7swWXU39BVyeTe8D-gB8zXLDmX8Qx5oJf', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TBYQWNIT', ''),
(206, NULL, NULL, NULL, '9953536448', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fn8JctyJims:APA91bHwfwiGucGI6KA9YsTaEDHIM2qpJdWKrn3K9NE_sEbe_KJd1xaLgeBlAlMRAGdEQc8dLQ-xVMFTU8QPIDrFa9mBXtTMToRt2TJZfpN6XVnCc3hRgw7ViS9iv3N3RFT2eHqDo_8u', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T4IG9PAT', ''),
(207, 'Shweta Khattar', NULL, 'shwetakhattar947@gmail.com', '9650035973', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'dQy0Vv_xCKc:APA91bHJ7spX5IWUzqwsp3H01Lk5v8EnEReLeyVMuQGD2IZjW8OvzGT0K08Neczrge5D8G1lZW7jKkms5qvYqOdGCAAQNO1l_lxA_OAGwd1ec4Udne_1gN83lr4y6tcCQqYXVsfmWEyl', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TIZVG2MA', '10-02-2018 19:11:48'),
(208, NULL, NULL, NULL, '9810534234', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cNJqYxyZe0s:APA91bHqmhpdKEO1ztRf2cXhZ-WYMHFW6ubkYWcg70W-Ct9ZPdQk-flNXv9SiPDZmWBp2fQfIdnTwM0BtQnIlhZ7SWVlPeva3sQfaSnTRyJKvnV3cjQ4ASqd44qX-YhghJSglE-_4yx6', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TMVRPNYI', ''),
(209, 'Vijay shree Bhardwaj', NULL, 'vij.moksh@gmail.com', '9560899141', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0618235', '28.4798417', '', '462, Hanuman nagar, Sector 17A, Sector 17, Gurugram, Haryana 122001, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'ejvdkG0K69w:APA91bEftmUAW5IFKMHygdwaWKODV6ZIkxB2GhkbWSAYnJg87PqXCbeWLhMYdlLHQA2351g60Src3cp_Q0x0unzMU7Uxn4-CMKqBDc9Kncg2WiTqJ9bGfsP2nJp9-yIbKa-Ak_drKjqf', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 0, '', '', '', '', 'TNKHZI7V', '10-02-2018 20:54:34'),
(210, NULL, NULL, NULL, '9891262647', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cN2uAXKc8VA:APA91bGi6RtoqtokCjNVe7JQ_8meUdKgByIsw2vxyNn1zLT9MDSvsK2F-OAChFrHItVfgA9d3j4xzZJgMRF_B_ds0xOevt-d4A4geqThs185vSFHWgtxOPz4ocr-iBO3mifWh1z1ZuaH', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T5QP98VJ', ''),
(211, NULL, NULL, NULL, '8076512061', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eMLsOKuctkU:APA91bFdV7seahKyGG4zc45RaM575JjaJcsn27hvugJFWt63UVxDCzzCEyCLK7pklSedGTjPBCmcNOxFjaavhU2rxPRJtUZjJoN5VEOjYQ94oLrkaQt45UxURziPV-cSIAaoBPQgjI17', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TX6MQ5IX', ''),
(212, 'Inderjeet', NULL, 'inderjeetgajwani007@gmail.com', '9871551197', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'ehBXint45TQ:APA91bGJgcu7z4GYSfnu0FoxhrYvBQSM588QxDJgX8SZctxMe8xG0KohMCg3rxiuYAJUZ_OfqV2EFQm6XtjZakgOk2fTOMH2hN7mgf4yBdi2nc3HLTFgwavDSYttDudOpNnprNjSVWDC', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T7VHT0ZJ', '12-02-2018 15:45:51'),
(213, 'Anil Solanki', NULL, 'shubhamsolanki14@gmail.com', '9818800892', '', '', 'teacher', NULL, 'M', '1975-08-01', '', NULL, NULL, '', '', NULL, '77.0656327', '28.6234681', '', 'B-14, Old Pankha Rd, Block D, Ram Datt Enclave, Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'f4BsFV2Cpr0:APA91bE7v0GkJhOLjnGK22PsCPwBPJtIak8RBLbImnDaG-HLdQ8dvby-PH072WcDblhTLvcD-9kh7wECwQ03IfXKyf6rhkQPNg7L9InkULKBNmevJ71BzbGYzTJ_3_LAO6MbQd-k8gfo', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 42, '', '', '', '', 'TN7SBRAA', '12-02-2018 16:55:44'),
(214, 'Aakanksha', NULL, 'akkusehrawat@gmail.com', '8802417295', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_214_2202.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eZVrHgi06hk:APA91bGmDEhGFedr3G7_uO26Xb-6MvxzjHCcTuc9s8k376djcB1o8iWflAWVy2Qz8GW2KccUJxbr4bq4GZnosDQuaHlwxHMC5sPGvxWaIzwjk33ReObGrcJkzr-gvfXo5eYsUKvwrSyu', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TAI5G8VP', '12-02-2018 17:41:30'),
(215, 'Lovjeet', NULL, 'lovjeet08@gmail.com', '9718240430', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_215_8247.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eo6T40dSjH8:APA91bFfgUsk3LfLwqUGxPQjgxW9EHh0ER5Zs4r1s1K6L_mM2hZP3WEPw5uJEAAqNrQOlPlUI-GYQ599JgqtD2s4BqD7pUKi8dwMYTggK_aG-bD6cbwqBAo78eoFalY6umZMMaz2Jsc-', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TGRIOXVF', '12-02-2018 18:12:00'),
(216, 'Kishore Dev', NULL, 'devkishore106@gmail.com', '7011171839', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'e49vHk1ALHY:APA91bFDgTjbsk8FirzGOFT28KYiRM3KL3KqOyUQVTUFZEZr9eUF2Ea8Pqu6V9O5XvBkks5cQuoNzcPzLAyaireyBnPsszMrneQ5eP_LZEu1TP3aC0t54tPdk9MRrBXqGOw8RWvXQcLi', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TDDCLIUE', '12-02-2018 18:53:30'),
(217, NULL, NULL, NULL, '8802215766', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'eZq8LbI0bYk:APA91bFDfIXU3G6ZJeLWTdjKRH-32pgSCJqnAenDF1tO42qOjVI0LgIdRQF3q3VMOcuaYRItVVeRJZl6xX_QzF8nhdWPzVgdDlA-i5vP9uH-pdw1L8gb7_El5C66KcSnM_8Rhafks7Ce', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TJTO6BRH', ''),
(218, 'Sumit Khatri', NULL, 'khatrisumit92@gmail.com', '9811648007', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fSwn3sVPQ4U:APA91bEOKlEvsduuSe5KVf7fJozghNNk0wByR8vvJoNDsPFVwAoGd_qerZO0iaw2_P5WUbBDo1TQIibGpk72vWr23IEGNxe5Z3E7djqCHVu_dTmm1UdfsxljcuBk1WTOJcIGsbxAF3lC', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TT1REKXU', '12-02-2018 23:29:40'),
(219, 'Tejas Coaching Academy', NULL, 'tejascoaching@gmail.com', '9953803132', '', '', 'teacher', NULL, 'M', '1986-05-18', '', NULL, NULL, '', '', NULL, '77.08666459999999', '28.709693599999998', '', 'Budh Vihar Phase I, Budh Vihar, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_219_1518509640.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 1, 0, 'fmGHcZzQ8ek:APA91bErBGWtZH1gVJMVZERpGWiF_ezirdQE4fTNPEtHJfd0doV2uioN_6EmyABJ3jlBBmaqozJb-PnZ86mAxddLGKRELA1rriwBqUalNWnQuF2GmA2FiNBBI9WIJi7ahzu7NTFcPDgZ', '', '', '', '', '', '', '', NULL, '5 years', 0, '1', NULL, 7, '', '', '', '', 'TSSKJU2Y', '13-02-2018 13:41:44'),
(220, 'Sashank', NULL, 'shashankg720@gmail.com', '9990897898', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'fEeaxcJPjK8:APA91bHJbBeVFWxgJ35E9pCTo8EqLSDiJkuuAxBV8dcNBLkcU46IR3DWnZA7fFqVKyXYYSOcfib5PZQTl_BPvQ4ziwZmFE7sXbCnUSKpD1m2OMFVokPFhq8RVnwLA-Sh2DBESLYmg01v', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TRYD4MQP', '13-02-2018 14:08:22'),
(221, 'Sumit', NULL, 'devdizz143@gmail.com', '8802915021', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fFAAvwuwnh0:APA91bG3VJq5x6tCYOIvZkKgP4uZakpFPdYkB_nNKUlsvOM7jIeSlJa6l5OBZX6N08uGSG3t9gm7T-B3PIxfdQui68lhvGjkHlmJiYMbMdTpn1SVwqGG5w7NT1Qkegf_Xlj-GKyUHMTg', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '9ab20b879f7e17482aa3c194d469deb5', '8ee14731d53cd67210015a456be6f0e7', 'df84d27504359c5594acc21a274f5b0f', 'd420611c11e0843c7ed180ea375a029b', 'TUKUDOTK', '13-02-2018 15:21:49'),
(222, 'Sourav gupta', NULL, 'souravgupta8282@gmail.com', '9582824711', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_222_1518517134.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'cYosTOMMgqU:APA91bGeXZNimpPdtLyvcA2Y_sVulWZYG83YG17HbxtyROolmogaN7o895KdXYn_-HuYIb8DQHVXegxSv8CSMGmZtU2SCgOE6QYYGy1395cOKQsr8Lono0r0AI2fGMWNbemk-m78CiDt', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TQKCUM4Y', '13-02-2018 15:46:32'),
(223, 'Ravi Rathore', NULL, 'ravirathore9718@gmail.com', '8700145260', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'd9OqHmpboxM:APA91bG-SgFkciU3K_5MAPYR4E82Acun7GRp4Vz-XytvGWkTVH5VkmLJD5NBOLMRzgzoGO1_MYGOjMlcKB1hmY-tE8BdXECs1sE3gulbhP8Cb_dIjEpkZ-SBGsEAoI4vvUoBs9Cak7MQ', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T6CSZ4MJ', '13-02-2018 16:29:20'),
(224, 'Narender Kumar', NULL, 'narender14134@gmail.com', '9253442200', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'dg-73pn1U_0:APA91bEMKv8fhNW-jhYyq1n3XfwKPt4umBOA2F70EEhXeIO0maY8wIJ_g1RnhLNzHoefNzZiOukO2sV-ZpIiM4On_yZWoO4VcimokiBcASpnn4J_nEtG9BqIcym_8sY5Q07SJIJ0A4rk', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TYPRQO5U', '13-02-2018 16:32:38'),
(225, 'A D Nayak', NULL, 'alopideen12nayak@gmail.com', '9810563093', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.11049090000002', '28.706563700000004', '', 'Maharaja Agrasain Road, Pocket 25, Sector 7F, Rohini, Delhi, 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_225_1518520714.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'dz3afdsMQ64:APA91bGAYAncWHqbLqcSqQXn4by3aneVItcSpJC1zTiWCiRnaac1pA501v_Kf3VmAm6fCltXzN7GwDuSPJyEdnAXyo1B_w21SoJJtthzkuSS-JTytb2suKcNdXNFB9mZdgq10xnsFJ4n', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 8, '', '', '', '', 'TAWKEROS', '13-02-2018 16:41:04'),
(226, 'manjeet', NULL, 'mjs_2110@yahoo.com', '9555562364', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, '', NULL, '0', '0', 0, 0, 'dpS7quv3Ukg:APA91bFrRYE6Prxu3Piq5_zSfrAYmJKqTNQRClxIAloEgnF7TEOQFY_Hi0k3VRMoPKMbxmkfiRw0STtG0RU89nH98OwHxwDtcfhPAXXrEKWld16VKiAaR6Cr9dyyANgAcBjssICjAv8e', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TFHD6SP1', '13-02-2018 17:55:13'),
(227, 'Neetu', NULL, 'neetu4775@gmail.com', '9654142590', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'c8kAnCNELbs:APA91bHEBQsB92GF07tEGtl42vQkTRn5yOZcYrJZyVh4rNN4iZMU4nFB5JNrZsc-GDFLKmDMN-TCJDMw4ATEAE87J3HV9kURGMn58l8gC1BGyVyy_i-cj69_N4pQt8-l8xGsGzG-iHWi', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TETALV3V', '13-02-2018 18:15:37'),
(228, 'Manjeet Singh', NULL, 'mjs_2110@gmail.com', '9911558346', '', '', 'teacher', NULL, 'M', '1988-10-21', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Avantika, Sector 1, Rohini, New Delhi, Delhi 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_228_1518526857.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English, Hindi ', NULL, '0', '0', 0, 0, 'ez3Lgij5fPo:APA91bHFUPxscCWb1jfnFKXHScDiOSaE4pRoeUc7HSDETyTqnjXzGkTLJfPPjRvRKdPYxeufusMd9JjRCHaqWxUzZgE6P9EK1MBKlq6Xza_FAmOhhoAUI7wuZn1ZGw_T1kytvdVCaGd6', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 8, '', '', '', '', 'TAQFZF60', '13-02-2018 18:24:48'),
(229, 'Haridass', NULL, 'tech.haridass@gmail.com', '9953576856', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'F5-147, Block F 5, Sultanpuri, Delhi, 110086, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_229_1518527357.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 1, 1, 'English', NULL, '0', '0', 1, 0, 'cv1DK1sytvw:APA91bFR1QE78lckLo8TGmLLs__ioVnyCWge73-TkV_oChydmmq9ss-K53mxZqYwNM53bQT_-ZqiJEji5PoMrWhhJTJlVKelDyj1Nj-aOh1wmbRxAkm7BbjnXxkK6shwtBd9apJC4QxO', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 0, '', '', '', '', 'T4CUEIOT', '13-02-2018 18:34:40'),
(230, 'Abhilash Raj', NULL, 'raj.abhilash81@gmail.com', '9015045742', '', '', 'teacher', NULL, 'M', '1990-01-17', '', NULL, NULL, '', '', NULL, '77.0051479', '28.7344761', '', 'Dr Sahib Singh Verma Marg, Kanjhawala Village, Delhi, 110081, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'd97GESgdwLA:APA91bHm0GWPOSS4hGiyxt4UvZXoKkjuI1t6Tc5ZgxdZ9fh_20ghuI3ZrLh7s3d87Zfn_WniE1B99t5WoZHdMfl5VNazMGwMEqDuL_tq3ZzZnCbtRDA4wYPeMfpAmIeysZmyLPClGpAc', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 4, '', '', '', '', 'TZDMFKZM', '13-02-2018 18:55:38'),
(231, NULL, NULL, NULL, '9999574088', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fQDzJBnEqSQ:APA91bGaYygh-_yZRYNEZjwDR1CH3ImK05Hq8HpmJJXr6Ekxg1GmiTXYf3su-I7CPKnbRFf15VbKsAU_xt1lErkd59xPECDSYjySBL3GOAi2FiPXhnPXNSDU5XjzH-bl2KRhDIct66es', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TNPN2JYO', ''),
(232, 'Manoj Saxena', NULL, 'perfectstudycentre123@gmail.co', '7011462277', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '77.098722', '28.702969', '', 'C-78,Sec-1, Behind Durga Mandir, Sir Chotu Ram Marg, Avantika, Sector 1, Rohini, Delhi, 110085, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_232_1518531243.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'fDteFg31uN4:APA91bEzifMtCus3mr0cbIGiAtO26bQZsyR-SkMca-1Cw2x2ip0mIjkw7LC77G-9XIyKq4BnSEPSZ_EbB4Lydz5COvPhC0tfq9TN-BVy80rlAnx_3lwBB1Mlwy6MW7l5W5j2n9qk9ED3', '', '', '', '', '', '', '', NULL, '10+ years', 0, '1', NULL, 3, '', '', '', '', 'TF1JQRZQ', '13-02-2018 19:38:26'),
(233, NULL, NULL, NULL, '9811617558', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'e5LiVehb4aw:APA91bGtBxIsuVEdi0x4um4Pub0B-tvoQYqtHDgc8ewmNhad6z-pyIVcrYgYX_3LZDRJZp63RO4xAF4F4AndjZHpdYfqaOjdeW2s0P2uTVdhygENEV7ZICLC0RMWayji02D24T-OYx9t', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TXGQBSVP', ''),
(234, 'kumar nishant', NULL, 'kumarnishu22@gmail.com', '7838230479', '', '', 'teacher', NULL, 'M', '1991-08-02', '', NULL, NULL, '', '', NULL, NULL, NULL, 'Block E, Param Puri, Uttam Nagar, Delhi, 110059, India', 'Om Vihar, Nawada, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'profile_234_1518544620.jpg', 'I can strongly commit to enhance and support the education of a child. I have worked with students in small groups of 4 to 5 as well as with individual to help develop their basic learning and writing skills. Do contact me for further queries.', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English', NULL, '0', '0', 1, 0, 'fBcKg7HwrPs:APA91bFB9dxhlVsmsUV4KfmppP5swhL4B6nkJ8CVmOERQf2XJWg-AXOm-OwJapNoQb1ZPF3u5AU92usAzM06HJYM5woxhVQMAeMK8ex-11Hdfpt1NS5UwraUun8hRvBSJA-0VyKmS4Rv', '', '', '', '', '', '', '', NULL, '2 years', 0, '1', NULL, 2, '', '', '', '', 'TBKIANUQ', '13-02-2018 22:58:07'),
(235, NULL, NULL, NULL, '9910787177', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'cw3b5XqVN3s:APA91bGmXpmyZ_-o_yMhXQpQsM-WxnyUI8cu-vCPNQcNV9qTqTlMFbvwqtC1LmDwtFhmGFvFzsr0rHFvab1pLWF2G7D67O7KBSng_jOtKG1c-7lcm5znYKvYRaQ-nVWawuPo7wzxehvw', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TIUMVJAX', ''),
(236, 'Anand', NULL, 'anandtripathi009@gmail.com', '9889101340', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, 'Janakpuri, New Delhi, Delhi, India', 'Vikaspuri, New Delhi, Delhi 110018, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 2, 'English, Hindi ', NULL, '0', '0', 0, 0, 'fEPv8GcfPC4:APA91bGXn3BAoZk88Gie-WJ1eDKoeoG3NL9Eaw2owt_KtCnpzVewC4W2eudF5-husae8G_Ce2S8oGp5yCLP3F8c32abEWs-hhBszAbWThQd5BU_mF8DBIGzwKWSaUYC0N5B0Q79VnvCy', '', '', '', '', '', '', '', NULL, '7 years', 0, '1', NULL, 3, 'bc4f90de2187894b772901e2949734de', '088e21e7a9a9216d565b787fa644aa8e', 'e49dcce5d1f5852f306a470e18dd60ae', 'e1490931f8c4c2b34af0defe52d30e02', 'T28VH9F7', '14-02-2018 14:01:43');
INSERT INTO `teachers` (`id`, `first_name`, `last_name`, `email`, `mobile`, `alt_number`, `web_link`, `user_type`, `access_token`, `gender`, `dob`, `password`, `qualification`, `tenure`, `batches_taught`, `student_taught`, `join_as`, `longitude`, `latitude`, `address`, `per_address`, `city`, `zip`, `state`, `country`, `modified_by`, `modified_at`, `created_at`, `tutor_type`, `specialization`, `stream`, `institution_name`, `display_institution_name`, `is_institute`, `is_academic`, `profile_img`, `about_me`, `video_url`, `mission`, `vision`, `inst_lat`, `inst_long`, `current_availblty_lat`, `current_availblty_long`, `contact_by_email`, `contact_by_sms`, `contact_by_call`, `call_start_time`, `call_end_time`, `status`, `is_login`, `is_online`, `user_role`, `language`, `referal_code`, `rating`, `is_anonymus`, `mobile_verify`, `profile_verify`, `fcm`, `device_id`, `type`, `guardian_contact_number`, `relation_ship`, `guardian_email`, `academic_aim`, `passion_for_life`, `last_qualification`, `total_experience`, `total_students`, `adhar_status`, `adhar_img`, `view`, `bank_name`, `account_number`, `ifsc_code`, `account_hold_name`, `referral_code`, `added_date`) VALUES
(237, 'Shivendra Pratap Singh', NULL, 'srservices.net@gmail.com', '8802572566', '', '', 'teacher', NULL, 'M', '1987-04-15', '', NULL, NULL, '', '', NULL, '77.0637835', '28.6208264', '', 'Near New Holy Public Schol, Block A, Uttam Nagar East, Bindapur, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', 'computer classes available', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'Hindi , English', NULL, '0', '0', 0, 0, 'foNI8O2dLj4:APA91bHuVWIhfus-nM8nDM_1ghQlNqqrpYeLl3JmxERwyUTD3hmfJZPA1h8q5dUCRviIEjfzQiA-MEnmR3ui5rJj_WLovnc-NvJUH1EmvECKBsYjMm9VoTtsYK3SkxaJb4aYoT5kBxDl', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 8, '', '', '', '', 'T1OXYUX9', '14-02-2018 14:15:44'),
(238, 'Prince Sir', NULL, 'bholamukul2190@gmail.com', '8860312351', '', '', 'teacher', NULL, 'M', '1992-08-21', '', NULL, NULL, '', '', NULL, NULL, NULL, '', 'Manas Kunj Rd, Vikas Vihar, New Uttam Nagar, Uttam Nagar, Delhi, 110059, India', NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', '', '', '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 1, 'English', NULL, '0', '0', 0, 0, 'dv4Io4HqTGE:APA91bGQC8DPyPVztcSV6je2lDwpLzOIi9jphfCIVlHDOCmfzxebB8xN3jRBRkevIJk-1c0RxTvKisCWyEcOE2apcTJTzjtKxx3E3OFNjka1I3X-aO0kb1hV8Bf2uZGf9vqI4Q81saNN', '', '', '', '', '', '', '', NULL, '8 years', 0, '1', NULL, 13, '', '', '', '', 'TNSYYCH9', '14-02-2018 14:55:57'),
(239, NULL, NULL, NULL, '8700601699', '', '', '', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 2, '', NULL, '0', '0', 0, 0, 'fOAkD9shsOk:APA91bEg6sQAu_vEsPebtFkCX-10xhsv4DZVX8ngqEZfSkkmKIS21vHBXZCexgcMtE01EJTBZpH6hA1XzQXC5URWYO16nOpmNm_LYjej88z4lHkDd87oHug5ppX-rm2h1PPOsnl_PQ8p', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'T0BAIGNE', ''),
(240, 'Konark Gupta', NULL, 'konarksays@gmail.com', '9911567735', '', '', 'teacher', NULL, 'M', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'fAsSBcl8yXI:APA91bGJm-P61Yi5hVkRvpEF8N8foGp385t_0K0RLdK9g_GF8mT1KiWy4Vo1nG_IZPIen1C7hpb9wjRH7L-R7iO3vH0uLBnDsmMLv1zEo41kGUQd63UsYKtIV7YyaGGjUnGW8LIqzmrj', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TZXL1GHI', '14-02-2018 23:17:08'),
(251, 'Jaipur Academy', NULL, 'jaipur@gma.com', '7906398702', '', '', 'Non-Academic', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0765483', '28.62196', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, 0, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 3, '', NULL, '0', '0', 1, 0, 'e8pPgu6gwvg:APA91bFaKextTVMXNO6Oyd-H0C3uvqXp7h6D_FgAuSy7yMBquFJ6-DFFtu9KxXqLOp5mAJDUoTb-f3JTcNzjxbWVuinOQ1txKocK75AA3ujkQFjml9ErnbOd0wwUZ2-Op4xbOTdu_hpE', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'TQXQRMXQ', '20-03-2018 11:07:05'),
(252, 'Football Versus', NULL, 'rrahuli@gmail.com', '9899575155', '', '', 'Non-Academic', NULL, NULL, '0000-00-00', '', NULL, NULL, '', '', NULL, '77.0765469', '28.6219639', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-04 05:34:57', NULL, NULL, '', '', '', '', NULL, 0, NULL, NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 1, 0, 1, 3, '', NULL, '0', '0', 1, 0, 'fvJhEMzD7BQ:APA91bG3vXDOnk30KI5t6zG1yyL4cRuG3kxRV9Kq-pw-9EnFtMbD2x9E3NLjU20XqytF3sVuH6eipFnx2GKe6RyywGiIn9Fm6reOkwwKVVdOYZgUHSXgT3VFZaKf99K6YMkcEcDBhTDd', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 0, '', '', '', '', 'THVZEOYD', '21-03-2018 15:06:30'),
(253, 'Deeksha', NULL, 'deekshagupta@gmail.com', '9004988285', '', '', 'teacher', NULL, 'F', '0000-00-00', '', NULL, NULL, '', '', NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-04-05 06:04:00', NULL, NULL, '', '', '', '', NULL, NULL, 'default.jpg', NULL, NULL, '', '', '', '', '', '', '0', '0', '0', '', '', 0, 0, 0, 1, '', NULL, '0', '0', 0, 0, 'duKT0rSrn_U:APA91bGrZ96-S9tqSJhKB6tBCe6xk6xJqxGfygGi8OEQTCPvrsCP0fr0gb7F7VdpkD6Kv54bo0POiXK9wcog9L51dP59fvapdVecg_TAWe-QWFVYWtDrmbzZ1MHlUV4_Wlxq5pDpdd69', '', '', '', '', '', '', '', NULL, '', 0, NULL, NULL, 1, '', '', '', '', NULL, '04-04-2018 18:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `teachers_assigment`
--

CREATE TABLE `teachers_assigment` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `classname` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_assigment`
--

INSERT INTO `teachers_assigment` (`id`, `teacher_id`, `batch_id`, `type`, `image`, `name`, `classname`, `subject`, `date`, `description`, `status`, `created`) VALUES
(1, 1, 1, 'ppt', 'assigment_1_1_8856.ppt', 'Hello Biddies', 'Gate BT', 'Engineering Mathematics', '2017-12-20', 'What to do or what to not do', 0, '2017-12-20 11:34:52'),
(2, 1, 2, 'pdf', 'assigment_1_1_6796.pdf', 'Treaure Pdf', 'Gate BT', 'Engineering Mathematics', '2017-12-30', 'Have to read it', 0, '2017-12-21 08:38:19'),
(3, 1, 2, 'ppt', 'assigment_1_1_2348.ppt', 'JCc', 'IMU-CET', 'General Knowledge', '2017-12-30', 'JCfjcjf', 0, '2017-12-21 08:40:48'),
(4, 18, 3, 'jpg', 'assigment_18_18_3296.jpg', 'Ghh', 'CLAT ', 'Elementary Mathematics (Numerical Ability)', '2017-12-22', 'Hhh', 0, '2017-12-22 11:22:04'),
(5, 44, 13, 'pdf', 'assigment_44_44_2218.pdf', 'Hello', 'NETWORKING', 'COMPUTER APPLICATION DIPLOMA', '2018-02-21', 'Helooo', 0, '2017-12-26 08:49:07'),
(6, 57, 17, 'doc', 'assigment_57_57_1673.doc', 'Fy ugg Huh', 'CLASS II', 'ENGLISH', '2017-12-28', 'Hccjhjh', 0, '2017-12-28 09:08:04'),
(7, 57, 17, 'jpg', 'assigment_57_57_1176.jpg', 'G', 'CLASS II', 'MATHEMATICS', '2017-12-29', 'Ighh', 0, '2017-12-28 09:08:57'),
(8, 79, 41, 'jpg', 'assigment_79_79_1311.jpg', 'Heloo', 'CLASSES I-V', 'CLASS III', '2018-01-31', 'Dhdhd', 0, '2018-01-15 11:09:27'),
(9, 90, 46, 'pdf', 'assigment_90_90_7770.pdf', 'Hdhbd', 'PERSONALITY DEVELOPMENT', 'PERSONALITY DEVELOPMENT', '2018-01-19', 'Jdjbdh', 0, '2018-01-19 08:31:33'),
(10, 75, 32, 'pptx', 'assigment_75_75_4694.pptx', 'Hxshgxehi', 'IMU-CET', 'General Knowledge', '2018-06-11', 'Kbxejrc', 0, '2018-01-19 12:42:55'),
(11, 75, 32, 'pptx', 'assigment_75_75_9626.pptx', 'Ygyuv', 'CLASS VII', 'COMPUTER', '2018-06-11', 'Cyhy', 0, '2018-01-19 12:47:07'),
(12, 2, 15, 'jpg', 'assigment_2_2_7058.jpg', 'Mathematics', 'JEE Mains', 'Mathematics', '2017-12-29', 'Importamnt questions', 0, '2018-01-21 08:23:06'),
(13, 75, 52, 'pdf', 'assigment_75_75_8581.pdf', 'Ywdydeu', 'CLASS VII', 'GENERAL KNOWLEDGE', '2018-01-19', 'hhwzj', 0, '2018-01-22 09:08:39'),
(14, 75, 52, 'jpeg', 'assigment_75_75_3857.jpeg', 'Bbj', 'CLASS VII', 'GERMAN', '2018-01-19', 'Ohbnji', 0, '2018-01-23 06:52:42'),
(15, 75, 52, 'pdf', 'assigment_75_75_2467.pdf', 'Assignment Testing', 'CLASS VII', 'SANSKRIT', '2018-01-21', 'This is for assignment testing', 0, '2018-01-24 09:15:43'),
(16, 75, 52, 'pdf', 'assigment_75_75_5521.pdf', 'Assignment Testing 2', 'CLASS VII', 'COMPUTER', '2018-01-24', 'This for testing purposes', 0, '2018-01-24 09:21:38'),
(17, 75, 52, 'pdf', 'assigment_75_75_2713.pdf', 'Dc Gf', 'CLASS VII', 'SANSKRIT', '2018-01-19', 'Gbcvnvk', 0, '2018-01-24 09:26:52'),
(18, 75, 52, 'pdf', 'assigment_75_75_9104.pdf', 'Dc Gf', 'CLASS VII', 'SANSKRIT', '2018-01-19', 'Gbcvnvk', 0, '2018-01-24 09:28:19'),
(19, 75, 52, 'pdf', 'assigment_75_75_3135.pdf', 'Dc Gf', 'CLASS VII', 'SANSKRIT', '2018-01-19', 'Gbcvnvk', 0, '2018-01-24 09:28:20'),
(20, 75, 52, 'pdf', 'assigment_75_75_3924.pdf', 'Dc Gf', 'CLASS VII', 'SANSKRIT', '2018-01-19', 'Gbcvnvk', 0, '2018-01-24 09:28:42'),
(21, 75, 52, 'pdf', 'assigment_75_75_3001.pdf', 'Dc Gf', 'CLASS VII', 'SANSKRIT', '2018-01-19', 'Gbcvnvk', 0, '2018-01-24 09:28:50'),
(22, 49, 104, 'pdf', 'assigment_49_49_4061.pdf', 'Xojcic', 'CLASS VI', 'SCIENCE', '2017-12-24', 'Bj,kcgkx', 0, '2018-01-24 12:36:03'),
(23, 87, 116, 'jpg', 'assigment_87_87_7443.jpg', 'Title', 'CLASS VIII', 'MATHEMATICS', '2017-12-25', 'Jdjd', 0, '2018-01-24 12:40:35'),
(24, 49, 104, 'pdf', 'assigment_49_49_6344.pdf', 'Kckf', 'CLASS VI', 'SCIENCE', '2017-12-24', 'Ixkcof', 0, '2018-01-24 13:39:03'),
(25, 9, 118, 'pdf', 'assigment_9_9_3879.pdf', 'What Is Ficket', 'Computer Science  Engineering', '	Computer Programming', '2023-04-28', 'Jfnfnfjdjdndkdkdmdmmdmdmfmfmfmfmfmfmfmfmdmmd', 0, '2018-01-25 06:04:20'),
(26, 107, 66, 'pdf', 'assigment_107_107_3447.pdf', 'Hdhdd', 'Mathematical Hons.', 'Differntial Geometry', '2018-01-25', 'Jdbdbfbb', 0, '2018-01-25 06:23:25'),
(27, 49, 110, 'pptx', 'assigment_49_49_2401.pptx', 'Jfjf', 'CLASS VI', 'SCIENCE', '2017-12-25', 'Kdkgkflghll vuplvlhclch', 0, '2018-01-25 07:14:44'),
(28, 49, 104, 'pptx', 'assigment_49_49_7856.pptx', 'Kfkgdkg', 'CLASS VI', 'MATHEMATICS', '2017-12-25', 'Ktkf', 0, '2018-01-25 07:28:55'),
(29, 49, 104, 'pptx', 'assigment_49_49_8950.pptx', 'Mcmv', 'CLASS VI', 'SCIENCE', '2017-12-25', 'Hohkhkg', 0, '2018-01-25 07:35:59'),
(30, 87, 114, 'jpg', 'assigment_87_87_3573.jpg', 'Nsnsbbsbs', 'CLASS VIII', 'SCIENCE', '2017-12-25', 'Bdbd', 0, '2018-01-25 07:42:47'),
(31, 87, 114, 'jpg', 'assigment_87_87_4663.jpg', 'Nsnsb', 'CLASS VIII', 'SCIENCE', '2017-12-25', 'Dnsnsns', 0, '2018-01-25 07:44:34'),
(32, 87, 114, 'jpg', 'assigment_87_87_9441.jpg', 'Hdhdhd', 'CLASS VIII', 'MATHEMATICS', '2017-12-25', 'Hdjbdjjdjdkdlslsppskejdndndjjdjdjdjdkdjdjdjdjdjdjdjdjdjsjsjdndjkdlsowowowkeuruhrbdbdbdbdhdjdjdjdjjd', 0, '2018-01-25 08:39:14'),
(33, 87, 114, 'jpg', 'assigment_87_87_6082.jpg', 'Ailo', 'NDA exam', 'Mathematics', '2017-12-29', 'Ndndnddndndndnnd', 0, '2018-01-25 10:05:37'),
(34, 87, 114, 'jpg', 'assigment_87_87_4275.jpg', 'BBb', 'CLASS VIII', 'MATHEMATICS', '2017-12-25', 'B\'s d d', 0, '2018-01-25 10:44:34'),
(35, 107, 66, 'pdf', 'assigment_107_107_9802.pdf', 'Hdhddshssles', 'Mathematical Hons.', 'Numerical Analysis ', '2018-01-25', 'Hhdhwqwejsjsjsjshjsmksndbdh', 0, '2018-01-25 11:21:43'),
(36, 74, 64, 'pdf', 'assigment_74_74_1027.pdf', 'Hdhhd', 'CLASS VIII', 'MATHEMATICS', '2018-01-24', 'Udhdhsksksjsjdjjd', 0, '2018-01-25 11:42:49'),
(37, 117, 65, 'jpg', 'assigment_117_117_3919.jpg', 'hji', 'Computer Science  Engineering', 'Computer Organization', '2018-02-01', 'jjj', 0, '2018-01-29 06:19:13'),
(38, 164, 95, 'png', 'assigment_164_164_5750.png', 'Maths', 'CLASSES I-V', 'CLASS I', '2018-02-06', 'Maths assignment', 0, '2018-02-06 09:34:47'),
(39, 241, 158, 'ppt', 'assigment_241_241_2565.ppt', 'Jhh', 'PROGRAMMING LANGUAGE', 'ASP.NET', '2018-02-26', 'Jne', 0, '2018-02-26 10:20:19'),
(40, 241, 158, 'ppt', 'assigment_241_241_7453.ppt', 'Hssn', 'SRMJEEE', 'Chemistry', '2018-02-26', 'Jsnsja', 0, '2018-02-26 10:21:10'),
(41, 241, 158, 'ppt', 'assigment_241_241_4490.ppt', 'Hssn', 'SRMJEEE', 'Chemistry', '2018-02-26', 'Jsnsja', 0, '2018-02-26 10:21:15'),
(42, 176, 204, 'doc', 'assigment_176_176_8813.doc', 'Bs', 'SRMJEEE', 'Select Subject', '2018-02-07', 'Bshs', 0, '2018-03-06 06:20:17'),
(43, 176, 204, 'doc', 'assigment_176_176_7735.doc', 'Bs', 'SRMJEEE', 'Select Subject', '2018-02-07', 'Bshs', 0, '2018-03-06 06:20:18'),
(44, 176, 204, 'pdf', 'assigment_176_176_5834.pdf', 'Hsiwhwkw', 'CLASS IX', 'SCIENCE', '2018-02-06', 'Hauanwiwbwuw', 0, '2018-03-06 06:26:17'),
(45, 176, 204, 'jpg', 'assigment_176_176_6587.jpg', 'Bshwuw', 'CLASS VI', 'ENGLISH', '2018-02-07', 'Haua', 0, '2018-03-06 06:27:14'),
(46, 176, 202, 'doc', 'assigment_176_176_6535.doc', 'Ghh', 'AFMC', 'Chemistry', '2018-02-07', 'Vvhy', 0, '2018-03-06 07:13:04'),
(47, 176, 204, 'jpg', 'assigment_176_176_4352.jpg', 'Bb', 'CLASS VI', 'ENGLISH', '2018-02-06', 'Bh', 0, '2018-03-06 09:04:37'),
(48, 241, 162, 'doc', 'assigment_241_241_9968.doc', 'Cg', 'CLASS X', 'ENGLISH', '2018-03-07', 'Bjdi', 0, '2018-03-12 07:37:16'),
(49, 241, 162, 'doc', 'assigment_241_241_1481.doc', 'Cg', 'CLASS X', 'ENGLISH', '2018-03-07', 'Bjdi', 0, '2018-03-12 07:37:17'),
(50, 241, 162, 'doc', 'assigment_241_241_4229.doc', 'Fh', 'SRMJEEE', 'Mathematics', '2018-03-07', 'Gh', 0, '2018-03-12 07:49:39'),
(51, 241, 162, 'doc', 'assigment_241_241_4653.doc', 'Fh', 'SRMJEEE', 'Mathematics', '2018-03-07', 'Gh', 0, '2018-03-12 07:49:40'),
(52, 241, 161, 'png', 'assigment_241_241_7803.png', 'Hwjmw', 'SRMJEEE', 'Chemistry', '2018-03-09', 'Hwuw', 0, '2018-03-12 09:36:58'),
(53, 241, 162, 'jpg', 'assigment_241_241_8726.jpg', 'Gyy', 'CLASS X', 'ENGLISH', '2018-03-07', 'Vgh', 0, '2018-03-12 09:39:06'),
(54, 241, 162, 'jpg', 'assigment_241_241_9064.jpg', 'N', 'NDA exam', 'General Aptitude', '2018-03-07', 'Ghhuii', 0, '2018-03-12 09:41:21'),
(55, 75, 160, 'pdf', 'assigment_75_75_8564.pdf', 'Ixtfiifif', 'CLASS VII', 'HINDI', '2018-03-07', 'Uguucgiihih', 0, '2018-03-12 09:52:10'),
(56, 75, 160, 'pdf', 'assigment_75_75_7377.pdf', 'Bdbdb', 'CLASS VII', 'HINDI', '2018-03-12', 'Bdbdjd', 0, '2018-03-12 09:52:52'),
(57, 241, 144, 'jpg', 'assigment_241_241_8945.jpg', 'Chh', 'SRMJEEE', 'Mathematics', '2018-03-14', 'Fftyuiiy', 0, '2018-03-14 10:49:53'),
(58, 241, 144, 'jpg', 'assigment_241_241_1993.jpg', 'Chh', 'SRMJEEE', 'Mathematics', '2018-03-14', 'Fftyuiiy', 0, '2018-03-14 10:49:56'),
(59, 241, 144, 'doc', 'assigment_241_241_5709.doc', 'Fyy', 'NEGOTIATION', 'NEGOTIATION', '2018-03-14', 'Vh', 0, '2018-03-14 10:51:04'),
(60, 241, 144, 'jpeg', 'assigment_241_241_5885.jpeg', 'Uuu', 'MAT ', 'Intelligence And Critical Reasoning', '2018-03-14', 'Htft', 0, '2018-03-14 10:51:43'),
(61, 241, 165, 'doc', 'assigment_241_241_9989.doc', 'Gsjw', 'JEE Advanced', 'Mathematics', '2018-03-14', 'Jshs', 0, '2018-03-15 09:58:56'),
(62, 241, 166, 'png', 'assigment_241_241_1009.png', 'Hshs', 'SRMJEEE', 'Chemistry', '2018-03-17', 'Hsyshs', 0, '2018-03-16 06:52:47'),
(63, 241, 167, 'jpg', 'assigment_241_241_4069.jpg', 'Yee', 'JEE Advanced', 'Mathematics', '2018-02-16', 'Bhw', 0, '2018-03-16 09:07:53'),
(64, 249, 168, 'doc', 'assigment_249_249_5974.doc', 'Ve', 'APPLICATION SOFTWARE', 'COMPUTER BASICS', '2018-02-16', 'Bjjbj', 0, '2018-03-16 09:42:54'),
(65, 250, 210, 'doc', 'assigment_250_250_5101.doc', 'Sbb', 'WBJEE JEHOM', 'Elementary Mathematics', '2018-03-16', 'Ggnn', 0, '2018-03-16 11:35:41');

-- --------------------------------------------------------

--
-- Table structure for table `teachers_events`
--

CREATE TABLE `teachers_events` (
  `id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `venue` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `joining_fees` double NOT NULL,
  `status` enum('0','1','2') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_events`
--

INSERT INTO `teachers_events` (`id`, `user_type`, `teacher_id`, `subject_id`, `subject_name`, `heading`, `image`, `description`, `venue`, `class_id`, `class_name`, `date_from`, `date_to`, `time_from`, `time_to`, `joining_fees`, `status`, `created_at`) VALUES
(23, '', '90', 0, 'maths', 'Hdhhdh', '', 'Hhhhddb', 'Janakpuri, New Delhi, Delhi, India', '', '', '2018-01-19', '2021-04-23', '00:00:00', '00:00:00', 500, '1', '0000-00-00 00:00:00'),
(12, '', '79', 0, 'english', 'Gyrub', 'event_79_16-Jan-2018_17-Jan-2018.jpg', 'Awesme', 'Sector 22 Block G Road, Sector 22, Noida, Uttar Pradesh 201301, India', '', '', '2018-01-16', '2018-04-04', '15:00:00', '21:00:00', 500, '1', '0000-00-00 00:00:00'),
(3, '', '12', 1092, 'Vector Analysis', 'Ndnx', '', 'Nxnddddndb', 'Jdnd', '93', 'Mathematical Hons.', '2018-01-25', '2018-04-05', '09:00:00', '21:00:00', 50, '1', '0000-00-00 00:00:00'),
(22, '', '79', 0, 'english', 'Gyrub', 'event_79_17-Mar-2018_22-Jun-2022.jpg', 'Gyrub fest', 'Vikaspuri, New Delhi, Delhi 110018, India', '', '', '2018-03-17', '2022-06-22', '18:00:00', '22:00:00', 100, '1', '0000-00-00 00:00:00'),
(5, '', '23', 1399, 'Cycling', 'Hdhdh', '', 'Bbdbdbdbbd', 'Hdhd', '185', '', '2017-12-30', '2018-01-25', '15:00:00', '22:00:00', 10, '0', '0000-00-00 00:00:00'),
(7, '', '30', 79, 'BIOLOGY', 'Crash Course Bio', '', 'Aimed towards board examination 2018', 'Dehradun', '21', 'CLASS XII(MEDICAL)', '2018-01-05', '2018-01-31', '05:00:00', '18:00:00', 1500, '1', '0000-00-00 00:00:00'),
(8, '', '45', 1392, 'Football', 'hxhdh', '', 'djbdbd', 'bdbd', '185', '', '2018-01-26', '2018-02-23', '09:00:00', '18:00:00', 500, '1', '0000-00-00 00:00:00'),
(43, '', '176', 0, 'health', 'Motivational', 'event_176_13-Feb-2018_17-Feb-2018.jpg', 'Hhhh', 'New Delhi, Delhi, India', '', '', '2018-02-13', '2018-02-17', '05:30:00', '05:10:00', 200, '1', '0000-00-00 00:00:00'),
(44, '', '176', 0, 'dft', 'Nkkk4*¢€', '', 'Fgtys', 'Mumbai, Maharashtra, India', '', '', '2018-02-14', '2018-02-17', '03:14:00', '02:25:00', 82, '0', '0000-00-00 00:00:00'),
(10, '', '30', 79, 'BIOLOGY', 'Test Series Board 2018', '', 'Test Series aimed to score exceptionally well in board exam. Test Series along with discussion of questions.', 'Dehradun', '21', 'CLASS XII(MEDICAL)', '2018-01-10', '2018-01-31', '00:00:00', '00:00:00', 1500, '2', '0000-00-00 00:00:00'),
(41, '', '136', 0, 'maths', 'Hellooo', '', 'Jdd', 'Janakpuri, New Delhi, Delhi, India', '', '', '2002-02-08', '2007-09-03', '00:00:00', '00:00:00', 100, '0', '0000-00-00 00:00:00'),
(29, '', '98', 0, 'bdhhd', 'Jjd', '', 'Jdjhhhdjjzskkkdd', 'Allahabad, Uttar Pradesh, India', '', '', '2018-01-22', '2019-01-22', '00:00:00', '16:00:00', 0, '0', '0000-00-00 00:00:00'),
(30, '', '98', 0, 'hbdh', 'Djddh', '', 'Bdbdbdbddbdbdbhjsssjsjsjsjsjsjsjsjsjsjsjhsjsjssj', 'Allahabad, Uttar Pradesh, India', '', '', '2018-01-22', '2021-01-22', '03:00:00', '21:00:00', 150, '0', '0000-00-00 00:00:00'),
(39, '', '164', 0, 'maths events', 'Grub Fest', 'event_164_06-Feb-2018_06-Feb-2019.jpg', 'How to inhabitated that is', 'CGO Complex, Pragati Vihar, New Delhi, Delhi 110003, India', '', '', '2018-02-06', '2019-02-06', '00:00:00', '04:00:00', 500, '1', '0000-00-00 00:00:00'),
(33, '', '107', 0, 'hdhdhd', 'Hdhhd', '', 'Bxbxbxhdb', 'Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '', '2018-01-23', '2019-01-23', '00:00:00', '16:00:00', 10, '0', '0000-00-00 00:00:00'),
(42, '', '136', 0, 'bdb', 'Dhd', '', 'Dhhddh', 'Janakpuri, New Delhi, Delhi, India', '', '', '2012-09-03', '2016-02-08', '00:00:00', '00:00:00', 23, '0', '0000-00-00 00:00:00'),
(38, '', '83', 0, 'bsjssjsh', 'Photo Shop', '', 'Bsjsjsn\n\nJzjsjz', 'Delhi, India', '', '', '2018-02-06', '2018-02-08', '03:00:00', '00:00:00', 979449, '1', '0000-00-00 00:00:00'),
(40, '', '176', 0, 'java', 'Java Session', '', 'Gggh', 'Embassy Tech Zone Block 1.3 Congo Plot 03, Phase 2, Hinjawadi Rajiv Gandhi Infotech Park, Hinjawadi, Hinjawadi Phase II, Hinjewadi Rajiv Gandhi Infotech Park, Hinjawadi, Pune, Maharashtra 411057, India', '', '', '2018-02-09', '2018-02-09', '02:00:00', '04:00:00', 3000, '0', '0000-00-00 00:00:00'),
(45, '', '241', 0, 'yh', 'Tg', '', 'Bhh', 'H.G. 357, Jaipuria Rd, Rail Bazar, Mirpur, Kanpur, Uttar Pradesh 208004, India', '', '', '2018-02-20', '2018-02-21', '01:00:00', '05:00:00', 20, '1', '0000-00-00 00:00:00'),
(46, '', '75', 0, 'bh', 'Yji', '', 'Hjj jn', 'Tilak Nagar, New Delhi, Delhi 110018, India', '', '', '2018-02-21', '2018-02-21', '00:00:00', '04:00:00', 66, '0', '0000-00-00 00:00:00'),
(47, '', '241', 0, 'hsb', 'Gab', '', 'Hwywn', 'B.Shettigeri, Karnataka 571218, India', '', '', '2018-02-28', '2018-03-01', '00:00:00', '00:04:00', 60, '1', '0000-00-00 00:00:00'),
(48, '', '176', 0, 'bh', 'Hsu', '', 'He', 'g s road, GS Rd, Six Mile, Guwahati, Assam 781036, India', '', '', '2018-03-03', '2018-03-05', '00:00:00', '00:00:00', 3, '0', '0000-00-00 00:00:00'),
(49, '', '241', 0, 'kkm', 'Jwkw', '', 'Kk', 'Hijli Station Rd, IIT Kharagpur, Kharagpur, West Bengal 721302, India', '', '', '2018-03-09', '2018-03-10', '00:00:00', '00:00:00', 69, '1', '0000-00-00 00:00:00'),
(50, '', '241', 0, 'cf', 'Eheb', '', 'Mksjd', '37,First Cross,First Floor,, venkata Nagar, Kamaraj Nagar, Puducherry, 605011, India', '', '', '2018-03-13', '2018-03-14', '00:02:00', '00:00:00', 1, '0', '0000-00-00 00:00:00'),
(51, '', '241', 0, 'ud', 'Df', '', 'Ns', 'DF-8, 2nd Avenue, DF Block, Sector 1, Salt Lake City, Kolkata, West Bengal 700064, India', '', '', '2018-03-12', '2018-03-13', '00:00:00', '00:00:00', 1, '1', '0000-00-00 00:00:00'),
(52, '', '241', 0, 'hwi', 'Bsjks', '', 'Bsj', 'Mumbai, Maharashtra, India', '', '', '2018-03-15', '2018-03-16', '00:00:00', '00:00:00', 0, '1', '0000-00-00 00:00:00'),
(53, '', '249', 0, 'cvnj', 'Vvhhh', '', 'Hdid', 'Mumbai Central, Mumbai, Maharashtra, India', '', '', '2018-03-16', '2018-03-17', '00:00:00', '00:00:00', 32, '1', '0000-00-00 00:00:00'),
(54, '', '250', 0, 'hrr', 'Heje', 'event_250_16-Mar-2018_17-Mar-2018.jpg', 'Ifjr', 'Block A, Old Seemapuri, Delhi, Uttar Pradesh 201005, India', '', '', '2018-03-16', '2018-03-17', '00:00:00', '00:00:00', 1, '0', '0000-00-00 00:00:00'),
(55, '', '253', 0, 'jjj', 'Hhhh', 'event_253_05-Apr-2018_06-Apr-2018.jpg', 'Heiwbe', 'Mumbai Central, Mumbai, Maharashtra, India', '', '', '2018-04-05', '2018-04-06', '12:00:00', '14:00:00', 1, '1', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `teachers_notes`
--

CREATE TABLE `teachers_notes` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `datetime` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers_notes`
--

INSERT INTO `teachers_notes` (`id`, `teacher_id`, `name`, `comment`, `datetime`) VALUES
(3, 136, '', '', '2018-02-08 10:01:08'),
(4, 136, '', '', '2018-02-08 11:17:43'),
(5, 136, '', '', '2018-02-08 11:24:53'),
(6, 136, '', '', '2018-02-08 11:27:11'),
(7, 136, '', '', '2018-02-08 11:28:20'),
(8, 184, '', 'give follow up to this teacher', '2018-02-08 12:40:47'),
(9, 74, 'amit k', 'ijust called', '2018-02-09 06:39:12'),
(10, 189, 'Rahul', 'followup given\r\npositive feedback', '2018-02-09 08:07:20'),
(11, 188, 'rahul', 'follow up given\r\nclass plus approached them\r\nattendance sent to parents', '2018-02-09 08:29:47'),
(12, 186, 'swetank', 'followup given to her,she will complete profile soon and after some time again followup will be given', '2018-02-09 09:18:13'),
(13, 185, 'gaurav', 'call her again at 6 pm on 09/02/2018', '2018-02-09 09:22:23'),
(14, 182, 'gaurav', 'folloup given to teacher,she will soon make her session', '2018-02-09 09:29:53'),
(15, 181, 'gaurav', 'followup given. he will complete his profile and make batch.', '2018-02-09 09:34:07'),
(16, 179, 'gaurav', 'call him again after 1.30 hours.', '2018-02-09 09:37:11'),
(17, 175, 'gaurav', 'followup given. she will create her sessions.', '2018-02-09 09:50:39'),
(18, 190, 'swetank', 'he cal to taktii from any reference and he ask that taktii giving place to teach student ????', '2018-02-09 10:22:42'),
(19, 173, 'gaurav', 'followup given he will complete profile when free', '2018-02-09 10:43:39'),
(20, 169, 'gaurav', 'followup given soon he will complete his profile', '2018-02-09 10:48:23'),
(21, 194, 'gaurav', 'followup given', '2018-02-09 10:52:47'),
(22, 168, 'gaurav', 'followup given and she ask to share app to her more friends', '2018-02-09 10:57:46'),
(23, 9, '', '', '2018-02-09 11:19:45'),
(24, 74, 'ssdf', 'sdf', '2018-02-09 11:21:17'),
(25, 74, 'asd', 'asd', '2018-02-09 16:53:49'),
(26, 91, 'satyender pal', 'Delete This ID... Already made account with another number.', '2018-02-09 17:21:44'),
(27, 200, 'GAURAV', 'LIVE THIS PROFILE AS ACC TO MR.RAJAN', '2018-02-09 18:37:08'),
(28, 209, 'Rahul', 'follow up given.\r\nteacher will complete her profile soon', '2018-02-11 13:19:22'),
(29, 70, 'Rahul', 'follow up given. address issues, reconnect after 2 days for intro video.', '2018-02-11 13:34:34'),
(30, 207, 'Rahul', 'geo locating problem.profile competition on her own', '2018-02-11 13:55:48'),
(31, 138, 'Amit K', 'MAking profile live as said by Ashish (Manu)', '2018-02-13 15:40:33');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_activity_batch`
--

CREATE TABLE `teacher_activity_batch` (
  `id` int(11) NOT NULL,
  `academy_id` varchar(255) NOT NULL,
  `activity_name` varchar(255) NOT NULL,
  `activity_id` varchar(255) NOT NULL,
  `batch_date` varchar(255) NOT NULL,
  `batch_start_time` varchar(255) NOT NULL,
  `batch_end_time` varchar(255) NOT NULL,
  `batch_level` varchar(255) NOT NULL,
  `batch_name` varchar(255) NOT NULL,
  `batch_status` varchar(255) NOT NULL,
  `coach_id` varchar(255) NOT NULL,
  `coach_name` varchar(255) NOT NULL,
  `working_days` varchar(255) NOT NULL,
  `trails` varchar(255) NOT NULL,
  `open` enum('0','1') NOT NULL DEFAULT '1',
  `added` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_activity_batch`
--

INSERT INTO `teacher_activity_batch` (`id`, `academy_id`, `activity_name`, `activity_id`, `batch_date`, `batch_start_time`, `batch_end_time`, `batch_level`, `batch_name`, `batch_status`, `coach_id`, `coach_name`, `working_days`, `trails`, `open`, `added`) VALUES
(40, '251', 'Basket ball', '1393', '2018-3-21', '12:00 AM', '04:00 AM', 'Beginner', 'Baseball 2', 'Running', '122,123,122,123', 'Billu Bawali,Ratan Tata,Billu Bawali,Ratan Tata', 'Sunday:false,Monday:false,Tuesday:true,Wednesday:false,Thursday:false,Friday:true,Saturday:true', '1', '1', '3/21/2018  10:48:18 AM'),
(42, '252', 'Football', '1392', '2018-3-19', '05:00 pm', '08:00 pm', 'Intermediate', 'Ronaldo', 'Running', '124', 'Ram', 'Sunday:true,Monday:false,Tuesday:false,Wednesday:false,Thursday:false,Friday:false,Saturday:true', '1', '1', '3/21/2018  3:10:42 PM'),
(38, '251', 'Basket ball', '1393', '2018-3-20', '12:00 AM', '04:00 PM', 'Intermediate', 'Xmxgkg', 'Upcoming', '122', 'Billu Bawali', 'Sunday:false,Monday:false,Tuesday:true,Wednesday:true,Thursday:true,Friday:false,Saturday:true', '6', '0', '3/20/2018  6:41:24 PM'),
(39, '251', 'Basket ball', '1393', '2020-3-22', '12:00 PM', '02:00 PM', 'Expert', 'Xmxgkg', 'Upcoming', '122', 'Billu Bawali', 'Sunday:true,Monday:true,Tuesday:true,Wednesday:true,Thursday:false,Friday:false,Saturday:true', '6', '0', '3/20/2018  6:41:53 PM'),
(41, '251', 'Basket ball', '1393', '2018-3-21', '12:00 AM', '01:00 AM', 'Beginner', 'BB 3', 'Upcoming', '122,123', 'Billu Bawali,Ratan Tata', 'Sunday:false,Monday:false,Tuesday:false,Wednesday:false,Thursday:false,Friday:true,Saturday:true', '5', '1', '3/21/2018  10:52:45 AM');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_activity_details`
--

CREATE TABLE `teacher_activity_details` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `desc_act` text NOT NULL,
  `coach_list` varchar(255) NOT NULL,
  `age_group` varchar(255) NOT NULL,
  `plans_list` varchar(255) NOT NULL,
  `created` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_activity_details`
--

INSERT INTO `teacher_activity_details` (`id`, `teacher_id`, `class_id`, `class_name`, `subject_id`, `subject_name`, `desc_act`, `coach_list`, `age_group`, `plans_list`, `created`) VALUES
(1, '244', '185', 'sports', '1392', 'Football', 'Hjj', '104,111,113', '10 to 15,20 to 25', '40, 39', '2018-02-17 17:49:20'),
(2, '244', '185', 'sports', '1393', 'Basket ball', '', '', '', '', '2018-02-17 17:49:20'),
(3, '244', '185', 'sports', '1391', 'Cricket', 'Bdbdb', '111', '16 to 19', '41', '2018-02-17 17:49:20'),
(4, '244', '185', 'sports', '1395', 'Base Ball', '', '', '', '', '2018-03-05 14:15:16'),
(5, '244', '185', 'sports', '1398', 'Judo', '', '', '', '', '2018-03-05 14:15:16'),
(6, '245', '185', 'sports', '1890', 'Skating', '', '', '', '', '2018-03-06 11:03:03'),
(7, '245', '185', 'sports', '1889', 'Ice Hockey', '', '', '', '', '2018-03-06 11:03:03'),
(8, '245', '185', 'sports', '1888', 'Yoga', '', '', '', '', '2018-03-06 11:03:03'),
(9, '247', '185', 'sports', '1391', 'Cricket', '', '', '', '', '2018-03-09 12:15:06'),
(10, '247', '185', 'sports', '1395', 'Base Ball', '', '', '', '', '2018-03-09 12:15:06'),
(11, '247', '185', 'sports', '1394', 'Gymnastics', '', '', '', '', '2018-03-09 12:15:06'),
(12, '251', '185', 'sports', '1391', 'Cricket', '', '', '', '', '2018-03-20 11:07:09'),
(13, '251', '185', 'sports', '1392', 'Football', '', '', '', '', '2018-03-20 11:07:09'),
(14, '251', '185', 'sports', '1393', 'Basket ball', 'Cnckkvkvkv', '122', '23 to 45', '91', '2018-03-20 11:07:09'),
(15, '252', '185', 'sports', '1392', 'Football', '', '', '', '', '2018-03-21 15:06:48'),
(16, '252', '185', 'sports', '1391', 'Cricket', '', '', '', '', '2018-03-21 15:06:48'),
(17, '252', '185', 'sports', '1393', 'Basket ball', '', '', '', '', '2018-03-21 15:06:48'),
(18, '251', '185', 'sports', '1394', 'Gymnastics', '', '', '', '72, 75', '2018-03-29 11:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_activity_plans`
--

CREATE TABLE `teacher_activity_plans` (
  `id` int(11) NOT NULL,
  `academy_id` varchar(255) NOT NULL,
  `activity` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `plan_duration` varchar(255) NOT NULL,
  `plan_amount` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `added` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_activity_plans`
--

INSERT INTO `teacher_activity_plans` (`id`, `academy_id`, `activity`, `title`, `plan_duration`, `plan_amount`, `description`, `added`) VALUES
(87, '251', '18|1394|Gymnastics|http://taktii.com/etc/api/non-academy/Gymnastics.jpg|true,14|1393|Basket ball|http://taktii.com/etc/api/non-academy/Basket-ball.jpg|true,13|1392|Football|http://taktii.com/etc/api/non-academy/Football.jpg|false,12|1391|Cricket|http://taktii.com/etc/api/non-academy/Cricket.jpg|false', 'Hxfh', '1', '555', 'Drg', '4/4/2018  11:54:31 AM'),
(86, '251', '', '', '17', '9', 'Bdbdbb', '2018-04-04 11:51:28'),
(85, '251', '', '', '9', '9494', 'Bsbs', '2018-04-04 11:51:14'),
(84, '251', '', '', '1', '1', 'Xbxbxjj', '2018-04-04 11:49:52'),
(83, '251', '', '', '1', '94', 'Bdbdjd Hshdhd J', '2018-04-04 11:47:40'),
(82, '251', '', '', '3', '1', 'Bdbdjdjd Jdjdj Jdnd Dj', '2018-04-04 11:26:49'),
(78, '251', '', '', '25', '668', 'Vbj', '2018-04-03 14:33:44'),
(79, '251', '', '', '88', '66', 'Ghh', '2018-04-03 14:39:27'),
(80, '251', '', '', '12', '6989', 'Chhh', '2018-04-03 14:40:34'),
(81, '251', '', '', '1', '1', 'Bdhdjd Jdndndn', '2018-04-04 11:25:21'),
(76, '251', '', '', '12', '3000', 'Bxjxkk', '2018-03-29 11:48:35'),
(77, '251', '', '', '98', '988986', 'Dhfhfhhhf Vjgkvhgjvj', '2018-04-03 14:10:27'),
(73, '251', '14|1393|Basket ball|http://taktii.com/etc/api/non-academy/Basket-ball.jpg|true,13|1392|Football|http://taktii.com/etc/api/non-academy/Football.jpg|false,12|1391|Cricket|http://taktii.com/etc/api/non-academy/Cricket.jpg|false', 'Lalalala jjbjb', '53', '9494966686', 'Bdbdb. Ndndjcuuvviih', '3/26/2018  12:22:22 PM'),
(74, '251', '14|1393|Basket ball|http://taktii.com/etc/api/non-academy/Basket-ball.jpg|true,13|1392|Football|http://taktii.com/etc/api/non-academy/Football.jpg|false,12|1391|Cricket|http://taktii.com/etc/api/non-academy/Cricket.jpg|false', 'Nccjc', '23', '8686868', 'B Xghckvkh', '3/23/2018  10:38:25 AM'),
(75, '251', '14|1393|Basket ball|http://taktii.com/etc/api/non-academy/Basket-ball.jpg|true,13|1392|Football|http://taktii.com/etc/api/non-academy/Football.jpg|false,12|1391|Cricket|http://taktii.com/etc/api/non-academy/Cricket.jpg|true', 'Lalalala', '3', '94949', 'Bdbdb. Ndndjhcjcvjivjv', '3/23/2018  10:38:34 AM'),
(72, '251', '14|1393|Basket ball|http://taktii.com/etc/api/non-academy/Basket-ball.jpg|true,13|1392|Football|http://taktii.com/etc/api/non-academy/Football.jpg|true,12|1391|Cricket|http://taktii.com/etc/api/non-academy/Cricket.jpg|false', 'Gdhhdh', '39', '949464', 'Vsvs', '3/23/2018  10:32:23 AM'),
(88, '251', '', '', '89', '9798', 'Bxdndn', '2018-04-04 12:04:50'),
(89, '251', '', '', '1', '1', 'Jccjcj', '2018-04-04 12:19:14'),
(90, '251', '', '', '1', '898868', 'Ajfcjf', '2018-04-04 12:19:24'),
(91, '251', '', '', '1', '1', 'Cnjvkvkv', '2018-04-04 12:22:47');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_certificates`
--

CREATE TABLE `teacher_certificates` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL,
  `image` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_degree_details`
--

CREATE TABLE `teacher_degree_details` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `degree_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `degree_img` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_degree_details`
--

INSERT INTO `teacher_degree_details` (`id`, `teacher_id`, `degree_name`, `description`, `degree_img`, `created`) VALUES
(1, 1, 'bachelor of technology', 'passed in 2016', 'degree_1_.jpg', '2017-12-20 06:56:45'),
(2, 2, 'B.tech', 'ip university', '', '2017-12-20 09:07:07'),
(5, 12, 'bachelor of science', 'batch 2009', 'degree_12_.jpg', '2017-12-22 07:06:30'),
(6, 18, 'b.tech', 'from iit kharagpur', 'degree_18_.jpg', '2017-12-22 09:24:20'),
(8, 54, 'M.Sc.', 'Biotechnology', '', '2017-12-22 12:13:00'),
(9, 15, 'b.tech', 'technology lover', '', '2017-12-22 17:24:49'),
(11, 26, 'B.com,M.com,CA-CPT, BASIC OF COMPUTER.', '', '', '2017-12-23 10:42:55'),
(12, 24, 'M.com, B.ed', '', '', '2017-12-23 10:58:25'),
(13, 28, 'PG IN MATHEMATICS B.Ed', '', '', '2017-12-23 11:57:05'),
(14, 30, 'Phd', 'Biology', '', '2017-12-24 09:02:08'),
(15, 29, 'M.Sc,MCA,M.Tech', 'Maths and Computer Science', '', '2017-12-24 09:05:54'),
(16, 31, '12th', 'like', '', '2017-12-24 12:47:14'),
(17, 19, 'music', 'degree from luknow sangeet vidhalaya', '', '2017-12-24 15:46:16'),
(18, 38, 'b.sc', 'bacheor of science', '', '2017-12-26 06:11:45'),
(19, 42, 'btech', 'btech in mechanical engg', '', '2017-12-26 07:44:08'),
(20, 44, 'bachelor of techoogy', 'iit-k', '', '2017-12-26 08:46:33'),
(21, 46, 'M.sc', 'microbiology', '', '2017-12-26 12:52:33'),
(22, 58, 'b.tech', 'bachelors', '', '2017-12-28 06:06:20'),
(23, 59, 'b.tech', '', '', '2017-12-28 06:19:07'),
(24, 57, 'fufuv', 'fjhcjv', 'degree_57_.jpg', '2017-12-28 08:43:29'),
(26, 60, 'hdhhd', 'hdh', '', '2017-12-29 06:38:41'),
(28, 39, 'ba', '2015', 'degree_39_.jpg', '2018-01-01 10:25:21'),
(29, 66, 'B.tech', '', '', '2018-01-02 08:38:28'),
(32, 71, 'B Tech', 'Faculty of Engineering and technology, Agra College, Agra', 'degree__.jpg', '2018-01-04 09:39:40'),
(33, 43, 'dukxizzgoz', 'yimxydhkgk', '', '2018-01-04 12:07:37'),
(36, 75, 'B tech', 'FET Agra College Agra', 'degree__5046.jpg', '2018-01-09 10:50:10'),
(37, 75, 'yydyf', 'ett', 'degree_75_.jpg', '2018-01-09 03:50:46'),
(38, 75, 'gyyv', 'ggii', 'degree_75_.jpg', '2018-01-09 09:20:59'),
(40, 74, 'Dhbvv', 'fhh', 'degree_74_.jpg', '2018-01-09 09:30:25'),
(41, 74, 'Hhh', 'hjh', 'degree_74_954.jpg', '2018-01-09 09:31:38'),
(42, 75, 'fycuu', 'vjoo', 'degree_75_6638.jpg', '2018-01-09 10:19:35'),
(43, 75, 'sann', 'guu', 'degree_75_3286.jpg', '2018-01-09 10:24:38'),
(44, 75, 'gjjj', 'vjii', 'degree_75_9749.jpg', '2018-01-09 10:27:09'),
(45, 75, 'hdhd', 'ndjdjd', '', '2018-01-09 10:29:45'),
(46, 75, 'yfyf', 'nfjfj', '', '2018-01-09 10:32:26'),
(47, 75, 'uzfuifz', 'jrzjdjrx', 'degree_75_651.jpg', '2018-01-09 10:36:01'),
(48, 75, 'n,jbxbdjdj', 'yshshsh', 'degree__4175.jpg', '2018-01-09 10:39:52'),
(49, 75, 'niih', 'vhuy', 'degree__5962.jpg', '2018-01-09 10:42:36'),
(50, 75, 'bjc', 'vdjd', '', '2018-01-09 10:44:49'),
(51, 75, 'hhsjjsjn', 'hshshjsj', 'degree_75_5473.jpg', '2018-01-09 10:45:11'),
(52, 75, 'yvgugy', 'hhhi', 'degree_75_3565.jpg', '2018-01-09 10:49:45'),
(53, 75, 'gyuuu', 'g7h', '', '2018-01-09 11:02:24'),
(54, 80, 'mba', 'mgu', '', '2018-01-10 06:26:05'),
(55, 61, 'b.tech', 'computer science', '', '2018-01-10 07:35:26'),
(57, 9, 'bachelor', 'aktu', 'degree_9_3944.jpg', '2018-01-11 06:33:58'),
(58, 9, 'bacelor', 'kktu', 'degree__6979.jpg', '2018-01-11 06:36:26'),
(59, 49, 'B Tech', 'FET Agra College Agra', 'degree_49_6266.jpg', '2018-01-12 05:23:57'),
(60, 83, 'masters in communication', 'aktu', '', '2018-01-16 12:36:15'),
(61, 88, 'M.Tech', 'IIT Delhi', '', '2018-01-17 03:22:14'),
(80, 90, 'bacheore of pharmact', 'aktu', '', '2018-01-19 07:14:26'),
(81, 90, 'hdhhhs', 'jdjdhdh', '', '2018-01-19 12:45:26'),
(82, 98, 'hdhhd', 'hdhdh', '', '2018-01-20 09:15:30'),
(83, 99, 'bachelor of technology', 'uptu', '', '2018-01-20 10:36:30'),
(84, 99, 'ba', 'du', '', '2018-01-21 08:34:33'),
(85, 105, 'bachelor of technology', 'kurukshetra university', '', '2018-01-21 19:15:28'),
(86, 134, 'Ph.D', 'Biotechnology', '', '2018-01-22 03:57:19'),
(87, 87, 'B. Tech', 'lovely professional university', 'degree_87_3516.jpg', '2018-01-23 11:23:38'),
(88, 107, 'bacheor of techology', 'aktu', '', '2018-01-24 05:31:12'),
(89, 117, 'b.tech', 'ggsipu', '', '2018-01-24 19:43:47'),
(90, 75, 'bhnjjhn', 'hhbhu', 'degree_75_1097.jpg', '2018-01-30 05:47:06'),
(91, 135, 'B.com', 'DU', '', '2018-01-30 08:16:56'),
(93, 171, 'Bachelor of science', 'I GNOU', '', '2018-02-06 13:25:00'),
(94, 175, 'JBT', 'scert', '', '2018-02-08 04:30:42'),
(95, 174, 'b.tech', 'ip university', '', '2018-02-08 05:14:25'),
(96, 155, 'PH. D', 'uttarkhand technical University', '', '2018-02-08 09:07:11'),
(97, 180, 'Msc ,Mphil', 'Allahabad university', '', '2018-02-08 11:16:42'),
(98, 183, 'B.Tech', 'GGSIPU', '', '2018-02-08 11:34:46'),
(99, 184, 'B Sc', 'Pune University', '', '2018-02-08 12:26:17'),
(100, 136, 'bacheor of technology', 'UPTU', '', '2018-02-09 06:37:00'),
(101, 189, 'b. tech (IT)', 'mdu, rohtak', '', '2018-02-09 07:18:34'),
(102, 182, 'B.a B.e.d', 'mdu', '', '2018-02-09 08:05:57'),
(108, 192, 'J.B.T', 'SCERT', '', '2018-02-09 10:02:57'),
(109, 193, 'M. A polity', 'ignou', '', '2018-02-09 10:14:08'),
(110, 194, 'B.com , M.com, CA(inter)', 'Delhi university', '', '2018-02-09 10:47:15'),
(111, 194, 'B.com', 'Delhi University', '', '2018-02-09 10:47:38'),
(112, 194, 'M.com', 'MGU', '', '2018-02-09 10:47:55'),
(113, 194, 'CA(inter)', 'ICAI', '', '2018-02-09 10:48:20'),
(114, 144, 'M. Phil Economics', 'M. D. University', '', '2018-02-09 12:18:24'),
(115, 198, 'b.ed', 'indraprastha University', '', '2018-02-09 12:52:32'),
(116, 200, 'B. Tech', 'V. T University', '', '2018-02-09 13:00:15'),
(117, 204, 'D.EL.ED', 'I.P.university', '', '2018-02-09 18:26:16'),
(118, 209, 'electronics and communication engineering plus BEd in maths and science', 'institute of engineers indua', '', '2018-02-11 08:07:52'),
(119, 70, 'Highschool,Inter,B.com  And  NTT completed', 'Highschool and Inter by ICSE and ISC Board B.com by Lucknow university and NTT by UNICEF', '', '2018-02-11 08:13:54'),
(120, 154, 'M.Tech', 'IP university', '', '2018-02-12 08:12:59'),
(121, 154, 'B.tech', 'IP university', '', '2018-02-12 08:13:26'),
(122, 152, 'M.A', 'M.D.U', '', '2018-02-12 11:05:25'),
(123, 213, 'M.Com', 'Delhi University', '', '2018-02-12 11:35:06'),
(125, 138, 'M.A. in Mathematics', 'H.p.university', '', '2018-02-13 09:50:27'),
(126, 225, 'M.Sc(chemistry)', 'Allahabad University', '', '2018-02-13 11:31:05'),
(127, 228, 'M.A pol science, M.A english', 'M.D.university', '', '2018-02-13 13:26:32'),
(128, 230, 'MA', 'DU', '', '2018-02-13 13:28:16'),
(129, 232, 'M.Sc in maths', 'baba saheb bhim rao ambedkar university', '', '2018-02-13 14:24:09'),
(130, 234, 'Bachelor of technology and Master of technology', 'AICTE Proved University (kurukshetra)', '', '2018-02-13 17:59:03'),
(131, 236, 'B Tech electrical engg.', 'AKTU Lucknow', '', '2018-02-14 08:39:30'),
(132, 237, 'MCA', 'SRM University', '', '2018-02-14 08:52:04'),
(133, 238, 'CA  pursuing', 'DU', '', '2018-02-14 09:29:54'),
(134, 176, 'be', 'bnm', '', '2018-02-20 10:46:20'),
(135, 241, 'hsne', 'sbsbdnsbs722-#', 'degree__6865.jpg', '2018-03-14 10:14:32'),
(136, 241, 'gaba', 'bagaba', '', '2018-03-09 08:31:16'),
(138, 249, 'bsnek', 'jjememe', '', '2018-03-16 09:28:04'),
(139, 249, 'hsjw', 'nsnwn', '', '2018-03-16 09:28:15'),
(140, 249, 'bss', 'bnds', '', '2018-03-16 09:29:18'),
(141, 249, 'beej', 'beue', '', '2018-03-16 09:31:48'),
(142, 250, 'xd', 'xx', '', '2018-03-16 11:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_educational_details`
--

CREATE TABLE `teacher_educational_details` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  `sub_topic_id` varchar(255) NOT NULL,
  `fees` varchar(255) DEFAULT NULL,
  `expert` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_expert_topic`
--

CREATE TABLE `teacher_expert_topic` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `topic_id` varchar(255) NOT NULL,
  `topic_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_feesinstallment`
--

CREATE TABLE `teacher_feesinstallment` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_follow`
--

CREATE TABLE `teacher_follow` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_login_history`
--

CREATE TABLE `teacher_login_history` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_manual_markachiever`
--

CREATE TABLE `teacher_manual_markachiever` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `achiever` text NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `profile_img` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_manual_markachiever`
--

INSERT INTO `teacher_manual_markachiever` (`id`, `teacher_id`, `first_name`, `year`, `achiever`, `class_id`, `profile_img`, `created`) VALUES
(3, 26, 'Vishal Yadav', '2016-12-22', 'Selected for GD\n', 'CLASS XII(COMMERCE)', 'acheivers_Vishal Yadav_261668.jpg', '2017-12-23 09:49:16'),
(4, 42, 'Vishal', '2010-12-25', 'Selected in upsc\n', 'CLASS V', 'acheivers_Vishal_422745.jpg', '2017-12-26 08:20:30'),
(5, 59, 'Hauio', '2012-12-27', 'Haulo', 'CLASS III', 'acheivers_Hauio_597486.jpg', '2017-12-28 07:19:21'),
(6, 73, 'Tdff', '2018-1-2', 'Hcjb', 'CLASS VI', 'acheivers_Tdff_735117.jpg', '2018-01-05 06:24:11'),
(7, 75, 'Fguub', '1986', 'Fugjg', 'CLASS VII', 'acheivers_Fguub_756405.jpg', '2018-01-08 05:52:52'),
(8, 75, 'Saurabh', '1986', 'Vhhbkkk', 'CLASS VII', 'acheivers_Saurabh_755250.jpg', '2018-01-08 10:21:39'),
(9, 75, 'Ystsdtidjtkxg', '1984', 'Zjg h hxjgjzjzjzjzjgkgxkgxkgdkgdkydkgdodiyd\nZjzjitzjgzkzjzkgz\nZjDhzjfzjzjzkdkditdidyitskdkgxkdkx\nXxjgjdjjxjxkgxkdkgkgfkgkgxkgdkxjxjc\nYdkxjxkxgkgxkd', 'CLASS VII', 'acheivers_Ystsdtidjtkxg_753408.jpg', '2018-01-09 11:15:10'),
(12, 9, 'Swetank', '2010-1-9', 'Aweome', 'B.Sc. Nursing', 'acheivers_Swetank_95667.jpg', '2018-01-10 09:28:34'),
(13, 75, 'Saurabh Singh', '1985', 'something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object', 'CLASS VII', 'acheivers_Saurabh Singh_759397.jpg', '2018-01-10 10:04:44'),
(14, 75, 'Vikash Kesharwani', '2010', 'something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object', 'CLASS VII', 'acheivers_Vikash Kesharwani_752511.jpg', '2018-01-10 10:07:04'),
(15, 75, 'Amit Thakur', '1982', 'something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s objectsomething accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object', 'Select Class/Course', 'acheivers_Amit Thakur_756148.jpg', '2018-01-10 10:08:24'),
(16, 75, 'Swetank Mohan', '1983', 'something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object', 'CLASS VII', 'acheivers_Swetank Mohan_757673.jpg', '2018-01-10 10:09:39'),
(17, 75, 'Naved Khan', '1981', 'something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object something accomplished, especially by superior ability, special effort, great courage, etc.; a great or heroic deed: his remarkable achievements in art. 2. the act of achieving; attainment or accomplishment: the achievement of one\'s object', 'CLASS VII', 'acheivers_Naved Khan_753261.jpg', '2018-01-10 10:10:42'),
(18, 49, 'Fyy', '', 'GgH', 'Select Class/Course', 'acheivers_Fyy_497704.jpg', '2018-01-15 11:31:28'),
(19, 107, 'Hdhdhhdhhdhdhdhdjdj', '0000', 'Hdhdhhdhhhdhdhdhdddhdh', '0000', 'acheivers_Hdhdhhdhhdhdhdhdjdj_1079196.jpg', '2018-01-23 11:43:32'),
(20, 87, 'Manu', '0000', 'Nice work', '0000', 'acheivers_Manu_877044.jpg', '2018-01-23 11:43:33'),
(21, 80, 'Ashish', '0000', 'Cleared ssc in 1st attempt', '0000', 'acheivers_Ashish_802626.jpg', '2018-01-23 12:27:25'),
(22, 80, 'Lakshya', '0000', 'CEO of google', '0000', 'acheivers_Lakshya_801852.jpg', '2018-01-23 12:34:02'),
(23, 75, 'Bjjjj Ujj', '0000', 'Ujniin bujh ', '0000', 'acheivers_Bjjjj Ujj_759336.jpg', '2018-01-30 05:50:57'),
(24, 159, 'Eti Aggarwal ', '0000', 'All India Rank 1\n', '0000', 'acheivers_Eti Aggarwal _1595880.jpg', '2018-02-03 04:17:39'),
(25, 136, 'Acheiver', '0000', 'Achiverer name cant be taken', '0000', 'acheivers_Acheiver_1367392.jpg', '2018-02-03 07:24:04'),
(26, 164, 'Aman Rajput', '0000', 'Selected in SBI po', '0000', 'acheivers_Aman Rajput_1649552.jpg', '2018-02-06 09:41:29'),
(27, 174, 'Rajan Jha', '0000', 'He is very talented guy. He has all potential to do big things in his life. He is very strong self motivated and very optimistic person. Now he is selected in NASA.\n', '0000', 'acheivers_Rajan Jha_1742512.jpg', '2018-02-08 05:09:12'),
(28, 174, 'Ashish Chikara', '0000', 'Selected in income tax department. After college, he took admission in kd campus then after 6 months, he cleared his SSC cgl exam and got selected in income tax department. \n', '0000', 'acheivers_Ashish Chikara_1747744.jpg', '2018-02-08 05:20:20'),
(29, 183, 'MOHIT SHOKEEN', '0000', '100 marks in mathematics', '0000', 'acheivers_MOHIT SHOKEEN_1831718.jpg', '2018-02-08 11:37:10'),
(30, 183, 'Bharat', '0000', '96 marks in mathematics', '0000', 'acheivers_Bharat_1836846.jpg', '2018-02-08 12:18:00'),
(31, 176, '67888', '0000', 'Hhh', '0000', 'acheivers_67888_1769789.jpg', '2018-02-09 12:07:14'),
(32, 188, 'STCians', '0000', 'Perfect ????', '0000', 'acheivers_STCians_1887466.jpg', '2018-02-11 03:08:16'),
(33, 176, '66', '0000', 'Hh', '0000', 'acheivers_66_1767391.jpg', '2018-02-12 10:58:17'),
(34, 176, 'F.tt.', '0000', 'Vg', '0000', 'acheivers_F.tt._1766593.jpg', '2018-02-15 06:03:57'),
(35, 241, 'Aj', '0000', 'Jz', '0000', 'acheivers_Aj_2417041.jpg', '2018-02-15 11:25:19'),
(36, 176, 'Dzns', '0000', 'Xd', '0000', 'acheivers_Dzns_1764815.jpg', '2018-02-20 11:14:49'),
(37, 241, 'Hj.', '0000', 'Uhfcn', '0000', '', '2018-03-06 11:12:24'),
(38, 249, 'Yhh', '0000', 'Hhji', '0000', '', '2018-03-16 10:17:15'),
(39, 251, 'Saurabh Singh', '0000', 'He has achieved gold medal in 100 m sprint', '0000', '', '2018-03-21 09:17:36');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_markachiever`
--

CREATE TABLE `teacher_markachiever` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `achiever` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_markachiever`
--

INSERT INTO `teacher_markachiever` (`id`, `teacher_id`, `student_id`, `achiever`, `created`) VALUES
(1, 1, 1, 'hardwker', '2017-12-21 08:42:03'),
(2, 18, 1, 'hellooo', '2017-12-22 10:53:38'),
(3, 30, 6, 'Selected in IIT roorkee', '2017-12-24 09:24:33'),
(4, 60, 1, 'hhhh', '2018-01-01 08:59:24'),
(8, 9, 2, 'jchd', '2018-01-10 09:53:00'),
(9, 97, 25, 'marvellous', '2018-01-20 08:54:51'),
(10, 75, 1, 'ycggu. yuv yhh ', '2018-01-23 04:58:48'),
(11, 74, 30, 'Hdhdhhdhdhhd', '2018-01-24 09:03:29'),
(12, 87, 30, 'Hdhdhdhd', '2018-01-24 12:15:11'),
(13, 9, 30, 'marvellous', '2018-01-25 06:30:16'),
(14, 136, 30, 'hdhddhsmts6skydlddddfiggdjdjdjdjddkkskssksk', '2018-02-07 06:28:29'),
(15, 75, 58, 'buhbjb', '2018-02-23 10:05:26'),
(16, 75, 36, 'hv', '2018-02-23 10:26:07'),
(17, 75, 61, 'huuuhvu', '2018-02-23 10:27:40'),
(18, 75, 62, 'hvjvv', '2018-02-23 10:28:25'),
(19, 75, 63, 'vhhj', '2018-02-23 10:29:55'),
(20, 75, 65, 'ug ug ug uu uu', '2018-02-23 12:12:02'),
(21, 75, 69, 'ug buubuh ', '2018-02-23 12:19:16'),
(22, 75, 64, 'ybbyybyyb', '2018-02-23 12:19:52'),
(23, 241, 81, 'hsie', '2018-02-23 12:41:15'),
(24, 241, 80, 'baja', '2018-02-23 12:41:26'),
(25, 241, 79, 'hhu', '2018-02-23 12:42:20'),
(26, 241, 78, ' snsjwbwowjsjsksksmsksnamalsmsmslsmsmslsmsmsmskwksnsnaksmaksmsmaksksmskwmsksmskwlsmslsnsmwkwmsmslsmwmslwmwlwmsmwlwmsmslw.smsmwlsmsmslwmsnsnskwkwks ekwjs smdlenems skemskekemsmekw sksnskekeiwjsueiwownwkanwpwmwnsnkwpwmwlwnwiw wkwnwkwnwksmskwksmslwmwlwm', '2018-02-23 12:45:16'),
(27, 241, 92, 'dguyss', '2018-02-23 13:23:29'),
(28, 241, 95, 'fhtrd u', '2018-02-23 13:25:45'),
(29, 75, 101, 'uixtifixix ifitxticicix idfiixgiyx', '2018-02-24 10:17:31'),
(30, 241, 104, 'bnn', '2018-02-26 06:34:50'),
(31, 241, 85, 'dddk', '2018-03-14 12:03:05'),
(32, 249, 130, 'bn ', '2018-03-16 09:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_myplans_rel`
--

CREATE TABLE `teacher_myplans_rel` (
  `id` int(10) UNSIGNED NOT NULL,
  `current_myplan_id` tinyint(4) DEFAULT '1',
  `old_myplan_id` int(11) DEFAULT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_offer`
--

CREATE TABLE `teacher_offer` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `offer_content` text NOT NULL,
  `offer_heading` text NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `profile_img` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_featured` int(1) NOT NULL DEFAULT '0',
  `batch_code` varchar(255) DEFAULT NULL,
  `inst_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_offer`
--

INSERT INTO `teacher_offer` (`id`, `teacher_id`, `subject`, `subject_id`, `offer_content`, `offer_heading`, `expiry_date`, `status`, `profile_img`, `created`, `is_featured`, `batch_code`, `inst_name`) VALUES
(2, 0, '0', 0, '20% off', 'institute abc', '2018-Jan-26', 1, 'offer_imgpsh_fullsize.png', '2018-01-25 13:03:32', 1, '123456', 'akash'),
(3, 0, '0', 0, 'xyz', 'abc', '2018-Feb-10', 1, 'offer_download.png', '2018-01-23 12:27:44', 1, '123455', 'tutorial');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_package_info`
--

CREATE TABLE `teacher_package_info` (
  `id` int(11) NOT NULL,
  `package_price` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `descripition` varchar(255) NOT NULL,
  `status` varchar(1) NOT NULL,
  `added_date` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_personal_details`
--

CREATE TABLE `teacher_personal_details` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `adress` text NOT NULL,
  `gender` varchar(255) NOT NULL,
  `adhar_status` int(11) NOT NULL DEFAULT '0',
  `adhar_img` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_personal_details`
--

INSERT INTO `teacher_personal_details` (`id`, `teacher_id`, `name`, `email`, `dob`, `adress`, `gender`, `adhar_status`, `adhar_img`, `created`) VALUES
(1, 26, 'rahul chhikara', 'rahul92chhikara@gmail.com', '1992-07-13', 'A-2, Janakpuri, Asalatpur Village, Janakpuri, New Delhi, Delhi 110058, India', 'M', 0, '', '2017-12-20 13:01:56'),
(2, 54, 'Ruchi Kapil', 'kapilruchi.kapil43@gmail.com', '1992-05-21', 'Subhash Nagar, Rajendra Nagar, Roorkee, Uttarakhand 247667, India', 'F', 0, '', '2017-12-21 10:08:43'),
(3, 43, 'satyender pal', 'satyenderpal1990@gmail.com', '1990-05-03', 'Budh Vihar Phase II, Budh Vihar, Delhi, India', 'M', 0, '', '2018-01-05 05:18:45');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_professional_details`
--

CREATE TABLE `teacher_professional_details` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `sub_topic` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_professional_details`
--

INSERT INTO `teacher_professional_details` (`id`, `teacher_id`, `class_id`, `class_name`, `subject_id`, `subject_name`, `sub_topic`, `created`) VALUES
(1, 1, 184, 'Gate BT', 1378, 'Biotechnology Engineering', '-', '2017-12-20 06:54:27'),
(2, 1, 184, 'Gate BT', 1376, 'Engineering Mathematics', '-', '2017-12-20 06:54:27'),
(3, 1, 184, 'Gate BT', 1377, 'General Aptitude', '-', '2017-12-20 06:54:27'),
(4, 1, 144, 'CAT', 1245, 'Verbal And Reading Comprehension', '-', '2017-12-20 06:54:37'),
(5, 1, 144, 'CAT', 1246, 'Data Interpretation And Logical Reasoning', '-', '2017-12-20 06:54:37'),
(6, 1, 144, 'CAT', 1247, 'Quantitative Ability', '-', '2017-12-20 06:54:37'),
(7, 1, 137, 'IMU-CET', 1215, 'Physics', '-', '2017-12-20 06:54:55'),
(8, 1, 137, 'IMU-CET', 1216, 'Chemistry', '-', '2017-12-20 06:54:55'),
(9, 1, 137, 'IMU-CET', 1219, 'General Knowledge', '-', '2017-12-20 06:54:55'),
(10, 1, 137, 'IMU-CET', 1220, 'Aptitude ', '-', '2017-12-20 06:54:55'),
(11, 1, 137, 'IMU-CET', 1218, 'English', '-', '2017-12-20 06:54:55'),
(12, 1, 137, 'IMU-CET', 1217, 'Maths', '-', '2017-12-20 06:54:55'),
(13, 2, 92, 'JEE Mains', 1115, 'Mathematics', '-', '2017-12-20 09:05:29'),
(14, 2, 92, 'JEE Mains', 1110, 'Physics', '-', '2017-12-20 09:05:29'),
(74, 26, 12, 'CLASS XII(COMMERCE)', 64, 'ACCOUNTANCY', '-', '2017-12-23 09:20:55'),
(73, 26, 11, 'CLASS XI(COMMERCE)', 45, 'ECONOMICS(MICRO)', '-', '2017-12-23 09:20:33'),
(72, 26, 11, 'CLASS XI(COMMERCE)', 43, 'ACCOUNTANCY', '-', '2017-12-23 09:20:33'),
(20, 7, 169, 'Gate CS', 1346, 'Engineering Mathematics', '-', '2017-12-21 07:09:28'),
(470, 79, 117, 'VITEEE', 1452, 'Mathematics', '-', '2018-01-18 06:25:01'),
(471, 79, 117, 'VITEEE', 1451, 'Chemistry', '-', '2018-01-18 06:25:01'),
(472, 79, 117, 'VITEEE', 1450, 'Physics', '-', '2018-01-18 06:25:01'),
(475, 9, 223, 'Electronics & Communication Engineering', 1715, '	Chemistry', '-', '2018-01-18 09:19:33'),
(477, 9, 223, 'Electronics & Communication Engineering', 1712, 'Physics', '-', '2018-01-18 09:19:33'),
(474, 9, 223, 'Electronics & Communication Engineering', 1716, 'Art of Programming', '-', '2018-01-18 09:19:33'),
(27, 13, 157, 'National Institute Of Design', 1298, 'Knowledge', '-', '2017-12-22 06:09:11'),
(28, 13, 157, 'National Institute Of Design', 1300, 'Analysis', '-', '2017-12-22 06:09:11'),
(29, 13, 157, 'National Institute Of Design', 1301, 'Creativity', '-', '2017-12-22 06:09:11'),
(30, 13, 157, 'National Institute Of Design', 1302, 'Visualisation', '-', '2017-12-22 06:09:11'),
(31, 13, 157, 'National Institute Of Design', 1299, 'Comprehension', '-', '2017-12-22 06:09:11'),
(32, 12, 93, 'Mathematical Hons.', 1092, 'Vector Analysis', '-', '2017-12-22 07:03:35'),
(33, 12, 93, 'Mathematical Hons.', 1087, 'Integral Calculus And Trigonometry', '-', '2017-12-22 07:03:35'),
(34, 18, 181, 'RTCG (Railway Technical Cadre exam)', 1514, 'General Knowledge', '-', '2017-12-22 09:22:03'),
(35, 18, 181, 'RTCG (Railway Technical Cadre exam)', 1513, 'Analytical and Quantitative Skills', '-', '2017-12-22 09:22:03'),
(36, 18, 181, 'RTCG (Railway Technical Cadre exam)', 1510, 'General English', '-', '2017-12-22 09:22:03'),
(37, 18, 181, 'RTCG (Railway Technical Cadre exam)', 1512, 'General Arithmetic', '-', '2017-12-22 09:22:03'),
(38, 18, 127, 'CLAT ', 1166, 'General Knowledge and current Affairs', '-', '2017-12-22 09:22:29'),
(39, 18, 127, 'CLAT ', 1167, 'Elementary Mathematics (Numerical Ability)', '-', '2017-12-22 09:22:29'),
(40, 18, 127, 'CLAT ', 1168, 'Legal Aptitude', '-', '2017-12-22 09:22:29'),
(420, 61, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-01-10 07:44:38'),
(421, 61, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-10 07:44:38'),
(384, 54, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-01-08 16:10:01'),
(183, 58, 94, 'JEE Advanced', 1121, 'Mathematics', '-', '2017-12-28 06:05:38'),
(45, 54, 99, 'Biotechnology', 1098, 'Plant Biotechnology', '-', '2017-12-22 12:00:59'),
(46, 54, 99, 'Biotechnology', 1093, 'Cell Biology', '-', '2017-12-22 12:00:59'),
(47, 54, 99, 'Biotechnology', 1096, 'Molecular Biology', '-', '2017-12-22 12:00:59'),
(48, 54, 101, 'Biochemistry', 1104, 'Immunology', '-', '2017-12-22 12:01:54'),
(49, 54, 101, 'Biochemistry', 1099, 'Biomolecules', '-', '2017-12-22 12:01:54'),
(50, 15, 99, 'Biotechnology', 1093, 'Cell Biology', '-', '2017-12-22 17:21:18'),
(65, 24, 176, 'SSC CGL exam', 1493, 'General Intelligence & Reasoning', '-', '2017-12-23 08:41:53'),
(64, 24, 11, 'CLASS XI(COMMERCE)', 45, 'ECONOMICS(MICRO)', '-', '2017-12-23 08:40:56'),
(63, 24, 11, 'CLASS XI(COMMERCE)', 44, 'BUSINESS STUDIES', '-', '2017-12-23 08:40:56'),
(62, 24, 11, 'CLASS XI(COMMERCE)', 43, 'ACCOUNTANCY', '-', '2017-12-23 08:40:56'),
(59, 24, 12, 'CLASS XII(COMMERCE)', 64, 'ACCOUNTANCY', '-', '2017-12-23 08:40:45'),
(60, 24, 12, 'CLASS XII(COMMERCE)', 65, 'BUSINESS STUDIES', '-', '2017-12-23 08:40:45'),
(61, 24, 12, 'CLASS XII(COMMERCE)', 66, 'ECONOMICS(MICRO)', '-', '2017-12-23 08:40:45'),
(66, 24, 176, 'SSC CGL exam', 1494, 'General Awareness', '-', '2017-12-23 08:41:53'),
(67, 24, 176, 'SSC CGL exam', 1495, 'Quantitative Aptitude', '-', '2017-12-23 08:41:53'),
(68, 24, 176, 'SSC CGL exam', 1496, 'English Language', '-', '2017-12-23 08:41:53'),
(69, 24, 189, 'Teaching Exams Tuitions', 1439, 'B.Ed. Entrance', '-', '2017-12-23 08:42:22'),
(70, 24, 189, 'Teaching Exams Tuitions', 1441, 'CTET', '-', '2017-12-23 08:42:22'),
(71, 24, 189, 'Teaching Exams Tuitions', 1442, 'HTET', '-', '2017-12-23 08:42:22'),
(75, 26, 12, 'CLASS XII(COMMERCE)', 66, 'ECONOMICS(MICRO)', '-', '2017-12-23 09:20:55'),
(118, 28, 5, 'CLASS V', 320, 'HINDI', '-', '2017-12-23 14:48:46'),
(117, 28, 5, 'CLASS V', 317, 'E.V.S', '-', '2017-12-23 14:48:46'),
(116, 28, 5, 'CLASS V', 318, 'ENGLISH', '-', '2017-12-23 14:48:46'),
(115, 28, 5, 'CLASS V', 319, 'MATHEMATICS', '-', '2017-12-23 14:48:46'),
(125, 28, 6, 'CLASS VI', 327, 'CIVICS', '-', '2017-12-23 14:49:06'),
(124, 28, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2017-12-23 14:49:06'),
(123, 28, 6, 'CLASS VI', 325, 'HISTORY', '-', '2017-12-23 14:49:06'),
(122, 28, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2017-12-23 14:49:06'),
(121, 28, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2017-12-23 14:49:06'),
(120, 28, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2017-12-23 14:49:06'),
(89, 28, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2017-12-23 12:01:51'),
(90, 28, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2017-12-23 12:01:51'),
(91, 28, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2017-12-23 12:01:51'),
(92, 28, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2017-12-23 12:01:51'),
(93, 28, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2017-12-23 12:01:51'),
(94, 28, 8, 'CLASS VIII', 358, 'HINDI', '-', '2017-12-23 12:01:51'),
(95, 28, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2017-12-23 12:01:51'),
(96, 28, 8, 'CLASS VIII', 360, 'GENERAL KNOWLEDGE', '-', '2017-12-23 12:01:51'),
(97, 28, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2017-12-23 12:01:51'),
(135, 28, 7, 'CLASS VII', 345, 'HINDI', '-', '2017-12-23 14:49:19'),
(134, 28, 7, 'CLASS VII', 340, 'CIVICS', '-', '2017-12-23 14:49:19'),
(133, 28, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2017-12-23 14:49:19'),
(132, 28, 7, 'CLASS VII', 63, 'HISTORY', '-', '2017-12-23 14:49:19'),
(131, 28, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2017-12-23 14:49:19'),
(130, 28, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2017-12-23 14:49:19'),
(129, 28, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2017-12-23 14:49:19'),
(128, 28, 7, 'CLASS VII', 63, 'HISTORY', '-', '2017-12-23 14:49:19'),
(127, 28, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2017-12-23 14:49:19'),
(109, 28, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2017-12-23 12:02:25'),
(110, 28, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2017-12-23 12:02:25'),
(111, 28, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2017-12-23 12:02:25'),
(112, 28, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2017-12-23 12:02:34'),
(113, 28, 10, 'CLASS X', 375, 'ENGLISH', '-', '2017-12-23 12:02:34'),
(114, 28, 10, 'CLASS X', 376, 'SCIENCE', '-', '2017-12-23 12:02:34'),
(119, 28, 5, 'CLASS V', 321, 'GENERAL KNOWLEDGE', '-', '2017-12-23 14:48:46'),
(126, 28, 6, 'CLASS VI', 332, 'HINDI', '-', '2017-12-23 14:49:06'),
(136, 28, 7, 'CLASS VII', 335, 'MATHEMATICS', '-', '2017-12-23 14:49:19'),
(137, 28, 7, 'CLASS VII', 338, 'HISTORY', '-', '2017-12-23 14:49:19'),
(138, 30, 18, 'CLASS XI(MEDICAL)', 59, 'BIOLOGY', '-', '2017-12-24 08:54:44'),
(139, 30, 21, 'CLASS XII(MEDICAL)', 79, 'BIOLOGY', '-', '2017-12-24 08:54:55'),
(140, 30, 109, 'AIPMT', 1453, 'Biology', '-', '2017-12-24 08:55:13'),
(141, 30, 111, 'NEET', 1466, 'Biology', '-', '2017-12-24 08:55:23'),
(142, 30, 108, 'AIIMS', 1457, 'Biology', '-', '2017-12-24 08:55:38'),
(143, 29, 218, 'Computer Science  Engineering', 1657, 'Java Technologies', '-', '2017-12-24 09:00:24'),
(144, 29, 218, 'Computer Science  Engineering', 1658, 'Objective C Programming ', '-', '2017-12-24 09:00:24'),
(145, 29, 218, 'Computer Science  Engineering', 1637, 'Mathematics', '-', '2017-12-24 09:00:24'),
(146, 29, 218, 'Computer Science  Engineering', 1645, '	Object Oriented Programming', '-', '2017-12-24 09:00:24'),
(147, 29, 218, 'Computer Science  Engineering', 1646, 'Applied Mathematics', '-', '2017-12-24 09:00:24'),
(148, 31, 92, 'JEE Mains', 1113, 'Chemistry', '-', '2017-12-24 12:45:09'),
(149, 31, 92, 'JEE Mains', 1110, 'Physics', '-', '2017-12-24 12:45:09'),
(150, 31, 92, 'JEE Mains', 1115, 'Mathematics', '-', '2017-12-24 12:45:09'),
(151, 19, 186, 'co-curricular', 1423, 'Instruments', '-', '2017-12-24 15:40:11'),
(152, 19, 186, 'co-curricular', 1421, 'Singing', '-', '2017-12-24 15:40:11'),
(153, 36, 183, 'RPF (Sub-Inspector/Constable) exam', 1521, 'Arithmetic Ability', '-', '2017-12-26 06:00:15'),
(154, 36, 183, 'RPF (Sub-Inspector/Constable) exam', 1520, 'General Awareness', '-', '2017-12-26 06:00:15'),
(155, 36, 183, 'RPF (Sub-Inspector/Constable) exam', 1519, 'General Intelligence & Reasoning', '-', '2017-12-26 06:00:15'),
(156, 38, 183, 'RPF (Sub-Inspector/Constable) exam', 1521, 'Arithmetic Ability', '-', '2017-12-26 06:10:07'),
(157, 38, 183, 'RPF (Sub-Inspector/Constable) exam', 1520, 'General Awareness', '-', '2017-12-26 06:10:07'),
(158, 38, 183, 'RPF (Sub-Inspector/Constable) exam', 1519, 'General Intelligence & Reasoning', '-', '2017-12-26 06:10:07'),
(159, 42, 5, 'CLASS V', 318, 'ENGLISH', '-', '2017-12-26 07:42:26'),
(160, 42, 5, 'CLASS V', 319, 'MATHEMATICS', '-', '2017-12-26 07:42:26'),
(161, 42, 5, 'CLASS V', 320, 'HINDI', '-', '2017-12-26 07:42:26'),
(162, 44, 16, 'NETWORKING', 491, 'INTERNET COURSES-CCNA CISCO CERTIFIED NETWORK ASSOCIATE', '-', '2017-12-26 08:42:36'),
(163, 44, 16, 'NETWORKING', 490, 'CCNA IT COURSES-NIELIT O-LEVEL COURSES', '-', '2017-12-26 08:42:36'),
(164, 44, 16, 'NETWORKING', 488, 'CCNA CISCO CERTIFIED NETWORK ASSOCIATE ROUTING & SWITCHING', '-', '2017-12-26 08:42:36'),
(165, 44, 16, 'NETWORKING', 487, 'CCNA CCNP CCIE MICROSOFT MCSE COURSES', '-', '2017-12-26 08:42:36'),
(166, 44, 16, 'NETWORKING', 76, 'COMPUTER APPLICATION DIPLOMA', '-', '2017-12-26 08:42:36'),
(167, 46, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2017-12-26 12:48:49'),
(168, 46, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2017-12-26 12:48:49'),
(169, 46, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2017-12-26 12:48:49'),
(170, 46, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2017-12-26 12:49:53'),
(171, 46, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2017-12-26 12:49:53'),
(172, 46, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2017-12-26 12:49:53'),
(173, 46, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2017-12-26 12:50:02'),
(174, 46, 10, 'CLASS X', 376, 'SCIENCE', '-', '2017-12-26 12:50:02'),
(732, 144, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-01-31 12:44:17'),
(731, 144, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-31 12:44:17'),
(177, 46, 21, 'CLASS XII(MEDICAL)', 79, 'BIOLOGY', '-', '2017-12-26 12:50:46'),
(178, 46, 21, 'CLASS XII(MEDICAL)', 409, 'CHEMISTRY', '-', '2017-12-26 12:50:46'),
(179, 2, 18, 'CLASS XI(MEDICAL)', 59, 'BIOLOGY', '-', '2017-12-27 11:49:32'),
(180, 2, 21, 'CLASS XII(MEDICAL)', 79, 'BIOLOGY', '-', '2017-12-27 11:49:44'),
(181, 53, 13, 'PROGRAMMING LANGUAGE', 11, 'PARALLEL COMPUTING', '-', '2017-12-27 12:38:32'),
(182, 53, 13, 'PROGRAMMING LANGUAGE', 430, 'DBMS', '-', '2017-12-27 12:38:32'),
(184, 58, 94, 'JEE Advanced', 1120, 'Chemistry', '-', '2017-12-28 06:05:38'),
(185, 58, 94, 'JEE Advanced', 1119, 'Physics', '-', '2017-12-28 06:05:38'),
(186, 59, 3, 'CLASS III', 22, 'ENGLISH ', '-', '2017-12-28 06:18:32'),
(187, 59, 3, 'CLASS III', 23, 'HINDI', '-', '2017-12-28 06:18:32'),
(191, 57, 92, 'JEE Mains', 1113, 'Chemistry', '-', '2017-12-28 07:18:24'),
(190, 57, 92, 'JEE Mains', 1110, 'Physics', '-', '2017-12-28 07:18:24'),
(192, 57, 92, 'JEE Mains', 1115, 'Mathematics', '-', '2017-12-28 07:18:24'),
(335, 70, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-01-03 12:06:58'),
(334, 70, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-01-03 12:06:58'),
(198, 57, 219, 'Civil Engineering ', 1601, 'Mathematics', '-', '2017-12-28 07:18:53'),
(199, 57, 219, 'Civil Engineering ', 1602, '	Chemistry', '-', '2017-12-28 07:18:53'),
(200, 57, 219, 'Civil Engineering ', 1603, '	Elements of Electrical Engineering', '-', '2017-12-28 07:18:53'),
(201, 57, 219, 'Civil Engineering ', 1607, 'Building Construction Materials', '-', '2017-12-28 07:18:53'),
(202, 57, 219, 'Civil Engineering ', 1606, '	Physics', '-', '2017-12-28 07:18:53'),
(203, 57, 219, 'Civil Engineering ', 1605, 'Engineering Graphics', '-', '2017-12-28 07:18:53'),
(204, 57, 219, 'Civil Engineering ', 1604, '	Art of Programming', '-', '2017-12-28 07:18:53'),
(205, 57, 219, 'Civil Engineering ', 1614, '	Law for Engineers', '-', '2017-12-28 07:18:53'),
(206, 57, 219, 'Civil Engineering ', 1613, 'Design of Structures', '-', '2017-12-28 07:18:53'),
(207, 57, 219, 'Civil Engineering ', 1612, '	Foundation Engineering', '-', '2017-12-28 07:18:53'),
(208, 57, 219, 'Civil Engineering ', 1610, 'Geology and Geotechnical Engineering', '-', '2017-12-28 07:18:53'),
(209, 57, 219, 'Civil Engineering ', 1611, 'Fluid Mechanics', '-', '2017-12-28 07:18:53'),
(210, 57, 219, 'Civil Engineering ', 1615, 'Construction and Project Management', '-', '2017-12-28 07:18:53'),
(211, 57, 219, 'Civil Engineering ', 1616, '	Organizational Behavior', '-', '2017-12-28 07:18:53'),
(212, 60, 1, 'CLASS I', 1810, 'EVS', '-', '2017-12-28 08:55:27'),
(213, 60, 1, 'CLASS I', 18, 'ENGLISH ', '-', '2017-12-28 08:55:27'),
(214, 60, 1, 'CLASS I', 19, ' MATHEMATICS', '-', '2017-12-28 08:55:27'),
(215, 57, 16, 'NETWORKING', 492, 'CCNA WIRELESS-IU WNE', '-', '2017-12-28 12:27:17'),
(216, 57, 16, 'NETWORKING', 491, 'INTERNET COURSES-CCNA CISCO CERTIFIED NETWORK ASSOCIATE', '-', '2017-12-28 12:27:17'),
(217, 57, 16, 'NETWORKING', 490, 'CCNA IT COURSES-NIELIT O-LEVEL COURSES', '-', '2017-12-28 12:27:17'),
(218, 61, 21, 'CLASS XII(MEDICAL)', 78, 'PHYSICS', '-', '2017-12-28 12:38:51'),
(219, 61, 21, 'CLASS XII(MEDICAL)', 79, 'BIOLOGY', '-', '2017-12-28 12:38:51'),
(220, 61, 21, 'CLASS XII(MEDICAL)', 409, 'CHEMISTRY', '-', '2017-12-28 12:38:51'),
(221, 62, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2017-12-29 09:21:36'),
(452, 39, 15, 'GRAPHIC/ANIMATION', 71, 'ANIMATION', '-', '2018-01-15 04:35:05'),
(451, 39, 15, 'GRAPHIC/ANIMATION', 74, 'WEB DESIGINING', '-', '2018-01-15 04:35:05'),
(450, 39, 15, 'GRAPHIC/ANIMATION', 70, 'COMPUTER GRAPHICS', '-', '2018-01-15 04:35:05'),
(227, 65, 173, 'Gate CIVIL', 1375, 'Civil Engineering', '-', '2017-12-31 05:38:20'),
(228, 65, 173, 'Gate CIVIL', 1374, 'General Aptitude', '-', '2017-12-31 05:38:20'),
(235, 60, 21, 'CLASS XII(MEDICAL)', 411, 'MATHEMATICS', '-', '2018-01-01 09:12:08'),
(234, 60, 21, 'CLASS XII(MEDICAL)', 78, 'PHYSICS', '-', '2018-01-01 09:12:08'),
(233, 60, 21, 'CLASS XII(MEDICAL)', 80, 'PHYSICAL EDUCATION', '-', '2018-01-01 09:12:08'),
(336, 70, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-01-03 12:06:58'),
(333, 70, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-01-03 12:06:58'),
(331, 68, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-01-03 11:15:40'),
(332, 70, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-01-03 12:06:58'),
(241, 66, 1, 'CLASS I', 1810, 'EVS', '-', '2018-01-02 08:33:11'),
(242, 66, 1, 'CLASS I', 18, 'ENGLISH ', '-', '2018-01-02 08:33:11'),
(243, 66, 1, 'CLASS I', 19, ' MATHEMATICS', '-', '2018-01-02 08:33:11'),
(244, 66, 1, 'CLASS I', 307, 'HINDI', '-', '2018-01-02 08:33:11'),
(245, 66, 1, 'CLASS I', 308, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:33:11'),
(246, 66, 2, 'CLASS II', 20, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:33:22'),
(247, 66, 2, 'CLASS II', 21, ' HINDI ', '-', '2018-01-02 08:33:22'),
(248, 66, 2, 'CLASS II', 309, 'E.V.S', '-', '2018-01-02 08:33:22'),
(249, 66, 2, 'CLASS II', 310, 'ENGLISH', '-', '2018-01-02 08:33:22'),
(250, 66, 2, 'CLASS II', 311, 'MATHEMATICS', '-', '2018-01-02 08:33:22'),
(251, 66, 3, 'CLASS III', 22, 'ENGLISH ', '-', '2018-01-02 08:33:30'),
(252, 66, 3, 'CLASS III', 23, 'HINDI', '-', '2018-01-02 08:33:30'),
(253, 66, 3, 'CLASS III', 24, 'MATHEMATICS', '-', '2018-01-02 08:33:30'),
(254, 66, 3, 'CLASS III', 312, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:33:30'),
(255, 66, 3, 'CLASS III', 313, 'E.V.S', '-', '2018-01-02 08:33:30'),
(256, 66, 4, 'CLASS IV', 26, 'ENGLISH', '-', '2018-01-02 08:33:37'),
(257, 66, 4, 'CLASS IV', 27, 'HINDI', '-', '2018-01-02 08:33:37'),
(258, 66, 4, 'CLASS IV', 314, 'E.V.S', '-', '2018-01-02 08:33:37'),
(259, 66, 4, 'CLASS IV', 315, 'MATHEMATICS', '-', '2018-01-02 08:33:37'),
(260, 66, 4, 'CLASS IV', 316, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:33:37'),
(261, 66, 5, 'CLASS V', 319, 'MATHEMATICS', '-', '2018-01-02 08:33:44'),
(262, 66, 5, 'CLASS V', 318, 'ENGLISH', '-', '2018-01-02 08:33:44'),
(263, 66, 5, 'CLASS V', 317, 'E.V.S', '-', '2018-01-02 08:33:44'),
(264, 66, 5, 'CLASS V', 320, 'HINDI', '-', '2018-01-02 08:33:44'),
(265, 66, 5, 'CLASS V', 321, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:33:44'),
(266, 66, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-01-02 08:33:54'),
(267, 66, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-01-02 08:33:54'),
(268, 66, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-01-02 08:33:54'),
(269, 66, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-01-02 08:33:54'),
(270, 66, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-01-02 08:33:54'),
(271, 66, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-01-02 08:33:54'),
(272, 66, 6, 'CLASS VI', 328, 'FRENCH', '-', '2018-01-02 08:33:54'),
(273, 66, 6, 'CLASS VI', 329, 'GERMAN', '-', '2018-01-02 08:33:54'),
(274, 66, 6, 'CLASS VI', 330, 'SANSKRIT', '-', '2018-01-02 08:33:54'),
(296, 66, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-01-02 08:34:39'),
(295, 66, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-01-02 08:34:39'),
(294, 66, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-01-02 08:34:39'),
(293, 66, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-01-02 08:34:39'),
(292, 66, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-01-02 08:34:39'),
(291, 66, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-01-02 08:34:39'),
(290, 66, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-01-02 08:34:39'),
(284, 66, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-01-02 08:34:24'),
(285, 66, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-02 08:34:24'),
(286, 66, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-02 08:34:24'),
(287, 66, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-01-02 08:34:24'),
(288, 66, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-01-02 08:34:24'),
(289, 66, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-01-02 08:34:24'),
(297, 66, 7, 'CLASS VII', 340, 'CIVICS', '-', '2018-01-02 08:34:39'),
(298, 66, 7, 'CLASS VII', 341, 'FRENCH', '-', '2018-01-02 08:34:39'),
(299, 66, 7, 'CLASS VII', 338, 'HISTORY', '-', '2018-01-02 08:34:39'),
(300, 66, 7, 'CLASS VII', 335, 'MATHEMATICS', '-', '2018-01-02 08:34:39'),
(301, 66, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-02 08:35:23'),
(302, 66, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-01-02 08:35:23'),
(303, 66, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-01-02 08:35:23'),
(304, 66, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-01-02 08:35:23'),
(305, 66, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-01-02 08:35:23'),
(306, 66, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-01-02 08:35:23'),
(307, 66, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-02 08:35:39'),
(308, 66, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-01-02 08:35:39'),
(309, 66, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-01-02 08:35:39'),
(310, 66, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-01-02 08:35:39'),
(311, 66, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-01-02 08:35:39'),
(312, 66, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-01-02 08:35:39'),
(313, 69, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-01-02 08:41:22'),
(314, 69, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-01-02 08:41:22'),
(315, 69, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-01-02 08:41:22'),
(316, 69, 6, 'CLASS VI', 334, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:41:22'),
(317, 69, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-02 08:42:33'),
(318, 69, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-01-02 08:42:33'),
(319, 69, 10, 'CLASS X', 386, 'GENERAL KNOWLEDGE', '-', '2018-01-02 08:42:33'),
(320, 69, 10, 'CLASS X', 384, 'HINDI', '-', '2018-01-02 08:42:33'),
(453, 39, 15, 'GRAPHIC/ANIMATION', 72, 'GRAPHIC DESIGINING', '-', '2018-01-15 04:35:05'),
(412, 61, 92, 'JEE Mains', 1115, 'Mathematics', '-', '2018-01-10 07:40:29'),
(997, 80, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-07 09:18:52'),
(996, 80, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-07 09:18:52'),
(995, 80, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-07 09:18:52'),
(340, 71, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-01-04 07:03:57'),
(339, 71, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-01-04 07:03:57'),
(341, 71, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-01-04 07:03:57'),
(342, 71, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-01-04 11:13:06'),
(343, 71, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-01-04 11:13:42'),
(344, 43, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-05 05:14:49'),
(345, 43, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-01-05 05:14:49'),
(346, 43, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-01-05 05:14:49'),
(347, 43, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-01-05 05:14:49'),
(348, 43, 9, 'CLASS IX', 373, 'GENERAL KNOWLEDGE', '-', '2018-01-05 05:14:49'),
(349, 43, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-05 05:15:15'),
(350, 43, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-01-05 05:15:15'),
(351, 43, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-01-05 05:15:15'),
(352, 43, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-01-05 05:15:15'),
(353, 43, 10, 'CLASS X', 384, 'HINDI', '-', '2018-01-05 05:15:15'),
(354, 73, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-01-05 05:22:48'),
(355, 73, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-01-05 05:22:48'),
(356, 73, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-01-05 05:22:48'),
(408, 80, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-01-10 06:25:17'),
(847, 161, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-05 10:06:58'),
(846, 160, 91, 'Computer Science Hons.', 1085, 'Algorithms ', '-', '2018-02-03 07:09:56'),
(845, 160, 91, 'Computer Science Hons.', 1082, 'Website Designing', '-', '2018-02-03 07:09:56'),
(844, 160, 91, 'Computer Science Hons.', 1083, 'Object Oriented Programming', '-', '2018-02-03 07:09:56'),
(843, 160, 91, 'Computer Science Hons.', 1084, 'Software Engineering', '-', '2018-02-03 07:09:56'),
(842, 160, 91, 'Computer Science Hons.', 1086, 'Computer Organisation', '-', '2018-02-03 07:09:56'),
(840, 75, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-03 06:51:31'),
(841, 75, 7, 'CLASS VII', 347, 'GENERAL KNOWLEDGE', '-', '2018-02-03 06:51:31'),
(385, 54, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-01-08 16:10:21'),
(419, 61, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-10 07:44:38'),
(454, 39, 15, 'GRAPHIC/ANIMATION', 73, 'MULTIMEDIA & WEB TECHNOLOGY', '-', '2018-01-15 04:35:05'),
(422, 61, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-01-10 07:44:38'),
(423, 61, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-01-10 07:44:38'),
(424, 61, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-01-10 07:44:38'),
(473, 9, 223, 'Electronics & Communication Engineering', 1717, '	Elements of Electrical Engineering', '-', '2018-01-18 09:19:33'),
(467, 88, 220, 'Mechanical Engineering', 1599, '	Production Technology', '-', '2018-01-17 03:24:14'),
(466, 88, 220, 'Mechanical Engineering', 1587, 'Fluid mechanics', '-', '2018-01-17 03:24:14'),
(465, 88, 220, 'Mechanical Engineering', 1586, 'Thermodynamics,', '-', '2018-01-17 03:24:14'),
(443, 83, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-01-11 12:53:22'),
(444, 83, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-01-11 12:53:22'),
(445, 83, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-01-11 12:53:22'),
(446, 83, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-01-11 12:53:22'),
(447, 83, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-01-11 12:53:22'),
(690, 49, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-01-22 05:33:12'),
(689, 49, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-01-22 05:33:12'),
(476, 9, 223, 'Electronics & Communication Engineering', 1713, 'Mechanics of Solids', '-', '2018-01-18 09:19:33'),
(464, 88, 220, 'Mechanical Engineering', 1584, 'Strength of materials and solid mechanics', '-', '2018-01-17 03:24:14'),
(468, 88, 172, 'Gate ME', 1371, 'General Aptitude', '-', '2018-01-17 03:24:52'),
(469, 88, 172, 'Gate ME', 1370, 'Engineering Mathematics', '-', '2018-01-17 03:24:52'),
(478, 9, 223, 'Electronics & Communication Engineering', 1711, 'Mathematics', '-', '2018-01-18 09:19:33'),
(479, 9, 223, 'Electronics & Communication Engineering', 1714, 'Engineering Graphics', '-', '2018-01-18 09:19:33'),
(480, 9, 223, 'Electronics & Communication Engineering', 1718, 'Electronics Devices and Circuits', '-', '2018-01-18 09:19:33'),
(481, 9, 223, 'Electronics & Communication Engineering', 1720, 'Network Analysis', '-', '2018-01-18 09:19:33'),
(482, 9, 223, 'Electronics & Communication Engineering', 1721, 'Linear Control System', '-', '2018-01-18 09:19:33'),
(483, 9, 223, 'Electronics & Communication Engineering', 1719, 'Digital Circuits', '-', '2018-01-18 09:19:33'),
(484, 9, 223, 'Electronics & Communication Engineering', 1722, 'Signals and Systems', '-', '2018-01-18 09:19:33'),
(485, 9, 223, 'Electronics & Communication Engineering', 1723, 'Electrical Machines and Drives', '-', '2018-01-18 09:19:33'),
(486, 9, 218, 'Computer Science  Engineering', 1637, 'Mathematics', '-', '2018-01-18 09:49:40'),
(487, 9, 218, 'Computer Science  Engineering', 1638, '	Physics', '-', '2018-01-18 09:49:40'),
(488, 9, 218, 'Computer Science  Engineering', 1639, '	Engineering Graphics', '-', '2018-01-18 09:49:40'),
(489, 9, 218, 'Computer Science  Engineering', 1640, '	Chemistry', '-', '2018-01-18 09:49:40'),
(490, 9, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2018-01-18 09:49:40'),
(491, 9, 218, 'Computer Science  Engineering', 1642, '	Elements of Electrical Engineering', '-', '2018-01-18 09:49:40'),
(492, 9, 218, 'Computer Science  Engineering', 1643, '	Basic Electronics', '-', '2018-01-18 09:49:40'),
(493, 9, 218, 'Computer Science  Engineering', 1644, '	Digital Systems', '-', '2018-01-18 09:49:40'),
(494, 9, 218, 'Computer Science  Engineering', 1645, '	Object Oriented Programming', '-', '2018-01-18 09:49:40'),
(495, 9, 218, 'Computer Science  Engineering', 1646, 'Applied Mathematics', '-', '2018-01-18 09:49:40'),
(496, 9, 218, 'Computer Science  Engineering', 1647, '	Data Communication', '-', '2018-01-18 09:49:40'),
(497, 9, 218, 'Computer Science  Engineering', 1648, 'Computer Organization', '-', '2018-01-18 09:49:40'),
(498, 9, 218, 'Computer Science  Engineering', 1650, 'Theory of Computation', '-', '2018-01-18 09:49:40'),
(499, 9, 218, 'Computer Science  Engineering', 1649, 'Data Structures', '-', '2018-01-18 09:49:40'),
(500, 9, 218, 'Computer Science  Engineering', 1651, 'Database Management Systems', '-', '2018-01-18 09:49:40'),
(501, 9, 218, 'Computer Science  Engineering', 1652, '	Computer Networks', '-', '2018-01-18 09:49:40'),
(502, 9, 218, 'Computer Science  Engineering', 1653, 'Operating Systems', '-', '2018-01-18 09:49:40'),
(503, 9, 218, 'Computer Science  Engineering', 1654, 'Web designing', '-', '2018-01-18 09:49:40'),
(504, 9, 218, 'Computer Science  Engineering', 1655, 'Software Engineering', '-', '2018-01-18 09:49:40'),
(505, 9, 218, 'Computer Science  Engineering', 1656, '.net Technologies', '-', '2018-01-18 09:49:40'),
(506, 9, 218, 'Computer Science  Engineering', 1657, 'Java Technologies', '-', '2018-01-18 09:49:40'),
(507, 9, 218, 'Computer Science  Engineering', 1658, 'Objective C Programming ', '-', '2018-01-18 09:49:40'),
(508, 9, 218, 'Computer Science  Engineering', 1659, 'System Software', '-', '2018-01-18 09:49:40'),
(509, 9, 218, 'Computer Science  Engineering', 1660, 'Mobile Applications Development Technologies', '-', '2018-01-18 09:49:40'),
(510, 9, 218, 'Computer Science  Engineering', 1661, 'Advanced Computer Networks', '-', '2018-01-18 09:49:40'),
(511, 9, 218, 'Computer Science  Engineering', 1662, 'Main Frame Systems', '-', '2018-01-18 09:49:40'),
(512, 9, 218, 'Computer Science  Engineering', 1663, 'Computer Graphics and Visualization ', '-', '2018-01-18 09:49:40'),
(513, 9, 218, 'Computer Science  Engineering', 1664, 'Software Testing ', '-', '2018-01-18 09:49:40'),
(514, 9, 218, 'Computer Science  Engineering', 1665, 'Cloud Computing', '-', '2018-01-18 09:49:40'),
(515, 9, 218, 'Computer Science  Engineering', 1666, 'Data Mining', '-', '2018-01-18 09:49:40'),
(516, 9, 218, 'Computer Science  Engineering', 1667, 'Artificial Intelligence', '-', '2018-01-18 09:49:40'),
(517, 9, 218, 'Computer Science  Engineering', 1668, 'Compiler Construction', '-', '2018-01-18 09:49:40'),
(518, 9, 218, 'Computer Science  Engineering', 1669, 'Interfacing with Microprocessor', '-', '2018-01-18 09:49:40'),
(519, 9, 218, 'Computer Science  Engineering', 1670, 'Digital Image Processing', '-', '2018-01-18 09:49:40'),
(520, 9, 218, 'Computer Science  Engineering', 1671, 'Web security', '-', '2018-01-18 09:49:40'),
(521, 9, 218, 'Computer Science  Engineering', 1672, 'Agile Software Development ', '-', '2018-01-18 09:49:40'),
(522, 9, 218, 'Computer Science  Engineering', 1673, 'Secure software engineering', '-', '2018-01-18 09:49:40'),
(523, 9, 218, 'Computer Science  Engineering', 1674, 'Network programming', '-', '2018-01-18 09:49:40'),
(836, 75, 137, 'IMU-CET', 1220, 'Aptitude ', '-', '2018-02-03 06:51:07'),
(835, 75, 137, 'IMU-CET', 1219, 'General Knowledge', '-', '2018-02-03 06:51:07'),
(839, 75, 7, 'CLASS VII', 343, 'SANSKRIT', '-', '2018-02-03 06:51:31'),
(838, 75, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-03 06:51:31'),
(566, 90, 24, 'PERSONALITY DEVELOPMENT', 913, 'PERSONALITY DEVELOPMENT', '-', '2018-01-19 07:26:31'),
(565, 90, 8, 'CLASS VIII', 360, 'GENERAL KNOWLEDGE', '-', '2018-01-18 11:55:05'),
(564, 90, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2018-01-18 11:55:05'),
(563, 90, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-01-18 11:55:05'),
(562, 90, 8, 'CLASS VIII', 357, 'JAPANESE', '-', '2018-01-18 11:55:05'),
(561, 90, 8, 'CLASS VIII', 356, 'SANSKRIT', '-', '2018-01-18 11:55:05'),
(560, 90, 8, 'CLASS VIII', 355, 'GERMAN', '-', '2018-01-18 11:55:05'),
(559, 90, 8, 'CLASS VIII', 354, 'FRENCH', '-', '2018-01-18 11:55:05'),
(558, 90, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-01-18 11:55:05'),
(557, 90, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-01-18 11:55:05'),
(556, 90, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-01-18 11:55:05'),
(555, 90, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-01-18 11:55:05'),
(554, 90, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-18 11:55:05'),
(553, 90, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-18 11:55:05'),
(595, 90, 218, 'Computer Science  Engineering', 1637, 'Mathematics', '-', '2018-01-19 11:24:30'),
(596, 90, 218, 'Computer Science  Engineering', 1638, '	Physics', '-', '2018-01-19 11:24:30'),
(597, 90, 218, 'Computer Science  Engineering', 1639, '	Engineering Graphics', '-', '2018-01-19 11:24:30'),
(598, 90, 218, 'Computer Science  Engineering', 1640, '	Chemistry', '-', '2018-01-19 11:24:30'),
(599, 90, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2018-01-19 11:24:30'),
(600, 90, 218, 'Computer Science  Engineering', 1642, '	Elements of Electrical Engineering', '-', '2018-01-19 11:24:30'),
(601, 90, 218, 'Computer Science  Engineering', 1643, '	Basic Electronics', '-', '2018-01-19 11:24:30'),
(602, 90, 218, 'Computer Science  Engineering', 1644, '	Digital Systems', '-', '2018-01-19 11:24:30'),
(603, 90, 218, 'Computer Science  Engineering', 1645, '	Object Oriented Programming', '-', '2018-01-19 11:24:30'),
(604, 90, 218, 'Computer Science  Engineering', 1646, 'Applied Mathematics', '-', '2018-01-19 11:24:30'),
(605, 90, 218, 'Computer Science  Engineering', 1647, '	Data Communication', '-', '2018-01-19 11:24:30'),
(606, 90, 218, 'Computer Science  Engineering', 1648, 'Computer Organization', '-', '2018-01-19 11:24:30'),
(607, 90, 218, 'Computer Science  Engineering', 1650, 'Theory of Computation', '-', '2018-01-19 11:24:30'),
(608, 90, 218, 'Computer Science  Engineering', 1651, 'Database Management Systems', '-', '2018-01-19 11:24:30'),
(609, 90, 218, 'Computer Science  Engineering', 1652, '	Computer Networks', '-', '2018-01-19 11:24:30'),
(610, 90, 218, 'Computer Science  Engineering', 1649, 'Data Structures', '-', '2018-01-19 11:24:30'),
(611, 90, 218, 'Computer Science  Engineering', 1653, 'Operating Systems', '-', '2018-01-19 11:24:30'),
(612, 90, 218, 'Computer Science  Engineering', 1655, 'Software Engineering', '-', '2018-01-19 11:24:30'),
(613, 90, 218, 'Computer Science  Engineering', 1654, 'Web designing', '-', '2018-01-19 11:24:30'),
(614, 90, 218, 'Computer Science  Engineering', 1656, '.net Technologies', '-', '2018-01-19 11:24:30'),
(615, 90, 218, 'Computer Science  Engineering', 1657, 'Java Technologies', '-', '2018-01-19 11:24:30'),
(616, 90, 218, 'Computer Science  Engineering', 1658, 'Objective C Programming ', '-', '2018-01-19 11:24:30'),
(617, 90, 218, 'Computer Science  Engineering', 1659, 'System Software', '-', '2018-01-19 11:24:30'),
(618, 90, 218, 'Computer Science  Engineering', 1660, 'Mobile Applications Development Technologies', '-', '2018-01-19 11:24:30'),
(619, 90, 218, 'Computer Science  Engineering', 1661, 'Advanced Computer Networks', '-', '2018-01-19 11:24:30'),
(620, 90, 218, 'Computer Science  Engineering', 1662, 'Main Frame Systems', '-', '2018-01-19 11:24:30'),
(621, 90, 218, 'Computer Science  Engineering', 1663, 'Computer Graphics and Visualization ', '-', '2018-01-19 11:24:30'),
(622, 90, 218, 'Computer Science  Engineering', 1664, 'Software Testing ', '-', '2018-01-19 11:24:30'),
(623, 90, 218, 'Computer Science  Engineering', 1665, 'Cloud Computing', '-', '2018-01-19 11:24:30'),
(624, 90, 218, 'Computer Science  Engineering', 1666, 'Data Mining', '-', '2018-01-19 11:24:30'),
(625, 90, 218, 'Computer Science  Engineering', 1667, 'Artificial Intelligence', '-', '2018-01-19 11:24:30'),
(626, 90, 218, 'Computer Science  Engineering', 1668, 'Compiler Construction', '-', '2018-01-19 11:24:30'),
(627, 90, 218, 'Computer Science  Engineering', 1669, 'Interfacing with Microprocessor', '-', '2018-01-19 11:24:30'),
(628, 90, 218, 'Computer Science  Engineering', 1670, 'Digital Image Processing', '-', '2018-01-19 11:24:30'),
(629, 90, 218, 'Computer Science  Engineering', 1671, 'Web security', '-', '2018-01-19 11:24:30'),
(630, 90, 218, 'Computer Science  Engineering', 1672, 'Agile Software Development ', '-', '2018-01-19 11:24:30'),
(631, 90, 218, 'Computer Science  Engineering', 1673, 'Secure software engineering', '-', '2018-01-19 11:24:30'),
(632, 90, 218, 'Computer Science  Engineering', 1674, 'Network programming', '-', '2018-01-19 11:24:30'),
(633, 97, 94, 'JEE Advanced', 1119, 'Physics', '-', '2018-01-20 06:24:17'),
(634, 97, 94, 'JEE Advanced', 1120, 'Chemistry', '-', '2018-01-20 06:24:17'),
(635, 97, 94, 'JEE Advanced', 1121, 'Mathematics', '-', '2018-01-20 06:24:17'),
(636, 98, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-01-20 09:15:21'),
(637, 98, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-01-20 09:15:21'),
(638, 98, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-01-20 09:15:21'),
(639, 98, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-01-20 09:15:21'),
(640, 98, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-01-20 09:15:21'),
(641, 98, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-01-20 09:15:21'),
(642, 99, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-20 10:28:48'),
(643, 99, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-01-20 10:28:48'),
(644, 99, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-20 10:28:48'),
(645, 99, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-01-20 10:29:04'),
(646, 99, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-01-20 10:29:04'),
(647, 99, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-01-20 10:29:04'),
(648, 99, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-01-20 10:29:04'),
(649, 99, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-01-20 10:29:04'),
(650, 103, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-21 08:59:51'),
(651, 103, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-01-21 08:59:51'),
(652, 103, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-21 08:59:59'),
(653, 103, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-01-21 08:59:59'),
(654, 103, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-01-21 09:00:14'),
(655, 103, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-01-21 09:00:14'),
(656, 103, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-01-21 09:00:23'),
(657, 103, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-01-21 09:00:23'),
(658, 105, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-21 19:11:15'),
(659, 105, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-01-21 19:11:15'),
(660, 105, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-21 19:11:27'),
(661, 105, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-01-21 19:11:27'),
(662, 105, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-01-21 19:11:42'),
(663, 105, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-01-21 19:11:42'),
(664, 105, 11, 'CLASS XI', 49, 'INFORMATION TECHNOLOGY', '-', '2018-01-21 19:11:42'),
(665, 105, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-01-21 19:11:52'),
(666, 105, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-01-21 19:11:52'),
(667, 105, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-01-21 19:11:52'),
(668, 105, 93, 'Mathematical Hons.', 1089, 'Abstract Algebra', '-', '2018-01-21 19:12:33'),
(669, 105, 93, 'Mathematical Hons.', 1090, 'Numerical Analysis ', '-', '2018-01-21 19:12:33'),
(670, 105, 93, 'Mathematical Hons.', 1091, 'Differntial Geometry', '-', '2018-01-21 19:12:33'),
(671, 105, 93, 'Mathematical Hons.', 1092, 'Vector Analysis', '-', '2018-01-21 19:12:33'),
(672, 105, 93, 'Mathematical Hons.', 1088, 'Differntial Equations ', '-', '2018-01-21 19:12:33'),
(673, 105, 93, 'Mathematical Hons.', 1087, 'Integral Calculus And Trigonometry', '-', '2018-01-21 19:12:33'),
(674, 105, 215, 'BBA Tuition', 1774, 'Business Mathematics', '-', '2018-01-21 19:13:03'),
(675, 105, 215, 'BBA Tuition', 1777, 'Fundamentals of Information Technology', '-', '2018-01-21 19:13:24'),
(676, 105, 216, 'B.Com. Hons. Tuition', 1760, 'Business Mathematics', '-', '2018-01-21 19:13:36'),
(677, 105, 216, 'B.Com. Hons. Tuition', 1762, 'Computer Applications', '-', '2018-01-21 19:13:36'),
(678, 105, 13, 'PROGRAMMING LANGUAGE', 13, 'C++', '-', '2018-01-21 19:14:07'),
(679, 105, 14, 'APPLICATION SOFTWARE', 15, 'COMPUTER BASICS', '-', '2018-01-21 19:14:56'),
(680, 105, 14, 'APPLICATION SOFTWARE', 472, 'INFORMATION PRACTICE FOR CLASS XI', '-', '2018-01-21 19:14:56'),
(681, 105, 14, 'APPLICATION SOFTWARE', 476, 'MS OFFICE', '-', '2018-01-21 19:14:56'),
(682, 105, 14, 'APPLICATION SOFTWARE', 486, 'MS POWER POINT', '-', '2018-01-21 19:14:56'),
(683, 134, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-01-22 03:58:10'),
(684, 134, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-01-22 03:58:28'),
(685, 134, 99, 'Biotechnology', 1096, 'Molecular Biology', '-', '2018-01-22 03:58:56'),
(686, 134, 101, 'Biochemistry', 1102, 'Molecular Biology', '-', '2018-01-22 04:00:43'),
(687, 134, 106, 'B.Sc. Nursing', 1154, 'Microbiology', '-', '2018-01-22 04:01:21'),
(688, 134, 106, 'B.Sc. Nursing', 1152, 'Biochemistry', '-', '2018-01-22 04:01:21'),
(691, 49, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-01-22 05:33:12'),
(703, 74, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-23 12:41:24'),
(702, 74, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-23 12:41:24'),
(695, 101, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-01-23 10:53:33'),
(707, 107, 93, 'Mathematical Hons.', 1088, 'Differntial Equations ', '-', '2018-01-24 05:30:47'),
(706, 107, 93, 'Mathematical Hons.', 1087, 'Integral Calculus And Trigonometry', '-', '2018-01-24 05:30:47'),
(700, 87, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-23 11:11:23'),
(701, 87, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-01-23 11:11:23'),
(708, 107, 93, 'Mathematical Hons.', 1089, 'Abstract Algebra', '-', '2018-01-24 05:30:47'),
(709, 107, 93, 'Mathematical Hons.', 1090, 'Numerical Analysis ', '-', '2018-01-24 05:30:47'),
(710, 107, 93, 'Mathematical Hons.', 1091, 'Differntial Geometry', '-', '2018-01-24 05:30:47'),
(711, 107, 93, 'Mathematical Hons.', 1092, 'Vector Analysis', '-', '2018-01-24 05:30:47'),
(712, 117, 218, 'Computer Science  Engineering', 1639, '	Engineering Graphics', '-', '2018-01-24 19:46:38'),
(713, 117, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2018-01-24 19:46:38'),
(714, 117, 218, 'Computer Science  Engineering', 1648, 'Computer Organization', '-', '2018-01-24 19:46:38'),
(715, 117, 218, 'Computer Science  Engineering', 1649, 'Data Structures', '-', '2018-01-24 19:46:38'),
(716, 118, 93, 'Mathematical Hons.', 1087, 'Integral Calculus And Trigonometry', '-', '2018-01-25 08:47:54'),
(717, 118, 93, 'Mathematical Hons.', 1088, 'Differntial Equations ', '-', '2018-01-25 08:47:54'),
(718, 118, 93, 'Mathematical Hons.', 1090, 'Numerical Analysis ', '-', '2018-01-25 08:47:54'),
(719, 118, 93, 'Mathematical Hons.', 1089, 'Abstract Algebra', '-', '2018-01-25 08:47:54'),
(720, 118, 93, 'Mathematical Hons.', 1091, 'Differntial Geometry', '-', '2018-01-25 08:47:54'),
(721, 118, 93, 'Mathematical Hons.', 1092, 'Vector Analysis', '-', '2018-01-25 08:47:54'),
(722, 87, 208, 'NDA exam', 1580, 'General Aptitude', '-', '2018-01-25 10:04:55'),
(723, 87, 208, 'NDA exam', 1579, 'Mathematics', '-', '2018-01-25 10:04:55'),
(724, 135, 24, 'PERSONALITY DEVELOPMENT', 913, 'PERSONALITY DEVELOPMENT', '-', '2018-01-30 08:15:27'),
(725, 135, 25, 'COMPENSATION OF BENEFITS', 914, 'COMPENSATION OF BENEFITS', '-', '2018-01-30 08:15:48'),
(726, 24, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-30 11:32:11'),
(793, 154, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-02 07:59:05'),
(792, 154, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-02 07:58:54'),
(791, 136, 168, 'PUTHAT', 1343, 'General Awareness', '-', '2018-02-02 05:48:50'),
(790, 136, 168, 'PUTHAT', 1342, 'English Aptitude', '-', '2018-02-02 05:48:50'),
(733, 144, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-01-31 12:44:17'),
(734, 144, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-01-31 12:44:17'),
(735, 144, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-01-31 12:44:17'),
(736, 144, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-01-31 12:44:17'),
(737, 144, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-01-31 12:44:20'),
(738, 144, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-01-31 12:44:20'),
(739, 144, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-01-31 12:44:20'),
(740, 144, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-01-31 12:44:20'),
(741, 144, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-01-31 12:44:20'),
(742, 144, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-01-31 12:44:20'),
(743, 144, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-01-31 12:45:10'),
(744, 144, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-01-31 12:45:10'),
(745, 144, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-01-31 12:45:10'),
(746, 144, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-01-31 12:45:10'),
(747, 144, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-01-31 12:45:10'),
(748, 144, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-01-31 12:45:10'),
(749, 144, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-01-31 12:45:10'),
(750, 144, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-01-31 12:45:10'),
(765, 144, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-01-31 12:46:00'),
(764, 144, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-01-31 12:46:00'),
(763, 144, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-01-31 12:46:00'),
(762, 144, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-01-31 12:46:00'),
(761, 144, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-01-31 12:46:00'),
(760, 144, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-01-31 12:46:00'),
(759, 144, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-01-31 12:46:00'),
(766, 144, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-01-31 12:46:00'),
(767, 144, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-01-31 12:46:21'),
(768, 144, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-01-31 12:46:21'),
(769, 144, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-01-31 12:46:21'),
(770, 144, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-01-31 12:46:21'),
(771, 144, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-01-31 12:46:21'),
(772, 144, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-01-31 12:46:21'),
(773, 144, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-01-31 12:47:38'),
(774, 144, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-01-31 12:47:38'),
(775, 144, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-01-31 12:47:38'),
(776, 144, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-01-31 12:47:38'),
(777, 144, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-01-31 12:47:38'),
(778, 144, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-01-31 12:47:38'),
(779, 144, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-01-31 12:48:43'),
(780, 144, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-01-31 12:48:43'),
(781, 144, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-01-31 12:48:43'),
(782, 144, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-01-31 12:48:43'),
(783, 144, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-01-31 12:48:43'),
(784, 144, 7, 'CLASS VII', 340, 'CIVICS', '-', '2018-01-31 12:48:43'),
(785, 144, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-01-31 12:49:31'),
(786, 144, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-01-31 12:49:31'),
(787, 144, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-01-31 12:49:31'),
(788, 144, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-01-31 12:49:31'),
(789, 144, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-01-31 12:49:31'),
(794, 154, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-02 07:59:36'),
(795, 154, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-02 07:59:36'),
(796, 154, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-02 07:59:36'),
(797, 154, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-02 07:59:36'),
(798, 154, 218, 'Computer Science  Engineering', 1637, 'Mathematics', '-', '2018-02-02 08:00:13'),
(799, 154, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2018-02-02 08:00:13'),
(800, 154, 218, 'Computer Science  Engineering', 1649, 'Data Structures', '-', '2018-02-02 08:00:13'),
(801, 154, 218, 'Computer Science  Engineering', 1654, 'Web designing', '-', '2018-02-02 08:00:13'),
(802, 155, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-02 08:53:48'),
(803, 155, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-02 08:53:48'),
(804, 155, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-02 08:53:48'),
(805, 155, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-02 08:54:23'),
(806, 155, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-02 08:54:23'),
(807, 155, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-02 08:54:23'),
(808, 155, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-02 08:54:23'),
(809, 155, 177, 'SSC CHSL exam', 1497, 'General Intelligence & Reasoning', '-', '2018-02-02 08:55:02'),
(810, 155, 177, 'SSC CHSL exam', 1498, 'Quantitative Aptitude', '-', '2018-02-02 08:55:02'),
(811, 155, 177, 'SSC CHSL exam', 1499, 'Reasoning And Intelligence', '-', '2018-02-02 08:55:02'),
(812, 155, 177, 'SSC CHSL exam', 1500, 'English Language', '-', '2018-02-02 08:55:02'),
(813, 157, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-02 10:15:55'),
(814, 157, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-02 10:15:55'),
(815, 158, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-02 12:06:29'),
(816, 158, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-02 12:06:29'),
(817, 158, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-02 12:06:40'),
(818, 158, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-02 12:06:40'),
(819, 158, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-02 12:06:40'),
(834, 75, 137, 'IMU-CET', 1218, 'English', '-', '2018-02-03 06:51:07'),
(837, 75, 137, 'IMU-CET', 1217, 'Maths', '-', '2018-02-03 06:51:07'),
(848, 161, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-05 10:06:58'),
(849, 161, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-05 10:07:12'),
(850, 161, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-05 10:07:12'),
(851, 162, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-05 11:01:09'),
(852, 162, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-05 11:01:38'),
(853, 162, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-05 11:01:38'),
(854, 162, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-05 11:01:38'),
(855, 162, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-05 11:01:38'),
(856, 162, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-05 11:01:38'),
(857, 162, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-05 11:01:38'),
(858, 162, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-05 11:02:24'),
(859, 162, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-05 11:02:24'),
(860, 162, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-05 11:02:24'),
(861, 162, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-05 11:02:24'),
(862, 162, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-05 11:02:24'),
(863, 162, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-05 11:02:24'),
(864, 162, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-05 11:02:24'),
(865, 162, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-05 11:02:24'),
(866, 162, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-05 11:02:24'),
(867, 162, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-05 11:02:24'),
(868, 162, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-05 11:02:24'),
(869, 162, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-05 11:02:48'),
(870, 162, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-05 11:02:48'),
(871, 162, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-05 11:02:48'),
(872, 162, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-05 11:02:48'),
(873, 162, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-05 11:02:48'),
(874, 162, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-05 11:02:48');
INSERT INTO `teacher_professional_details` (`id`, `teacher_id`, `class_id`, `class_name`, `subject_id`, `subject_name`, `sub_topic`, `created`) VALUES
(875, 162, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-05 11:02:48'),
(876, 162, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-05 11:02:48'),
(877, 162, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-05 11:02:48'),
(878, 162, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-05 11:02:48'),
(879, 162, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-05 11:02:48'),
(880, 162, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-05 11:03:08'),
(881, 162, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-05 11:03:08'),
(882, 162, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-05 11:03:08'),
(883, 162, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-05 11:03:08'),
(884, 162, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-05 11:03:08'),
(885, 162, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-05 11:03:08'),
(886, 162, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-05 11:03:08'),
(887, 162, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-05 11:03:08'),
(888, 162, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-05 11:03:08'),
(889, 162, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-05 11:03:08'),
(890, 162, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-05 11:03:08'),
(891, 162, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-05 11:03:08'),
(892, 163, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-06 08:39:11'),
(893, 163, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-06 08:39:30'),
(894, 163, 176, 'SSC CGL exam', 1495, 'Quantitative Aptitude', '-', '2018-02-06 08:39:49'),
(897, 164, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-06 09:16:28'),
(898, 164, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-06 09:16:28'),
(899, 164, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-06 09:16:35'),
(900, 164, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-06 09:16:35'),
(1017, 165, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-07 23:55:04'),
(1042, 165, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-07 23:56:11'),
(1026, 165, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-07 23:55:43'),
(906, 166, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-06 10:16:07'),
(907, 166, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-06 10:16:07'),
(908, 166, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-06 10:16:07'),
(909, 166, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-06 10:16:07'),
(910, 166, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-06 10:16:07'),
(911, 166, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-06 10:16:07'),
(912, 166, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-06 10:16:35'),
(913, 166, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-06 10:16:35'),
(914, 166, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-06 10:16:35'),
(915, 166, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-06 10:16:35'),
(916, 166, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-06 10:16:35'),
(917, 166, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-06 10:16:35'),
(918, 166, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-06 10:16:35'),
(919, 166, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-06 10:17:24'),
(920, 166, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-06 10:17:24'),
(921, 166, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-06 10:17:24'),
(922, 166, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-06 10:17:24'),
(923, 166, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-06 10:17:24'),
(924, 166, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-06 10:17:24'),
(925, 166, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-06 10:17:46'),
(926, 166, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-06 10:17:46'),
(927, 166, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-06 10:17:46'),
(928, 166, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-06 10:17:46'),
(929, 166, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-06 10:17:46'),
(930, 166, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-06 10:17:46'),
(931, 166, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-06 10:17:46'),
(932, 166, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-06 10:17:46'),
(933, 166, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-06 10:17:46'),
(934, 166, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-06 10:17:46'),
(935, 166, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-06 10:18:06'),
(936, 166, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-06 10:18:06'),
(937, 166, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-06 10:18:06'),
(938, 166, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-06 10:18:06'),
(939, 166, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-06 10:18:06'),
(940, 166, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-06 10:18:06'),
(941, 166, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-06 10:18:06'),
(942, 166, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-06 10:18:06'),
(943, 166, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-06 10:18:06'),
(944, 166, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-06 10:18:06'),
(945, 167, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-06 10:42:03'),
(946, 167, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-06 10:42:03'),
(947, 167, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-06 10:42:03'),
(948, 167, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-06 10:42:24'),
(949, 167, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-06 10:42:24'),
(950, 167, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-06 10:42:24'),
(989, 167, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-07 03:30:14'),
(988, 167, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-07 03:30:14'),
(987, 167, 11, 'CLASS XI', 1846, 'HINDI', '-', '2018-02-07 03:30:14'),
(986, 167, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-07 03:30:14'),
(985, 167, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-07 03:30:14'),
(984, 167, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-07 03:30:14'),
(983, 167, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-07 03:30:14'),
(980, 167, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-06 15:26:09'),
(979, 167, 12, 'CLASS XII', 1862, 'HINDI', '-', '2018-02-06 15:26:09'),
(978, 167, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-06 15:26:09'),
(977, 167, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-06 15:26:09'),
(976, 167, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-06 15:26:09'),
(975, 167, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-06 15:26:09'),
(974, 167, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-06 15:26:09'),
(1019, 165, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-07 23:55:04'),
(1018, 165, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-07 23:55:04'),
(971, 171, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-06 13:23:52'),
(972, 171, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-06 13:23:52'),
(973, 171, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-06 13:23:52'),
(981, 167, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-06 15:26:09'),
(982, 167, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-06 15:26:09'),
(990, 167, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-07 03:30:14'),
(991, 167, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-07 03:30:14'),
(1000, 80, 10, 'CLASS X', 386, 'GENERAL KNOWLEDGE', '-', '2018-02-07 09:18:52'),
(999, 80, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-07 09:18:52'),
(998, 80, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-07 09:18:52'),
(1001, 80, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-07 09:18:52'),
(1002, 174, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-07 09:44:24'),
(1003, 174, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-07 09:44:24'),
(1004, 174, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-07 09:44:24'),
(1005, 174, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-07 09:44:34'),
(1006, 174, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-07 09:44:34'),
(1007, 174, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-07 09:44:34'),
(1008, 174, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-07 09:44:46'),
(1009, 174, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-07 09:44:46'),
(1010, 174, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-07 09:44:46'),
(1011, 174, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-07 09:45:18'),
(1012, 174, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-07 09:45:28'),
(1013, 80, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-07 11:42:22'),
(1014, 80, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-07 11:42:30'),
(1015, 80, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-07 11:46:55'),
(1016, 80, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-07 11:47:05'),
(1020, 165, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-07 23:55:04'),
(1021, 165, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-07 23:55:04'),
(1022, 165, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-02-07 23:55:04'),
(1023, 165, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-07 23:55:04'),
(1024, 165, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-07 23:55:04'),
(1025, 165, 9, 'CLASS IX', 373, 'GENERAL KNOWLEDGE', '-', '2018-02-07 23:55:04'),
(1027, 165, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-07 23:55:43'),
(1028, 165, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-07 23:55:43'),
(1029, 165, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-07 23:55:43'),
(1030, 165, 11, 'CLASS XI', 48, 'PHYSICAL EDUCATION', '-', '2018-02-07 23:55:43'),
(1031, 165, 11, 'CLASS XI', 49, 'INFORMATION TECHNOLOGY', '-', '2018-02-07 23:55:43'),
(1032, 165, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-07 23:55:43'),
(1033, 165, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-07 23:55:43'),
(1034, 165, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-07 23:55:43'),
(1035, 165, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-07 23:55:43'),
(1036, 165, 11, 'CLASS XI', 1846, 'HINDI', '-', '2018-02-07 23:55:43'),
(1037, 165, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-07 23:55:43'),
(1038, 165, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-07 23:55:43'),
(1039, 165, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-07 23:55:43'),
(1040, 165, 11, 'CLASS XI', 1852, 'SOCIOLOGY', '-', '2018-02-07 23:55:43'),
(1041, 165, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-07 23:55:43'),
(1043, 165, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-07 23:56:11'),
(1044, 165, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-07 23:56:11'),
(1045, 165, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-07 23:56:11'),
(1046, 165, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-07 23:56:11'),
(1047, 165, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-02-07 23:56:11'),
(1048, 165, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-07 23:56:11'),
(1049, 165, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-07 23:56:11'),
(1050, 165, 10, 'CLASS X', 386, 'GENERAL KNOWLEDGE', '-', '2018-02-07 23:56:11'),
(1051, 165, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-07 23:56:46'),
(1052, 165, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-07 23:56:46'),
(1053, 165, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-07 23:56:46'),
(1054, 165, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-07 23:56:46'),
(1055, 165, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-07 23:56:46'),
(1056, 165, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-07 23:56:46'),
(1057, 165, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-07 23:56:46'),
(1058, 165, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-07 23:56:46'),
(1059, 165, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-07 23:56:46'),
(1060, 165, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-07 23:56:46'),
(1061, 165, 12, 'CLASS XII', 1862, 'HINDI', '-', '2018-02-07 23:56:46'),
(1062, 165, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-07 23:56:46'),
(1063, 165, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-07 23:56:46'),
(1064, 165, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-07 23:56:46'),
(1065, 165, 12, 'CLASS XII', 1868, 'SOCIOLOGY', '-', '2018-02-07 23:56:46'),
(1066, 165, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-07 23:57:06'),
(1067, 165, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-07 23:57:06'),
(1068, 165, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-07 23:57:06'),
(1069, 165, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-07 23:57:06'),
(1070, 165, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-07 23:57:06'),
(1071, 165, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-07 23:57:06'),
(1072, 165, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-07 23:57:29'),
(1073, 165, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-07 23:57:29'),
(1074, 165, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-07 23:57:29'),
(1075, 165, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-07 23:57:29'),
(1076, 165, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-02-07 23:57:29'),
(1077, 165, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-02-07 23:57:29'),
(1078, 165, 6, 'CLASS VI', 332, 'HINDI', '-', '2018-02-07 23:57:29'),
(1079, 165, 6, 'CLASS VI', 333, 'COMPUTER', '-', '2018-02-07 23:57:29'),
(1080, 165, 6, 'CLASS VI', 334, 'GENERAL KNOWLEDGE', '-', '2018-02-07 23:57:29'),
(1081, 165, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-07 23:57:50'),
(1082, 165, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-02-07 23:57:50'),
(1083, 165, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-07 23:57:50'),
(1084, 165, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-07 23:57:50'),
(1085, 165, 7, 'CLASS VII', 340, 'CIVICS', '-', '2018-02-07 23:57:50'),
(1086, 165, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-02-07 23:57:50'),
(1087, 165, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-07 23:57:50'),
(1088, 165, 7, 'CLASS VII', 347, 'GENERAL KNOWLEDGE', '-', '2018-02-07 23:57:50'),
(1089, 165, 7, 'CLASS VII', 346, 'COMPUTER', '-', '2018-02-07 23:57:50'),
(1090, 165, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-07 23:58:06'),
(1091, 165, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-07 23:58:06'),
(1092, 165, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-07 23:58:06'),
(1093, 165, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-02-07 23:58:06'),
(1094, 165, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-02-07 23:58:06'),
(1095, 165, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-02-07 23:58:06'),
(1096, 165, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-02-07 23:58:06'),
(1097, 165, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2018-02-07 23:58:06'),
(1098, 165, 8, 'CLASS VIII', 360, 'GENERAL KNOWLEDGE', '-', '2018-02-07 23:58:06'),
(1099, 165, 217, 'B.A. Hons. (Economics)', 1750, 'Mathematical Methods for Economics', '-', '2018-02-07 23:59:25'),
(1100, 165, 217, 'B.A. Hons. (Economics)', 1748, 'Introductory Macroeconomics', '-', '2018-02-07 23:59:25'),
(1101, 165, 217, 'B.A. Hons. (Economics)', 1745, 'Introductory Microeconomics', '-', '2018-02-07 23:59:25'),
(1102, 165, 217, 'B.A. Hons. (Economics)', 1751, 'Economic Theory', '-', '2018-02-07 23:59:25'),
(1103, 165, 217, 'B.A. Hons. (Economics)', 1752, 'Macroeconomics and International Trade', '-', '2018-02-07 23:59:25'),
(1104, 165, 217, 'B.A. Hons. (Economics)', 1753, 'Money and Financial Systems', '-', '2018-02-07 23:59:25'),
(1105, 165, 217, 'B.A. Hons. (Economics)', 1754, 'Indian Economy Since Independence', '-', '2018-02-07 23:59:25'),
(1106, 165, 217, 'B.A. Hons. (Economics)', 1755, 'Economic Systems', '-', '2018-02-07 23:59:25'),
(1107, 165, 217, 'B.A. Hons. (Economics)', 1756, 'Public Finance', '-', '2018-02-07 23:59:25'),
(1108, 165, 217, 'B.A. Hons. (Economics)', 1757, 'Comparative Economic Development', '-', '2018-02-07 23:59:25'),
(1109, 165, 216, 'B.Com. Hons. Tuition', 1759, 'Elements of Commerce', '-', '2018-02-08 00:00:07'),
(1110, 165, 216, 'B.Com. Hons. Tuition', 1760, 'Business Mathematics', '-', '2018-02-08 00:00:07'),
(1111, 165, 216, 'B.Com. Hons. Tuition', 1761, 'Principles of Micro Economics', '-', '2018-02-08 00:00:07'),
(1112, 165, 216, 'B.Com. Hons. Tuition', 1762, 'Computer Applications', '-', '2018-02-08 00:00:07'),
(1113, 165, 216, 'B.Com. Hons. Tuition', 1764, 'Business Statistics', '-', '2018-02-08 00:00:07'),
(1114, 165, 216, 'B.Com. Hons. Tuition', 1765, 'Principles of Macro Economics', '-', '2018-02-08 00:00:07'),
(1115, 165, 216, 'B.Com. Hons. Tuition', 1766, 'Business Communication', '-', '2018-02-08 00:00:07'),
(1116, 175, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-08 04:28:34'),
(1117, 175, 6, 'CLASS VI', 334, 'GENERAL KNOWLEDGE', '-', '2018-02-08 04:29:17'),
(1118, 175, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-08 04:29:17'),
(1119, 175, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-08 04:29:17'),
(1120, 175, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-08 04:29:17'),
(1121, 175, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-08 04:29:38'),
(1122, 175, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-08 04:29:38'),
(1123, 175, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-08 04:29:38'),
(1124, 175, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-08 04:29:38'),
(1125, 175, 7, 'CLASS VII', 346, 'COMPUTER', '-', '2018-02-08 04:29:38'),
(1126, 175, 7, 'CLASS VII', 347, 'GENERAL KNOWLEDGE', '-', '2018-02-08 04:29:38'),
(1127, 175, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-08 04:29:53'),
(1128, 175, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-08 04:29:53'),
(1129, 175, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-08 04:29:53'),
(1446, 209, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-11 07:53:24'),
(1131, 177, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-08 06:21:04'),
(1132, 177, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-08 06:21:04'),
(1133, 177, 128, 'AILET', 1173, 'Legal Aptitude', '-', '2018-02-08 06:23:17'),
(1134, 177, 128, 'AILET', 1174, 'Logical Reasoning', '-', '2018-02-08 06:23:17'),
(1436, 188, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-11 02:46:45'),
(1322, 192, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-09 10:01:14'),
(1445, 209, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-11 07:53:08'),
(1440, 188, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-11 02:47:02'),
(1441, 209, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-11 07:52:35'),
(1442, 209, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-11 07:52:35'),
(1443, 209, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-11 07:52:47'),
(1444, 209, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-11 07:52:47'),
(1435, 188, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-11 02:46:38'),
(1176, 180, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-08 10:59:11'),
(1175, 180, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-08 10:59:11'),
(1174, 180, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-08 10:59:03'),
(1173, 180, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-08 10:59:03'),
(1172, 180, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-08 10:59:03'),
(1171, 180, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-08 10:58:41'),
(1170, 177, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-08 09:21:35'),
(1169, 177, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-08 09:21:35'),
(1177, 180, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-08 10:59:11'),
(1178, 180, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-08 10:59:37'),
(1179, 180, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-08 10:59:37'),
(1180, 180, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-08 10:59:37'),
(1181, 180, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-08 10:59:37'),
(1182, 180, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-08 10:59:37'),
(1183, 180, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-08 10:59:56'),
(1184, 180, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-08 10:59:56'),
(1185, 180, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-08 10:59:56'),
(1186, 180, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-08 10:59:56'),
(1187, 180, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-08 10:59:56'),
(1188, 180, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-08 10:59:56'),
(1189, 180, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-08 10:59:56'),
(1190, 183, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-08 11:34:06'),
(1191, 183, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-08 11:34:06'),
(1192, 183, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-08 11:34:21'),
(1193, 183, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-08 11:34:21'),
(1194, 183, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-08 11:34:21'),
(1195, 184, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-08 12:23:49'),
(1196, 184, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-08 12:23:49'),
(1197, 184, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-08 12:24:06'),
(1198, 184, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-08 12:24:06'),
(1199, 184, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-08 12:24:22'),
(1200, 184, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-08 12:24:22'),
(1201, 184, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-08 12:24:34'),
(1202, 184, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-08 12:24:34'),
(1203, 136, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-08 12:39:31'),
(1204, 136, 12, 'CLASS XII', 408, 'PHYSICAL EDUCATION', '-', '2018-02-08 12:39:31'),
(1205, 136, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-08 12:39:31'),
(1206, 136, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-08 12:39:31'),
(1207, 136, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-08 12:39:31'),
(1208, 136, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-08 12:39:31'),
(1209, 136, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-08 12:39:31'),
(1210, 136, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-08 12:39:31'),
(1211, 136, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-08 12:39:31'),
(1212, 136, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-08 12:39:31'),
(1213, 136, 12, 'CLASS XII', 1862, 'HINDI', '-', '2018-02-08 12:39:31'),
(1214, 136, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-08 12:39:31'),
(1215, 136, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-08 12:39:31'),
(1216, 136, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-08 12:39:31'),
(1217, 136, 12, 'CLASS XII', 1866, 'MUSIC', '-', '2018-02-08 12:39:31'),
(1218, 136, 12, 'CLASS XII', 1867, 'PSYCHOLOGY', '-', '2018-02-08 12:39:31'),
(1219, 136, 12, 'CLASS XII', 1868, 'SOCIOLOGY', '-', '2018-02-08 12:39:31'),
(1220, 136, 12, 'CLASS XII', 1870, 'SANSKRIT', '-', '2018-02-08 12:39:31'),
(1221, 136, 12, 'CLASS XII', 1871, 'ANTHROPOLOGY', '-', '2018-02-08 12:39:31'),
(1222, 136, 12, 'CLASS XII', 1872, 'ARCHAEOLOGY', '-', '2018-02-08 12:39:31'),
(1223, 136, 12, 'CLASS XII', 1869, 'FASHION STUDIES', '-', '2018-02-08 12:39:31'),
(1323, 192, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-09 10:01:31'),
(1451, 154, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-02-12 08:23:35'),
(1450, 154, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-12 08:23:35'),
(1447, 154, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-12 08:23:35'),
(1452, 154, 6, 'CLASS VI', 333, 'COMPUTER', '-', '2018-02-12 08:23:35'),
(1238, 136, 218, 'Computer Science  Engineering', 1637, 'Mathematics', '-', '2018-02-09 06:18:39'),
(1239, 136, 218, 'Computer Science  Engineering', 1638, '	Physics', '-', '2018-02-09 06:18:39'),
(1240, 136, 218, 'Computer Science  Engineering', 1639, '	Engineering Graphics', '-', '2018-02-09 06:18:39'),
(1241, 136, 218, 'Computer Science  Engineering', 1640, '	Chemistry', '-', '2018-02-09 06:18:39'),
(1242, 136, 218, 'Computer Science  Engineering', 1641, '	Computer Programming', '-', '2018-02-09 06:18:39'),
(1243, 136, 218, 'Computer Science  Engineering', 1642, '	Elements of Electrical Engineering', '-', '2018-02-09 06:18:39'),
(1244, 136, 218, 'Computer Science  Engineering', 1643, '	Basic Electronics', '-', '2018-02-09 06:18:39'),
(1245, 136, 218, 'Computer Science  Engineering', 1644, '	Digital Systems', '-', '2018-02-09 06:18:39'),
(1246, 136, 218, 'Computer Science  Engineering', 1645, '	Object Oriented Programming', '-', '2018-02-09 06:18:39'),
(1247, 136, 218, 'Computer Science  Engineering', 1647, '	Data Communication', '-', '2018-02-09 06:18:39'),
(1248, 136, 218, 'Computer Science  Engineering', 1648, 'Computer Organization', '-', '2018-02-09 06:18:39'),
(1249, 136, 218, 'Computer Science  Engineering', 1646, 'Applied Mathematics', '-', '2018-02-09 06:18:39'),
(1250, 136, 218, 'Computer Science  Engineering', 1649, 'Data Structures', '-', '2018-02-09 06:18:39'),
(1251, 136, 218, 'Computer Science  Engineering', 1651, 'Database Management Systems', '-', '2018-02-09 06:18:39'),
(1252, 136, 218, 'Computer Science  Engineering', 1653, 'Operating Systems', '-', '2018-02-09 06:18:39'),
(1253, 136, 218, 'Computer Science  Engineering', 1654, 'Web designing', '-', '2018-02-09 06:18:39'),
(1254, 136, 218, 'Computer Science  Engineering', 1652, '	Computer Networks', '-', '2018-02-09 06:18:39'),
(1255, 136, 218, 'Computer Science  Engineering', 1655, 'Software Engineering', '-', '2018-02-09 06:18:39'),
(1256, 136, 218, 'Computer Science  Engineering', 1656, '.net Technologies', '-', '2018-02-09 06:18:39'),
(1257, 136, 218, 'Computer Science  Engineering', 1657, 'Java Technologies', '-', '2018-02-09 06:18:39'),
(1258, 136, 218, 'Computer Science  Engineering', 1658, 'Objective C Programming ', '-', '2018-02-09 06:18:39'),
(1259, 136, 218, 'Computer Science  Engineering', 1659, 'System Software', '-', '2018-02-09 06:18:39'),
(1260, 136, 218, 'Computer Science  Engineering', 1660, 'Mobile Applications Development Technologies', '-', '2018-02-09 06:18:39'),
(1261, 136, 218, 'Computer Science  Engineering', 1661, 'Advanced Computer Networks', '-', '2018-02-09 06:18:39'),
(1262, 136, 218, 'Computer Science  Engineering', 1662, 'Main Frame Systems', '-', '2018-02-09 06:18:39'),
(1263, 136, 218, 'Computer Science  Engineering', 1664, 'Software Testing ', '-', '2018-02-09 06:18:39'),
(1264, 136, 218, 'Computer Science  Engineering', 1663, 'Computer Graphics and Visualization ', '-', '2018-02-09 06:18:39'),
(1265, 136, 218, 'Computer Science  Engineering', 1665, 'Cloud Computing', '-', '2018-02-09 06:18:39'),
(1266, 136, 218, 'Computer Science  Engineering', 1666, 'Data Mining', '-', '2018-02-09 06:18:39'),
(1267, 136, 218, 'Computer Science  Engineering', 1667, 'Artificial Intelligence', '-', '2018-02-09 06:18:39'),
(1268, 136, 218, 'Computer Science  Engineering', 1668, 'Compiler Construction', '-', '2018-02-09 06:18:39'),
(1269, 136, 218, 'Computer Science  Engineering', 1669, 'Interfacing with Microprocessor', '-', '2018-02-09 06:18:39'),
(1270, 136, 218, 'Computer Science  Engineering', 1670, 'Digital Image Processing', '-', '2018-02-09 06:18:39'),
(1271, 136, 218, 'Computer Science  Engineering', 1671, 'Web security', '-', '2018-02-09 06:18:39'),
(1272, 136, 218, 'Computer Science  Engineering', 1672, 'Agile Software Development ', '-', '2018-02-09 06:18:39'),
(1273, 136, 218, 'Computer Science  Engineering', 1673, 'Secure software engineering', '-', '2018-02-09 06:18:39'),
(1274, 136, 218, 'Computer Science  Engineering', 1674, 'Network programming', '-', '2018-02-09 06:18:39'),
(1275, 74, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-09 06:37:13'),
(1276, 74, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-09 06:37:13'),
(1277, 74, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-09 06:37:13'),
(1278, 189, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-09 07:17:13'),
(1279, 189, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-09 07:17:13'),
(1280, 189, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-09 07:17:13'),
(1281, 189, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-09 07:17:13'),
(1282, 189, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-09 07:17:27'),
(1283, 189, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-09 07:17:27'),
(1284, 189, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-09 07:17:27'),
(1285, 189, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-09 07:17:27'),
(1286, 189, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-09 07:17:50'),
(1287, 189, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-09 07:17:50'),
(1288, 189, 11, 'CLASS XI', 49, 'INFORMATION TECHNOLOGY', '-', '2018-02-09 07:17:50'),
(1289, 189, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-09 07:17:50'),
(1290, 189, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-09 07:18:04'),
(1291, 189, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-09 07:18:04'),
(1292, 189, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-09 07:18:04'),
(1293, 189, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-09 07:18:04'),
(1294, 190, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-09 07:24:50'),
(1295, 190, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-09 07:24:50'),
(1296, 190, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-09 07:25:06'),
(1297, 190, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-09 07:25:06'),
(1298, 182, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-09 08:04:29'),
(1299, 182, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-09 08:04:55'),
(1300, 182, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-09 08:04:55'),
(1301, 182, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-09 08:04:55'),
(1455, 154, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-02-12 08:23:49'),
(1454, 154, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-12 08:23:49'),
(1453, 154, 6, 'CLASS VI', 334, 'GENERAL KNOWLEDGE', '-', '2018-02-12 08:23:35'),
(1449, 154, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-12 08:23:35'),
(1448, 154, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-12 08:23:35'),
(1324, 192, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-09 10:01:31'),
(1325, 192, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-09 10:01:31'),
(1326, 192, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-09 10:01:46'),
(1327, 192, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-09 10:01:46'),
(1328, 192, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-09 10:01:46'),
(1329, 192, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-09 10:02:03'),
(1330, 192, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-09 10:02:03'),
(1331, 192, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-09 10:02:03'),
(1332, 193, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-09 10:08:34'),
(1333, 193, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-09 10:08:34'),
(1334, 193, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-09 10:08:34'),
(1335, 193, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-09 10:08:43'),
(1336, 193, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-09 10:08:43'),
(1337, 193, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-09 10:08:43'),
(1338, 193, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-09 10:08:51'),
(1339, 193, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-09 10:08:51'),
(1340, 193, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-09 10:08:51'),
(1341, 193, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-09 10:09:30'),
(1342, 193, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-09 10:09:30'),
(1343, 193, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-09 10:09:30'),
(1344, 193, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-09 10:09:30'),
(1345, 193, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-09 10:09:30'),
(1346, 193, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-09 10:09:30'),
(1347, 193, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-09 10:09:30'),
(1348, 193, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-09 10:09:51'),
(1349, 193, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-09 10:09:51'),
(1350, 193, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-09 10:09:51'),
(1351, 193, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-09 10:09:51'),
(1352, 193, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-09 10:09:51'),
(1353, 193, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-09 10:09:51'),
(1354, 193, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-09 10:09:51'),
(1355, 194, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-09 10:40:24'),
(1356, 194, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-09 10:40:24'),
(1357, 194, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-09 10:40:35'),
(1358, 194, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-09 10:40:35'),
(1359, 197, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-09 11:51:58'),
(1360, 197, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-09 11:51:58'),
(1361, 197, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-09 11:51:58'),
(1362, 197, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-09 11:51:58'),
(1363, 197, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-09 11:51:58'),
(1364, 197, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-09 11:51:58'),
(1365, 197, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-09 11:52:14'),
(1366, 197, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-09 11:52:14'),
(1367, 197, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-09 11:52:14'),
(1368, 197, 6, 'CLASS VI', 330, 'SANSKRIT', '-', '2018-02-09 11:52:14'),
(1369, 197, 6, 'CLASS VI', 332, 'HINDI', '-', '2018-02-09 11:52:14'),
(1370, 197, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-09 11:52:30'),
(1371, 197, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-09 11:52:30'),
(1372, 197, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-09 11:52:30'),
(1373, 197, 7, 'CLASS VII', 343, 'SANSKRIT', '-', '2018-02-09 11:52:30'),
(1374, 197, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-09 11:52:30'),
(1375, 197, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-09 11:52:43'),
(1376, 197, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-09 11:52:43'),
(1377, 197, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-09 11:52:43'),
(1378, 197, 8, 'CLASS VIII', 356, 'SANSKRIT', '-', '2018-02-09 11:52:43'),
(1379, 197, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-02-09 11:52:43'),
(1380, 198, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-09 11:53:31'),
(1439, 188, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-11 02:47:02'),
(1437, 188, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-11 02:46:54'),
(1438, 188, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-11 02:46:54'),
(1393, 200, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-09 12:56:51'),
(1394, 200, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-09 12:57:25'),
(1395, 200, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-09 12:57:25'),
(1396, 200, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-09 12:57:25'),
(1397, 200, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-09 12:57:25'),
(1398, 200, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-09 12:57:25'),
(1399, 200, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-02-09 12:57:25'),
(1400, 200, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-09 12:57:25'),
(1401, 200, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-09 12:57:25'),
(1402, 200, 9, 'CLASS IX', 373, 'GENERAL KNOWLEDGE', '-', '2018-02-09 12:57:25'),
(1403, 200, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-09 12:58:17'),
(1404, 200, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-09 12:58:17'),
(1405, 200, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-09 12:58:17'),
(1406, 200, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-09 12:58:17'),
(1407, 200, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-09 12:58:17'),
(1408, 200, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-09 12:58:17'),
(1409, 200, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-09 12:59:03'),
(1410, 200, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-09 12:59:03'),
(1411, 200, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-09 12:59:03'),
(1412, 200, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-09 12:59:03'),
(1413, 200, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-09 12:59:03'),
(1414, 200, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-09 12:59:03'),
(1415, 200, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-09 12:59:03'),
(1416, 200, 11, 'CLASS XI', 49, 'INFORMATION TECHNOLOGY', '-', '2018-02-09 12:59:03'),
(1417, 200, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-09 12:59:03'),
(1418, 200, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-09 12:59:03'),
(1419, 200, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-09 12:59:03'),
(1420, 200, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-09 12:59:03'),
(1421, 200, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-09 12:59:03'),
(1422, 200, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-09 12:59:36'),
(1423, 200, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-09 12:59:36'),
(1424, 200, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-09 12:59:36'),
(1425, 200, 12, 'CLASS XII', 1875, 'INFORMATION TECHNOLOGY', '-', '2018-02-09 12:59:36'),
(1426, 200, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-09 12:59:36'),
(1427, 200, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-09 12:59:36'),
(1428, 200, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-09 12:59:36'),
(1429, 200, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-09 12:59:36'),
(1430, 200, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-09 12:59:36'),
(1431, 200, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-09 12:59:36'),
(1432, 200, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-09 12:59:36'),
(1433, 200, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-09 12:59:36'),
(1434, 200, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-09 12:59:36'),
(1456, 154, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-12 08:23:49'),
(1457, 154, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-12 08:23:49'),
(1458, 154, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-02-12 08:23:49'),
(1459, 154, 7, 'CLASS VII', 346, 'COMPUTER', '-', '2018-02-12 08:23:49'),
(1460, 154, 7, 'CLASS VII', 347, 'GENERAL KNOWLEDGE', '-', '2018-02-12 08:23:49'),
(1461, 154, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-12 08:24:02'),
(1462, 154, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-12 08:24:02'),
(1463, 154, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-12 08:24:02'),
(1464, 154, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-02-12 08:24:02'),
(1465, 154, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-02-12 08:24:02'),
(1466, 154, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2018-02-12 08:24:02'),
(1467, 154, 8, 'CLASS VIII', 360, 'GENERAL KNOWLEDGE', '-', '2018-02-12 08:24:02'),
(1468, 154, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-12 08:24:18'),
(1469, 154, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-12 08:24:18'),
(1470, 154, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-12 08:24:18'),
(1471, 154, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-12 08:24:18'),
(1472, 154, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-12 08:24:18'),
(1473, 154, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-12 08:24:18'),
(1474, 154, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-12 08:24:31'),
(1475, 154, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-12 08:24:31'),
(1476, 154, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-12 08:24:31'),
(1477, 154, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-12 08:24:31'),
(1478, 154, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-12 08:24:31'),
(1479, 154, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-12 08:24:31'),
(1480, 154, 10, 'CLASS X', 386, 'GENERAL KNOWLEDGE', '-', '2018-02-12 08:24:31'),
(1481, 154, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-12 08:25:07'),
(1482, 154, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-12 08:25:07'),
(1483, 154, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-12 08:25:07'),
(1484, 154, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-12 08:25:07'),
(1485, 154, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-12 08:25:07'),
(1486, 154, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-12 08:25:07'),
(1487, 154, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-12 08:25:07'),
(1488, 154, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-12 08:25:07'),
(1489, 154, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-12 08:25:29'),
(1490, 154, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-12 08:25:29'),
(1491, 154, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-12 08:25:29'),
(1492, 154, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-12 08:25:29'),
(1493, 154, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-12 08:25:29'),
(1494, 154, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-12 08:25:29'),
(1495, 154, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-12 08:25:29'),
(1496, 154, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-12 08:25:29'),
(1497, 154, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-12 08:25:29'),
(1498, 154, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-12 08:25:29'),
(1499, 154, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-12 08:25:29'),
(1500, 154, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-12 08:25:29'),
(1640, 219, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-13 08:16:00'),
(1502, 152, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-12 11:03:34'),
(1503, 152, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-12 11:03:44'),
(1504, 152, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-12 11:03:52'),
(1505, 152, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-12 11:04:01'),
(1506, 152, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-12 11:04:11'),
(1507, 152, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-12 11:04:47'),
(1508, 213, 216, 'B.Com. Hons. Tuition', 1758, 'Financial Accounting', '-', '2018-02-12 11:34:17'),
(1509, 213, 216, 'B.Com. Hons. Tuition', 1759, 'Elements of Commerce', '-', '2018-02-12 11:34:17'),
(1510, 213, 216, 'B.Com. Hons. Tuition', 1763, 'Costing', '-', '2018-02-12 11:34:17'),
(1511, 213, 216, 'B.Com. Hons. Tuition', 1761, 'Principles of Micro Economics', '-', '2018-02-12 11:34:17'),
(1512, 213, 216, 'B.Com. Hons. Tuition', 1769, 'Company Law', '-', '2018-02-12 11:34:17'),
(1513, 213, 216, 'B.Com. Hons. Tuition', 1771, 'Financial Management', '-', '2018-02-12 11:34:17'),
(1514, 213, 216, 'B.Com. Hons. Tuition', 1772, 'Direct Tax Laws', '-', '2018-02-12 11:34:17'),
(1515, 213, 216, 'B.Com. Hons. Tuition', 1768, 'Cost and Management Accounting', '-', '2018-02-12 11:34:17'),
(1637, 176, 115, 'AFMC', 1477, 'Chemistry', '-', '2018-02-13 06:58:17'),
(1529, 176, 97, 'SRMJEEE', 1134, 'Mathematics', '-', '2018-02-12 12:08:36'),
(1923, 176, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-14 09:47:10'),
(1533, 176, 89, 'Physics hons.', 1077, 'Electricity and Magnetism', '-', '2018-02-12 12:08:57'),
(1534, 176, 89, 'Physics hons.', 1105, 'Thermal Physics', '-', '2018-02-12 12:08:57'),
(1532, 176, 89, 'Physics hons.', 1076, ' Chemistry', '-', '2018-02-12 12:08:57'),
(1531, 176, 89, 'Physics hons.', 1075, 'Mechanics', '-', '2018-02-12 12:08:57'),
(1530, 176, 97, 'SRMJEEE', 1133, 'Chemistry', '-', '2018-02-12 12:08:36'),
(1638, 219, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-13 08:12:38'),
(1639, 219, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 08:16:00'),
(1921, 176, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-14 09:47:10'),
(1541, 215, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-12 12:42:29'),
(1670, 219, 7, 'CLASS VII', 340, 'CIVICS', '-', '2018-02-13 08:16:43'),
(1544, 176, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-12 12:54:10'),
(1545, 176, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-12 12:54:10'),
(1546, 216, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-12 13:24:38'),
(1547, 216, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-12 13:24:38'),
(1548, 216, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-12 13:24:38'),
(1549, 218, 92, 'JEE Mains', 1110, 'Physics', '-', '2018-02-12 18:00:59'),
(1647, 219, 9, 'CLASS IX', 369, 'SANSKRIT', '-', '2018-02-13 08:16:00'),
(1922, 176, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-14 09:47:10'),
(1645, 219, 9, 'CLASS IX', 367, 'FRENCH', '-', '2018-02-13 08:16:00'),
(1646, 219, 9, 'CLASS IX', 368, 'GERMAN', '-', '2018-02-13 08:16:00'),
(1641, 219, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 08:16:00'),
(1642, 219, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-13 08:16:00'),
(1643, 219, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-13 08:16:00'),
(1644, 219, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-02-13 08:16:00'),
(1669, 219, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-02-13 08:16:43'),
(1668, 219, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-13 08:16:43'),
(1667, 219, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-13 08:16:43'),
(1666, 219, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-02-13 08:16:43'),
(1665, 219, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-13 08:16:43'),
(1663, 219, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2018-02-13 08:16:29'),
(1664, 219, 8, 'CLASS VIII', 360, 'GENERAL KNOWLEDGE', '-', '2018-02-13 08:16:29'),
(1662, 219, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-02-13 08:16:29'),
(1661, 219, 8, 'CLASS VIII', 357, 'JAPANESE', '-', '2018-02-13 08:16:29'),
(1660, 219, 8, 'CLASS VIII', 356, 'SANSKRIT', '-', '2018-02-13 08:16:29'),
(1659, 219, 8, 'CLASS VIII', 355, 'GERMAN', '-', '2018-02-13 08:16:29'),
(1658, 219, 8, 'CLASS VIII', 354, 'FRENCH', '-', '2018-02-13 08:16:29'),
(1657, 219, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-02-13 08:16:29'),
(1656, 219, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-02-13 08:16:29'),
(1655, 219, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-02-13 08:16:29'),
(1654, 219, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-13 08:16:29'),
(1653, 219, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-13 08:16:29'),
(1652, 219, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-13 08:16:29'),
(1651, 219, 9, 'CLASS IX', 373, 'GENERAL KNOWLEDGE', '-', '2018-02-13 08:16:00'),
(1650, 219, 9, 'CLASS IX', 372, 'COMPUTER', '-', '2018-02-13 08:16:00'),
(1649, 219, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-13 08:16:00'),
(1937, 243, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-16 10:08:50'),
(1648, 219, 9, 'CLASS IX', 370, 'JAPANESE', '-', '2018-02-13 08:16:00'),
(1996, 250, 167, 'WBJEE JEHOM', 1341, 'Elementary Mathematics', '-', '2018-03-16 11:20:06'),
(1671, 219, 7, 'CLASS VII', 341, 'FRENCH', '-', '2018-02-13 08:16:43'),
(1672, 219, 7, 'CLASS VII', 342, 'GERMAN', '-', '2018-02-13 08:16:43'),
(1673, 219, 7, 'CLASS VII', 343, 'SANSKRIT', '-', '2018-02-13 08:16:43'),
(1674, 219, 7, 'CLASS VII', 344, 'JAPANESE', '-', '2018-02-13 08:16:43'),
(1675, 219, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-13 08:16:43'),
(1676, 219, 7, 'CLASS VII', 346, 'COMPUTER', '-', '2018-02-13 08:16:43'),
(1677, 219, 7, 'CLASS VII', 347, 'GENERAL KNOWLEDGE', '-', '2018-02-13 08:16:43'),
(1678, 219, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-13 08:17:00'),
(1679, 219, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-13 08:17:00'),
(1680, 219, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-13 08:17:00'),
(1681, 219, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-13 08:17:00'),
(1682, 219, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-02-13 08:17:00'),
(1683, 219, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-02-13 08:17:00'),
(1684, 219, 6, 'CLASS VI', 328, 'FRENCH', '-', '2018-02-13 08:17:00'),
(1685, 219, 6, 'CLASS VI', 329, 'GERMAN', '-', '2018-02-13 08:17:00'),
(1686, 219, 6, 'CLASS VI', 330, 'SANSKRIT', '-', '2018-02-13 08:17:00'),
(1687, 219, 6, 'CLASS VI', 331, 'JAPANESE', '-', '2018-02-13 08:17:00'),
(1688, 219, 6, 'CLASS VI', 332, 'HINDI', '-', '2018-02-13 08:17:00'),
(1689, 219, 6, 'CLASS VI', 333, 'COMPUTER', '-', '2018-02-13 08:17:00'),
(1690, 219, 6, 'CLASS VI', 334, 'GENERAL KNOWLEDGE', '-', '2018-02-13 08:17:00'),
(1691, 219, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-13 08:17:09'),
(1692, 219, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-13 08:17:09'),
(1693, 219, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-13 08:17:09'),
(1694, 219, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-13 08:17:09'),
(1695, 219, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-13 08:17:09'),
(1696, 219, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-13 08:17:09'),
(1697, 219, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-13 08:17:47'),
(1698, 219, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-13 08:17:47'),
(1699, 219, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-13 08:17:47'),
(1700, 219, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-13 08:17:47'),
(1701, 219, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-13 08:17:47'),
(1702, 219, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-13 08:17:47'),
(1703, 219, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-13 08:17:47'),
(1704, 219, 11, 'CLASS XI', 1845, 'BIOLOGY', '-', '2018-02-13 08:17:47'),
(1705, 219, 11, 'CLASS XI', 1846, 'HINDI', '-', '2018-02-13 08:17:47'),
(1706, 219, 11, 'CLASS XI', 1847, 'HISTORY', '-', '2018-02-13 08:17:47'),
(1707, 219, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-13 08:17:47'),
(1708, 219, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-13 08:17:47'),
(1709, 219, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-13 08:17:47'),
(1710, 219, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-13 08:18:17'),
(1711, 219, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-13 08:18:17'),
(1712, 219, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-13 08:18:17'),
(1713, 219, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-13 08:18:17'),
(1714, 219, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-13 08:18:17'),
(1715, 219, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-13 08:18:17'),
(1716, 219, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-13 08:18:17'),
(1717, 219, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-13 08:18:17'),
(1718, 219, 12, 'CLASS XII', 1862, 'HINDI', '-', '2018-02-13 08:18:17'),
(1719, 219, 12, 'CLASS XII', 1863, 'HISTORY', '-', '2018-02-13 08:18:17'),
(1720, 219, 12, 'CLASS XII', 1864, 'GEOGRAPHY', '-', '2018-02-13 08:18:17'),
(1721, 219, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-13 08:18:17'),
(1722, 219, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 08:18:41'),
(1723, 219, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-13 08:18:41'),
(1724, 219, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 08:18:41'),
(1725, 219, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-13 08:18:41'),
(1726, 219, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-13 08:18:41'),
(1727, 219, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-02-13 08:18:41'),
(1728, 219, 10, 'CLASS X', 380, 'FRENCH', '-', '2018-02-13 08:18:41'),
(1729, 219, 10, 'CLASS X', 381, 'GERMAN', '-', '2018-02-13 08:18:41'),
(1730, 219, 10, 'CLASS X', 383, 'JAPANESE', '-', '2018-02-13 08:18:41'),
(1731, 219, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-13 08:18:41'),
(1732, 219, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-13 08:18:41'),
(1733, 219, 10, 'CLASS X', 386, 'GENERAL KNOWLEDGE', '-', '2018-02-13 08:18:41'),
(1734, 138, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-13 09:47:48'),
(1735, 138, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-13 09:47:48'),
(1736, 138, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-13 09:47:48'),
(1737, 138, 1, 'CLASSES I-V', 1877, 'CLASS II', '-', '2018-02-13 09:47:48'),
(1738, 138, 1, 'CLASSES I-V', 1876, 'CLASS I', '-', '2018-02-13 09:47:48'),
(1739, 138, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-13 09:47:48'),
(1740, 138, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 09:48:22'),
(1741, 138, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-13 09:48:22');
INSERT INTO `teacher_professional_details` (`id`, `teacher_id`, `class_id`, `class_name`, `subject_id`, `subject_name`, `sub_topic`, `created`) VALUES
(1742, 138, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 09:48:22'),
(1743, 138, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-13 09:48:22'),
(1744, 138, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-13 09:48:22'),
(1745, 138, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-02-13 09:48:22'),
(1746, 138, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-13 09:48:22'),
(1747, 138, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 09:48:38'),
(1748, 138, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-13 09:48:38'),
(1749, 138, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 09:48:38'),
(1750, 138, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-13 09:48:38'),
(1751, 138, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-02-13 09:48:38'),
(1752, 138, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-13 09:48:38'),
(1753, 138, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-13 09:48:38'),
(1754, 138, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-13 09:48:48'),
(1755, 138, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-13 09:48:56'),
(1756, 222, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-13 10:19:30'),
(1757, 222, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-13 10:19:30'),
(1758, 222, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-13 10:19:48'),
(1759, 222, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-13 10:19:48'),
(1760, 113, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-13 10:29:41'),
(1761, 113, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-13 10:29:53'),
(1762, 225, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 11:22:27'),
(1763, 225, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 11:22:36'),
(1764, 225, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-13 11:22:46'),
(1765, 225, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-13 11:22:56'),
(1766, 225, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-13 11:22:56'),
(1767, 225, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-13 11:23:05'),
(1768, 225, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-13 11:23:05'),
(1769, 228, 1, 'CLASSES I-V', 1878, 'CLASS III', '-', '2018-02-13 13:01:34'),
(1770, 228, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-02-13 13:01:34'),
(1771, 228, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-02-13 13:01:34'),
(1772, 228, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-13 13:01:57'),
(1773, 228, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-13 13:01:57'),
(1774, 228, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-13 13:01:57'),
(1775, 228, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-13 13:01:57'),
(1776, 228, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-02-13 13:01:57'),
(1777, 228, 6, 'CLASS VI', 333, 'COMPUTER', '-', '2018-02-13 13:01:57'),
(1778, 228, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-13 13:02:12'),
(1779, 228, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-02-13 13:02:12'),
(1780, 228, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-13 13:02:12'),
(1781, 228, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-13 13:02:12'),
(1782, 228, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-02-13 13:02:12'),
(1783, 228, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-13 13:02:12'),
(1784, 228, 7, 'CLASS VII', 346, 'COMPUTER', '-', '2018-02-13 13:02:12'),
(1785, 228, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-13 13:02:26'),
(1786, 228, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-13 13:02:26'),
(1787, 228, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-13 13:02:26'),
(1788, 228, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-02-13 13:02:26'),
(1789, 228, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-02-13 13:02:26'),
(1790, 228, 8, 'CLASS VIII', 359, 'COMPUTER', '-', '2018-02-13 13:02:26'),
(1791, 228, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-02-13 13:02:26'),
(1792, 228, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 13:02:40'),
(1793, 228, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-13 13:02:40'),
(1794, 228, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 13:02:40'),
(1795, 228, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-13 13:02:40'),
(1796, 228, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-13 13:02:40'),
(1797, 228, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 13:02:53'),
(1798, 228, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-13 13:02:53'),
(1799, 228, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 13:02:53'),
(1800, 228, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-13 13:02:53'),
(1801, 228, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-13 13:02:53'),
(1802, 228, 10, 'CLASS X', 385, 'COMPUTER', '-', '2018-02-13 13:02:53'),
(1803, 228, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-13 13:02:53'),
(1804, 228, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-13 13:03:25'),
(1805, 228, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-13 13:03:25'),
(1806, 228, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-13 13:03:25'),
(1807, 228, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-13 13:03:43'),
(1808, 228, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-13 13:03:43'),
(1809, 228, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-13 13:03:43'),
(1810, 230, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-13 13:27:01'),
(1811, 230, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-13 13:27:01'),
(1812, 230, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-13 13:27:17'),
(1813, 230, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-13 13:27:17'),
(1814, 230, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 13:27:27'),
(1815, 230, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 13:27:35'),
(1819, 229, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 13:40:49'),
(1818, 229, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 13:40:49'),
(1820, 229, 220, 'Mechanical Engineering', 1581, 'Mathematics', '-', '2018-02-13 13:44:30'),
(1821, 232, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 14:14:39'),
(1822, 232, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-13 14:14:39'),
(1823, 232, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 14:14:39'),
(1824, 232, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 14:14:51'),
(1825, 232, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-13 14:14:51'),
(1826, 232, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 14:14:51'),
(1827, 232, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-13 14:14:51'),
(1828, 232, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-13 14:14:51'),
(1829, 232, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-13 14:15:13'),
(1830, 232, 11, 'CLASS XI', 44, 'BUSINESS STUDIES', '-', '2018-02-13 14:15:13'),
(1831, 232, 11, 'CLASS XI', 46, 'ENGLISH', '-', '2018-02-13 14:15:13'),
(1832, 232, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-13 14:15:13'),
(1833, 232, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-13 14:15:13'),
(1834, 232, 11, 'CLASS XI', 1843, 'CHEMISTRY', '-', '2018-02-13 14:15:13'),
(1835, 232, 11, 'CLASS XI', 1844, 'COMPUTER SCIENCE', '-', '2018-02-13 14:15:13'),
(1836, 232, 11, 'CLASS XI', 1848, 'GEOGRAPHY', '-', '2018-02-13 14:15:13'),
(1837, 232, 11, 'CLASS XI', 1849, 'POLITICAL SCIENCE', '-', '2018-02-13 14:15:13'),
(1838, 232, 11, 'CLASS XI', 1857, 'ECONOMICS', '-', '2018-02-13 14:15:13'),
(1839, 232, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-13 14:15:34'),
(1840, 232, 12, 'CLASS XII', 65, 'BUSINESS STUDIES', '-', '2018-02-13 14:15:34'),
(1841, 232, 12, 'CLASS XII', 1873, 'ECONOMICS', '-', '2018-02-13 14:15:34'),
(1842, 232, 12, 'CLASS XII', 406, 'ENGLISH', '-', '2018-02-13 14:15:34'),
(1843, 232, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-13 14:15:34'),
(1844, 232, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-13 14:15:34'),
(1845, 232, 12, 'CLASS XII', 1859, 'CHEMISTRY', '-', '2018-02-13 14:15:34'),
(1846, 232, 12, 'CLASS XII', 1860, 'COMPUTER SCIENCE', '-', '2018-02-13 14:15:34'),
(1847, 232, 12, 'CLASS XII', 1861, 'BIOLOGY', '-', '2018-02-13 14:15:34'),
(1848, 232, 12, 'CLASS XII', 1865, 'POLITICAL SCIENCE', '-', '2018-02-13 14:15:34'),
(1849, 234, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-13 17:29:20'),
(1850, 234, 9, 'CLASS IX', 362, 'ENGLISH', '-', '2018-02-13 17:29:20'),
(1851, 234, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-13 17:29:20'),
(1852, 234, 9, 'CLASS IX', 364, 'HISTORY', '-', '2018-02-13 17:29:20'),
(1853, 234, 9, 'CLASS IX', 365, 'GEOGRAPHY', '-', '2018-02-13 17:29:20'),
(1854, 234, 9, 'CLASS IX', 366, 'CIVICS', '-', '2018-02-13 17:29:20'),
(1855, 234, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-13 17:29:20'),
(1856, 234, 8, 'CLASS VIII', 358, 'HINDI', '-', '2018-02-13 17:29:42'),
(1857, 234, 8, 'CLASS VIII', 353, 'CIVICS', '-', '2018-02-13 17:29:42'),
(1858, 234, 8, 'CLASS VIII', 352, 'GEOGRAPHY', '-', '2018-02-13 17:29:42'),
(1859, 234, 8, 'CLASS VIII', 351, 'HISTORY', '-', '2018-02-13 17:29:42'),
(1860, 234, 8, 'CLASS VIII', 348, 'MATHEMATICS', '-', '2018-02-13 17:29:42'),
(1861, 234, 8, 'CLASS VIII', 349, 'ENGLISH', '-', '2018-02-13 17:29:42'),
(1862, 234, 8, 'CLASS VIII', 350, 'SCIENCE', '-', '2018-02-13 17:29:42'),
(1863, 234, 7, 'CLASS VII', 31, 'MATHEMATICS', '-', '2018-02-13 17:29:57'),
(1864, 234, 7, 'CLASS VII', 336, 'ENGLISH', '-', '2018-02-13 17:29:57'),
(1865, 234, 7, 'CLASS VII', 63, 'HISTORY', '-', '2018-02-13 17:29:57'),
(1866, 234, 7, 'CLASS VII', 339, 'GEOGRAPHY', '-', '2018-02-13 17:29:57'),
(1867, 234, 7, 'CLASS VII', 337, 'SCIENCE', '-', '2018-02-13 17:29:57'),
(1868, 234, 7, 'CLASS VII', 340, 'CIVICS', '-', '2018-02-13 17:29:57'),
(1869, 234, 7, 'CLASS VII', 345, 'HINDI', '-', '2018-02-13 17:29:57'),
(1870, 234, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-13 17:30:11'),
(1871, 234, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-02-13 17:30:11'),
(1872, 234, 6, 'CLASS VI', 324, 'SCIENCE', '-', '2018-02-13 17:30:11'),
(1873, 234, 6, 'CLASS VI', 325, 'HISTORY', '-', '2018-02-13 17:30:11'),
(1874, 234, 6, 'CLASS VI', 326, 'GEOGRAPHY', '-', '2018-02-13 17:30:11'),
(1875, 234, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-02-13 17:30:11'),
(1876, 234, 6, 'CLASS VI', 332, 'HINDI', '-', '2018-02-13 17:30:11'),
(1877, 234, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-13 17:31:17'),
(1878, 234, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-13 17:31:17'),
(1879, 234, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-13 17:31:17'),
(1880, 234, 10, 'CLASS X', 377, 'HISTORY', '-', '2018-02-13 17:31:17'),
(1881, 234, 10, 'CLASS X', 378, 'GEOGRAPHY', '-', '2018-02-13 17:31:17'),
(1882, 234, 10, 'CLASS X', 379, 'CIVICS', '-', '2018-02-13 17:31:17'),
(1883, 234, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-13 17:31:17'),
(1884, 236, 9, 'CLASS IX', 361, 'MATHEMATICS', '-', '2018-02-14 08:37:27'),
(1885, 236, 9, 'CLASS IX', 363, 'SCIENCE', '-', '2018-02-14 08:37:27'),
(1886, 236, 9, 'CLASS IX', 371, 'HINDI', '-', '2018-02-14 08:37:27'),
(1889, 236, 10, 'CLASS X', 374, 'MATHEMATICS', '-', '2018-02-14 08:37:52'),
(1890, 236, 10, 'CLASS X', 376, 'SCIENCE', '-', '2018-02-14 08:37:52'),
(1891, 236, 10, 'CLASS X', 384, 'HINDI', '-', '2018-02-14 08:37:52'),
(1892, 236, 11, 'CLASS XI', 47, 'MATHEMATICS', '-', '2018-02-14 08:38:13'),
(1893, 236, 11, 'CLASS XI', 1842, 'PHYSICS', '-', '2018-02-14 08:38:13'),
(1894, 236, 12, 'CLASS XII', 407, 'MATHEMATICS', '-', '2018-02-14 08:38:29'),
(1895, 236, 12, 'CLASS XII', 1858, 'PHYSICS', '-', '2018-02-14 08:38:29'),
(1896, 237, 14, 'APPLICATION SOFTWARE', 15, 'COMPUTER BASICS', '-', '2018-02-14 08:51:25'),
(1897, 237, 14, 'APPLICATION SOFTWARE', 457, 'PHOTOSHOP', '-', '2018-02-14 08:51:25'),
(1898, 237, 14, 'APPLICATION SOFTWARE', 459, 'DATA ENTRY', '-', '2018-02-14 08:51:25'),
(1899, 237, 14, 'APPLICATION SOFTWARE', 469, 'DIGITAL MARKETING', '-', '2018-02-14 08:51:25'),
(1900, 237, 14, 'APPLICATION SOFTWARE', 470, 'CYBER SECURITY', '-', '2018-02-14 08:51:25'),
(1901, 237, 14, 'APPLICATION SOFTWARE', 473, 'INTERNET', '-', '2018-02-14 08:51:25'),
(1902, 237, 14, 'APPLICATION SOFTWARE', 477, 'MS EXCEL', '-', '2018-02-14 08:51:25'),
(1903, 237, 14, 'APPLICATION SOFTWARE', 476, 'MS OFFICE', '-', '2018-02-14 08:51:25'),
(1904, 237, 14, 'APPLICATION SOFTWARE', 486, 'MS POWER POINT', '-', '2018-02-14 08:51:25'),
(1905, 237, 14, 'APPLICATION SOFTWARE', 485, 'WORDPRESS', '-', '2018-02-14 08:51:25'),
(1906, 237, 14, 'APPLICATION SOFTWARE', 472, 'INFORMATION PRACTICE FOR CLASS XI', '-', '2018-02-14 08:51:25'),
(1907, 237, 14, 'APPLICATION SOFTWARE', 463, 'PAGE MAKER', '-', '2018-02-14 08:51:25'),
(1908, 237, 14, 'APPLICATION SOFTWARE', 460, 'VISUAL BASIC', '-', '2018-02-14 08:51:25'),
(1909, 237, 14, 'APPLICATION SOFTWARE', 456, 'CORAL DRAW', '-', '2018-02-14 08:51:25'),
(1910, 237, 14, 'APPLICATION SOFTWARE', 455, 'INFORMATION PRACTICE FOR CLASS XII', '-', '2018-02-14 08:51:25'),
(1911, 238, 11, 'CLASS XI', 43, 'ACCOUNTANCY', '-', '2018-02-14 09:28:58'),
(1912, 238, 12, 'CLASS XII', 64, 'ACCOUNTANCY', '-', '2018-02-14 09:29:08'),
(1987, 249, 14, 'APPLICATION SOFTWARE', 15, 'COMPUTER BASICS', '-', '2018-03-16 09:26:48'),
(1920, 176, 6, 'CLASS VI', 322, 'MATHEMATICS', '-', '2018-02-14 09:47:10'),
(1924, 176, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-02-14 09:47:10'),
(1984, 241, 148, 'MAT ', 1265, 'Intelligence And Critical Reasoning', '-', '2018-03-14 10:34:14'),
(1934, 176, 30, 'TEAM BUILDING', 919, 'TEAM BUILDING', '-', '2018-02-14 12:45:34'),
(1938, 243, 1, 'CLASSES I-V', 1881, 'ALL CLASSES', '-', '2018-02-16 10:08:50'),
(1945, 241, 97, 'SRMJEEE', 1134, 'Mathematics', '-', '2018-02-24 05:58:11'),
(1944, 241, 97, 'SRMJEEE', 1133, 'Chemistry', '-', '2018-02-24 05:58:11'),
(1990, 249, 30, 'TEAM BUILDING', 919, 'TEAM BUILDING', '-', '2018-03-16 09:26:58'),
(1946, 241, 97, 'SRMJEEE', 1132, 'Physics', '-', '2018-02-24 05:58:11'),
(1988, 249, 14, 'APPLICATION SOFTWARE', 16, 'LINUX', '-', '2018-03-16 09:26:48'),
(1978, 248, 6, 'CLASS VI', 327, 'CIVICS', '-', '2018-03-10 09:27:31'),
(1953, 241, 28, 'NEGOTIATION', 917, 'NEGOTIATION', '-', '2018-02-27 05:51:24'),
(1989, 249, 14, 'APPLICATION SOFTWARE', 455, 'INFORMATION PRACTICE FOR CLASS XII', '-', '2018-03-16 09:26:48'),
(1958, 241, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-27 05:51:59'),
(1959, 241, 10, 'CLASS X', 375, 'ENGLISH', '-', '2018-02-27 05:55:54'),
(1981, 241, 94, 'JEE Advanced', 1121, 'Mathematics', '-', '2018-03-14 10:33:52'),
(1983, 241, 148, 'MAT ', 1264, 'Data Analysis And Sufficiency', '-', '2018-03-14 10:34:14'),
(1965, 241, 142, 'AFCAT', 1238, 'Verbal Ability', '-', '2018-02-28 09:57:08'),
(1966, 241, 142, 'AFCAT', 1239, 'Numerical Ability', '-', '2018-02-28 09:57:08'),
(1967, 241, 194, 'RBI exam', 1563, 'English', '-', '2018-03-09 08:29:33'),
(1982, 241, 13, 'PROGRAMMING LANGUAGE', 12, 'CORE JAVA', '-', '2018-03-14 10:34:01'),
(1995, 250, 112, 'GUJCET', 1469, 'Biology', '-', '2018-03-16 11:19:57'),
(1977, 248, 6, 'CLASS VI', 323, 'ENGLISH', '-', '2018-03-10 09:27:31'),
(1975, 241, 182, 'RRB NTPC exam', 1518, 'Arithmetic Ability', '-', '2018-03-09 08:30:34'),
(1976, 241, 182, 'RRB NTPC exam', 1517, 'General Science', '-', '2018-03-09 08:30:34'),
(1994, 250, 112, 'GUJCET', 1468, 'Chemistry', '-', '2018-03-16 11:19:57'),
(1997, 250, 167, 'WBJEE JEHOM', 1340, 'Logical Reasoning', '-', '2018-03-16 11:20:06'),
(1998, 250, 26, 'LINEAR PIRATE TRAINING', 915, 'LINEAR PIRATE TRAINING', '-', '2018-03-16 11:20:19'),
(1999, 253, 1, 'CLASSES I-V', 1879, 'CLASS IV', '-', '2018-04-04 13:20:40'),
(2000, 253, 1, 'CLASSES I-V', 1880, 'CLASS V', '-', '2018-04-04 13:20:40'),
(2001, 253, 264, 'German', 1893, 'German', '-', '2018-04-04 13:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_qualification`
--

CREATE TABLE `teacher_qualification` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `qualification` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_question`
--

CREATE TABLE `teacher_question` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `ans_type` varchar(255) NOT NULL,
  `option1` varchar(255) DEFAULT NULL,
  `option2` varchar(255) DEFAULT NULL,
  `option3` varchar(255) DEFAULT NULL,
  `option4` varchar(255) DEFAULT NULL,
  `option5` varchar(255) DEFAULT NULL,
  `answer` varchar(10) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_question`
--

INSERT INTO `teacher_question` (`id`, `teacher_id`, `batch_id`, `question`, `ans_type`, `option1`, `option2`, `option3`, `option4`, `option5`, `answer`, `created`) VALUES
(1, 1, 1, 'what is your name', 'multiple', 'swetank', 'helloo', NULL, NULL, NULL, '1', '2017-12-20 11:36:13'),
(2, 1, 2, 'what is your namw', 'multiple', 'swe', 'amit', 'vikash', 'naved', 'aman', '1', '2017-12-21 08:32:55'),
(3, 1, 2, 'whay is your name', 'multiple', 'vikas', 'arjun', NULL, NULL, NULL, '1', '2017-12-21 08:33:51'),
(4, 1, 2, 'what is your name', 'multiple', '21', '22', '23', '24', NULL, '1', '2017-12-21 12:46:21'),
(5, 18, 3, 'bdbbd', 'multiple', '1', '2', NULL, NULL, NULL, '1', '2017-12-22 11:18:01'),
(6, 26, 6, 'population of india is', 'multiple', '130', '135', '140', '150', NULL, '1', '2017-12-23 09:31:53'),
(7, 28, 7, 'Is Earth round??', 'multiple', 'yes', 'no', 'maybe', 'no idea', NULL, '1', '2017-12-23 12:13:16'),
(8, 30, 10, 'No of bones in human body', 'multiple', '207', '208', '206', '209', NULL, '3', '2017-12-24 09:22:12'),
(9, 2, 5, 'is earth round?', 'multiple', 'yes', 'no', 'maybe', NULL, NULL, '1', '2017-12-24 13:53:57'),
(10, 46, 14, 'who is the father of modern biology', 'multiple', 'Mendel', 'aristotle', 'Watson crick', NULL, NULL, '2', '2017-12-26 13:03:07'),
(11, 2, 15, 'is earth round??', 'multiple', 'yes', 'no', NULL, NULL, NULL, '1', '2018-01-09 10:35:39'),
(12, 9, 1, 'what is your age?', 'multiple', '21', '22', '23', '24', NULL, '3', '2018-01-10 12:09:02'),
(13, 9, 1, 'what is your name?', 'multiple', '21', '22', NULL, NULL, NULL, '1', '2018-01-10 12:12:32'),
(14, 9, 1, 'What is your name?', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-11 05:56:08'),
(15, 79, 41, 'hdhddh', 'multiple', 'hes', 'dhd', 'hdhd', NULL, NULL, '2', '2018-01-15 10:39:07'),
(16, 79, 41, 'what is yout', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-15 10:39:27'),
(17, 90, 46, 'hhdhhdhjjjsjsjnnsngktsktzktktsktzkslslzkzlzlzkzktstzktzkzlzlyzyztzjtstTztztztzktzkss6gkzllylyzkslylsl6dltzl5zlgysls6d6d5srstsydtsysk6ldl6dl6dldl6d6d6d6d6ldlyxldl6d6s6dldldl6dldls5stxykxktzlyxld6d6xlyxkyxk6xk6x6lxld', 'multiple', 'hdhddgxtstsktststsksktsststtzttztztzktztztzkzkzkzkzkzlxkyzykzkskzlzl6z6kz6kz6zlzlyzlslxlsllzl6zl6zdl6l6sl6sodl6d6ldldldl6dl6sj5zjtstnzktsksys6s6ksk6sysydystsys', 'gmxmzydydllylydylsysyykysskydysktydydjtalsktayydrRydjttstsydykhchcgznzydnrkyydkskdk6dkydkysysysksktskyskyskskdky', NULL, NULL, NULL, '1', '2018-01-19 08:37:14'),
(18, 90, 46, 'jdhdjdhjdjddddjdhdkykskskysl6slsl6sldlyyskudktsiktysk6sldl6dyldkdydydydkdydydydysggsghfgxyysdgdydittsrzksydj5surtysyduDsdsyjtzdrarTzlyxxkyktsydjsydtslsldlydllddjdhddjdkdwowdjjjddjddjdiddiiwoidhxhddhdiijdjdddddddjdjdjdjdjdjdididjdjddjjdjdjdddddisiidhddhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-19 08:42:43'),
(198, 75, 159, 'Q3 for multiple', 'multiple', 'ufjj', 'chhcgj', NULL, NULL, NULL, '2', '2018-04-04 10:23:55'),
(196, 75, 159, 'Q2 for multiple', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:23:55'),
(197, 75, 160, 'Q2 for multiple', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:23:55'),
(22, 99, 56, 'what is refraction', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-21 08:02:26'),
(195, 75, 160, 'Q1 for multiple', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:23:55'),
(193, 75, 160, 'jcckjc', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:07:28'),
(194, 75, 159, 'Q1 for multiple', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:23:55'),
(191, 75, 160, 'dil3', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:05:46'),
(192, 75, 159, 'xhcj', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:06:52'),
(189, 75, 160, 'dil', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 10:05:46'),
(190, 75, 160, 'dil 2', 'multiple', 'd', 'r', 'g', NULL, NULL, '3', '2018-04-04 10:05:46'),
(188, 75, 160, 'fjjvvjvj', 'multiple', 'vgjjhghj', '\'h]v]vv]j[v', 'vohjph', NULL, NULL, '3', '2018-04-04 09:59:14'),
(186, 75, 160, 'xhcjcj', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 09:59:14'),
(187, 75, 160, 'ffuuv', 'multiple', 'cc', 'ghh', NULL, NULL, NULL, '1', '2018-04-04 09:59:14'),
(184, 75, 160, 'ghc', 'multiple', 'hhj', 'gbd', NULL, NULL, NULL, '2', '2018-04-04 09:56:30'),
(185, 75, 160, 'ghfbd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-04 09:56:48'),
(182, 75, 0, 'chchjvvu', 'multiple', 'vuvjvj', 'vjjj', NULL, NULL, NULL, '2', '2018-04-02 10:30:07'),
(183, 75, 160, 'fufjvgjgk xyyg', 'multiple', 'g', 'g', 'f', NULL, NULL, '2', '2018-04-04 09:32:25'),
(180, 75, 0, 'yxddcuixxgxgxt xtxyyx', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 10:27:16'),
(181, 75, 0, 'cigggixc nahi ccy', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 10:27:16'),
(178, 75, 0, 'vividh icivh', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 09:45:40'),
(179, 75, 0, 'cjvyvu', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 10:26:57'),
(176, 75, 0, 'hcjcv', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 09:43:47'),
(177, 75, 0, 'xhcuc', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-04-02 09:43:47'),
(174, 250, 210, 'ggbb', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 11:35:01'),
(175, 250, 210, 'vv', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 11:35:01'),
(168, 241, 165, 'chh', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-15 11:14:56'),
(169, 241, 166, 'hudud', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 06:51:35'),
(170, 241, 167, 'heje', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 09:07:11'),
(171, 241, 167, 'wheie', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 09:07:11'),
(172, 241, 167, 'hehu', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 09:07:11'),
(173, 249, 168, 'hwhwn', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-16 09:41:40'),
(155, 241, 144, 'ahwh', 'multiple', 'ma', '.am', NULL, NULL, NULL, '2', '2018-02-28 09:36:37'),
(156, 176, 204, 'hnwk', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-06 06:19:24'),
(157, 176, 204, 'vw', 'multiple', 'vs', 'hs', NULL, NULL, NULL, '1', '2018-03-06 06:19:24'),
(158, 241, 162, 'vv', 'multiple', 'ff', 'usbaksnwkwnwnwjw', '', NULL, NULL, '2', '2018-03-09 10:42:12'),
(159, 241, 162, 'tum pagal ho', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-09 10:47:03'),
(160, 241, 162, 'snwj', 'multiple', 'hs', 'hsuw', NULL, NULL, NULL, '2', '2018-03-12 07:29:49'),
(161, 241, 162, 'fhbvdi', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-12 07:30:48'),
(162, 241, 162, 'ffg', 'multiple', 'gg', 'ggg', NULL, NULL, NULL, '1', '2018-03-12 07:30:50'),
(163, 241, 162, 'scv', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-14 10:03:55'),
(164, 241, 162, 'xdtthu', 'multiple', 'cjk', 'gnh', NULL, NULL, NULL, '1', '2018-03-14 10:04:25'),
(165, 241, 165, 'bsbsnsnw', 'multiple', 'baja', 'bsja', NULL, NULL, NULL, '2', '2018-03-15 09:57:08'),
(166, 241, 165, 'ns w', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-03-15 09:57:08'),
(167, 241, 165, 'baba', 'multiple', 'bsbs', 'baja', NULL, NULL, NULL, '2', '2018-03-15 09:57:09'),
(37, 74, 64, 'What is your name', 'multiple', 'Swe', 'Amit', 'Vikas', 'Saurabh', 'Naved', '1', '2018-01-24 09:05:58'),
(38, 74, 64, 'What is your age', 'multiple', '19', '20', '21', '22', '23', '5', '2018-01-24 09:08:00'),
(154, 241, 158, 'gyufd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-28 09:34:34'),
(40, 49, 104, 'this is question', 'multiple', '1', '2', '4', '7', '6', '3', '2018-01-24 10:49:47'),
(41, 49, 104, 'This is short question answer', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-24 10:50:19'),
(42, 49, 104, 'idkfhkf', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-24 13:38:35'),
(43, 9, 118, 'what is maiden name??', 'multiple', 'naved', 'aman', 'amit', NULL, NULL, '1', '2018-01-25 05:35:59'),
(44, 9, 118, 'what is yout dog name', 'multiple', 'jdjdjd', 'ndndndndn', 'jdjd', 'ndndnd', 'ndndn', '5', '2018-01-25 06:03:01'),
(45, 9, 118, 'whshhsdhhdhdhdhdjdjdjdjddndndndndjdjdjjddjdjdjjdjdjdjdjdjdlslsjdjdjjddjdkdksksjsjjsjssjkskskskskjsjsjsjjdjdj', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:27:19'),
(46, 49, 104, 'udxjfjf', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:32:37'),
(47, 49, 104, 'nxjxhmcmf', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:37:36'),
(48, 87, 114, 'Hdjdn', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:51:41'),
(49, 49, 104, 'itxhxtjg', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:56:22'),
(50, 49, 104, 'djgdhjjdykdt', 'multiple', 'fhhf', 'BBC hhbb hc', NULL, NULL, NULL, '2', '2018-01-25 06:56:50'),
(51, 49, 104, 'itxhxtjg', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 06:57:13'),
(52, 87, 114, 'Iibsohiv', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 07:09:32'),
(53, 49, 104, 'lhclgkg', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 07:36:05'),
(54, 87, 114, 'Chl be', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 07:41:38'),
(55, 87, 114, 'What is naved second name', 'multiple', 'Bak', 'Bakooo', 'Bakiol', 'Baklol', 'Lol', '1', '2018-01-25 08:49:22'),
(56, 87, 114, 'What is naved pet name>>', 'multiple', '1', '2', '3', '4', '5', '5', '2018-01-25 08:54:30'),
(57, 87, 114, 'What is jdjd', 'multiple', 'Gdh', 'Hdhd', 'Hdhd', 'Bdhd', 'Bdbd', '3', '2018-01-25 08:54:53'),
(58, 87, 114, 'What is your bamejj', 'multiple', 'Hdhd', 'Hdhd', 'Hdbdh', 'Bdbhd', 'Bdbd', '5', '2018-01-25 08:55:50'),
(59, 87, 114, 'What is your name', 'multiple', '21', '22', '23', '24', '25', '3', '2018-01-25 09:09:28'),
(60, 87, 114, 'What is dilemaaaa', 'multiple', 'Abstract', 'Smiley', 'Methaphot', 'Doodle', 'Navedinisation', '5', '2018-01-25 09:11:11'),
(61, 87, 114, 'What is meaning of dkdjd', 'multiple', 'Hdjd', 'Hdhdhd', 'Hdhdhd', 'Bdhd', 'Bdbdb', '1', '2018-01-25 09:17:40'),
(62, 87, 114, 'What is kallu ????? name', 'multiple', '21', '22', '23.', '2626.', 'Hhdhdhd', '3', '2018-01-25 09:22:27'),
(63, 87, 114, 'What is my name', 'multiple', 'Swetank', 'Amit', 'Saurabh', 'Naved', 'Aman', '4', '2018-01-25 09:23:25'),
(64, 87, 114, 'What is your name', 'multiple', '21', '22', '23', '24', '25', '1', '2018-01-25 09:26:43'),
(65, 87, 114, 'Nsnsbsbs', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:37:56'),
(66, 87, 114, 'Djbsvsvs', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:38:38'),
(67, 87, 114, 'Zbzb', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:45:16'),
(68, 87, 114, 'Sjbs z s  su', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:52:30'),
(69, 87, 114, 'What is yoor name', 'multiple', '21', '22', '23', '24', '25', '3', '2018-01-25 09:55:59'),
(70, 87, 114, 'Bzbsjsnsb', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:58:17'),
(71, 87, 114, 'Bebsbsb', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 09:58:49'),
(72, 87, 114, 'Jdjdjddjjd', 'multiple', 'Jdjd', 'Jdhd', 'Bdbbd', 'Bdbd', 'Ndbdn', '4', '2018-01-25 10:00:01'),
(73, 87, 114, 'Whatsappo dude', 'multiple', 'Oli', 'Kio', 'Hdhd', NULL, NULL, '2', '2018-01-25 10:02:24'),
(74, 87, 114, 'Whatsapp dude', 'multiple', 'Hdhd', 'Hdhd', NULL, NULL, NULL, '1', '2018-01-25 10:02:49'),
(75, 87, 114, 'Whatsappo bro', 'multiple', 'Hdhs', 'Hdhd', NULL, NULL, NULL, '2', '2018-01-25 10:03:38'),
(76, 87, 114, 'Bdbdhhd', 'multiple', 'Hdhd', 'Hdhhd', NULL, NULL, NULL, '1', '2018-01-25 10:04:14'),
(77, 87, 114, 'Nxndndbdn', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:07:14'),
(78, 87, 114, 'Hdhd', 'multiple', 'Hdhd', 'Hdhhd', NULL, NULL, NULL, '1', '2018-01-25 10:11:38'),
(79, 87, 114, 'Jdfjf', 'multiple', 'Hdhd', 'Hdhehe', 'Ndjjd', 'Ndnnd', 'Bdnd', '2', '2018-01-25 10:12:36'),
(80, 87, 114, 'Hdhd11122', 'multiple', 'Hdhd', 'Hdhd', 'Hdhd', NULL, NULL, '2', '2018-01-25 10:12:50'),
(81, 87, 114, 'Nxjhd', 'multiple', 'Jdhe', 'Behhd', 'Bdhdhd', 'Ndndnd', 'Nfndnnnd', '3', '2018-01-25 10:14:35'),
(82, 87, 114, 'Jdhhdhdhdhhdhe', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:14:42'),
(83, 87, 114, 'Hdjddjdjd hdhdhhdjdjdjd hdhhdhd hdhhdhd hdhdhhdjdjdjd bhshshhs bhshshhs hshhshsbsjs hsjsjslqo1i5) 2(5)85)55)5)558>@5@5@5@66>]<]>]^^[^^         ®¥®€™€>] <] |] ¦>] ^] ^^[^[>] ¦] ¦¦] >] ^¡¡	^] >{>¢©¦{>{>{{>>{¦>[^[^>] >] >] >>] >] >] ¦] ¦] ¦] ¦] ¦¦] >] ¦] >>]', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:23:46'),
(84, 87, 114, 'Dnnfnfnfjfjfjf', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:25:07'),
(85, 87, 114, 'Hdhdhdhdhhdhdhdhdjjdjdjdjdjjdjdjdjjdjdjs5=5=55=5=56!66@6@63#3?2&5_8&58@8>|[\\>]^]^[>€>€^¿$^$>>$>€¦[>[>>[>]¦]¦¦÷¦{¦]¦]^¡[¬]¦¦]¦]¦¦]>]>', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:25:43'),
(86, 87, 114, 'Hdhhdhshs', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:26:02'),
(87, 87, 114, 'Hdhdhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:26:53'),
(88, 87, 114, 'Hdhhdhdhdjdjdkapowieje8ejeheue hdhhdhd ?????? 6hdhdhdhhdhs bdhdhhdhdhdhhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:27:14'),
(89, 87, 114, 'Hdhdhdhhshshshshs', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:27:42'),
(90, 87, 114, 'Bdbdhfhdhdjjdjdklalwowiue8e8u3hehdhdjjd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:28:29'),
(91, 87, 114, 'Hshshshsjwkjwjs', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:28:44'),
(92, 87, 114, 'Hehhehhd', 'multiple', 'Bdbd', 'Hdhdhd', NULL, NULL, NULL, '1', '2018-01-25 10:29:05'),
(93, 87, 114, 'Heheee+? ? wke', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:29:18'),
(94, 87, 114, 'Ndndjjd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:29:28'),
(95, 87, 114, 'Nsjdjdjd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:32:37'),
(96, 87, 114, 'What is youbdhdhhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:36:19'),
(97, 87, 114, 'Tbtvtvt', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:36:39'),
(98, 87, 114, 'Hdhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:37:19'),
(99, 87, 114, 'HddhdhdHdhhdhd.', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:37:24'),
(100, 87, 114, 'Hdhdhdhdjieoekejjdjhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:37:30'),
(101, 87, 114, 'Hdhhdhd.', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:37:36'),
(102, 87, 114, 'Hdhdhdhdhhdhdhdhdjjdjdjdjdjjdjdjdjjdjdjs5.', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:37:41'),
(103, 107, 66, 'hdhdhhdsls', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:41:12'),
(104, 107, 66, 'hdhdhhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:42:33'),
(105, 107, 66, 'jxhdhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:43:20'),
(106, 87, 114, 'B\'s B\'s', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 10:43:42'),
(107, 107, 66, 'bdhdddhd', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-01-25 11:20:46'),
(108, 74, 64, 'Bshshshshhs', 'multiple', 'Hdhd', 'Hdhhd', 'Bdbbd', 'Bdbdbd', 'Y16262', '2', '2018-01-25 11:41:41'),
(109, 136, 87, 'what is my name,,,,,', 'multiple', 'hshs', 'bzhhs', 'hshssh', 'bdbbs', NULL, '1', '2018-02-03 07:27:17'),
(110, 136, 87, 'whhshsshshstzmtxmyxymymffyxyxufuclcigogucyyxyzydydyhndhxgGxgmxmujhxubjccjcfydmydkydkudmd6djndmjsmdy zmyz6udtjsmd6h8dk \n\n\n\n\n\n\n\n\ndmznsjmdmgms\ntsyky\n\n\n\n\ndkyddyd', 'multiple', 'bdbshstsjtktt\n\ndd\nddd\nddd\nd', 'dd\n\ndddddd\nddddddddd\n\n\nd\ndddddd', 'dddd\nddddd\ndddddddd\nddddd\ndd\nd\ndd', 'd\nddd\ndddd\ndd\nd\ndd\nree\nreee\nee', 'shshjsjskskksksfffff\nds\n\n\n\n\n\nrrususs\n\nshsjsjs', '5', '2018-02-03 07:31:39'),
(111, 164, 95, 'what king aurthur', 'multiple', 'first knight', 'second knight', 'third knight', 'fourth knight', 'fifth knight', '1', '2018-02-06 09:30:11'),
(112, 174, 102, 'is square a rectangle?', 'multiple', 'yes', 'no', NULL, NULL, NULL, '1', '2018-02-07 09:54:25'),
(113, 174, 102, 'what is the value of 25÷54', 'multiple', '4', '3', '5', '2', NULL, '3', '2018-02-07 09:56:33'),
(114, 80, 36, 'who is the greatest batsman of all time.?', 'multiple', 'brian charles lara', 'don bradman', 'sachin tendulkar', 'ricky ponting', NULL, '3', '2018-02-07 09:57:36'),
(115, 80, 101, 'who is the greatest batsman of all time?', 'multiple', 'virat kohli', 'steve smith', 'sachin tendulkar', 'AB devilliar', NULL, '3', '2018-02-07 10:06:33'),
(116, 74, 64, 'Hrllo', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-09 06:48:06'),
(117, 74, 64, 'Today date', 'multiple', '9', '10', '12', NULL, NULL, '1', '2018-02-09 06:49:31'),
(118, 183, 123, 'What is the sqaure of 13', 'multiple', '149', '169', '242', '546', NULL, '2', '2018-02-11 09:24:13'),
(119, 2, 15, 'what is emergency?', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-13 10:25:46'),
(120, 2, 15, 'is earth round?', 'multiple', 'yes', 'no', 'dont know', NULL, NULL, '1', '2018-02-13 13:33:27'),
(121, 241, 143, 'ggg', 'multiple', 'gg', 'gg', NULL, NULL, NULL, '1', '2018-02-15 12:02:09'),
(153, 241, 158, 'nnrry.', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-28 09:34:33'),
(152, 241, 158, 'hgfr', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-28 09:34:31'),
(151, 241, 158, 'ggdf', 'multiple', 'vv', 'fgg', NULL, NULL, NULL, '1', '2018-02-28 09:33:51'),
(150, 241, 158, 'dfgh', 'multiple', 'cg', 'ggg', NULL, NULL, NULL, '1', '2018-02-28 09:33:50'),
(148, 75, 155, 'Who is president of India?', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-28 05:51:01'),
(149, 241, 158, 'mff', 'multiple', 'cg', 'ff', 'fg', 'fg', 'gg', '1', '2018-02-28 09:30:08'),
(147, 75, 155, 'Which animal lives in antartica?', 'multiple', 'Leopard', 'Polar Bear', 'Grizzly bear', 'Lion', 'Penguin', '2', '2018-02-28 05:50:59'),
(146, 75, 155, 'Question 3', 'multiple', 'chsgs', 'ghahshi', 'givjjk', NULL, NULL, '2', '2018-02-28 05:43:40'),
(145, 75, 155, 'who the hell is this?', 'single', NULL, NULL, NULL, NULL, NULL, '', '2018-02-28 05:43:38'),
(144, 75, 155, 'Question 1', 'multiple', 'gdhhd', 'hshdh', NULL, NULL, NULL, '2', '2018-02-28 05:43:35'),
(199, 75, 160, 'Q3 for multiple', 'multiple', 'ufjj', 'chhcgj', NULL, NULL, NULL, '2', '2018-04-04 10:23:55');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_question_sub`
--

CREATE TABLE `teacher_question_sub` (
  `id` int(11) NOT NULL,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `teacher_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `my_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `batch_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `on_created` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_question_sub`
--

INSERT INTO `teacher_question_sub` (`id`, `question_id`, `teacher_id`, `student_id`, `my_answer`, `batch_id`, `status`, `on_created`) VALUES
(1, '1', '1', '2', '1', '1', 1, '20-12-2017 17:06:28'),
(2, '2', '1', '1', '1', '2', 1, '21-12-2017 14:03:28'),
(3, '3', '1', '1', '2', '2', 0, '21-12-2017 14:04:49'),
(4, '4', '1', '1', '2', '2', 0, '21-12-2017 18:16:46'),
(5, '5', '18', '1', '2', '3', 0, '22-12-2017 16:48:21'),
(6, '6', '26', '6', '3', '6', 0, '23-12-2017 15:03:18'),
(7, '7', '28', '6', '3', '7', 0, '23-12-2017 17:43:44'),
(8, '8', '30', '6', '4', '10', 0, '24-12-2017 14:52:31'),
(9, '1', '1', '1', '1', '1', 1, '15-01-2018 14:03:33'),
(10, '11', '2', '2', '1', '15', 1, '20-01-2018 16:09:42'),
(11, '22', '99', '25', 'refraction is edition of fraction.', '56', 2, '21-01-2018 13:53:37'),
(12, '36', '75', '1', '1', '52', 0, '23-01-2018 13:44:35'),
(13, '32', '75', '1', '3', '52', 0, '23-01-2018 13:44:46'),
(14, '31', '75', '1', '2', '52', 1, '23-01-2018 13:44:52'),
(15, '30', '75', '1', '3', '52', 1, '23-01-2018 13:44:56'),
(16, '29', '75', '1', '2', '52', 1, '23-01-2018 13:45:01'),
(17, '26', '75', '1', 'tare zameen par...ohhhh ho ohhhhh', '52', 2, '23-01-2018 13:45:32'),
(18, '25', '75', '1', 'jdjdjdjddddddj', '52', 2, '23-01-2018 13:47:53'),
(19, '27', '75', '1', 'hdddjjdhdtztztzmxyxmmgxmzmyztzxgzntzynzdjtzmyxstzmysdkysTudhdfhfigidjkhfkxatsfdjdddjddlaoeidjdhjjkdkddjdjdjjdkdkdkdkdkdkdkjdkdkdkjjdjkslapwirurhnfbffdwiejrnfldlpwkdnfnnjfjfjkdkkeepwwldfjfjfjgztts5skysymtydysmydtskysjtsmysystmsmsmsjthdddjjdhdtztztzmxy', '52', 2, '23-01-2018 13:49:03'),
(20, '28', '75', '1', 'jdjjdhdhdtdtTsmYrzujzkdtzmYmtzhdddjjdhdtztztzmxyxmmgxmzmyztzxgzntzynzdjtzmyxstzmysdkysTudhdfhfigidjkhfkxatsfdjdddjddlaoeidjdhjjkdkddjdjdjjdkdkdkdkdkdkdkjdkdkdkjjdjkslapwirurhnfbffdwiejrnfldlpwkdnfnnjfjfjkdkkeepwwldfjfjfjgztts5skysymtydysmydtskysjtsmy', '52', 2, '23-01-2018 13:55:56'),
(21, '37', '74', '30', '2', '64', 0, '24-01-2018 14:36:56'),
(22, '33', '75', '4', '1', '52', 1, '24-01-2018 14:43:14'),
(23, '38', '74', '30', '5', '64', 1, '24-01-2018 14:46:20'),
(24, '33', '75', '30', '2', '52', 0, '24-01-2018 14:47:41'),
(25, '39', '75', '30', 'los angeles', '52', 2, '24-01-2018 14:51:24'),
(26, '39', '75', '4', 'Hdhdhhdhdjdjdkskjdhdjdjdkkdjddjdjdkdkdkdkjdjdjdjdhdhdjdjdjdjdjjdjdjdjdkdklslsjsjdjdjdjdjdjdjdjdjdjdjdjdjdjdjjdjdjdjdjdjdjj', '52', 2, '24-01-2018 14:52:12'),
(27, '40', '49', '30', '4', '104', 0, '24-01-2018 18:11:11'),
(28, '41', '49', '30', 'hdhdhddhdhdh', '104', 2, '24-01-2018 18:11:16'),
(29, '43', '9', '30', '3', '118', 0, '25-01-2018 11:10:48'),
(30, '44', '9', '30', '5', '118', 1, '25-01-2018 11:33:17'),
(31, '45', '9', '30', 'what is my name', '118', 2, '25-01-2018 11:57:57'),
(32, '36', '75', '20', '2', '52', 1, '30-01-2018 10:33:55'),
(33, '32', '75', '20', '3', '52', 0, '30-01-2018 10:33:59'),
(34, '33', '75', '20', '3', '52', 0, '30-01-2018 10:34:05'),
(35, '111', '164', '30', '1', '95', 1, '06-02-2018 15:00:27'),
(36, '111', '164', '5', '3', '95', 0, '06-02-2018 15:03:38'),
(37, '115', '80', '17', '4', '101', 0, '07-02-2018 15:36:59'),
(38, '114', '80', '2', '3', '36', 1, '12-02-2018 16:44:51'),
(39, '156', '176', '42', 'vbhhf', '204', 2, '06-03-2018 12:44:15'),
(40, '157', '176', '42', '1', '204', 1, '06-03-2018 12:44:26'),
(41, '160', '241', '6', '1', '162', 0, '12-03-2018 13:00:24'),
(42, '161', '241', '6', 'gichkl', '162', 2, '12-03-2018 13:01:22'),
(43, '162', '241', '6', '2', '162', 0, '12-03-2018 13:01:41'),
(44, '160', '241', '42', '1', '162', 0, '12-03-2018 13:04:09'),
(45, '161', '241', '42', 'jsiebe', '162', 2, '12-03-2018 13:04:14'),
(46, '162', '241', '42', '2', '162', 0, '12-03-2018 13:04:19'),
(47, '158', '241', '6', '2', '162', 1, '12-03-2018 13:05:37'),
(48, '159', '241', '6', 'yes', '162', 2, '12-03-2018 13:05:54'),
(49, '155', '241', '85', '1', '144', 0, '14-03-2018 12:51:09'),
(50, '165', '241', '124', '2', '165', 1, '15-03-2018 15:32:43'),
(51, '166', '241', '124', 'hsjiw', '165', 2, '15-03-2018 15:32:52'),
(52, '167', '241', '124', '1', '165', 0, '15-03-2018 15:33:06'),
(53, '169', '241', '124', 'nsm', '166', 2, '16-03-2018 12:29:08'),
(54, '114', '80', '25', '2', '36', 0, '04-04-2018 15:18:31');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_slot`
--

CREATE TABLE `teacher_slot` (
  `id` int(11) NOT NULL,
  `teacher_id` int(10) NOT NULL,
  `days` varchar(255) NOT NULL,
  `start_time` varchar(20) NOT NULL,
  `end_time` varchar(20) NOT NULL,
  `slot_id` varchar(11) NOT NULL,
  `fee_hour` varchar(10) NOT NULL,
  `strength` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for free slot, 0 for occupied '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_slot`
--

INSERT INTO `teacher_slot` (`id`, `teacher_id`, `days`, `start_time`, `end_time`, `slot_id`, `fee_hour`, `strength`, `status`) VALUES
(2, 12, 'Saturday, Friday, Thursday', '12:00 pm', '01:00 pm', '8', '690', '5', 1),
(8, 24, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '09:00 am', '10:00 am', '5', '1200', '6', 1),
(9, 24, 'Monday, Tuesday, Thursday, Wednesday, Friday, Saturday', '04:00 pm', '05:00 pm', '12', '1200', '6', 1),
(10, 24, 'Tuesday, Monday, Wednesday, Thursday, Friday, Saturday', '06:00 pm', '07:00 pm', '14', '1200', '6', 1),
(11, 24, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '10:00 am', '11:00 am', '6', '1200', '6', 1),
(12, 24, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '03:00 pm', '04:00 pm', '11', '1200', '6', 1),
(13, 24, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 pm', '06:00 pm', '13', '1200', '6', 1),
(17, 38, 'Friday, Thursday, Saturday, Wednesday, Tuesday, Monday, Sunday', '08:00 am', '09:00 am', '4', '180', '6', 1),
(20, 54, 'Sunday, Monday', '06:00 am', '07:00 am', '2', '500', '2', 1),
(21, 54, 'Sunday, Monday, Tuesday', '07:00 am', '08:00 am', '3', '500', '2', 1),
(22, 54, 'Thursday, Friday, Saturday', '02:00 pm', '03:00 pm', '10', '500', '1', 1),
(23, 59, 'Saturday, Sunday', '08:00 am', '09:00 am', '4', '150', '6', 1),
(24, 59, 'Friday, Thursday', '08:00 am', '09:00 am', '4', '15', '6', 1),
(75, 15, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 am', '06:00 am', '1', '200', 'Students', 1),
(100, 15, 'Sunday, Saturday', '06:00 am', '07:00 am', '2', '848484', 'Students', 1),
(104, 49, 'Sunday', '06:00 am', '07:00 am', '2', '666', 'Students', 1),
(105, 83, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '10:00 am', '11:00 am', '6', '120', 'Students', 1),
(106, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '12:00 pm', '01:00 pm', '8', '500', 'Students', 1),
(107, 101, 'Sunday, Monday', '10:00 am', '11:00 am', '6', '500', 'Students', 1),
(108, 101, 'Thursday, Friday, Saturday', '11:00 am', '12:00 pm', '7', '500', 'Students', 1),
(110, 49, 'Wednesday, Thursday, Friday, Saturday', '06:00 am', '07:00 am', '2', '5665', 'Students', 1),
(111, 49, 'Monday, Tuesday', '06:00 am', '07:00 am', '2', '639', 'Students', 1),
(114, 87, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 am', '06:00 am', '1', '630', 'Students', 1),
(115, 87, 'Tuesday, Thursday, Friday, Saturday', '06:00 am', '07:00 am', '2', '630', 'Students', 1),
(116, 87, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '11:00 am', '12:00 pm', '7', '650', 'Students', 1),
(117, 87, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '04:00 pm', '05:00 pm', '12', '665', 'Students', 1),
(118, 9, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 am', '06:00 am', '1', '150', 'Students', 1),
(119, 9, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '07:00 am', '08:00 am', '3', '690', 'Students', 1),
(120, 87, 'Wednesday, Thursday, Friday, Saturday', '10:00 am', '11:00 am', '6', '554', 'Students', 1),
(122, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '04:00 pm', '05:00 pm', '12', '500', 'Students', 1),
(123, 42, 'Sunday', '04:00 pm', '05:00 pm', '12', '500', 'Students', 1),
(124, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '06:00 pm', '07:00 pm', '14', '500', 'Students', 1),
(125, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '02:00 pm', '03:00 pm', '10', '500', 'Students', 1),
(126, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 pm', '09:00 pm', '16', '200', 'Students', 1),
(127, 42, 'Sunday', '12:00 pm', '01:00 pm', '8', '500', 'Students', 1),
(128, 87, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '01:00 pm', '02:00 pm', '9', '1', 'Students', 1),
(129, 87, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '07:00 pm', '08:00 pm', '15', '1', 'Students', 1),
(130, 42, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 am', '09:00 am', '4', '500', 'Students', 1),
(131, 42, 'Sunday, Monday, Tuesday, Wednesday, Thursday', '06:00 am', '07:00 am', '2', '500', 'Students', 1),
(132, 9, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 pm', '09:00 pm', '16', '680', 'Students', 1),
(133, 158, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 am', '09:00 am', '4', '1000', 'Students', 1),
(134, 160, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '06:00 pm', '07:00 pm', '14', '1', 'Students', 1),
(135, 9, 'Sunday, Wednesday, Saturday', '04:00 pm', '05:00 pm', '12', '600', 'Students', 1),
(143, 184, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '03:00 pm', '04:00 pm', '11', '500', 'Students', 1),
(144, 184, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 pm', '06:00 pm', '13', '500', 'Students', 1),
(145, 184, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '07:00 pm', '08:00 pm', '15', '500', 'Students', 1),
(155, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 pm', '06:00 pm', '13', '500', 'Students', 1),
(156, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '06:00 pm', '07:00 pm', '14', '500', 'Students', 1),
(157, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '07:00 pm', '08:00 pm', '15', '500', 'Students', 1),
(158, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 pm', '09:00 pm', '16', '500', 'Students', 1),
(159, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '09:00 pm', '10:00 pm', '17', '500', 'Students', 1),
(160, 194, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '10:00 pm', '11:00 pm', '18', '500', 'Students', 1),
(162, 9, 'Sunday, Monday', '06:00 am', '07:00 am', '2', '200', 'Students', 1),
(163, 9, 'Sunday', '11:00 am', '12:00 pm', '7', '200', 'Students', 1),
(175, 70, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '03:00 pm', '04:00 pm', '11', '1500', 'Students', 1),
(176, 70, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 pm', '06:00 pm', '13', '1500', 'Students', 1),
(177, 70, 'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '07:00 pm', '08:00 pm', '15', '1500', 'Students', 1),
(181, 215, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '12:00 pm', '01:00 pm', '8', '600', 'Students', 1),
(182, 218, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday', '06:00 am', '07:00 am', '2', '300', 'Students', 1),
(183, 176, 'Tuesday, Wednesday', '05:00 am', '06:00 am', '1', '580', 'Students', 1),
(184, 176, 'Tuesday, Wednesday', '07:00 am', '08:00 am', '3', '580', 'Students', 1),
(185, 176, 'Tuesday, Wednesday', '09:00 am', '10:00 am', '5', '580', 'Students', 1),
(186, 176, 'Friday', '05:00 am', '06:00 am', '1', '800', 'Students', 1),
(187, 176, 'Thursday, Saturday', '05:00 am', '06:00 am', '1', '500', 'Students', 1),
(188, 176, 'Sunday, Monday, Thursday, Friday, Saturday', '07:00 am', '08:00 am', '3', '500', 'Students', 1),
(189, 176, 'Friday, Saturday', '01:00 pm', '02:00 pm', '9', '600', 'Students', 1),
(190, 176, 'Friday', '03:00 pm', '04:00 pm', '11', '50', 'Students', 1),
(191, 176, 'Tuesday', '01:00 pm', '02:00 pm', '9', '50', 'Students', 1),
(192, 176, 'Friday', '09:00 am', '10:00 am', '5', '50', 'Students', 1),
(193, 236, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '03:00 pm', '04:00 pm', '11', '500', 'Students', 1),
(194, 236, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '04:00 pm', '05:00 pm', '12', '500', 'Students', 1),
(195, 236, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 pm', '06:00 pm', '13', '500', 'Students', 1),
(196, 236, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '06:00 pm', '07:00 pm', '14', '500', 'Students', 1),
(197, 236, 'Monday, Wednesday, Friday', '02:00 pm', '03:00 pm', '10', '500', 'Students', 1),
(198, 176, 'Tuesday', '11:00 am', '12:00 pm', '7', '1', 'Students', 1),
(199, 176, 'Friday, Saturday', '08:00 am', '09:00 am', '4', '3', 'Students', 1),
(200, 236, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '08:00 pm', '09:00 pm', '16', '500', 'Students', 1),
(201, 176, 'Monday', '11:00 am', '12:00 pm', '7', '1', 'Students', 1),
(202, 176, 'Wednesday', '04:00 pm', '05:00 pm', '12', '1', 'Students', 1),
(203, 176, 'Tuesday', '03:00 pm', '04:00 pm', '11', '1', 'Students', 1),
(204, 176, 'Thursday', '03:00 pm', '04:00 pm', '11', '5', 'Students', 1),
(206, 243, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '05:00 am', '06:00 am', '1', '2', 'Students', 1),
(207, 243, 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday', '06:00 am', '07:00 am', '2', '2000', 'Students', 1),
(209, 248, 'Sunday, Monday', '06:00 am', '07:00 am', '2', '2000', 'Students', 1),
(210, 250, 'Thursday, Friday, Saturday', '07:00 am', '08:00 am', '3', '1', 'Students', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_slot_booked`
--

CREATE TABLE `teacher_slot_booked` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '0: in review, 1:Booked, 2:completed',
  `booked_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `review_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'teacher_slot_review id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_slot_booked`
--

INSERT INTO `teacher_slot_booked` (`id`, `teacher_id`, `student_id`, `slot_id`, `status`, `booked_date`, `review_id`) VALUES
(1, '0', '0', '0', '0', '0', '0'),
(13, '49', '30', '110', '1', '2018-01-24', '29'),
(3, '9', '2', '1', '1', '21-12-2017 14:29:15', '2'),
(12, '49', '4', '104', '1', '2018-01-24', '28'),
(5, '9', '20', '1', '1', '09-01-2018 14:45:09', '8'),
(6, '9', '6', '1', '1', '09-01-2018 18:12:59', '9'),
(11, '49', '30', '104', '1', '2018-01-24', '27'),
(14, '49', '30', '111', '1', '2018-01-24', '30'),
(15, '87', '30', '114', '1', '2018-01-24', '31'),
(24, '15', '6', '75', '1', '20-01-2018 00:06:48', '16'),
(17, '87', '30', '115', '1', '2018-01-24', '32'),
(18, '87', '30', '116', '1', '2018-01-24', '34'),
(19, '87', '20', '116', '1', '2018-01-24', '35'),
(20, '87', '30', '117', '1', '2018-01-24', '36'),
(21, '87', '25', '116', '1', '2018-01-24', '37'),
(22, '9', '30', '118', '1', '2018-01-25', '38'),
(23, '9', '20', '119', '1', '2018-01-25', '39'),
(25, '87', '5', '128', '1', '2018-02-02', '42'),
(26, '87', '4', '114', '1', '2018-01-25', '40'),
(27, '87', '25', '128', '1', '2018-02-02', '45'),
(28, '87', '25', '129', '1', '2018-02-02', '46'),
(29, '9', '30', '119', '1', '2018-02-02', '47'),
(30, '24', '30', '8', '1', '2018-02-08', '49'),
(31, '24', '30', '8', '1', '2018-02-08', '50'),
(32, '15', '1', '75', '1', '20-01-2018 13:48:34', '17'),
(33, '15', '25', '75', '1', '20-01-2018 14:50:33', '18'),
(34, '15', '6', '100', '1', '2018-02-10', '52'),
(35, '176', '42', '198', '1', '2018-02-14', '53'),
(36, '176', '42', '198', '1', '2018-02-14', '54'),
(37, '176', '42', '204', '1', '2018-02-28', '56'),
(38, '176', '42', '202', '1', '2018-02-26', '55'),
(39, '176', '42', '183', '1', '2018-03-05', '57'),
(40, '248', '25', '209', '1', '2018-03-10', '58'),
(41, '250', '124', '210', '1', '2018-03-16', '61');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_slot_review`
--

CREATE TABLE `teacher_slot_review` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `days` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subjects` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subjects_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` enum('now','later') COLLATE utf8_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requested_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requested_time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `added_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL COMMENT '0:in revirew, 1: accepted, 2:rejected, 3:ended'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_slot_review`
--

INSERT INTO `teacher_slot_review` (`id`, `teacher_id`, `student_id`, `slot_id`, `days`, `class_id`, `class_name`, `subjects`, `subjects_name`, `payment_type`, `order_id`, `requested_date`, `requested_time`, `added_date`, `status`) VALUES
(2, '9', '2', '1', 'Saturday', '93', 'Mathematical Hons.', '1090,1087', 'Numerical Analysis ,Integral Calculus And Trigonometry', 'now', '', '21-12-2017 14:29:15', '', '21-12-2017 14:29:24', 1),
(21, '49', '25', '103', 'Sunday,Monday,Tuesday', '6', 'CLASS VI', '323,322,324', 'ENGLISH,MATHEMATICS,SCIENCE', 'later', '', '23-01-2018 15:41:06', '', NULL, 0),
(4, '9', '22', '1', 'Sunday', '93', 'Mathematical Hons.', '1089,1088,1087', 'Abstract Algebra,Differntial Equations ,Integral Calculus And Trigonometry', 'now', '', '28-12-2017 10:33:33', '', '10-01-2018 10:29:53', 2),
(5, '54', '22', '20', 'Sunday,Monday', '18', 'CLASS XI(MEDICAL)', '59', 'BIOLOGY', 'now', '', '28-12-2017 10:40:49', '', NULL, 0),
(6, '59', '22', '23', 'Saturday,Sunday', '3', 'CLASS III', '22,23', 'ENGLISH ,HINDI', 'now', '', '28-12-2017 11:53:26', '', NULL, 0),
(7, '59', '22', '24', 'Friday,Thursday', '3', 'CLASS III', '22,23', 'ENGLISH ,HINDI', 'now', '', '28-12-2017 12:30:29', '', NULL, 0),
(8, '9', '20', '1', 'Sunday', '175', 'SSC CPO(S.I) exam', '1492', 'English Comprehension', 'now', '', '09-01-2018 14:45:09', '', '09-01-2018 14:48:42', 1),
(9, '9', '6', '1', 'Sunday', '12', 'CLASS XII', '64', 'ACCOUNTANCY', 'now', '', '09-01-2018 18:12:59', '', '09-01-2018 18:13:49', 1),
(11, '9', '20', '28', 'Friday,Monday', '23', 'PAYROLL TRAINING', '912', 'PAYROLL TRAINING', 'now', '', '11-01-2018 15:07:43', '', NULL, 0),
(12, '15', '1', '67', 'Sunday,Monday,Friday,Saturday', '99', 'Biotechnology', '1093', 'Cell Biology', 'now', '', '17-01-2018 18:15:06', '', NULL, 0),
(13, '49', '1', '63', 'Sunday,Monday', '1', 'CLASSES I-V', '1880,1879', 'CLASS V,CLASS IV', 'now', '', '18-01-2018 16:30:53', '', NULL, 0),
(28, '49', '4', '104', 'Sunday', '6', 'CLASS VI', '323,322,324', 'ENGLISH,MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '16:20:49', '24-01-2018 16:20:54', 1),
(29, '49', '30', '110', 'Friday,Saturday', '6', 'CLASS VI', '324', 'SCIENCE', 'later', '', '2018-01-24', '16:26:50', '24-01-2018 16:33:59', 1),
(16, '15', '6', '75', 'Friday,Saturday', '99', 'Biotechnology', '1093', 'Cell Biology', 'later', '', '20-01-2018 00:06:48', '', '27-01-2018 23:17:26', 1),
(17, '15', '1', '75', 'Sunday,Saturday', '99', 'Biotechnology', '1093', 'Cell Biology', 'later', '', '20-01-2018 13:48:34', '', '13-02-2018 19:00:56', 1),
(18, '15', '25', '75', 'Monday,Tuesday,Wednesday,Thursday,Friday', '99', 'Biotechnology', '1093', 'Cell Biology', 'later', '', '20-01-2018 14:50:33', '', '13-02-2018 19:01:02', 1),
(30, '49', '30', '111', 'Tuesday', '6', 'CLASS VI', '324', 'SCIENCE', 'later', '', '2018-01-24', '16:36:04', '24-01-2018 16:36:12', 1),
(27, '49', '30', '104', 'Sunday', '6', 'CLASS VI', '322,324', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '16:16:47', '24-01-2018 16:17:15', 1),
(31, '87', '30', '114', 'Sunday,Monday,Wednesday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '17:19:20', '24-01-2018 17:24:22', 1),
(40, '87', '4', '114', 'Tuesday,Thursday,Friday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-25', '15:56:39', '02-02-2018 15:49:29', 1),
(32, '87', '30', '115', 'Thursday,Friday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '17:36:32', '24-01-2018 17:37:47', 1),
(34, '87', '30', '116', 'Wednesday,Thursday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '17:59:22', '24-01-2018 17:59:31', 1),
(35, '87', '20', '116', 'Sunday,Monday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '18:23:15', '24-01-2018 18:23:19', 1),
(36, '87', '30', '117', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'later', '', '2018-01-24', '18:38:09', '24-01-2018 18:38:14', 1),
(38, '9', '30', '118', 'Tuesday,Wednesday,Thursday,Friday,Saturday', '223', 'Electronics & Communication Engineering', '1715,1712,1716,1717,1713,1711,1714,1718,1720,1721,1719,1722,1723', '	Chemistry,Physics,Art of Programming,	Elements of Electrical Engineering,Mechanics of Solids,Mathematics,Engineering Graphics,Electronics Devices and Circuits,Network Analysis,Linear Control System,Digital Circuits,Signals and Systems,Electrical Machines', 'later', '', '2018-01-25', '11:02:59', '25-01-2018 11:03:10', 1),
(39, '9', '20', '119', 'Sunday,Monday,Tuesday,Wednesday', '218', 'Computer Science  Engineering', '1639', '	Engineering Graphics', 'later', '', '2018-01-25', '11:28:49', '25-01-2018 11:29:22', 1),
(45, '87', '25', '128', 'Wednesday,Thursday,Friday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'now', '', '2018-02-02', '15:43:25', '02-02-2018 15:49:31', 1),
(42, '87', '5', '128', 'Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'now', '', '2018-02-02', '15:36:45', '02-02-2018 15:44:04', 1),
(46, '87', '25', '129', 'Friday,Saturday', '8', 'CLASS VIII', '348,350', 'MATHEMATICS,SCIENCE', 'now', '', '2018-02-02', '15:47:30', '02-02-2018 15:49:33', 1),
(47, '9', '30', '119', 'Thursday,Friday,Saturday', '223', 'Electronics & Communication Engineering', '1711,1714,1718,1720', 'Mathematics,Engineering Graphics,Electronics Devices and Circuits,Network Analysis', 'later', '', '2018-02-02', '17:28:58', '03-02-2018 12:35:39', 1),
(48, '9', '30', '132', 'Wednesday,Friday', '223', 'Electronics & Communication Engineering', '1713,1714', 'Mechanics of Solids,Engineering Graphics', 'later', '', '2018-02-06', '12:05:22', NULL, 0),
(49, '24', '30', '8', 'Monday,Saturday', '12', 'CLASS XII', '64,65', 'ACCOUNTANCY,BUSINESS STUDIES', 'later', '', '2018-02-08', '12:15:18', '08-02-2018 13:17:40', 1),
(50, '24', '30', '8', 'Tuesday,Wednesday,Friday,Saturday', '176', 'SSC CGL exam', '1493,1494,1495,1496', 'General Intelligence & Reasoning,General Awareness,Quantitative Aptitude,English Language', 'later', '', '2018-02-08', '12:17:04', '08-02-2018 13:17:48', 1),
(51, '9', '30', '132', 'Wednesday,Thursday,Friday,Saturday', '218', 'Computer Science  Engineering', '1641,1643', '	Computer Programming,	Basic Electronics', 'later', '', '2018-02-08', '12:32:36', NULL, 0),
(52, '15', '6', '100', 'Saturday', '99', 'Biotechnology', '1093', 'Cell Biology', 'later', '', '2018-02-10', '18:30:45', '13-02-2018 19:01:04', 1),
(53, '176', '42', '198', 'Tuesday', '6', 'CLASS VI', '323', 'ENGLISH', 'later', '', '2018-02-14', '16:31:54', '15-02-2018 00:01:54', 1),
(54, '176', '42', '198', 'Tuesday', '89', 'Physics hons.', '1075', 'Mechanics', 'now', '', '2018-02-14', '16:38:43', '15-02-2018 00:02:00', 1),
(55, '176', '42', '202', 'Wednesday', '9', 'CLASS IX', '363', 'SCIENCE', 'later', '', '2018-02-26', '13:26:34', '28-02-2018 11:40:04', 1),
(56, '176', '42', '204', 'Thursday', '115', 'AFMC', '1477', 'Chemistry', 'later', '', '2018-02-28', '11:34:35', '28-02-2018 11:39:58', 1),
(57, '176', '42', '183', 'Tuesday', '97', 'SRMJEEE', '1133', 'Chemistry', 'later', '', '2018-03-05', '16:05:29', '06-03-2018 12:02:36', 1),
(58, '248', '25', '209', 'Sunday,Monday', '6', 'CLASS VI', '327,323', 'CIVICS,ENGLISH', 'later', '', '2018-03-10', '15:00:52', '10-03-2018 15:01:20', 1),
(59, '24', '6', '8', 'Thursday', '176', 'SSC CGL exam', '1495', 'Quantitative Aptitude', 'later', '', '2018-03-12', '00:01:32', NULL, 0),
(60, '87', '124', '116', 'Friday', '208', 'NDA exam', '1579', 'Mathematics', 'later', '', '2018-03-16', '12:13:59', NULL, 0),
(61, '250', '124', '210', 'Friday,Saturday', '112', 'GUJCET', '1469,1468', 'Biology,Chemistry', 'later', '', '2018-03-16', '17:04:16', '16-03-2018 17:04:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_student_rel`
--

CREATE TABLE `teacher_student_rel` (
  `id` int(11) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `teacher_id` varchar(50) NOT NULL,
  `batch_id` varchar(50) NOT NULL,
  `approve` int(11) NOT NULL DEFAULT '0',
  `al_ready_student` int(5) NOT NULL COMMENT '0: already student, 1: new student',
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_student_rel`
--

INSERT INTO `teacher_student_rel` (`id`, `student_id`, `teacher_id`, `batch_id`, `approve`, `al_ready_student`, `added_date`) VALUES
(34, '22', '2', '15', 1, 0, '2018-01-15 06:10:12'),
(59, '30', '74', '59', 1, 0, '2018-01-24 06:08:44'),
(4, '18', '1', '2', 1, 0, '2017-12-21 12:40:41'),
(58, '30', '75', '52', 1, 0, '2018-01-24 06:10:24'),
(7, '6', '26', '6', 1, 1, '2018-01-11 06:19:43'),
(8, '6', '28', '7', 1, 0, '2017-12-23 12:11:38'),
(9, '8', '28', '7', 0, 1, '2018-01-11 06:22:49'),
(10, '2', '28', '9', 0, 0, '2017-12-23 13:01:08'),
(11, '6', '30', '10', 1, 0, '2017-12-24 09:15:26'),
(12, '1', '30', '10', 0, 0, '2017-12-26 08:44:50'),
(57, '1', '75', '51', 1, 0, '2018-01-24 06:06:47'),
(17, '6', '61', '18', 1, 0, '2018-01-12 12:40:18'),
(56, '1', '75', '31', 1, 0, '2018-01-24 05:58:45'),
(88, '1', '75', '52', 1, 0, '0000-00-00 00:00:00'),
(31, '22', '26', '6', 0, 0, '2018-01-08 06:57:19'),
(55, '4', '75', '52', 1, 0, '2018-02-23 07:47:52'),
(29, '2', '2', '15', 1, 0, '2018-01-08 08:41:53'),
(35, '22', '2', '5', 1, 0, '2018-01-16 08:31:42'),
(36, '2', '80', '36', 1, 0, '2018-01-16 14:21:15'),
(38, '25', '99', '56', 1, 0, '2018-01-20 10:46:37'),
(39, '25', '99', '57', 1, 0, '2018-01-21 08:08:28'),
(40, '25', '80', '36', 1, 0, '2018-01-21 08:39:39'),
(61, '30', '74', '64', 1, 1, '2018-01-24 09:03:00'),
(62, '20', '75', '52', 1, 0, '2018-01-24 09:33:51'),
(63, '20', '39', '42', 1, 0, '2018-01-24 13:02:56'),
(64, '30', '107', '66', 1, 0, '2018-01-25 06:22:46'),
(65, '30', '107', '68', 1, 0, '2018-01-25 11:25:10'),
(66, '34', '61', '18', 0, 0, '2018-01-26 16:48:38'),
(67, '30', '99', '57', 0, 0, '2018-01-29 06:01:08'),
(68, '5', '107', '85', 1, 0, '2018-01-29 12:10:41'),
(69, '5', '107', '81', 1, 0, '2018-01-29 12:10:44'),
(74, '30', '136', '87', 1, 0, '2018-02-02 12:42:08'),
(71, '5', '107', '78', 1, 0, '2018-01-29 12:10:46'),
(73, '5', '107', '66', 0, 1, '2018-01-29 12:58:12'),
(75, '30', '136', '86', 1, 0, '2018-02-02 12:42:11'),
(76, '30', '75', '88', 1, 0, '2018-02-21 10:12:22'),
(77, '2', '30', '10', 0, 0, '2018-02-05 11:17:07'),
(78, '30', '164', '95', 1, 0, '2018-02-06 09:27:51'),
(79, '5', '164', '95', 1, 0, '2018-02-06 09:33:15'),
(80, '5', '75', '88', 1, 0, '2018-02-21 10:12:24'),
(81, '17', '80', '101', 1, 0, '2018-02-07 09:59:27'),
(82, '17', '80', '36', 1, 0, '2018-02-07 09:59:54'),
(83, '30', '161', '91', 0, 1, '2018-02-08 06:22:16'),
(84, '42', '39', '124', 1, 1, '2018-02-12 08:49:40'),
(87, '42', '80', '33', 0, 0, '2018-02-12 11:26:30'),
(89, '1', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(90, '58', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(91, '59', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(92, '60', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(93, '36', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(94, '61', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(95, '62', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(96, '63', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(97, '64', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(98, '65', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(99, '66', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(100, '67', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(101, '68', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(102, '69', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(103, '73', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(104, '76', '75', '147', 1, 0, '0000-00-00 00:00:00'),
(105, '77', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(106, '78', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(107, '79', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(108, '80', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(109, '81', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(110, '82', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(111, '83', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(112, '84', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(113, '85', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(114, '86', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(115, '92', '241', '156', 1, 0, '0000-00-00 00:00:00'),
(116, '93', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(117, '94', '241', '156', 1, 0, '0000-00-00 00:00:00'),
(118, '95', '241', '157', 1, 0, '0000-00-00 00:00:00'),
(119, '96', '241', '157', 1, 0, '0000-00-00 00:00:00'),
(120, '97', '241', '156', 1, 0, '0000-00-00 00:00:00'),
(121, '98', '241', '143', 1, 0, '0000-00-00 00:00:00'),
(122, '99', '241', '143', 1, 0, '0000-00-00 00:00:00'),
(123, '100', '241', '143', 1, 0, '0000-00-00 00:00:00'),
(124, '101', '75', '155', 1, 0, '0000-00-00 00:00:00'),
(125, '102', '241', '156', 1, 0, '0000-00-00 00:00:00'),
(126, '103', '241', '156', 1, 0, '0000-00-00 00:00:00'),
(127, '104', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(128, '105', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(129, '106', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(130, '107', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(131, '108', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(132, '109', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(133, '110', '75', '51', 1, 0, '0000-00-00 00:00:00'),
(134, '111', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(135, '112', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(136, '113', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(137, '114', '241', '158', 1, 0, '0000-00-00 00:00:00'),
(138, '115', '241', '144', 1, 0, '0000-00-00 00:00:00'),
(139, '116', '241', '162', 1, 0, '0000-00-00 00:00:00'),
(140, '117', '241', '162', 1, 0, '0000-00-00 00:00:00'),
(141, '118', '241', '162', 1, 0, '0000-00-00 00:00:00'),
(142, '20', '74', '163', 1, 0, '2018-03-10 07:53:38'),
(143, '119', '75', '160', 1, 0, '0000-00-00 00:00:00'),
(144, '120', '241', '162', 1, 0, '0000-00-00 00:00:00'),
(145, '4', '74', '163', 1, 0, '2018-03-10 12:02:11'),
(146, '25', '74', '163', 1, 0, '2018-03-12 05:23:04'),
(147, '42', '241', '162', 1, 0, '2018-03-12 06:47:04'),
(148, '121', '241', '162', 1, 0, '2018-03-12 07:04:23'),
(149, '6', '241', '162', 1, 0, '2018-03-12 07:05:42'),
(150, '122', '241', '162', 1, 0, '0000-00-00 00:00:00'),
(151, '85', '241', '165', 1, 1, '2018-03-14 11:11:09'),
(152, '123', '241', '164', 1, 0, '0000-00-00 00:00:00'),
(161, '128', '241', '167', 1, 0, '0000-00-00 00:00:00'),
(160, '127', '241', '167', 1, 0, '0000-00-00 00:00:00'),
(159, '126', '241', '167', 1, 0, '0000-00-00 00:00:00'),
(158, '124', '241', '166', 1, 0, '2018-03-16 08:57:30'),
(162, '129', '241', '167', 1, 0, '0000-00-00 00:00:00'),
(163, '130', '249', '168', 1, 0, '0000-00-00 00:00:00'),
(164, '131', '249', '168', 1, 0, '0000-00-00 00:00:00'),
(165, '132', '249', '168', 1, 0, '0000-00-00 00:00:00'),
(166, '133', '249', '168', 1, 0, '0000-00-00 00:00:00'),
(167, '124', '249', '168', 1, 0, '2018-03-16 10:01:19'),
(168, '134', '249', '169', 1, 0, '0000-00-00 00:00:00'),
(169, '135', '249', '169', 1, 0, '0000-00-00 00:00:00'),
(170, '124', '249', '169', 1, 0, '2018-03-16 10:42:07'),
(171, '137', '253', '170', 1, 0, '0000-00-00 00:00:00'),
(172, '138', '253', '170', 1, 0, '0000-00-00 00:00:00'),
(173, '139', '253', '170', 1, 0, '0000-00-00 00:00:00'),
(174, '125', '253', '170', 1, 0, '2018-04-04 13:26:54');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_subject`
--

CREATE TABLE `teacher_subject` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `class_id` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active, 2 for in-active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_sub_topic`
--

CREATE TABLE `teacher_sub_topic` (
  `id` bigint(22) NOT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `subject_id` double NOT NULL,
  `subtopic_code` varchar(255) NOT NULL,
  `topic_fees` int(11) NOT NULL,
  `top_expert_flag` enum('1','0') NOT NULL DEFAULT '0' COMMENT '1 for Expertise, 0 for Non-expertise',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active, 0 for in-active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_teaching_details`
--

CREATE TABLE `teacher_teaching_details` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institute_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `inst_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manual_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `img_5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `added` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_teaching_details`
--

INSERT INTO `teacher_teaching_details` (`id`, `teacher_id`, `institute_name`, `inst_address`, `manual_address`, `latt`, `longi`, `img_1`, `img_2`, `img_3`, `img_4`, `img_5`, `added`) VALUES
(1, '1', 'Aptron Academy', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219919', '77.0765254', 'https://taktii.com/etc/api/institute_image/inst_1_456_1.jpg', NULL, NULL, NULL, NULL, '0'),
(2, '2', 'JEE INSTITUTE OF LEARNING', 'Narela, New Delhi, Delhi, India', '', '28.853959699999997', '77.0917838', 'https://taktii.com/etc/api/institute_image/inst_2_137_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_2_9629_2.jpg', NULL, NULL, NULL, '0'),
(3, '26', 'Taktii Library', 'WZ-13, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.622003510956908', '77.0766723973289', 'https://taktii.com/etc/api/institute_image/inst_26_1262_1.jpg', NULL, NULL, NULL, NULL, '0'),
(4, '18', 'Aptron Academy Of Science Ad Tech', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219835', '77.0765372', 'https://taktii.com/etc/api/institute_image/inst_18_7447_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_18_4916_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_18_5984_3.jpg', NULL, NULL, '0'),
(5, '28', 'Aryan Classes', 'Gyan Lok Colony, Kankhal, Haridwar, Uttarakhand, India', '', '29.931723899999994', '78.1436159', NULL, NULL, NULL, NULL, NULL, '0'),
(6, '30', 'Athns Roorkee', 'Roorkee, Uttarakhand, India', '', '29.854262600000002', '77.8880002', NULL, NULL, NULL, NULL, NULL, '0'),
(7, '29', 'ATHENS', 'Ganpativihar Ganeshpur, Gandhi Nagar, Roorkee, Uttarakhand 247667, India', '', '29.8636478', '77.8733505', NULL, NULL, NULL, NULL, NULL, '0'),
(8, '31', 'Gurukul Education Form', 'Dr Baldev Singh Marg, Civil Lines, Roorkee, Uttarakhand 247667, India', '', '29.8679209', '77.8887104', NULL, NULL, NULL, NULL, NULL, '0'),
(9, '19', 'Maairaag', 'Rajputana St, Rajputana West, Purani Tehsil, Roorkee, Uttarakhand 247667, India', '', '29.8767386', '77.88558900000001', NULL, NULL, NULL, NULL, NULL, '0'),
(10, '44', 'Aptron Academy', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6220082', '77.0765253', NULL, NULL, NULL, NULL, NULL, '0'),
(11, '46', 'JSR COACHING CENTRE', 'Subhash Nagar, Jwalapur, Uttarakhand, India', '', '29.9254999', '78.0908651', NULL, NULL, NULL, NULL, NULL, '0'),
(12, '58', 'Aptron Acadey', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219805', '77.0765247', NULL, NULL, NULL, NULL, NULL, '0'),
(13, '57', NULL, NULL, '', NULL, NULL, 'https://taktii.com/etc/api/institute_image/inst_57_3711_1.jpg', NULL, NULL, NULL, NULL, '0'),
(14, '61', 'TAKTII INSTITUTE OF TECHNOLOGY', 'A2, Janakpuri, Block B1, Janakpuri, New Delhi, Delhi 110058, India', '', '28.630736499999998', '77.0890993', 'https://taktii.com/etc/api/institute_image/inst_61_3270_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_61_1994_2.jpg', NULL, NULL, NULL, '0'),
(15, '60', 'Tansen Academy', 'Janakpuri, New Delhi, Delhi, India', '', '28.621899', '77.08783849999999', 'https://taktii.com/etc/api/institute_image/inst_60_5566_1.jpg', NULL, NULL, NULL, NULL, '0'),
(16, '68', 'Aptron Academy', 'Dwarka Sector-3, Dwarka, Delhi, India', '', '28.607817599999997', '77.04061469999999', NULL, NULL, NULL, NULL, NULL, '0'),
(17, '74', 'Power Institute Of Life Dev', 'K, Uttam Nagar, Sitapuri Part 2, Sitapuri, 54, 25 Feet Rd, Chanakya Place II, Chanakya Place I, Bindapur, New Delhi, Delhi 110059, India', 'Janak Puri Detail', '28.612113448764823', '77.07712903618813', 'https://taktii.com/etc/api/institute_image/inst_74_7690_1.jpg', NULL, NULL, NULL, NULL, '0'),
(18, '75', 'FET Agra College Agra', 'Moti Katra Road, Mantola, Agra, Uttar Pradesh 282003, India', 'ss mota singh marg', '27.1865174', '78.0044156', 'https://taktii.com/etc/api/institute_image/inst_75_8530_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_75_323_2.jpg', NULL, NULL, NULL, '0'),
(19, '80', 'S.k', 'Budh Vihar Phase II, Budh Vihar, Delhi, India', '', '28.718565500000004', '77.0895421', NULL, NULL, NULL, NULL, NULL, '0'),
(20, '79', 'Aptron Academy', 'Vikaspuri, New Delhi, Delhi 110018, India', '', '28.6387532', '77.0738028', NULL, NULL, NULL, NULL, NULL, '0'),
(21, '39', 'Aptron Academy', 'Punjabi Bagh, New Delhi, Delhi, India', '', '28.673751000000003', '77.12733759999999', 'https://taktii.com/etc/api/institute_image/inst_39_676_1.jpg', NULL, NULL, NULL, NULL, '0'),
(22, '90', 'Takktii Academy', 'A-2, Janakpuri, Asalatpur Village, Janakpuri, New Delhi, Delhi 110058, India', '', '28.6221107', '77.0764598', NULL, NULL, NULL, NULL, NULL, '0'),
(23, '97', 'Institute', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219794', '77.0765236', NULL, NULL, NULL, NULL, NULL, '0'),
(24, '99', 'Alpha Institute', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219751', '77.0765396', NULL, NULL, NULL, NULL, NULL, '0'),
(25, '105', 'Educom Institute', 'RZ-6, Manas Kunj Rd, Santosh Park, Uttam Nagar, Delhi, 110059, India', 'Rz -6,Manas Kunj Rd,Santosh Park,Uttam Nagar,New Delhi.', '28.6172647', '77.0593287', NULL, NULL, NULL, NULL, NULL, '0'),
(26, '134', 'Individual', 'JNU Ring Rd, Jawaharlal Nehru University, New Delhi, Delhi 110067, India', '', '28.53911236', '77.16579308', NULL, NULL, NULL, NULL, NULL, '0'),
(27, '117', NULL, NULL, '', NULL, NULL, 'https://taktii.com/etc/api/institute_image/inst_117_8493_1.jpg', NULL, NULL, NULL, NULL, '0'),
(28, '107', 'Abdul Kalam Grahmin Bank', '13F, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219705', '77.0765143', NULL, NULL, NULL, NULL, NULL, '0'),
(29, '135', 'Sudhin Classes', 'Janakpuri District Center, Janakpuri, Delhi, India', '', '28.6295538', '77.0801899', 'https://taktii.com/etc/api/institute_image/inst_135_4118_1.jpg', NULL, NULL, NULL, NULL, '0'),
(30, '136', 'Dramtram Arts Of Drama', 'Vikaspuri, New Delhi, Delhi 110018, India', '', '28.6387532', '77.0738028', 'https://taktii.com/etc/api/institute_image/inst_136_5115_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_136_2432_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_136_6163_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_136_9651_4.jpg', 'https://taktii.com/etc/api/institute_image/inst_136_7142_5.jpg', '0'),
(31, '164', 'Aptron Academy', 'Janakpuri, Block B1, Janakpuri, New Delhi, Delhi 110058, India', '', '28.631961', '77.0878318', 'https://taktii.com/etc/api/institute_image/inst_164_665_1.jpg', NULL, NULL, NULL, NULL, '0'),
(32, '166', 'Career Coaching Centre', 'D 149, Saini Mohalla, Block D, Vandana Vihar, Nangloi, Delhi, 110041, India', '', '28.6747669', '77.0717363', NULL, NULL, NULL, NULL, NULL, '0'),
(33, '167', 'Maa Study Centre', 'F-74, Saini Mohalla, Laxmi Park, Block B, Lakshmi Park, Nangloi, Delhi, 110041, India', '', '28.6749803', '77.0701083', 'https://taktii.com/etc/api/institute_image/inst_167_1193_1.jpg', NULL, NULL, NULL, NULL, '0'),
(34, '174', 'Rajan Tuition Centre', '20021006, Asalatpur Village, Janakpuri, Delhi, 110058, India', '', '28.6219429', '77.0765916', 'https://taktii.com/etc/api/institute_image/inst_174_5996_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_174_639_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_174_3188_3.jpg', NULL, NULL, '0'),
(35, '155', 'Ayush Study Point', '88, Pocket 6, Sector 21, Rohini, Delhi, 110086, India', '', '28.712925799999997', '77.0695464', 'https://taktii.com/etc/api/institute_image/inst_155_2948_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_155_9032_2.jpg', NULL, NULL, NULL, '0'),
(36, '180', 'Future Point', 'Sector 21, Rohini, Delhi, 110086, India', '', '28.711454999999997', '77.063141', 'https://taktii.com/etc/api/institute_image/inst_180_8398_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_180_4118_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_180_4887_3.jpg', NULL, NULL, '0'),
(37, '183', 'MAIT', 'A2, Janakpuri, Block B1, Janakpuri, New Delhi, Delhi 110058, India', '', '28.630736499999998', '77.0890993', 'https://taktii.com/etc/api/institute_image/inst_183_8797_1.jpg', NULL, NULL, NULL, NULL, '0'),
(38, '190', 'success institute', 'Kapas Hera Extension, Kapashera, New Delhi, Delhi, India', '', '28.525589699999998', '77.0796189', NULL, NULL, NULL, NULL, NULL, '0'),
(39, '189', 'Bright Future Classes', 'Gali Number 5, Kapas Hera Extension, Kapashera, New Delhi, Delhi 110037, India', '', '28.524475', '77.0795817', NULL, NULL, NULL, NULL, NULL, '0'),
(40, '193', 'ABCL ACADEMY', 'N 48, SP Rd, Sector 20 Extension, Block P 1, Block D5, Krishan Vihar, Delhi, 110086, India', '', '28.7063397', '77.0734403', 'https://taktii.com/etc/api/institute_image/inst_193_6415_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_193_8652_2.jpg', NULL, NULL, NULL, '0'),
(41, '192', 'Lakshya Institute', 'Budh Vihar Phase II, Budh Vihar, Delhi, India', '', '28.718565500000004', '77.0895421', NULL, NULL, NULL, NULL, NULL, '0'),
(42, '144', 'REAL INSTITUTE', '124, Pocket 9, Sector 21, Rohini, Delhi, 110086, India', '', '28.7132234', '77.0694791', 'https://taktii.com/etc/api/institute_image/inst_144_586_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_144_6587_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_144_2895_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_144_7204_4.jpg', 'https://taktii.com/etc/api/institute_image/inst_144_9327_5.jpg', '0'),
(43, '198', 'Shanti Bal Model School', 'Rawat\'s apartment, Mahavir Enclave Part 2, Mahavir Enclave Part 3, Mahavir Enclave, New Delhi, Delhi 110059, India', '', '28.6067256', '77.0735173', NULL, NULL, NULL, NULL, NULL, '0'),
(44, '200', 'A1 Institute And Home Tutors', 'Sector 21, Rohini, Delhi, 110086, India', '', '28.711454999999997', '77.063141', 'https://taktii.com/etc/api/institute_image/inst_200_3792_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_200_4443_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_200_2092_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_200_6501_4.jpg', NULL, '0'),
(45, '188', 'Sachin Tuition Centre', 'F-25/128, Ayodhya Chowk, Sector 7, Pocket 25, Sector 7F, Rohini, Delhi 110085, India', '', '28.706882999999998', '77.110047', 'https://taktii.com/etc/api/institute_image/inst_188_8756_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_188_7241_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_188_6105_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_188_4353_4.jpg', 'https://taktii.com/etc/api/institute_image/inst_188_245_5.jpg', '0'),
(46, '209', 'Shree Tutorials', '462, Hanuman nagar, Sector 17A, Sector 17, Gurugram, Haryana 122001, India', '', '28.479828', '77.0618357', NULL, NULL, NULL, NULL, NULL, '0'),
(47, '154', 'EDS Institute', '396, Pocket 9, Pocket 9, Sector 21, Rohini, Delhi, 110086, India', '', '28.713808800000002', '77.06759009999999', 'https://taktii.com/etc/api/institute_image/inst_154_7782_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_154_4934_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_154_7614_3.jpg', NULL, NULL, '0'),
(48, '152', 'Inspire', 'N 48, SP Rd, Sector 20 Extension, Block P 1, Block D5, Krishan Vihar, Delhi, 110086, India', '', '28.7063397', '77.0734403', 'https://taktii.com/etc/api/institute_image/inst_152_8483_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_152_2018_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_152_4476_3.jpg', NULL, NULL, '0'),
(49, '213', 'O-SUM EDUTECH Pvt Ltd', 'D-18, Block D, Ram Datt Enclave, Uttam Nagar, Delhi, 110059, India', '', '28.6234135', '77.0653911', NULL, NULL, NULL, NULL, NULL, '0'),
(50, '219', 'Tejas Coaching Academy', 'Budh Vihar Phase I, Budh Vihar, Delhi, 110086, India', '', '28.709693599999998', '77.08666459999999', 'https://taktii.com/etc/api/institute_image/inst_219_2159_1.jpg', NULL, NULL, NULL, NULL, '0'),
(51, '138', 'BALAJI FUTURE POINT', '26C,Prem Nagar, 23, Prem Nagar, Block E, Param Puri, Uttam Nagar, Delhi, 110059, India', '', '28.6201306', '77.0574362', 'https://taktii.com/etc/api/institute_image/inst_138_514_1.jpg', NULL, NULL, NULL, NULL, '0'),
(52, '225', 'Delhi Super 30', 'Maharaja Agrasain Road, Pocket 25, Sector 7F, Rohini, Delhi, 110085, India', '', '28.706563700000004', '77.11049090000002', 'https://taktii.com/etc/api/institute_image/inst_225_6254_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_225_2846_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_225_9134_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_225_8694_4.jpg', 'https://taktii.com/etc/api/institute_image/inst_225_7043_5.jpg', '0'),
(53, '228', 'Accurate Academy', 'Avantika, Sector 1, Rohini, New Delhi, Delhi 110085, India', '', '28.703538699999996', '77.0986376', 'https://taktii.com/etc/api/institute_image/inst_228_5028_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_228_7954_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_228_2830_3.jpg', NULL, NULL, '0'),
(54, '230', 'Quick Learners', 'Dr Sahib Singh Verma Marg, Kanjhawala Village, Delhi, 110081, India', '', '28.7345377', '77.0052543', NULL, NULL, NULL, NULL, NULL, '0'),
(55, '229', NULL, NULL, '', NULL, NULL, 'https://taktii.com/etc/api/institute_image/inst_229_5215_1.jpg', NULL, NULL, NULL, NULL, '0'),
(56, '232', 'Perfect Study Centre', 'C-78,Sec-1, Behind Durga Mandir, Sir Chotu Ram Marg, Avantika, Sector 1, Rohini, Delhi, 110085, India', '', '28.702969', '77.098722', 'https://taktii.com/etc/api/institute_image/inst_232_6653_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_232_2415_2.jpg', 'https://taktii.com/etc/api/institute_image/inst_232_3800_3.jpg', 'https://taktii.com/etc/api/institute_image/inst_232_6876_4.jpg', NULL, '0'),
(57, '237', 'S.R.Computer Academy', 'Ram Dutt Enclave,, A-97, Block A, Uttam Nagar East, Uttam Nagar, Delhi, 110059, India', '', '28.6207907', '77.0640088', 'https://taktii.com/etc/api/institute_image/inst_237_9582_1.jpg', 'https://taktii.com/etc/api/institute_image/inst_237_7381_2.jpg', NULL, NULL, NULL, '0'),
(58, '238', 'NLC CLASSES', 'A-102, Block E, Ram Datt Enclave, Uttam Nagar, Delhi, 110059, India', '', '28.6204554', '77.064615', NULL, NULL, NULL, NULL, NULL, '0'),
(59, '249', 'Hsus', 'C-56, Pankha Rd, Block E, Jivan Park, Uttam Nagar, New Delhi, Delhi 110059, India', 'hwuebsisbsisv', '28.618540597310616', '77.07152757793666', 'https://taktii.com/etc/api/institute_image/inst_249_2892_1.jpg', NULL, NULL, NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_technical_details`
--

CREATE TABLE `teacher_technical_details` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `teaching_since` varchar(255) NOT NULL,
  `total_exp` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `language` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_technical_details`
--

INSERT INTO `teacher_technical_details` (`id`, `teacher_id`, `teaching_since`, `total_exp`, `description`, `language`, `created`) VALUES
(1, 1, '2013-12-20', '4 years', '4 year of teaching experiemce in Jr\'s tutorial', 'English, Hindi, Assames, Bengali, Gujarati, Kannada, Kashmiri, Konkani, Malayalam', '2017-12-20 09:06:33'),
(2, 2, '2016-12-20', '1 year', 'one year experience of teaching in highly reputed institutions', 'English, Hindi', '2017-12-20 09:06:33'),
(3, 26, '2012-5-10', '5 years  7 months  13 days', 'We provide conceptual learning', 'Hindi', '2017-12-23 09:22:40'),
(9, 24, '2008-12-23', '9 years', 'm teaching  form last 9years', 'English, Hindi', '2017-12-23 11:06:42'),
(4, 9, '2011-12-21', '6 years', 'x-akashian', 'Gujarati, Kannada', '2017-12-21 08:15:51'),
(5, 54, '2017-12-21', '0 days', 'fresher', 'Hindi, English', '2017-12-21 10:10:42'),
(6, 12, '2008-12-22', '9 years', 'ex-akashian', 'English, Hindi, Urdu, Telugu, Tamil, Sanskrit, Nepali, Marathi, Malayalam, Gujarati, Kashmiri, Oriya, Punjabi, Bengali, Assames, Konkani, Kannada', '2017-12-22 07:04:48'),
(7, 18, '1968-12-22', '49 years', 'ex-profesor of IIT MADRAS', 'Assames, Bengali', '2017-12-22 09:23:31'),
(8, 15, '2013-12-22', '4 years', '4 years of experience', 'English, Hindi', '2017-12-22 17:24:21'),
(10, 28, '2003-12-9', '14 years  14 days', '14 years of experience \nCBSE BOARD', 'Hindi, English', '2017-12-23 11:56:06'),
(11, 30, '2007-8-1', '10 years  4 months  23 days', 'Teaching has always been my first career choice. Being a teacher one not only teaches his or her students but also keeps on striving for excellence through experiences at professional front.', 'English, Hindi', '2017-12-24 09:01:33'),
(12, 29, '2006-12-24', '11 years', 'Our aim is to promote student skills and their subjective knowledge', 'English', '2017-12-24 09:03:52'),
(13, 31, '2017-12-24', '0 days', 'life', 'Hindi', '2017-12-24 12:46:40'),
(14, 19, '2013-12-24', '4 years', 'i m highly ambitious for music', 'Hindi', '2017-12-24 15:44:58'),
(15, 38, '2012-12-26', '5 years', 'asdfghjkl', 'Nepali, Marathi', '2017-12-26 06:11:15'),
(16, 42, '2011-12-26', '6 years', '5 years experience in teaching', 'Kannada, Gujarati, Bengali', '2017-12-26 07:43:29'),
(17, 44, '1998-12-26', '19 years', 'hd', 'Nepali, Malayalam', '2017-12-26 08:45:38'),
(18, 46, '2015-7-1', '2 years  5 months  25 days', 'teaching is my passiin', 'Hindi, English', '2017-12-26 12:51:46'),
(19, 58, '1990-12-28', '27 years', 'b.tech', 'Marathi, Malayalam', '2017-12-28 06:06:04'),
(20, 59, '2011-12-28', '6 years', 'awesome', 'Nepali, Marathi, Malayalam', '2017-12-28 06:18:51'),
(21, 57, '2017-12-28', '0 days', 'gucu', 'Nepali, Malayalam', '2017-12-28 08:42:59'),
(22, 61, '1990-12-28', '27 years', 'I am very passionate teacher', 'English, Hindi', '2017-12-28 12:39:53'),
(23, 60, '2011-12-29', '6 years', 'hxhxhx', 'Marathi, Malayalam, Konkani', '2017-12-29 06:38:32'),
(24, 39, '2017-9-18', '3 months  14 days', 'teacher', 'Oriya', '2018-01-01 10:24:39'),
(25, 68, '1995-1-2', '23 years', 'hdhd', 'Nepali, Marathi, Malayalam, Konkani', '2018-01-02 07:24:03'),
(26, 66, '2012-8-1', '5 years  5 months  1 day', 'skilled and interesting teaching style', 'Hindi, English', '2018-01-02 08:38:01'),
(27, 43, '2008-12-1', '9 years  1 month  3 days', 'fhkddyydmbxmhdksghdylyddduhxdudhxnxjssunx', 'Hindi, English', '2018-01-04 12:06:51');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_visitors`
--

CREATE TABLE `teacher_visitors` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teasurebox`
--

CREATE TABLE `teasurebox` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL,
  `pdf_heading` text NOT NULL,
  `pdf_desc` text NOT NULL,
  `img_heading` text NOT NULL,
  `img_desc` text NOT NULL,
  `pdf_img` varchar(255) NOT NULL,
  `teasure_ing` varchar(255) NOT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `subject_id` varchar(255) NOT NULL,
  `class_id` varchar(255) NOT NULL,
  `subject_name` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `chapter_name` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teasurebox`
--

INSERT INTO `teasurebox` (`id`, `teacher_id`, `batch_id`, `type`, `pdf_heading`, `pdf_desc`, `img_heading`, `img_desc`, `pdf_img`, `teasure_ing`, `video_url`, `subject_id`, `class_id`, `subject_name`, `class_name`, `chapter_name`, `topic`, `created`) VALUES
(1, 1, 0, 'pdf', 'alts d vista', 'alts d vista', '', '', 'teasurebox_1_1_3748.pdf', '', NULL, '', '', '', '', '', '', '2017-12-20 07:09:18'),
(2, 1, 0, 'image', '', '', 'alts d vista', 'alts d vista', '', 'teasurebox_4073.jpg', NULL, '', '', '', '', '', '', '2017-12-20 07:09:40'),
(3, 1, 0, 'video', '', '', 'alts d vista', 'alts d vista', '', '', 'https://youtu.be/pdkHZSnzofY', '', '', '', '', '', '', '2017-12-20 07:10:20'),
(4, 1, 0, 'ppt', 'https://youtu.be/pdkHZSnzofYjddhdhdhdhhssjshshshsh', 'hzhshshshhshshshhshshhshshshssjsjalakksjjsjsj', '', '', 'teasurebox_1_1_6239.ppt', '', NULL, '', '', '', '', '', '', '2017-12-20 07:11:04'),
(5, 1, 0, 'ppt', 'https://youtu.be/pdkHZSnzofY', '', '', '', 'teasurebox_1_1_7437.ppt', '', NULL, '', '', '', '', '', '', '2017-12-20 07:18:07'),
(6, 1, 0, 'ppt', 'where', '', '', '', 'teasurebox_1_1_6831.ppt', '', NULL, '', '', '', '', '', '', '2017-12-20 07:19:45'),
(7, 1, 0, 'ppt', 'alta d viata part 2', '', '', '', 'teasurebox_1_1_1091.ppt', '', NULL, '', '', '', '', '', '', '2017-12-20 07:25:56'),
(8, 1, 0, 'pdf', 'helloo', '', '', '', 'teasurebox_1_1_2466.pdf', '', NULL, '', '', '', '', '', '', '2017-12-20 07:27:06'),
(9, 1, 0, 'pdf', 'bdbdd', '', '', '', 'teasurebox_1_1_9813.pdf', '', NULL, '', '', '', '', '', '', '2017-12-20 07:27:19'),
(10, 1, 0, 'image', '', '', 'bssbb', '', '', 'teasurebox_3429.jpg', NULL, '', '', '', '', '', '', '2017-12-20 07:27:38'),
(11, 1, 0, 'image', '', '', 'ndndb', '', '', 'teasurebox_4074.jpg', NULL, '', '', '', '', '', '', '2017-12-20 07:28:21'),
(12, 1, 0, 'video', '', '', 'hdhdh', '', '', '', 'https://youtu.be/zX65xRqH-S4', '', '', '', '', '', '', '2017-12-20 07:28:55'),
(13, 1, 0, 'video', '', '', 'hshshs', '', '', '', 'https://youtu.be/zX65xRqH-S4', '', '', '', '', '', '', '2017-12-20 07:29:46'),
(15, 1, 2, 'video', '', '', 'learn for all', 'yhruf', '', '', 'https://youtu.be/KcWXKmnZZVo', '', '', '', '', '', '', '2017-12-21 08:40:08'),
(16, 42, 0, 'pdf', 'abcd', 'abcd', '', '', 'teasurebox_42_42_2107.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:17:00'),
(17, 42, 0, 'image', '', '', 'abcsef', 'abcdefgh', '', 'teasurebox_2330.jpg', NULL, '', '', '', '', '', '', '2017-12-26 08:19:02'),
(18, 44, 0, 'pdf', 'hdhhd', 'jdjdjdddhdddhs', '', '', 'teasurebox_44_44_6642.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:28:58'),
(19, 44, 0, 'pdf', 'hdhdh', '', '', '', 'teasurebox_44_44_1989.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:31:04'),
(20, 44, 0, 'pdf', 'ghh', '', '', '', 'teasurebox_44_44_2326.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:32:03'),
(21, 44, 0, 'video', '', '', 'hdhhd', '', '', '', 'https://youtu.be/OYrPYGf18mY', '', '', '', '', '', '', '2017-12-26 08:33:16'),
(22, 44, 0, 'ppt', 'hdhdh', '', '', '', 'teasurebox_44_44_8660.ppt', '', NULL, '', '', '', '', '', '', '2017-12-26 08:35:41'),
(23, 44, 0, 'ppt', 'hshdhd', '', '', '', 'teasurebox_44_44_8458.ppt', '', NULL, '', '', '', '', '', '', '2017-12-26 08:37:08'),
(24, 44, 0, 'ppt', 'hddd', '', '', '', 'teasurebox_44_44_2289.ppt', '', NULL, '', '', '', '', '', '', '2017-12-26 08:38:09'),
(25, 44, 0, 'pdf', 'hdhd', '', '', '', 'teasurebox_44_44_6540.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:57:34'),
(26, 44, 0, 'pdf', 'hdh', '', '', '', 'teasurebox_44_44_6274.pdf', '', NULL, '', '', '', '', '', '', '2017-12-26 08:58:57'),
(27, 44, 0, 'pdf', 'bxbbx', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-26 08:59:31'),
(28, 44, 0, 'pdf', 'bxbbx', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-26 09:05:46'),
(29, 44, 0, 'pdf', 'bxbbx', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-26 09:06:22'),
(32, 44, 0, 'pdf', 'hi guys\nHH', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-26 09:12:35'),
(31, 44, 0, 'pdf', 'dyyf', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-26 09:09:21'),
(33, 50, 0, 'pdf', 'ghvj', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-27 10:12:12'),
(34, 50, 0, 'pdf', 'chhvh', '', '', '', '', '', NULL, '', '', '', '', '', '', '2017-12-27 10:14:57'),
(35, 59, 0, 'pdf', 'hdhd', '', '', '', 'teasurebox_59_59_7266.pdf', '', NULL, '', '', '', '', '', '', '2017-12-28 07:04:25'),
(36, 59, 0, 'image', '', '', 'hdhdh', '', '', 'teasurebox_2221.jpg', NULL, '', '', '', '', '', '', '2017-12-28 07:05:44'),
(37, 68, 0, 'video', '', '', 'hdhdd', '', '', '', 'https://youtu.be/QMRt5I9YA3Y', '', '', '', '', '', '', '2018-01-02 07:29:27'),
(57, 49, 0, 'pdf', 'ifiof', 'kgdkfkdyoydof idkdkdykky dkgxkx', '', '', 'teasurebox_49_49_4978.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:34'),
(56, 49, 0, 'pdf', 'yeufdt', 'hx kh xitd', '', '', 'teasurebox_49_49_2378.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:36'),
(40, 39, 0, 'image', '', '', 'dps', 'new year', '', 'teasurebox_4609.jpg', NULL, '', '', '', '', '', '', '2018-01-05 07:07:11'),
(41, 68, 0, 'ppt', 'gello vikkubhau', '', '', '', 'teasurebox_68_68_3819.ppt', '', NULL, '', '', '', '', '', '', '2018-01-09 09:09:50'),
(42, 9, 0, 'ppt', 'hello c bhaiya', '', '', '', 'teasurebox_9_9_1016.ppt', '', NULL, '', '', '', '', '', '', '2018-01-09 09:13:43'),
(76, 99, 0, 'pdf', 'climate', 'changes on climate', '', '', 'teasurebox_99_99_3110.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:22'),
(44, 80, 0, 'pdf', 'science', 'class5', '', '', '', '', NULL, '', '', '', '', '', '', '2018-01-10 06:30:27'),
(45, 80, 0, 'image', '', '', 'ndhsh', '', '', 'teasurebox_4314.jpg', NULL, '', '', '', '', '', '', '2018-01-10 06:34:16'),
(46, 80, 0, 'image', '', '', 'nhg', '', '', 'teasurebox_8605.jpg', NULL, '', '', '', '', '', '', '2018-01-10 06:35:12'),
(55, 39, 0, 'image', '', '', 'batch', '', '', 'teasurebox_3334.jpg', NULL, '', '', '', '', '', '', '2018-01-15 05:03:46'),
(48, 9, 0, 'pdf', 'hellooo', '', '', '', '', '', NULL, '', '', '', '', '', '', '2018-01-10 06:59:25'),
(49, 9, 0, 'pdf', 'holi', '', '', '', '', '', NULL, '', '', '', '', '', '', '2018-01-10 06:59:46'),
(54, 61, 0, 'image', '', '', 'biology', 'taktii', '', 'teasurebox_7020.jpg', NULL, '', '', '', '', '', '', '2018-01-10 12:02:25'),
(58, 49, 0, 'pdf', '077td7tddixo6d97do7d7n to 68t8ft9y', '', '', '', 'teasurebox_49_49_6547.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:31'),
(69, 90, 0, 'image', '', '', 'hdhdddshhsjsjjsjsjjhshshdhskelskssjssjsjsjjsssjsjss', 'jdjdjdnddjndjdndjdsjsjsjsaowkeenennndjdaaksddjdjdjdjdjssksqwieurrndndjddjydlstztzmttzmzmzmzmzzyzyhxuccjkvkvuusiyzlfydfyaodtodyisuiUguziuzoysojdjdjdnddjndjdndjdsjsjsjsaowkeenennndjdaaksddjdjdjdjdjssksqwieurrndndjddjydlstztzmttzmzmzmzmzzyzyhxuccjkvkvuusiyzlfydfyaodtodyisuiUguziuzoysojdjdjdnddjndjdndjdsjsjsjsaowkeenennndjdaaksddjdjdjdjdjssksqwieurrndndjddjydlstztzmttzmzmzmzmzzyzyhxuccjkvkvuusiyzlfydfyaodtodyisuiUguziuzoyso', '', 'teasurebox_8519.jpg', NULL, '', '', '', '', '', '', '2018-01-19 06:14:30'),
(60, 49, 0, 'image', '', '', 'f7f7g8t', '', '', 'teasurebox_2277.jpg', NULL, '', '', '', '', '', '', '2018-01-16 07:25:30'),
(61, 49, 0, 'video', '', '', '<font color=\'#FFFFFF\'>ADD Videos</font>', '', '', '', 'https://youtu.be/oQHZojI5OlM', '', '', '', '', '', '', '2018-01-16 08:48:46'),
(62, 49, 0, 'video', '', '', '8t8y9cy9ycc', '', '', '', 'https://youtu.be/oQHZojI5OlM', '', '', '', '', '', '', '2018-01-16 08:50:36'),
(63, 75, 31, 'video', '', '', 'jdfxkt', 'njxhkxkxkch', '', '', 'https://youtu.be/oQHZojI5OlM', '', '', '', '', '', '', '2018-01-16 08:57:31'),
(64, 61, 0, 'image', '', '', 'physics rotation', 'Kepler law', '', 'teasurebox_6561.jpg', NULL, '', '', '', '', '', '', '2018-01-16 14:12:18'),
(127, 164, 0, 'video', '', '', 'maths video', '', '', '', 'YouTube.com', '', '', '', '', '', '', '2018-02-06 09:39:47'),
(120, 74, 64, 'video', '', '', 'Fiz', 'Jf', '', '', 'https://youtu.be/2bIMrtCOQEo', '', '', '', '', '', '', '2018-01-25 11:52:12'),
(121, 74, 64, 'video', '', '', 'Sbsb', 'Bsbsb', '', '', 'https://youtu.be/2bIMrtCOQEo', '', '', '', '', '', '', '2018-01-25 11:56:55'),
(122, 117, 0, 'mp4', 'jkkk', 'hshsjej', '', '', 'teasurebox_117_117_1174394.pdf', '', NULL, '', '', '', '', '', '', '2018-01-29 06:22:39'),
(124, 164, 95, 'video', '', '', 'embassador', 'embassador to do', '', '', 'YouTube.com', '', '', '', '', '', '', '2018-02-06 09:35:51'),
(125, 164, 0, 'pdf', 'abstaract', 'abstract vision', '', '', 'teasurebox_164_164_1649379.pdf', '', NULL, '', '', '', '', '', '', '2018-02-06 09:38:39'),
(126, 164, 0, 'image', '', '', 'help others', '', '', 'teasurebox_3646.jpg', NULL, '', '', '', '', '', '', '2018-02-06 09:39:14'),
(70, 90, 0, 'ppt', 'image have to come for itthat bsbdbdbdhdjsjhsjhsbsjdjdjs', 'bxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdjbxbhhxdhhdbdbsbbshsnsnskjssjjsjsjsjssjsjskssjsjsjssjsjjsjsjsjsjssjsjskssksjsssjjssksaksnddbnddsjsnsnsssskssjdj', '', '', 'teasurebox_90_90_2068.ppt', '', NULL, '', '', '', '', '', '', '2018-01-19 06:24:27'),
(71, 90, 0, 'ppt', 'nsjshhhsddhdhhdhsjjejejsjsjsjsjsjjdjjsjdjjdjdjdjdjdjdjdjdjdjjd', 'hdhdhdhdhdhfdhjdjdjdjdjjdjdjdjdjjdhdjdjjddjjdjdjdjkdksklslappqieueurhrhjfjfjfjdjkdmnxncncfjeopwpwjdjdjdjdjjdjdkqlpqieirir8ejjrjdjdjdnfnjfjdkkdkamcnncmdkspwieurntNxkdosjdjjdkdkskskdjdje', '', '', 'teasurebox_90_90_2337.ppt', '', NULL, '', '', '', '', '', '', '2018-01-19 06:45:22'),
(72, 90, 0, 'ppt', 'hdhdhshshhshshshhs', 'hshsss', '', '', 'teasurebox_90_90_9157.ppt', '', NULL, '', '', '', '', '', '', '2018-01-19 06:49:10'),
(73, 90, 0, 'pdf', 'hdhddddh', 'hdhdhddhd', '', '', 'teasurebox_90_90_9854.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:26'),
(74, 90, 0, 'pdf', 'hdhdhd', 'dhdhd', '', '', 'teasurebox_90_90_6199.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:45:24'),
(77, 99, 0, 'image', '', '', 'data', 'data on science', '', 'teasurebox_6638.jpg', NULL, '', '', '', '', '', '', '2018-01-21 07:42:29'),
(81, 49, 0, 'ppt', 'x6r7txt8itd', 'yccyyfgyuvuvivicyhfc8 fcioc', '', '', 'teasurebox_49_49_7282.ppt', '', NULL, '', '', '', '', '', '', '2018-01-22 09:54:19'),
(82, 49, 0, 'ppt', 'gyhuu', 'uguhhu', '', '', 'teasurebox_49_49_8800.ppt', '', NULL, '', '', '', '', '', '', '2018-01-22 10:08:49'),
(84, 75, 52, 'video', '', '', 'video testing', '', '', '', 'https://youtu.be/IT2kcxJCNBQ', '', '', '', '', '', '', '2018-01-24 09:30:41'),
(94, 107, 0, 'pdf', 'hdjdhdhfhhhd', 'hdjdjdjjddjdkddldk', '', '', 'teasurebox_107_107_1077409.pdf', '', NULL, '', '', '', '', '', '', '2018-01-24 09:49:21'),
(95, 49, 104, 'video', '', '', 'kfzifuz', 'nxjckc', '', '', 'https://youtu.be/ljUByjCvKT0', '', '', '', '', '', '', '2018-01-24 12:28:17'),
(96, 49, 104, 'video', '', '', '8e8rirt', 'lfylglg', '', '', 'https://youtu.be/ljUByjCvKT0', '', '', '', '', '', '', '2018-01-24 13:39:20'),
(97, 117, 0, 'image', '', '', 'kibrary', 'value for money .value for time', '', 'teasurebox_9062.jpg', NULL, '', '', '', '', '', '', '2018-01-24 19:52:04'),
(98, 9, 0, 'image', '', '', 'ndbdhdbdhdbd', '', '', 'teasurebox_4493.jpg', NULL, '', '', '', '', '', '', '2018-01-25 04:58:19'),
(99, 9, 0, 'image', '', '', 'jdhdhdhhd', 'ndjdjdjjdksllskaksksjdhhdjdksjsjdjdkdkdjddjjdksksllalaksjhdbdndndndndndklsksjdjdbndlslslalwoeijdjdjd', '', 'teasurebox_6181.jpg', NULL, '', '', '', '', '', '', '2018-01-25 04:59:00'),
(100, 9, 0, 'pdf', 'hdbhd', 'bdhdhdhdbbdbdbdn', '', '', 'teasurebox_9_9_99014.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 07:18:47'),
(101, 9, 0, 'image', '', '', 'bdhdhjdjd', 'hdjdjdhd', '', 'teasurebox_3466.jpg', NULL, '', '', '', '', '', '', '2018-01-25 07:20:29'),
(102, 9, 0, 'video', '', '', 'hdhdhdhd', 'hdbdbbdbd', '', '', 'https://youtu.be/oA9Pv2NKuio', '', '', '', '', '', '', '2018-01-25 07:21:13'),
(103, 49, 104, 'video', '', '', 'hsnfz', 'ncnbvnvv', '', '', 'https://youtu.be/nCvmBH0drQ8', '', '', '', '', '', '', '2018-01-25 07:29:34'),
(104, 49, 0, 'video', '', '', 'jfgsjf', 'jdjfkf', '', '', 'https://youtu.be/nCvmBH0drQ8', '', '', '', '', '', '', '2018-01-25 07:35:21'),
(105, 87, 114, 'video', '', '', 'Sjhsh', 'Bahab', '', '', 'Youtube.Com', '', '', '', '', '', '', '2018-01-25 07:45:02'),
(106, 87, 0, 'pdf', 'Hdjjd', 'Bxhxhxhdhd', '', '', 'teasurebox_87_87_875245.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 10:55:58'),
(107, 87, 0, 'image', '', '', 'Hdhhd', 'Hdhhhr', '', 'teasurebox_2736.jpg', NULL, '', '', '', '', '', '', '2018-01-25 10:57:56'),
(108, 87, 0, 'image', '', '', 'Hdhhd', 'Hdhdhd', '', 'teasurebox_8161.jpg', NULL, '', '', '', '', '', '', '2018-01-25 11:04:09'),
(109, 87, 0, 'pdf', 'Snbsb', 'Haha', '', '', 'teasurebox_87_87_876793.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 11:07:58'),
(110, 87, 0, 'pdf', 'Snsn', 'Nsnsn', '', '', 'teasurebox_87_87_876409.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 11:09:14'),
(111, 87, 0, 'pdf', 'Jsnsb', 'Nnznsn', '', '', 'teasurebox_87_87_873430.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 11:14:27'),
(112, 87, 0, 'pdf', 'Ab b', 'Babab', '', '', 'teasurebox_87_87_874418.pdf', '', NULL, '', '', '', '', '', '', '2018-01-25 11:17:01'),
(113, 87, 0, 'video', '', '', 'Z Bsb', 'BBABA', '', '', 'Youtube.com', '', '', '', '', '', '', '2018-01-25 11:30:43'),
(115, 74, 64, 'video', '', '', 'Hehhe', 'Hehehehhd', '', '', 'Youtube.com', '', '', '', '', '', '', '2018-01-25 11:44:38'),
(116, 74, 64, 'video', '', '', 'Hdhhd', 'Dhjdhdhd', '', '', 'Youtube.com', '', '', '', '', '', '', '2018-01-25 11:45:20'),
(117, 74, 64, 'video', '', '', 'Zjjsjs', 'Hahaha', '', '', 'https://youtu.be/2bIMrtCOQEo', '', '', '', '', '', '', '2018-01-25 11:48:06'),
(118, 74, 64, 'video', '', '', 'Uf, Tu', 'Uf, uf,', '', '', 'https://youtu.be/2bIMrtCOQEo', '', '', '', '', '', '', '2018-01-25 11:50:22'),
(119, 74, 64, 'video', '', '', 'Jtzit', 'Igxig8x', '', '', 'https://youtu.be/2bIMrtCOQEo', '', '', '', '', '', '', '2018-01-25 11:51:26'),
(128, 136, 0, 'pdf', 'neved success story', '', '', '', 'teasurebox_136_136_1362434.pdf', '', NULL, '', '', '', '', '', '', '2018-02-07 06:26:13'),
(129, 136, 0, 'pdf', 'jdhd', '', '', '', 'teasurebox_136_136_1361069.pdf', '', NULL, '', '', '', '', '', '', '2018-02-07 06:26:27'),
(130, 136, 0, 'image', '', '', 'bdhd', '', '', 'teasurebox_3721.jpg', NULL, '', '', '', '', '', '', '2018-02-07 06:27:22'),
(131, 136, 0, 'image', '', '', 'hdhdh', '', '', 'teasurebox_5006.jpg', NULL, '', '', '', '', '', '', '2018-02-07 06:27:40'),
(132, 136, 0, 'ppt', 'wat to do', 'have a understnd on what to have', '', '', 'teasurebox_136_136_1362900.ppt', '', NULL, '', '', '', '', '', '', '2018-02-08 06:24:13'),
(133, 180, 0, 'image', '', '', 'chemical reaction and eguation', 'class 10th', '', 'teasurebox_9170.jpg', NULL, '', '', '', '', '', '', '2018-02-08 11:45:29'),
(134, 180, 0, 'image', '', '', 'chemical reaction and eguation', 'class 10th', '', 'teasurebox_1482.jpg', NULL, '', '', '', '', '', '', '2018-02-08 11:47:06'),
(135, 136, 0, 'video', '', '', 'what to do', '', '', '', 'https://youtu.be/q31US-yTS90', '', '', '', '', '', '', '2018-02-09 07:27:54'),
(153, 241, 0, 'all', 'bsjd', 'neje', '', '', 'teasurebox_241_241_2418730.doc', '', NULL, '', '', '', '', '', '', '2018-03-15 06:09:59'),
(152, 75, 0, 'all', 'hshshdhdhdbb', '', '', '', 'teasurebox_75_75_756561.pdf', '', NULL, '', '', '', '', '', '', '2018-03-08 08:45:50'),
(138, 176, 0, 'ppt', 'ff', 'fdd', '', '', 'teasurebox_176_176_1762402.ppt', '', NULL, '', '', '', '', '', '', '2018-02-09 09:15:38'),
(139, 136, 0, 'ppt', 'hellkochennu bhaiya', 'hdhshshshxyxydyxydmyxysmyfuftFzckzyxmzctzyRXt bshshss bdhddhdjsssjsslla', '', '', 'teasurebox_136_136_1368089.ppt', '', NULL, '', '', '', '', '', '', '2018-02-09 11:39:39'),
(140, 136, 0, 'ppt', 'tztsktskydydmssd6dkddkdk5dl6dldls65s5sks5s6d6dyd6', 'dyktstststsjtydrsrststsfsttstddfyghdkhdjgdgdigydfydigydigydfhjohudugfdffhtdSTsydtsydkd6dyddyxlxyxyxy', '', '', 'teasurebox_136_136_1366207.ppt', '', NULL, '', '', '', '', '', '', '2018-02-09 11:50:43'),
(141, 144, 0, 'image', '', '', '9th Science', 'Atom & molecules', '', 'teasurebox_8688.jpg', NULL, '', '', '', '', '', '', '2018-02-09 12:15:35'),
(142, 188, 0, 'image', '', '', 'STCians Gems', '', '', 'teasurebox_1922.jpg', NULL, '', '', '', '', '', '', '2018-02-11 03:05:21'),
(143, 188, 0, 'image', '', '', 'STCian Gems', '', '', 'teasurebox_1261.jpg', NULL, '', '', '', '', '', '', '2018-02-11 03:06:29'),
(144, 154, 0, 'image', '', '', '9th test maths', 'full syllabus test.', '', 'teasurebox_2321.jpg', NULL, '', '', '', '', '', '', '2018-02-12 08:16:16'),
(145, 154, 0, 'image', '', '', '9th class maths test', 'full syllabus', '', 'teasurebox_5971.jpg', NULL, '', '', '', '', '', '', '2018-02-12 08:18:00'),
(147, 176, 0, 'ppt', 'hh', 'hh', '', '', 'teasurebox_176_176_1765118.ppt', '', NULL, '', '', '', '', '', '', '2018-02-12 11:43:46'),
(149, 223, 0, 'image', '', '', 'Determinant', 'Properties of determinant', '', 'teasurebox_6282.jpg', NULL, '', '', '', '', '', '', '2018-02-13 11:05:58'),
(154, 241, 166, 'video', '', '', 'hjs', 'nwn', '', '', 'https://www.YouTube.com', '', '', '', '', '', '', '2018-03-16 06:54:10'),
(155, 249, 0, 'all', 'hha', 'vhzu', '', '', 'teasurebox_249_249_2491595.doc', '', NULL, '', '', '', '', '', '', '2018-03-16 09:54:52');

-- --------------------------------------------------------

--
-- Table structure for table `teasurebox_std`
--

CREATE TABLE `teasurebox_std` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `treasure_id` int(11) NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teasurebox_std`
--

INSERT INTO `teasurebox_std` (`id`, `student_id`, `treasure_id`, `type`) VALUES
(1, 1, 1, 'pdf'),
(2, 1, 4, 'ppt'),
(3, 1, 3, 'video'),
(4, 1, 2, 'image'),
(21, 1, 34, 'banner'),
(8, 0, 34, 'banner'),
(10, 0, 32, 'banner'),
(11, 0, 38, 'banner'),
(12, 0, 40, 'banner'),
(13, 0, 33, 'banner'),
(15, 0, 35, 'banner'),
(16, 0, 39, 'banner'),
(17, 0, 31, 'banner'),
(18, 0, 36, 'banner'),
(19, 0, 37, 'banner'),
(20, 22, 35, 'pdf'),
(22, 22, 38, 'banner'),
(25, 1, 58, 'banner'),
(26, 1, 57, 'banner'),
(35, 1, 42, 'ppt'),
(28, 1, 55, 'banner'),
(36, 25, 42, 'ppt'),
(38, 6, 56, 'banner'),
(39, 6, 42, 'ppt'),
(46, 6, 55, 'banner'),
(41, 6, 57, 'banner'),
(43, 6, 58, 'banner'),
(44, 25, 44, 'pdf'),
(45, 2, 57, 'banner'),
(47, 30, 55, 'banner'),
(48, 30, 56, 'banner'),
(49, 30, 57, 'banner'),
(50, 30, 58, 'banner'),
(89, 0, 57, 'banner'),
(109, 0, 42, 'ppt'),
(110, 0, 59, 'banner'),
(75, 0, 58, 'banner'),
(79, 0, 56, 'banner'),
(80, 0, 102, 'video'),
(111, 0, 60, 'banner'),
(84, 0, 49, 'pdf'),
(85, 0, 100, 'pdf'),
(86, 0, 48, 'pdf'),
(88, 0, 55, 'banner'),
(90, 33, 55, 'banner'),
(91, 33, 56, 'banner'),
(92, 33, 57, 'banner'),
(93, 33, 58, 'banner'),
(107, 30, 121, 'video'),
(108, 25, 60, 'banner'),
(112, 0, 121, 'video'),
(113, 0, 120, 'video'),
(114, 0, 119, 'video'),
(115, 0, 118, 'video'),
(116, 0, 117, 'video'),
(117, 0, 115, 'video'),
(118, 6, 88, 'video'),
(119, 0, 131, 'image'),
(120, 25, 140, 'ppt'),
(121, 25, 139, 'ppt'),
(122, 25, 132, 'ppt');

-- --------------------------------------------------------

--
-- Table structure for table `test_prep`
--

CREATE TABLE `test_prep` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `creadet` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` bigint(22) NOT NULL,
  `teacher_id` bigint(22) NOT NULL,
  `topic_wise_fees` int(11) NOT NULL,
  `specialization` enum('YES','NO') NOT NULL DEFAULT 'NO',
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE `university` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `username` varchar(32) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `user_role` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `mobile`, `user_role`) VALUES
(1, 'admin', 'admin', 'admin@taktii.com', '71df30a8bf013e97a71655da45033673', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `visits`
--

CREATE TABLE `visits` (
  `id` int(11) NOT NULL,
  `teacher_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `visit_on` varchar(255) NOT NULL,
  `visit_ip` varchar(255) NOT NULL,
  `count` int(99) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visits`
--

INSERT INTO `visits` (`id`, `teacher_id`, `student_id`, `visit_on`, `visit_ip`, `count`) VALUES
(4, '2', '4', '03-01-2018 12:20:26', '47.30.209.72', 3),
(5, '9', '4', '03-01-2018 12:20:42', '47.30.209.72', 6),
(6, '54', '4', '03-01-2018 12:20:54', '47.30.209.72', 1),
(7, '57', '4', '03-01-2018 17:07:25', '103.212.145.158', 3),
(8, '2', '22', '03-01-2018 17:22:54', '103.212.145.158', 68),
(9, '26', '22', '03-01-2018 17:23:00', '103.212.145.158', 22),
(10, '28', '22', '03-01-2018 17:23:05', '103.212.145.158', 54),
(11, '2', '6', '03-01-2018 20:32:10', '1.39.28.5', 24),
(12, '9', '000', '04-01-2018 14:48:29', '103.212.145.158', 40),
(13, '2', '000', '04-01-2018 14:48:38', '103.212.145.158', 10),
(14, '26', '000', '04-01-2018 22:24:58', '106.202.92.223', 19),
(15, '9', '2', '04-01-2018 22:26:39', '106.202.92.223', 28),
(16, '9', '22', '05-01-2018 09:31:29', '103.212.145.158', 58),
(17, '30', '22', '05-01-2018 09:42:39', '103.212.145.158', 8),
(18, '2', '1', '05-01-2018 09:59:59', '103.212.147.217', 16),
(19, '30', '1', '05-01-2018 10:00:58', '103.212.147.217', 3),
(20, '9', '1', '05-01-2018 10:45:19', '47.31.104.44', 72),
(21, '26', '7', '05-01-2018 13:51:40', '139.167.32.28', 2),
(22, '6', '1', '05-01-2018 17:23:58', '47.31.126.65', 2),
(23, '74', '2', '05-01-2018 18:30:03', '103.212.147.217', 4),
(24, '9', '3', '06-01-2018 11:55:33', '103.44.137.61', 8),
(25, '74', '27', '06-01-2018 17:44:40', '47.31.79.254', 1),
(26, '74', '22', '08-01-2018 10:55:50', '103.212.147.217', 1),
(27, '2', '2', '08-01-2018 10:57:05', '103.212.147.217', 24),
(28, '28', '2', '08-01-2018 11:24:01', '103.212.147.217', 1),
(29, '30', '6', '08-01-2018 11:32:03', '1.39.28.154', 3),
(30, '28', '6', '08-01-2018 11:32:15', '1.39.28.154', 7),
(31, '26', '20', '09-01-2018 14:21:21', '103.212.147.217', 7),
(32, '9', '20', '09-01-2018 14:21:24', '103.212.147.217', 24),
(33, '74', '1', '09-01-2018 15:00:25', '47.31.207.190', 11),
(34, '68', '20', '09-01-2018 15:35:37', '103.212.147.217', 1),
(35, '26', '3', '09-01-2018 15:38:30', '103.212.147.217', 6),
(36, '74', '3', '09-01-2018 15:38:47', '103.212.147.217', 5),
(37, '9', '21', '09-01-2018 16:03:02', '1.39.28.33', 1),
(38, '9', '6', '09-01-2018 18:12:28', '103.212.147.217', 19),
(39, '74', '6', '09-01-2018 20:16:05', '103.212.147.217', 4),
(40, '26', '6', '09-01-2018 20:16:33', '103.212.147.217', 11),
(41, '68', '22', '10-01-2018 00:33:26', '103.212.147.217', 2),
(42, '9', '27', '10-01-2018 12:45:19', '47.31.234.183', 2),
(43, '61', '6', '10-01-2018 13:25:56', '103.212.147.217', 2),
(44, '61', '17', '10-01-2018 13:27:31', '103.212.147.217', 3),
(45, '80', '17', '10-01-2018 13:29:13', '103.212.147.217', 22),
(46, '80', '1', '10-01-2018 14:19:57', '103.212.147.217', 12),
(47, '80', '2', '10-01-2018 17:41:52', '45.118.166.114', 15),
(48, '61', '2', '10-01-2018 18:21:33', '106.222.85.73', 6),
(49, '61', '20', '11-01-2018 08:04:56', '103.212.144.134', 4),
(50, '2', '36', '11-01-2018 14:27:56', '47.31.217.10', 1),
(51, '80', '36', '11-01-2018 14:29:11', '47.31.217.10', 2),
(52, '80', '20', '11-01-2018 15:15:58', '103.212.145.43', 1),
(53, '2', '20', '11-01-2018 15:17:31', '103.212.145.43', 10),
(54, '2', '3', '11-01-2018 15:17:32', '103.212.145.43', 1),
(55, '61', '3', '11-01-2018 15:18:18', '103.212.145.43', 2),
(56, '30', '3', '11-01-2018 15:20:40', '103.212.145.43', 2),
(57, '9', '23', '11-01-2018 17:01:08', '103.212.145.43', 3),
(58, '80', '22', '11-01-2018 18:08:14', '103.212.145.43', 2),
(59, '80', '27', '11-01-2018 18:27:36', '47.31.202.55', 1),
(60, '2', '17', '11-01-2018 21:30:56', '110.235.132.13', 1),
(61, '15', '6', '12-01-2018 00:41:03', '1.39.28.0', 22),
(62, '15', '2', '12-01-2018 00:41:19', '223.176.157.13', 9),
(63, '80', '000', '12-01-2018 10:34:43', '103.212.145.43', 7),
(64, '26', '2', '12-01-2018 11:09:55', '103.212.146.112', 1),
(65, '15', '22', '12-01-2018 12:30:43', '103.212.146.112', 20),
(66, '9', '17', '12-01-2018 18:10:22', '103.212.146.112', 3),
(67, '80', '6', '12-01-2018 19:11:44', '103.212.146.112', 3),
(68, '15', '4', '13-01-2018 15:21:04', '47.31.233.114', 7),
(69, '49', '4', '13-01-2018 15:22:08', '47.31.233.114', 19),
(70, '15', '000', '13-01-2018 21:42:46', '103.83.68.182', 16),
(71, '49', '000', '13-01-2018 21:42:59', '103.83.68.182', 20),
(72, '15', '19', '14-01-2018 06:31:27', '125.213.232.115', 3),
(73, '28', '000', '14-01-2018 22:22:28', '103.212.145.7', 6),
(74, '15', '20', '15-01-2018 10:11:45', '103.212.145.139', 8),
(75, '39', '20', '15-01-2018 10:28:27', '103.212.145.139', 11),
(76, '39', '6', '15-01-2018 10:29:44', '103.212.145.139', 7),
(77, '39', '1', '15-01-2018 11:08:03', '103.212.145.139', 3),
(78, '61', '1', '15-01-2018 11:09:32', '103.212.145.139', 3),
(79, '15', '1', '15-01-2018 12:15:28', '103.212.145.139', 12),
(80, '79', '1', '15-01-2018 14:29:08', '47.31.153.57', 18),
(81, '26', '4', '15-01-2018 14:44:19', '47.30.245.141', 4),
(82, '49', '3', '15-01-2018 16:28:33', '103.212.145.139', 3),
(83, '15', '3', '15-01-2018 16:35:01', '103.212.145.139', 9),
(84, '79', '20', '15-01-2018 16:37:02', '103.212.145.139', 1),
(85, '49', '22', '15-01-2018 16:43:40', '103.212.145.139', 12),
(86, '39', '3', '15-01-2018 16:49:05', '103.212.145.139', 9),
(87, '32', '1', '16-01-2018 11:55:57', '103.212.145.139', 1),
(88, '38', '1', '16-01-2018 11:56:14', '103.212.145.139', 1),
(89, '79', '3', '16-01-2018 12:29:00', '103.212.145.139', 1),
(90, '80', '3', '16-01-2018 12:36:05', '103.212.145.139', 2),
(91, '49', '20', '16-01-2018 15:33:57', '103.212.145.139', 2),
(92, '83', '1', '16-01-2018 18:22:35', '103.212.145.139', 7),
(93, '79', '2', '16-01-2018 18:40:18', '106.222.7.115', 1),
(94, '39', '2', '16-01-2018 18:45:53', '103.212.145.139', 2),
(95, '15', '17', '16-01-2018 18:46:42', '103.212.145.139', 1),
(96, '79', '17', '16-01-2018 18:48:43', '103.212.145.139', 2),
(97, '39', '17', '16-01-2018 18:49:48', '103.212.145.139', 2),
(98, '15', '23', '16-01-2018 22:37:18', '47.31.44.120', 6),
(99, '49', '1', '17-01-2018 11:03:44', '103.212.145.139', 23),
(100, '26', '1', '17-01-2018 11:08:37', '103.212.145.139', 12),
(101, '9', '25', '17-01-2018 12:35:46', '103.212.145.139', 19),
(102, '49', '25', '17-01-2018 12:36:13', '103.212.145.139', 42),
(103, '49', '5', '17-01-2018 12:43:48', '47.31.49.110', 3),
(104, '15', '5', '17-01-2018 12:47:45', '47.31.49.110', 4),
(105, '2', '25', '17-01-2018 15:11:31', '103.212.145.139', 19),
(106, '79', '25', '17-01-2018 15:17:52', '103.212.145.139', 1),
(107, '15', '25', '17-01-2018 15:39:57', '103.212.145.139', 34),
(108, '28', '1', '17-01-2018 17:39:32', '103.212.145.139', 3),
(109, '49', '6', '17-01-2018 17:55:59', '1.39.28.24', 4),
(110, '9', '5', '18-01-2018 10:52:50', '47.31.221.40', 5),
(111, '2', '5', '18-01-2018 10:56:19', '47.31.221.40', 1),
(112, '79', '5', '18-01-2018 12:20:08', '47.31.221.40', 3),
(113, '83', '5', '18-01-2018 12:23:51', '47.31.221.40', 1),
(114, '82', '17', '18-01-2018 15:23:10', '47.31.26.64', 1),
(115, '80', '23', '18-01-2018 16:07:47', '47.31.63.3', 11),
(116, '26', '26', '18-01-2018 22:12:41', '47.31.192.100', 1),
(117, '83', '6', '19-01-2018 10:37:59', '103.212.145.139', 2),
(118, '61', '4', '19-01-2018 12:21:34', '103.212.145.139', 2),
(119, '80', '25', '19-01-2018 12:21:37', '103.212.145.139', 11),
(120, '90', '1', '19-01-2018 12:45:39', '103.212.145.139', 9),
(121, '90', '6', '19-01-2018 14:22:42', '103.212.145.139', 1),
(122, '90', '25', '19-01-2018 16:09:42', '103.212.145.139', 5),
(123, '97', '1', '20-01-2018 10:59:33', '103.212.145.139', 4),
(124, '97', '25', '20-01-2018 11:56:46', '103.212.145.139', 10),
(125, '39', '25', '20-01-2018 12:16:36', '103.212.145.139', 5),
(126, '61', '25', '20-01-2018 12:18:43', '103.212.145.139', 1),
(127, '26', '25', '20-01-2018 12:32:22', '103.212.145.139', 18),
(128, '83', '25', '20-01-2018 12:44:25', '103.212.145.139', 3),
(129, '97', '000', '20-01-2018 13:40:32', '103.212.145.139', 1),
(130, '99', '1', '20-01-2018 16:07:23', '103.212.145.139', 9),
(131, '99', '25', '20-01-2018 16:14:53', '103.212.145.139', 14),
(132, '99', '6', '21-01-2018 14:20:16', '1.39.28.99', 6),
(133, '103', '2', '21-01-2018 14:36:05', '106.202.2.115', 1),
(134, '15', '15', '21-01-2018 18:48:29', '103.212.145.98', 3),
(135, '39', '15', '21-01-2018 18:53:23', '103.212.145.98', 1),
(136, '99', '3', '22-01-2018 15:56:03', '103.212.145.139', 4),
(137, '83', '3', '22-01-2018 15:56:49', '103.212.145.139', 1),
(138, '75', '1', '22-01-2018 16:37:33', '103.212.145.139', 9),
(139, '9', '29', '22-01-2018 20:40:36', '106.223.123.48', 1),
(140, '15', '29', '22-01-2018 20:40:52', '106.223.123.48', 1),
(141, '74', '20', '23-01-2018 12:24:41', '103.212.145.139', 5),
(142, '74', '4', '23-01-2018 12:25:28', '47.30.59.240', 20),
(143, '74', '25', '23-01-2018 12:31:25', '103.212.145.139', 10),
(144, '75', '25', '23-01-2018 12:47:21', '103.212.145.139', 8),
(145, '26', '33', '23-01-2018 13:30:10', '106.215.89.38', 1),
(146, '99', '17', '23-01-2018 14:40:40', '47.31.197.211', 1),
(147, '87', '4', '23-01-2018 16:57:15', '47.30.118.112', 3),
(148, '99', '000', '23-01-2018 17:04:40', '47.31.124.158', 9),
(149, '87', '25', '23-01-2018 18:29:13', '103.212.145.139', 18),
(150, '107', '1', '24-01-2018 10:58:51', '47.31.143.131', 1),
(151, '75', '4', '24-01-2018 11:24:33', '103.212.144.59', 8),
(152, '75', '30', '24-01-2018 11:34:10', '47.31.165.117', 22),
(153, '74', '30', '24-01-2018 11:38:32', '47.31.165.117', 17),
(154, '15', '30', '24-01-2018 14:24:00', '47.31.135.16', 10),
(155, '87', '20', '24-01-2018 14:51:53', '103.212.144.59', 9),
(156, '75', '20', '24-01-2018 15:03:28', '103.212.144.59', 5),
(157, '28', '20', '24-01-2018 15:06:15', '103.212.144.59', 2),
(158, '15', '14', '24-01-2018 15:22:21', '47.31.210.254', 25),
(159, '49', '30', '24-01-2018 15:23:24', '47.31.137.89', 14),
(160, '87', '30', '24-01-2018 15:25:54', '47.31.137.89', 35),
(161, '49', '23', '24-01-2018 15:59:52', '47.31.108.95', 1),
(162, '74', '23', '24-01-2018 16:33:59', '47.31.127.20', 2),
(163, '99', '20', '24-01-2018 18:35:05', '103.212.144.59', 1),
(164, '80', '30', '24-01-2018 18:53:09', '103.212.144.59', 3),
(165, '107', '000', '24-01-2018 19:36:25', '108.177.6.57', 8),
(166, '83', '000', '24-01-2018 19:37:22', '108.177.7.84', 9),
(167, '30', '000', '24-01-2018 19:40:59', '108.177.7.91', 3),
(168, '99', '29', '24-01-2018 22:39:30', '103.212.144.59', 1),
(169, '9', '30', '25-01-2018 11:02:34', '103.212.144.59', 16),
(170, '107', '30', '25-01-2018 11:52:29', '103.212.144.59', 5),
(171, '74', '29', '25-01-2018 14:09:30', '106.223.224.199', 1),
(172, '39', '000', '25-01-2018 18:06:03', '103.212.147.176', 2),
(173, '99', '30', '25-01-2018 18:08:02', '103.212.147.176', 7),
(174, '26', '30', '25-01-2018 18:09:10', '103.212.147.176', 3),
(175, '74', '000', '25-01-2018 18:10:45', '103.212.147.176', 4),
(176, '87', '000', '25-01-2018 18:27:05', '103.212.147.176', 5),
(177, '15', '34', '26-01-2018 18:17:35', '103.212.147.176', 3),
(178, '74', '34', '26-01-2018 22:18:56', '42.111.35.63', 2),
(179, '107', '6', '26-01-2018 23:58:46', '103.212.147.176', 1),
(180, '15', '33', '27-01-2018 07:23:36', '106.67.41.6', 1),
(181, '9', '33', '27-01-2018 07:24:11', '106.67.41.6', 1),
(182, '9', '35', '27-01-2018 21:38:03', '106.223.197.52', 1),
(183, '105', '6', '27-01-2018 23:50:10', '103.212.147.176', 6),
(184, '9', '19', '28-01-2018 19:31:07', '106.223.247.168', 1),
(185, '105', '30', '29-01-2018 10:30:33', '103.212.147.176', 10),
(186, '80', '34', '29-01-2018 12:16:49', '42.111.46.209', 1),
(187, '107', '3', '29-01-2018 13:00:58', '103.212.147.176', 1),
(188, '107', '20', '29-01-2018 13:58:21', '103.212.147.176', 1),
(189, '105', '34', '29-01-2018 14:47:52', '42.111.45.20', 2),
(190, '75', '3', '29-01-2018 15:35:56', '47.30.63.242', 2),
(191, '107', '5', '29-01-2018 16:59:55', '103.212.147.176', 16),
(192, '75', '5', '29-01-2018 17:18:44', '103.212.147.176', 13),
(193, '99', '5', '29-01-2018 17:33:01', '103.212.147.176', 2),
(194, '105', '3', '30-01-2018 11:45:33', '103.212.147.176', 2),
(195, '99', '35', '30-01-2018 13:50:58', '106.223.67.255', 1),
(196, '66', '30', '30-01-2018 16:57:57', '47.31.170.165', 7),
(197, '24', '30', '30-01-2018 16:58:00', '47.31.170.165', 21),
(198, '46', '12', '31-01-2018 08:48:16', '139.167.23.97', 1),
(199, '24', '000', '31-01-2018 10:10:16', '103.212.147.176', 13),
(200, '26', '37', '31-01-2018 12:01:41', '139.5.253.115', 1),
(201, '66', '29', '31-01-2018 18:09:36', '43.252.29.192', 1),
(202, '9', '38', '31-01-2018 18:26:14', '106.223.153.158', 1),
(203, '9', '14', '01-02-2018 10:43:18', '47.31.111.225', 1),
(204, '75', '17', '01-02-2018 13:30:48', '122.161.51.51', 1),
(205, '80', '35', '01-02-2018 14:40:11', '106.223.117.132', 1),
(206, '87', '5', '02-02-2018 15:35:30', '103.212.145.206', 2),
(207, '46', '000', '02-02-2018 15:54:18', '108.177.6.57', 2),
(208, '66', '000', '02-02-2018 15:56:23', '108.177.7.89', 7),
(209, '105', '000', '02-02-2018 15:57:24', '108.177.7.83', 7),
(210, '136', '30', '02-02-2018 16:50:48', '103.212.145.206', 31),
(211, '30', '30', '02-02-2018 17:41:17', '103.212.145.206', 2),
(212, '2', '30', '02-02-2018 17:41:26', '103.212.145.206', 1),
(213, '83', '34', '05-02-2018 16:29:41', '122.161.11.124', 1),
(214, '49', '34', '05-02-2018 16:29:56', '122.161.11.124', 1),
(215, '83', '2', '05-02-2018 16:41:04', '122.161.11.124', 1),
(216, '26', '41', '05-02-2018 19:43:19', '47.31.173.27', 2),
(217, '9', '41', '05-02-2018 19:43:25', '47.31.173.27', 2),
(218, '66', '41', '05-02-2018 19:56:30', '47.31.173.27', 1),
(219, '164', '30', '06-02-2018 14:51:52', '47.31.30.125', 2),
(220, '164', '5', '06-02-2018 15:02:57', '47.31.30.11', 1),
(221, '136', '5', '06-02-2018 15:26:56', '47.31.30.11', 1),
(222, '26', '5', '06-02-2018 15:29:18', '47.31.30.11', 1),
(223, '136', '6', '07-02-2018 11:26:30', '122.161.77.69', 3),
(224, '136', '000', '07-02-2018 11:34:42', '47.31.41.158', 7),
(225, '75', '6', '07-02-2018 12:40:36', '1.39.28.79', 2),
(226, '173', '6', '07-02-2018 12:43:01', '1.39.28.79', 1),
(227, '136', '20', '07-02-2018 14:58:04', '27.6.232.96', 2),
(228, '24', '17', '07-02-2018 16:17:55', '122.161.77.69', 2),
(229, '66', '4', '07-02-2018 16:51:09', '47.31.239.252', 1),
(230, '66', '5', '07-02-2018 16:51:20', '47.31.23.206', 1),
(231, '171', '2', '07-02-2018 18:05:37', '122.161.77.69', 5),
(232, '105', '2', '07-02-2018 18:06:15', '122.161.77.69', 8),
(233, '167', '2', '07-02-2018 18:06:30', '122.161.77.69', 5),
(234, '24', '2', '07-02-2018 18:07:07', '122.161.77.69', 1),
(235, '167', '17', '07-02-2018 18:16:42', '122.161.77.69', 8),
(236, '171', '35', '07-02-2018 18:16:49', '106.223.155.111', 1),
(237, '105', '35', '07-02-2018 18:17:01', '106.223.155.111', 1),
(238, '167', '35', '07-02-2018 18:17:25', '106.223.155.111', 1),
(239, '166', '17', '07-02-2018 18:17:52', '122.161.77.69', 3),
(240, '105', '17', '07-02-2018 18:21:30', '122.161.77.69', 2),
(241, '171', '23', '07-02-2018 23:40:37', '106.223.244.124', 2),
(242, '66', '23', '07-02-2018 23:40:37', '106.223.244.124', 1),
(243, '167', '000', '08-02-2018 10:55:35', '47.31.229.90', 2),
(244, '171', '30', '08-02-2018 12:31:30', '47.31.109.80', 3),
(245, '105', '42', '08-02-2018 14:10:28', '223.180.19.247', 12),
(246, '166', '30', '08-02-2018 14:27:14', '47.31.70.111', 2),
(247, '167', '30', '08-02-2018 14:27:17', '47.31.70.111', 2),
(248, '46', '30', '08-02-2018 14:27:20', '47.31.70.111', 1),
(249, '28', '30', '08-02-2018 14:27:26', '47.31.70.111', 1),
(250, '155', '000', '08-02-2018 15:04:06', '47.31.47.91', 20),
(251, '155', '6', '08-02-2018 15:16:35', '122.161.10.252', 1),
(252, '155', '2', '08-02-2018 15:20:57', '42.111.66.39', 11),
(253, '166', '2', '08-02-2018 15:31:57', '42.111.66.39', 4),
(254, '155', '17', '08-02-2018 15:32:16', '47.31.22.220', 7),
(255, '171', '20', '08-02-2018 15:58:13', '27.6.232.96', 2),
(256, '155', '20', '08-02-2018 15:59:58', '27.6.232.96', 2),
(257, '167', '3', '08-02-2018 16:00:50', '122.161.10.252', 1),
(258, '155', '3', '08-02-2018 16:01:23', '122.161.10.252', 1),
(259, '66', '3', '08-02-2018 16:02:19', '122.161.10.252', 1),
(260, '155', '23', '08-02-2018 16:13:22', '47.31.247.133', 1),
(261, '105', '23', '08-02-2018 16:14:11', '47.31.247.133', 3),
(262, '180', '000', '08-02-2018 17:02:28', '47.31.253.117', 12),
(263, '180', '2', '08-02-2018 17:14:36', '42.111.66.39', 4),
(264, '180', '17', '08-02-2018 17:45:45', '47.31.18.56', 2),
(265, '183', '17', '08-02-2018 17:53:46', '47.31.18.56', 1),
(266, '183', '30', '08-02-2018 18:06:20', '47.31.115.9', 3),
(267, '136', '17', '08-02-2018 18:44:22', '47.31.47.24', 1),
(268, '66', '2', '08-02-2018 18:46:35', '42.111.66.39', 1),
(269, '136', '25', '09-02-2018 10:50:18', '122.161.10.252', 8),
(270, '184', '000', '09-02-2018 11:34:55', '47.31.29.158', 1),
(271, '80', '26', '09-02-2018 13:02:52', '47.31.37.225', 2),
(272, '189', '6', '09-02-2018 13:42:48', '122.161.10.252', 2),
(273, '189', '000', '09-02-2018 13:54:12', '150.129.181.84', 4),
(274, '136', '23', '09-02-2018 14:43:05', '27.6.232.96', 1),
(275, '182', '2', '09-02-2018 15:15:38', '42.111.69.165', 2),
(276, '175', '000', '09-02-2018 15:29:31', '47.31.13.87', 1),
(277, '189', '17', '09-02-2018 16:41:29', '110.235.138.207', 1),
(278, '182', '17', '09-02-2018 16:42:49', '110.235.138.207', 2),
(279, '167', '42', '09-02-2018 17:46:12', '106.209.160.95', 1),
(280, '144', '000', '09-02-2018 17:55:57', '47.31.241.4', 16),
(281, '9', '44', '09-02-2018 18:09:52', '43.245.159.91', 4),
(282, '46', '44', '09-02-2018 18:11:12', '43.245.159.91', 1),
(283, '200', '000', '09-02-2018 18:39:02', '47.31.35.124', 8),
(284, '198', '2', '10-02-2018 11:50:30', '42.111.89.194', 6),
(285, '198', '30', '10-02-2018 13:31:02', '47.31.8.83', 1),
(286, '183', '2', '10-02-2018 14:09:06', '42.111.90.98', 1),
(287, '176', '2', '10-02-2018 14:47:41', '42.111.80.146', 1),
(288, '189', '44', '10-02-2018 16:48:29', '43.245.159.146', 3),
(289, '200', '44', '10-02-2018 16:52:44', '43.245.159.146', 1),
(290, '80', '44', '10-02-2018 16:56:36', '43.245.159.146', 1),
(291, '194', '2', '10-02-2018 17:24:05', '42.111.84.146', 1),
(292, '171', '44', '10-02-2018 17:54:26', '106.223.26.97', 1),
(293, '198', '6', '10-02-2018 18:28:31', '103.80.22.4', 9),
(294, '105', '45', '10-02-2018 19:16:04', '106.223.250.128', 2),
(295, '9', '45', '10-02-2018 19:21:48', '106.223.250.128', 1),
(296, '198', '42', '10-02-2018 20:52:09', '106.209.171.177', 2),
(297, '192', '44', '10-02-2018 22:27:51', '43.245.159.146', 1),
(298, '176', '44', '10-02-2018 22:29:29', '43.245.159.146', 1),
(299, '180', '47', '10-02-2018 23:34:00', '47.30.220.60', 1),
(300, '144', '47', '10-02-2018 23:34:49', '47.30.220.60', 1),
(301, '183', '47', '10-02-2018 23:35:45', '47.30.220.60', 1),
(302, '198', '000', '11-02-2018 11:03:12', '47.31.211.130', 6),
(303, '182', '27', '11-02-2018 11:50:52', '47.31.118.193', 1),
(304, '24', '6', '11-02-2018 13:04:18', '122.161.30.227', 3),
(305, '193', '2', '11-02-2018 14:39:07', '42.111.11.49', 1),
(306, '184', '2', '11-02-2018 14:42:32', '42.111.11.49', 1),
(307, '183', '6', '11-02-2018 14:56:14', '27.6.232.96', 1),
(308, '136', '33', '11-02-2018 15:40:10', '103.212.156.56', 1),
(309, '198', '33', '11-02-2018 15:40:44', '103.212.156.56', 1),
(310, '82', '6', '12-02-2018 12:21:44', '1.39.28.87', 1),
(311, '167', '6', '12-02-2018 12:22:13', '1.39.28.87', 1),
(312, '167', '23', '12-02-2018 12:25:14', '27.6.232.96', 2),
(313, '193', '23', '12-02-2018 12:26:08', '27.6.232.96', 1),
(314, '180', '23', '12-02-2018 12:26:21', '27.6.232.96', 1),
(315, '39', '42', '12-02-2018 12:53:38', '106.209.163.136', 6),
(316, '154', '000', '12-02-2018 14:03:20', '47.31.30.12', 7),
(317, '154', '17', '12-02-2018 14:19:54', '47.31.24.57', 2),
(318, '189', '29', '12-02-2018 14:56:54', '106.223.44.4', 1),
(319, '198', '29', '12-02-2018 14:57:30', '106.223.44.4', 3),
(320, '193', '29', '12-02-2018 14:58:01', '106.223.44.4', 1),
(321, '194', '29', '12-02-2018 14:58:50', '106.223.44.4', 1),
(322, '154', '2', '12-02-2018 15:00:48', '42.111.14.26', 3),
(323, '144', '2', '12-02-2018 15:01:21', '42.111.14.26', 1),
(324, '193', '17', '12-02-2018 15:40:43', '47.31.49.48', 1),
(325, '182', '48', '12-02-2018 15:59:46', '106.202.143.224', 1),
(326, '9', '42', '12-02-2018 16:33:05', '27.6.232.96', 1),
(327, '152', '000', '12-02-2018 16:46:28', '47.31.224.39', 2),
(328, '154', '6', '12-02-2018 16:46:37', '1.39.28.87', 3),
(329, '180', '20', '12-02-2018 16:48:30', '27.6.232.96', 2),
(330, '80', '42', '12-02-2018 16:48:34', '27.6.232.96', 3),
(331, '183', '25', '12-02-2018 17:03:28', '122.161.30.227', 5),
(332, '198', '25', '12-02-2018 17:03:42', '122.161.30.227', 5),
(333, '24', '42', '12-02-2018 17:04:27', '27.6.232.96', 7),
(334, '213', '23', '12-02-2018 17:13:13', '47.31.130.177', 8),
(335, '105', '25', '12-02-2018 17:22:53', '122.161.30.227', 6),
(336, '213', '6', '12-02-2018 18:51:22', '1.39.28.15', 1),
(337, '152', '17', '12-02-2018 19:20:59', '47.31.35.242', 1),
(338, '154', '51', '12-02-2018 20:10:05', '103.211.12.140', 1),
(339, '105', '51', '12-02-2018 20:10:52', '103.211.12.140', 1),
(340, '167', '51', '12-02-2018 20:11:17', '103.211.12.140', 1),
(341, '144', '29', '12-02-2018 20:29:46', '47.31.114.104', 1),
(342, '176', '48', '12-02-2018 20:30:36', '106.222.188.143', 1),
(343, '105', '29', '13-02-2018 00:08:50', '27.6.232.96', 2),
(344, '118', '6', '13-02-2018 00:14:45', '27.6.232.96', 1),
(345, '183', '29', '13-02-2018 00:46:23', '27.6.232.96', 1),
(346, '39', '29', '13-02-2018 00:47:06', '27.6.232.96', 1),
(347, '46', '6', '13-02-2018 00:51:12', '27.6.232.96', 3),
(348, '182', '6', '13-02-2018 00:52:27', '27.6.232.96', 1),
(349, '182', '29', '13-02-2018 00:53:48', '27.6.232.96', 1),
(350, '155', '4', '13-02-2018 12:26:53', '47.30.111.75', 1),
(351, '198', '23', '13-02-2018 13:12:57', '47.31.117.213', 9),
(352, '138', '23', '13-02-2018 15:27:43', '47.31.120.251', 1),
(353, '87', '6', '13-02-2018 16:42:39', '1.39.28.151', 2),
(354, '225', '000', '13-02-2018 17:04:58', '47.31.204.122', 4),
(355, '228', '000', '13-02-2018 18:58:16', '47.31.140.245', 2),
(356, '232', '000', '13-02-2018 19:56:08', '47.31.227.44', 2),
(357, '138', '000', '13-02-2018 21:46:08', '47.31.56.169', 1),
(358, '230', '000', '14-02-2018 14:22:57', '47.31.175.226', 1),
(359, '188', '000', '14-02-2018 15:13:09', '47.31.128.60', 1),
(360, '176', '000', '14-02-2018 15:15:24', '47.31.128.60', 1),
(361, '236', '6', '14-02-2018 15:32:42', '1.39.28.100', 1),
(362, '219', '2', '14-02-2018 15:56:26', '42.111.17.38', 2),
(363, '188', '2', '14-02-2018 15:58:51', '42.111.89.173', 1),
(364, '213', '25', '14-02-2018 16:13:57', '122.161.76.233', 19),
(365, '176', '25', '14-02-2018 16:18:26', '122.161.76.233', 7),
(366, '176', '42', '14-02-2018 16:25:23', '27.6.232.96', 19),
(367, '188', '17', '14-02-2018 16:27:03', '47.31.115.39', 1),
(368, '230', '17', '14-02-2018 16:27:54', '47.31.115.39', 2),
(369, '176', '17', '14-02-2018 16:32:51', '47.31.115.39', 1),
(370, '198', '17', '14-02-2018 16:34:35', '47.31.115.39', 1),
(371, '192', '2', '14-02-2018 16:53:08', '42.111.75.68', 1),
(372, '228', '2', '14-02-2018 16:53:24', '42.111.75.68', 3),
(373, '144', '6', '14-02-2018 17:47:31', '1.39.28.100', 1),
(374, '174', '2', '14-02-2018 17:55:02', '42.111.10.81', 1),
(375, '105', '57', '14-02-2018 18:53:08', '103.25.92.43', 1),
(376, '80', '57', '14-02-2018 18:53:34', '103.25.92.43', 1),
(377, '232', '25', '15-02-2018 16:31:09', '122.161.252.8', 1),
(378, '225', '25', '16-02-2018 10:52:01', '122.161.1.103', 1),
(379, '167', '25', '16-02-2018 11:39:34', '122.161.156.150', 1),
(380, '87', '42', '16-02-2018 13:00:12', '27.6.232.96', 4),
(381, '238', '42', '16-02-2018 13:02:52', '27.6.232.96', 2),
(382, '194', '42', '16-02-2018 13:03:43', '27.6.232.96', 1),
(383, '180', '42', '16-02-2018 13:08:57', '27.6.232.96', 2),
(384, '182', '42', '16-02-2018 13:10:16', '27.6.232.96', 1),
(385, '138', '42', '16-02-2018 13:12:26', '27.6.232.96', 2),
(386, '66', '42', '16-02-2018 13:13:49', '27.6.232.96', 2),
(387, '144', '42', '16-02-2018 13:19:23', '27.6.232.96', 1),
(388, '154', '42', '16-02-2018 13:21:25', '27.6.232.96', 1),
(389, '213', '42', '20-02-2018 13:53:43', '27.6.232.96', 7),
(390, '237', '42', '20-02-2018 14:16:17', '27.6.232.96', 7),
(391, '244', '42', '20-02-2018 18:28:51', '106.209.247.180', 4),
(392, '184', '25', '22-02-2018 16:56:42', '122.161.61.85', 1),
(393, '75', '42', '24-02-2018 15:59:23', '103.212.146.221', 2),
(394, '184', '42', '26-02-2018 00:27:11', '223.180.9.123', 1),
(395, '219', '42', '27-02-2018 11:55:46', '223.180.2.212', 1),
(396, '188', '42', '27-02-2018 11:56:02', '223.180.2.212', 1),
(397, '200', '42', '27-02-2018 11:56:08', '223.180.2.212', 1),
(398, '189', '42', '28-02-2018 11:20:40', '106.209.243.123', 1),
(399, '238', '25', '06-03-2018 11:50:21', '122.161.17.197', 4),
(400, '228', '25', '06-03-2018 11:51:40', '122.161.17.197', 1),
(401, '171', '25', '07-03-2018 16:04:40', '106.205.103.214', 1),
(402, '248', '25', '10-03-2018 15:00:15', '122.161.155.181', 1),
(403, '241', '42', '12-03-2018 12:56:16', '122.161.155.181', 1),
(404, '27', '20', '12-03-2018 15:09:44', '103.212.146.121', 2),
(405, '29', '20', '12-03-2018 15:15:14', '103.212.146.121', 2),
(406, '138', '25', '14-03-2018 12:05:45', '1.39.28.54', 1),
(407, '192', '25', '14-03-2018 12:06:02', '1.39.28.54', 3),
(408, '176', '85', '14-03-2018 12:19:45', '122.161.12.75', 2),
(409, '238', '85', '14-03-2018 12:20:14', '122.161.12.75', 1),
(410, '237', '85', '14-03-2018 12:20:19', '122.161.12.75', 1),
(411, '194', '85', '14-03-2018 12:20:45', '122.161.12.75', 2),
(412, '87', '85', '14-03-2018 12:44:11', '122.161.12.75', 2),
(413, '171', '85', '14-03-2018 12:46:36', '122.161.12.75', 1),
(414, '234', '25', '14-03-2018 12:47:46', '1.39.28.54', 1),
(415, '236', '25', '14-03-2018 12:47:56', '1.39.28.54', 1),
(416, '241', '85', '14-03-2018 12:48:57', '122.161.12.75', 3),
(417, '189', '25', '14-03-2018 12:49:49', '1.39.28.54', 1),
(418, '230', '25', '14-03-2018 12:49:57', '1.39.28.54', 1),
(419, '30', '25', '14-03-2018 12:50:02', '1.39.28.54', 1),
(420, '66', '25', '14-03-2018 12:51:54', '1.39.28.54', 1),
(421, '188', '85', '14-03-2018 12:52:11', '106.209.195.241', 1),
(422, '144', '85', '14-03-2018 12:53:14', '106.209.195.241', 1),
(423, '105', '20', '14-03-2018 14:46:07', '103.212.144.191', 1),
(424, '66', '20', '14-03-2018 14:46:29', '103.212.144.191', 1),
(425, '184', '85', '14-03-2018 17:08:27', '223.180.17.110', 1),
(426, '194', '6', '14-03-2018 18:24:43', '103.212.146.219', 1),
(427, '213', '124', '15-03-2018 14:16:37', '122.161.159.74', 7),
(428, '238', '124', '15-03-2018 14:16:48', '122.161.159.74', 6),
(429, '105', '124', '15-03-2018 14:17:01', '122.161.159.74', 5),
(430, '219', '124', '15-03-2018 14:22:05', '122.161.159.74', 4),
(431, '228', '124', '15-03-2018 14:28:47', '122.161.159.74', 2),
(432, '87', '124', '15-03-2018 14:46:03', '122.161.159.74', 7),
(433, '154', '124', '15-03-2018 14:46:55', '122.161.159.74', 3),
(434, '144', '124', '15-03-2018 14:47:06', '122.161.159.74', 4),
(435, '171', '124', '15-03-2018 14:47:13', '122.161.159.74', 2),
(436, '184', '124', '15-03-2018 14:49:01', '122.161.159.74', 3),
(437, '193', '124', '15-03-2018 14:58:21', '122.161.159.74', 2),
(438, '194', '124', '15-03-2018 15:17:17', '122.161.159.74', 1),
(439, '24', '124', '15-03-2018 15:17:43', '122.161.159.74', 1),
(440, '241', '124', '15-03-2018 15:46:01', '106.209.238.149', 17),
(441, '198', '124', '15-03-2018 16:00:23', '106.209.238.149', 1),
(442, '176', '124', '15-03-2018 17:04:30', '223.180.26.213', 3),
(443, '236', '124', '16-03-2018 11:05:43', '122.161.152.88', 1),
(444, '234', '124', '16-03-2018 11:05:46', '122.161.152.88', 1),
(445, '248', '124', '16-03-2018 11:05:49', '122.161.152.88', 1),
(446, '75', '124', '16-03-2018 11:38:37', '122.161.152.88', 1),
(447, '188', '124', '16-03-2018 11:52:54', '122.161.152.88', 2),
(448, '249', '124', '16-03-2018 16:11:42', '122.161.152.88', 2),
(449, '250', '124', '16-03-2018 17:03:47', '122.161.152.88', 1),
(450, '138', '6', '18-03-2018 15:40:28', '1.39.28.67', 1),
(451, '225', '20', '19-03-2018 17:43:17', '103.212.146.219', 2),
(452, '144', '20', '19-03-2018 17:44:11', '103.212.146.219', 1),
(453, '304', '20', '19-03-2018 17:44:17', '103.212.146.219', 4),
(454, '286', '20', '19-03-2018 17:45:38', '103.212.146.219', 5),
(455, '200', '20', '19-03-2018 17:46:18', '103.212.146.219', 1),
(456, '258', '20', '19-03-2018 17:46:48', '103.212.146.219', 2),
(457, '285', '20', '19-03-2018 17:47:09', '103.212.146.219', 3),
(458, '289', '20', '19-03-2018 17:47:58', '103.212.146.219', 1),
(459, '154', '20', '19-03-2018 17:48:02', '103.212.146.219', 3),
(460, '250', '20', '19-03-2018 17:48:17', '103.212.146.219', 1),
(461, '225', '6', '19-03-2018 23:39:44', '1.39.28.34', 1),
(462, '198', '125', '04-04-2018 18:02:36', '103.212.146.162', 1),
(463, '253', '125', '05-04-2018 11:34:00', '103.212.146.162', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acadmic`
--
ALTER TABLE `acadmic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aim`
--
ALTER TABLE `aim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_like_dislike`
--
ALTER TABLE `all_like_dislike`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_notifications`
--
ALTER TABLE `all_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answerofquestion`
--
ALTER TABLE `answerofquestion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_class`
--
ALTER TABLE `app_class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_courses`
--
ALTER TABLE `app_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_subjects`
--
ALTER TABLE `app_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_topic`
--
ALTER TABLE `app_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment_mark`
--
ALTER TABLE `assignment_mark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendence`
--
ALTER TABLE `attendence`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `availability`
--
ALTER TABLE `availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `barcode`
--
ALTER TABLE `barcode`
  ADD PRIMARY KEY (`bid`),
  ADD UNIQUE KEY `barcode_id` (`barcode_id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_join`
--
ALTER TABLE `batch_join`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batch_query`
--
ALTER TABLE `batch_query`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booked_activity_rel`
--
ALTER TABLE `booked_activity_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarkteacher`
--
ALTER TABLE `bookmarkteacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_activity_demo`
--
ALTER TABLE `book_activity_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_batch_demo`
--
ALTER TABLE `book_batch_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_session_demo`
--
ALTER TABLE `book_session_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centers`
--
ALTER TABLE `centers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificates`
--
ALTER TABLE `certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_room`
--
ALTER TABLE `class_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_category`
--
ALTER TABLE `course_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_fee`
--
ALTER TABLE `course_fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datesheet`
--
ALTER TABLE `datesheet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `debug`
--
ALTER TABLE `debug`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `demoes`
--
ALTER TABLE `demoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquires_status`
--
ALTER TABLE `enquires_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expanses`
--
ALTER TABLE `expanses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expanses_category`
--
ALTER TABLE `expanses_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `explore_banner`
--
ALTER TABLE `explore_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `explore_banner_video`
--
ALTER TABLE `explore_banner_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_receipt`
--
ALTER TABLE `faculty_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_management`
--
ALTER TABLE `fee_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute_with_address`
--
ALTER TABLE `institute_with_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inst_admins`
--
ALTER TABLE `inst_admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `inst_program`
--
ALTER TABLE `inst_program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inst_teacher`
--
ALTER TABLE `inst_teacher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interns_fileupload`
--
ALTER TABLE `interns_fileupload`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intern_register`
--
ALTER TABLE `intern_register`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_code` (`user_code`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_no` (`mobile_no`);

--
-- Indexes for table `intro_request`
--
ALTER TABLE `intro_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_classes`
--
ALTER TABLE `ms_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_subjects`
--
ALTER TABLE `ms_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ms_topics`
--
ALTER TABLE `ms_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_question`
--
ALTER TABLE `multiple_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `myplans`
--
ALTER TABLE `myplans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `na_gallery`
--
ALTER TABLE `na_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `non_academic_coach`
--
ALTER TABLE `non_academic_coach`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otp`
--
ALTER TABLE `otp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passion_for_life`
--
ALTER TABLE `passion_for_life`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_quiz`
--
ALTER TABLE `question_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipts`
--
ALTER TABLE `receipts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal`
--
ALTER TABLE `referal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referals`
--
ALTER TABLE `referals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reffer_code`
--
ALTER TABLE `reffer_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rel_student_batch`
--
ALTER TABLE `rel_student_batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session_slot`
--
ALTER TABLE `session_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_setting`
--
ALTER TABLE `site_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slot`
--
ALTER TABLE `slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `star_achievers`
--
ALTER TABLE `star_achievers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `std_sample_papers`
--
ALTER TABLE `std_sample_papers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `story`
--
ALTER TABLE `story`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `referral_code` (`referral_code`);

--
-- Indexes for table `student_assigment`
--
ALTER TABLE `student_assigment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_batch`
--
ALTER TABLE `student_batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_contribution`
--
ALTER TABLE `student_contribution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_event_rel`
--
ALTER TABLE `student_event_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_package_info`
--
ALTER TABLE `student_package_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_search_query_pending`
--
ALTER TABLE `student_search_query_pending`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_subject_rel`
--
ALTER TABLE `student_subject_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_material`
--
ALTER TABLE `study_material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachernotification`
--
ALTER TABLE `teachernotification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacherrating`
--
ALTER TABLE `teacherrating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacherrating_demo`
--
ALTER TABLE `teacherrating_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacherrating_demo_nonaca`
--
ALTER TABLE `teacherrating_demo_nonaca`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `referral_code` (`referral_code`);

--
-- Indexes for table `teachers_assigment`
--
ALTER TABLE `teachers_assigment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_events`
--
ALTER TABLE `teachers_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers_notes`
--
ALTER TABLE `teachers_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_activity_batch`
--
ALTER TABLE `teacher_activity_batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_activity_details`
--
ALTER TABLE `teacher_activity_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_activity_plans`
--
ALTER TABLE `teacher_activity_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_certificates`
--
ALTER TABLE `teacher_certificates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_degree_details`
--
ALTER TABLE `teacher_degree_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_educational_details`
--
ALTER TABLE `teacher_educational_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_expert_topic`
--
ALTER TABLE `teacher_expert_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_feesinstallment`
--
ALTER TABLE `teacher_feesinstallment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_follow`
--
ALTER TABLE `teacher_follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_login_history`
--
ALTER TABLE `teacher_login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_manual_markachiever`
--
ALTER TABLE `teacher_manual_markachiever`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_markachiever`
--
ALTER TABLE `teacher_markachiever`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_myplans_rel`
--
ALTER TABLE `teacher_myplans_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_offer`
--
ALTER TABLE `teacher_offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_package_info`
--
ALTER TABLE `teacher_package_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_personal_details`
--
ALTER TABLE `teacher_personal_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_professional_details`
--
ALTER TABLE `teacher_professional_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_qualification`
--
ALTER TABLE `teacher_qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_question`
--
ALTER TABLE `teacher_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_question_sub`
--
ALTER TABLE `teacher_question_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_slot`
--
ALTER TABLE `teacher_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_slot_booked`
--
ALTER TABLE `teacher_slot_booked`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_slot_review`
--
ALTER TABLE `teacher_slot_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_student_rel`
--
ALTER TABLE `teacher_student_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_sub_topic`
--
ALTER TABLE `teacher_sub_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_teaching_details`
--
ALTER TABLE `teacher_teaching_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_technical_details`
--
ALTER TABLE `teacher_technical_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_visitors`
--
ALTER TABLE `teacher_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teasurebox`
--
ALTER TABLE `teasurebox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teasurebox_std`
--
ALTER TABLE `teasurebox_std`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_prep`
--
ALTER TABLE `test_prep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `university`
--
ALTER TABLE `university`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visits`
--
ALTER TABLE `visits`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acadmic`
--
ALTER TABLE `acadmic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `aim`
--
ALTER TABLE `aim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `all_like_dislike`
--
ALTER TABLE `all_like_dislike`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `all_notifications`
--
ALTER TABLE `all_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;

--
-- AUTO_INCREMENT for table `answerofquestion`
--
ALTER TABLE `answerofquestion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `app_class`
--
ALTER TABLE `app_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=265;

--
-- AUTO_INCREMENT for table `app_courses`
--
ALTER TABLE `app_courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `app_subjects`
--
ALTER TABLE `app_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1896;

--
-- AUTO_INCREMENT for table `app_topic`
--
ALTER TABLE `app_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `assignment_mark`
--
ALTER TABLE `assignment_mark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendence`
--
ALTER TABLE `attendence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `availability`
--
ALTER TABLE `availability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `barcode`
--
ALTER TABLE `barcode`
  MODIFY `bid` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `batch_join`
--
ALTER TABLE `batch_join`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `batch_query`
--
ALTER TABLE `batch_query`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booked_activity_rel`
--
ALTER TABLE `booked_activity_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `bookmarkteacher`
--
ALTER TABLE `bookmarkteacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_activity_demo`
--
ALTER TABLE `book_activity_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `book_batch_demo`
--
ALTER TABLE `book_batch_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `book_session_demo`
--
ALTER TABLE `book_session_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `centers`
--
ALTER TABLE `centers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `certificates`
--
ALTER TABLE `certificates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `class_room`
--
ALTER TABLE `class_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course_category`
--
ALTER TABLE `course_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `course_fee`
--
ALTER TABLE `course_fee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `datesheet`
--
ALTER TABLE `datesheet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `debug`
--
ALTER TABLE `debug`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `demoes`
--
ALTER TABLE `demoes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquires_status`
--
ALTER TABLE `enquires_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expanses`
--
ALTER TABLE `expanses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expanses_category`
--
ALTER TABLE `expanses_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `explore_banner`
--
ALTER TABLE `explore_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `explore_banner_video`
--
ALTER TABLE `explore_banner_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `faculty_receipt`
--
ALTER TABLE `faculty_receipt`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_management`
--
ALTER TABLE `fee_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `inst_admins`
--
ALTER TABLE `inst_admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `inst_program`
--
ALTER TABLE `inst_program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inst_teacher`
--
ALTER TABLE `inst_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `interns_fileupload`
--
ALTER TABLE `interns_fileupload`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `intern_register`
--
ALTER TABLE `intern_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `intro_request`
--
ALTER TABLE `intro_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ms_classes`
--
ALTER TABLE `ms_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ms_subjects`
--
ALTER TABLE `ms_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=491;

--
-- AUTO_INCREMENT for table `ms_topics`
--
ALTER TABLE `ms_topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=359;

--
-- AUTO_INCREMENT for table `multiple_question`
--
ALTER TABLE `multiple_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `myplans`
--
ALTER TABLE `myplans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `na_gallery`
--
ALTER TABLE `na_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `non_academic_coach`
--
ALTER TABLE `non_academic_coach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `otp`
--
ALTER TABLE `otp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=315;

--
-- AUTO_INCREMENT for table `passion_for_life`
--
ALTER TABLE `passion_for_life`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_quiz`
--
ALTER TABLE `question_quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `receipts`
--
ALTER TABLE `receipts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referal`
--
ALTER TABLE `referal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `referals`
--
ALTER TABLE `referals`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reffer_code`
--
ALTER TABLE `reffer_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rel_student_batch`
--
ALTER TABLE `rel_student_batch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `session_slot`
--
ALTER TABLE `session_slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `site_setting`
--
ALTER TABLE `site_setting`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slot`
--
ALTER TABLE `slot`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `star_achievers`
--
ALTER TABLE `star_achievers`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `std_sample_papers`
--
ALTER TABLE `std_sample_papers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `story`
--
ALTER TABLE `story`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `student_assigment`
--
ALTER TABLE `student_assigment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_batch`
--
ALTER TABLE `student_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_contribution`
--
ALTER TABLE `student_contribution`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_event_rel`
--
ALTER TABLE `student_event_rel`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `student_package_info`
--
ALTER TABLE `student_package_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_search_query_pending`
--
ALTER TABLE `student_search_query_pending`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `student_subject_rel`
--
ALTER TABLE `student_subject_rel`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `study_material`
--
ALTER TABLE `study_material`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teachernotification`
--
ALTER TABLE `teachernotification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `teacherrating`
--
ALTER TABLE `teacherrating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `teacherrating_demo`
--
ALTER TABLE `teacherrating_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `teacherrating_demo_nonaca`
--
ALTER TABLE `teacherrating_demo_nonaca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT for table `teachers_assigment`
--
ALTER TABLE `teachers_assigment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `teachers_events`
--
ALTER TABLE `teachers_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `teachers_notes`
--
ALTER TABLE `teachers_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `teacher_activity_batch`
--
ALTER TABLE `teacher_activity_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `teacher_activity_details`
--
ALTER TABLE `teacher_activity_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `teacher_activity_plans`
--
ALTER TABLE `teacher_activity_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `teacher_certificates`
--
ALTER TABLE `teacher_certificates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_degree_details`
--
ALTER TABLE `teacher_degree_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `teacher_educational_details`
--
ALTER TABLE `teacher_educational_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_expert_topic`
--
ALTER TABLE `teacher_expert_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_feesinstallment`
--
ALTER TABLE `teacher_feesinstallment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_follow`
--
ALTER TABLE `teacher_follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_login_history`
--
ALTER TABLE `teacher_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_manual_markachiever`
--
ALTER TABLE `teacher_manual_markachiever`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `teacher_markachiever`
--
ALTER TABLE `teacher_markachiever`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `teacher_myplans_rel`
--
ALTER TABLE `teacher_myplans_rel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_offer`
--
ALTER TABLE `teacher_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teacher_package_info`
--
ALTER TABLE `teacher_package_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_personal_details`
--
ALTER TABLE `teacher_personal_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teacher_professional_details`
--
ALTER TABLE `teacher_professional_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2002;

--
-- AUTO_INCREMENT for table `teacher_qualification`
--
ALTER TABLE `teacher_qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_question`
--
ALTER TABLE `teacher_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT for table `teacher_question_sub`
--
ALTER TABLE `teacher_question_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `teacher_slot`
--
ALTER TABLE `teacher_slot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=211;

--
-- AUTO_INCREMENT for table `teacher_slot_booked`
--
ALTER TABLE `teacher_slot_booked`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `teacher_slot_review`
--
ALTER TABLE `teacher_slot_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `teacher_student_rel`
--
ALTER TABLE `teacher_student_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `teacher_subject`
--
ALTER TABLE `teacher_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_sub_topic`
--
ALTER TABLE `teacher_sub_topic`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teacher_teaching_details`
--
ALTER TABLE `teacher_teaching_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `teacher_technical_details`
--
ALTER TABLE `teacher_technical_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `teacher_visitors`
--
ALTER TABLE `teacher_visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teasurebox`
--
ALTER TABLE `teasurebox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `teasurebox_std`
--
ALTER TABLE `teasurebox_std`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `test_prep`
--
ALTER TABLE `test_prep`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `university`
--
ALTER TABLE `university`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visits`
--
ALTER TABLE `visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=464;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

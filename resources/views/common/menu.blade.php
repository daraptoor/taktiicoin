
<ul class="nav" style="overflow-y: scroll;
    height: 100%;">
    <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
        <a href="{{ URL::to('/dashboard')}}">
            <i class="material-icons">dashboard</i>
            <p>Dashboard</p>
        </a>
    </li>
    @if(Session::has('user_type'))
        <?php $user_type = Session::get('user_type'); ?>
    @endif
    @if($user_role == 1 && $user_type == 'A')
    <!-- menu for Admin -->
    <!-- <li class="{{ Request::is('admin/list','admin/add_admin') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsAdmin">
        <i class="material-icons">account_balance</i>
            <p>Manage Institute
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('admin/list','admin/add_admin') ? 'in' : '' }}" id="componentsAdmin">
            <ul class="nav">
                <li class="{{ Request::is('admin/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/admin/list')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">Institute List</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/add_admin') ? 'active' : '' }}">
                    <a href="{{ URL::to('/admin/add_admin')}}">
                        <span class="sidebar-mini">AA</span>
                        <span class="sidebar-normal">Add Institute </span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li> -->
    @endif
    @if( $user_role == 1 && $user_type == 'A')
    {{--  program menu  --}}
    <li class="{{ Request::is('program/list','program/add') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsProgram">
        <i class="material-icons">event_seat</i>
            <p>Manage Program
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('program/list','program/add') ? 'in' : '' }}" id="componentsProgram">
            <ul class="nav">
                <li class="{{ Request::is('program/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/program/list')}}">
                        <span class="sidebar-mini">PL</span>
                        <span class="sidebar-normal">Program List</span>
                    </a>
                </li>
                <li class="{{ Request::is('program/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/program/add')}}">
                        <span class="sidebar-mini">PA</span>
                        <span class="sidebar-normal">Add Program </span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>

    {{--  teacher menu  --}}
    <li class="{{ Request::is('teacher/list','teacher/add') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsTeacher">
        <i class="material-icons">perm_identity</i>
            <p>Manage Teacher
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teacher/list','teacher/add') ? 'in' : '' }}" id="componentsTeacher">
            <ul class="nav">
                <li class="{{ Request::is('teacher/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teacher/list')}}">
                        <span class="sidebar-mini">TL</span>
                        <span class="sidebar-normal">Teacher List</span>
                    </a>
                </li>
                <li class="{{ Request::is('teacher/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teacher/add')}}">
                        <span class="sidebar-mini">TA</span>
                        <span class="sidebar-normal">Add Teacher </span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li>


      {{--  Batches menu  --}}
    <li class="{{ Request::is('batch/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsBatches">
        <i class="material-icons">weekend</i>
            <p>Manage Batches
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('batch/list','batch/add') ? 'in' : '' }}" id="componentsBatches">
            <ul class="nav">
                <li class="{{ Request::is('batch/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/batch/list')}}">
                        <span class="sidebar-mini">BL</span>
                        <span class="sidebar-normal">Batches List</span>
                    </a>
                </li>
                <li class="{{ Request::is('batch/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/batch/add')}}">
                        <span class="sidebar-mini">BA</span>
                        <span class="sidebar-normal">Add Batch</span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li>


    

    {{--  student menu  --}}
    <li class="{{ Request::is('student/list','student/add','student/assign') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsStudents">
        <i class="material-icons">recent_actors</i>
            <p>Manage Students
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('student/list','student/add','student/assign') ? 'in' : '' }}" id="componentsStudents">
            <ul class="nav">
                <li class="{{ Request::is('student/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/student/list')}}">
                        <span class="sidebar-mini">TL</span>
                        <span class="sidebar-normal">Student List</span>
                    </a>
                </li>
                <li class="{{ Request::is('student/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/student/add')}}">
                        <span class="sidebar-mini">TA</span>
                        <span class="sidebar-normal">Add Student </span>
                    </a>
                </li>                  
                <li class="{{ Request::is('student/assign') ? 'active' : '' }}">
                    <a href="{{ URL::to('/student/assign')}}">
                        <span class="sidebar-mini">SA</span>
                        <span class="sidebar-normal">Assign Student </span>
                    </a>
                </li> 
            </ul>
        </div>
    </li>

      {{--  fee menu  --}}
    <li class="{{ Request::is('fee/batch','fee/student','fee/add') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsFees">
            <i class="material-icons">monetization_on</i>
            <p>Manage Fees
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('fee/batch','fee/student','fee/add') ? 'in' : '' }}" id="componentsFees">
            <ul class="nav">
                <li class="{{ Request::is('fee/batch') ? 'active' : '' }}">
                    <a href="{{ URL::to('/fee/batch')}}">
                        <span class="sidebar-mini">FB</span>
                        <span class="sidebar-normal">Fees Detail</span>
                    </a>
                </li>
                {{--  <li class="{{ Request::is('fee/student') ? 'active' : '' }}">
                    <a href="{{ URL::to('/fee/student')}}">
                        <span class="sidebar-mini">FS</span>
                        <span class="sidebar-normal">Fee by Student</span>
                    </a>
                </li>  --}}
                <li class="{{ Request::is('fee/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/fee/add')}}">
                        <span class="sidebar-mini">AF</span>
                        <span class="sidebar-normal">Add Fee</span>
                    </a>
                </li> 

                <li class="{{ Request::is('fee/payments') ? 'active' : '' }}">
                    <a href="{{ URL::to('/fee/payments')}}">
                        <span class="sidebar-mini">OL</span>
                        <span class="sidebar-normal">Online Payments</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

      {{--  parent menu  --}}
    <li class="{{ Request::is('parent/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsParents">
        <i class="material-icons">contacts</i>
            <p>Manage Parents
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('parent/list','student/add') ? 'in' : '' }}" id="componentsParents">
            <ul class="nav">
                <li class="{{ Request::is('parent/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/parent/list')}}">
                        <span class="sidebar-mini">PL</span>
                        <span class="sidebar-normal">Parents List</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

     {{--  Attendance menu  --}}
    <li class="{{ Request::is('attendance/list','attendance/student_mark') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsAttendance">
        <i class="material-icons">date_range</i>
            <p>Manage Attendance
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('attendance/list','attendance/student_mark') ? 'in' : '' }}" id="componentsAttendance">
            <ul class="nav">
                <li class="{{ Request::is('attendance/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/attendance/list')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">Attendance List</span>
                    </a>
                </li>
                <!-- <li class="{{ Request::is('attendance/student_mark') ? 'active' : '' }}">
                    <a href="{{ URL::to('/attendance/student_mark')}}">
                        <span class="sidebar-mini">MA</span>
                        <span class="sidebar-normal">Mark Attendance</span>
                    </a>
                </li> -->
                
            </ul>
        </div>
    </li>
  

    {{--  Demo menu  --}}
    <li class="{{ Request::is('demo/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsDemo">
        <i class="material-icons">weekend</i>
            <p>Manage Demo
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('demo/list','demo/report','demo/penddingreport') ? 'in' : '' }}" id="componentsDemo">
            <ul class="nav">
                <li class="{{ Request::is('demo/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/demo/list')}}">
                        <span class="sidebar-mini">DL</span>
                        <span class="sidebar-normal"> Demo List</span>
                    </a>
                </li>

                <li class="{{ Request::is('demo/report') ? 'active' : '' }}">
                    <a href="{{ URL::to('/demo/report')}}">
                        <span class="sidebar-mini">DR</span>
                        <span class="sidebar-normal"> Demo Approve Report</span>
                    </a>
                </li>

                 <li class="{{ Request::is('demo/penddingreport') ? 'active' : '' }}">
                    <a href="{{ URL::to('/demo/penddingreport')}}">
                        <span class="sidebar-mini">DR</span>
                        <span class="sidebar-normal"> Demo Pendding Report</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

      {{--  Bookmark menu  --}}
    <li class="{{ Request::is('bookmark/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsBookmark">
        <i class="material-icons">weekend</i>
            <p>Manage Bookmark
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('bookmark/list') ? 'in' : '' }}" id="componentsBookmark">
            <ul class="nav">
                <li class="{{ Request::is('bookmark/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/bookmark/list')}}">
                        <span class="sidebar-mini">BL</span>
                        <span class="sidebar-normal">Bookmark List</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

     {{--  Rating menu  --}}
    <li class="{{ Request::is('rating/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsRating">
        <i class="material-icons">weekend</i>
            <p>Manage rating
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('rating/list') ? 'in' : '' }}" id="componentsRating">
            <ul class="nav">
                <li class="{{ Request::is('rating/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/rating/list')}}">
                        <span class="sidebar-mini">RL</span>
                        <span class="sidebar-normal">Rating List</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

     {{--  WhiteBoard menu  --}}
    <li class="{{ Request::is('whiteboard/datesheetlist') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsWhiteboard">
        <i class="material-icons">weekend</i>
            <p>Manage Whiteboard
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('whiteboard/datesheetlist','whiteboard/assignmentlist','whiteboard/videoslist','whiteboard/notificationslist') ? 'in' : '' }}" id="componentsWhiteboard">
            <ul class="nav">
                <li class="{{ Request::is('whiteboard/datesheetlist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/whiteboard/datesheetlist')}}">
                        <span class="sidebar-mini">DL</span>
                        <span class="sidebar-normal"> Datesheet List</span>
                    </a>
                </li>

                <li class="{{ Request::is('whiteboard/assignmentlist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/whiteboard/assignmentlist')}}">
                        <span class="sidebar-mini">Al</span>
                        <span class="sidebar-normal">Assignment List</span>
                    </a>
                </li>

                 <li class="{{ Request::is('whiteboard/videoslist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/whiteboard/videoslist')}}">
                        <span class="sidebar-mini">VL</span>
                        <span class="sidebar-normal"> Videos List</span>
                    </a>
                </li>

                <li class="{{ Request::is('whiteboard/notificationslist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/whiteboard/notificationslist')}}">
                        <span class="sidebar-mini">NL</span>
                        <span class="sidebar-normal"> Notifications List</span>
                    </a>
                </li>

            </ul>
        </div>
    </li>

    
    {{--  Query menu  --}}
    <li class="{{ Request::is('query/list') ? 'active' : '' }}"> 
        <a data-toggle="collapse" href="#componentsQuery">
        <i class="material-icons">weekend</i>
            <p>Manage Query
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('query/list','query/add') ? 'in' : '' }}" id="componentsQuery">
            <ul class="nav">
                <li class="{{ Request::is('query/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/query/list')}}">
                        <span class="sidebar-mini">QL</span>
                        <span class="sidebar-normal"> Query List</span>
                    </a>
                </li>
            </ul>
        </div>
    </li>

  
  
   
    {{--  Treasure Box menu  --}}
    <li class="{{ Request::is('treasure/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsTreasure">
        <i class="material-icons">card_giftcard</i>
            <p>Manage Treasure Box
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('treasure/list','treasure/add') ? 'in' : '' }}" id="componentsTreasure">
            <ul class="nav">
                <li class="{{ Request::is('treasure/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/treasure/list')}}">
                        <span class="sidebar-mini">TB</span>
                        <span class="sidebar-normal">Treasure Box</span>
                    </a>
                </li>

                <li class="{{ Request::is('treasure/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/treasure/add')}}">
                        <span class="sidebar-mini">TB</span>
                        <span class="sidebar-normal">Add</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>
 
    {{--  Assessments menu  --}}
    <!-- <li class="{{ Request::is('ques/listques','ques/addques') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsQuiz">
        <i class="material-icons">touch_app</i>
            <p>Assessment and Quiz
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('ques/listques','ques/addques') ? 'in' : '' }}" id="componentsQuiz">
            <ul class="nav">
                <li class="{{ Request::is('ques/listques') ? 'active' : '' }}">
                    <a href="{{ URL::to('/ques/listques')}}">
                        <span class="sidebar-mini">QL</span>
                        <span class="sidebar-normal">QuesList</span>
                    </a>
                </li>

                <li class="{{ Request::is('ques/addques') ? 'active' : '' }}">
                    <a href="{{ URL::to('/ques/addques')}}">
                        <span class="sidebar-mini">AQ</span>
                        <span class="sidebar-normal">Add Ques</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li> -->

     {{--  Promotion  Box menu  --}}
    <li class="{{ Request::is('promotion/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsPromotion">
        <i class="material-icons">card_giftcard</i>
            <p>Manage Promotion Box
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('promotion/list','promotion/add') ? 'in' : '' }}" id="componentsPromotion">
            <ul class="nav">
                <li class="{{ Request::is('promotion/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/promotion/list')}}">
                        <span class="sidebar-mini">PB</span>
                        <span class="sidebar-normal">Promotion Box</span>
                    </a>
                </li>

                <li class="{{ Request::is('promotion/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/promotion/add')}}">
                        <span class="sidebar-mini">PB</span>
                        <span class="sidebar-normal">Add</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>

      {{-- StarAchiever menu  --}}
    <li class="{{ Request::is('starachiever/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsStarachiever">
        <i class="material-icons">card_giftcard</i>
            <p>Manage StarAchiever
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('starachiever/list','starachiever/add') ? 'in' : '' }}" id="componentsStarachiever">
            <ul class="nav">
                <li class="{{ Request::is('starachiever/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/starachiever/list')}}">
                        <span class="sidebar-mini">SA</span>
                        <span class="sidebar-normal">Star Achiever</span>
                    </a>
                </li>

                <li class="{{ Request::is('starachiever/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/starachiever/add')}}">
                        <span class="sidebar-mini">AS</span>
                        <span class="sidebar-normal">Add Achiever</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>

     {{--  Calendar menu  --}}
    <li class="{{ Request::is('calendar/view') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsCalendar">
        <i class="material-icons">account_balance</i>
            <p>Calender
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('calendar/view') ? 'in' : '' }}" id="componentsCalendar">
            <ul class="nav">
                <li class="{{ Request::is('calendar/view') ? 'active' : '' }}">
                    <a href="{{ URL::to('calendar/view')}}">
                        <span class="sidebar-mini">C</span>
                        <span class="sidebar-normal">Calendar</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li>


    {{--  Reports menu  --}}
    <li class="{{ Request::is('reports/detail') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsReports">
        <i class="material-icons">report_problem</i>
            <p>Manage Reports
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('reports/detail') ? 'in' : '' }}" id="componentsReports">
            <ul class="nav">
                <li class="{{ Request::is('reports/detail') ? 'active' : '' }}">
                    <a href="{{ URL::to('/reports/detail')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">Detail</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li>
    <!-- menu for Admin END -->
    @endif
    @if( $user_role == 2 && $user_type == 'A')


    @endif
    @if( $user_role == '1'&& $user_type == 'T')
         <!-- below menu for teacher -->
    {{-- Teacher Batches menu  --}}
    <li class="{{ Request::is('teachers/batches/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsBatches">
        <i class="material-icons">weekend</i>
            <p>Manage Batches
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/batches/list','teachers/batches/add') ? 'in' : '' }}" id="componentsBatches">
            <ul class="nav">
                <li class="{{ Request::is('teachers/batches/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/batches/list')}}">
                        <span class="sidebar-mini">BL</span>
                        <span class="sidebar-normal">Batch's List</span>
                    </a>
                </li>
                <li class="{{ Request::is('teachers/batches/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/batches/add')}}">
                        <span class="sidebar-mini">BA</span>
                        <span class="sidebar-normal">Add Batch </span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li>


    {{--  Teacher joined Institite menu  --}}
    <li class="{{ Request::is('teachers/institute/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsInstitute">
        <i class="material-icons">account_balance</i>
            <p>Institutes
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/institute/list') ? 'in' : '' }}" id="componentsInstitute">
            <ul class="nav">
                <li class="{{ Request::is('teachers/institute/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/institute/list')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">List</span>
                    </a>
                </li>    
            </ul>
        </div>
    </li>

     {{--  Students joined Teacher menu  --}}
    <li class="{{ Request::is('teachers/student/join_student') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsStudent">
        <i class="material-icons">account_balance</i>
            <p>Join Students
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/student/join_student') ? 'in' : '' }}" id="componentsStudent">
            <ul class="nav">
                <li class="{{ Request::is('teachers/student/join_student') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/student/join_student')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">Student List</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li>

    {{--  Teacher Live menu  --}}
    <li class="{{ Request::is('teachers/live/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsLive">
        <i class="material-icons">videocam</i>
            <p>Live Session
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/live/list','teachers/live/add','teachers/live/addclass') ? 'in' : '' }}" id="componentsLive">
            <ul class="nav">
                <li class="{{ Request::is('teachers/live/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/live/list')}}">
                        <span class="sidebar-mini">LS</span>
                        <span class="sidebar-normal">Live Session's List</span>
                    </a>
                </li>
                <li class="{{ Request::is('teachers/live/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/live/add')}}">
                        <span class="sidebar-mini">LB</span>
                        <span class="sidebar-normal">Add Live Batch</span>
                    </a>
                </li>
                <li class="{{ Request::is('teachers/live/addclass') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/live/addclass')}}">
                        <span class="sidebar-mini">LC</span>
                        <span class="sidebar-normal">Add Live Class</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li>

    <!-- {{--  Teacher notifications menu  --}}
    <li class="{{ Request::is('teachers/notification/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsNotification">
        <i class="material-icons">notification_important</i>
            <p>Notification
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/notification/list') ? 'in' : '' }}" id="componentsNotification">
            <ul class="nav">
                <li class="{{ Request::is('teachers/notification/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/notification/list')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">List</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li> -->


     {{--  Teacher Calendar menu  --}}
    <li class="{{ Request::is('teachers/calendar/view') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsCalendar">
        <i class="material-icons">account_balance</i>
            <p>Calender
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/calendar/view') ? 'in' : '' }}" id="componentsCalendar">
            <ul class="nav">
                <li class="{{ Request::is('teachers/calendar/view') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/calendar/view')}}">
                        <span class="sidebar-mini">C</span>
                        <span class="sidebar-normal">Calendar</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
    </li>

    {{-- Teacher Question/Quiz menu  --}}
    <li class="{{ Request::is('teachers/question/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsQuestion">
        <i class="material-icons">weekend</i>
            <p>Assignment & Question
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/question/list','teachers/question/add') ? 'in' : '' }}" id="componentsQuestion">
            <ul class="nav">
                <li class="{{ Request::is('teachers/question/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/question/list')}}">
                        <span class="sidebar-mini">AL</span>
                        <span class="sidebar-normal">Assignment List</span>
                    </a>
                </li>
                <li class="{{ Request::is('teachers/question/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/question/add')}}">
                        <span class="sidebar-mini">QA</span>
                        <span class="sidebar-normal">Add Assignment</span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li>

    {{--  Treasure Box menu  in Dashteacher --}}
    <li class="{{ Request::is('teachers/treasure/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsTreasure">
        <i class="material-icons">card_giftcard</i>
            <p>Manage Treasure Box
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/treasure/list','teachers/treasure/add') ? 'in' : '' }}" id="componentsTreasure">
            <ul class="nav">
                <li class="{{ Request::is('teachers/treasure/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/treasure/list')}}">
                        <span class="sidebar-mini">TB</span>
                        <span class="sidebar-normal">TreasureBox List</span>
                    </a>
                </li>

                <li class="{{ Request::is('teachers/treasure/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/treasure/add')}}">
                        <span class="sidebar-mini">TB</span>
                        <span class="sidebar-normal">Add Treasure Box</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>

  {{--  fee menu  in Dashteacher --}}
    <li class="{{ Request::is('teachers/fee/list') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsFee">
        <i class="material-icons">card_giftcard</i>
            <p>Manage Fees
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('teachers/fee/list','teachers/fee/add') ? 'in' : '' }}" id="componentsFee">
            <ul class="nav">
                <li class="{{ Request::is('teachers/fee/list') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/fee/list')}}">
                        <span class="sidebar-mini">FL</span>
                        <span class="sidebar-normal">Fees List</span>
                    </a>
                </li>

                <li class="{{ Request::is('teachers/fee/add') ? 'active' : '' }}">
                    <a href="{{ URL::to('/teachers/fee/add')}}">
                        <span class="sidebar-mini">AF</span>
                        <span class="sidebar-normal">Add Fees</span>
                    </a>
                </li>
                
            </ul>
        </div>
    </li>

    

    

    <!-- menu for teacher END -->
    @elseif( $user_role == '2' && $user_type == 'T')

    
    @elseif( $user_role == '3' && $user_type == 'T')
        
    
    @else

  
   
    @endif
    @if( $user_type == 'S')
    <!-- below menu for Student -->
    {{-- Student Batches menu  --}}
    <li class="{{ Request::is('students/batches/batchlist') ? 'active' : '' }}">
        <a data-toggle="collapse" href="#componentsBatches">
        <i class="material-icons">weekend</i>
            <p>Manage Batches
                <b class="caret"></b>
            </p>
        </a>
        <div class="collapse {{ Request::is('students/batches/batchlist','students/batches/livebatchlist') ? 'in' : '' }}" id="componentsBatches">
            <ul class="nav">
                <li class="{{ Request::is('students/batches/batchlist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/students/batches/batchlist')}}">
                        <span class="sidebar-mini">BL</span>
                        <span class="sidebar-normal">All Live Batches</span>
                    </a>
                </li>
                <li class="{{ Request::is('students/batches/livebatchlist') ? 'active' : '' }}">
                    <a href="{{ URL::to('/students/batches/livebatchlist')}}">
                        <span class="sidebar-mini">LC</span>
                        <span class="sidebar-normal">Live Class</span>
                    </a>
                </li>                  
                
            </ul>
        </div>
    </li>



    <!-- menu for Student END -->
    @endif
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</ul>

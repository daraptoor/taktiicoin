<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ $title }} | CMS by TAKTii</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ url('/') }}/public/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="{{ url('/') }}/public/assets/css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--   Core JS Files   -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/public/assets/js/material.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/public/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{ url('/') }}/public/assets/js/arrive.min.js" type="text/javascript"></script>
    <!-- Forms Validations Plugin -->
    <script src="{{ url('/') }}/public/assets/js/jquery.validate.min.js"></script>
    <!-- Promise Library for SweetAlert2 working on IE -->
    <script src="{{ url('/') }}/public/assets/js/es6-promise-auto.min.js"></script>
    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="{{ url('/') }}/public/assets/js/moment.min.js"></script>
    <!--  Charts Plugin, full documentation here: https://gionkunz.github.io/chartist-js/ -->
    <script src="{{ url('/') }}/public/assets/js/chartist.min.js"></script>
    <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{ url('/') }}/public/assets/js/jquery.bootstrap-wizard.js"></script>
    <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
    <script src="{{ url('/') }}/public/assets/js/bootstrap-notify.js"></script>

    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{ url('/') }}/public/assets/js/bootstrap-datetimepicker.js"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{ url('/') }}/public/assets/js/jquery-jvectormap.js"></script>
    <!-- Sliders Plugin, full documentation here: https://refreshless.com/nouislider/ -->
    <script src="{{ url('/') }}/public/assets/js/nouislider.min.js"></script>
    <!--  Google Maps Plugin    -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAFPQibxeDaLIUHsC6_KqDdFaUdhrbhZ3M"></script> -->
    <!--  Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{ url('/') }}/public/assets/js/jquery.select-bootstrap.js"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
    <script src="{{ url('/') }}/public/assets/js/jquery.datatables.js"></script>
    <!-- Sweet Alert 2 plugin, full documentation here: https://limonte.github.io/sweetalert2/ -->
    <script src="{{ url('/') }}/public/assets/js/sweetalert2.js"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{ url('/') }}/public/assets/js/jasny-bootstrap.min.js"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{ url('/') }}/public/assets/js/fullcalendar.min.js"></script>
    <!-- Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{ url('/') }}/public/assets/js/jquery.tagsinput.js"></script>
    <script src="{{ url('/') }}/public/assets/js/material-dashboard.js?v=1.2.0"></script>
    <script src="{{ url('/') }}/public/assets/js/taktii.js"></script>
</head>
@if(Session::has('user_type'))
    <?php $user_type = Session::get('user_type'); ?>
@endif
<body>
    <div class="wrapper">
        @if($user_type == 'T')
        <div class="sidebar" data-active-color="blue" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
        @elseif($user_type == 'S')
        <div class="sidebar" data-active-color="rose" data-background-color="white" data-image="assets/img/sidebar-1.jpg">
        @else
        <div class="sidebar" data-active-color="green" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
        @endif
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
            <div class="logo">
                <a href="{{ URL::to('/dashboard')}}" class="simple-text logo-mini">
                @if($user_type == 'A')
                    <img src="{{ url('/') }}/{{ $inst_logo }}" alt="logo"  style="height:30px;width:30px;"/>
                @else
                    <img src="https://taktii.sgp1.digitaloceanspaces.com/static/30.png" alt="logo"  style="height:30px;width:30px;"/>
                @endif
                </a>
                <a href="{{ URL::to('/dashboard')}}" class="simple-text logo-normal">
                    {{ $inst_name }}
                    @if($user_type == 'S')
                        <span style="font-size:9px;vertical-align: middle;font-weight:700;">( Student )</span>
                    @elseif($user_type == 'T')
                        <span style="font-size:9px;vertical-align: middle;font-weight:700;">( Teacher )</span>
                    @else
                        
                    @endif
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <div class="photo">
                        <img src="{{ url('/') }}/public/assets/img/faces/avatar.jpg" class="hidden" />
                    </div>
                    <div class="info">
                        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                            <span>
                                {{ $inst_name }}
                                <b class="caret"></b>
                            </span>
                        </a>
                        <div class="clearfix"></div>
                        <div class="collapse" id="collapseExample">
                            <ul class="nav">
                                <li>
                                    <a href="{{ URL::to('/admin/details')}}">
                                        <span class="sidebar-mini"><i class="material-icons">person</i></span> 
                                        <span class="sidebar-normal">My Profile</span>
                                    </a>
                                </li>
                                @if($user_role == 1)
                                <!-- <li>
                                    <a href="#">
                                        <span class="sidebar-mini">PR</span>
                                        <span class="sidebar-normal">Permission</span>
                                    </a>
                                </li> -->
                                @endif                         
                            </ul>
                        </div>
                    </div>
                </div>
                @include('common.menu')
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" > {{ $title }} </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">domain</i>
                                    <!-- <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p> -->
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Institute 1</a>
                                    </li>
                                    <li>
                                        <a href="#">Institute 2</a>
                                    </li>
                                    <li>
                                        <a href="#">Institute 3</a>
                                    </li>
                                    <li>
                                        <a href="#">Institute 4</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                                
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{URL::to('/logout')}}">Logout</a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>

                    </div>
                </div>
            </nav>
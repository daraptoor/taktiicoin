
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Sno</b></th>
                                                    <th><b>Name</b></th>
                                                    <th><b>Course</b></th>
                                                    <th><b>Class</b></th>
                                                    <th><b>Subject</b></th>
                                                    <th><b>Duration</b></th>
                                                    <th><b>Fees</b></th>
                                                    <!-- <th><b>Status</b></th> -->
                                                    <th><b>Demo</b></th>
                                                    <th><b>View/Edit</b></th>
                                                    <th><b>Remove</b></th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($program_list as $plist)
                                                    <tr id="row_{{ $plist->id }}">
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $plist->prog_name }}</td>
                                                    <td>{{ $plist->course_name }}</td>
                                                    <td>{{ $plist->class_name }}</td>
                                                    <td>{{ $plist->subject_name }}</td>
                                                    <td>{{ $plist->duration }}</td>
                                                    <td>{{ $plist->fees }}</td>
                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $plist->status == 1)
                                                                <input type="checkbox" id="status_{{ $plist->id }}" onChange="change_status('{{ $plist->id }}','{{ base64_encode($plist->id) }}')" checked="checked">
                                                            @elseif($plist->status == 0)
                                                                <input type="checkbox" id="status_{{ $plist->id }}" onChange="change_status('{{ $plist->id }}','{{ base64_encode($plist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->
                                                    <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $plist->demo == 1)
                                                                <input type="checkbox" id="demo_{{ $plist->id }}" onChange="change_demo('{{ $plist->id }}','{{ base64_encode($plist->id) }}')" checked="checked">
                                                            @elseif($plist->demo == 0)
                                                                <input type="checkbox" id="demo_{{ $plist->id }}" onChange="change_demo('{{ $plist->id }}','{{ base64_encode($plist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/program/edit/{{base64_encode($plist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $plist->id }}','{{ base64_encode($plist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td>
                                                   
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_program_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Program status enabled Successfully', 500);
                 }else{
                     error('Unable to update Program status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_program_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Program status disabled Successfully', 500);
                 }else{
                     error('Unable to update Program status', 500);
                 }
             }
         })
    }    
}


function change_demo(id,data){
    if($("#demo_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_program_demo/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Program demo enabled Successfully', 500);
                 }else{
                     error('Unable to update Program demo', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_program_demo/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Program demo disabled Successfully', 500);
                 }else{
                     error('Unable to update Program demo', 500);
                 }
             }
         })
    }    
}

function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Program?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_program_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Program Deleted Successfully', 500);
                             var table = $('#tbleTeacherList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Program', 500);
                         }
                     }
                })
            }            
        })

}
</script>
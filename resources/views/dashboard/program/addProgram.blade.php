
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/program/add_new_program')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Program Info</h4>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course<span style="color: red;">* </span></label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="prog_courses" name="prog_courses" required>
                                                        @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_course_name" value="" id="prog_course_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class<span style="color: red;">* </span></label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="prog_class" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="prog_class_name" value="" id="prog_class_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject<span style="color: red;">* </span></label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="prog_subject" required  >
                                                        <option disabled > Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="prog_subject_name" value="" id="prog_subject_name">
                                                    <!-- <p><strong>Press Ctrl Key and select more than one subject from the list.</strong></p> -->
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Program Title<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" placeholder="eg: Title for your Program" name="prog_name" required>                                                    
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3">
                                                    <label for="examplePass" class="bmd-label-floating">Program Duration<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control" id="" name="prog_duration" required>  
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating"> Duration types<span style="color: red;">* </span></label>
                                                    <select name="prog_duration_2" id="" class="form-control" required>
                                                        <option value=" Day">Day</option>
                                                        <option value=" Month">Month</option>
                                                        <option value="Year">Year</option>                                                      
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Program Fees<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control" placeholder="eg: 2000" number="true"  id="" name="prog_fees" required="true" aria-required="true" aria-invalid="true">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Program Fees types <span style="color: red;">* </span></label>
                                                    <select name="prog_fees_2" id="" class="form-control" required>
                                                        <option value="Per Month">Per Month</option>
                                                        <option value="Complete Course">Complete Course</option>                                                      
                                                    </select>
                                                </div>
                                              

                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

function onCheck(name){
    if(name != ''){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_username/'+name,
            success:function(data){
                if(data > 0){
                    $('#class_username').addClass('has-error');
                    $('#submit_box').hide();

                    swal({
                    title: 'Oops...',
                    text: "Username already Exist. Please use diffrent username.",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ok'
                    }).then((result) => {
                        $("#exampleUsername").focus();
                    })
                  
                }else{
                    $('#class_username').removeClass('has-error');
                    $('#submit_box').show();
                }
            }
        })
    }
}

function getClassesFromCourse(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            loaderEnd();
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            loaderEnd();
            $('#setSubjects').html(data);
        }
    })
    var decodedString = atob(id);
    $('#prog_class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#prog_subject_name').val($('#sub_'+decodedString).data('name'));
}
// function set_Subjects(sel){
//     var opts = [],
//     opt;
//     var list = '';
//     var len = sel.options.length;
//     for (var i = 0; i < len; i++) {
//     opt = sel.options[i];

//         if (opt.selected) {
//             opts.push(opt.text+"|"+opt.value);
//             list = opts.join()
//             $('#prog_subject_name').val(list);
//         }
//     }

//     return opts;
// }
</script>
<!-- Modal -->
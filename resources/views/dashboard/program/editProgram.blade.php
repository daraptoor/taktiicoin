
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/program/update_program')}}">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Program Info</h4>
                                                    <input type="hidden" name="pro_id" value="{{ $prog_data->id }}">
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse2(this.value)" id="prog_courses" name="prog_courses" required>
                                                        @foreach($courses_list as $cl)
                                                            <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}"
                                                            @if( $prog_data->course_id == $cl->id )
                                                                selected
                                                            @endif
                                                            >{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_course_name" value="{{$prog_data->course_name}}" id="prog_course_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass2(this.value)" name="prog_class" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden"  name="prog_class_name" value="{{$prog_data->class_name}}" id="prog_class_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" >
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="prog_subject" >
                                                        <option disabled> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="prog_subject_name" value="{{$prog_data->course_name}}" id="prog_subject_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <label for="examplePass" class="bmd-label-floating">Program Title</label>
                                                    <input type="text" class="form-control" placeholder="eg: Title for your Program" name="prog_name" value="{{$prog_data->prog_name}}" required>                                                    
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Program Duration</label>
                                                    <input type="text" class="form-control" placeholder="eg: 6 Month" id="" name="prog_duration" value="{{$prog_data->duration}}" required>
                                                </div>
                                                <!-- <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Duration types</label>
                                                    <select name="prog_duration_2" id="" class="form-control" required>
                                                        <option value="Month"
                                                         @if( $prog_data->prog_duration_2   ==  " Month" )
                                                                selected
                                                            @endif> Month
                                                            </option>
                                                        <option value="Year" 
                                                        @if( $prog_data->prog_duration_2   ==  "Year" )
                                                                selected
                                                            @endif>Year</option>                                                      
                                                    </select>
                                                </div> -->
                                              


                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Program Fees</label>
                                                    <input type="number" class="form-control" placeholder="eg: 2000" number="true"  id="" name="prog_fees" value="{{$prog_data->fees}}" required="true" aria-required="true" aria-invalid="true">
                                                </div>


                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Program Fees types</label>
                                                    <select name="prog_fees_2" id="" class="form-control" required>
                                                        <option value="Per Month"
                                                         @if( $prog_data->prog_fees_2   ==  "Per Month" )
                                                                selected
                                                            @endif>Per Month
                                                            </option>
                                                        <option value="Complete Course" 
                                                        @if( $prog_data->prog_fees_2   ==  "Complete Course" )
                                                                selected
                                                            @endif>Complete Course</option>                                                      
                                                    </select>
                                                </div>
                                              

                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    
    getClassesFromCourse('{{ base64_encode($prog_data->course_id)}}','{{ base64_encode($prog_data->class_id)}}');
    getSubjectsFromClass('{{ base64_encode($prog_data->class_id)}}','{{ base64_encode($prog_data->subject_id)}}');

})

function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

function onCheck(name){
    if(name != ''){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_username/'+name,
            success:function(data){
                if(data > 0){
                    $('#class_username').addClass('has-error');
                    $('#submit_box').hide();

                    swal({
                    title: 'Oops...',
                    text: "Username already Exist. Please use diffrent username.",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ok'
                    }).then((result) => {
                        $("#exampleUsername").focus();
                    })
                  
                }else{
                    $('#class_username').removeClass('has-error');
                    $('#submit_box').show();
                }
            }
        })
    }
}

function getClassesFromCourse(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_classes_from_course/'+id+'/'+selected,
        success:function(data){
            $('#setClasses').html(data);
            
        }
    })
    var decodedString = atob(id);
   // alert($('#crs_'+decodedString).data('name'));
    $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_subject_from_class/'+id+'/'+selected,
        success:function(data){
            $('#setSubjects').html(data);            
        }
    })
   
}
function getClassesFromCourse2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
    loaderEnd();
}

function getSubjectsFromClass2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            $('#setSubjects').html(data);
            loaderEnd();
        }
    })
    var decodedString = atob(id);
    $('#prog_class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#prog_subject_name').val($('#sub_'+decodedString).data('name'));
}

</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        
                                        <div class="col-ms-12">
                        
                                            <div class="form-group bmd-form-group  col-md-6">
                                                <label for="exampleEmail" class="bmd-label-floating">Attendance Date</label>
                                                <input type="text" class="form-control datepicker" id="batch_data" name="batch_data" onfocusout="check_date(this.value)" required>
                                            </div>

                                             <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                <select name="batch_id" class="form-control" id="batch_id" onChange="get_batch_students(this.value)">
                                                    <option value="0000">Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}">{{$blist->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-12" id="get_data">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleStudentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('#batch_data').datetimepicker({
        format: 'DD-MM-YYYY',
        maxDate: moment().format('MM-DD-YYYY')
    });
}) 
var mainDate='';
function check_date(date){
    var batch_id = $('#batch_id').val();
    if(mainDate == date){
        
    }else{
        
        $('#get_data').html('');
        //$('#batch_data option[value=-]').prop('selected', true);
        if(batch_id != '0000'){
            $("#batch_id").val(batch_id).change();
        }else{
            $("#batch_id").val("0000").change();
            if(mainDate != ''){
                error('Date has been changed Please Select Batch Again.');
            }            
        } 
        mainDate = date;       
    }
}
function get_batch_students(idss){
    var datez = $('#batch_data').val();
    var batch_id = $('#batch_id').val();
    if(batch_id != '0000'){
        if(datez != ''){
            loaderStart('Loading Students...');
            $.ajax({
                type:'get',
                url:'{{ url('/')}}/ajax/get_batches_students_list/'+idss+'/'+datez,
                success:function(data){
                    if(data){
                        $('#get_data').html(data);
                        loaderEnd();
                    }else{
                        
                    }
                }
            })
        }else{
            error('Please Select date first.');
        }
    }   
}

</script>
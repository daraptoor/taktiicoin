
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>Logo</th>
                                                    <th>Inst Username</th>
                                                    <th>Inst Name</th>
                                                    <th>Inst Email</th>
                                                    <th>Inst Mobile</th>
                                                    <th>Status</th>
                                                    <th>QR</th>
                                                    <th>Remove</th>
                                                    <th>View/Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($admin_list as $alist)
                                                    <tr id="row_{{ $alist->id }}">
                                                        <td><img src="{{ url('/') }}/{{ $alist->logo }}" alt="{{ $alist->username }} Logo" style="height: 50px;width: 50px;"></td>
                                                        <td>{{ $alist->username }}</td>
                                                        <td>{{ $alist->inst_name }}</td>
                                                        <td>{{ $alist->inst_email }}</td>
                                                        <td>{{ $alist->inst_mobile }}</td>
                                                        <td>
                                                            <div class="togglebutton">
                                                                <label>
                                                                @if( $alist->status == 1)
                                                                    <input type="checkbox" id="status_{{ $alist->id }}" onChange="change_status('{{ $alist->id }}','{{ base64_encode($alist->id) }}')" checked="checked">
                                                                @elseif($alist->status == 0)
                                                                    <input type="checkbox" id="status_{{ $alist->id }}" onChange="change_status('{{ $alist->id }}','{{ base64_encode($alist->id) }}')">
                                                                @endif                                                                 
                                                                </label>
                                                            </div>  
                                                        </td>
                                                        <td><img src="{{ url('/') }}/{{ $alist->bar_code }}" alt="{{ $alist->bar_code }}" style="height: 50px;width: 50px;"> </td>
                                                        <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $alist->id }}','{{ base64_encode($alist->id) }}')">
                                                            <i class="material-icons">close</i>
                                                            Delete
                                                        <div class="ripple-container"></div></button>
                                                        </td>
                                                        <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/admin/edit/{{base64_encode($alist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                     
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

})

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/update_user_status/'+data+'/1',
            success:function(data){
                if(data == 1){
                    success('Institute/Admin status enabled Successfully', 500);
                }else{
                    error('Unable to update Institute/Admin status', 500);
                }
            }
        })
    }else{
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/update_user_status/'+data+'/0',
            success:function(data){
                if(data == 1){
                    success('Institute/Admin status disabled Successfully', 500);
                }else{
                    error('Unable to update Institute/Admin status', 500);
                }
            }
        })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Institute/Admin?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                    type:'get',
                    url:'{{ url('/')}}/ajax/update_user_status/'+data+'/2',
                    success:function(data){
                        if(data == 1){
                            success('Institute/Admin Deleted Successfully', 500);
                            var table = $('#tbleTeacherList').DataTable();  
                            table.row( $("#row_"+id) ).remove().draw();
                        }else{
                            error('Unable to Delete Institute/Admin status', 500);
                        }
                    }
                })
            }            
        })

}
</script>
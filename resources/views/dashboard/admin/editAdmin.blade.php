
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/admin/update_admin')}}" enctype="multipart/form-data">
                                   
                                    @csrf
                                        <div class="card-body col-md-8">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Branch Basic Info</h4>
                                                    <input type="hidden" name="user_id" value="{{ $inst_data->id }}">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Username</label>
                                                    <input type="text" class="form-control" id="exampleUsername" name="username" value="{{ $inst_data->username }}"  readonly>
                                                </div>
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Password</label>
                                                    <input type="text" class="form-control" id="" maxlength="8" name="password" value="**********"  readonly >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Insitute Name</label>
                                                    <input type="text" class="form-control" id="" name="inst_name" value="{{ $inst_data->inst_name }}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Insitute Email</label>
                                                    <input type="email" class="form-control" id="" name="inst_email" value="{{ $inst_data->inst_email }}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Insitute Mobile</label>
                                                    <input type="number" class="form-control" id="" name="inst_mobile" max="9999999999" value="{{ $inst_data->inst_mobile }}" required>
                                                </div>
                                               
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <label for="examplePass" class="bmd-label-floating">Insitute Address</label>
                                                    <input type="text" class="form-control"  name="address"  value="{{ $inst_data->address }}" onFocus="initializeAutocomplete()" id="locality" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                   
                                                    <input type="hidden" class="form-control"  name="lng" value="{{ $inst_data->lng }}" id="longitude" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                   
                                                    <input type="hidden" class="form-control"  name="lat" value="{{ $inst_data->lat }}"  id="latitude" >
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Branch Admin Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleEmail1" class="bmd-label-floating">Branch Admin First Name</label>
                                                    <input type="text" class="form-control" id="exampleEmail1" name="ba_fname" value="{{ $inst_data->ba_fname }}" required>
                                                </div>
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Branch Admin Last Name</label>
                                                    <input type="text" class="form-control" id="" name="ba_lname" value="{{ $inst_data->ba_lname }}" required>
                                                </div>

                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h4 class="title">Institute Image or Logo</h4>                                           
                                            <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail">
                                                    <img src="{{url('/')}}/{{ $inst_data->logo }}" alt="...">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{ $inst_data->logo }}" name="image_exist"><input type="file" name="image" >
                                                    <div class="ripple-container"></div></span>
                                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> Remove<div class="ripple-container">
                                                    <div class="ripple-decorator ripple-on ripple-out" style="left: 11.9063px; top: 32.5px; background-color: rgb(255, 255, 255); transform: scale(15.623);">
                                                    </div></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkmRzgPT_YinELO77cj-Sju5IsqUrfrn0&sensor=false&libraries=places"
async defer></script>
<script>
function initializeAutocomplete(){
var input = document.getElementById('locality');

var options = {}

var autocomplete = new google.maps.places.Autocomplete(input, options);

google.maps.event.addListener(autocomplete, 'place_changed', function() {
var place = autocomplete.getPlace();
var lat = place.geometry.location.lat();
var lng = place.geometry.location.lng();
var placeId = place.place_id;
// to set city name, using the locality param
var componentForm = {
locality: 'short_name',
};
for (var i = 0; i < place.address_components.length; i++) {
var addressType = place.address_components[i].types[0];
if (componentForm[addressType]) {
var val = place.address_components[i][componentForm[addressType]];
//document.getElementById("city").value = val;
}
}
console.log(lat);
console.log(lng);


document.getElementById("latitude").value = lat;
document.getElementById("longitude").value = lng;
document.getElementById("location_id").value = placeId;
});
}

function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

function onCheck(name){
    if(name != ''){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_username/'+name,
            success:function(data){
                if(data > 0){
                    $('#class_username').addClass('has-error');
                    $('#submit_box').hide();

                    swal({
                    title: 'Oops...',
                    text: "Username already Exist. Please use diffrent username.",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ok'
                    }).then((result) => {
                        $("#exampleUsername").focus();
                    })
                  
                }else{
                    $('#class_username').removeClass('has-error');
                    $('#submit_box').show();
                }
            }
        })
    }
}
</script>
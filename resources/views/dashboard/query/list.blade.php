
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">

                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <!-- <th>Id</th> -->
                                                    <!-- <th>Query Id</th> -->
                                                    <!-- <th>StudentId</th> -->
                                                    <th><b>Student Name</b></th>
                                                    <th><b>Mobile No</b></th>
                                                    <th><b>Class Name</b></th>
                                                    <th><b>Subject Name</b></th>
                                                    <th><b>Days</b></th>
                                                    <th><b>Address</b></th>
                                                    <!-- <th>Additional Info</th> -->
                                                    <th><b>Date</b></th>
                                                    <th><b>Interested</b></th>
                                                    <!-- <th>Remove</th> -->
                                                   
                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($query_list as $dlist)
                                                    <tr id="row_{{ $dlist->id }}">
                                                    <!-- <td>{{ $dlist->id }}</td> -->
                                                    <!-- <td>{{ $dlist->query_id }}</td> -->
                                                    <!-- <td>{{ $dlist->student_id }}</td> -->
                                                   <td>{{ $dlist->students['first_name'] }} {{ $dlist->students['last_name'] }}</td>
                                                   <td>{{ $dlist->students['mobile'] }}</td>
                                                   <td>{{ $dlist->querystudentdata['class_name'] }}</td>
                                                   <td>{{ $dlist->querystudentdata['subject_name'] }}</td>
                                                   <td><a href="#" data-toggle="tooltip" data-placement="right" title="{{ str_replace(",",", ",$dlist->querystudentdata['days']) }}"> {{ substr_count( str_replace(",",", ",$dlist->querystudentdata['days']),', ')+1 }} Days</a></td>
                                                   <!-- <td>{{ $dlist->querystudentdata['days'] }}</td> -->
                                                   <td>{{ $dlist->querystudentdata['address'] }}</td>
                                                   <!-- <td>{{ $dlist->querystudentdata['additinal_info'] }}</td> -->
                                                   <td>{{ $dlist->querystudentdata['created'] }}</td>
                    
                                                    <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $dlist->status == 1)
                                                                <input type="checkbox" id="status_{{ $dlist->id }}" onChange="change_status('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')" checked="checked">
                                                            @elseif($dlist->status == 0)
                                                                <input type="checkbox" id="status_{{ $dlist->id }}" onChange="change_status('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td>
                                                    <!-- <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td> -->

                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})
function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_institute_query/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Query status enabled Successfully', 500);
                 }else{
                     error('Unable to update Query status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_institute_query/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Query status disabled Successfully', 500);
                 }else{
                     error('Unable to update Query status', 500);
                 }
             }
         })
    }    
}

// function delete_admin(id,data){
//     swal({
//         title: 'Wait...',
//         text: "Are you sure you want to delete Query?<br><strong>This can't be Undone</strong>",
//         type: 'question',
//         showCancelButton: true,
//         confirmButtonColor: '#3085d6',
//         cancelButtonColor: '#d33',
//         confirmButtonText: 'Yes, Delete it'
//         }).then((result) => {            
//             if(result){
//                 $.ajax({
//                      type:'get',
//                      url:'{{ url('/')}}/ajax/update_institute_query/'+data+'/2',
//                      success:function(data){
//                          if(data == 1){
//                              success('Query Deleted Successfully', 500);
//                              var table = $('#tbleTeacherList').DataTable();  
//                              table.row( $("#row_"+id) ).remove().draw();
//                          }else{
//                              error('Unable to Delete Query', 500);
//                          }
//                      }
//                 })
//             }            
//         })

// }


</script>
<!-- Modal -->

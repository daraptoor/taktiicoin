
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                            <div class="col-md-12"> 

                                                <div class="row">
                                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="orange">
                                                                <i class="material-icons">recent_actors</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Students</p>
                                                                <h3 class="card-title">{{ $student_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/student/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="rose">
                                                                <i class="material-icons">perm_identity</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Teachers</p>
                                                                <h3 class="card-title">{{ $teacher_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/teacher/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="green">
                                                                <i class="material-icons">weekend</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Batches</p>
                                                                <h3 class="card-title">{{ $batch_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/batch/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="blue">
                                                                <i class="material-icons">event_seat</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Program</p>
                                                                <h3 class="card-title">{{ $program_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/program/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <h3>Payment Data</h3>
                                                    <div class="ct-chart ct-perfect-fourth"></div>
                                                </div>

                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <h3>Institute Data</h3>
                                                    <div class="ct-chart2 ct-perfect-fourth"></div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    var data = {
    // A labels array that can contain any sort of values
    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    // Our series array that contains series objects or in this case series data arrays
    series: [
        [500, 300, 400, 200, 1000, 500, 200, 400, 200, 1000, 500, 200]
    ]
    };

    // Create a new line chart object where as first parameter we pass in a selector
    // that is resolving to our chart container element. The Second parameter
    // is the actual data object.
    new Chartist.Line('.ct-chart', data);


    var data = {
    labels: ['Teachers ({{ $teacher_count }})', 'Batch ({{ $batch_count }})', 'Students ({{ $student_count }})'],
    series: [{{ $teacher_count }}, {{ $batch_count }}, {{ $student_count }}]
    };

    var options = {
    labelInterpolationFnc: function(value) {
        return value[0]
    }
    };

    var responsiveOptions = [
    ['screen and (min-width: 640px)', {
        chartPadding: 30,
        labelOffset: 100,
        labelDirection: 'explode',
        labelInterpolationFnc: function(value) {
        return value;
        }
    }],
    ['screen and (min-width: 1024px)', {
        labelOffset: 80,
        chartPadding: 20
    }]
    ];

    new Chartist.Pie('.ct-chart2', data, options, responsiveOptions);
})


</script>
<style>
.ct-label{
    font-size: 16px;
    font-weight: 800;
}
</style>
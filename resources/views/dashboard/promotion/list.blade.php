<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    
                                                    <th><b>Title</b></th>
                                                    <th><b>Attachment</b></th>
                                                    <th><b>File Type</b></th>
                                                    <!-- <th><b>status</b></th> -->
                                                    <th><b>Remove</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($promotion_box as $slist)
                                                    <tr id="row_{{ $slist->id }}">                                                    
                                                    <td>{{ $slist->title }}</td>
                                                    <td>
                                                    <a href="{{ url('/') }}/{{$slist->file_url}}" target="_blank">FILE</a>
                                                    </td>
                                                    <td>{{ $slist->file_type }}</td>                                          
                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $slist->status == 1)
                                                                <input type="checkbox" id="status_{{ $slist->id }}" onChange="change_status('{{ $slist->id }}','{{ base64_encode($slist->id) }}')" checked="checked">
                                                            @elseif($slist->status == 0)
                                                                <input type="checkbox" id="status_{{ $slist->id }}" onChange="change_status('{{ $slist->id }}','{{ base64_encode($slist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->
                                                    <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $slist->id }}','{{ base64_encode($slist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_promotion_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('File status enabled Successfully', 500);
                 }else{
                     error('Unable to update File status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_promotion_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('File status disabled Successfully', 500);
                 }else{
                     error('Unable to update File status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete File?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_promotion_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('File Deleted Successfully', 500);
                             var table = $('#tbleTeacherList    ').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete File', 500);
                         }
                     }
                })
            }            
        })

}
</script>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/promotion/add_promotion')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">

                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" name="title" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File Type</label>
                                                    <select name="file_type" id="file_type" class="form-control" required>
                                                        <option value="doc">DOC</option>
                                                        <option value="img">Image</option>
                                                        <option value="video">Video</option>
                                                        <option value="pdf">Audio</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File description</label>
                                                    <textarea class="form-control" rows="3"name="file_desc" required ></textarea>
                                                </div>
                                    
                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

                                                    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
                                                    <p class="help-block">Example block-level help text here.</p>
                                                </div>


                                        </div>



                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Update</button>
                                            </div>
                                        </div>

                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})




</script>

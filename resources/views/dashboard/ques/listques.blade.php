<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><b>Batch Name</b></th>
                                                <th><b>Question</b></th>
                                                <th><b>Types</b></th>
                                                <th><b>Detail</b></th>
                                                <th><b>Date<b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($ques_list as $qlist)
                                                <tr id="row_{{ $qlist->id }}">
                                                    <td>{{ $qlist->batchinfo['title'] }}</td>
                                                    <td>{{ $qlist->question }}</td>
                                                    <td>{{ $qlist->ans_type }}</td>
                                                    <td><a href="{{ url('/') }}/ques/viewmultiques/{{$qlist->id}}">Detail
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    <td>{{ $qlist->created }}</td>
                                                    
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})


</script>
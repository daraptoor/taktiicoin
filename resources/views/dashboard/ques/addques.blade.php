<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/ques/add_new_ques')}}">
                                    @csrf
                                        <div class="col-ms-12">

										  <div class="form-group" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Type</label>
                                                    <select name="type" id="type" class="form-control">
                                                        <option value="inst" selected>Institute</option>
                                                    </select>
                                                </div>

										<div class="form-group" id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                <select class="form-control" onChange="getClassesFromCourse(this.value)" id="course_id" name="course_id">
                                                    <option disabled selected>Select Course</option>
                                                    @foreach($courses_list as $cl)
                                                    <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" name="course_name" value="" id="course_name">
                                            </div>

											 <div class="form-group " id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="class_id">
                                                    <option disabled selected>Select Course First</option>
                                                </select>
                                                <input type="hidden" name="class_name" value="" id="class_name">
                                            </div>
    
                                            <div class="form-group" id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="subject_id" >
                                                    <option disabled selected> Select Class First </option>
                                                </select>
                                                <input type="hidden" name="subject_name" value="" id="subject_name">
                                            </div>


                                          <div class="form-group bmd-form-group">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Name</label>
                                                    <select class="form-control" onChange="set_batch(this.value)" id="batch_id" name="batch_id" required>
                                                        <option value="" selected disabled>Select Batch</option>
                                                        @foreach($batch_list as $bl)
                                                        <option id="batch_{{$bl->id}}" value="{{$bl->id }}" data-name="{{ $bl->title }}">{{ $bl->title }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="title" id="title" value="">
                                                </div>
								
                                       	
                                                <div class="form-group bmd-form-group">
                                                    <label for="exampleUsername" class="bmd-label-floating">Topic</label>
                                                    <input type="text" class="form-control" id="topic" name="topic" required>
                                                </div>
                                        

         <div class="checkbox-radios">
		<div class="radio">
		<label>
		<input type="radio" name="ans_type" id="id_radio1" value="multiple" onchange="upQues(this.value);"><span class="circle"></span><span class="check"></span> Quiz </label>
		</div>
		<div class="radio">
		<label>
		<input type="radio" name="ans_type" id="id_radio2" value="single"  onchange="upQues(this.value);" ><span class="circle"></span><span class="check"></span> Question</label>
		</div> 
		</div>

		<div class="clearfix"></div>
		<div class="question append_box hidden" id="id_data" >
		<div class="form-group" id="counter">
		<label class="control-label">Q1) Write your Question  </label>
		<textarea class="form-control" id="mq1" name="ques_1[]"></textarea> 
		</div>

		<div class="form-group">
		<label class="control-label">Write Option A  </label>
		<input type="text" class="form-control" id="otn_1" name="option_a_1[]">
		</div>

		<div class="form-group">
		<label class="control-label">Write Option B </label>
		<input type="text" class="form-control" id="otn_2" name="option_b_1[]">
		</div>

		<div class="form-group">
		<label class="control-label">Write Option C  </label>
		<input type="text" class="form-control" id="otn_3" name="option_c_1[]">
		</div>

		<div class="form-group">
		<label class="control-label">Write Option D  </label>
		<input type="text" class="form-control" id="otn_4" name="option_d_1[]">
		</div>

		<p><b>Please Select write answer</b></p>
		<div class="select_ans">

		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label>
		<input type="radio"  name="ans_1[]" id="ans1" value="1"><b> A</b>
		</label>
		</div>		
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label>
		<input type="radio"  name="ans_1[]" id="ans2" value="2"><b> B</b>
		</label>
		</div>		
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label>
		<input type="radio" name="ans_1[]" id="ans3" value="3"><b>C</b>
		</label>
		</div>		
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label>
		<input type="radio" name="ans_1[]" id="ans4"  value="4"><b> D</b>
		</label>
		</div>		
	    </div>
		</div>
		</div>
	
		<div class="question append_box2 " id="id_data_1"></div>
		<!--question-->
		<div class="clearfix"></div>

		<div class="multiques append hidden" id="id_data2">
		<div class="form-group">
		<label class="control-label">Q1) Write your Question </label>
		<textarea class="form-control" id="q1" name="sin_ques[]"></textarea> 
		</div>
		</div>
		<div class="multiques append2" id="id_data2_1"></div>
		<div class="clearfix"></div>
		<span class="btn btn-fill hidden" id="append_btn_1">append</span>

		<span class="btn btn-fill hidden" id="append_btn_2">append</span>
		<input type="hidden" id="total_question" name="total_question" value="1">
		<button type="submit" class="btn btn-fill btn-rose">Submit</button>

		</form>        

	</div>
	</div>
	</div> 
	</div>
	</div>
	</div>



<script>
var num = 1;
var count = 1;
let click_chk = 0;
let show_ques = r =>{
	$('#id_data2').removeClass('hidden');
	$('#append_btn_2').hide();
	$('#id_data_1').html("");
	$('#append_btn_1').removeClass('hidden');
	$('#append_btn_1').show();

	$("#id_data").hide("fast");
	$("#id_data2").show("fast");
	$("#id_data_1").hide("fast");
	$("#id_data2_1").show("fast");

	$("#mq1").val("");
	$("#otn_1").val("");
	$("#otn_2").val("");
	$("#otn_3").val("");
	$("#otn_4").val("");
	$("#ans1").prop("checked", false);
	$("#ans2").prop("checked", false);
	$("#ans3").prop("checked", false);
	$("#ans4").prop("checked", false);
	


}

let show_quiz = r =>{
	$('#id_data').removeClass('hidden');
	$('#append_btn_1').hide();
	$('#id_data2_1').html("");
	$('#append_btn_2').removeClass('hidden');
	$('#append_btn_2').show();
	
	$("#id_data").show("fast");
    $("#id_data2").hide("fast");
    $("#id_data_1").show("fast");
    $("#id_data2_1").hide("fast");

    $("#q1").val("");
}


let upQues = r => {
	
	//alert(r);
	if(click_chk == 0){
		console.log(click_chk);
		if(r == 'single'){
			count = 1;
			show_ques();				
		}else{
			num = 1;
			show_quiz();			   
		    
		}
	}else{
		console.log(click_chk);
		let confir = confirm("All question added will be removed. if you change question type. are you sure you want to change?");
		if(confir == true){
			if(r == 'single'){
				count = 1;
				show_ques();
				$('#total_question').val(1);				
			}else{
				num = 1;
				show_quiz();
				$('#total_question').val(1);
			}
		}else{
			//return false;
			if(r == "single"){
				$('#id_radio1').prop('checked', true);
				$('#id_radio2').prop('checked', false);	
			}else{
				
				$('#id_radio2').prop('checked', true);
				$('#id_radio1').prop('checked', false);		
			}

		}
	}
	
click_chk++;

}


$(function() {


	$("#append_btn_2").click(function(){

		num++;
		$('#total_question').val(num);
		let append_box = `<div class="clearfix"></div>
		<div class="box">
		<label class="control-label">Q${num}) Write your Question</label>
		<textarea class="form-control" name="ques_${num}[]"></textarea>
		</div>
		<div class="form-group label-floating is-empty">
		<label class="control-label">Write Option A  </label>
		<input type="text" class="form-control" name="option_a_${num}[]" >
		</div>
		<div class="form-group label-floating is-empty">
		<label class="control-label">Write Option B </label>
		<input type="text" class="form-control" name="option_b_${num}[]">
		</div>
		<div class="form-group label-floating is-empty">
		<label class="control-label">Write Option C  </label>
		<input type="text" class="form-control" name="option_c_${num}[]">
		</div>
		<div class="form-group label-floating is-empty">
		<label class="control-label">Write Option D </label>
		<input type="text" class="form-control" name="option_d_${num}[]">
		</div>
		<p><b>Please Select write answer</b></p>
		<div class="select_ans">
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label><input type="radio" name="ans_${num}[]" value=1"><b> A</b></label>
		</div>
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label><input type="radio" name="ans_${num}[]" value="2"><b> B</b></label>
		</div>
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio">
		<label><input type="radio" name="ans_${num}[]" value="3"><b>C</b></label>
		</div>
		</div>
		<div class="checkbox-radios" style="float: left;">
		<div class="radio"><label>
		<input type="radio" name="ans_${num}[]" value="4"><b> D</b></label>
		</div>
		</div>
		</div>
		</div>
		</div>`;
		$(".append_box2").append(append_box); 
		
	});
		


$("#append_btn_1").click(function(){ //Single Question

count++;
$('#total_question').val(count);
let append = `<div class="box">
<div class="form-group">
<label class="control-label">Q${count}) Write your Question  </label>
<textarea class="form-control" name="sin_ques[]"></textarea>
</div></div>`;
		$(".append2").append(append); 
	});

});

$(document).ready(function() {
 // show the table as soon as the DOM is ready
 $("#id_data").hide();

});

function getClassesFromCourse(id){
	loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
			loaderEnd();
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id){
	loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
			loaderEnd();
            $('#setSubjects').html(data);
        }
    })
    var decodedString = atob(id);
    $('#class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#subject_name').val($('#sub_'+decodedString).data('name'));
}

</script>
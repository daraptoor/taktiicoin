
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><b>Class Name</b></th>
                                                <th><b>Subject Name</b></th>
                                                <th><b>Topic</b></th>
                                                <th><b>Question</b></th>
                                                <th><b>Answer</b></th>
                                                <th><b>option1</b></th>
                                                <th><b>option2</b></th>
                                                <th><b>option3</b></th>
                                                <th><b>option4</b></th>
                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($viewques_list as $vqlist)
                                                <tr id="row_{{ $vqlist->id }}">
                                                    <td>{{ $vqlist->class_name }}</td>
                                                    <td>{{ $vqlist->subject_name }}</td>
                                                    <td>{{ $vqlist->topic }}</td>
                                                    <td>{{ $vqlist->question }}</td>
                                                    <td>{{ $vqlist->answer }}</td>
                                                    <td>{{ $vqlist->option1 }}</td>
                                                    <td>{{ $vqlist->option2 }}</td>
                                                    <td>{{ $vqlist->option3}}</td>
                                                    <td>{{ $vqlist->option4 }}</td>


                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})
</script>
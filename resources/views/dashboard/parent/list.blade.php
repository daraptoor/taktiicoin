
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Parent List</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleParentList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Mobile</th>
                                                    <th>Father Name</th>
                                                    <th>Mother Name</th>
                                                    <th>Parent Mobile</th>
                                                    <th>SMS</th>
                                                    <th>Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($parent_list as $slist)
                                                <tr id="row_{{ $slist->id }}">
                                                    <td>
                                                    <?php $url = $slist->image ;?>
                                                        @if ((!(substr($url, 0, 7) == 'http://')) && (!(substr($url, 0, 8) == 'https://')))
                                                            <?php $return =  url('/')."/".$slist->image;?> 
                                                        @else
                                                            <?php $return = $slist->image;?> 
                                                        @endif
                                                            <img src="{{ $return }}" alt="{{ $slist->username }} Logo" style="height: 50px;width: 50px;">
                                                    </td>
                                                    <td>{{ $slist->first_name }}</td>
                                                    <td>{{ $slist->email }}</td>
                                                    <td class="text-primary"><strong>{{ $slist->mobile }}</strong></td>
                                                    <td>{{ $slist->father_name }}</td>
                                                    <td>{{ $slist->mother_name }}</td>
                                                    <td class="text-warning"><strong>{{ $slist->parent_mob }}</strong></td>
                                                     <td>
                                                     <button type="button" class="btn btn-primary btn-round btn-fab" onClick="showSmsModal('{{ $slist->mobile }}','{{ $slist->parent_mob }}')" style="height:40px;width:40px;min-width: 40px;">
                                                        <i class="material-icons">textsms</i>
                                                     </button>
                                                    </td>
                                                    <td>
                                                    <button type="button" class="btn btn-warning btn-round btn-fab" onClick="showEditModal('{{ $slist->id }}','{{ $slist->mother_name }}','{{ $slist->father_name }}','{{ $slist->parent_mob }}')" style="height:40px;width:40px;min-width: 40px;">
                                                    <i class="material-icons">create</i>
                                                     </button>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- SMS Modal -->
  <div class="modal fade" id="showSmsModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Message</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
            <label for="sel1">Choose mobile number</label>
            <select class="form-control" id="mobile_no" name="mobile_no">
              <option value="9560610340" id="num_1">9560610340</option>
              <option value="9560610340" id="num_2">9560610340</option>
            </select>
          </div>
          <div class="form-group">
            <label for="comment">Message:</label>
            <textarea class="form-control" rows="5" id="message" name="message"></textarea>
          </div>
          <button type="button" class="btn btn-primary" onClick="sendSms()">Submit</button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Edit Modal -->
<div class="modal fade" id="showEditModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit/Update Parent Detail</h4>
        </div>

        <div class="modal-body">
        <form method="post" action="{{URL::to('/parent/update3')}}">
        @csrf
        <input type="hidden" class="form-control" id="std_id" name="std_id" value="" required>
            <div class="form-group bmd-form-group">
                <label for="examplePass" class="bmd-label-floating">Mother Name</label>
                <input type="text" class="form-control" id="mother_name" name="mother_name" value="" required>
            </div>
            <div class="form-group bmd-form-group">
                <label for="examplePass" class="bmd-label-floating">Father Name</label>
                <input type="text" class="form-control" id="father_name" name="father_name" value=""  required>
            </div>
            <div class="form-group bmd-form-group">
                <label for="examplePass" class="bmd-label-floating">Parent Number</label>
                <input type="text" class="form-control" maxlength="10" id="parent_mob" name="parent_mob" value=""  required>
            </div>
            <div class="form-group bmd-form-group" id="submit_box">
                <button class="btn btn-success" >Update</button>
            </div>
        </form>
        </div>

      </div>
      
    </div>
  </div>

<script>
$(function(){
    dt('tbleParentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function showSmsModal(num1,num2){
    $('#num_1').val(num1).text(num1+" (Student Number)");
    if(num2){
        $('#num_2').val(num2).text(num2+" (Parent Number)");
    }else{
        $('#num_2').val('0000').text("Parent Number Not avaliable.");
    }

    $('#showSmsModal').modal('show');
}

function showEditModal(std_id,m_name,f_name,p_mob){
    $('#std_id').val(std_id);
    $('#mother_name').val(m_name);
    $('#father_name').val(f_name);
    $('#parent_mob').val(p_mob);

    $('#showEditModal').modal('show');
}

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_student_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Student status enabled Successfully', 500);
                 }else{
                     error('Unable to update Student status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_student_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Student status disabled Successfully', 500);
                 }else{
                     error('Unable to update Student status', 500);
                 }
             }
         })
    }    
}


function sendSms(){
var mob_number = document.getElementById("mobile_no").value;
var message =  document.getElementById("message").value;
    if(mob_number!='' && message!='' && mob_number != '0000'){
        var sms = sendSMS(mob_number,message);
        success(`SMS to ${mob_number} sent Successfully...`);
    }else{
        error(`Unable to SMS...`);
    }
}
</script>
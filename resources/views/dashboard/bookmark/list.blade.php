
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Sno</b></th>
                                                    <th><b>Student Name<b></th>
                                                    <th><b>City<b></th>
                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                           
                                                @foreach($bookmark_list as $dlist)
                                               
                                                    <tr id="row_{{ $dlist->id }}">
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $dlist->students['first_name'] }} {{ $dlist->students['last_name'] }}</td>
                                                    <td>{{ $dlist->students['city'] }}</td>
                                                    </tr>
                                                    
                                                @endforeach

                                              
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            
            <script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  



</script>
<!-- Modal -->
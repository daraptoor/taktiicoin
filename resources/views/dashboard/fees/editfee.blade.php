
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/fee/update_fees')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Fees Info</h4>
                                                     <input type="hidden" name="id"  value="{{ $fee_data->id }}">
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Student Mobile</label>
                                                    <input type="number" class="form-control" id="mobile" name="mobile" value="{{ $fee_data->mobile }}" max="9999999999"  onkeyup="nospaces(this)" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Student Name</label>
                                                    <input type="text" class="form-control"  name="student_name" value="{{ $fee_data->student_name }}" required>
                                                </div>  
                                               
                                                <!-- <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                    <select name="batch_id" class="form-control" id="batch_id" onChange="set_batch(this.value)" required>
                                                    <option>Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}"
                                                    @if( $fee_data->batch_id   ==  $blist->id )
                                                        selected
                                                    @endif
                                                    >{{$blist->title}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" id="batch_name" name="batch_name"  value="{{ $fee_data->batch_name }}" required> 
                                                </div> -->
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="prog_id" class="bmd-label-floating">Program Name</label>
                                                    <select name="prog_id" class="form-control" id="prog_id" onChange="set_prog(this.value)" required>
                                                    <option>Select Program</option>
                                                    @foreach($program_list as $plist)
                                                    <option value="{{$plist->id}}" id="prog_{{$plist->id}}"  data-name="{{ $plist->prog_name }}"
                                                    @if( $fee_data->prog_id   ==  $plist->id )
                                                        selected
                                                    @endif
                                                    >{{$plist->prog_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" id="prog_name" name="prog_name"  value="{{ $fee_data->prog_name }}" > 
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Paid Amount</h4>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount</label>
                                                    <input type="number" class="form-control" id="" name="amount"   value="{{ $fee_data->amount }}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount Paid</label>
                                                    <input type="number" class="form-control" id="" name="paid"  value="{{ $fee_data->paid }}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Status</label>
                                                    <select name="paid_status" id="" class="form-control">
                                                        <option value="paid"
                                                            @if( $fee_data->paid_status   ==  "paid" )
                                                                selected
                                                            @endif
                                                        >Paid</option>
                                                        <option value="unpaid"
                                                            @if( $fee_data->paid_status   ==  "unpaid" )
                                                                selected
                                                            @endif
                                                        >Unpaid</option>
                                                        <option value="part"
                                                            @if( $fee_data->paid_status   ==  "part" )
                                                                selected
                                                            @endif
                                                        >Part Payment</option>                                                        
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Paid Date</label>
                                                    <input type="text" class="form-control date" id="" name="paid_date"  value="{{ $fee_data->paid_date }}" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Fees Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Amount</label>
                                                    <input type="number" class="form-control" id="" name="pending"   value="{{ $fee_data->pending }}" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Status</label>
                                                    <select name="pending_status" id="" class="form-control" required>
                                                        <option value="paid"
                                                            @if( $fee_data->pending_status   ==  "paid" )
                                                                selected
                                                            @endif
                                                        >Paid</option>
                                                        <option value="unpaid"
                                                            @if( $fee_data->pending_status   ==  "unpaid" )
                                                                selected
                                                            @endif
                                                        >Unpaid</option>                                                      
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Due Date</label>
                                                    <input type="text" class="form-control date" name="pending_date"   value="{{ $fee_data->pending_date }}" >
                                                </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){
    
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

function set_batch(idss){
    $('#batch_name').val($('#batch_'+idss).data('name'));
}
function set_prog(idss){
    $('#prog_name').val($('#prog_'+idss).data('name'));
}

</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <!-- <th><b>Student ID</b></th> -->
                                                    <th><b>Student name<b></th>
                                                    <th><b>Program name<b></th>
                                                    <th><b>Mobile<b></th>
                                                    <!-- <th><b>Program ID<b></th> -->
                                                    <th><b>Amount</b></th>
                                                    <th><b>Paid Date</b></th>
                                                  
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($fee_list as $plist)
                                                    <tr id="row_{{ $plist->id }}">
                                                    <!-- <td>{{ $plist->students['id'] }}</td> -->
                                                    <td>{{ $plist->students['first_name'] }}</td>
                                                    <td>{{ $plist->programdetails['prog_name'] }}</td>
                                                    <td>{{ $plist->students['mobile'] }}</td>
                                                    <!-- <td>{{ $plist->batch_id }}</td> -->
                                                    <td>{{ $plist->amount }}</td>
                                                    <td>{{ $plist->paid_on }}</td>
                                                  
                                                    <!-- <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/fee/edit/{{ $plist->id }}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td> -->
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

</script>
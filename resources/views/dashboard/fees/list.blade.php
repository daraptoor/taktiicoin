
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Student name</b></th>
                                                    <th><b>Mobile</b></th>
                                                    <th><b>Prog Name</b></th>
                                                    <!-- <th><b>Batch</b></th> -->
                                                    <th><b>Amount</b></th>
                                                    <th><b>Paid</b></th>
                                                    <th><b>Pending</b></th>
                                                    <th><b>Paid Date</b></th>
                                                    <th><b>View/Edit</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($fee_list as $plist)
                                                    <tr id="row_{{ $plist->id }}">
                                                    <td>{{ $plist->student_name }}</td>
                                                    <td>{{ $plist->mobile }}</td>
                                                    <td>{{ $plist->prog_name }}</td>
                                                    <!-- <td>{{ $plist->batch_name }}</td> -->
                                                    <td>{{ $plist->amount }}</td>
                                                    <td>{{ $plist->paid }}</td>
                                                    <td>{{ $plist->pending }}</td>
                                                    <td>{{ $plist->created_at }}</td>
                                                  
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/fee/edit/{{ $plist->id }}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

</script>
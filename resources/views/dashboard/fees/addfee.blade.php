
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/fee/add_new_fees')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Fees Info</h4>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Student Mobile<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control" id="mobile" name="mobile"  max="9999999999" onfocusout="onCheck(this.value);" required>
                                                    <!-- <input type="number" class="form-control" id="mobile" name="student_mobile" max="9999999999"  onkeyup="nospaces(this)" required> -->
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Student Name<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="first_name" name="student_name" required>
                                                </div>
                                                <!-- <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="batch_id" class="bmd-label-floating">Batch Name<span style="color: red;">* </span></label>
                                                    <select name="batch_id" class="form-control" id="batch_id" onChange="set_batch(this.value)" required>
                                                    <option>Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}">{{$blist->title}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" value="" id="batch_name" name="batch_name"> 
                                                </div> -->
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="prog_id" class="bmd-label-floating">Program Name<span style="color: red;">* </span></label>
                                                    <select name="prog_id" class="form-control" id="prog_id" onChange="set_prog(this.value)" required>
                                                    <option>Select Program</option>
                                                    @foreach($program_list as $plist)
                                                    <option value="{{$plist->id}}" id="prog_{{$plist->id}}"  data-name="{{ $plist->prog_name }}">{{$plist->prog_name}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" value="" id="prog_name" name="prog_name"> 
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Paid Amount<span style="color: red;">* </span></h4>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control" id="" name="amount" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount Paid<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control" id="" name="paid" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Status<span style="color: red;">* </span></label>
                                                    <select name="paid_status" id="" class="form-control" required>
                                                        <option value="paid">Paid</option>
                                                        <option value="unpaid">Unpaid</option>
                                                        <option value="due">Due</option>                                                        
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Paid Date<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control date" id="" name="paid_date" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Fees Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Amount</label>
                                                    <input type="number" class="form-control" id="" name="pending">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Status</label>
                                                    <select name="pending_status" id="" class="form-control" >
                                                        <option value="paid">Paid</option>
                                                        <option value="unpaid">Unpaid</option>                                                       
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Due Date</label>
                                                    <input type="text" class="form-control date" name="pending_date"  >
                                                </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Update</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){
    
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

function set_batch(idss){
    $('#batch_name').val($('#batch_'+idss).data('name'));
}
function set_prog(idss){
    $('#prog_name').val($('#prog_'+idss).data('name'));
}

function onCheck(number){
   
   if(number != ''){
       loaderStart('Validating Number..');
       $.ajax({
           type:'get',
           url:'{{ url('/')}}/ajax/chk_exist_number/'+number,
           success:function(data){
               console.log('data>>>>',data)
               loaderEnd();
               if(data.id){
                 $('#std_id').val(data.id);
                 var name = data.first_name
                 var name2 = name.split(' ');
                 //console.log(name2.length);
                 var fname = name2[0];
                 var lname = name2[1];
                 for(i=2;i<name2.length;i++){
                   lname += " "+name2[i];
                   //console.log(name2[i]);
                 }
                 $('#first_name').val(fname);
                 $('#last_name').val(lname);
                 $('#email').val(data.email);
                 $('#gender').val(data.gender);
                 $('#parent_mob').val(data.parent_mob);
                 $('#image_exist').val("https://taktii.com/etc/api/profile_image/"+data.profile_pic);
                 $('#img_show').attr("src","https://taktii.com/etc/api/profile_image/"+data.profile_pic);
                 $('#address').val(data.address);
                 $('#address2').val(data.address2);
                 $('#city').val(data.city);
                 $('#state').val(data.state);
                 $('#zip').val(data.zip);
                 $('#mother_name').val(data.mother_name);
                 $('#father_name').val(data.father_name);
               }else{
                   $(`#first_name,
                    #last_name,
                    #email,
                    #gender,
                    #parent_mob,
                    #address,
                    #address2,
                    #city,                   
                    #state,                   
                    #zip,                   
                    #mother_name,                   
                    #father_name                 
                    `).val('');
                   $('#img_show').attr("src","https://taktii.sgp1.digitaloceanspaces.com/static/300x200.png");
                   $('#std_id').val('0');
                   $('#image_exist').val('');
               }
           }
       })
   }
}

</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                   
                                                    
                                                    <th><b>Student Name<b></th>
                                                    <th><b>Student Mobile<b></th>
                                                    <th><b>Program Name<b></th>
                                                    <th><b>Class Name<b></th>
                                                    <th><b>Subject Name<b></th>
                                                    <th><b>Request Date<b></th>
                                                    <!-- <th><b>Status<b></th>   -->
                                                    <th><b>Approval</b></th>
                                                    <!-- <th><b>Remove<b></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($demo_list as $dlist)
                                                    <tr id="row_{{ $dlist->id }}">
                                                
                                                    
                                                    <td>{{ $dlist->std_name }}</td>
                                                    <td>{{ $dlist->std_mobile }}</td>
                                                    <td>{{ $dlist->prog_name }}</td>
                                                    <td>{{ $dlist->progdetails['class_name'] }}</td>
                                                    <td>{{ $dlist->progdetails['subject_name'] }}</td>
                                                    <td>{{ $dlist->req_date }}</td>

                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $dlist->status == 1)
                                                                <input type="checkbox" id="status_{{ $dlist->id }}" onChange="change_status('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')" checked="checked">
                                                            @elseif($dlist->status == 0)
                                                                <input type="checkbox" id="status_{{ $dlist->id }}" onChange="change_status('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->

                                                    <td id="demo_{{ $dlist->id }}" >
           
                                                    @if( $dlist->demo == 1)
                                                        <button type="button" class="btn btn-success btn-sm">Accepted</button>
                                                    @elseif($dlist->demo == 2)
                                                        <!-- <button type="button" class="btn btn-danger">Declined</button>     -->
                                                    @else
                                                        <button type="button" class="btn btn-danger btn-sm"  onclick="change_demo('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')" >Pendding</button>
                                                            
                                                        <!-- <button type="button" class="btn btn-danger"  onclick="change_demo('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">Decline</button>        -->
                                                    @endif
                                                    </td>


                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $dlist->demo == 1)
                                                                <input type="checkbox" id="demo_{{ $dlist->id }}" onChange="change_demo('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')" checked="checked">
                                                            @elseif($dlist->demo == 0)
                                                                <input type="checkbox" id="demo_{{ $dlist->id }}" onChange="change_demo('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->

                                                

                                                    <!-- <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $dlist->id }}','{{ base64_encode($dlist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td> -->

                                                
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_institute_demo/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Demo status enabled Successfully', 500);
                 }else{
                     error('Unable to update Demo status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_institute_demo/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Demo status disabled Successfully', 500);
                 }else{
                     error('Unable to update Demo status', 500);
                 }
             }
         })
    }    
}


function change_demo(id,data){
    if($("#demo_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_book_institute_demo/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success(' Demo enabled Successfully', 500);
                 }else{
                     error('Unable to update demo', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_book_institute_demo/'+data+'/2',
             success:function(data){
                 if(data == 1){
                     success('Demo enabled Successfully', 500);
                 }else{
                     error('Unable to update demo', 500);
                 }
             }
         })
    }    
}

function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Program?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_program_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Program Deleted Successfully', 500);
                             var table = $('#tbleTeacherList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Program', 500);
                         }
                     }
                })
            }            
        })

}
</script>
<!-- Modal -->
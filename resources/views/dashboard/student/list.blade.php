
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Student's List</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleStudentList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Image</b></th>
                                                    <th><b>Name</b></th>
                                                    <th><b>Email</b></th>
                                                    <th><b>Mobile</b></th>
                                                    <th><b>Gender</b></th> 
                                                    <!-- <th><b>Program name</b></th> 
                                                    <th><b>Batch name</b></th>                                                     -->
                                        
                                                    <th><b>View/Edit</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($student_list as $slist)
                                                <tr id="row_{{ $slist->id }}">
                                                    <td>
                                                   <?php $url = $slist->image ;?>
                                                    @if ((!(substr($url, 0, 7) == 'http://')) && (!(substr($url, 0, 8) == 'https://')))
                                                        <?php $return =  url('/')."/".$slist->image;?> 
                                                    @else
                                                        <?php $return = $slist->image;?> 
                                                    @endif
                                                        <img src="{{ $return }}" alt="{{ $slist->username }} Logo" style="height: 50px;width: 50px;">
                                                    </td>
                                                    <td>{{ $slist->first_name }}</td>
                                                    <td>{{ $slist->email }}</td>
                                                    <td>{{ $slist->mobile }}</td>
                                                    <td>{{ $slist->gender }}</td>
                                                    <!-- <td>{{ $slist->batchdetails['program_name'] }}</td>
                                                    <td>{{ $slist->batchdetails['title'] }}</td> -->
                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $slist->status == 1)
                                                                <input type="checkbox" id="status_{{ $slist->id }}" onChange="change_status('{{ $slist->id }}','{{ base64_encode($slist->id) }}')" checked="checked">
                                                            @elseif($slist->status == 0)
                                                                <input type="checkbox" id="status_{{ $slist->id }}" onChange="change_status('{{ $slist->id }}','{{ base64_encode($slist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->
                                                    <!-- <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $slist->id }}','{{ base64_encode($slist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td> -->
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/student/edit/{{base64_encode($slist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleStudentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  



function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_student_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Student status enabled Successfully', 500);
                 }else{
                     error('Unable to update Student status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_student_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Student status disabled Successfully', 500);
                 }else{
                     error('Unable to update Student status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Student?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_student_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Student Deleted Successfully', 500);
                             var table = $('#tbleStudentList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Student', 500);
                         }
                     }
                })
            }            
        })

}
</script>
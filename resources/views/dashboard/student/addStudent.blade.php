
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/student/add_new_student')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-8">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Student Info</h4>
                                                </div>
                                                    <input type="hidden" name="std_id" id="std_id" value="0">
                                                 <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Mobile Number<span style="color: red;">*</label>
                                                    <input type="number" class="form-control" id="mobile" name="mobile"  max="9999999999" onfocusout="onCheck(this.value);" required>
                                                </div>
                                                <!-- <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Password</label>
                                                    <input type="text" class="form-control" id="" name="password" required>
                                                </div> -->
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">First Name<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="first_name" name="first_name" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Last Name</label>
                                                    <input type="text" class="form-control" id="last_name" name="last_name" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Email<span style="color: red;">*</label>
                                                    <input type="email" class="form-control" id="email" name="email" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Gender<span style="color: red;">*</label>
                                                    <select name="gender" id="gender" class="form-control" required >
                                                        <option selected disabled>Select Gender</option>
                                                        <option value="Male">Male</option>
                                                        <option value="FeMale">Female</option>
                                                    </select>
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Address1<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="address" name="address" required>
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Address2</label>
                                                    <input type="text" class="form-control" id="address2" name="address2" >
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">City<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="city" name="city" required>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">State<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="state" name="state" required>
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Zipcode<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="zip" name="zip" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Mother Name</label>
                                                    <input type="text" class="form-control" id="mother_name" name="mother_name" >
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Father Name</label>
                                                    <input type="text" class="form-control" id="father_name" name="father_name" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Parent Number</label>
                                                    <input type="number" class="form-control"  max="9999999999" id="parent_mob" name="parent_mob" >
                                                </div>
                                                 <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Language Known<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="language" name="language" required >
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating"> Date of Joining<span style="color: red;">*</label>
                                                    <input type="text" class="form-control" id="join_date" name="date_join" required >
                                                </div>

                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h4 class="title">Student Profile Image</h4>                                           
                                            <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail">
                                                    <img id="img_show" src="https://taktii.sgp1.digitaloceanspaces.com/static/300x200.png" alt="...">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="" name="image_exist" id="image_exist">
                                                        <input type="file" name="image" >
                                                    <div class="ripple-container"></div></span>
                                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> Remove<div class="ripple-container">
                                                    <div class="ripple-decorator ripple-on ripple-out" style="left: 11.9063px; top: 32.5px; background-color: rgb(255, 255, 255); transform: scale(15.623);">
                                                    </div></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})

function onCheck(number){
   
    if(number != ''){
        loaderStart('Validating Number..');
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_number/'+number,
            success:function(data){
                console.log('data>>>>',data)
                loaderEnd();
                if(data.id){
                  $('#std_id').val(data.id);
                  var name = data.first_name
                  var name2 = name.split(' ');
                  //console.log(name2.length);
                  var fname = name2[0];
                  var lname = name2[1];
                  for(i=2;i<name2.length;i++){
                    lname += " "+name2[i];
                    //console.log(name2[i]);
                  }
                  $('#first_name').val(fname);
                  $('#last_name').val(lname);
                  $('#email').val(data.email);
                  $('#gender').val(data.gender);
                  $('#parent_mob').val(data.parent_mob);
                  $('#image_exist').val("https://taktii.com/etc/api/profile_image/"+data.profile_pic);
                  $('#img_show').attr("src","https://taktii.com/etc/api/profile_image/"+data.profile_pic);
                  $('#address').val(data.address);
                  $('#address2').val(data.address2);
                  $('#city').val(data.city);
                  $('#state').val(data.state);
                  $('#zip').val(data.zip);
                  $('#mother_name').val(data.mother_name);
                  $('#father_name').val(data.father_name);
                }else{
                    $(`#first_name,
                     #last_name,
                     #email,
                     #gender,
                     #parent_mob,
                     #address,
                     #address2,
                     #city,                   
                     #state,                   
                     #zip,                   
                     #mother_name,                   
                     #father_name                 
                     `).val('');
                    $('#img_show').attr("src","https://taktii.sgp1.digitaloceanspaces.com/static/300x200.png");
                    $('#std_id').val('0');
                    $('#image_exist').val('');
                }
            }
        })
    }
}

function getClassesFromCourse(id){
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            $('#setClasses').html(data);
        }
    })
   
}

function getSubjectsFromClass(id){
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            $('#setSubjects').html(data);
        }
    })

}

$(function(){
    $('#join_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})
</script>
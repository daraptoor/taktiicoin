
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                        <form method="post" action="{{URL::to('/student/assign_student')}}" enctype="multipart/form-data">
                                         @csrf
                                            <div class="form-group bmd-form-group col-md-5"  >
                                                <label for="std_id" class="bmd-label-floating">Student</label>
                                                <select name="std_id" class="form-control" onChange="get_batches(this.value)" required >
                                                    <option selected disabled>Select Student</option>
                                                    @foreach($student_list as $slist)
                                                    <option value="{{$slist->std_id}}" id="student_{{$slist->std_id}}" data-name="{{$slist->first_name}} {{$slist->last_name}}">{{$slist->first_name}} {{$slist->last_name}} ( {{$slist->mobile}} )</option>
                                                    @endforeach
                                                </select>                                                 
                                            </div>

                                            <div class="form-group bmd-form-group col-md-5" >
                                                <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                <select name="batch_id" class="form-control" onChange="set_batch(this.value)" required>
                                                    <option selected disabled>Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}">{{$blist->title}}    ___   {{$blist->program_name}}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" value="" id="batch_name" name="batch_name"> 
                                            </div>
                                            <div class="form-group bmd-form-group col-md-2" >
                                                <button type="submit" class="btn btn-primary" >Assign</button>
                                            </div>
                                        </form>
                                        <div class="col-md-12" id="get_data">
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function get_batch_students(){
    loaderStart('Loading Students...');
}
function set_batch(idss){
    $('#batch_name').val($('#batch_'+idss).data('name'));
}
function get_batches(idss){
    var name = $('#student_'+idss).data('name');
    loaderStart('Loading Students...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_batches_rel/'+idss+'/'+name,
        success:function(data){
            if(data){
                $('#get_data').html(data);  
                loaderEnd();
            }else{
                
            }
        }
    })   
}
</script>
<!-- Modal -->
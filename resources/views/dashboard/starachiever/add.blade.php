<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/starachiever/add_achiever')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">

                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Achiever Name</label>
                                                    <input type="text" class="form-control" name="achiever_name" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Achievement</label>
                                                    <input type="text" class="form-control" name="achievement" required>
                                                </div>

                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                <h4 class="title">Achiever Image</h4>                                           
                                                <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail">
                                                        <img src="https://taktii.sgp1.digitaloceanspaces.com/static/300x200.png" alt="...">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                    <div>
                                                        <span class="btn btn-rose btn-round btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="hidden" value="" name=""><input type="file" name="image" required>
                                                        <div class="ripple-container"></div></span>
                                                        <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                        <i class="fa fa-times"></i> Remove<div class="ripple-container">
                                                        <div class="ripple-decorator ripple-on ripple-out" style="left: 11.9063px; top: 32.5px; background-color: rgb(255, 255, 255); transform: scale(15.623);">
                                                        </div></div></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                       
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Update</button>
                                            </div>
                                        </div>

                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})




</script>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/batch/update_batch')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Batch Info</h4>
                                                    <input type="hidden" name="b_id" value="{{ $batch_data->id }}">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Name</label>
                                                    <input type="text" class="form-control" id="" name="title" value="{{$batch_data->title}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Assign Program for Batch</label>
                                                    <select class="form-control" onChange="set_program(this.value)" id="program_id" name="program_id" required>
                                                        <option value="" selected disabled>Select Program</option>
                                                        @foreach($program_list as $pl)
                                                        <option id="prog_{{$pl->id}}" value="{{ base64_encode($pl->id) }}" data-name="{{ $pl->prog_name }}"
                                                        @if( $batch_data->program_id   ==  $pl->id )
                                                        selected
                                                     @endif
                                                        >{{ $pl->prog_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_name" id="prog_name" value="{{$batch_data->program_name}}">
                                                </div>
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Batch Start</label>
                                                    <input type="text" class="form-control datepicker" id="batch_start" name="batch_start"  value="{{$batch_data->batch_start}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Batch End</label>
                                                    <input type="text" class="form-control datepicker" id="batch_end" name="batch_end" value="{{$batch_data->batch_end}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time</label>
                                                    <input type="text" class="form-control" id="batch_stime" name="batch_stime" value="{{$batch_data->batch_stime}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time</label>
                                                    <input type="text" class="form-control" id="batch_etime" name="batch_etime" value="{{$batch_data->batch_etime}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Assign teacher for Batch</label>
                                                   
                                                    <select class="form-control" onChange="set_teacher(this.value)" id="teacher_id" name="teacher_id" required>
                                                    <option value="" selected disabled>Select Teacher</option>
                                                        @foreach($teachers_list as $tl)
                                                        <option id="teacher_{{$tl->id}}" value="{{ base64_encode($tl->id) }}" data-name="{{ $tl->first_name }} {{ $tl->last_name }}"
                                                        @if( $batch_data->teacher_id   ==  $tl->id )
                                                        selected
                                                     @endif
                                                        >{{ $tl->first_name }} {{ $tl->last_name }} ({{ $tl->mobile }})</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="teacher_name" id="teacher_name" value="{{$batch_data->teacher_name}}">
                                                </div>
                                        </div>
                                        <div class="col-sm-3 checkbox-radios">         
                                            <div class="form-group bmd-form-group col-md-12">
                                                <h4>Batch Days</h4>
                                                <?php 
                                                        $days = explode(',',$batch_data->batch_days);
                                                        
                                                    
                                                    ?>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Sunday" {{(in_array('Sunday',$days))?"checked":""}}>
                                                        Sunday
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox"  value="Monday" {{(in_array('Monday',$days))?"checked":""}}>
                                                        Monday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Tuesday" {{(in_array('Tuesday',$days))?"checked":""}}>
                                                        Tuesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Wedenesday" {{(in_array('Wedenesday',$days))?"checked":""}}>
                                                        Wedenesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox"  value="Thursday" {{(in_array('Thursday',$days))?"checked":""}}>
                                                        Thursday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Friday" {{(in_array('Friday',$days))?"checked":""}}>
                                                        Friday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Saturday" {{(in_array('Saturday',$days))?"checked":""}}>
                                                        Saturday
                                                    </label>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('#batch_start, #batch_end').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#batch_stime, #batch_etime').datetimepicker({
        format: 'LT'
    });

})

function set_program(id){
    var decodedString = atob(id);
    $('#prog_name').val($('#prog_'+decodedString).data('name'));
}
function set_teacher(id){
    var decodedString = atob(id);
    $('#teacher_name').val($('#teacher_'+decodedString).data('name'));
}
</script>
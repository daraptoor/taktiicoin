<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleStudentList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>SNO</b></th>
                                                    
                                                    <th><b>Name</b></th>
                                                    <th><b>Email</b></th>
                                                    <th><b>Mobile</b></th>
                                                    <th><b>Gender</b></th>   
                                                    <th><b>SMS</b></th>                                                 
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $count=1;?>
                                              @foreach($std_list as $inlist)
                                                <tr id="row_{{ $inlist->id }}">
                                                    <td>{{ $count++ }}</td>
                                                  
                                                    <td>{{ $inlist->students['first_name'] }}</td>
                                                    <td>{{ $inlist->students['email'] }}</td>
                                                    <td>{{ $inlist->students['mobile'] }}</td>
                                                    <td>{{ $inlist->students['gender'] }}</td>
                                                    <td>
                                                     <button type="button" class="btn btn-primary btn-round btn-fab" onClick="showSmsModal('{{ $inlist->students['mobile'] }}')" style="height:40px;width:40px;min-width: 40px;">
                                                        <i class="material-icons">textsms</i>
                                                     </button>
                                                    </td>
                                                   
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            
                                               
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <!-- SMS Modal -->
  <div class="modal fade" id="showSmsModal" role="dialog">
  <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Send Message</h4>
        </div>
        <div class="modal-body">
        <div class="form-group">
            <label for="sel1">Choose mobile number</label>
            <select class="form-control" id="mobile_no" name="mobile_no">
              <option value="9560610340" id="num_1">9560610340</option>
            </select>
          </div>
          <div class="form-group">
            <label for="comment">Message:</label>
            <textarea class="form-control" rows="5" id="message" name="message"></textarea>
          </div>
          <button type="button" class="btn btn-primary" onClick="sendSms()">Submit</button>
        </div>
      </div>
      
    </div>
  </div>
<script>

$(function(){
    dt('tbleStudentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  


function showSmsModal(num1){
    $('#num_1').val(num1).text(num1+" (Student Number)");
    $('#showSmsModal').modal('show');
}

function sendSms(){
var mob_number = document.getElementById("mobile_no").value;
var message =  document.getElementById("message").value;
    if(mob_number!='' && message!='' && mob_number != '0000'){
        var sms = sendSMS(mob_number,message);
        success(`SMS to ${mob_number} sent Successfully...`);
    }else{
        error(`Unable to SMS...`);
    }
}
</script>


            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                    <th><b>Batch Name</b></th>
                                                    <th><b>Prog Name</b></th>
                                                    <th><b>Start Date</b></th>
                                                    <th><b>End Date</b></th>
                                                    <th><b>Days</b></th>
                                                    <th><b>Time</b></th>
                                                    <th><b>Teacher</b></th>
                                                    <!-- <th><b>Status</b></th> -->
                                                    <!-- <th><b>Remove</b></th> -->
                                                    <th><b>View/Edit</b></th>
                                                    <th><b>join student</b></th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($batch_list as $blist)
                                                <tr id="row_{{ $blist->id }}">
                                                    
                                                    <td>{{ $blist->title }}</td>
                                                    <td>{{ $blist->program_name }}</td>
                                                    <td>{{ $blist->batch_start }}</td>
                                                    <td>{{ $blist->batch_end }}</td>
                                                    <td><a href="#" data-toggle="tooltip" data-placement="right" title="{{ str_replace(",",", ",$blist->batch_days) }}"> {{ substr_count( str_replace(",",", ",$blist->batch_days),', ')+1 }} Days</a></td>
                                                    <td>{{ $blist->batch_stime }}-{{ $blist->batch_etime }}</td>
                                                    <td>{{ $blist->teacher_name }}</td>
                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $blist->status == 1)
                                                                <input type="checkbox" id="status_{{ $blist->id }}" onChange="change_status('{{ $blist->id }}','{{ base64_encode($blist->id) }}')" checked="checked">
                                                            @elseif($blist->status == 0)
                                                                <input type="checkbox" id="status_{{ $blist->id }}" onChange="change_status('{{ $blist->id }}','{{ base64_encode($blist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->
                                                    <!-- <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $blist->id }}','{{ base64_encode($blist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td> -->
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/batch/edit/{{base64_encode($blist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>

                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/batch/joinstudent/{{base64_encode($blist->id)}}">
                                                        <i class="material-icons">visibility</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})


function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Batch status enabled Successfully', 500);
                 }else{
                     error('Unable to update Batch status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Batch status disabled Successfully', 500);
                 }else{
                     error('Unable to update Batch status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Batch?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Batch Deleted Successfully', 500);
                             var table = $('#tbleTeacherList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Batch', 500);
                         }
                     }
                })
            }            
        })

}
</script>
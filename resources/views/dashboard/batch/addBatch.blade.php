
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/batch/add_new_batch')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Batch Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Name<span style="color: red;">* </span> </label>
                                                    <input type="text" class="form-control" id="" name="title" required>
                                                   
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Assign Program for Batch<span style="color: red;">* </span></label>
                                                    <select class="form-control" onChange="set_program(this.value)" id="program_id" name="program_id" required>
                                                        <option value="" selected disabled>Select Program</option>
                                                        @foreach($program_list as $pl)
                                                        <option id="prog_{{$pl->id}}" value="{{ base64_encode($pl->id) }}" data-name="{{ $pl->prog_name }}">{{ $pl->prog_name }}   __   {{ $pl->class_name }}    __  {{ $pl->subject_name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_name" id="prog_name" value="">
                                                </div>
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Batch Start<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="batch_start" name="batch_start" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Batch End<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="batch_end" name="batch_end" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="batch_stime" name="batch_stime" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="batch_etime" name="batch_etime" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <label for="exampleUsername" class="bmd-label-floating">Assign teacher for Batch<span style="color: red;">* </span></label>
                                                   
                                                    <select class="form-control" onChange="set_teacher(this.value)" id="teacher_id" name="teacher_id" required>
                                                    <option value="" selected disabled>Select Teacher</option>
                                                        @foreach($teachers_list as $tl)
                                                        <option id="teacher_{{$tl->id}}" value="{{ base64_encode($tl->id) }}" data-name="{{ $tl->first_name }} {{ $tl->last_name }}">{{ $tl->first_name }} {{ $tl->last_name }} ({{ $tl->mobile }})</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="teacher_name" id="teacher_name" value="">
                                                </div>
                                        </div>
                                        <div class="col-sm-3 checkbox-radios">         
                                            <div class="form-group bmd-form-group col-md-12">
                                            <label><h4>Batch Days<span style="color: red;">* </span></h4></label>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Sunday" checked="">
                                                        Sunday
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Monday" checked="">
                                                        Monday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Tuesday" checked="">
                                                        Tuesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Wedenesday" checked="">
                                                        Wedenesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Thursday" checked="">
                                                        Thursday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Friday" checked="">
                                                        Friday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="batch_days[]" type="checkbox" value="Saturday" checked="">
                                                        Saturday
                                                    </label>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 <script>
  $(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('#batch_start,#batch_end').datetimepicker({
        format: 'DD-MM-YYYY'
 
    });

     $('#batch_stime, #batch_etime').datetimepicker({
        format: 'LT'
   });

 })

</script>




 <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js" type="text/javascript"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
 type="text/javascript"></script>
 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css"
 rel="Stylesheet"type="text/css"/>
<script type="text/javascript">
$(function () {
    $("#startdate").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#enddate").datepicker("option", "minDate", dt);
        }
    });
    $("#enddate").datepicker({
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#startdate").datepicker("option", "maxDate", dt);
        }
    });
    
}); -->

<script>

function set_program(id){
    var decodedString = atob(id);
    $('#prog_name').val($('#prog_'+decodedString).data('name'));
}
function set_teacher(id){
    var decodedString = atob(id);
    $('#teacher_name').val($('#teacher_'+decodedString).data('name'));
}
</script>




            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{$title}}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/teacher/update_teacher')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-8">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Teacher Info</h4>
                                                    <input type="hidden" name="tea_id" value="{{ $teacher_data->id }}">

                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Number</label>
                                                    <input type="number" class="form-control" id="" name="number"  onfocusout="onCheck(this.value);" value="{{$teacher_data->mobile}}" required>
                                                </div>
                                                <!-- <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Password</label>
                                                    <input type="text" class="form-control" id="" name="password"  >
                                                </div> -->
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">First Name</label>
                                                    <input type="text" class="form-control" id="" name="first_name" value ="{{$teacher_data->first_name}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Last Name</label>
                                                    <input type="text" class="form-control" id="" name="last_name" value ="{{$teacher_data->last_name}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Email</label>
                                                    <input type="email" class="form-control" id="" name="email" value ="{{$teacher_data->email}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Teacher Status Type</label>
                                                    <select class="form-control" name="teacher_type">
                                                        <option disabled selected>Select Teacher Status </option>
                                                        <option value="Permanent"
                                                        @if( $teacher_data->teacher_type   ==  "Permanent" )
                                                                selected
                                                            @endif
                                                         >Permanent</option>
                                                         <option value="Training" 
                                                         @if( $teacher_data->teacher_type   ==  "Training" )
                                                                selected
                                                            @endif >Training</option>
                                                        <option value="Ad-hoc" 
                                                         @if( $teacher_data->teacher_type   ==  "Ad-hoc" )
                                                                selected
                                                            @endif >Ad-hoc</option>
                                                        <option value="Guest"
                                                         @if( $teacher_data->teacher_type   ==  "Guest" )
                                                                selected
                                                            @endif 
                                                            >Guest</option>
                                                        <option value="Master Teacher"
                                                        @if( $teacher_data->teacher_type   ==  "Master Teacher" )
                                                                selected
                                                            @endif
                                                         >Master Teacher</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Teacher Salary</label>
                                                    <input type="text" class="form-control" id="" name="teacher_sal" value ="{{$teacher_data->teacher_salary}}" required>
                                                </div>

                                                <!-- <div class="form-group bmd-form-group col-md-4">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="prog_courses" name="prog_courses">
                                                        @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}" 
                                                        @if( $teacher_data->course_id == $cl->id )
                                                                selected
                                                            @endif
                                                            >{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_courses_name" value="{{$teacher_data->course_name}}" id="prog_course_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="prog_class">
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="prog_class_name" value="{{$teacher_data->course_name}}" id="prog_class_name">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4" >
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="prog_subject" >
                                                        <option disabled selected> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="prog_subject_name" value="{{$teacher_data->course_name}}" id="prog_subject_name">
                                                </div> -->

                                                
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Address</label>
                                                    <input type="text" class="form-control" id="" name="address" value ="{{$teacher_data->address}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">joining date</label>
                                                    <input type="text" class="form-control" id="" name="join_date" value ="{{$teacher_data->join_date}}" required>
                                                </div>
                                                

                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h4 class="title">Teacher Profile</h4>                                           
                                            <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail">
                                                <img src="{{url('/')}}/{{ $teacher_data->image }}" alt="...">                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="{{ $teacher_data->image }}" name="image_exist"><input type="file" name="image" >
                                                    <div class="ripple-container"></div></span>
                                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> Remove<div class="ripple-container">
                                                    <div class="ripple-decorator ripple-on ripple-out" style="left: 11.9063px; top: 32.5px; background-color: rgb(255, 255, 255); transform: scale(15.623);">
                                                    </div></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>

$(function(){ 
    getClassesFromCourse('{{ base64_encode($teacher_data->course_id)}}','{{ base64_encode($teacher_data->class_id)}}');
    getSubjectsFromClass('{{ base64_encode($teacher_data->class_id)}}','{{ base64_encode($teacher_data->subject_id)}}');

})

function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

function onCheck(name){
    if(name != ''){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_username/'+name,
            success:function(data){
                if(data > 0){
                    $('#class_username').addClass('has-error');
                    $('#submit_box').hide();

                    swal({
                    title: 'Oops...',
                    text: "Username already Exist. Please use diffrent username.",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ok'
                    }).then((result) => {
                        $("#exampleUsername").focus();
                    })
                  
                }else{
                    $('#class_username').removeClass('has-error');
                    $('#submit_box').show();
                }
            }
        })
    }
}

function getClassesFromCourse(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_classes_from_course/'+id+'/'+selected,
        success:function(data){
            $('#setClasses').html(data);
            
        }
    })
    var decodedString = atob(id);
   // alert($('#crs_'+decodedString).data('name'));
    $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_subject_from_class/'+id+'/'+selected,
        success:function(data){
            $('#setSubjects').html(data);            
        }
    })
   
}
function getClassesFromCourse2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
    loaderEnd();
}

function getSubjectsFromClass2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            $('#setSubjects').html(data);
            loaderEnd();
        }
    })
    var decodedString = atob(id);
    $('#prog_class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#prog_subject_name').val($('#sub_'+decodedString).data('name'));
}
</script>
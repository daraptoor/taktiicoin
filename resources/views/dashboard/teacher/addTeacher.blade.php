            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/teacher/add_new_teacher')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-8">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Teacher Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="teachNumber" class="bmd-label-floating">Mobile Number<span style="color: red;">* </span></label>
                                                    <input type="number" class="form-control"  id="teachNumber"  max="9999999999" name="number" onfocusout="onCheck(this.value);" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Email<span style="color: red;">* </span></label>
                                                    <input type="email" class="form-control" id="" name="email" required>
                                                </div>
                                                <!-- <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Password (Auto Generated)</label>
                                                    <input type="text" class="form-control" id="" name="password" value="{{str_random(10)}}" readonly required>
                                                </div> -->
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">First Name<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="" name="first_name" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Last Name</label>
                                                    <input type="text" class="form-control" id="" name="last_name" >
                                                </div>
                                                

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Teacher Status Type<span style="color: red;">* </span></label>
                                                    <select class="form-control" name="teacher_type">
                                                        <option disabled selected>Select Teacher Status </option>
                                                        <option value="Permanent" >Permanent</option>
                                                        <option value="Training" >Training</option>
                                                        <option value="Ad-hoc" >Ad-hoc</option>
                                                        <option value="Guest" >Guest</option>
                                                        <option value="Master Teacher" >Master Teacher</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Gender<span style="color: red;">* </span></label>
                                                    <select class="form-control" name="gender">
                                                        <option disabled selected>Select gender </option>
                                                        <option value="Female" >Female</option>
                                                        <option value="Male" >Male</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Teacher Salary</label>
                                                    <input type="number" class="form-control" id="" name="teacher_sal" >
                                                </div>

                                               <!-- <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="prog_courses" name="prog_courses" required>
                                                    <option disabled selected>Select Course</option>
                                                        @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="prog_course_name" value="" id="prog_course_name">
                                                </div> 

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="prog_class" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="prog_class_name" value="" id="prog_class_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="prog_subject[]" multiple >
                                                        <option disabled selected> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="prog_subject_name" value="" id="prog_subject_name">
                                                </div>-->

                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Teacher Experience<span style="color: red;">* </span></label>
                                                    <select class="form-control" name="experience">
                                                        <option disabled selected>Select Teacher Experience</option>
                                                        <option value="1 year of experience" >1 year of experience</option>
                                                        <option value="2 year of experience" >2 year of experience</option>
                                                        <option value="3 year of experience" >3 year of experience</option>
                                                        <option value="4 year of experience" >4 year of experience</option>
                                                        <option value="5 year of experience" >5 year of experience</option>
                                                        <option value="6 year of experience" >6 year of experience</option>
                                                        <option value="7 year of experience" >7 year of experience</option>
                                                        <option value="8 year of experience" >8 year of experience</option>
                                                        <option value="9 year of experience" >9 year of experience</option>
                                                        <option value="10 year of experience" >10 year of experience</option>
                                                        <option value="10 + year of experience" >10 + year of experience</option>
                                                        
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">Address<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control"  name="address"  required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-4">
                                                    <label for="examplePass" class="bmd-label-floating">joining date<span style="color: red;">* </span></label>
                                                    <input type="text" class="form-control" id="join_date" name="join_date" required>
                                                </div>

                                                <!-- <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Introductory  Video(Optional)</label>
                                                    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
                                                    <p class="help-block">Example block-level help text here.</p>
                                                </div>
                                                 -->

                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h4 class="title">Teacher Profile</h4>                                           
                                            <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail">
                                                    <img src="https://taktii.sgp1.digitaloceanspaces.com/static/300x200.png" alt="...">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                                <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="hidden" value="" name="image_exist"><input type="file" name="image" >
                                                    <div class="ripple-container"></div></span>
                                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput">
                                                    <i class="fa fa-times"></i> Remove<div class="ripple-container">
                                                    <div class="ripple-decorator ripple-on ripple-out" style="left: 11.9063px; top: 32.5px; background-color: rgb(255, 255, 255); transform: scale(15.623);">
                                                    </div></div></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkmRzgPT_YinELO77cj-Sju5IsqUrfrn0&sensor=false&libraries=places"
async defer></script>
<script>
function initializeAutocomplete(){
var input = document.getElementById('locality');

var options = {}

var autocomplete = new google.maps.places.Autocomplete(input, options);

google.maps.event.addListener(autocomplete, 'place_changed', function() {
var place = autocomplete.getPlace();
var lat = place.geometry.location.lat();
var lng = place.geometry.location.lng();
var placeId = place.place_id;
// to set city name, using the locality param
var componentForm = {
locality: 'short_name',
};
for (var i = 0; i < place.address_components.length; i++) {
var addressType = place.address_components[i].types[0];
if (componentForm[addressType]) {
var val = place.address_components[i][componentForm[addressType]];
document.getElementById("city").value = val;
}
}
document.getElementById("latitude").value = lat;
document.getElementById("longitude").value = lng;
document.getElementById("location_id").value = placeId;
});
}


$(function(){
    $('#join_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}


function onCheck(name){
    if(name != ''){
        $.ajax({
            type:'get',
            url:'{{ url('/')}}/ajax/chk_exist_teacher/'+name,
            success:function(data){
                if(data > 0){
                    $('#class_username').addClass('has-error');
                    $('#submit_box').hide();

                    swal({
                    title: 'Oops...',
                    text: "Teacher already Exist in your institute. Please use diffrent number or you can update teacher profile.",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'ok'
                    }).then((result) => {
                        $("#teachNumber").focus();
                    })
                  
                }else{
                    $('#class_username').removeClass('has-error');
                    $('#submit_box').show();
                }
            }
        })
    }
}

// function getClassesFromCourse(id){
//     loaderStart('Loading...');
//     $.ajax({
//         type:'get',
//         url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
//         success:function(data){
//             loaderEnd();
//             $('#setClasses').html(data);
//         }
//     })
//     var decodedString = atob(id);
//     $('#prog_course_name').val($('#crs_'+decodedString).data('name'));
// }

// function getSubjectsFromClass(id){
//     loaderStart('Loading...');
//     $.ajax({
//         type:'get',
//         url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
//         success:function(data){
//             loaderEnd();
//             $('#setSubjects').html(data);
//         }
//     })
//     var decodedString = atob(id);
//     $('#prog_class_name').val($('#cls_'+decodedString).data('name'));
// }


// function set_Subjects(id){
//     var decodedString = atob(id);
//     $('#prog_subject_name').val($('#sub_'+decodedString).data('name'));
// }
</script>
<!-- Modal -->
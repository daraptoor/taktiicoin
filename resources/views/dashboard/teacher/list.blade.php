
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Teacher's List</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Sno</b></th>
                                                    <th><b>Profile Pics</b></th>
                                                    <th><b>Name</b></th>
                                                    <th><b>Mobile</b></th>
                                                    <th><b>Email</b></th>
                                                    <th><b>Gender</b></th>
                                                    <th><b>type</b></th>
                                                    <!-- <th><b>Status</b></th> -->
                                                    <!-- <th><b>Remove</b></th> -->
                                                    <th><b>View/Edit</b></th>
                                                    <th><b>Pay Teacher</b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($teachers_list as $tlist)
                                                <tr id="row_{{ $tlist->id }}">
                                                    <td>{{ $no++ }}</td>
                                                    <td><img src="{{ url('/') }}/{{ $tlist->image }}" alt="{{ $tlist->first_name }} Logo" style="height: 50px;width: 50px;"></td>
                                                    <td>{{ $tlist->first_name }} {{ $tlist->last_name }}</td>
                                                    <td>{{ $tlist->mobile }}</td>
                                                    <td>{{ $tlist->email }}</td>
                                                    <td>{{ $tlist->gender }}</td>
                                                    <td>{{ $tlist->teacher_type }}</td>
                                                    <!-- <td>
                                                        <div class="togglebutton">
                                                            <label>
                                                            @if( $tlist->status == 1)
                                                                <input type="checkbox" id="status_{{ $tlist->id }}" onChange="change_status('{{ $tlist->id }}','{{ base64_encode($tlist->id) }}')" checked="checked">
                                                            @elseif($tlist->status == 0)
                                                                <input type="checkbox" id="status_{{ $tlist->id }}" onChange="change_status('{{ $tlist->id }}','{{ base64_encode($tlist->id) }}')">
                                                            @endif                                                                 
                                                            </label>
                                                        </div>  
                                                    </td> -->
                                                    <!-- <td><button class="btn btn-danger btn-sm" onClick="delete_admin('{{ $tlist->id }}','{{ base64_encode($tlist->id) }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td> -->
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teacher/edit/{{base64_encode($tlist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teacher/pay/{{base64_encode($tlist->id)}}">
                                                        <i class="material-icons">add</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})


function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_teacher_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Teacher status enabled Successfully', 500);
                 }else{
                     error('Unable to update Teacher status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_teacher_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Teacher status disabled Successfully', 500);
                 }else{
                     error('Unable to update Teacher status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Teacher?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_teacher_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Teacher Deleted Successfully', 500);
                             var table = $('#tbleTeacherList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Teacher', 500);
                         }
                     }
                })
            }            
        })

}
</script>
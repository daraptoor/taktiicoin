<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/teacher/add_pay_teacher')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-8">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Pay Teacher Info</h4>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <input type="hidden" class="form-control" id="teacher_id" name="teacher_id" value="{{$teacher_id}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Number</label>
                                                    <input type="text" class="form-control" id="" name="number" onfocusout="onCheck(this.value);" value="{{$teacher_data->mobile}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="amount" class="bmd-label-floating">Amount</label>
                                                    <input type="text" class="form-control" id="amount" name="amount" required>
                                                </div>

                                                <div class="form-group bmd-form-group  col-md-3">
                                                    <label for="exampleEmail" class="bmd-label-floating">Paid Date</label>
                                                    <input type="text" class="form-control datepicker" id="paid_date" name="paid_date" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-sm-3">
                                                    <div class="form-group bmd-form-group col-md-4" id="submit_box">
                                                            <button class="btn btn-success" >Submit</button>
                                                    </div>
                                                </div>
                                        </form>
                                        <h4> Monthly Pay Information</h4>

                                            <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>Amount</th>
                                                    <th>Paid Date</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($pay_info as $tlist)
                                                <tr id="row_{{ $tlist->id }}">
                                                    <td>{{ $tlist->amount }}</td>
                                                    <td>{{ $tlist->paid_date }}</td>
                
                                                   
                                                    </tr>
                                                @endforeach                                                    
                                            </tbody>
                                        </table>
                                   </div>
                                 </div>
                              </div>
                         </div>
                     </div>
                 </div>
            </div>
         </div>

     

<script>
$(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
    $('#paid_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });

})

</script>

            

<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>Sno</b></th>
                                                    <th><b>Image<b></th>
                                                    <th><b>Teacher Name<b></th>
                                                    <th><b>Batch Name<b></th>
                                                    <th><b>Heading<b></th>
                                                    <th><b>Description<b></th>
                                                    <th><b>Date<b></th>

                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                           
                                                @foreach($assignment_list as $dlist)
                                            
                                                    <tr id="row_{{ $dlist->id }}">
                                                    <td>{{ $no++ }}</td>
                                                    <td><img src="{{ url('/') }}/assigment_image/{{ $dlist->image }}" alt="{{ $dlist->name }} image" style="height: 50px;width: 50px;"></td>
                                                    <td>{{ $dlist->instteachers['first_name'] }}</td>
                                                    <td>{{ $dlist->batchinfo['title'] }}</td>
                                                    <td>{{ $dlist->name }}</td>
                                                    <td>{{ $dlist->description }}</td>
                                                    <td>{{ $dlist->date }}</td>
                                                    </tr>
                                                    
                                                @endforeach

                                              
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  



</script>
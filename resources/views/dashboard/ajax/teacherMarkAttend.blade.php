<?php
use \App\Http\Controllers\TeacherinstituteController;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form method="post" action="mark_attendence">
            <input type="hidden" name="attend_date" value="{{ $date }}">
            <input type="hidden" name="batch_id" value="{{ $batch_id }}">
            <input type="hidden" name="assistant" value="{{ $user_id }}">
            <input type="hidden" name="attendence_by" value="{{ $assign }}">
            @csrf
            <h2>Mark attendence for {{ $date }}</h2>
            <table id="" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                <thead>
                    <tr>
                        <th>Student Name</th>
                        <th>Mobile</th>
                        <th>Present</th>
                        <th>Absent</th>
                        <th>Leave</th>
                        <th>Late</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $list)
                    <tr>
                        <td>{{ $list->students['first_name']}} {{ $list->students['last_name']}}</td>
                        <td>{{ $list->students['mobile']}} 
                            <?php $chk = TeacherinstituteController::chk_attendence( $list->students['id'],$batch_id,$date );

                            ?>                        
                        </td>
                        <td ><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="1" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == '1')
                                    checked
                                @endif
                            >
                            <p class="text-success pull-right" >Present</p>
                        </label></td>
                        <td><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="2" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == '2')
                                    checked
                                @endif
                            >
                            <p class="text-danger pull-right" >Absent</p>
                        </label></td>
                        <td><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="3" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == '3')
                                    checked
                                @endif
                            >
                            <p class="text-primary pull-right" >Leave</p>
                        </label></td>
                        <td><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="4" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == '4')
                                    checked
                                @endif
                            >
                            <p class="text-warning pull-right" >Late</p>
                        </label></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                <button class="btn btn-primary btn-sm" type="submit">Submit Attendence</button>
            </form>
            
        </div>
    </div>
</div>
<script>
    $(function(){
        dt('tbleAttendList');
    })
</script>
<?php
use \App\Http\Controllers\TeacherBatchController;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form method="post" action="mark_attendence">
            <input type="hidden" name="date" value="{{ $date }}">
            <input type="hidden" name="batch_id" value="{{ $batch_id }}">
            @csrf
            <h2>Mark attendence for {{ $date }}</h2>
            <table id="" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                <thead>
                    <tr>
                        <th>Student Name</th>
                        <th>Mobile</th>
                        <th>Present</th>
                        <th>Absent</th>
                        <th>Late</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $list)
                    <tr>
                        <td>{{ $list->students['first_name']}} {{ $list->students['last_name']}}</td>
                        <td>{{ $list->students['mobile']}} 
                            <?php $chk = TeacherBatchController::chk_attendence( $list->students['id'],$batch_id,$date );

                            ?>                        
                        </td>
                        <td ><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="P" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == 'P')
                                    checked
                                @endif
                            >
                            <p class="text-success pull-right" >Present</p>
                        </label></td>
                        <td><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="A" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == 'A')
                                    checked
                                @endif
                            >
                            <p class="text-danger pull-right" >Absent</p>
                        </label></td>
                        <td><label class="form-check-label" style="cursor: pointer;">
                            <input class="form-check-input" type="radio" value="L" name="attend[{{ $list->students['id']}}]" 
                                @if(isset($chk[0]->status) && $chk[0]->status == 'L')
                                    checked
                                @endif
                            >
                            <p class="text-warning pull-right" >Late</p>
                        </label></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
                <button class="btn btn-primary btn-sm" type="submit">Submit Attendence</button>
            </form>
            
        </div>
    </div>
</div>
<script>
    $(function(){
        dt('tbleAttendList');
    })
</script>
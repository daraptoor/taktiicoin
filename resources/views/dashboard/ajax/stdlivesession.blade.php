
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><strong>Sno</strong></th>
                                                <th><strong>Teacher Name</strong></th>
                                                <th><strong>startTime</strong></th>
                                                <th><strong>endTime</strong></th>
                                                <th><strong>Date</strong></th>
                                                <th><strong>meetingID</strong></th>
                                                <th><strong>Join Meeting</strong></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $count=1;?>
                                                @foreach($live_list as $llist)
                                                <tr id="row_{{ $llist->id }}">
                                                    <td>{{ $count++ }}</td>
                                                    <td>{{ $llist['teacherName']->first_name}}</td>
                                                    <td>{{ $llist->startTime }}</td>
                                                    <td>{{ $llist->endTime }}</td>
                                                    <td>{{ $llist->liveDate }}</td>
                                                    <td>{{ $llist->meetingID }}</td>
                                                    <td id="timer_<?=$llist['id'];?>" onload="timeLeft">
                                                    <a class="btn btn-info btn-sm" target="_blank" onclick="timeLeft('{{ $llist->liveDate }}','{{ $llist->startTime }}','{{ $llist->endTime }}','{{ $llist->id }}','{{ $llist->teacher_url }}','{{ $llist->teacher_id }}','{{ $llist->meetingID }}');">Check Status</a></td>

                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script type="text/javascript">

$(document).ready(function() {

$('#myTable3').DataTable( {

// "paging":   true,

// "ordering": true,

//  "info":     true
"stateSave": true

});

});

 //timeLeft();
 function timeLeft(dat, st, et, id){
        var currentTime = moment().utc("+05:30").format('YYYY-MM-DD HH:mm:ss');
        var expectedTime = `${dat} ${st}:00`;
        var leftTime = expectedTime - currentTime;
        setInterval(function(){ 
            
            var expectedTime2 = new Date(expectedTime);
            currentTime = moment().utc("+05:30").format('YYYY-MM-DD HH:mm:ss');
            var currentTime2 = new Date(currentTime);
            var timeDiff = Math.abs(expectedTime2.getTime() - currentTime2.getTime()) / 1000;
            var days = Math.floor(timeDiff / 86400);
            timeDiff -= days * 86400;
            var hours = Math.floor(timeDiff / 3600) % 24;
            timeDiff -= hours * 3600;
            var minutes = Math.floor(timeDiff / 60) % 60;
            timeDiff -= minutes * 60;
            var seconds = timeDiff % 60;

            if(currentTime > expectedTime){
                $('#timer_'+id).html("<button class='btn btn-danger btn-sm'>Time Expired</button>");
                //console.log();
            }else{
                $('#timer_'+id).html("<button class='btn btn-success btn-sm'>"+days+" d "+hours+" h "+minutes+" m "+seconds+" s Left</button>");
                //console.log();
            }
                      
        }, 500);
    }

</script>

              

         
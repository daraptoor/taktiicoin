<?php
use \App\Http\Controllers\AttendanceController;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form method="post" action="mark_attendence">
            <!-- <input type="hidden" name="attend_date" value="{{ $date }}">
            <input type="hidden" name="batch_id" value="{{ $batch_id }}">
            <input type="hidden" name="assistant" value="{{ $user_id }}">
            <input type="hidden" name="attendence_by" value="{{ $assign }}"> -->
            @csrf
            <h2>Attendence List for {{ $date }}</h2>
            <table id="" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                <thead>
                    <tr>
                        <th><b>Student Name</b></th>
                        <th><b>Attendence Status<b></th>
                        <th><b>Attendence  By</b></th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $list)
                    <tr>
                        <td>{{ $list->students['first_name']}} {{ $list->students['last_name']}}</td>

                         <td>
                            <div class="togglebutton">
                               
                                @if( $list->status == 1)
                                Present
                        
                                @elseif($list->status == 2)
                                Absent
                                @endif                                                                 
                               
                            </div>  
                        </td> 
                       
                        <td>{{ $list->Attendence['first_name']}}</td>
                       
                       
                    </tr>
                    @endforeach
                </tbody>
            </table>
                <!-- <button class="btn btn-primary btn-sm" type="submit">Submit Attendence</button> -->
            </form>
            
        </div>
    </div>
</div>
<script>
    $(function(){
        dt('tbleAttendList');
    })
</script>
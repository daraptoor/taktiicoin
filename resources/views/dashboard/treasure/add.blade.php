
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/treasure/add_treasure')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">

                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" name="title" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File Type</label>
                                                    <select name="file_type" id="file_type" class="form-control" required>
                                                        <option value="doc">DOC</option>
                                                        <option value="img">Image</option>
                                                        <option value="video">Video</option>
                                                        <option value="pdf">Audio</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File description</label>
                                                    <textarea class="form-control" rows="3"name="file_desc" required ></textarea>
                                                </div>



                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                    <select name="batch_id" class="form-control" id="batch_id" onChange="set_batch(this.value)" required>
                                                    <option>Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}">{{$blist->title}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" value="" id="batch_name" name="batch_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount</label>
                                                    <input type="number" class="form-control" id="" name="amount" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Is file paid?</label>
                                                    <select name="is_paid" id="" class="form-control" required>
                                                        <option value="Yes">Paid</option>
                                                        <option value="no">Free</option>
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

                                                    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
                                                    <p class="help-block">Example block-level help text here.</p>
                                                </div>


                                        </div>



                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Update</button>
                                            </div>
                                        </div>

                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

function set_batch(idss){
    $('#batch_name').val($('#batch_'+idss).data('name'));
}


</script>

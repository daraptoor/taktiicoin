
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/treasure/update_treasure')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                <input type="hidden" name="main_id" value="{{ $treasure_data->id }}">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" name="title" value="{{$treasure_data -> title}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File Type</label>
                                                    <select name="file_type" id="file_type" class="form-control">
                                                        <option value="doc"
                                                         @if( $treasure_data->file_type   ==  "doc")
                                                        selected
                                                     @endif
                                                     >DOC</option>                                                    
                                                        <option value="img"
                                                        @if( $treasure_data->file_type   ==  "img")
                                                        selected
                                                     @endif
                                                        >Image</option>
                                                        <option value="video"
                                                        @if( $treasure_data->file_type   ==  "video")
                                                        selected
                                                     @endif
                                                        >Video</option>
                                                        <option value="pdf"
                                                        @if( $treasure_data->file_type   ==  "pdf")
                                                        selected
                                                     @endif
                                                        >Audio</option>                                 
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File description</label>
                                                    <textarea class="form-control" rows="3" name="file_desc" value="" required  >{{$treasure_data->file_desc}}</textarea>
                                                </div>
                                                
                                                
                                                
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                    <select name="batch_id" class="form-control" id="batch_id" onChange="set_batch(this.value)">
                                                    <option>Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}"
                                                    @if( $treasure_data->batch_id   ==  $blist->id)
                                                        selected
                                                     @endif
                                                    >{{$blist->title}}</option>
                                                    @endforeach
                                                    </select>
                                                    <input type="hidden" id="batch_name" name="batch_name" value="{{$treasure_data-> batch_name}}"> 
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount</label>
                                                    <input type="number" class="form-control" id="" name="amount" value="{{$treasure_data -> amount}}" required>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Is file paid?</label>
                                                    <select name="is_paid" id="" class="form-control">
                                                        <option value="Yes"
                                                        @if( $treasure_data->is_paid == "Yes")
                                                            selected
                                                        @endif
                                                        >Paid</option>
                                                        <option value="no"
                                                        @if( $treasure_data->is_paid == "no")
                                                            selected
                                                        @endif
                                                        >Free</option>                                                      
                                                    </select>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">File</label>
                                                    <input type="hidden" value="{{$treasure_data->file_url}}" name="image_exist" id="image_exist">
                                                    <input type="file" id="exampleInputFile" name="image"  style="opacity: 1; position: inherit;">
                                                    <p class="help-block">Example block-level help text here.</p>
                                                </div>
                                                

                                        </div>



                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Update</button>
                                            </div>
                                        </div>

                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){
    
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

function set_batch(idss){
    $('#batch_name').val($('#batch_'+idss).data('name'));
}


</script>
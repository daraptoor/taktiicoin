
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="#">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Institute Info</h4>
                                                    <input type="hidden" name="insid" value="{{ $inst_data->id }}">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">First Name</label>
                                                    <input type="text" class="form-control" id="" name="first_name" value="{{$inst_data->first_name}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Last Name</label>
                                                    <input type="text" class="form-control" id="" name="last_name" value="{{$inst_data->last_name}}" readonly>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating"> Email</label>
                                                    <input type="text" class="form-control" id="" name="email" value="{{$inst_data->email}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Teacher Type</label>
                                                    <input type="text" class="form-control" id="" name="teacher_type" value="{{$inst_data->teacher_type}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Teacher Salary</label>
                                                    <input type="text" class="form-control" id="" name="teacher_salary" value="{{$inst_data->teacher_salary}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Course Name</label>
                                                    <input type="text" class="form-control" id="" name="course_name" value="{{$inst_data->course_name}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Class Name</label>
                                                    <input type="text" class="form-control" id="" name="class_name" value="{{$inst_data->class_name}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Subject Name</label>
                                                    <input type="text" class="form-control" id="" name="subject_name" value="{{$inst_data->subject_name}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating"> Address</label>
                                                    <input type="text" class="form-control" id="" name="address" value="{{$inst_data->address}}" readonly>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Join Date</label>
                                                    <input type="text" class="form-control" id="" name="join_date" value="{{$inst_data->join_date}}" readonly>
                                                </div>

                                              
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>

</script>
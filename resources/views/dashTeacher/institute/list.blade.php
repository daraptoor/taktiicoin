
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <!-- <th>Type</th>
                                                    <th>Salary</th> 
                                                    <th>Course Name</th> 
                                                    <th>Class Name</th>  -->
                                                    <th>Subject Name</th> 
                                                    <th>Address</th> 
                                                    <th>Join Date</th> 
                                                    <th>Attendence</th>
                                                    <th>Contact Inst</th>
                                                    <th>View</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($institute_list as $inlist)
                                                <tr id="row_{{ $inlist->id }}">
                                                    
                                                    <td>{{ $inlist->first_name }} {{ $inlist->last_name }}</td>
                                                    <td>{{ $inlist->email }}</td>
                                                    <!-- <td>{{ $inlist->teacher_type }}</td>
                                                    <td>{{ $inlist->teacher_salary }}</td>
                                                    <td>{{ $inlist->course_name }}</td>
                                                    <td>{{ $inlist->class_name }}</td> -->
                                                    <td>{{ $inlist->subject_name }}</td>
                                                    <td>{{ $inlist->address }}</td>
                                                    <td>{{ $inlist->join_date }}</td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/institute/attendence">
                                                        <i class="material-icons">group_add</i>
                                                    <div class="ripple-container"></div></a></td>
                                                    <td><a class="btn btn-primary btn-sm" href="#">
                                                        <i class="material-icons">local_post_office</i>
                                                    <div class="ripple-container"></div></a></td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/institute/view/{{base64_encode($inlist->id)}}">
                                                        <i class="material-icons">visibility</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>

                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})


function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('Batch status enabled Successfully', 500);
                 }else{
                     error('Unable to update Batch status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('Batch status disabled Successfully', 500);
                 }else{
                     error('Unable to update Batch status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete Batch?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_batch_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('Batch Deleted Successfully', 500);
                             var table = $('#tbleTeacherList').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete Batch', 500);
                         }
                     }
                })
            }            
        })

}
</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><strong>Sno</strong></th>
                                                <th><strong>Teacher Name</strong></th>
                                                <th><strong>startTime</strong></th>
                                                <th><strong>endTime</strong></th>
                                                <th><strong>Date</strong></th>
                                                <th><strong>meetingID</strong></th>
                                                <th><strong>Join Meeting</strong></th>
                                                <th><strong>booked student List</strong></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $count=1;?>
                                                @foreach($live_list as $llist)
                                                <tr id="row_{{ $llist->id }}">
                                                    <td>{{ $count++ }}</td>
                                                    <td>{{ $llist['teacherName']->first_name}}</td>
                                                    <td>{{ $llist->startTime }}</td>
                                                    <td>{{ $llist->endTime }}</td>
                                                    <td>{{ $llist->liveDate }}</td>
                                                    <td>{{ $llist->meetingID }}</td>
                                                    <td id="timer_<?=$llist->id?>" >
                                                    <a class="btn btn-info btn-sm" target="_blank" onclick="timeLeft('{{ $llist->liveDate }}','{{ $llist->startTime }}','{{ $llist->endTime }}','{{ $llist->id }}','{{ $llist->teacher_url }}','{{ $llist->teacher_id }}','{{ $llist->meetingID }}');">Check Status</a></td>

                                                     <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/live/studentview/{{ $llist->id }}">
                                                        <i class="material-icons">visibility</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
          <h3>Send Notification to student and Continue</h3>
          <span id="updatedLink"></span>
         
      </div>

    </div>

  </div>
</div>

<script>

$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})
//timeLeft();
function timeLeft(dat, st, et, id, url, teacher_id, batch_code){

$("#updatedLink").html(` <button  <a onclick="notify('${teacher_id}','${batch_code}')" class="btn btn-primary btn-lg">Yes, Continue </a></button>`);
var currentTime = moment().utc("+05:30").format('YYYY-M-DD HH:mm:ss');
var expectedTime = `${dat} ${st}:00`;
var expectedEndTime = `${dat} ${et}:00`;
var leftTime = expectedTime - currentTime;
setInterval(function(){ 
    
    var expectedTime2 = new Date(expectedTime);
    currentTime = moment().utc("+05:30").format('YYYY-M-DD HH:mm:ss');
    var currentTime2 = new Date(currentTime);
    var timeDiff = Math.abs(expectedTime2.getTime() - currentTime2.getTime()) / 1000;
    var days = Math.floor(timeDiff / 86400);
    timeDiff -= days * 86400;
    var hours = Math.floor(timeDiff / 3600) % 24;
    timeDiff -= hours * 3600;
    var minutes = Math.floor(timeDiff / 60) % 60;
    timeDiff -= minutes * 60;
    var seconds = timeDiff % 60;

    if(currentTime > expectedEndTime){
        $('#timer_'+id).html("<button class='btn btn-danger btn-sm'>Time Expired</button>");
        //console.log();
    }else{
        var exdate = moment(expectedTime, 'YYYY-M-DD HH:mm:ss').subtract(5,'minutes').format('YYYY-M-DD HH:mm:ss');
        //console.log(`${exdate}: 5 min before, | ${currentTime}:current time | ${expectedEndTime}: end time `);
        //$('#timer_'+id).html(`<a  href='${url}' class='btn btn-success btn-sm' target='_blank' >Start Live</a>`);
        

        if(exdate <= currentTime && exdate <= expectedEndTime){
             $('#timer_'+id).html(`<a  href='${url}' class='btn btn-success btn-sm' target='_blank' >Start Live</a>`);
             // $('#timer_'+id).html(`<button class='btn btn-success btn-sm' target='_blank' data-toggle="modal" data-target="#myModal">Start Live</button>`);
        }else{
            $('#timer_'+id).html("<button class='btn btn-success btn-sm'>"+days+" d "+hours+" h "+ parseInt(minutes-5) +" m "+seconds+" s Left</button>");
        }
        
       
        //console.log(exdate);
    }
              
}, 1000);
}





</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('teachers/live/add_live_class')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">      
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="course_id" name="course_id" required>
                                                    <option disabled selected>Select Course</option>
                                                        @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="course_name" value="" id="course_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="class_id" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="class_name" value="" id="class_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="subject_id" required >
                                                        <option disabled selected> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="subject_name" value="" id="subject_name">
                                                </div>
                                               
                                               
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Topic</label>
                                                    <input type="text" class="form-control" id="topic" name="topic" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" id="title" name="title" required>
                                                </div>
                                     
                                    

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time</label>
                                                    <input type="text" class="form-control time_picker" name="startTime" id="startTime" required/>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time</label>
                                                    <input type="text" class="form-control time_picker" name="endTime" id="endTime" required/>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start date</label>
                                                    <input type="text" class="form-control start_date" name="liveDate" id="liveDate" required/>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Paid</label>
                                                    <select class="form-control" name="is_paid" required>
                                                        <option disabled selected>Select Type </option>
                                                        <option value="Yes" >Paid</option>
                                                        <option value="No" >Free</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Amount</label>
                                                    <input type="text" class="form-control" name="amount" id="amount" value="0" required/>
                                                    
                                                </div>

                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Description</label>
                                                    <textarea class="form-control" id="description" name="description" required ></textarea>
                                                </div>
                                 

                                               
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
function set_teacher(id){
    var decodedString = atob(id);
    $('#teacher_name').val($('#teacher_'+decodedString).data('name'));
}

function set_batch(id){
    var decodedString = atob(id);
    $('#class_name').val($('#batch_'+decodedString).data('name'));
}

$(function(){
  $('#liveDate').datetimepicker({
        format: 'YYYY-M-DD'
    });

    $('#startTime, #endTime').datetimepicker({
        format: 'HH:mm:ss'
    });
})

function getClassesFromCourse(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            loaderEnd();
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            loaderEnd();
            $('#setSubjects').html(data);
        }
    })
    var decodedString = atob(id);
    $('#class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#subject_name').val($('#sub_'+decodedString).data('name'));
}
</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('teachers/live/add_live_session')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Name</label>
                                                    <select class="form-control" onChange="set_batch(this.value)" id="batch_code" name="batch_code" required>
                                                        <option value="" selected disabled>Select Batch</option>
                                                        @foreach($batch_list as $bl)
                                                        <option id="batch_{{$bl->id}}" value="{{$bl->batch_code }}" data-name="{{ $bl->title }}">{{ $bl->title }}({{ $bl->batch_code }})</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="title" id="title" value="">
                                                </div>
                                               
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="exampleUsername" class="bmd-label-floating">Meeting Name</label>
                                                    <input type="text" class="form-control" id="title" name="title" required >
                                                </div>
                                     
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start date</label>
                                                    <input type="text" class="form-control start_date" name="liveDate" id="liveDate" required/>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time</label>
                                                    <input type="text" class="form-control time_picker" name="startTime" id="startTime" required/>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time</label>
                                                    <input type="text" class="form-control time_picker" name="endTime" id="endTime" required/>
                                                </div>
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Description</label>
                                                    <textarea class="form-control" id="description" name="description"></textarea required>
                                                </div>

                                               
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script>
function set_batch(id){
    var decodedString = atob(id);
    $('#title').val($('#batch_'+decodedString).data('name'));
}

$(function(){
  $('#liveDate').datetimepicker({
        format: 'YYYY-M-DD'
    });

    $('#startTime, #endTime').datetimepicker({
        format: 'HH:mm:ss'
    });
})
</script>
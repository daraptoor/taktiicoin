<?php
use \App\Http\Controllers\ActionController;
?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleStudentList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><strong>Sno</strong></th>
                                                <th><strong>Student Name</strong></th>
                                                <th><strong>Paid</strong></th>
                                                <th><strong>Amount</strong></th>
                                                <th><strong>Type</strong></th>
                                                <th><strong>Join Date</strong></th>
                                            
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $count=1;?>
                                                @foreach($live_list as $llist)
                                                <tr id="row_{{ $llist->id }}">
                                                    <td>{{ $count++ }}</td>
                                                    <td> {{ $llist->students['first_name'] }}</td>
                                                    <td>{{ $llist->is_paid }}</td>
                                                    <td>{{ $llist->amount }}</td>
                                                    <td>{{ $llist->type }}</td>
                                                    <td>{{ $llist->added_date }}</td>
                                                   
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleStudentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

</script>
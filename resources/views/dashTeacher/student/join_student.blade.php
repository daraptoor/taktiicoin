<?php
use \App\Http\Controllers\ActionController;
?>
         <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Join Student's List</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleStudentList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th><b>SNO</b></th>
                                                    <th><b>Image</b></th>
                                                    <th><b>Name</b></th>
                                                    <th><b>Email</b></th>
                                                    <th><b>Mobile</b></th>
                                                    <th><b>Gender</b></th>                                                    
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $count=1;?>
                                            @foreach($std_list as $list)
                                                <?php
                                                $get_sinfo = ActionController::studentdetails( $list, array("id","profile_pic","first_name","email","mobile","gender")); 
                                                
                                                //print_r($get_sinfo);  

                                                ?>
                                                <tr id="row_{{$get_sinfo[0]->id}}">
                                                <td>{{ $count++ }}</td>
                                                <td>{{ $get_sinfo[0]->profile_pic }}</td>
                                                <td>{{ $get_sinfo[0]->first_name }}</td>
                                                <td>{{ $get_sinfo[0]->email }}</td>
                                                <td>{{ $get_sinfo[0]->mobile }}</td>
                                                <td>{{ $get_sinfo[0]->gender }}</td>
                                                 
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleStudentList');

    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

</script>

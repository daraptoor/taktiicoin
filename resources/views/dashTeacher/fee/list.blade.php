
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    <th>Student name</th>
                                                    <th>Mobile</th>
                                                    <th>Batch Name</th>
                                                    <th>Batch Type</th>
                                                    <th>Amount</th>
                                                    <th>Payment Mode</th>
                                                    <th>Paid</th>
                                                    <th>Pending</th>
                                                    <th>View/Edit</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($fee_list as $plist)
                                                    <tr id="row_{{ $plist->id }}">
                                                    <td>{{ $plist->studentinfo['first_name'] }}</td>
                                                    <td>{{ $plist->studentinfo['mobile'] }}</td>
                                                    <td>{{ $plist->batchinfo['title']}}</td>
                                                    <td>{{ $plist->batch_type }}</td>
                                                    <td>{{ $plist->amount }}</td>
                                                    <td>
                                                        <div class="togglebutton">
                                                            @if( $plist->payment_mode == 1)	
                                                             Online  
                                                            @else
                                                             Offline
                                                              @endif                                                         
                                                        </div>  
                                                    </td>
                                                    <td>{{ $plist->paid_on }}</td>
                                                    <td>{{ $plist->pending }}</td>
                                                     <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/fee/edit/{{ $plist->id }}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

</script>
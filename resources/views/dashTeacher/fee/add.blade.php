
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('/teachers/fee/add_fee')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="card-body col-md-12">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Fees Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                <label for="batch_id" class="bmd-label-floating">Batch Name</label>
                                                <select name="batch_id" class="form-control" id="batch_id" onChange="get_batch_students(this.value)">
                                                    <option value="0000">Select Batch</option>
                                                    @foreach($batch_list as $blist)
                                                    <option value="{{$blist->id}}" id="batch_{{$blist->id}}" data-name="{{ $blist->title }}">{{$blist->title}}</option>
                                                    @endforeach
                                                </select>
                                                    <input type="hidden" value="" id="title" name="title"> 
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Student</label>
                                                    <select class="form-control" id="get_data"  name="student_id" >
                                                        <option disabled selected> Select Batch First </option>
                                                    </select>
                                                    <input type="hidden" name="first_name" value="" id="first_name">
                                                </div>

                                               

                                                 <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Type</label>
                                                    <select name="batch_type" id="" class="form-control">
                                                        <option value="Batch">Batch</option>
                                                        <option value="Session">Session</option>                                                      
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Payment Mode</label>
                                                    <select name="payment_mode" id="" class="form-control">
                                                        <option value="2">Offline</option>                                                      
                                                    </select>
                                                </div>


                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Amount</label>
                                                    <input type="number" class="form-control" id="" name="amount" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-3" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Amount</label>
                                                    <input type="Number" class="form-control" id="" name="pending" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Paid Date</label>
                                                    <input type="text" class="form-control date" name="paid_on" >
                                                </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
function nospaces(t){
    if(t.value.match(/\s/g)){
        t.value=t.value.replace(/\s/g,'');
    }
}

$(function(){
    
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('.date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
})

// function set_batch(idss){
//     $('#batch_name').val($('#batch_'+idss).data('name'));
// }
// function set_std(idss){
//     $('#std_name').val($('#std_'+idss).data('name'));
// }

function  get_batch_students(id){
    loaderStart('Loading Students...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_student_selected_by_batches/'+id,
        success:function(data){
            $('#get_data').html(data);
            loaderEnd();
        }
    })
}

</script>
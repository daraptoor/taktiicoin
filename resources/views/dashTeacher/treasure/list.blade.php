
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr>
                                                    
                                                
                                                <th><b>Heading</b></th>
                                                <th><b>Types</b></th>
                                                <th><b>file Url</b></th>
                                                <th><b>Date<b></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($treasure_box as $slist)
                                                    <tr id="row_{{ $slist->id }}">                                                    
                                                    <td>
                                                    @if( $slist->type == 'image')
                                                    {{ $slist->img_heading }}
                                                    @elseif($slist->type == 'Video')
                                                    {{ $slist->pdf_heading }}
                                                    @elseif($slist->type == 'doc')
                                                    {{ $slist->pdf_heading }}
                                                    @elseif($slist->type == 'pdf')
                                                    {{ $slist->pdf_heading }}
                                                     @endif 
                                                   </td>
                                                    <td>{{ $slist->type }}</td>
                                                    <td>
                                                    @if( $slist->type == 'image')
                                                    <a href="{{$slist->pdf_img}}" target="_blank">Image Link</a>
                                                    @elseif($slist->type == 'Video')
                                                    <a href="{{$slist->video_url}}" target="_blank">Video Link</a>
                                                    @elseif($slist->type == 'doc')
                                                    <a href="{{$slist->pdf_img}}" target="_blank">DOC & Other</a>
                                                    @elseif($slist->type == 'pdf')
                                                    <a href="{{$slist->pdf_img}}" target="_blank">PDF</a>
                                                     @endif  
                                                    </td>

                                                    <td>{{ $slist->created }}</td>                                               
                                                
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})  

function change_status(id,data){
    if($("#status_"+id).is(':checked')){
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_treasure_status/'+data+'/1',
             success:function(data){
                 if(data == 1){
                     success('File status enabled Successfully', 500);
                 }else{
                     error('Unable to update File status', 500);
                 }
            }
         })
    }else{
         $.ajax({
             type:'get',
             url:'{{ url('/')}}/ajax/update_treasure_status/'+data+'/0',
             success:function(data){
                 if(data == 1){
                     success('File status disabled Successfully', 500);
                 }else{
                     error('Unable to update File status', 500);
                 }
             }
         })
    }    
}


function delete_admin(id,data){
    swal({
        title: 'Wait...',
        text: "Are you sure you want to delete File?<br><strong>This can't be Undone</strong>",
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Delete it'
        }).then((result) => {            
            if(result){
                $.ajax({
                     type:'get',
                     url:'{{ url('/')}}/ajax/update_treasure_status/'+data+'/2',
                     success:function(data){
                         if(data == 1){
                             success('File Deleted Successfully', 500);
                             var table = $('#tbleTeacherList    ').DataTable();  
                             table.row( $("#row_"+id) ).remove().draw();
                         }else{
                             error('Unable to Delete File', 500);
                         }
                     }
                })
            }            
        })

}
</script>
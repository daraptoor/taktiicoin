<div class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-icon" data-background-color="green">
							<i class="material-icons">&#xE894;</i>
						</div>
						<div class="card-content">

							<h4 class="card-title">{{ $title }}</h4>
							<div class="row">
								<form method="post" action="{{URL::to('/teachers/treasure/add_treasure')}}" enctype="multipart/form-data">
									@csrf
									<div class="card-body col-md-12">
                                        <div class="form-group col-md-4" id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                <select class="form-control" onChange="getClassesFromCourse(this.value)" id="course_id" name="course_id">
                                                    <option disabled selected>Select Course</option>
                                                    @foreach($courses_list as $cl)
                                                    <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                    @endforeach
                                                </select>
                                                <input type="hidden" name="course_name" value="" id="course_name">
                                            </div>
    
                                            <div class="form-group col-md-4 " id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="class_id">
                                                    <option disabled selected>Select Course First</option>
                                                </select>
                                                <input type="hidden" name="class_name" value="" id="class_name">
                                            </div>
    
                                            <div class="form-group col-md-4" id="">
                                                <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="subject_id" >
                                                    <option disabled selected> Select Class First </option>
                                                </select>
                                                <input type="hidden" name="subject_name" value="" id="subject_name">
                                            </div>

                                            <div class="form-group col-md-4">
                                                <label class="control-label">Chapter</label>
                                                <input type="text" class="form-control" name="chapter_name" value = "" >
                                            </div>
    
                                            <div class="form-group col-md-4">
                                                <label class="control-label">Topic</label>
                                                <input type="text" class="form-control" name="topic" value = "" >
                                            </div>
    
                                            <div class="form-group col-md-4" id="class_username">
                                                <label for="exampleUsername" class="bmd-label-floating">File Type</label>
                                                <select name="type" id="file_type" class="form-control">
                                                <option disa selectedbled>Select File Types</option>
                                                    <option value="doc">DOC/PPT</option>
                                                    <option value="image" >Image</option>
                                                    <option value="Video">Video</option>
                                                    <option value="pdf">PDF</option>
                                                </select>
                                            </div>
                                        <span id="docSection"></span>
                                            <div class="col-md-12 col-sm-12">
                                                    <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                        <button type ="submit" class="btn btn-success" >Update</button>
                                                    </div>
                                                </div>
                                    </div>
                                </form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>

			function getClassesFromCourse(id){
				$.ajax({
					type:'get',
					url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
					success:function(data){
						$('#setClasses').html(data);
					}
				})
				var decodedString = atob(id);
				$('#course_name').val($('#crs_'+decodedString).data('name'));
			}

			function getSubjectsFromClass(id){
				$.ajax({
					type:'get',
					url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
					success:function(data){
						$('#setSubjects').html(data);
					}
				})
				var decodedString = atob(id);
				$('#class_name').val($('#cls_'+decodedString).data('name'));
			}


			function set_Subjects(id){
				var decodedString = atob(id);
				$('#subject_name').val($('#sub_'+decodedString).data('name'));
			}



			$(document).ready(function(){
				$('#file_type').on('change', function() {
					if ( this.value == 'doc' )
					{
						$("#docSection").html(`<span id="doc" class="pull-left" >

<div class="form-group col-md-4">
    <label class="control-label">DOC Tittle</label>
    <input type="text" class="form-control" name="pdf_heading" value = "">
</div>



<div class="form-group col-md-4">
    <label class="control-label">DOC Description(optional)</label>
    <input type="text" class="form-control" name="pdf_desc" value = "">
</div>

<div class="form-group col-md-4" id="class_username">
    <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
    <p class="help-block">Example block-level help text here.</p>
</div>
</span>`);

}else if(this.value == 'img' ){
$("#docSection").html(`<span id="img" class="pull-left" >
<div class="form-group  col-md-4">
    <label class="control-label">Image Tittle</label>
    <input type="text" class="form-control" name="img_heading" value = "">
</div>


<div class="form-group col-md-4">
    <label class="control-label">Image Description(optional)</label>
    <input type="text" class="form-control" name="img_desc" value = "">
</div>

<div class="form-group col-md-4" id="class_username">
    <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
    <p class="help-block">Example block-level help text here.</p>
</div>
</span>`);
}else if(this.value == 'video' ){
$("#docSection").html(`
<div class="form-group col-md-4">
    <label class="control-label">Video Tittle</label>
    <input type="text" class="form-control" name="img_heading" value = "">
</div>


<div class="form-group col-md-4">
    <label class="control-label">video Description(optional)</label>
    <input type="text" class="form-control" name="img_desc" value = "">
</div>


<div class="form-group col-md-4">
    <label for="Image">Video Type you want to use : </label><br>
    <input type="radio" name="video_type" value="upload" id="idupload" onchange="whichType('upl');" > Upload Video
    <input type="radio" name="video_type" value="url" id="idurl" onchange="whichType('url');" > Enter Url
</div>

<div class="form-group col-md-12" id="upl" style="display: none;">
    <label for="Image">Upload Video</label>

    <div class="form-group bmd-form-group col-md-12" id="class_username">
        <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

        <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
        <p class="help-block">Example block-level help text here.</p>
    </div>


        <!--  <input type="file" name="upload_video" class="form-control"> -->
    </div>

    <div class="form-group col-md-12" id="url" style="display: none;">
        <label for="Image">Video URL</label>
        <input type="text" name="upload_video_url" class="form-control" placeholder="Enter video url eg. https://www.youtube.com/embed/3tmd-ClpJxA">
    </div>

</div>
`);
}else if(this.value == 'pdf' ){
    $("#docSection").html(`<span class="pull-left"  id="pdf">

<div class="form-group  col-md-4">
    <label class="control-label"> Pdf Tittle</label>
    <input type="text" class="form-control" name="pdf_heading" value = "">
</div>
<div class="form-group col-md-4">
    <label class="control-label">Pdf Description(optional)</label>
    <input type="text" class="form-control" name="pdf_desc" value = "">
</div>

<div class="form-group col-md-4" id="class_username">
    <label for="exampleUsername" class="bmd-label-floating">Files (You can upload multiple files)</label>

    <input type="file" id="exampleInputFile" name="image[]" multiple required style="opacity: 1; position: inherit;">
    <p class="help-block">Example block-level help text here.</p>
</div>
</span>`);
					}
					else
					{

					}
				});
				$("#doc").show();
				$("#pdf").hide();
				$("#img").hide();
				$("#video").hide();
			});



			function whichType(path){

				if(path == 'upl'){
					$('#upl').show();
					$('#url').hide();
				}else if(path == 'url'){
					$('#url').show();
					$('#upl').hide();
				}
			}



		</script>

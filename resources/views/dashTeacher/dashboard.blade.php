<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                            <div class="col-md-12"> 

                                                <div class="row">
                                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="orange">
                                                                <i class="material-icons">recent_actors</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Students</p>
                                                                <h3 class="card-title">{{ $student_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/teachers/student/join_student">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 
                                                    <div class="col-lg-4 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="green">
                                                                <i class="material-icons">weekend</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Batches</p>
                                                                <h3 class="card-title">{{ $batch_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/teachers/batches/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                       <div class="col-lg-4 col-md-6 col-sm-6">
                                                        <div class="card card-stats">
                                                            <div class="card-header" data-background-color="rose">
                                                                <i class="material-icons">perm_identity</i>
                                                            </div>
                                                            <div class="card-content">
                                                                <p class="category">Treasure Box</p>
                                                                <h3 class="card-title">{{ $treasure_count }}</h3>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="stats">
                                                                    <i class="material-icons text-danger">link</i>
                                                                    <a href="{{ url('/') }}/teachers/treasure/list">View List</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
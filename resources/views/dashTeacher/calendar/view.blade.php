<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                <?php $elist;
                    $data = array();
                    $count = 0;
                    
                    //$canList = json_encode($data);
                ?>
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">&#xE894;</i>
                    </div>
                    <div class="card-content">
                    <div class="row">
                    <br>
                        <div class="col-md-12">
                        <div id="fullCalendar"></div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $calendar = $('#fullCalendar');
    // var data = '';
    // console.log(data);
    today = new Date();
    y = today.getFullYear();
    m = today.getMonth();
    d = today.getDate();

    $calendar.fullCalendar({
        eventClick: function(calEvent, jsEvent, view) {

            // alert('Event: ' + calEvent.title);
            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            // alert('View: ' + view.name);

            // change the border color just for fun
            //$(this).css('border-color', 'red');

            swal({
                title: 'Event Detail',
                html: unescape(calEvent.title),
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                confirmButtonText: 'Delete',
                cancelButtonClass: 'btn btn-danger',
                cancelButtonText: 'Close',
                buttonsStyling: false
            }).then(function(result) {
                loaderStart('Deleting...');
                $.ajax({
                    type:'get',
                    url:'{{ url('/')}}/ajax/del_event_calender/'+calEvent.id,
                    success:function(data){
                        //alert(data);
                        $('#fullCalendar').fullCalendar('removeEvents', calEvent.id);
                        loaderEnd();
                    }
                })
            });

        },
        viewRender: function(view, element) {
            // We make sure that we activate the perfect scrollbar when the view isn't on Month
            if (view.name != 'month') {
                $(element).find('.fc-scroller').perfectScrollbar();
            }
        },
        header: {
            left: 'title',
            center: 'month,agendaWeek,agendaDay',
            right: 'prev,next,today'
        },
        defaultDate: today,
        selectable: true,
        selectHelper: true,
        views: {
            month: { // name of view
                titleFormat: 'MMMM YYYY'
                // other view-specific options here
            },
            week: {
                titleFormat: " MMMM D YYYY"
            },
            day: {
                titleFormat: 'D MMM, YYYY'
            }
        },

        select: function(start, end) {

            // on select we show the Sweet Alert modal with an input
            swal({
                title: 'Create an Event',
                html: '<div class="form-group">' +
                    '<textarea class="form-control" rows="10" placeholder="Event Title" id="input-field"></textarea>' +
                    '</div>',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            }).then(function(result) {

                var eventData;
                event_title = $('#input-field').val();

                if (event_title) {
                    eventData = {
                        title: event_title,
                        start: start,
                        end: end
                    };
                    $calendar.fullCalendar('renderEvent', eventData, true); // stick? = true
                    insert_data(event_title,start);
                }

                $calendar.fullCalendar('unselect');

            });
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events


        // color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
        
        events: 
            <?php
            foreach($elist as $list){
                //print_r($list->user_id);
                $evDate = date('d-m-Y',$list->date_alloted/1000);
                $today = date('d-m-Y');
                $data[$count]['id'] = $list->id;
                $data[$count]['title'] = $list->content;
                $data[$count]['start'] = date('D M j G:i:s T Y',$list->date_alloted/1000);
                if($evDate >= $today){
                    $class = "event-green";
                }else{
                    $class = "event-red";
                }
                $data[$count]['className'] = $class;
                $count++;
            }
            echo json_encode( $data);
            ?>
        //     [
        //     {
        //         title: 'All Day Event',
        //         start: new Date(y, m, 1),
        //         className: 'event-default'
        //     },
        //     {
        //         id: 999,
        //         title: 'Repeating Event',
        //         start: new Date(y, m, d - 4, 6, 0),
        //         allDay: false,
        //         className: 'event-rose'
        //     }
        // ]
        
    });
   // alert(new Date(y, m, d - 4, 6, 0));

});
function insert_data(text,start){
    //alert(start);
    loaderStart('Adding Event...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/add_event_calendar/'+escape(text)+'/'+start,
        success:function(data){
            //alert(data);
            loaderEnd();
        }
    })
}
</script>
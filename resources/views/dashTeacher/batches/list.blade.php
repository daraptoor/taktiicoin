
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                    <th>ID</th>
                                                    <th>Title</th>
                                                    <th>BatchCode</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Days</th>
                                                    <th>Time</th>
                                                    <th>Status</th>
                                                    <th>Attendence</th>
                                                    <th>Remove</th>
                                                    <th>View/Edit</th>
                                                    <th>join student's</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                @foreach($batch_list as $blist)
                                                <tr id="row_{{ $blist->id }}">


                                                    <td>{{ $blist->id }}</td>
                                                    <td>{{ $blist->title }}</td>
                                                    <td>{{ $blist->batch_code }}</td>
                                                    <td>{{ $blist->batch_start_date }}</td>
                                                    <td>{{ $blist->batch_end_date }}</td>
                                                    <td><a href="#" data-toggle="tooltip" data-placement="right" title="{{ str_replace(",",", ",$blist->day_name) }}"> {{ substr_count( str_replace(",",", ",$blist->day_name),', ')+1 }} Days</a></td>
                                                    <td>{{ $blist->batch_start_time }}-{{ $blist->batch_end_time }}</td>
                                                    <td>
                                                        <div class="togglebutton">
                                                      
                                                            @if( $blist->batch_status == 1)	
                                                             Active batch
                                                             
                                                            @elseif( $blist->batch_status == 2) 
                                                             Upcomming batch
                                                    
                                                            @else
                                                               Past batch 
                                                              @endif                                                         
                                                           
                                                        </div>  
                                                    </td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/batches/attendence">
                                                        <i class="material-icons">group_add</i>
                                                    <div class="ripple-container"></div></a></td>
                                                    <td><button class="btn btn-danger btn-sm" onClick="delete_batch('{{ $blist->id }}')">
                                                        <i class="material-icons">close</i>
                                                        Delete
                                                    <div class="ripple-container"></div></button>
                                                    </td>
                                                    <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/batches/edit/{{base64_encode($blist->id)}}">
                                                        <i class="material-icons">edit</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                     <td><a class="btn btn-primary btn-sm" href="{{ url('/') }}/teachers/batches/joinstudent/{{ $blist->id }}">
                                                        <i class="material-icons">visibility</i>
                                                    <div class="ripple-container"></div></a>
                                                    </td>
                                                    </tr>
                                                @endforeach                                                    
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})


    function delete_batch(id){
	swal({
              title: 'Are you sure?',
              text: "You want to delete Batch !!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, Delete it!'
            }).then((result) => {
                loaderStart('Loading Deleting...');
                $.ajax({
                type:'GET',
                url:'{{ url('/')}}/ajax/del_batch/'+id,
                success:function(data){ 
                loaderEnd();
                   if(data == 1){
                        swal(
                          'Success',
                          'Batch removed Successfully.',
                          'success'
                        )
                       $("#row_"+id).remove();
                   }else{
                    swal(
                          'Failed!',
                          'Unable to remove Batch.',
                          'error'
                        )
                        
                   }         
                  },
                  error:function(){
                    swal(
                          'Failed!',
                          'Unable to remove Batch.',
                          'error'
                        )
                  }
                });
                
            })
}

</script>
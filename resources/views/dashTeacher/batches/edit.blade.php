
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('teachers/batches/update_tbatch')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Batch Info</h4>
                                                    <input type="hidden" name="b_id" value="{{ $batch_data->id }}">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" id="" name="title" value="{{$batch_data->title}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Fees</label>
                                                    <input type="number" class="form-control" id="batch_fee" name="batch_fee" value="{{$batch_data->batch_fee}}" required>
                                                </div>
                                               
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Start Batch</label>
                                                    <input type="text" class="form-control datepicker" id="batch_start" name="batch_start_date" value="{{$batch_data->batch_start_date}}" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Batch(optional)</label>
                                                    <input type="text" class="form-control datepicker" id="batch_end" name="batch_end_date" value="{{$batch_data->batch_end_date}}" >
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time</label>
                                                    <input type="text" class="form-control" id="batch_stime" name="batch_start_time" value="{{$batch_data->batch_start_time}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time</label>
                                                    <input type="text" class="form-control" id="batch_etime" name="batch_end_time" value="{{$batch_data->batch_end_time}}" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Duration</label>
                                                    <input type="text" class="form-control" id="duration" name="duration"  placeholder=" Example :-6 Month , 60 Day, 4 Week" value="{{$batch_data->duration}}" required>
                                                </div>

                                        
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Batch Strength</label>
                                                    <input type="number" class="form-control" id="batch_strength" name="batch_strength" value="{{$batch_data->batch_strength}}" required>
                                                </div>
                                                
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Batch Demo</label>
                                                    <input type="number" class="form-control" id="batch_demo" name="batch_demo" value="{{$batch_data->batch_demo}}" required>
                                                </div>


                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Type</label>
                                                    <select name="batch_status" id="batch_status" class="form-control" required>
                                                        <option value="0"
                                                        @if( $batch_data->batch_status   ==  "0" )
                                                                selected
                                                            @endif
                                                            >Past Batch</option>
                                                        <option value="1"
                                                        
                                                        @if( $batch_data->batch_status   ==  "1" )
                                                                selected
                                                            @endif
                                                            
                                                            >Active Batch</option>
                                                        <option value="2"
                                                        @if( $batch_data->batch_status   ==  "2" )
                                                                selected
                                                            @endif
                                                        >Upcomming Batch</option>
                                                    </select>
                                                </div>


                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Topic</label>
                                                    <input type="text" class="form-control" id="" name="topic" value="{{$batch_data->topic}}"  required>
                                                </div>

                                                  <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="course_id" name="course_id" required>
                                                    <option disabled selected>Select Course</option>
                                                    @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}" 
                                                        @if( $batch_data->course_id == $cl->id )
                                                                selected
                                                            @endif
                                                            >{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="course_name" value="{{$batch_data->course_name}}" id="course_name" >
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="class_id" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="class_name" value="{{$batch_data->class_name}}" id="class_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="subject_id"required >
                                                        <option disabled selected> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="subject_name" value="{{$batch_data->subject_name}}" id="subject_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-3 checkbox-radios">         
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Batch Days</h4>
                                                    <?php 
                                                        $days = explode(',',$batch_data->day_name);
                                                        
                                                    
                                                    ?>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Sunday" {{(in_array('Sunday',$days))?"checked":""}}>
                                                            Sunday
                                                        </label>
                                                    </div>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Monday" {{(in_array('Monday',$days))?"checked":""}}>
                                                            Monday
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Tuesday" {{(in_array('Tuesday',$days))?"checked":""}}>
                                                            Tuesday
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Wedenesday" {{(in_array('Wedenesday',$days))?"checked":""}}>
                                                            Wedenesday
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Thursday" {{(in_array('Thursday',$days))?"checked":""}}>
                                                            Thursday
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Friday" {{(in_array('Friday',$days))?"checked":""}}>
                                                            Friday
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="day_name[]" type="checkbox" value="Saturday" {{(in_array('Saturday',$days))?"checked":""}}>
                                                            Saturday
                                                        </label>
                                                    </div>
                                                </div>

                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>

$(function(){ 
    getClassesFromCourse('{{ base64_encode($batch_data->course_id)}}','{{ base64_encode($batch_data->class_id)}}');
    getSubjectsFromClass('{{ base64_encode($batch_data->class_id)}}','{{ base64_encode($batch_data->subject_id)}}');

})

$(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('#batch_start, #batch_end').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#batch_stime, #batch_etime').datetimepicker({
        format: 'LT'
    });

})


function getClassesFromCourse(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_classes_from_course/'+id+'/'+selected,
        success:function(data){
            $('#setClasses').html(data);
            
        }
    })
    var decodedString = atob(id);
   // alert($('#crs_'+decodedString).data('name'));
    $('#course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id,selected){
    
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_selected_subject_from_class/'+id+'/'+selected,
        success:function(data){
            $('#setSubjects').html(data);            
        }
    })
   
}
function getClassesFromCourse2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#course_name').val($('#crs_'+decodedString).data('name'));
    loaderEnd();
}

function getSubjectsFromClass2(id){
    loaderStart("Geting data...");
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            $('#setSubjects').html(data);
            loaderEnd();
        }
    })
    var decodedString = atob(id);
    $('#class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#subject_name').val($('#sub_'+decodedString).data('name'));
}

</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                    <form method="post" action="{{URL::to('teachers/batches/add_new_tbatch')}}">
                                    @csrf
                                        <div class="card-body col-md-9">
                                                <div class="form-group bmd-form-group col-md-12">
                                                    <h4>Batch Info</h4>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Title</label>
                                                    <input type="text" class="form-control" id="" name="title" required>
                                                </div>

                                                 <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Fees</label>
                                                    <input type="number" class="form-control" id="batch_fee" name="batch_fee" required>
                                                </div>
                                               
                                                <div class="form-group bmd-form-group  col-md-6">
                                                    <label for="exampleEmail" class="bmd-label-floating">Start Batch</label>
                                                    <input type="text" class="form-control datepicker" id="batch_start" name="batch_start_date" required>
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Batch(optional)</label>
                                                    <input type="text" class="form-control datepicker" id="batch_end" name="batch_end_date">
                                                </div>
                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Start Time</label>
                                                    <input type="text" class="form-control" id="batch_stime" name="batch_start_time" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">End Time</label>
                                                    <input type="text" class="form-control" id="batch_etime" name="batch_end_time" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Duration</label>
                                                    <select name="duration" id="duration" class="form-control" required>
                                                        <option value="1 months">1 months</option>
                                                        <option value="2 months">2 months</option>
                                                        <option value="3 months">3 months</option>
                                                        <option value="4 months">4 months</option>
                                                        <option value="5 months">5 months</option>
                                                        <option value="6 months">6 months</option>
                                                        <option value="7 months">7 months</option>
                                                        <option value="8 months">8 months</option>
                                                        <option value="9 months">9 months</option>
                                                        <option value="10 months">10 months</option>
                                                        <option value="11 months">11 months</option>
                                                        <option value="12 months">12 months</option>
                                                        <option value="13 months">13 months</option>
                                                        <option value="14 months">14 months</option>
                                                        <option value="15 months">15 months</option>
                                                        <option value="16 months">16 months</option>
                                                        <option value="17 months">17 months</option>
                                                        <option value="18 months">18 months</option>
                                                        <option value="19 months">19 months</option>
                                                        <option value="20 months">20 months</option>
                                                        <option value="21 months">21 months</option>
                                                        <option value="22 months">22 months</option>
                                                        <option value="23 months">23 months</option>
                                                        <option value="24 months">24 months</option>
                                                    </select>
                                                    
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Type</label>
                                                    <select name="batch_status" id="batch_status" class="form-control" required>
                                                        <option value="0">Past Batch</option>
                                                        <option value="1">Active Batch</option>
                                                        <option value="2">Upcomming Batch</option>
                                                    </select>
                                                </div>

                                            

                                                <div class="form-group bmd-form-group col-md-6">
                                                    <label for="examplePass" class="bmd-label-floating">Batch Strength</label>
                                                    <input type="number" class="form-control" id="batch_strength" name="batch_strength" required>
                                                </div>
                                                
                                                
                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Batch Demo</label>
                                                    <select name="batch_demo" id="batch_demo" class="form-control" required>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-12" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Topic</label>
                                                    <input type="text" class="form-control" id="" name="topic" required>
                                                </div>

                                                  <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Course</label>
                                                    <select class="form-control" onChange="getClassesFromCourse(this.value)" id="course_id" name="course_id" required>
                                                    <option disabled selected>Select Course</option>
                                                        @foreach($courses_list as $cl)
                                                        <option id="crs_{{$cl->id}}" value="{{ base64_encode($cl->id) }}" data-name="{{ $cl->name }}">{{ $cl->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <input type="hidden" name="course_name" value="" id="course_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select Class</label>
                                                    <select class="form-control" id="setClasses" onChange="getSubjectsFromClass(this.value)" name="class_id" required>
                                                        <option disabled selected>Select Course First</option>
                                                    </select>
                                                    <input type="hidden" name="class_name" value="" id="class_name">
                                                </div>

                                                <div class="form-group bmd-form-group col-md-4" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Select subject</label>
                                                    <select class="form-control" id="setSubjects" onChange="set_Subjects(this.value)" name="subject_id" required>
                                                        <option disabled selected> Select Class First </option>
                                                    </select>
                                                    <input type="hidden" name="subject_name" value="" id="subject_name">
                                                </div>


                                        </div>
                                        <div class="col-sm-3 checkbox-radios">         
                                            <div class="form-group bmd-form-group col-md-12">
                                                <h4>Batch Days</h4>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Sunday" checked="">
                                                        Sunday
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Monday" checked="">
                                                        Monday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Tuesday" checked="">
                                                        Tuesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Wedenesday" checked="">
                                                        Wedenesday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Thursday" checked="">
                                                        Thursday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Friday" checked="">
                                                        Friday
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group bmd-form-group col-md-12">
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" name="day_name[]" type="checkbox" value="Saturday" checked="">
                                                        Saturday
                                                    </label>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<script>
$(function(){
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif

    $('#batch_start, #batch_end').datetimepicker({
        format: 'DD-MM-YYYY'
    });

    $('#batch_stime, #batch_etime').datetimepicker({
        format: 'LT'
    });

})


function set_teacher(id){
    var decodedString = atob(id);
    $('#teacher_name').val($('#teacher_'+decodedString).data('name'));
}

function getClassesFromCourse(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_classes_from_course/'+id,
        success:function(data){
            loaderEnd();
            $('#setClasses').html(data);
        }
    })
    var decodedString = atob(id);
    $('#course_name').val($('#crs_'+decodedString).data('name'));
}

function getSubjectsFromClass(id){
    loaderStart('Loading...');
    $.ajax({
        type:'get',
        url:'{{ url('/')}}/ajax/get_subject_from_class/'+id,
        success:function(data){
            loaderEnd();
            $('#setSubjects').html(data);
        }
    })
    var decodedString = atob(id);
    $('#class_name').val($('#cls_'+decodedString).data('name'));
}


function set_Subjects(id){
    var decodedString = atob(id);
    $('#subject_name').val($('#sub_'+decodedString).data('name'));
}
</script>
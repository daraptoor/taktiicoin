<?php
use \App\Http\Controllers\ActionController;
?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">
                                           
                                           <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                            <thead>
                                                <tr style="font-size: 12px;">
                                                <th><strong>Sno</strong></th>
                                                <th><strong>Teacher Name</strong></th>
                                                <th><strong>startTime</strong></th>
                                                <th><strong>endTime</strong></th>
                                                <th><strong>Date</strong></th>
                                                <th><strong>meetingID</strong></th>
                                                <th><strong>Join Meeting</strong></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <?php $count=1;?>
                                                @foreach($activestd_list as $llist)
                                                <tr id="row_{{ $llist->id }}">
                                                    <td>{{ $count++ }}</td>
                                                    <td>
                                                    <?php $get_info = ActionController::teacherdetails( $llist->livedetails['teacher_id'], array("first_name"));  
                                                        echo $teacher_name = $get_info['0']->first_name;
                                                        $std_url = explode('?',$llist->livedetails['student_login_url']);
                                                    ?>
                                                    </td>
                                                    <td>{{ $llist->livedetails['startTime']}}</td>
                                                    <td>{{ $llist->livedetails['endTime']}}</td>
                                                    <td>{{ $llist->livedetails['liveDate'] }}</td>
                                                    <td>{{ $llist->livedetails['meetingID']}}</td>
                                                    <td id="timer_<?=$llist->livedetails['id'];?>">
                                                    <a class="btn btn-info btn-sm" target="_blank" onclick="timeLeft('{{ $llist->livedetails['liveDate'] }}','{{ $llist->livedetails['startTime'] }}','{{ $llist->livedetails['endTime'] }}','{{ $llist->livedetails['id'] }}','{{ $std_url[0] }}?attendeeName={{str_replace(' ', '',$username)}}&meetingID={{ $llist->livedetails['meetingID']}}&attendeePW={{ $llist->livedetails['student_pass']}}','{{ $llist->livedetails['teacher_id'] }}','{{ $llist->livedetails['meetingID'] }}');">Check Status</a></td>

                                                    </tr>
                                                @endforeach                                                    
                                                                                                   
                                                
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal --><!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
          <h3>Send Notification to student and Continue</h3>
          <span id="updatedLink"></span>
         
      </div>

    </div>

  </div>
</div>
<script>
//timeLeft();
function timeLeft(dat, st, et, id, url, teacher_id, batch_code){

$("#updatedLink").html(` <button  <a onclick="notify('${teacher_id}','${batch_code}')" class="btn btn-primary btn-lg">Yes, Continue </a></button>`);
var currentTime = moment().utc("+05:30").format('YYYY-M-DD HH:mm:ss');
var expectedTime = `${dat} ${st}:00`;
var expectedEndTime = `${dat} ${et}:00`;
var leftTime = expectedTime - currentTime;
setInterval(function(){ 
    
    var expectedTime2 = new Date(expectedTime);
    currentTime = moment().utc("+05:30").format('YYYY-M-DD HH:mm:ss');
    var currentTime2 = new Date(currentTime);
    var timeDiff = Math.abs(expectedTime2.getTime() - currentTime2.getTime()) / 1000;
    var days = Math.floor(timeDiff / 86400);
    timeDiff -= days * 86400;
    var hours = Math.floor(timeDiff / 3600) % 24;
    timeDiff -= hours * 3600;
    var minutes = Math.floor(timeDiff / 60) % 60;
    timeDiff -= minutes * 60;
    var seconds = timeDiff % 60;

    if(currentTime > expectedEndTime){
        $('#timer_'+id).html("<button class='btn btn-danger btn-sm'>Time Expired</button>");
        //console.log();
    }else{
        var exdate = moment(expectedTime, 'YYYY-M-DD HH:mm:ss').subtract(5,'minutes').format('YYYY-M-DD HH:mm:ss');
        //console.log(`${exdate}: 5 min before, | ${currentTime}:current time | ${expectedEndTime}: end time `);
        //$('#timer_'+id).html(`<a  href='${url}' class='btn btn-success btn-sm' target='_blank' >Start Live</a>`);
        

        if(exdate <= currentTime && exdate <= expectedEndTime){
             $('#timer_'+id).html(`<a  href='${url}' class='btn btn-success btn-sm' target='_blank' >Start Live</a>`);
             // $('#timer_'+id).html(`<button class='btn btn-success btn-sm' target='_blank' data-toggle="modal" data-target="#myModal">Start Live</button>`);
        }else{
            $('#timer_'+id).html("<button class='btn btn-success btn-sm'>"+days+" d "+hours+" h "+ parseInt(minutes-5) +" m "+seconds+" s Left</button>");
        }
        
       
        //console.log(exdate);
    }
              
}, 1000);
}

</script>
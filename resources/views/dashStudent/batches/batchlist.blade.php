
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">&#xE894;</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">{{ $title }}</h4>
                        <div class="row">
                            <div class="col-ms-12">
                                
                                <table id="tbleTeacherList" class="table table-striped table-no-bordered table-hover dataTable dtr-inline">
                                <thead>
                                    <tr style="font-size: 12px;">
                                        
                                        <th>Teacher Name</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Live Date</th>
                                        <th>Start End Time</th>
                                        <th>Amount</th>
                                        <th> Meeting ID</th>
                                        <th>Join</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($activestd_list as $ablist)
                                    <tr id="row_{{ $ablist->id }}">
                                        <!-- <td>{{ $ablist->teacherinfo['first_name'] }}</td> -->
                                        <td>{{ $ablist->teacherName['first_name'] }}</td>
                                        <td>{{ $ablist->title}}</td>
                                        <td>{{ $ablist->description}}</td>
                                        <td>{{ $ablist->liveDate}}</td>
                                        <td>{{ $ablist->startTime}} - {{$ablist->endTime}}</td>
                                        <td>{{ $ablist->amount}}</td>
                                        <td>{{ $ablist->meetingID}}</td>
                                        <td>
                                        <button type="button" class="btn btn-info btn-sm" onclick="openModal(
                                            '{{ $ablist->id }}',
                                            '{{ $ablist->teacherName['first_name'] }}',
                                            '{{ $ablist->title}}',
                                            '{{ $ablist->liveDate}}',
                                            '{{ $ablist->startTime}}',
                                            '{{$ablist->endTime}}',
                                            '{{ $ablist->amount}}'
                                            )">Join</button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title"></h4>
      </div>
      <div class="modal-body">
      <div style="text-align: center">
        	<img src="http://dosolbuk2nq6n.cloudfront.net/loader.gif" class="img-responsive" alt="AWS Loader" style="margin: auto;">
        	<h4><strong>Loading...</strong></h4>  
        </div>   
      </div>
      <div class="modal-footer">
            <form action="{{ url('/')}}/payTmSDK/pgRedirect.php" method="post">
                <input type="hidden" name="ORDER_ID" value="" id="ORDER_ID">
                <input type="hidden" name="CUST_ID" value="{{ $user_id }}" id="CUST_ID">
                <input type="hidden" name="INDUSTRY_TYPE_ID" value="Retail109" id="INDUSTRY_TYPE_ID">
                <input type="hidden" name="CHANNEL_ID" value="WEB" id="CHANNEL_ID">
                <input type="hidden" name="TXN_AMOUNT" value="" id="TXN_AMOUNT">
                <input type="hidden" name="Live_ID" value="" id="Live_ID">
                <input type="submit" name="Continue" value="Join" class="btn btn-success">
            </form>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div> 
<div></div>   
<style>
.modal-footer {
    padding: 15px;
    text-align: center !important;
}
</style>
<script>
// $('.modal-title').html("");
// $('.modal-body').html('<div style="text-align: center"><img src="http://dosolbuk2nq6n.cloudfront.net/loader.gif" class="img-responsive" alt="AWS Loader" style="margin: auto;"><h4><strong>Loading...</strong></h4></div>');
// $('#myModal').modal('show');
$(function(){
    dt('tbleTeacherList');
    @if(isset($error) && $error != '')
        error('{{$error}}');
    @endif

    @if(isset($success) && $success != '')
        success('{{$success}}');
    @endif
})
var formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'INR',
  minimumFractionDigits: 2,
});

let openModal = (id, tName, title, sdate, stime, etime, amount) => {
    var day = moment(sdate).format('DD-MM-YYYY');
    var sttime = moment(sdate+" "+stime).format('HH:mm a');
    var ettime = moment(sdate+" "+etime).format('HH:mm a');
    $('#TXN_AMOUNT').val(amount);
    $('#Live_ID').val(id);
    $('#ORDER_ID').val("LIVE{{rand(10000,999999)}}-{{ $user_id }}-"+id);

    if(amount == 0){
        var ammount = "Free";
    }else{
        var ammount = formatter.format(amount);
    }

    $('.modal-title').html("");
    $('.modal-body').html(`
    <div style="text-align: center">
        <h2 style="margin-top: -33px;">Payment Detail</h2>
        <h4><strong>${title}</strong></h4>
        <h6>by ${tName}</h6><hr>
        <h5>${day}</h5>
        <h5>${sttime}-${ettime}</h5>
        <h6>${ammount}</h6>
    </div>
    `);
    $('#myModal').modal('show');
}
</script>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">&#xE894;</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">{{ $title }}</h4>
                                    <div class="row">
                                        <div class="col-ms-12">

                                          <form method="post" action="{{URL::to('/students/batches/paystd')}}" enctype="multipart/form-data">
                                    @csrf

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pay Amount</label>
                                                    <input type="number" class="form-control" id="" name="amount" required>
                                                </div>

                                                <div class="form-group bmd-form-group col-md-6" id="class_username">
                                                    <label for="exampleUsername" class="bmd-label-floating">Pending Amount</label>
                                                    <input type="Number" class="form-control" id="" name="pending" required>
                                                </div>

                                              
                                        </div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group bmd-form-group col-md-12" id="submit_box">
                                                <button class="btn btn-success" >Pay</button>


                             </div>
                         </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
            
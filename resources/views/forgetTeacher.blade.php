<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ url('/') }}/public/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ url('/') }}/public/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="{{ url('/') }}/public/assets/css/animate.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/demo2.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/component.css" />
</head>
<body class="off-canvas-sidebar">
    <div class="demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="main-title col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">

                            <form method="post" action="{{URL::to('/forgetTeacher')}}">
                            @csrf
                                <div class="card card-login ">
                                    <div class="card-header text-center " data-background-color="rose">
                                        <h6 class="card-title" style="font-size: 20px;">Teacher Forget Password</h6>
                                    </div>
                                    </br>
                                    <div class="card-content">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">mobile_screen_share</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Mobile Number</label>
                                                <input type="number" id="mobile" max="9999999999" class="form-control" name="mobile">
                                            </div>
                                        </div>
                                        <span class="hidden" id="extraData">
                                            <input type="hidden" name="type" value="setpass">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Set Password</label>
                                                    <input type="text" minlength="8" class="form-control" name="password" required>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">fingerprint</i>
                                                </span>
                                                <div class="form-group label-floating">
                                                    <label class="control-label">OTP</label>
                                                    <input type="password" maxlength="4" class="form-control" name="otp" required>
                                                </div>
                                            </div>
                                        </span>
                                    </div>
                                    <div class="footer text-center" id="subBtn">
                                        <a onclick="chk_number();" class="btn btn-primary btn-wd btn-md">Validate</a>
                                    </div>
                                </div>
                            </form>
                        </div>
				</div>
			</div>
		</div>
    <style>
        .control-label{
            top: -10px !important;
        } 
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/public/assets/js/material.min.js" type="text/javascript"></script>
    <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
    <script src="{{ url('/') }}/public/assets/js/bootstrap-notify.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/TweenLite.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/EasePack.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/rAF.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/demo-1.js"></script>
    <script src="{{ url('/') }}/public/assets/js/sweetalert2.js"></script>
    <script src="{{ url('/') }}/public/assets/js/taktii.js"></script>
    <script>
    $('#mobile').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            chk_number();
            return false;  
        }
    });
    function chk_number(){
        var mob = $('#mobile').val();
        if(mob.length == 10){
            //alert(mob);
            loaderStart("Checking Number..");
            $.ajax({
                type:'get',
                url:'{{ url('/')}}/ajax/chk_forget_teacher_main/'+mob,
                success:function(data){
                    if(data.count > 0){
                        $('#extraData').removeClass('hidden');
                        $('#subBtn').html(`<button type="submit" class="btn btn-primary btn-wd btn-md">Let's go</button>`);
                    }else{
                        $.notify({
                        message: '<span style="font-size:16px;">Number not exist, Please Register.<a href="#">Link</a></span>'
                        },{
                            type: 'danger',
                            timer: 10000,
                            placement: {
                                from: "bottom",
                                align: "left"
                            },
                            animate: {
                                enter: 'animated pulse',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                    loaderEnd();
                }
            })
            
        }else{
            var error = '<span style="font-size:16px;">Please Enter valid mobile number</span>';
            $.notify({
                message: error 
            },{
                type: 'danger',
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                animate: {
                    enter: 'animated pulse',
                    exit: 'animated fadeOutUp'
                }
            });
        }
        

        
    }

        $(function(){
            <?php
            if(isset($error)){ ?>
            //http://bootstrap-notify.remabledesigns.com/
            var error = '<span style="font-size:16px;"><?php echo $error ?></span>';
            $.notify({
                message: error 
            },{
                type: 'danger',
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                animate: {
                    enter: 'animated pulse',
                    exit: 'animated fadeOutUp'
                }
            });
            <?php }  ?>

            <?php
            if(isset($success)){ ?>
            //http://bootstrap-notify.remabledesigns.com/
            var error = '<span style="font-size:16px;"><?php echo $success ?></span>';
            $.notify({
                message: success 
            },{
                type: 'success',
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                animate: {
                    enter: 'animated pulse',
                    exit: 'animated fadeOutUp'
                }
            });
            <?php }  ?>

        })
    </script>
</body>
</html>
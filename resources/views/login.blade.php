<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="{{ url('/') }}/public/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ url('/') }}/public/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <link href="{{ url('/') }}/public/assets/css/animate.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/demo2.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/public/assets/css/component.css" />
</head>
<body class="off-canvas-sidebar">
    <div class="demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="main-title col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">

                            <form method="post" action="{{URL::to('/login')}}">
                            @csrf
                                <div class="card card-login">
                                    <div class="card-header text-center" data-background-color="rose">
                                        <h6 class="card-title">Institute Login</h6>
                                        <div style="font-size:11px;">or Login as</div>
                                        <a href="{{ url('/') }}/login/teacher" class="btn btn-primary btn-md" >Teacher</a>
                                        <a href="{{ url('/') }}/login/student" class="btn btn-success btn-md" >student</a>
                                    </div>
                                    </br>
                                    <div class="card-content">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Username</label>
                                                <input type="text" class="form-control" name="username">
                                            </div>
                                        </div>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" name="password">
                                            </div>
                                        </div>
                                        
                                        <!-- <div class="input-group center-block">
                                        <h6 style="font-size:12px;margin: 0px;line-height: 0;padding-top: 15px;">You are ?</h6>
                                            <label class="radio-inline"><input type="radio" name="user_type" value="0" checked required><span class="text-primary">Admin</span></label>
                                            <label class="radio-inline"><input type="radio" name="user_type" value="1" required><span class="text-info">Teacher</span></label>
                                            <label class="radio-inline"><input type="radio" name="user_type" value="2" required><span class="text-success">Student</span></label>
                                        </div> -->

                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-rose btn-wd btn-md">Let's go</button>
                                    </div>
                                </div>
                            </form>
                        </div>
				</div>
			</div>
		</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="{{ url('/') }}/public/assets/js/material.min.js" type="text/javascript"></script>
    <!--  Notifications Plugin, full documentation here: http://bootstrap-notify.remabledesigns.com/    -->
    <script src="{{ url('/') }}/public/assets/js/bootstrap-notify.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/TweenLite.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/EasePack.min.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/rAF.js"></script>
    <script src="{{ url('/') }}/public/assets/js/login/demo-1.js"></script>
    <script>
        $(function(){
            <?php
            if(isset($error)){ ?>
            //http://bootstrap-notify.remabledesigns.com/
            var error = '<span style="font-size:16px;"><?php echo $error ?></span>';
            $.notify({
                message: error 
            },{
                type: 'danger',
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                animate: {
                    enter: 'animated pulse',
                    exit: 'animated fadeOutUp'
                }
            });
            <?php }  ?>

            <?php
            if(isset($success)){ ?>
            //http://bootstrap-notify.remabledesigns.com/
            var error = '<span style="font-size:16px;"><?php echo $success ?></span>';
            $.notify({
                message: success 
            },{
                type: 'success',
                timer: 1000,
                placement: {
                    from: "bottom",
                    align: "left"
                },
                animate: {
                    enter: 'animated pulse',
                    exit: 'animated fadeOutUp'
                }
            });
            <?php }  ?>

        })
    </script>
</body>
</html>